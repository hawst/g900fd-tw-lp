.class public final Lcom/google/android/music/store/KeepOnRadioUtils;
.super Ljava/lang/Object;
.source "KeepOnRadioUtils.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/KeepOnRadioUtils;->LOGV:Z

    return-void
.end method

.method private static addMusicIdsOfCachedSubmittedRadioSongs(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/ArrayList;)V
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "musicIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 345
    const-string v1, "RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id) "

    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "MusicId"

    aput-object v0, v2, v6

    const-string v3, "RadioStationId=? AND State=? AND LocalCopyType>?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const/16 v0, 0x3e8

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    const/4 v0, 0x2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    const-string v7, "RANDOM()"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 362
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 363
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 368
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 370
    return-void
.end method

.method static deleteRadioSongsFromUnpinnedStations(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 379
    const-string v0, "RADIO_SONGS"

    const-string v1, "RadioStationId NOT IN (SELECT KEEPON.RadioStationId FROM KEEPON WHERE KEEPON.RadioStationId NOT NULL)"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static getBatchReadyPercent(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 406
    const-string v0, "music_keepon_radio_batch_ready_percent"

    const/16 v1, 0x50

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static getBatchSize(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 400
    const-string v0, "music_keepon_radio_batch_size"

    const/16 v1, 0x19

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static getKeepOnStationsToUpdate(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Collection;
    .locals 12
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 210
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v10, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "KEEPON.RadioStationId"

    aput-object v0, v2, v1

    const-string v0, "KEEPON.KeepOnId"

    aput-object v0, v2, v3

    .line 212
    .local v2, "columns":[Ljava/lang/String;
    const-string v1, "KEEPON"

    const-string v3, "KEEPON.RadioStationId NOT NULL AND  NOT EXISTS (SELECT 1 FROM RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id)  WHERE RADIO_SONGS.RadioStationId=KEEPON.RadioStationId AND RADIO_SONGS.State=0 )"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 222
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 223
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 224
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 226
    .local v11, "stationId":Ljava/lang/Long;
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 227
    .local v9, "keepOnId":Ljava/lang/Long;
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v11, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 231
    .end local v9    # "keepOnId":Ljava/lang/Long;
    .end local v11    # "stationId":Ljava/lang/Long;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 234
    return-object v10
.end method

.method private static getKeepOnStationsToUpdate(Lcom/google/android/music/store/Store;)Ljava/util/Collection;
    .locals 2
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/store/Store;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 244
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/KeepOnRadioUtils;->getKeepOnStationsToUpdate(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 246
    invoke-virtual {p0, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {p0, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v1
.end method

.method private static getNumberOfAllowedUncachedSongs(II)I
    .locals 4
    .param p0, "batchSize"    # I
    .param p1, "requiredCachedPercent"    # I

    .prologue
    const/4 v1, 0x0

    .line 422
    const/16 v2, 0x64

    if-lt p1, v2, :cond_0

    move v0, v1

    .line 433
    :goto_0
    return v0

    .line 424
    :cond_0
    if-gtz p1, :cond_1

    .line 425
    add-int/lit8 v0, p0, -0x1

    goto :goto_0

    .line 427
    :cond_1
    int-to-float v2, p0

    int-to-float v3, p1

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    sub-int v0, p0, v2

    .line 430
    .local v0, "allowedUncached":I
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 432
    add-int/lit8 v1, p0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 433
    goto :goto_0
.end method

.method static isEagerRefill(Landroid/content/ContentResolver;)Z
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 394
    const-string v0, "music_keepon_radio_eager_refill"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static refreshAllKeepOnStations(Landroid/content/Context;)Ljava/util/List;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v5, "updatedKeepOnIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/store/KeepOnRadioUtils;->getKeepOnStationsToUpdate(Lcom/google/android/music/store/Store;)Ljava/util/Collection;

    move-result-object v4

    .line 165
    .local v4, "stationsToUpdate":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 166
    .local v2, "stationAndKeepOnId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    .line 167
    .local v3, "stationId":Ljava/lang/Long;
    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    .line 168
    .local v1, "keepOnId":Ljava/lang/Long;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {p0, v6, v7}, Lcom/google/android/music/store/KeepOnRadioUtils;->refreshRadioIfNeeded(Landroid/content/Context;J)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 169
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    .end local v1    # "keepOnId":Ljava/lang/Long;
    .end local v2    # "stationAndKeepOnId":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v3    # "stationId":Ljava/lang/Long;
    :cond_1
    return-object v5
.end method

.method static refreshKeepOnStations(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "stationsToUpdate":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 182
    .local v0, "howManyRefreshed":I
    if-eqz p1, :cond_1

    .line 183
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 184
    .local v2, "radioStationId":Ljava/lang/Long;
    if-eqz v2, :cond_0

    .line 185
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/google/android/music/store/KeepOnRadioUtils;->refreshRadioIfNeeded(Landroid/content/Context;J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "radioStationId":Ljava/lang/Long;
    :cond_1
    return v0
.end method

.method static refreshRadioIfNeeded(Landroid/content/Context;J)Z
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "radioStationId"    # J

    .prologue
    .line 71
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 72
    const/16 v16, 0x0

    .line 117
    :goto_0
    return v16

    .line 75
    :cond_0
    const/16 v16, 0x0

    .line 76
    .local v16, "success":Z
    new-instance v6, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 77
    .local v6, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/KeepOnRadioUtils;->getBatchSize(Landroid/content/ContentResolver;)I

    move-result v10

    .line 78
    .local v10, "maxEntries":I
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v14

    .line 81
    .local v14, "store":Lcom/google/android/music/store/Store;
    move-wide/from16 v0, p1

    invoke-static {v14, v0, v1}, Lcom/google/android/music/store/RadioStation;->getSubmittedRadioSongIds(Lcom/google/android/music/store/Store;J)Ljava/util/List;

    move-result-object v15

    .line 83
    .local v15, "submittedSongIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    invoke-virtual {v14, v15}, Lcom/google/android/music/store/Store;->getServerTrackIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 84
    .local v5, "blackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    const/4 v13, 0x0

    .line 85
    .local v13, "remoteRadioId":Ljava/lang/String;
    new-instance v12, Ljava/lang/Object;

    invoke-direct {v12}, Ljava/lang/Object;-><init>()V

    .line 86
    .local v12, "prefsHolder":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v11

    .line 88
    .local v11, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v11}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v3

    .line 89
    .local v3, "account":Landroid/accounts/Account;
    if-eqz v3, :cond_1

    .line 90
    invoke-static {v3}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v4

    .line 91
    .local v4, "accountId":I
    move-wide/from16 v0, p1

    invoke-virtual {v14, v0, v1}, Lcom/google/android/music/store/Store;->getRadioRemoteId(J)Ljava/lang/String;

    move-result-object v13

    .line 92
    invoke-virtual {v11}, Lcom/google/android/music/preferences/MusicPreferences;->getContentFilter()I

    move-result v7

    .line 93
    .local v7, "contentFilter":I
    invoke-interface {v6, v13, v10, v5, v7}, Lcom/google/android/music/cloudclient/MusicCloud;->getRadioFeed(Ljava/lang/String;ILjava/util/List;I)Lcom/google/android/music/cloudclient/RadioFeedResponse;

    move-result-object v9

    .line 95
    .local v9, "feed":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    const/16 v17, 0x0

    .line 96
    .local v17, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    if-eqz v9, :cond_1

    .line 97
    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/RadioFeedResponse;->getFirstStationTracks()Ljava/util/List;

    move-result-object v17

    .line 98
    if-eqz v17, :cond_1

    .line 99
    move-wide/from16 v0, p1

    move-object/from16 v2, v17

    invoke-static {v14, v4, v0, v1, v2}, Lcom/google/android/music/store/KeepOnRadioUtils;->replaceWithNewSongs(Lcom/google/android/music/store/Store;IJLjava/util/List;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v18

    if-lez v18, :cond_2

    const/16 v16, 0x1

    .line 114
    .end local v4    # "accountId":I
    .end local v7    # "contentFilter":I
    .end local v9    # "feed":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .end local v17    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_1
    :goto_1
    invoke-static {v12}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    .restart local v4    # "accountId":I
    .restart local v7    # "contentFilter":I
    .restart local v9    # "feed":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .restart local v17    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_2
    const/16 v16, 0x0

    goto :goto_1

    .line 104
    .end local v3    # "account":Landroid/accounts/Account;
    .end local v4    # "accountId":I
    .end local v7    # "contentFilter":I
    .end local v9    # "feed":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .end local v17    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :catch_0
    move-exception v8

    .line 105
    .local v8, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v18, "KeepOnRadio"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "DB error while updating pinned radio. "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    invoke-static {v12}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 106
    .end local v8    # "e":Landroid/database/SQLException;
    :catch_1
    move-exception v8

    .line 107
    .local v8, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v18, "KeepOnRadio"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Interrupted while updating pinned radio. "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 114
    invoke-static {v12}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 108
    .end local v8    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v8

    .line 109
    .local v8, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    const-string v18, "KeepOnRadio"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Could not find pinned radio. "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 114
    invoke-static {v12}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 110
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v8

    .line 111
    .local v8, "e":Ljava/io/IOException;
    :try_start_4
    const-string v18, "KeepOnRadio"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Failed to get songs of pinned radio ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ") "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 114
    invoke-static {v12}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    invoke-static {v12}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v18
.end method

.method private static replaceWithNewSongs(Lcom/google/android/music/store/Store;IJLjava/util/List;)I
    .locals 6
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "account"    # I
    .param p2, "radioStationId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/store/Store;",
            "IJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .local p4, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    const/4 v4, 0x0

    .line 133
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v4

    .line 136
    :cond_1
    const/4 v3, 0x0

    .line 138
    .local v3, "success":Z
    invoke-virtual {p0}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 140
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v1, p1, p4, v5}, Lcom/google/android/music/store/Store;->tryToInsertOrUpdateExternalSongs(Landroid/database/sqlite/SQLiteDatabase;ILjava/util/List;Z)Ljava/util/List;

    move-result-object v2

    .line 141
    .local v2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 142
    invoke-static {v1, p2, p3}, Lcom/google/android/music/store/RadioStation;->deleteNewSongs(Landroid/database/sqlite/SQLiteDatabase;J)I

    .line 143
    invoke-static {v1, p2, p3, v2}, Lcom/google/android/music/store/RadioStation;->addRadioSongs(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/List;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 144
    .local v0, "count":I
    if-lez v0, :cond_2

    const/4 v3, 0x1

    .line 150
    :goto_1
    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    move v4, v0

    goto :goto_0

    :cond_2
    move v3, v4

    .line 144
    goto :goto_1

    .line 150
    .end local v0    # "count":I
    :cond_3
    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto :goto_0

    .end local v2    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v4

    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v4
.end method

.method static updateKeepOnRadio(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 49
    .local v1, "store":Lcom/google/android/music/store/Store;
    const/4 v2, 0x0

    .line 50
    .local v2, "success":Z
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 52
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/RadioStation;->deleteOrphanedRadioSongs(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    const/4 v2, 0x1

    .line 55
    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 58
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnRadioUtils;->refreshAllKeepOnStations(Landroid/content/Context;)Ljava/util/List;

    .line 59
    return-void

    .line 55
    :catchall_0
    move-exception v3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3
.end method

.method public static useCachedSongs(Landroid/content/Context;JZ)Ljava/util/List;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "radioStationId"    # J
    .param p3, "offline"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JZ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v17

    .line 268
    .local v17, "store":Lcom/google/android/music/store/Store;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v16

    .line 270
    .local v16, "resolver":Landroid/content/ContentResolver;
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/KeepOnRadioUtils;->getBatchReadyPercent(Landroid/content/ContentResolver;)I

    move-result v15

    .line 271
    .local v15, "requiredCachedPercent":I
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 272
    .local v14, "musicIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v18, 0x0

    .line 275
    .local v18, "success":Z
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 276
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v12, 0x0

    .line 279
    .local v12, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id) "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "MusicId"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "LocalCopyType"

    aput-object v6, v4, v5

    const-string v5, "RadioStationId=? AND State=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "RADIO_SONGS.Id"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 291
    if-eqz v12, :cond_1

    .line 292
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v11

    .line 293
    .local v11, "batchSize":I
    invoke-static {v11, v15}, Lcom/google/android/music/store/KeepOnRadioUtils;->getNumberOfAllowedUncachedSongs(II)I

    move-result v10

    .line 295
    .local v10, "allowedUncached":I
    sget-boolean v3, Lcom/google/android/music/store/KeepOnRadioUtils;->LOGV:Z

    if-eqz v3, :cond_0

    const-string v3, "KeepOnRadio"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Allow "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " uncached songs for batch size of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_0
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 298
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 299
    .local v13, "copyType":I
    const/16 v3, 0xc8

    if-ge v13, v3, :cond_5

    .line 300
    add-int/lit8 v10, v10, -0x1

    if-gez v10, :cond_0

    .line 302
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    .line 311
    .end local v10    # "allowedUncached":I
    .end local v11    # "batchSize":I
    .end local v13    # "copyType":I
    :cond_1
    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 312
    sget-boolean v3, Lcom/google/android/music/store/KeepOnRadioUtils;->LOGV:Z

    if-eqz v3, :cond_2

    const-string v3, "KeepOnRadio"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Will use "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new songs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_2
    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1}, Lcom/google/android/music/store/RadioStation;->deleteSubmittedRadioSongs(Landroid/database/sqlite/SQLiteDatabase;J)I

    .line 318
    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1}, Lcom/google/android/music/store/RadioStation;->markNewRadioSongsAsSubmitted(Landroid/database/sqlite/SQLiteDatabase;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    :cond_3
    :goto_1
    const/16 v18, 0x1

    .line 330
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 331
    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 334
    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/KeepOnRadioUtils;->isEagerRefill(Landroid/content/ContentResolver;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 336
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateOnlineContainers(Landroid/content/Context;)V

    .line 339
    :cond_4
    return-object v14

    .line 306
    .restart local v10    # "allowedUncached":I
    .restart local v11    # "batchSize":I
    .restart local v13    # "copyType":I
    :cond_5
    const/4 v3, 0x0

    :try_start_1
    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 330
    .end local v10    # "allowedUncached":I
    .end local v11    # "batchSize":I
    .end local v13    # "copyType":I
    :catchall_0
    move-exception v3

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 331
    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v3

    .line 320
    :cond_6
    if-eqz p3, :cond_7

    .line 322
    :try_start_2
    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1, v14}, Lcom/google/android/music/store/KeepOnRadioUtils;->addMusicIdsOfCachedSubmittedRadioSongs(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/ArrayList;)V

    .line 323
    sget-boolean v3, Lcom/google/android/music/store/KeepOnRadioUtils;->LOGV:Z

    if-eqz v3, :cond_3

    const-string v3, "KeepOnRadio"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Will use "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " old songs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 325
    :cond_7
    sget-boolean v3, Lcom/google/android/music/store/KeepOnRadioUtils;->LOGV:Z

    if-eqz v3, :cond_3

    const-string v3, "KeepOnRadio"

    const-string v4, "No new songs are available for online playback"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
