.class public Lcom/google/android/music/download/stream/AllowedStreams;
.super Ljava/lang/Object;
.source "AllowedStreams.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mAllowed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/stream/StreamingContent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/stream/AllowedStreams;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/AllowedStreams;->mAllowed:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public declared-synchronized findStreamByRequest(Lcom/google/android/music/download/TrackDownloadRequest;)Lcom/google/android/music/download/stream/StreamingContent;
    .locals 5
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/stream/AllowedStreams;->mAllowed:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 62
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v0, p1}, Lcom/google/android/music/download/stream/StreamingContent;->hasRequest(Lcom/google/android/music/download/TrackDownloadRequest;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    sget-boolean v2, Lcom/google/android/music/download/stream/AllowedStreams;->LOGV:Z

    if-eqz v2, :cond_1

    .line 64
    const-string v2, "AllowedStreams"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findStreamByRequest: found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 69
    :cond_2
    :try_start_1
    sget-boolean v2, Lcom/google/android/music/download/stream/AllowedStreams;->LOGV:Z

    if-eqz v2, :cond_3

    .line 70
    const-string v2, "AllowedStreams"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findStreamByRequest: didn\'t find for request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 61
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized findStreamBySecureId(J)Lcom/google/android/music/download/stream/StreamingContent;
    .locals 5
    .param p1, "secureId"    # J

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/stream/AllowedStreams;->mAllowed:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 82
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingContent;->getSecureId()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 83
    sget-boolean v2, Lcom/google/android/music/download/stream/AllowedStreams;->LOGV:Z

    if-eqz v2, :cond_1

    .line 84
    const-string v2, "AllowedStreams"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findStreamById: found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 89
    :cond_2
    :try_start_1
    sget-boolean v2, Lcom/google/android/music/download/stream/AllowedStreams;->LOGV:Z

    if-eqz v2, :cond_3

    .line 90
    const-string v2, "AllowedStreams"

    const-string v3, "findStreamById: didn\'t find streaming content"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setAllowedStreams(Lcom/google/android/music/download/stream/StreamingContent;Lcom/google/android/music/download/stream/StreamingContent;)V
    .locals 4
    .param p1, "current"    # Lcom/google/android/music/download/stream/StreamingContent;
    .param p2, "next"    # Lcom/google/android/music/download/stream/StreamingContent;

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/music/download/stream/AllowedStreams;->LOGV:Z

    if-eqz v0, :cond_0

    .line 41
    const-string v0, "AllowedStreams"

    const-string v1, "setAllowedStreams: current=%s next=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/AllowedStreams;->mAllowed:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 46
    if-eqz p1, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/android/music/download/stream/AllowedStreams;->mAllowed:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_1
    if-eqz p2, :cond_2

    .line 51
    iget-object v0, p0, Lcom/google/android/music/download/stream/AllowedStreams;->mAllowed:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :cond_2
    monitor-exit p0

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 26
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v3, "("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    iget-object v3, p0, Lcom/google/android/music/download/stream/AllowedStreams;->mAllowed:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/stream/StreamingContent;

    .line 29
    .local v1, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v1}, Lcom/google/android/music/download/stream/StreamingContent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 26
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 32
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    monitor-exit p0

    return-object v3
.end method
