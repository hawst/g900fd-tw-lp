.class public Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;
.super Ljava/lang/Object;
.source "PostFroyoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/PostFroyoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SQLiteDatabaseComp"
.end annotation


# direct methods
.method public static query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;
    .locals 10
    .param p0, "qb"    # Landroid/database/sqlite/SQLiteQueryBuilder;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "groupBy"    # Ljava/lang/String;
    .param p6, "having"    # Ljava/lang/String;
    .param p7, "sortOrder"    # Ljava/lang/String;
    .param p8, "limit"    # Ljava/lang/String;
    .param p9, "cancellationSignal"    # Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    .prologue
    .line 436
    if-eqz p9, :cond_0

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->hasSignal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    invoke-virtual/range {p9 .. p9}, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->getCancellationSignal()Landroid/os/CancellationSignal;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    .line 440
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual/range {p0 .. p8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public static rawQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "queryArgs"    # [Ljava/lang/String;
    .param p3, "cancellationSignal"    # Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    .prologue
    .line 426
    invoke-virtual {p3}, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->hasSignal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {p3}, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->getCancellationSignal()Landroid/os/CancellationSignal;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    .line 429
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method
