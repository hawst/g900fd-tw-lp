.class public Lcom/google/android/music/io/ChunkedOutputStreamAdapter;
.super Ljava/io/OutputStream;
.source "ChunkedOutputStreamAdapter.java"


# instance fields
.field final mBuffer:[B

.field final mChunkSize:I

.field final mOutput:Lcom/google/android/music/io/ChunkedOutputStream;

.field mPosition:I

.field mWroteShort:Z


# direct methods
.method public constructor <init>(Lcom/google/android/music/io/ChunkedOutputStream;)V
    .locals 1
    .param p1, "chunkedOutputStream"    # Lcom/google/android/music/io/ChunkedOutputStream;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mOutput:Lcom/google/android/music/io/ChunkedOutputStream;

    .line 16
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mOutput:Lcom/google/android/music/io/ChunkedOutputStream;

    invoke-interface {v0}, Lcom/google/android/music/io/ChunkedOutputStream;->getChunkSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mChunkSize:I

    .line 17
    iget v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mChunkSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mBuffer:[B

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    .line 19
    return-void
.end method

.method private flushBuffer()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 67
    iget v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    if-nez v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mWroteShort:Z

    if-eqz v0, :cond_1

    .line 71
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t write anything after a short block has been written."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    iget v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    iget v2, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mChunkSize:I

    if-ge v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mWroteShort:Z

    .line 75
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mOutput:Lcom/google/android/music/io/ChunkedOutputStream;

    iget-object v2, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mBuffer:[B

    iget v3, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/music/io/ChunkedOutputStream;->write([BII)V

    .line 76
    iput v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 74
    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->flushBuffer()V

    .line 24
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mOutput:Lcom/google/android/music/io/ChunkedOutputStream;

    invoke-interface {v0}, Lcom/google/android/music/io/ChunkedOutputStream;->close()V

    .line 25
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->flushBuffer()V

    .line 31
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mOutput:Lcom/google/android/music/io/ChunkedOutputStream;

    invoke-interface {v0}, Lcom/google/android/music/io/ChunkedOutputStream;->flush()V

    .line 32
    return-void
.end method

.method public write(I)V
    .locals 3
    .param p1, "oneByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mBuffer:[B

    iget v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 37
    iget v0, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    iget v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mChunkSize:I

    if-ne v0, v1, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->flushBuffer()V

    .line 40
    :cond_0
    return-void
.end method

.method public write([B)V
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->write([BII)V

    .line 45
    return-void
.end method

.method public write([BII)V
    .locals 3
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    :goto_0
    if-lez p3, :cond_2

    .line 50
    iget v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mChunkSize:I

    iget v2, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    sub-int/2addr v1, v2

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 51
    .local v0, "bytesToCopy":I
    iget v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mChunkSize:I

    if-ne v0, v1, :cond_1

    .line 53
    iget-object v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mOutput:Lcom/google/android/music/io/ChunkedOutputStream;

    invoke-interface {v1, p1, p2, v0}, Lcom/google/android/music/io/ChunkedOutputStream;->write([BII)V

    .line 61
    :cond_0
    :goto_1
    add-int/2addr p2, v0

    .line 62
    sub-int/2addr p3, v0

    .line 63
    goto :goto_0

    .line 55
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mBuffer:[B

    iget v2, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    iget v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    .line 57
    iget v1, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mPosition:I

    iget v2, p0, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->mChunkSize:I

    if-ne v1, v2, :cond_0

    .line 58
    invoke-direct {p0}, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;->flushBuffer()V

    goto :goto_1

    .line 64
    .end local v0    # "bytesToCopy":I
    :cond_2
    return-void
.end method
