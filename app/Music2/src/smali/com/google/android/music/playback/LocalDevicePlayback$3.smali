.class Lcom/google/android/music/playback/LocalDevicePlayback$3;
.super Lcom/google/android/music/net/IStreamabilityChangeListener$Stub;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Lcom/google/android/music/net/IStreamabilityChangeListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStreamabilityChanged(Z)V
    .locals 3
    .param p1, "isStreamable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$500(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 378
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z
    invoke-static {v0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$502(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 379
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Streamability changed to :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsStreamingEnabled:Z
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$500(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Updating the next track"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$3;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$700(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 387
    :cond_1
    return-void
.end method
