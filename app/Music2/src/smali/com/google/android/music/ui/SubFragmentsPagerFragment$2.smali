.class Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;
.super Ljava/lang/Object;
.source "SubFragmentsPagerFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

.field final synthetic val$pager:Landroid/support/v4/view/ViewPager;

.field final synthetic val$tabBar:Lcom/google/android/music/ui/PlayTabContainer;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SubFragmentsPagerFragment;Lcom/google/android/music/ui/PlayTabContainer;Landroid/support/v4/view/ViewPager;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->val$tabBar:Lcom/google/android/music/ui/PlayTabContainer;

    iput-object p3, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->val$pager:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->val$tabBar:Lcom/google/android/music/ui/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/PlayTabContainer;->onPageScrollStateChanged(I)V

    .line 132
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "offset"    # F
    .param p3, "offsetPixels"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->val$tabBar:Lcom/google/android/music/ui/PlayTabContainer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/ui/PlayTabContainer;->onPageScrolled(IFI)V

    .line 127
    return-void
.end method

.method public onPageSelected(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 109
    iget-object v1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->val$tabBar:Lcom/google/android/music/ui/PlayTabContainer;

    invoke-virtual {v1, p1}, Lcom/google/android/music/ui/PlayTabContainer;->onPageSelected(I)V

    .line 111
    iget-object v1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    # getter for: Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mAdapter:Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;
    invoke-static {v1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->access$000(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 113
    .local v0, "selectedTabTitle":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    # getter for: Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->access$100(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->val$pager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    invoke-virtual {v3}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0b0353

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->val$pager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    invoke-virtual {v5}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/music/utils/ViewUtils;->announceTextForAccessibility(Landroid/view/accessibility/AccessibilityManager;Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    return-void
.end method
