.class public Lcom/google/android/music/ui/MaterialContainerHeaderBg;
.super Landroid/widget/FrameLayout;
.source "MaterialContainerHeaderBg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final sBackgroundHandler:Landroid/os/Handler;

.field private static final sMainHandler:Landroid/os/Handler;


# instance fields
.field private mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mArtClearedOnStop:Z

.field private final mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

.field private final mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

.field private mArtistArtUrls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentArtistArtUrlIndex:I

.field private mHeight:I

.field private mShowingFirstSlide:Z

.field private mSingleArtistArtUrl:Ljava/lang/String;

.field private mSlideRefreshRunnable:Ljava/lang/Runnable;

.field private mSlideShowHandler:Landroid/os/Handler;

.field private mSlideShowInitialized:Z

.field private mSlideSwitchRunnable:Ljava/lang/Runnable;

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mViewFlipper:Landroid/widget/ViewFlipper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v1

    sput-boolean v1, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->LOGV:Z

    .line 68
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->sMainHandler:Landroid/os/Handler;

    .line 69
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ArtHelperThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 70
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 71
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->sBackgroundHandler:Landroid/os/Handler;

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 102
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 76
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Top"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 77
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Bottom"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 92
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z

    .line 93
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 106
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Top"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 77
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Bottom"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 92
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z

    .line 93
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Top"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 77
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Bottom"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 92
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z

    .line 93
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 76
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Top"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 77
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const-string v1, "Bottom"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    .line 92
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z

    .line 93
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    .line 116
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->sMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1100()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->sBackgroundHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Landroid/widget/ViewFlipper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/music/ui/MaterialContainerHeaderBg;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600()Z
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->LOGV:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getNextArtistUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private artistSlideshowAvailable()Z
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0}, Lcom/google/android/music/medialist/SongList;->shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private extractArtistArtUrls(Landroid/database/Cursor;)V
    .locals 14
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, -0x1

    .line 373
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 375
    .local v4, "start":J
    const-string v7, "ArtistArtLocation"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 378
    .local v0, "artistArtIdx":I
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 383
    .local v3, "origPos":I
    invoke-interface {p1, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 385
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 386
    .local v1, "artistsArtUrlsSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 387
    .local v2, "i":I
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v7

    const/16 v8, 0xa

    if-ge v7, v8, :cond_2

    const/16 v7, 0x3e8

    if-ge v2, v7, :cond_2

    .line 390
    if-ltz v0, :cond_1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 393
    .local v6, "url":Ljava/lang/String;
    :goto_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 394
    invoke-virtual {v1, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 396
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 397
    goto :goto_0

    .line 390
    .end local v6    # "url":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 401
    :cond_2
    invoke-interface {p1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v7

    if-nez v7, :cond_3

    if-eq v3, v9, :cond_3

    .line 402
    const-string v7, "MaterialHeaderBg"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to restore cursor position. Current pos="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_3
    new-instance v7, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v7, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    .line 408
    iget-object v7, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 410
    sget-boolean v7, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->LOGV:Z

    if-eqz v7, :cond_4

    .line 411
    const-string v7, "MaterialHeaderBg"

    const-string v8, "Gathering %d artist urls took: %d ms. Looked at %d rows."

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_4
    return-void
.end method

.method private getFirstArtistUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 459
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mCurrentArtistArtUrlIndex:I

    .line 460
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mCurrentArtistArtUrlIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private getNextArtistUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 468
    iget v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mCurrentArtistArtUrlIndex:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mCurrentArtistArtUrlIndex:I

    .line 469
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mCurrentArtistArtUrlIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private setupArtistArtSlideShow(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->hasCount(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 307
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 308
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 311
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$1;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_2

    .line 334
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    .line 364
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->startArtistArtSlideShow()V

    .line 366
    :cond_3
    return-void
.end method

.method private showDefaultForNoArtistArt()V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->showDefaultArtistArt()V

    .line 286
    return-void
.end method

.method private startArtistArtSlideShow()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x5dc

    .line 422
    iget-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 455
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 425
    sget-boolean v2, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->LOGV:Z

    if-eqz v2, :cond_0

    .line 426
    const-string v2, "MaterialHeaderBg"

    const-string v3, "Not enough artists to do a slide show"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v2

    if-eqz v2, :cond_3

    .line 433
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->showNext()V

    .line 437
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getFirstArtistUrl()Ljava/lang/String;

    move-result-object v0

    .line 438
    .local v0, "firstUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    invoke-virtual {v2, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->requestBitmap(Ljava/lang/String;)V

    .line 441
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getNextArtistUrl()Ljava/lang/String;

    move-result-object v1

    .line 442
    .local v1, "secondUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    invoke-virtual {v2, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->requestBitmap(Ljava/lang/String;)V

    .line 445
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050007

    invoke-virtual {v2, v3, v4}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 446
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050008

    invoke-virtual {v2, v3, v4}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 447
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 448
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getOutAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 451
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z

    .line 452
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 453
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 454
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 122
    const v0, 0x7f0e00d1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 125
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    if-nez v0, :cond_0

    .line 126
    const v0, 0x7f0e00e7

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    .line 127
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const v0, 0x7f0e0196

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->setImageView(Landroid/widget/ImageView;)V

    .line 129
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    const v0, 0x7f0e0197

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->setImageView(Landroid/widget/ImageView;)V

    .line 131
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 135
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getMeasuredWidth()I

    move-result v2

    .line 138
    .local v2, "width":I
    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 141
    .local v1, "exactWidthMeasureSpec":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->usingAlbumArtForBackground()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 144
    iput v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mHeight:I

    .line 145
    iget v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mHeight:I

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 146
    .local v0, "exactHeightMeasureSpec":I
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v3, v1, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->measure(II)V

    .line 154
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->setMeasuredDimension(II)V

    .line 155
    return-void

    .line 149
    .end local v0    # "exactHeightMeasureSpec":I
    :cond_0
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    int-to-double v6, v2

    mul-double/2addr v4, v6

    double-to-int v3, v4

    iput v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mHeight:I

    .line 150
    iget v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mHeight:I

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 151
    .restart local v0    # "exactHeightMeasureSpec":I
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3, v1, v0}, Landroid/widget/ViewFlipper;->measure(II)V

    goto :goto_0
.end method

.method onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    if-eqz v0, :cond_1

    .line 242
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    if-eqz v0, :cond_0

    .line 243
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->artistSlideshowAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 245
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->startArtistArtSlideShow()V

    .line 256
    :cond_0
    :goto_0
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    .line 258
    :cond_1
    return-void

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 248
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSingleArtistArtUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 251
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSingleArtistArtUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_4
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->showDefaultForNoArtistArt()V

    goto :goto_0
.end method

.method onStop()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->clearArtistArt()V

    .line 270
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->clearArtistArt()V

    .line 272
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowInitialized:Z

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 277
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtClearedOnStop:Z

    .line 279
    :cond_1
    return-void
.end method

.method setCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mViewFlipper:Landroid/widget/ViewFlipper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0}, Lcom/google/android/music/medialist/SongList;->shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0}, Lcom/google/android/music/medialist/SongList;->shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->hasCount(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->extractArtistArtUrls(Landroid/database/Cursor;)V

    .line 223
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->artistSlideshowAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->setupArtistArtSlideShow(Landroid/database/Cursor;)V

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 226
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->showDefaultForNoArtistArt()V

    goto :goto_0
.end method

.method setHeaderArt(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->usingAlbumArtForBackground()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v0, :cond_1

    .line 184
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setSharedPlaylistArt(Ljava/lang/String;)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setArtForSonglist(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 195
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0}, Lcom/google/android/music/medialist/SongList;->shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 202
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSingleArtistArtUrl:Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    iget-object v1, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0

    .line 205
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->showDefaultForNoArtistArt()V

    goto :goto_0
.end method

.method setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 0
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 172
    return-void
.end method

.method usingAlbumArtForBackground()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
