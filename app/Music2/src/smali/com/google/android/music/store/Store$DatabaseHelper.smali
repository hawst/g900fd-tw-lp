.class Lcom/google/android/music/store/Store$DatabaseHelper;
.super Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;
.source "Store.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/Store;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatabaseHelper"
.end annotation


# instance fields
.field private mDBPath:Ljava/lang/String;

.field private mFullResync:Z

.field private mPartialResyncFlags:I

.field private mResetDerivedAudioData:Z

.field private mResetMediaStoreImport:Z

.field private mResetRemoteContent:Z

.field final synthetic this$0:Lcom/google/android/music/store/Store;


# direct methods
.method constructor <init>(Lcom/google/android/music/store/Store;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 5942
    iput-object p1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    .line 5943
    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "music.db"

    const/16 v4, 0x68

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/icing/SearchCorpora;->getInstance(Landroid/content/Context;)Lcom/google/android/music/icing/SearchCorpora;

    move-result-object v0

    iget-object v5, v0, Lcom/google/android/music/icing/SearchCorpora;->tableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-object v0, p0

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V

    .line 5907
    iput-boolean v7, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetMediaStoreImport:Z

    .line 5912
    iput-boolean v7, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetRemoteContent:Z

    .line 5918
    iput-boolean v7, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetDerivedAudioData:Z

    .line 5922
    iput-boolean v7, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mFullResync:Z

    .line 5938
    iput v7, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mPartialResyncFlags:I

    .line 5940
    iput-object v3, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mDBPath:Ljava/lang/String;

    .line 5945
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/store/Store$DatabaseHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/Store$DatabaseHelper;

    .prologue
    .line 5905
    iget-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mDBPath:Ljava/lang/String;

    return-object v0
.end method

.method private forcePartialSync(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "partialResyncFlags"    # I

    .prologue
    .line 7899
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    .line 7900
    .local v3, "prefObject":Ljava/lang/Object;
    iget-object v5, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 7902
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 7903
    if-nez v0, :cond_1

    .line 7938
    :cond_0
    :goto_0
    return-void

    .line 7907
    :cond_1
    and-int/lit8 v5, p2, 0x20

    if-eqz v5, :cond_2

    .line 7908
    iget-object v5, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->resetLastRunTime(Landroid/content/Context;)V

    .line 7913
    :cond_2
    :try_start_0
    invoke-static {p1, v0}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->get(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v4

    .line 7914
    .local v4, "syncState":Lcom/google/android/music/sync/google/ClientSyncState;
    if-eqz v4, :cond_0

    .line 7915
    invoke-static {v4}, Lcom/google/android/music/sync/google/ClientSyncState;->newBuilder(Lcom/google/android/music/sync/google/ClientSyncState;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v1

    .line 7917
    .local v1, "builder":Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    and-int/lit8 v5, p2, 0x1

    if-eqz v5, :cond_3

    .line 7918
    const-wide/16 v6, 0x0

    invoke-virtual {v1, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteTrackVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    .line 7920
    :cond_3
    and-int/lit8 v5, p2, 0x2

    if-eqz v5, :cond_4

    .line 7921
    const-wide/16 v6, 0x0

    invoke-virtual {v1, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlaylistVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    .line 7923
    :cond_4
    and-int/lit8 v5, p2, 0x4

    if-eqz v5, :cond_5

    .line 7924
    const-wide/16 v6, 0x0

    invoke-virtual {v1, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlentryVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    .line 7926
    :cond_5
    and-int/lit8 v5, p2, 0x8

    if-eqz v5, :cond_6

    .line 7927
    const-wide/16 v6, 0x0

    invoke-virtual {v1, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteRadioStationVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    .line 7929
    :cond_6
    and-int/lit8 v5, p2, 0x10

    if-eqz v5, :cond_7

    .line 7930
    const-wide/16 v6, 0x0

    invoke-virtual {v1, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteBlacklistItemVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    .line 7932
    :cond_7
    invoke-virtual {v1}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->build()Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v4

    .line 7933
    invoke-static {p1, v0, v4}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->set(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Lcom/google/android/music/sync/google/ClientSyncState;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 7935
    .end local v1    # "builder":Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .end local v4    # "syncState":Lcom/google/android/music/sync/google/ClientSyncState;
    :catch_0
    move-exception v2

    .line 7936
    .local v2, "e":Lcom/google/android/music/sync/common/ProviderException;
    const-string v5, "MusicStore"

    const-string v6, "Unable to access sync state."

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private forceSyncRadioStation(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7877
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 7878
    .local v2, "prefObject":Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 7880
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 7881
    if-nez v0, :cond_1

    .line 7896
    :cond_0
    :goto_0
    return-void

    .line 7884
    :cond_1
    const/4 v3, 0x0

    .line 7886
    .local v3, "syncState":Lcom/google/android/music/sync/google/ClientSyncState;
    :try_start_0
    invoke-static {p1, v0}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->get(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v3

    .line 7887
    if-eqz v3, :cond_0

    .line 7888
    invoke-static {v3}, Lcom/google/android/music/sync/google/ClientSyncState;->newBuilder(Lcom/google/android/music/sync/google/ClientSyncState;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteRadioStationVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->build()Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v3

    .line 7891
    invoke-static {p1, v0, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->set(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Lcom/google/android/music/sync/google/ClientSyncState;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 7893
    :catch_0
    move-exception v1

    .line 7894
    .local v1, "e":Lcom/google/android/music/sync/common/ProviderException;
    const-string v4, "MusicStore"

    const-string v5, "Unable to access sync state."

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private postImportProcessing(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 6539
    iget-boolean v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetMediaStoreImport:Z

    if-eqz v1, :cond_0

    .line 6540
    # invokes: Lcom/google/android/music/store/Store;->deleteLocalMusicAndPlaylists(Landroid/database/sqlite/SQLiteDatabase;)V
    invoke-static {p1}, Lcom/google/android/music/store/Store;->access$400(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6545
    iget-object v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mMediaStoreImporter:Lcom/google/android/music/store/MediaStoreImporter;
    invoke-static {v1}, Lcom/google/android/music/store/Store;->access$300(Lcom/google/android/music/store/Store;)Lcom/google/android/music/store/MediaStoreImporter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/MediaStoreImporter;->invalidateMediaStoreImport(Landroid/content/Context;)V

    .line 6548
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetRemoteContent:Z

    if-eqz v1, :cond_1

    .line 6549
    iget-object v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/google/android/music/store/Store;->deleteRemoteMusicAndPlaylists(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z
    invoke-static {v1, p1}, Lcom/google/android/music/store/Store;->access$500(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6553
    const-string v1, "com.google.android.music.MusicContent"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v3, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 6558
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mFullResync:Z

    if-eqz v1, :cond_4

    .line 6559
    const-string v1, "_sync_state"

    invoke-virtual {p1, v1, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 6560
    const-string v1, "com.google.android.music.MusicContent"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v3, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 6568
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetDerivedAudioData:Z

    if-eqz v1, :cond_3

    .line 6569
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeDerivedMusicData(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6571
    :cond_3
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 6572
    .local v0, "prefObject":Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    const/16 v2, 0x68

    invoke-virtual {v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setDatabaseVersion(I)V

    .line 6574
    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 6575
    return-void

    .line 6562
    .end local v0    # "prefObject":Ljava/lang/Object;
    :cond_4
    iget v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mPartialResyncFlags:I

    if-eqz v1, :cond_2

    .line 6563
    iget v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mPartialResyncFlags:I

    invoke-direct {p0, p1, v1}, Lcom/google/android/music/store/Store$DatabaseHelper;->forcePartialSync(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 6564
    const-string v1, "com.google.android.music.MusicContent"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v3, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private resetTrackSyncState(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 19
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6584
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "data"

    aput-object v3, v4, v2

    .line 6589
    .local v4, "syncStateResetProjection":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 6591
    .local v16, "syncStateProjectionIdIndex":I
    const/4 v15, 0x1

    .line 6595
    .local v15, "syncStateProjectionDataIndex":I
    const-string v3, "_sync_state"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 6602
    .local v11, "c":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 6603
    .local v14, "originalSyncState":Lcom/google/android/music/sync/google/ClientSyncState;
    const/16 v17, 0x0

    .line 6605
    .local v17, "updatedSyncState":Lcom/google/android/music/sync/google/ClientSyncState;
    if-eqz v11, :cond_0

    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 6635
    :cond_0
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 6637
    :goto_0
    return-void

    .line 6608
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 6609
    .local v13, "id":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    .line 6610
    .local v10, "blob":[B
    invoke-static {v10}, Lcom/google/android/music/sync/google/ClientSyncState;->parseFrom([B)Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v14

    .line 6611
    if-eqz v14, :cond_2

    .line 6614
    invoke-static {v14}, Lcom/google/android/music/sync/google/ClientSyncState;->newBuilder(Lcom/google/android/music/sync/google/ClientSyncState;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v2

    const-wide/16 v6, 0x1

    invoke-virtual {v2, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteTrackVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setEtagTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->build()Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v17

    .line 6620
    new-instance v18, Landroid/content/ContentValues;

    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 6621
    .local v18, "values":Landroid/content/ContentValues;
    const-string v2, "data"

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/sync/google/ClientSyncState;->toBytes()[B

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 6622
    const-string v2, "_sync_state"

    const-string v3, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v13, v5, v6

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v2, v1, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 6626
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t update record in _sync_state table for ID: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6630
    .end local v10    # "blob":[B
    .end local v13    # "id":Ljava/lang/String;
    .end local v18    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v12

    .line 6631
    .local v12, "e":Lcom/google/android/music/sync/common/ProviderException;
    :try_start_2
    const-string v2, "MusicStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t update client sync state. original: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " updated: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 6635
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v12    # "e":Lcom/google/android/music/sync/common/ProviderException;
    .restart local v10    # "blob":[B
    .restart local v13    # "id":Ljava/lang/String;
    :cond_2
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v10    # "blob":[B
    .end local v13    # "id":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method private updatefullPathToRelativePathAndStorageType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/io/File;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "fullPathColumnName"    # Ljava/lang/String;
    .param p4, "storageTypeColumnName"    # Ljava/lang/String;
    .param p5, "internalStorageTypeValue"    # I
    .param p6, "externalStorageTypeValue"    # I
    .param p7, "internalCache"    # Ljava/io/File;

    .prologue
    .line 6923
    const-string v1, "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ.-~`@#$%^&*()_+=;,<>? "

    invoke-static {v1}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6930
    .local v0, "filenameChars":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UPDATE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SET "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "substr("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "length("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rtrim("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") + 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CASE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIKE \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x21

    invoke-static {v2, v3}, Lcom/google/android/music/utils/DbUtils;->escapeForLikeOperator(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\' ESCAPE \'!\' "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " THEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ELSE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " END"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT NULL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6954
    return-void
.end method

.method private upgradeDerivedMusicData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 6641
    const/4 v10, 0x0

    .line 6642
    .local v10, "update":Landroid/database/sqlite/SQLiteStatement;
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/MusicFile;->FULL_PROJECTION:[Ljava/lang/String;

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 6645
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 6646
    :try_start_0
    new-instance v9, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v9}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 6647
    .local v9, "file":Lcom/google/android/music/store/MusicFile;
    invoke-static {p1}, Lcom/google/android/music/store/MusicFile;->compileFullUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v10

    .line 6648
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6649
    invoke-virtual {v9, v8}, Lcom/google/android/music/store/MusicFile;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V

    .line 6650
    invoke-virtual {v9}, Lcom/google/android/music/store/MusicFile;->resetDerivedFields()V

    .line 6651
    invoke-virtual {v9, v10, p1}, Lcom/google/android/music/store/MusicFile;->updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 6655
    .end local v9    # "file":Lcom/google/android/music/store/MusicFile;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 6656
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v0

    .line 6655
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 6656
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 6658
    return-void
.end method

.method private upgradeFrom100(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8206
    const-string v0, "DROP TABLE CLOUD_QUEUE_ITEMS"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8208
    const-string v0, "CREATE TABLE CLOUD_QUEUE_ITEMS(Id INTEGER PRIMARY KEY AUTOINCREMENT, MusicId INTEGER NOT NULL, QueueContainerId INTEGER NOT NULL, State INTEGER NOT NULL, ItemOrder INTEGER NOT NULL, ItemUnshuffledOrder INTEGER NOT NULL, CloudQueueVersion INTEGER NOT NULL, CloudQueueId TEXT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8219
    const-string v0, "CREATE INDEX CLOUD_QUEUE_ITEMS_STATE_INDEX ON CLOUD_QUEUE_ITEMS(State)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8222
    const-string v0, "CREATE INDEX CLOUD_QUEUE_ITEMS_ORDER_INDEX ON CLOUD_QUEUE_ITEMS(ItemOrder)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8225
    const-string v0, "CREATE INDEX CLOUD_QUEUE_ITEMS_UNSHUFFLED_ORDER_INDEX ON CLOUD_QUEUE_ITEMS(ItemUnshuffledOrder)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8229
    const-string v0, "CREATE INDEX CLOUD_QUEUE_ITEMS_VERSION_INDEX ON CLOUD_QUEUE_ITEMS(CloudQueueVersion)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8233
    const-string v0, "ALTER TABLE QUEUE_ITEMS ADD COLUMN CloudQueueVersion INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8236
    const-string v0, "ALTER TABLE QUEUE_ITEMS ADD COLUMN CloudQueueId TEXT NOT NULL DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8239
    const-string v0, "CREATE INDEX QUEUE_ITEMS_VERSION_INDEX ON QUEUE_ITEMS(CloudQueueVersion)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8241
    return-void
.end method

.method private upgradeFrom101(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8244
    const-string v0, "ALTER TABLE QUEUE_CONTAINERS ADD COLUMN CloudQueueId TEXT NOT NULL DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8246
    const-string v0, "ALTER TABLE QUEUE_CONTAINERS ADD COLUMN CloudQueueVersion INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8248
    const-string v0, "QUEUE_CONTAINERS"

    # invokes: Lcom/google/android/music/store/Store;->backfillCloudQueueIdsIntoContainerTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    invoke-static {p1, v0}, Lcom/google/android/music/store/Store;->access$700(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 8250
    const-string v0, "ALTER TABLE CLOUD_QUEUE_CONTAINERS ADD COLUMN CloudQueueId TEXT NOT NULL DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8252
    const-string v0, "ALTER TABLE CLOUD_QUEUE_CONTAINERS ADD COLUMN CloudQueueVersion INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8254
    const-string v0, "CLOUD_QUEUE_CONTAINERS"

    # invokes: Lcom/google/android/music/store/Store;->backfillCloudQueueIdsIntoContainerTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    invoke-static {p1, v0}, Lcom/google/android/music/store/Store;->access$700(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 8255
    return-void
.end method

.method private upgradeFrom102(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8258
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8259
    const/16 v0, 0x25

    iput v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mPartialResyncFlags:I

    .line 8262
    :cond_0
    return-void
.end method

.method private upgradeFrom103(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8265
    const-string v0, "ALTER TABLE CLOUD_QUEUE ADD COLUMN RepeatMode INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8267
    const-string v0, "ALTER TABLE CLOUD_QUEUE ADD COLUMN ShuffleMode INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8269
    return-void
.end method

.method private upgradeFrom25(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 6661
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN LocalCopySize INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6665
    const-string v1, "MUSIC"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "Id"

    aput-object v0, v2, v6

    const-string v0, "LocalCopyPath"

    aput-object v0, v2, v7

    const-string v3, "LocalCopyType in (?,?) "

    new-array v4, v4, [Ljava/lang/String;

    const/16 v0, 0xc8

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 6672
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    .line 6673
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6674
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 6675
    .local v10, "filename":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6676
    .local v9, "f":Ljava/io/File;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 6677
    .local v11, "values":Landroid/content/ContentValues;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6678
    const-string v0, "LocalCopySize"

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 6679
    const-string v0, "LocalCopyType"

    invoke-virtual {v11, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 6685
    :goto_1
    const-string v0, "MUSIC"

    const-string v1, "Id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, v11, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 6689
    .end local v9    # "f":Ljava/io/File;
    .end local v10    # "filename":Ljava/lang/String;
    .end local v11    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 6681
    .restart local v9    # "f":Ljava/io/File;
    .restart local v10    # "filename":Ljava/lang/String;
    .restart local v11    # "values":Landroid/content/ContentValues;
    :cond_0
    :try_start_1
    const-string v0, "LocalCopyType"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6683
    const-string v0, "LocalCopySize"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 6689
    .end local v9    # "f":Ljava/io/File;
    .end local v10    # "filename":Ljava/lang/String;
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 6691
    return-void
.end method

.method private upgradeFrom26(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6696
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetRemoteContent:Z

    .line 6698
    const-string v0, "DROP INDEX LISTITEMS_ORDER_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6700
    const-string v0, "ALTER TABLE LISTITEMS ADD COLUMN ServerOrder TEXT DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6704
    const-string v0, "CREATE INDEX LISTITEMS_ORDER_INDEX ON LISTITEMS (ListId, ServerOrder, ClientPosition);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6709
    return-void
.end method

.method private upgradeFrom27(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6712
    const-string v0, "ALTER TABLE LISTITEMS ADD COLUMN ClientId TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6714
    return-void
.end method

.method private upgradeFrom28(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6717
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetDerivedAudioData:Z

    .line 6720
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN TrackType INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6723
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN LocalCopyBitrate INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6725
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN ArtistOrigin INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6728
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN ArtistId INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6730
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN CanonicalArtist TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6733
    const-string v0, "create index MUSIC_ARTISTID_INDEX on MUSIC(ArtistId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6737
    const-string v0, "ALTER TABLE LISTS ADD COLUMN ListType INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6740
    const-string v0, "ALTER TABLE LISTS ADD COLUMN ListArtworkLocation TEXT "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6742
    return-void
.end method

.method private upgradeFrom29(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6748
    const-string v0, "DROP INDEX MUSIC_CANONICAL_NAME_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6758
    const-string v0, "DROP INDEX MUSIC_CANONICAL_ALBUM_CANONICAL_NAME_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6769
    const-string v0, "DROP INDEX MUSIC_CANONICAL_ARTIST_CANONICAL_NAME_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6779
    const-string v0, "create index LIST_SYNC_INDEX on LISTS(SourceAccount,SourceId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6782
    const-string v0, "create index LISTITEMS_SYNC_INDEX on LISTS(SourceAccount,SourceId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6785
    return-void
.end method

.method private upgradeFrom30(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6791
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetMediaStoreImport:Z

    .line 6794
    const-string v0, "DROP INDEX LISTITEMS_SYNC_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6795
    const-string v0, "create index LISTITEMS_SYNC_INDEX on LISTITEMS(SourceAccount,SourceId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6798
    return-void
.end method

.method private upgradeFrom31(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6802
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN Rating INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6805
    return-void
.end method

.method private upgradeFrom32(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6808
    const-string v0, "CREATE TABLE RINGTONES(Id INTEGER PRIMARY KEY AUTOINCREMENT, MusicId INTEGER UNIQUE ON CONFLICT REPLACE, RequestDate INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6813
    return-void
.end method

.method private upgradeFrom33(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6819
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->resetTrackSyncState(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6820
    return-void
.end method

.method private upgradeFrom34(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6826
    const-string v0, "CREATE INDEX MUSIC_RATING ON MUSIC (Rating)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6830
    const-string v0, "CREATE INDEX MUSIC_FILE_DATE ON MUSIC (FileDate)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6834
    const-string v0, "CREATE INDEX MUSIC_TRACK_TYPE ON MUSIC (TrackType)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6836
    return-void
.end method

.method private upgradeFrom35(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6840
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN StoreId TEXT "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6842
    return-void
.end method

.method private upgradeFrom36(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6845
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN StoreAlbumId TEXT "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6847
    return-void
.end method

.method private upgradeFrom37(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6851
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentReason INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6863
    const-string v0, "UPDATE RECENT SET RecentReason = CASE (SELECT TrackType FROM MUSIC WHERE AlbumId=RecentAlbumId AND FileDate=ItemDate LIMIT 1) WHEN 0 THEN 3 WHEN 2 THEN 3 WHEN 1 THEN 2 ELSE 1 END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6878
    return-void
.end method

.method private upgradeFrom38(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6882
    const-string v0, "CREATE TABLE SUGGESTED_SEEDS(Id INTEGER PRIMARY KEY AUTOINCREMENT, SeedSourceAccount INTEGER NOT NULL, SeedTrackSourceId TEXT NOT NULL, SeedListId INTEGER, UNIQUE( SeedSourceAccount,SeedTrackSourceId) ON CONFLICT IGNORE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6891
    return-void
.end method

.method private upgradeFrom39(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6958
    iget-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalMusicCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;

    move-result-object v7

    .line 6959
    .local v7, "internalMusicCache":Ljava/io/File;
    if-nez v7, :cond_0

    .line 6960
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to find the internal cache location"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6964
    :cond_0
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN LocalCopyStorageType INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6968
    const-string v2, "MUSIC"

    const-string v3, "LocalCopyPath"

    const-string v4, "LocalCopyStorageType"

    const/4 v5, 0x1

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/store/Store$DatabaseHelper;->updatefullPathToRelativePathAndStorageType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/io/File;)V

    .line 6973
    return-void
.end method

.method private upgradeFrom40(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6977
    const-string v0, "CREATE TABLE PLAYQ_GROUPS(Id INTEGER PRIMARY KEY AUTOINCREMENT, Time INTEGER NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6981
    return-void
.end method

.method private upgradeFrom41(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 6985
    iget-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalArtworkCacheDirectory_Old(Landroid/content/Context;)Ljava/io/File;

    move-result-object v7

    .line 6986
    .local v7, "internalArtworkCache":Ljava/io/File;
    if-nez v7, :cond_0

    .line 6987
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to find the internal cache location"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6991
    :cond_0
    const-string v0, "ALTER TABLE ARTWORK ADD COLUMN LocalLocationStorageType INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6995
    const-string v2, "ARTWORK"

    const-string v3, "LocalLocation"

    const-string v4, "LocalLocationStorageType"

    const/4 v5, 0x1

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/store/Store$DatabaseHelper;->updatefullPathToRelativePathAndStorageType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/io/File;)V

    .line 7000
    return-void
.end method

.method private upgradeFrom42(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7003
    const-string v0, "ALTER TABLE RECENT ADD COLUMN Priority INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7005
    return-void
.end method

.method private upgradeFrom43(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7008
    const-string v0, "CREATE TABLE MUSIC_TOMBSTONES(Id INTEGER PRIMARY KEY AUTOINCREMENT, SourceAccount INTEGER, SourceId TEXT, _sync_version TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7014
    return-void
.end method

.method private upgradeFrom44(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7017
    const-string v0, "ALTER TABLE SUGGESTED_SEEDS ADD COLUMN SeedOrder INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7019
    return-void
.end method

.method private upgradeFrom45(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7022
    const-string v0, "ALTER TABLE KEEPON ADD COLUMN AutoListId INTEGER "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7024
    return-void
.end method

.method private upgradeFrom46(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7031
    const-string v0, "KEEPON"

    const-string v1, "ArtistId IS NOT NULL"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7032
    return-void
.end method

.method private upgradeFrom47(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7035
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN Domain INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7038
    return-void
.end method

.method private upgradeFrom48(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7041
    const-string v0, "ALTER TABLE KEEPON ADD COLUMN SongCount INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7044
    const-string v0, "ALTER TABLE KEEPON ADD COLUMN DownloadedSongCount INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7047
    iget-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1}, Lcom/google/android/music/store/Store;->updateKeeponDownloadSongCounts_Old(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 7048
    return-void
.end method

.method private upgradeFrom49(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "startVersion"    # I

    .prologue
    .line 7073
    const/16 v4, 0x31

    if-ge p2, v4, :cond_1

    .line 7137
    :cond_0
    :goto_0
    return-void

    .line 7080
    :cond_1
    :try_start_0
    const-string v4, "SELECT SONG_COUNT, DOWNLOADED_SONG_COUNT FROM KEEPON LIMIT 1"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 7082
    .local v1, "c":Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7083
    const/4 v0, 0x1

    .line 7090
    .end local v1    # "c":Landroid/database/Cursor;
    .local v0, "badColumnNames":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 7094
    const-string v4, "MusicStore"

    const-string v5, "Renaming KeepOn columns"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 7097
    const-string v3, "temp_49Upgrade_KEEPON"

    .line 7098
    .local v3, "tempName":Ljava/lang/String;
    const-string v4, "ALTER TABLE KEEPON RENAME TO temp_49Upgrade_KEEPON"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7101
    const-string v4, "CREATE TABLE KEEPON(KeepOnId INTEGER PRIMARY KEY AUTOINCREMENT, ListId INTEGER UNIQUE, AlbumId INTEGER UNIQUE, ArtistId INTEGER UNIQUE, DateAdded INTEGER ,AutoListId INTEGER ,SongCount INTEGER NOT NULL DEFAULT 0,DownloadedSongCount INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7113
    const-string v4, "INSERT INTO KEEPON(KeepOnId, ListId, AlbumId, ArtistId, DateAdded, AutoListId, SongCount, DownloadedSongCount) SELECT KeepOnId, ListId, AlbumId, ArtistId, DateAdded, AutoListId, SONG_COUNT, DOWNLOADED_SONG_COUNT FROM temp_49Upgrade_KEEPON"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7136
    const-string v4, "DROP TABLE temp_49Upgrade_KEEPON"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    .line 7084
    .end local v0    # "badColumnNames":Z
    .end local v3    # "tempName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 7086
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "MusicStore"

    const-string v5, "Old column names not found"

    invoke-static {v4, v5, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 7087
    const/4 v0, 0x0

    .restart local v0    # "badColumnNames":Z
    goto :goto_1
.end method

.method private upgradeFrom50(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7166
    invoke-static {p1}, Lcom/google/android/music/store/OldMusicFile50;->fixUnknownAlbumsAndArtists50(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 7169
    const-string v0, "431126106"

    .line 7170
    .local v0, "unknownAlbumHash":Ljava/lang/String;
    const-string v1, "SHOULDKEEPON"

    const-string v2, "KeepOnId IN (SELECT KeepOnId FROM KEEPON WHERE AlbumId=? OR AlbumId=? )"

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v5

    const-string v4, "431126106"

    aput-object v4, v3, v6

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7178
    const-string v1, "KEEPON"

    const-string v2, "AlbumId=? OR AlbumId=? "

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v5

    const-string v4, "431126106"

    aput-object v4, v3, v6

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7183
    const-string v1, "SHOULDKEEPON"

    const-string v2, "KeepOnId NOT IN (SELECT KeepOnId FROM KEEPON)"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7187
    return-void
.end method

.method private upgradeFrom51(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7191
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN CacheDate INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7195
    const-string v0, "UPDATE MUSIC SET CacheDate=LastPlayDate WHERE LastPlayDate!=0 AND LocalCopyType IN (100,200)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7203
    return-void
.end method

.method private upgradeFrom52(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7206
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN ArtistArtLocation TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7208
    return-void
.end method

.method private upgradeFrom53(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7212
    const-string v2, "temp_52Upgrade_LISTITEMS"

    .line 7213
    .local v2, "tempName":Ljava/lang/String;
    const-string v3, "CREATE TABLE temp_52Upgrade_LISTITEMS(Id INTEGER PRIMARY KEY AUTOINCREMENT, ClientId TEXT, MusicId INTEGER NOT NULL REFERENCES MUSIC, ListId INTEGER NOT NULL REFERENCES LISTS, ClientPosition INTEGER NOT NULL, ServerOrder TEXT DEFAULT \'\', PlayGroupId INTEGER NOT NULL DEFAULT 0, SourceAccount INTEGER, SourceId TEXT, _sync_version TEXT, _sync_dirty INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7231
    const-string v0, "LISTITEMS.MusicSourceAccount=MUSIC.SourceAccount AND LISTITEMS.MusicSourceId=MUSIC.SourceId"

    .line 7236
    .local v0, "OLD_LISTITEMS_MUSIC_CONDITION":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " JOIN MUSIC ON ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7240
    .local v1, "OLD_LISTITEMS_MUSIC_JOIN":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSERT INTO temp_52Upgrade_LISTITEMS(Id, ClientId, MusicId,ListId, ClientPosition, ServerOrder, PlayGroupId, SourceAccount, SourceId, _sync_version, _sync_dirty) SELECT LISTITEMS.Id, LISTITEMS.ClientId, MUSIC.Id, LISTITEMS.ListId, LISTITEMS.ClientPosition, LISTITEMS.ServerOrder, LISTITEMS.ServerPosition,LISTITEMS.SourceAccount,LISTITEMS.SourceId,LISTITEMS._sync_version,LISTITEMS._sync_dirty FROM LISTITEMS"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7269
    const-string v3, "DROP TABLE LISTITEMS"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7272
    const-string v3, "ALTER TABLE temp_52Upgrade_LISTITEMS RENAME TO LISTITEMS"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7275
    const-string v3, "CREATE INDEX LISTITEMS_ORDER_INDEX ON LISTITEMS (ListId, ServerOrder, ClientPosition);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7282
    const-string v3, "CREATE INDEX LISTITEMS_SYNC_INDEX on LISTITEMS(SourceAccount,SourceId)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7287
    const-string v3, "ALTER TABLE MUSIC ADD COLUMN SourceType INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7292
    const-string v3, "UPDATE MUSIC SET SourceType=1 WHERE MUSIC.SourceAccount=0"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7300
    const-string v3, "UPDATE MUSIC SET SourceType=2 WHERE MUSIC.SourceAccount!=0 AND TrackType!=4 AND TrackType!=5"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7312
    const-string v3, "ALTER TABLE MUSIC ADD COLUMN Nid TEXT"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7316
    const-string v3, "ALTER TABLE MUSIC ADD COLUMN ClientId TEXT"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7320
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE MUSIC SET SourceType=3, Nid=SourceId, ClientId=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/google/android/music/store/Store;->generateClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " WHERE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SourceAccount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "!="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "TrackType"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " or "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "TrackType"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7334
    return-void
.end method

.method private upgradeFrom54(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7337
    const-string v0, "CREATE TABLE RADIO_STATIONS(Id INTEGER PRIMARY KEY AUTOINCREMENT, ClientId TEXT NOT NULL, Name TEXT NOT NULL, Description TEXT, RecentTimestamp INTEGER NOT NULL DEFAULT 0, ArtworkLocation TEXT, SeedSourceId TEXT NOT NULL, SeedSourceType INTEGER NOT NULL DEFAULT 0, SourceAccount INTEGER NOT NULL, SourceId TEXT , _sync_version TEXT, _sync_dirty INTEGER NOT NULL DEFAULT 0 );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7352
    const-string v0, "CREATE TABLE RADIO_STATION_TOMBSTONES(Id INTEGER PRIMARY KEY AUTOINCREMENT, SourceAccount INTEGER, SourceId TEXT, _sync_version TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7358
    return-void
.end method

.method private upgradeFrom55(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7361
    const-string v0, "ALTER TABLE RADIO_STATIONS ADD COLUMN ArtworkType INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7363
    return-void
.end method

.method private upgradeFrom56(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7366
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentRadioId INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7374
    const-string v0, "CREATE UNIQUE INDEX RECENT_RADIO_ID_IDX ON RECENT (RecentRadioId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7376
    return-void
.end method

.method private upgradeFrom57(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7379
    const-string v0, "DROP TABLE RADIO_STATIONS"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7380
    const-string v0, "CREATE TABLE RADIO_STATIONS(Id INTEGER PRIMARY KEY AUTOINCREMENT, ClientId TEXT NOT NULL, Name TEXT NOT NULL, Description TEXT, RecentTimestamp INTEGER NOT NULL DEFAULT 0, ArtworkLocation TEXT, ArtworkType INTEGER NOT NULL DEFAULT 0, SeedSourceId TEXT NOT NULL, SeedSourceType INTEGER NOT NULL DEFAULT 0, SourceAccount INTEGER NOT NULL, SourceId TEXT , _sync_version TEXT, _sync_dirty INTEGER NOT NULL DEFAULT 0, UNIQUE( SeedSourceId,SeedSourceType) ON CONFLICT IGNORE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7398
    return-void
.end method

.method private upgradeFrom58(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7401
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN CpData BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7403
    return-void
.end method

.method private upgradeFrom59(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7407
    const-string v0, "CREATE TABLE CONFIG(id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT NOT NULL, Value TEXT, UNIQUE(Name) ON CONFLICT REPLACE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7414
    return-void
.end method

.method private upgradeFrom60(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7419
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentNautilusAlbum TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7421
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentNautilusAlbumId TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7423
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentNautilusAlbumArt TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7425
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentNautilusAlbumArtist TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7429
    const-string v0, "CREATE UNIQUE INDEX RECENT_NAUTILUS_ALBUM_ID_IDX ON RECENT (RecentNautilusAlbumId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7432
    return-void
.end method

.method private upgradeFrom61(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7435
    const-string v0, "DROP TABLE CONFIG"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7436
    return-void
.end method

.method private upgradeFrom62(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7439
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN ArtistMetajamId TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7441
    return-void
.end method

.method private upgradeFrom63(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7444
    const-string v0, "ALTER TABLE LISTS ADD COLUMN ShareToken TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7447
    const-string v0, "ALTER TABLE LISTS ADD COLUMN OwnerName TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7450
    const-string v0, "ALTER TABLE LISTS ADD COLUMN Description TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7452
    return-void
.end method

.method private upgradeFrom64(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7455
    const-string v0, "DROP TABLE IF EXISTS ARTWORK_CACHE"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7458
    const-string v0, "CREATE TABLE ARTWORK_CACHE(RemoteLocation TEXT PRIMARY KEY,Timestamp INTEGER, FileSize INTEGER, LocalLocation STRING,LocalLocationStorageType INTEGER NOT NULL DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7467
    const-string v0, "create index ARTWORK_CACHE_TIMESTAMP_INDEX on ARTWORK_CACHE(Timestamp)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7469
    return-void
.end method

.method private upgradeFrom65(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7472
    const-string v0, "ALTER TABLE LISTS ADD COLUMN OwnerProfilePhotoUrl TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7474
    return-void
.end method

.method private upgradeFrom66()V
    .locals 1

    .prologue
    .line 7477
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mFullResync:Z

    .line 7478
    return-void
.end method

.method private upgradeFrom67(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7488
    return-void
.end method

.method private upgradeFrom68(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7493
    const-string v0, "DROP INDEX IF EXISTS MUSIC_TRACK_TYPE_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7496
    const-string v0, "CREATE INDEX MUSIC_ALBUM_METAJAM_ID_INDEX on MUSIC(StoreAlbumId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7498
    return-void
.end method

.method private upgradeFrom69(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 7511
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 7512
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "TrackType"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7513
    const-string v1, "_sync_dirty"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7514
    const-string v1, "MUSIC"

    const-string v2, "Domain=0 AND TrackType=4"

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7531
    const-string v1, "RECENT"

    const-string v2, "RecentAlbumId NOT NULL AND NOT EXISTS(SELECT MUSIC.AlbumId FROM MUSIC WHERE MUSIC.AlbumId=RecentAlbumId AND Domain=0)"

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7537
    return-void
.end method

.method private upgradeFrom70(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7546
    const-string v0, "create index MUSIC_LOCAL_COPY_TYPE_INDEX on MUSIC(LocalCopyType)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7550
    const-string v0, "CREATE INDEX LISTITEMS_MUSICID_INDEX on LISTITEMS(MusicId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7552
    return-void
.end method

.method private upgradeFrom71(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7557
    const-string v0, "DROP INDEX IF EXISTS MUSIC_TRACKS_BY_ARTIST_SORT_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7560
    const-string v0, "DROP INDEX IF EXISTS MUSIC_TRACKS_BY_ALBUM_SORT_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7563
    const-string v0, "DROP INDEX IF EXISTS MUSIC_TRACKS_BY_NAME_SORT_INDEX"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7567
    const-string v0, "CREATE INDEX MUSIC_DOMAIN_CANONICALNAME_SONGID_INDEX ON MUSIC(Domain, CanonicalName, SongId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7572
    const-string v0, "CREATE INDEX MUSIC_DOMAIN_CANONICAL_ALBUM_ARTIST_INDEX ON MUSIC(Domain, CanonicalAlbumArtist)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7577
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN AlbumIdSourceText TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7579
    invoke-static {p1}, Lcom/google/android/music/store/Store;->updateAlbumIdSourceText(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 7582
    const-string v0, "CREATE INDEX MUSIC_DOMAIN_ALBUMID_SOURCE_TEXT_INDEX ON MUSIC(Domain, AlbumIdSourceText, FileDate)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7586
    return-void
.end method

.method private upgradeFrom72(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7589
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN LocalCopyStorageVolumeId TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7591
    return-void
.end method

.method private upgradeFrom73(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "startVersion"    # I

    .prologue
    .line 7594
    const-string v2, "CREATE TABLE DB_PARAMS(id INTEGER PRIMARY KEY AUTOINCREMENT, DatabaseId TEXT)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7597
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 7598
    .local v0, "uuid":Ljava/util/UUID;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 7599
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "DatabaseId"

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7600
    const-string v2, "DB_PARAMS"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 7602
    iget-object v2, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/google/android/music/store/Store;->migrateStorageVolumeIds(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 7603
    return-void
.end method

.method private upgradeFrom74(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7606
    const-string v0, "CREATE TABLE MUSIC_RESTORE(id INTEGER PRIMARY KEY AUTOINCREMENT, MusicId INTEGER UNIQUE NOT NULL, LocalCopyPath TEXT NOT NULL, LocalCopyStorageVolumeId TEXT NOT NULL, LocalCopyType INTEGER, Timestamp INTEGER NOT NULL, UNIQUE(LocalCopyStorageVolumeId,LocalCopyPath))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7616
    return-void
.end method

.method private upgradeFrom75(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 16
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7619
    const-string v1, "ALTER TABLE LISTS ADD COLUMN NameSort TEXT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7623
    const-string v2, "LISTS"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "Id"

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const-string v4, "Name"

    aput-object v4, v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 7627
    .local v9, "c":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 7653
    :cond_0
    return-void

    .line 7629
    :cond_1
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 7632
    .local v14, "valueList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_2
    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 7633
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 7634
    .local v11, "listId":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 7635
    .local v12, "listName":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 7636
    new-instance v1, Landroid/util/Pair;

    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v11, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 7640
    .end local v11    # "listId":Ljava/lang/String;
    .end local v12    # "listName":Ljava/lang/String;
    :catchall_0
    move-exception v1

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1

    :cond_3
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 7644
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 7645
    .local v15, "values":Landroid/content/ContentValues;
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 7646
    .local v10, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7647
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/util/Pair;

    .line 7648
    .local v13, "namePair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 7649
    const-string v2, "NameSort"

    iget-object v1, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v15, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7650
    const-string v2, "LISTS"

    const-string v3, "Id=?"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v1, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    aput-object v1, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method private upgradeFrom76(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7659
    const-string v0, "CREATE TABLE QUEUE_ITEMS(Id INTEGER PRIMARY KEY AUTOINCREMENT, MusicId INTEGER NOT NULL, QueueContainerId INTEGER NOT NULL, State INTEGER NOT NULL, ItemOrder INTEGER NOT NULL, ItemUnshuffledOrder INTEGER NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7668
    const-string v0, "CREATE INDEX QUEUE_ITEMS_STATE_INDEX ON QUEUE_ITEMS(State)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7671
    const-string v0, "CREATE INDEX QUEUE_ITEMS_ORDER_INDEX ON QUEUE_ITEMS(ItemOrder)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7674
    const-string v0, "CREATE TABLE QUEUE_CONTAINERS(Id INTEGER PRIMARY KEY AUTOINCREMENT, ContainerId INTEGER NOT NULL, Type INTEGER NOT NULL, Name TEXT, ExtId TEXT, ExtData TEXT, IS_SEVERED INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7683
    return-void
.end method

.method private upgradeFrom77(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7686
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentNautilusAlbumArtistId String"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7688
    return-void
.end method

.method private upgradeFrom78(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7692
    const-string v0, "ALTER TABLE KEEPON ADD COLUMN RadioStationId INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7695
    const-string v0, "CREATE UNIQUE INDEX KEEPON_RADIO_ID_IDX ON KEEPON (RadioStationId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7698
    return-void
.end method

.method private upgradeFrom79(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7701
    const-string v0, "CREATE TABLE RADIO_SONGS(Id INTEGER PRIMARY KEY AUTOINCREMENT, MusicId INTEGER NOT NULL, RadioStationId INTEGER NOT NULL, State INTEGER NOT NULL DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7709
    const-string v0, "CREATE INDEX RADIO_SONGS_RADIO_STATION_ID_INDEX ON RADIO_SONGS (RadioStationId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7711
    return-void
.end method

.method private upgradeFrom80(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7716
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 7717
    .local v2, "prefObject":Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 7719
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 7720
    if-nez v0, :cond_1

    .line 7739
    :cond_0
    :goto_0
    return-void

    .line 7723
    :cond_1
    const/4 v3, 0x0

    .line 7725
    .local v3, "syncState":Lcom/google/android/music/sync/google/ClientSyncState;
    :try_start_0
    invoke-static {p1, v0}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->get(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v3

    .line 7726
    if-eqz v3, :cond_0

    .line 7727
    invoke-static {}, Lcom/google/android/music/sync/google/ClientSyncState;->newBuilder()Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/ClientSyncState;->getRemoteTrackVersion()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteTrackVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/ClientSyncState;->getRemotePlaylistVersion()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlaylistVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/ClientSyncState;->getRemotePlentryVersion()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlentryVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteRadioStationVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteAccount(I)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->build()Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v3

    .line 7734
    invoke-static {p1, v0, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->set(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Lcom/google/android/music/sync/google/ClientSyncState;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 7736
    :catch_0
    move-exception v1

    .line 7737
    .local v1, "e":Lcom/google/android/music/sync/common/ProviderException;
    const-string v4, "MusicStore"

    const-string v5, "Unable to access sync state."

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private upgradeFrom81(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7742
    const-string v0, "CREATE INDEX QUEUE_ITEMS_UNSHUFFLED_ORDER_INDEX ON QUEUE_ITEMS(ItemUnshuffledOrder)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7745
    return-void
.end method

.method private upgradeFrom82(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v5, 0x1

    .line 7749
    const-string v0, " \'\u001f\' || "

    .line 7751
    .local v0, "connector":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UPDATE MUSIC SET AlbumIdSourceText=CanonicalAlbum || (CASE WHEN AlbumArtistOrigin=? AND CanonicalAlbum!=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " THEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \'\u001f\' || "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ELSE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \'\u001f\' || "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CanonicalAlbumArtist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " END)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CanonicalAlbum"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "CanonicalAlbumArtist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "AlbumArtistOrigin"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7769
    return-void
.end method

.method private upgradeFrom83(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 17
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7773
    const-string v5, "ListType=?"

    .line 7774
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 7777
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-string v3, "LISTS"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "Id"

    aput-object v7, v4, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 7781
    .local v10, "c":Landroid/database/Cursor;
    if-nez v10, :cond_1

    .line 7842
    :cond_0
    :goto_0
    return-void

    .line 7785
    :cond_1
    const-wide/16 v12, -0x1

    .line 7787
    .local v12, "oldQueueId":J
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    if-nez v2, :cond_2

    .line 7792
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 7790
    :cond_2
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v12

    .line 7792
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 7794
    const-wide/16 v2, -0x1

    cmp-long v2, v12, v2

    if-eqz v2, :cond_0

    .line 7799
    const-string v5, "RecentListId=?"

    .line 7800
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 7801
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    const-string v2, "RECENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7805
    const-string v5, "Type=? AND ContainerId=?"

    .line 7807
    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x1

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 7812
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    const-string v3, "QUEUE_CONTAINERS"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "Id"

    aput-object v7, v4, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 7816
    if-eqz v10, :cond_0

    .line 7820
    new-instance v16, Ljava/util/LinkedList;

    invoke-direct/range {v16 .. v16}, Ljava/util/LinkedList;-><init>()V

    .line 7822
    .local v16, "queueContainerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :goto_1
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 7823
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 7824
    .local v14, "queueContainerId":J
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 7827
    .end local v14    # "queueContainerId":J
    :catchall_0
    move-exception v2

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 7792
    .end local v16    # "queueContainerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :catchall_1
    move-exception v2

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 7827
    .restart local v16    # "queueContainerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_3
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 7829
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 7830
    .restart local v14    # "queueContainerId":J
    const-string v5, "QueueContainerId=?"

    .line 7831
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 7834
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    const-string v2, "QUEUE_ITEMS"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7836
    const-string v5, "Id=?"

    .line 7837
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 7840
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    const-string v2, "QUEUE_CONTAINERS"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2
.end method

.method private upgradeFrom84(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7845
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->forceSyncRadioStation(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 7846
    return-void
.end method

.method private upgradeFrom85(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 7849
    const-string v3, "ListType=?"

    .line 7850
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v2, [Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 7853
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v1, "LISTS"

    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "Id"

    aput-object v0, v2, v6

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 7858
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7859
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 7860
    .local v10, "oldQueueId":J
    const-string v3, "ListId=?"

    .line 7861
    const/4 v0, 0x1

    new-array v9, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7864
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .local v9, "selectionArgs":[Ljava/lang/String;
    :try_start_1
    const-string v0, "LISTITEMS"

    invoke-virtual {p1, v0, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7866
    const-string v3, "Id=?"

    .line 7867
    const-string v0, "LISTS"

    invoke-virtual {p1, v0, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v4, v9

    .line 7870
    .end local v9    # "selectionArgs":[Ljava/lang/String;
    .end local v10    # "oldQueueId":J
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 7872
    return-void

    .line 7870
    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .restart local v9    # "selectionArgs":[Ljava/lang/String;
    .restart local v10    # "oldQueueId":J
    :catchall_1
    move-exception v0

    move-object v4, v9

    .end local v9    # "selectionArgs":[Ljava/lang/String;
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0
.end method

.method private upgradeFrom86(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7941
    const-string v0, "CREATE TABLE MAINSTAGE_BLACKLIST(Id INTEGER PRIMARY KEY AUTOINCREMENT, ReasonType INTEGER, DismissDate INTEGER, AlbumTitle TEXT, AlbumArtist TEXT, AlbumMetajamId TEXT, AlbumLocalId TEXT, ListLocalId INTEGER, ListShareToken TEXT, RadioRemoteId TEXT, RadioSeedId TEXT, RadioSeedType INTEGER, _sync_dirty INTEGER NOT NULL DEFAULT 0, SourceAccount INTEGER NOT NULL, SourceId TEXT, _sync_version TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7960
    return-void
.end method

.method private upgradeFrom87(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7963
    const-string v0, "CREATE TABLE EVENTS (Id INTEGER PRIMARY KEY AUTOINCREMENT, SourceAccount INTEGER NOT NULL, TrackSourceId TEXT NOT NULL, EventType INTEGER NOT NULL, EventContextSourceId TEXT, EventContextType INTEGER NOT NULL, EventTimestampMillisec INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7972
    const-string v0, "CREATE INDEX EVENTS_TRACK_SOURCE_ID_INDEX ON EVENTS(TrackSourceId)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7975
    const-string v0, "CREATE INDEX EVENTS_EVENT_TIMESTAMP_MS_INDEX ON EVENTS(EventTimestampMillisec)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7977
    return-void
.end method

.method private upgradeFrom88(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7980
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN RatingTimestampMicrosec INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 7982
    return-void
.end method

.method private upgradeFrom89(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 7987
    const-string v1, "L1"

    .line 7988
    .local v1, "listAlias1":Ljava/lang/String;
    const-string v2, "L2"

    .line 7990
    .local v2, "listAlias2":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "(SELECT Id FROM LISTS AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE EXISTS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SELECT 1 FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "LISTS"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ShareToken"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ShareToken"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8007
    .local v0, "duplicates":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Id IN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 8008
    .local v3, "whereClause":Ljava/lang/String;
    const-string v4, "LISTS"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8010
    const-string v4, "CREATE UNIQUE INDEX LISTS_SHARE_TOKEN_IDX ON LISTS (ShareToken)"

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8012
    return-void
.end method

.method private upgradeFrom90(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8015
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN Vid TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8017
    return-void
.end method

.method private upgradeFrom91(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8020
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN LocalCopyStreamFidelity INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8022
    return-void
.end method

.method private upgradeFrom92(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8025
    const-string v0, "ALTER TABLE MUSIC_RESTORE ADD COLUMN LocalCopyStreamFidelity INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8027
    return-void
.end method

.method private upgradeFrom93(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8031
    const-string v0, "DROP TABLE IF EXISTS RINGTONES"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8032
    return-void
.end method

.method private upgradeFrom94(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 17
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8035
    const-string v2, "ALTER TABLE KEEPON ADD COLUMN ContainerSizeBytes INTEGER DEFAULT 0"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8037
    const/4 v11, 0x0

    .line 8040
    .local v11, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "SHOULDKEEPON  JOIN MUSIC ON (SHOULDKEEPON.MusicId = MUSIC.Id) "

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "SHOULDKEEPON.KeepOnId"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "sum(MUSIC.Size)"

    aput-object v5, v4, v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "SHOULDKEEPON.KeepOnId"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 8046
    if-eqz v11, :cond_1

    .line 8047
    :cond_0
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 8048
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 8049
    .local v14, "keeponId":J
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 8051
    .local v12, "containerSize":J
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 8052
    .local v16, "values":Landroid/content/ContentValues;
    const-string v2, "ContainerSizeBytes"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 8054
    const-string v2, "KEEPON"

    const-string v3, "KEEPON.KeepOnId=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 8058
    const-string v2, "MusicStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to update the download size for keeponId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8064
    .end local v12    # "containerSize":J
    .end local v14    # "keeponId":J
    .end local v16    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    :cond_1
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 8066
    return-void
.end method

.method private upgradeFrom95(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8069
    const-string v0, "ALTER TABLE MUSIC ADD COLUMN VThumbnailUrl TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8071
    return-void
.end method

.method private upgradeFrom96(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 31
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8074
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "artwork"

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8076
    .local v16, "currentAlbumArtDirectory":Ljava/io/File;
    new-instance v23, Ljava/io/File;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    const-string v6, "artwork2"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v5, "folder"

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8079
    .local v23, "newArtDirectory":Ljava/io/File;
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 8080
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->mkdirs()Z

    .line 8082
    :cond_0
    const-string v5, "ARTWORK"

    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "AlbumId"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "LocalLocation"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "LocalLocationStorageType"

    aput-object v7, v6, v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 8088
    .local v14, "albumArtTableCursor":Landroid/database/Cursor;
    if-eqz v14, :cond_5

    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_5

    .line 8089
    new-instance v30, Landroid/content/ContentValues;

    invoke-direct/range {v30 .. v30}, Landroid/content/ContentValues;-><init>()V

    .line 8090
    .local v30, "values":Landroid/content/ContentValues;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 8091
    .local v26, "now":J
    :cond_1
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 8092
    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 8096
    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 8097
    .local v15, "albumId":Ljava/lang/String;
    const/4 v5, 0x0

    const-string v6, "MUSIC"

    const/4 v4, 0x1

    new-array v7, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v8, "AlbumArtLocation"

    aput-object v8, v7, v4

    const-string v8, "AlbumId=? AND MUSIC.SourceAccount!=0 AND AlbumArtLocation IS NOT NULL"

    const/4 v4, 0x1

    new-array v9, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v15, v9, v4

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, "1"

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v22

    .line 8104
    .local v22, "matchingAlbumCursor":Landroid/database/Cursor;
    if-eqz v22, :cond_2

    :try_start_1
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    if-nez v4, :cond_3

    .line 8131
    :cond_2
    :try_start_2
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 8138
    .end local v15    # "albumId":Ljava/lang/String;
    .end local v22    # "matchingAlbumCursor":Landroid/database/Cursor;
    .end local v26    # "now":J
    .end local v30    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v4

    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4

    .line 8107
    .restart local v15    # "albumId":Ljava/lang/String;
    .restart local v22    # "matchingAlbumCursor":Landroid/database/Cursor;
    .restart local v26    # "now":J
    .restart local v30    # "values":Landroid/content/ContentValues;
    :cond_3
    const/4 v4, 0x0

    :try_start_3
    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 8108
    .local v29, "remoteLocation":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 8109
    .local v20, "localLocation":Ljava/lang/String;
    const/4 v4, 0x2

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 8110
    .local v21, "localLocationStorageType":I
    new-instance v28, Ljava/io/File;

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8111
    .local v28, "oldFile":Ljava/io/File;
    if-eqz v28, :cond_4

    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->length()J

    move-result-wide v18

    .line 8113
    .local v18, "fileLength":J
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v29 .. v29}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 8114
    .local v25, "newLocalLocation":Ljava/lang/String;
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 8116
    .local v24, "newFile":Ljava/io/File;
    :try_start_4
    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/google/common/io/Files;->move(Ljava/io/File;Ljava/io/File;)V

    .line 8117
    const-string v4, "RemoteLocation"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8118
    const-string v4, "FileSize"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 8119
    const-string v4, "Timestamp"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 8120
    const-string v4, "LocalLocation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "artwork2/folder/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8122
    const-string v4, "LocalLocationStorageType"

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8124
    const-string v4, "ARTWORK_CACHE"

    const/4 v5, 0x0

    const/4 v6, 0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v4, v5, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 8131
    :goto_2
    :try_start_5
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 8111
    .end local v18    # "fileLength":J
    .end local v24    # "newFile":Ljava/io/File;
    .end local v25    # "newLocalLocation":Ljava/lang/String;
    :cond_4
    const-wide/16 v18, -0x1

    goto/16 :goto_1

    .line 8126
    .restart local v18    # "fileLength":J
    .restart local v24    # "newFile":Ljava/io/File;
    .restart local v25    # "newLocalLocation":Ljava/lang/String;
    :catch_0
    move-exception v17

    .line 8127
    .local v17, "e":Ljava/io/IOException;
    :try_start_6
    const-string v4, "MusicStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException moving file from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 8131
    .end local v17    # "e":Ljava/io/IOException;
    .end local v18    # "fileLength":J
    .end local v20    # "localLocation":Ljava/lang/String;
    .end local v21    # "localLocationStorageType":I
    .end local v24    # "newFile":Ljava/io/File;
    .end local v25    # "newLocalLocation":Ljava/lang/String;
    .end local v28    # "oldFile":Ljava/io/File;
    .end local v29    # "remoteLocation":Ljava/lang/String;
    :catchall_1
    move-exception v4

    :try_start_7
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4

    .line 8135
    .end local v15    # "albumId":Ljava/lang/String;
    .end local v22    # "matchingAlbumCursor":Landroid/database/Cursor;
    .end local v26    # "now":J
    .end local v30    # "values":Landroid/content/ContentValues;
    :cond_5
    const-string v4, "DROP TABLE IF EXISTS ARTWORK"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8136
    # invokes: Lcom/google/android/music/store/Store;->deleteFilesRecursively(Ljava/io/File;)V
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->access$600(Ljava/io/File;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 8138
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 8140
    return-void
.end method

.method private upgradeFrom97(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8143
    const-string v0, "CREATE TABLE CLOUD_QUEUE(Id INTEGER PRIMARY KEY, Version INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8148
    const-string v0, "CREATE TABLE CLOUD_QUEUE_ITEMS(Id INTEGER PRIMARY KEY AUTOINCREMENT, MusicId INTEGER NOT NULL, QueueContainerId INTEGER NOT NULL, State INTEGER NOT NULL, ItemOrder INTEGER NOT NULL, ItemUnshuffledOrder INTEGER NOT NULL, CloudQueueVersion INTEGER NOT NULL, CloudQueueId TEXT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8159
    const-string v0, "CREATE INDEX CLOUD_QUEUE_ITEMS_STATE_INDEX ON CLOUD_QUEUE_ITEMS(State)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8162
    const-string v0, "CREATE INDEX CLOUD_QUEUE_ITEMS_ORDER_INDEX ON CLOUD_QUEUE_ITEMS(ItemOrder)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8165
    const-string v0, "CREATE INDEX CLOUD_QUEUE_ITEMS_UNSHUFFLED_ORDER_INDEX ON CLOUD_QUEUE_ITEMS(ItemUnshuffledOrder)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8169
    const-string v0, "CREATE TABLE CLOUD_QUEUE_CONTAINERS(Id INTEGER PRIMARY KEY AUTOINCREMENT, ContainerId INTEGER NOT NULL, Type INTEGER NOT NULL, Name TEXT, ExtId TEXT, ExtData TEXT, IS_SEVERED INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8178
    return-void
.end method

.method private upgradeFrom98(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8181
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentReasonText TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8184
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentNautilusAlbumDescription TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8186
    const-string v0, "ALTER TABLE RECENT ADD COLUMN RecentNautilusAlbumArtistProfileImage TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8189
    const-string v0, "ALTER TABLE LISTS ADD COLUMN EditorArtworkUrl TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8192
    const-string v0, "ALTER TABLE RADIO_STATIONS ADD COLUMN HighlightColor TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8194
    const-string v0, "ALTER TABLE RADIO_STATIONS ADD COLUMN ProfileImage TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 8196
    return-void
.end method

.method private upgradeFrom99(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 8199
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8200
    const/16 v0, 0x25

    iput v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mPartialResyncFlags:I

    .line 8203
    :cond_0
    return-void
.end method


# virtual methods
.method public doOnCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5959
    sget-boolean v0, Lcom/google/android/music/store/Store;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "MusicStore"

    const-string v1, "Database created"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5960
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mDBPath:Ljava/lang/String;

    .line 5961
    iget-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mSyncHelper:Lcom/android/common/content/SyncStateContentProviderHelper;
    invoke-static {v0}, Lcom/google/android/music/store/Store;->access$200(Lcom/google/android/music/store/Store;)Lcom/android/common/content/SyncStateContentProviderHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/common/content/SyncStateContentProviderHelper;->createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 5962
    const/4 v0, -0x1

    const/16 v1, 0x68

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/store/Store$DatabaseHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 5963
    return-void
.end method

.method public doOnOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5949
    iget-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Database opened on main thread"

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 5950
    invoke-static {p1}, Lcom/google/android/music/store/Store;->configureDatabaseConnection(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 5951
    const-string v0, "CREATE TEMP VIEW USER_MUSIC AS SELECT * FROM MUSIC WHERE Domain=0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5954
    sget-boolean v0, Lcom/google/android/music/store/Store;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "MusicStore"

    const-string v1, "Database opened"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5955
    :cond_0
    return-void
.end method

.method public doOnUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/16 v9, 0x68

    .line 5972
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mDBPath:Ljava/lang/String;

    .line 5973
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 5974
    .local v0, "startTime":J
    move v2, p2

    .line 5976
    .local v2, "startVersion":I
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v3

    const-string v6, "Database upgraded on main thread"

    invoke-static {v3, v6}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 5978
    const/16 v3, 0x19

    if-ge p2, v3, :cond_0

    .line 5979
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->mResetMediaStoreImport:Z

    .line 5982
    const-string v3, "DROP TABLE IF EXISTS XFILESMUSIC"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5983
    const-string v3, "DROP TABLE IF EXISTS XFILES"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5984
    const-string v3, "DROP INDEX IF EXISTS XFILESMUSIC_SONGID_INDEX"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5985
    const-string v3, "DROP INDEX IF EXISTS XFILESMUSIC_ALBUMID_INDEX"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5986
    const-string v3, "DROP INDEX IF EXISTS XFILESMUSIC_ALBUMARTISTID_INDEX"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5987
    const-string v3, "DROP INDEX IF EXISTS XFILESMUSIC_GENREID_INDEX"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5988
    const-string v3, "DROP TABLE IF EXISTS LIBRARIES"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5995
    const-string v3, "DROP TABLE IF EXISTS MUSIC"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5996
    const-string v3, "CREATE TABLE MUSIC(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, SourceAccount INTEGER NOT NULL, SourceId TEXT NOT NULL, SourcePath TEXT, Size INTEGER NOT NULL, FileType INTEGER NOT NULL, FileDate INTEGER NOT NULL, LocalCopyPath TEXT, LocalCopyType INTEGER NOT NULL, PlayCount INTEGER NOT NULL DEFAULT 0, LastPlayDate INTEGER NOT NULL DEFAULT 0, Title TEXT NOT NULL, Album TEXT, Artist TEXT, AlbumArtist TEXT, AlbumArtistOrigin INTEGER, Composer TEXT , Genre TEXT, Year INTEGER, Duration INTEGER, TrackCount INTEGER, TrackNumber INTEGER, DiscCount INTEGER, DiscNumber INTEGER, Compilation INTEGER, BitRate INTEGER, AlbumArtLocation TEXT, SongId INTEGER NOT NULL, AlbumId INTEGER NOT NULL DEFAULT 0, AlbumArtistId INTEGER NOT NULL DEFAULT 0, GenreId INTEGER NOT NULL DEFAULT 0, CanonicalName TEXT NOT NULL, CanonicalAlbum TEXT, CanonicalAlbumArtist TEXT, CanonicalGenre TEXT, _sync_dirty INTEGER NOT NULL DEFAULT 0, _sync_version TEXT, UNIQUE( SourceAccount,SourceId) ON CONFLICT REPLACE);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6038
    const-string v3, "create index MUSIC_SONGID_INDEX on MUSIC(SongId)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6040
    const-string v3, "create index MUSIC_ALBUMID_INDEX on MUSIC(AlbumId)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6042
    const-string v3, "create index MUSIC_ALBUMARTISTID_INDEX on MUSIC(AlbumArtistId)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6044
    const-string v3, "create index MUSIC_GENREID_INDEX on MUSIC(GenreId)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6048
    const-string v3, "create index MUSIC_CANONICAL_NAME_INDEX on MUSIC(CanonicalName)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6050
    const-string v3, "create index MUSIC_CANONICAL_ALBUM_CANONICAL_NAME_INDEX on MUSIC(CanonicalAlbum, CanonicalName)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6053
    const-string v3, "create index MUSIC_CANONICAL_ARTIST_CANONICAL_NAME_INDEX on MUSIC(CanonicalAlbumArtist, CanonicalName)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6057
    const-string v3, "DROP TABLE IF EXISTS LISTS"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6058
    const-string v3, "CREATE TABLE LISTS(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT NOT NULL, SourceAccount INTEGER, SourceId TEXT, _sync_version TEXT, _sync_dirty INTEGER NOT NULL DEFAULT 0, MediaStoreId INTEGER);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6068
    const-string v3, "DROP TABLE IF EXISTS LIST_TOMBSTONES"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6069
    const-string v3, "CREATE TABLE LIST_TOMBSTONES(Id INTEGER PRIMARY KEY AUTOINCREMENT, SourceAccount INTEGER, SourceId TEXT, _sync_version TEXT);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6076
    const-string v3, "DROP TABLE IF EXISTS LISTITEMS"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6077
    const-string v3, "CREATE TABLE LISTITEMS(Id INTEGER PRIMARY KEY AUTOINCREMENT, ListId INTEGER NOT NULL REFERENCES LISTS, MusicSourceAccount INTEGER NOT NULL, MusicSourceId TEXT NOT NULL, ClientPosition INTEGER NOT NULL, ServerPosition INTEGER NOT NULL DEFAULT 0, SourceAccount INTEGER, SourceId TEXT, _sync_version TEXT, _sync_dirty INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6093
    const-string v3, "CREATE INDEX LISTITEMS_ORDER_INDEX ON LISTITEMS (ListId, ServerPosition, ClientPosition);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6099
    const-string v3, "DROP TABLE IF EXISTS LISTITEM_TOMBSTONES"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6100
    const-string v3, "CREATE TABLE LISTITEM_TOMBSTONES(Id INTEGER PRIMARY KEY AUTOINCREMENT, SourceAccount INTEGER, SourceId TEXT, _sync_version TEXT);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6107
    const-string v3, "DROP TABLE IF EXISTS KEEPON"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6108
    const-string v3, "CREATE TABLE KEEPON(KeepOnId INTEGER PRIMARY KEY AUTOINCREMENT, ListId INTEGER UNIQUE, AlbumId INTEGER UNIQUE, ArtistId INTEGER UNIQUE, DateAdded INTEGER );"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6116
    const-string v3, "DROP TABLE IF EXISTS ARTWORK"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6119
    const-string v3, "CREATE TABLE ARTWORK(AlbumId INTEGER PRIMARY KEY,LocalLocation STRING)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6123
    const-string v3, "DROP TABLE IF EXISTS SHOULDKEEPON"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6124
    const-string v3, "CREATE TABLE SHOULDKEEPON(\nMusicId INTEGER,\nKeepOnId INTEGER,\nFOREIGN KEY (KeepOnId) REFERENCES KEEPON (KeepOnId) ON DELETE CASCADE,\nFOREIGN KEY (MusicId) REFERENCES MUSIC (Id) ON DELETE CASCADE,\nUNIQUE (MusicId, KeepOnId) ON CONFLICT IGNORE)"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6133
    const-string v3, "CREATE INDEX SHOULDKEEPON_MusicId ON SHOULDKEEPON (MusicId);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6137
    const-string v3, "DROP TABLE IF EXISTS RECENT"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6138
    const-string v3, "CREATE TABLE RECENT(RecentId INTEGER PRIMARY KEY AUTOINCREMENT, ItemDate INTEGER, RecentListId INTEGER UNIQUE ON CONFLICT REPLACE, RecentAlbumId INTEGER UNIQUE ON CONFLICT REPLACE);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6145
    const-string v3, "DROP TABLE IF EXISTS _sync_state"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 6146
    iget-object v3, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mSyncHelper:Lcom/android/common/content/SyncStateContentProviderHelper;
    invoke-static {v3}, Lcom/google/android/music/store/Store;->access$200(Lcom/google/android/music/store/Store;)Lcom/android/common/content/SyncStateContentProviderHelper;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/common/content/SyncStateContentProviderHelper;->createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6147
    const/16 p2, 0x19

    .line 6150
    :cond_0
    const/16 v3, 0x1a

    if-ge p2, v3, :cond_1

    .line 6151
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom25(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6152
    const/16 p2, 0x1a

    .line 6155
    :cond_1
    const/16 v3, 0x1b

    if-ge p2, v3, :cond_2

    .line 6156
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom26(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6157
    const/16 p2, 0x1a

    .line 6160
    :cond_2
    const/16 v3, 0x1c

    if-ge p2, v3, :cond_3

    .line 6161
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom27(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6162
    const/16 p2, 0x1b

    .line 6165
    :cond_3
    const/16 v3, 0x1d

    if-ge p2, v3, :cond_4

    .line 6166
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom28(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6167
    const/16 p2, 0x1c

    .line 6170
    :cond_4
    const/16 v3, 0x1e

    if-ge p2, v3, :cond_5

    .line 6171
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom29(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6172
    const/16 p2, 0x1d

    .line 6175
    :cond_5
    const/16 v3, 0x1f

    if-ge p2, v3, :cond_6

    .line 6176
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom30(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6177
    const/16 p2, 0x1e

    .line 6180
    :cond_6
    const/16 v3, 0x20

    if-ge p2, v3, :cond_7

    .line 6181
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom31(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6182
    const/16 p2, 0x1f

    .line 6185
    :cond_7
    const/16 v3, 0x21

    if-ge p2, v3, :cond_8

    .line 6186
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom32(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6187
    const/16 p2, 0x20

    .line 6190
    :cond_8
    const/16 v3, 0x22

    if-ge p2, v3, :cond_9

    .line 6191
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom33(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6192
    const/16 p2, 0x21

    .line 6195
    :cond_9
    const/16 v3, 0x23

    if-ge p2, v3, :cond_a

    .line 6196
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom34(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6197
    const/16 p2, 0x22

    .line 6200
    :cond_a
    const/16 v3, 0x24

    if-ge p2, v3, :cond_b

    .line 6201
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom35(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6202
    const/16 p2, 0x23

    .line 6205
    :cond_b
    const/16 v3, 0x25

    if-ge p2, v3, :cond_c

    .line 6206
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom36(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6207
    const/16 p2, 0x24

    .line 6210
    :cond_c
    const/16 v3, 0x26

    if-ge p2, v3, :cond_d

    .line 6211
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom37(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6212
    const/16 p2, 0x25

    .line 6215
    :cond_d
    const/16 v3, 0x27

    if-ge p2, v3, :cond_e

    .line 6216
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom38(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6217
    const/16 p2, 0x26

    .line 6220
    :cond_e
    const/16 v3, 0x28

    if-ge p2, v3, :cond_f

    .line 6221
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom39(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6222
    const/16 p2, 0x27

    .line 6225
    :cond_f
    const/16 v3, 0x29

    if-ge p2, v3, :cond_10

    .line 6226
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom40(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6227
    const/16 p2, 0x28

    .line 6230
    :cond_10
    const/16 v3, 0x2a

    if-ge p2, v3, :cond_11

    .line 6231
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom41(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6232
    const/16 p2, 0x29

    .line 6235
    :cond_11
    const/16 v3, 0x2b

    if-ge p2, v3, :cond_12

    .line 6236
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom42(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6237
    const/16 p2, 0x2a

    .line 6240
    :cond_12
    const/16 v3, 0x2c

    if-ge p2, v3, :cond_13

    .line 6241
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom43(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6242
    const/16 p2, 0x2b

    .line 6245
    :cond_13
    const/16 v3, 0x2d

    if-ge p2, v3, :cond_14

    .line 6246
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom44(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6247
    const/16 p2, 0x2c

    .line 6250
    :cond_14
    const/16 v3, 0x2e

    if-ge p2, v3, :cond_15

    .line 6251
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom45(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6252
    const/16 p2, 0x2d

    .line 6255
    :cond_15
    const/16 v3, 0x2f

    if-ge p2, v3, :cond_16

    .line 6256
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom46(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6257
    const/16 p2, 0x2e

    .line 6260
    :cond_16
    const/16 v3, 0x30

    if-ge p2, v3, :cond_17

    .line 6261
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom47(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6262
    const/16 p2, 0x2f

    .line 6265
    :cond_17
    const/16 v3, 0x31

    if-ge p2, v3, :cond_18

    .line 6266
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom48(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6267
    const/16 p2, 0x30

    .line 6270
    :cond_18
    const/16 v3, 0x32

    if-ge p2, v3, :cond_19

    .line 6271
    invoke-direct {p0, p1, v2}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom49(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 6272
    const/16 p2, 0x31

    .line 6275
    :cond_19
    const/16 v3, 0x33

    if-ge p2, v3, :cond_1a

    .line 6276
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom50(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6277
    const/16 p2, 0x32

    .line 6280
    :cond_1a
    const/16 v3, 0x34

    if-ge p2, v3, :cond_1b

    .line 6281
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom51(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6282
    const/16 p2, 0x33

    .line 6285
    :cond_1b
    const/16 v3, 0x35

    if-ge p2, v3, :cond_1c

    .line 6286
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom52(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6287
    const/16 p2, 0x34

    .line 6290
    :cond_1c
    const/16 v3, 0x36

    if-ge p2, v3, :cond_1d

    .line 6291
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom53(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6292
    const/16 p2, 0x35

    .line 6295
    :cond_1d
    const/16 v3, 0x37

    if-ge p2, v3, :cond_1e

    .line 6296
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom54(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6297
    const/16 p2, 0x36

    .line 6300
    :cond_1e
    const/16 v3, 0x38

    if-ge p2, v3, :cond_1f

    .line 6301
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom55(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6302
    const/16 p2, 0x37

    .line 6305
    :cond_1f
    const/16 v3, 0x39

    if-ge p2, v3, :cond_20

    .line 6306
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom56(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6307
    const/16 p2, 0x38

    .line 6310
    :cond_20
    const/16 v3, 0x3a

    if-ge p2, v3, :cond_21

    .line 6311
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom57(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6312
    const/16 p2, 0x39

    .line 6315
    :cond_21
    const/16 v3, 0x3b

    if-ge p2, v3, :cond_22

    .line 6316
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom58(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6317
    const/16 p2, 0x3a

    .line 6320
    :cond_22
    const/16 v3, 0x3c

    if-ge p2, v3, :cond_23

    .line 6321
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom59(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6322
    const/16 p2, 0x3b

    .line 6325
    :cond_23
    const/16 v3, 0x3d

    if-ge p2, v3, :cond_24

    .line 6326
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom60(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6327
    const/16 p2, 0x3c

    .line 6330
    :cond_24
    const/16 v3, 0x3e

    if-ge p2, v3, :cond_25

    .line 6331
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom61(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6332
    const/16 p2, 0x3d

    .line 6335
    :cond_25
    const/16 v3, 0x3f

    if-ge p2, v3, :cond_26

    .line 6336
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom62(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6337
    const/16 p2, 0x3e

    .line 6340
    :cond_26
    const/16 v3, 0x40

    if-ge p2, v3, :cond_27

    .line 6341
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom63(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6342
    const/16 p2, 0x3f

    .line 6345
    :cond_27
    const/16 v3, 0x41

    if-ge p2, v3, :cond_28

    .line 6346
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom64(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6347
    const/16 p2, 0x40

    .line 6350
    :cond_28
    const/16 v3, 0x42

    if-ge p2, v3, :cond_29

    .line 6351
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom65(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6352
    const/16 p2, 0x41

    .line 6355
    :cond_29
    const/16 v3, 0x43

    if-ge p2, v3, :cond_2a

    .line 6356
    invoke-direct {p0}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom66()V

    .line 6357
    const/16 p2, 0x42

    .line 6360
    :cond_2a
    const/16 v3, 0x44

    if-ge p2, v3, :cond_2b

    .line 6361
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom67(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6362
    const/16 p2, 0x43

    .line 6365
    :cond_2b
    const/16 v3, 0x45

    if-ge p2, v3, :cond_2c

    .line 6366
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom68(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6367
    const/16 p2, 0x44

    .line 6370
    :cond_2c
    const/16 v3, 0x46

    if-ge p2, v3, :cond_2d

    .line 6371
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom69(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6372
    const/16 p2, 0x45

    .line 6375
    :cond_2d
    const/16 v3, 0x47

    if-ge p2, v3, :cond_2e

    .line 6376
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom70(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6377
    const/16 p2, 0x46

    .line 6380
    :cond_2e
    const/16 v3, 0x48

    if-ge p2, v3, :cond_2f

    .line 6381
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom71(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6382
    const/16 p2, 0x47

    .line 6385
    :cond_2f
    const/16 v3, 0x49

    if-ge p2, v3, :cond_30

    .line 6386
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom72(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6387
    const/16 p2, 0x48

    .line 6390
    :cond_30
    const/16 v3, 0x4a

    if-ge p2, v3, :cond_31

    .line 6391
    invoke-direct {p0, p1, v2}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom73(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 6392
    const/16 p2, 0x49

    .line 6395
    :cond_31
    const/16 v3, 0x4b

    if-ge p2, v3, :cond_32

    .line 6396
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom74(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6397
    const/16 p2, 0x4a

    .line 6400
    :cond_32
    const/16 v3, 0x4c

    if-ge p2, v3, :cond_33

    .line 6401
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom75(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6402
    const/16 p2, 0x4b

    .line 6405
    :cond_33
    const/16 v3, 0x4d

    if-ge p2, v3, :cond_34

    .line 6406
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom76(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6407
    const/16 p2, 0x4c

    .line 6410
    :cond_34
    const/16 v3, 0x4e

    if-ge p2, v3, :cond_35

    .line 6411
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom77(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6412
    const/16 p2, 0x4d

    .line 6415
    :cond_35
    const/16 v3, 0x4f

    if-ge p2, v3, :cond_36

    .line 6416
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom78(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6417
    const/16 p2, 0x4e

    .line 6420
    :cond_36
    const/16 v3, 0x50

    if-ge p2, v3, :cond_37

    .line 6421
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom79(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6422
    const/16 p2, 0x4f

    .line 6425
    :cond_37
    const/16 v3, 0x51

    if-ge p2, v3, :cond_38

    .line 6426
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom80(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6427
    const/16 p2, 0x50

    .line 6430
    :cond_38
    const/16 v3, 0x52

    if-ge p2, v3, :cond_39

    .line 6431
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom81(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6432
    const/16 p2, 0x51

    .line 6435
    :cond_39
    const/16 v3, 0x53

    if-ge p2, v3, :cond_3a

    .line 6436
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom82(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6437
    const/16 p2, 0x52

    .line 6440
    :cond_3a
    const/16 v3, 0x54

    if-ge p2, v3, :cond_3b

    .line 6441
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom83(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6442
    const/16 p2, 0x53

    .line 6445
    :cond_3b
    const/16 v3, 0x55

    if-ge p2, v3, :cond_3c

    .line 6446
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom84(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6447
    const/16 p2, 0x54

    .line 6450
    :cond_3c
    const/16 v3, 0x56

    if-ge p2, v3, :cond_3d

    .line 6451
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom85(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6452
    const/16 p2, 0x55

    .line 6455
    :cond_3d
    const/16 v3, 0x57

    if-ge p2, v3, :cond_3e

    .line 6456
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom86(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6457
    const/16 p2, 0x56

    .line 6459
    :cond_3e
    const/16 v3, 0x58

    if-ge p2, v3, :cond_3f

    .line 6460
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom87(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6461
    const/16 p2, 0x57

    .line 6463
    :cond_3f
    const/16 v3, 0x59

    if-ge p2, v3, :cond_40

    .line 6464
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom88(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6465
    const/16 p2, 0x58

    .line 6467
    :cond_40
    const/16 v3, 0x5a

    if-ge p2, v3, :cond_41

    .line 6468
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom89(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6469
    const/16 p2, 0x59

    .line 6471
    :cond_41
    const/16 v3, 0x5b

    if-ge p2, v3, :cond_42

    .line 6472
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom90(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6473
    const/16 p2, 0x5a

    .line 6475
    :cond_42
    const/16 v3, 0x5c

    if-ge p2, v3, :cond_43

    .line 6476
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom91(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6477
    const/16 p2, 0x5b

    .line 6479
    :cond_43
    const/16 v3, 0x5d

    if-ge p2, v3, :cond_44

    .line 6480
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom92(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6481
    const/16 p2, 0x5c

    .line 6483
    :cond_44
    const/16 v3, 0x5e

    if-ge p2, v3, :cond_45

    .line 6484
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom93(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6485
    const/16 p2, 0x5d

    .line 6487
    :cond_45
    const/16 v3, 0x5f

    if-ge p2, v3, :cond_46

    .line 6488
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom94(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6489
    const/16 p2, 0x5e

    .line 6491
    :cond_46
    const/16 v3, 0x60

    if-ge p2, v3, :cond_47

    .line 6492
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom95(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6493
    const/16 p2, 0x5f

    .line 6495
    :cond_47
    const/16 v3, 0x61

    if-ge p2, v3, :cond_48

    .line 6496
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom96(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6497
    const/16 p2, 0x60

    .line 6499
    :cond_48
    const/16 v3, 0x62

    if-ge p2, v3, :cond_49

    .line 6500
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom97(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6501
    const/16 p2, 0x61

    .line 6503
    :cond_49
    const/16 v3, 0x63

    if-ge p2, v3, :cond_4a

    .line 6504
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom98(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6505
    const/16 p2, 0x62

    .line 6507
    :cond_4a
    const/16 v3, 0x64

    if-ge p2, v3, :cond_4b

    .line 6508
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom99(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6509
    const/16 p2, 0x63

    .line 6511
    :cond_4b
    const/16 v3, 0x65

    if-ge p2, v3, :cond_4c

    .line 6512
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom100(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6513
    const/16 p2, 0x64

    .line 6515
    :cond_4c
    const/16 v3, 0x66

    if-ge p2, v3, :cond_4d

    .line 6516
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom101(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6517
    const/16 p2, 0x65

    .line 6519
    :cond_4d
    const/16 v3, 0x67

    if-ge p2, v3, :cond_4e

    .line 6520
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom102(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6521
    const/16 p2, 0x66

    .line 6523
    :cond_4e
    if-ge p2, v9, :cond_4f

    .line 6524
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->upgradeFrom103(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 6525
    const/16 p2, 0x67

    .line 6527
    :cond_4f
    invoke-direct {p0, p1}, Lcom/google/android/music/store/Store$DatabaseHelper;->postImportProcessing(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6531
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v0

    .line 6532
    .local v4, "upgradeTime":J
    const-string v3, "MusicStore"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Upgrade from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " took "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 6535
    return-void

    .line 6531
    .end local v4    # "upgradeTime":J
    :catchall_0
    move-exception v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v0

    .line 6532
    .restart local v4    # "upgradeTime":J
    const-string v6, "MusicStore"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Upgrade from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " took "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 6534
    throw v3
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 5966
    iget-object v0, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mMediaStoreImporter:Lcom/google/android/music/store/MediaStoreImporter;
    invoke-static {v0}, Lcom/google/android/music/store/Store;->access$300(Lcom/google/android/music/store/Store;)Lcom/google/android/music/store/MediaStoreImporter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/store/Store$DatabaseHelper;->this$0:Lcom/google/android/music/store/Store;

    # getter for: Lcom/google/android/music/store/Store;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/store/Store;->access$100(Lcom/google/android/music/store/Store;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/MediaStoreImporter;->invalidateMediaStoreImport(Landroid/content/Context;)V

    .line 5967
    new-instance v0, Lcom/google/android/music/store/DowngradeException;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/store/DowngradeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
