.class public Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSharedPlaylistEntriesResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public mNextPageToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "nextPageToken"
    .end annotation
.end field

.field public mPlaylistEntry:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlistEntry"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;",
            ">;"
        }
    .end annotation
.end field

.field public mResponseCode:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "responseCode"
    .end annotation
.end field

.field public mShareToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "shareToken"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;->mPlaylistEntry:Ljava/util/List;

    return-void
.end method
