.class public Lcom/google/android/music/ui/ArtistHeaderView;
.super Landroid/widget/RelativeLayout;
.source "ArtistHeaderView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field private static final sBackgroundHandler:Landroid/os/Handler;

.field private static final sMainHandler:Landroid/os/Handler;


# instance fields
.field private mArtWasClearedOnStop:Z

.field private mArtistArt:Landroid/widget/ImageView;

.field private mArtistArtUrl:Ljava/lang/String;

.field private mArtistId:J

.field private mArtistName:Ljava/lang/String;

.field private mPlayRadioSongList:Lcom/google/android/music/medialist/SongList;

.field private mRadioButton:Landroid/widget/ImageView;

.field private mRadioButtonWrapper:Landroid/view/View;

.field private mRadioTextView:Landroid/widget/TextView;

.field private mShowingNonDefaultArt:Z

.field private mShuffleButton:Landroid/widget/ImageView;

.field private mShuffleButtonWrapper:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/music/ui/ArtistHeaderView;->sMainHandler:Landroid/os/Handler;

    .line 71
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ArtHelperThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 73
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/music/ui/ArtistHeaderView;->sBackgroundHandler:Landroid/os/Handler;

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    iput-boolean v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShowingNonDefaultArt:Z

    .line 67
    iput-boolean v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtWasClearedOnStop:Z

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/ArtistHeaderView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/ArtistHeaderView;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistId:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/ArtistHeaderView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;
    .param p1, "x1"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistId:J

    return-wide p1
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArtUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ArtistHeaderView;->getBitmap(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/ArtistHeaderView;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mPlayRadioSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/music/ui/ArtistHeaderView;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/medialist/SongList;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;
    .param p1, "x1"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mPlayRadioSongList:Lcom/google/android/music/medialist/SongList;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/ArtistHeaderView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButtonWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/ArtistHeaderView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistHeaderView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShuffleButtonWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/music/ui/ArtistHeaderView;->sMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getBitmap(Ljava/lang/String;)V
    .locals 2
    .param p1, "artistUrl"    # Ljava/lang/String;

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/music/ui/ArtistHeaderView;->sBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/ui/ArtistHeaderView$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/ArtistHeaderView$2;-><init>(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    return-void
.end method


# virtual methods
.method public onBitmapResult(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 228
    if-eqz p2, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArt:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShowingNonDefaultArt:Z

    .line 232
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 160
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 161
    const v3, 0x7f0e00d5

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/ArtistHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArt:Landroid/widget/ImageView;

    .line 162
    const v3, 0x7f0e00d8

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/ArtistHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButtonWrapper:Landroid/view/View;

    .line 163
    const v3, 0x7f0e00d9

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/ArtistHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButton:Landroid/widget/ImageView;

    .line 164
    const v3, 0x7f0e00da

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/ArtistHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioTextView:Landroid/widget/TextView;

    .line 166
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 167
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    .line 168
    .local v1, "isPaidUser":Z
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    .line 170
    .local v0, "isOnDeviceOnly":Z
    if-eqz v0, :cond_0

    .line 171
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButtonWrapper:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButtonWrapper:Landroid/view/View;

    new-instance v4, Lcom/google/android/music/ui/ArtistHeaderView$3;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/ArtistHeaderView$3;-><init>(Lcom/google/android/music/ui/ArtistHeaderView;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    const v3, 0x7f0e00d6

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/ArtistHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShuffleButtonWrapper:Landroid/view/View;

    .line 186
    const v3, 0x7f0e00d7

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/ArtistHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShuffleButton:Landroid/widget/ImageView;

    .line 190
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 191
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShuffleButtonWrapper:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 194
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShuffleButtonWrapper:Landroid/view/View;

    new-instance v4, Lcom/google/android/music/ui/ArtistHeaderView$4;

    invoke-direct {v4, p0, v2}, Lcom/google/android/music/ui/ArtistHeaderView$4;-><init>(Lcom/google/android/music/ui/ArtistHeaderView;Lcom/google/android/music/preferences/MusicPreferences;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 213
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 215
    .local v1, "v":Landroid/view/View;
    if-ne v1, p0, :cond_0

    .line 216
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    neg-int v2, v2

    div-int/lit8 v0, v2, 0x2

    .line 217
    .local v0, "offset":I
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArt:Landroid/widget/ImageView;

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 220
    .end local v0    # "offset":I
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 225
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtWasClearedOnStop:Z

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArtUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ArtistHeaderView;->getBitmap(Ljava/lang/String;)V

    .line 242
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 249
    iget-boolean v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShowingNonDefaultArt:Z

    if-eqz v1, :cond_1

    .line 250
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArt:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 251
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 252
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArt:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 255
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mShowingNonDefaultArt:Z

    .line 256
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mArtWasClearedOnStop:Z

    .line 258
    :cond_1
    return-void
.end method

.method public setAlbumList(Lcom/google/android/music/medialist/AlbumList;)V
    .locals 3
    .param p1, "albumlist"    # Lcom/google/android/music/medialist/AlbumList;

    .prologue
    .line 84
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 85
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButtonWrapper:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 89
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 91
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButton:Landroid/widget/ImageView;

    const v2, 0x7f020106

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 92
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView;->mRadioTextView:Landroid/widget/TextView;

    const v2, 0x7f0b023b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 95
    :cond_1
    new-instance v1, Lcom/google/android/music/ui/ArtistHeaderView$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/ArtistHeaderView$1;-><init>(Lcom/google/android/music/ui/ArtistHeaderView;Lcom/google/android/music/medialist/AlbumList;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 140
    return-void
.end method
