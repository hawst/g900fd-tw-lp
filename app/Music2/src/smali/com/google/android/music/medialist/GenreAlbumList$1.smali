.class final Lcom/google/android/music/medialist/GenreAlbumList$1;
.super Ljava/lang/Object;
.source "GenreAlbumList.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/medialist/GenreAlbumList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/medialist/GenreAlbumList;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/medialist/GenreAlbumList;
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 95
    .local v2, "id":J
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 96
    .local v0, "filter":Z
    :goto_0
    new-instance v1, Lcom/google/android/music/medialist/GenreAlbumList;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/music/medialist/GenreAlbumList;-><init>(JZ)V

    return-object v1

    .line 95
    .end local v0    # "filter":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/GenreAlbumList$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/medialist/GenreAlbumList;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/medialist/GenreAlbumList;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 100
    new-array v0, p1, [Lcom/google/android/music/medialist/GenreAlbumList;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/GenreAlbumList$1;->newArray(I)[Lcom/google/android/music/medialist/GenreAlbumList;

    move-result-object v0

    return-object v0
.end method
