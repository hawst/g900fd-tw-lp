.class final enum Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
.super Ljava/lang/Enum;
.source "DialRemotePlaybackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/DialRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SessionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

.field public static final enum CONNECTED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

.field public static final enum ERROR_EVICTED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

.field public static final enum ERROR_JOIN_FAILED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

.field public static final enum ERROR_SESSION_IN_PROGRESS:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

.field public static final enum ERROR_TERMINATED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

.field public static final enum UNKNOWN:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 110
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    const-string v1, "CONNECTED"

    const-string v2, "SESSION_STATE_CONNECTED"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->CONNECTED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    .line 111
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    const-string v1, "UNKNOWN"

    const-string v2, "SESSION_STATE_UNKNOWN"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->UNKNOWN:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    .line 112
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    const-string v1, "ERROR_EVICTED"

    const-string v2, "ERROR_SESSION_EVICTED"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_EVICTED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    .line 113
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    const-string v1, "ERROR_JOIN_FAILED"

    const-string v2, "ERROR_SESSION_JOIN_FAILED"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_JOIN_FAILED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    .line 114
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    const-string v1, "ERROR_TERMINATED"

    const-string v2, "ERROR_SESSION_TERMINATED"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_TERMINATED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    .line 115
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    const-string v1, "ERROR_SESSION_IN_PROGRESS"

    const/4 v2, 0x5

    const-string v3, "ERROR_SESSION_IN_PROGRESS"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_SESSION_IN_PROGRESS:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    .line 109
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->CONNECTED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->UNKNOWN:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_EVICTED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_JOIN_FAILED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_TERMINATED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ERROR_SESSION_IN_PROGRESS:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->$VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "v"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 120
    iput-object p3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->value:Ljava/lang/String;

    .line 121
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->value:Ljava/lang/String;

    return-object v0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    .locals 7
    .param p0, "v"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-static {}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->values()[Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 125
    .local v3, "s":Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    iget-object v4, v3, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 126
    return-object v3

    .line 124
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    .end local v3    # "s":Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    const-class v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->$VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    invoke-virtual {v0}, [Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    return-object v0
.end method
