.class public interface abstract Lcom/google/android/music/store/Schema$Music;
.super Ljava/lang/Object;
.source "Schema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/Schema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Music"
.end annotation


# static fields
.field public static final EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 443
    new-instance v0, Ljava/lang/String;

    const v1, 0x10ffff

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sput-object v0, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    return-void
.end method
