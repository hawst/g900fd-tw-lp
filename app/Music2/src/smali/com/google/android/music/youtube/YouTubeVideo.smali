.class public Lcom/google/android/music/youtube/YouTubeVideo;
.super Ljava/lang/Object;
.source "YouTubeVideo.java"


# instance fields
.field private mAuthor:Ljava/lang/String;

.field private mDurationSeconds:I

.field private mId:Ljava/lang/String;

.field private mIsHd:Z

.field private mThumbnailUrl:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mViewCount:J

.field private final mYoutubeJson:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "youtubeJson"    # Lorg/json/JSONObject;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mTitle:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mAuthor:Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mYoutubeJson:Lorg/json/JSONObject;

    .line 47
    return-void
.end method

.method private getArray(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 1
    .param p1, "object"    # Lorg/json/JSONObject;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getObject(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1
    .param p1, "object"    # Lorg/json/JSONObject;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mAuthor:Ljava/lang/String;

    return-object v0
.end method

.method public getDurationSeconds()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mDurationSeconds:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mThumbnailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getViewCount()J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mViewCount:J

    return-wide v0
.end method

.method public isHd()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/music/youtube/YouTubeVideo;->mIsHd:Z

    return v0
.end method

.method public parse()Z
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 80
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mDurationSeconds:I

    .line 81
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/youtube/YouTubeVideo;->mYoutubeJson:Lorg/json/JSONObject;

    move-object/from16 v19, v0

    const-string v20, "media$group"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 82
    .local v9, "mediaGroup":Lorg/json/JSONObject;
    const-string v19, "media$content"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 83
    .local v8, "mediaContent":Lorg/json/JSONArray;
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v13

    .line 84
    .local v13, "size":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v13, :cond_0

    .line 85
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 86
    .local v6, "content":Lorg/json/JSONObject;
    const-string v19, "yt$format"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 87
    .local v14, "videoFormat":I
    const/16 v19, 0x5

    move/from16 v0, v19

    if-ne v14, v0, :cond_2

    .line 88
    const-string v19, "url"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 89
    .local v16, "videoUrl":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/youtube/YouTubeUtils;->getVideoId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 90
    .local v15, "videoId":Ljava/lang/String;
    if-eqz v15, :cond_1

    .line 91
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/music/youtube/YouTubeVideo;->mId:Ljava/lang/String;

    .line 92
    const-string v19, "duration"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 93
    const-string v19, "duration"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mDurationSeconds:I

    .line 104
    .end local v6    # "content":Lorg/json/JSONObject;
    .end local v14    # "videoFormat":I
    .end local v15    # "videoId":Ljava/lang/String;
    .end local v16    # "videoUrl":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/youtube/YouTubeVideo;->mId:Ljava/lang/String;

    move-object/from16 v19, v0

    if-nez v19, :cond_3

    .line 105
    const-string v19, "YouTubeVideo"

    const-string v20, "A video could not be extracted from the feed entry."

    invoke-static/range {v19 .. v20}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/16 v19, 0x0

    .line 156
    :goto_1
    return v19

    .line 97
    .restart local v6    # "content":Lorg/json/JSONObject;
    .restart local v14    # "videoFormat":I
    .restart local v15    # "videoId":Ljava/lang/String;
    .restart local v16    # "videoUrl":Ljava/lang/String;
    :cond_1
    const-string v19, "YouTubeVideo"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Error parsing video ID from "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .end local v15    # "videoId":Ljava/lang/String;
    .end local v16    # "videoUrl":Ljava/lang/String;
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 100
    :cond_2
    const-string v19, "YouTubeVideo"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Skipping video with format "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 110
    .end local v6    # "content":Lorg/json/JSONObject;
    .end local v14    # "videoFormat":I
    :cond_3
    const-string v19, ""

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mTitle:Ljava/lang/String;

    .line 111
    const-string v19, "media$title"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v9, v1}, Lcom/google/android/music/youtube/YouTubeVideo;->getObject(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 112
    .local v12, "mediaTitle":Lorg/json/JSONObject;
    if-eqz v12, :cond_4

    .line 113
    const-string v19, "$t"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mTitle:Ljava/lang/String;

    .line 117
    :cond_4
    const-string v19, ""

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mAuthor:Ljava/lang/String;

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/youtube/YouTubeVideo;->mYoutubeJson:Lorg/json/JSONObject;

    move-object/from16 v19, v0

    const-string v20, "author"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/youtube/YouTubeVideo;->getArray(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 119
    .local v4, "author":Lorg/json/JSONArray;
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v19

    if-lez v19, :cond_5

    .line 120
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 121
    .local v5, "authorObj":Lorg/json/JSONObject;
    const-string v19, "name"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v19

    const-string v20, "$t"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mAuthor:Ljava/lang/String;

    .line 125
    .end local v5    # "authorObj":Lorg/json/JSONObject;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/youtube/YouTubeVideo;->mYoutubeJson:Lorg/json/JSONObject;

    move-object/from16 v19, v0

    const-string v20, "yt$hd"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/youtube/YouTubeVideo;->getObject(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 126
    .local v17, "ytHd":Lorg/json/JSONObject;
    if-eqz v17, :cond_9

    const/16 v19, 0x1

    :goto_3
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mIsHd:Z

    .line 129
    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/youtube/YouTubeVideo;->mViewCount:J

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/youtube/YouTubeVideo;->mYoutubeJson:Lorg/json/JSONObject;

    move-object/from16 v19, v0

    const-string v20, "yt$statistics"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/youtube/YouTubeVideo;->getObject(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    .line 131
    .local v18, "ytStatistics":Lorg/json/JSONObject;
    if-eqz v18, :cond_6

    const-string v19, "viewCount"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 133
    :try_start_0
    const-string v19, "viewCount"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/youtube/YouTubeVideo;->mViewCount:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_6
    :goto_4
    const-string v19, "media$thumbnail"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v9, v1}, Lcom/google/android/music/youtube/YouTubeVideo;->getArray(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 141
    .local v11, "mediaThumbnails":Lorg/json/JSONArray;
    if-eqz v11, :cond_8

    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v19

    if-lez v19, :cond_8

    .line 142
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v13

    .line 143
    const/4 v7, 0x0

    :goto_5
    if-ge v7, v13, :cond_7

    .line 144
    invoke-virtual {v11, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 145
    .local v10, "mediaThumbnail":Lorg/json/JSONObject;
    const-string v19, "yt$name"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v20, "hqdefault"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 146
    const-string v19, "url"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mThumbnailUrl:Ljava/lang/String;

    .line 151
    .end local v10    # "mediaThumbnail":Lorg/json/JSONObject;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/youtube/YouTubeVideo;->mThumbnailUrl:Ljava/lang/String;

    move-object/from16 v19, v0

    if-nez v19, :cond_8

    .line 152
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    const-string v20, "url"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/youtube/YouTubeVideo;->mThumbnailUrl:Ljava/lang/String;

    .line 156
    :cond_8
    const/16 v19, 0x1

    goto/16 :goto_1

    .line 126
    .end local v11    # "mediaThumbnails":Lorg/json/JSONArray;
    .end local v18    # "ytStatistics":Lorg/json/JSONObject;
    :cond_9
    const/16 v19, 0x0

    goto/16 :goto_3

    .line 143
    .restart local v10    # "mediaThumbnail":Lorg/json/JSONObject;
    .restart local v11    # "mediaThumbnails":Lorg/json/JSONArray;
    .restart local v18    # "ytStatistics":Lorg/json/JSONObject;
    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 134
    .end local v10    # "mediaThumbnail":Lorg/json/JSONObject;
    .end local v11    # "mediaThumbnails":Lorg/json/JSONArray;
    :catch_0
    move-exception v19

    goto :goto_4
.end method
