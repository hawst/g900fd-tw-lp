.class public Lcom/google/android/music/store/DowngradeException;
.super Ljava/lang/RuntimeException;
.source "DowngradeException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final mFilepath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/google/android/music/store/DowngradeException;->mFilepath:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public getFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/music/store/DowngradeException;->mFilepath:Ljava/lang/String;

    return-object v0
.end method
