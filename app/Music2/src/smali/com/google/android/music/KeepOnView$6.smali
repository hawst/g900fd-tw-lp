.class Lcom/google/android/music/KeepOnView$6;
.super Ljava/lang/Object;
.source "KeepOnView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/KeepOnView;->onKeepOnViewClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/KeepOnView;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/google/android/music/KeepOnView;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Lcom/google/android/music/KeepOnView$6;->this$0:Lcom/google/android/music/KeepOnView;

    iput-object p2, p0, Lcom/google/android/music/KeepOnView$6;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$6;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setShowUnpinDialog(Z)V

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$6;->this$0:Lcom/google/android/music/KeepOnView;

    # invokes: Lcom/google/android/music/KeepOnView;->handleOnKeepOnClicked()V
    invoke-static {v0}, Lcom/google/android/music/KeepOnView;->access$1100(Lcom/google/android/music/KeepOnView;)V

    .line 513
    return-void
.end method
