.class Lcom/google/android/music/ui/dialogs/UndoDialog$3;
.super Ljava/lang/Object;
.source "UndoDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/dialogs/UndoDialog;->getRemoveRunnable(Landroid/app/Dialog;)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/dialogs/UndoDialog;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$400(Lcom/google/android/music/ui/dialogs/UndoDialog;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$200(Lcom/google/android/music/ui/dialogs/UndoDialog;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$400(Lcom/google/android/music/ui/dialogs/UndoDialog;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$500(Lcom/google/android/music/ui/dialogs/UndoDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->performDelete(Ljava/util/ArrayList;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;)V

    .line 127
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$200(Lcom/google/android/music/ui/dialogs/UndoDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 128
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->dismiss()V

    .line 129
    return-void

    .line 125
    :cond_0
    const-string v0, "MusicUndo"

    const-string v1, "Song list has not been set for this dialog."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
