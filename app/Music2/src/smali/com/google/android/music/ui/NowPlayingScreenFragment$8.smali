.class Lcom/google/android/music/ui/NowPlayingScreenFragment$8;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;->startLightsAnimation(ZLandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

.field final synthetic val$target:Landroid/view/View;

.field final synthetic val$turnOn:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 845
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    iput-boolean p2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$turnOn:Z

    iput-object p3, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$target:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 853
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 854
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$turnOn:Z

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    sget-object v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;->ON:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    # setter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1302(Lcom/google/android/music/ui/NowPlayingScreenFragment;Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;)Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    .line 861
    :goto_0
    return-void

    .line 857
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$target:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 858
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$target:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setEnabledAll(Landroid/view/View;Z)V

    .line 859
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    sget-object v1, Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;->OFF:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    # setter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mCurrentLightsState:Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1302(Lcom/google/android/music/ui/NowPlayingScreenFragment;Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;)Lcom/google/android/music/ui/NowPlayingScreenFragment$LightsState;

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 862
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 847
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$turnOn:Z

    if-eqz v0, :cond_0

    .line 848
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$target:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 849
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$8;->val$target:Landroid/view/View;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setEnabledAll(Landroid/view/View;Z)V

    .line 851
    :cond_0
    return-void
.end method
