.class public Lcom/google/android/music/io/ThrottledInputStream;
.super Ljava/io/FilterInputStream;
.source "ThrottledInputStream.java"


# instance fields
.field private mFirstThrottledReadTime:J

.field private final mMaxRateBytesPerMillisecond:D

.field private mTotalBytesRead:J

.field private final mUnthrottledBytesRequested:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;IJ)V
    .locals 5
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "maxKbps"    # I
    .param p3, "unthrottledBytes"    # J

    .prologue
    .line 46
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 49
    int-to-double v0, p2

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/music/io/ThrottledInputStream;->mMaxRateBytesPerMillisecond:D

    .line 50
    iput-wide p3, p0, Lcom/google/android/music/io/ThrottledInputStream;->mUnthrottledBytesRequested:J

    .line 51
    return-void
.end method

.method private delayIfNeeded()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .prologue
    .line 108
    iget-wide v10, p0, Lcom/google/android/music/io/ThrottledInputStream;->mTotalBytesRead:J

    iget-wide v12, p0, Lcom/google/android/music/io/ThrottledInputStream;->mUnthrottledBytesRequested:J

    cmp-long v1, v10, v12

    if-gtz v1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 114
    .local v6, "now":J
    iget-wide v10, p0, Lcom/google/android/music/io/ThrottledInputStream;->mFirstThrottledReadTime:J

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-nez v1, :cond_2

    .line 116
    iput-wide v6, p0, Lcom/google/android/music/io/ThrottledInputStream;->mFirstThrottledReadTime:J

    .line 120
    :cond_2
    iget-wide v10, p0, Lcom/google/android/music/io/ThrottledInputStream;->mTotalBytesRead:J

    iget-wide v12, p0, Lcom/google/android/music/io/ThrottledInputStream;->mUnthrottledBytesRequested:J

    sub-long/2addr v10, v12

    long-to-double v10, v10

    iget-wide v12, p0, Lcom/google/android/music/io/ThrottledInputStream;->mMaxRateBytesPerMillisecond:D

    div-double v2, v10, v12

    .line 122
    .local v2, "expectedDurationMillis":D
    iget-wide v10, p0, Lcom/google/android/music/io/ThrottledInputStream;->mFirstThrottledReadTime:J

    long-to-double v10, v10

    add-double/2addr v10, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    .line 123
    .local v4, "expectedEndTime":J
    sub-long v8, v4, v6

    .line 124
    .local v8, "waitTime":J
    const-wide/16 v10, 0xa

    cmp-long v1, v8, v10

    if-ltz v1, :cond_0

    .line 125
    const-wide/16 v10, 0x3e8

    cmp-long v1, v8, v10

    if-lez v1, :cond_3

    .line 126
    const-wide/16 v8, 0x3e8

    .line 130
    :cond_3
    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 133
    new-instance v1, Ljava/io/InterruptedIOException;

    const-string v10, "Interrupted while throttling"

    invoke-direct {v1, v10}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public read()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    .line 56
    .local v0, "read":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 57
    iget-wide v2, p0, Lcom/google/android/music/io/ThrottledInputStream;->mTotalBytesRead:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/music/io/ThrottledInputStream;->mTotalBytesRead:J

    .line 58
    invoke-direct {p0}, Lcom/google/android/music/io/ThrottledInputStream;->delayIfNeeded()V

    .line 60
    :cond_0
    return v0
.end method

.method public read([BII)I
    .locals 8
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    if-nez p3, :cond_1

    const/4 v2, 0x0

    .line 100
    :cond_0
    :goto_0
    return v2

    .line 67
    :cond_1
    if-gez p3, :cond_2

    .line 68
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Length cannot be negative: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 73
    :cond_2
    const/4 v2, 0x0

    .line 74
    .local v2, "read":I
    const/4 v0, 0x0

    .line 75
    .local v0, "eos":Z
    :goto_1
    if-ge v2, p3, :cond_4

    .line 76
    sub-int v3, p3, v2

    .line 77
    .local v3, "toRead":I
    add-int v4, p2, v2

    invoke-super {p0, p1, v4, v3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v1

    .line 78
    .local v1, "oneRead":I
    if-nez v1, :cond_3

    .line 81
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "InputStream.read() returned zero when asked for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 83
    :cond_3
    if-gez v1, :cond_5

    .line 85
    const/4 v0, 0x1

    .line 92
    .end local v1    # "oneRead":I
    .end local v3    # "toRead":I
    :cond_4
    iget-wide v4, p0, Lcom/google/android/music/io/ThrottledInputStream;->mTotalBytesRead:J

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/music/io/ThrottledInputStream;->mTotalBytesRead:J

    .line 94
    if-eqz v0, :cond_6

    .line 97
    if-nez v2, :cond_0

    const/4 v2, -0x1

    goto :goto_0

    .line 88
    .restart local v1    # "oneRead":I
    .restart local v3    # "toRead":I
    :cond_5
    add-int/2addr v2, v1

    .line 90
    goto :goto_1

    .line 99
    .end local v1    # "oneRead":I
    .end local v3    # "toRead":I
    :cond_6
    invoke-direct {p0}, Lcom/google/android/music/io/ThrottledInputStream;->delayIfNeeded()V

    goto :goto_0
.end method
