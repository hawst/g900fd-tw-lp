.class Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistAlbumsCursor.java"


# static fields
.field private static final PROJECTION_ARTIST_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->PROJECTION_ARTIST_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 56
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    .line 57
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 59
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mImageWidth:I

    .line 60
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mImageHeight:I

    .line 62
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->addAlbums(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method private addAlbums(Ljava/lang/String;)V
    .locals 9
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    .line 67
    .local v4, "isNautilusId":Z
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v3

    .line 68
    .local v3, "isNautilusEnabled":Z
    const/4 v5, 0x0

    .line 76
    .local v5, "nautilusArtistId":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 77
    const-wide/16 v0, 0x0

    .line 79
    .local v0, "artistIdLong":J
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 84
    invoke-direct {p0, v0, v1}, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->addRowsForNonNautilusArtist(J)V

    .line 87
    if-eqz v3, :cond_2

    .line 88
    iget-object v6, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    invoke-static {v6, v0, v1}, Lcom/google/android/music/xdi/XdiUtils;->getArtistMetajamId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 89
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 90
    const-string v6, "MusicXdi"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Metajam artist not found for artist ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .end local v0    # "artistIdLong":J
    :cond_0
    :goto_0
    return-void

    .line 80
    .restart local v0    # "artistIdLong":J
    :catch_0
    move-exception v2

    .line 81
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v6, "MusicXdi"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Artist ID could not be converted to long: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    .end local v0    # "artistIdLong":J
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    move-object v5, p1

    .line 98
    :cond_2
    if-eqz v3, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 99
    invoke-direct {p0, v5}, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->addRowsForNautilusArtist(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private addRowsForCursor(Landroid/database/Cursor;Z)V
    .locals 20
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "isNautilus"    # Z

    .prologue
    .line 136
    if-nez p1, :cond_0

    .line 212
    :goto_0
    return-void

    .line 140
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v15}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v15

    new-array v14, v15, [Ljava/lang/Object;

    .line 143
    .local v14, "values":[Ljava/lang/Object;
    :goto_1
    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v15

    if-eqz v15, :cond_8

    .line 144
    const/4 v15, 0x0

    invoke-static {v14, v15}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 145
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 146
    .local v6, "albumId":J
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "album":Ljava/lang/String;
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 148
    .local v3, "albumArtist":Ljava/lang/String;
    const/4 v15, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 149
    .local v4, "albumArtistId":J
    const/4 v8, 0x0

    .line 150
    .local v8, "albumMetajamId":Ljava/lang/String;
    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_1

    .line 151
    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 153
    :cond_1
    const/4 v10, 0x0

    .line 154
    .local v10, "artistMetajamId":Ljava/lang/String;
    const/4 v15, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_2

    .line 155
    const/4 v15, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 158
    :cond_2
    const/4 v12, 0x0

    .line 161
    .local v12, "trackCount":I
    if-eqz p2, :cond_5

    .line 162
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 163
    const-string v15, "MusicXdi"

    const-string v16, "Invalid album metajam ID for nautilus artist."

    invoke-static/range {v15 .. v16}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 210
    .end local v2    # "album":Ljava/lang/String;
    .end local v3    # "albumArtist":Ljava/lang/String;
    .end local v4    # "albumArtistId":J
    .end local v6    # "albumId":J
    .end local v8    # "albumMetajamId":Ljava/lang/String;
    .end local v10    # "artistMetajamId":Ljava/lang/String;
    .end local v12    # "trackCount":I
    :catchall_0
    move-exception v15

    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v15

    .line 166
    .restart local v2    # "album":Ljava/lang/String;
    .restart local v3    # "albumArtist":Ljava/lang/String;
    .restart local v4    # "albumArtistId":J
    .restart local v6    # "albumId":J
    .restart local v8    # "albumMetajamId":Ljava/lang/String;
    .restart local v10    # "artistMetajamId":Ljava/lang/String;
    .restart local v12    # "trackCount":I
    :cond_3
    const/4 v15, 0x2

    :try_start_1
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 167
    .local v9, "artUriString":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 168
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 170
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v15, v8, v0}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInNautilusAlbumCount(Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v12

    .line 183
    :goto_2
    if-eqz p2, :cond_7

    .end local v8    # "albumMetajamId":Ljava/lang/String;
    :goto_3
    invoke-static {v8}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    .line 186
    .local v11, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "_id"

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "display_name"

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 188
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "display_subname"

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const/high16 v16, 0x7f120000

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v12, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 192
    .local v13, "trackCountText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "display_description"

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0, v13}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 194
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "display_number"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "image_uri"

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "item_display_type"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 200
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "user_rating_count"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "user_rating"

    const/16 v17, -0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "action_uri"

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v16, "music_albumArtist"

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 207
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 173
    .end local v9    # "artUriString":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v13    # "trackCountText":Ljava/lang/String;
    .restart local v8    # "albumMetajamId":Ljava/lang/String;
    :cond_5
    const-wide/16 v16, -0x1

    cmp-long v15, v6, v16

    if-gtz v15, :cond_6

    .line 174
    const-string v15, "MusicXdi"

    const-string v16, "Invalid album ID for non-nautilus artist."

    invoke-static/range {v15 .. v16}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 177
    :cond_6
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mImageWidth:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mImageHeight:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v6, v7, v15, v0, v1}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 179
    .restart local v9    # "artUriString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v15, v6, v7, v0}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInAlbumCount(Landroid/content/Context;JZ)I

    move-result v12

    goto/16 :goto_2

    .line 183
    :cond_7
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    goto/16 :goto_3

    .line 210
    .end local v2    # "album":Ljava/lang/String;
    .end local v3    # "albumArtist":Ljava/lang/String;
    .end local v4    # "albumArtistId":J
    .end local v6    # "albumId":J
    .end local v8    # "albumMetajamId":Ljava/lang/String;
    .end local v9    # "artUriString":Ljava/lang/String;
    .end local v10    # "artistMetajamId":Ljava/lang/String;
    .end local v12    # "trackCount":I
    :cond_8
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method private addRowsForNautilusArtist(Ljava/lang/String;)V
    .locals 9
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 120
    iget-object v0, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByNautilusArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->PROJECTION_ARTIST_ALBUMS:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 126
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 128
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v8, v0}, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->addRowsForCursor(Landroid/database/Cursor;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 133
    :cond_0
    return-void

    .line 130
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private addRowsForNonNautilusArtist(J)V
    .locals 9
    .param p1, "artistId"    # J

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->mContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->PROJECTION_ARTIST_ALBUMS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 110
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 112
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v8, v0}, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;->addRowsForCursor(Landroid/database/Cursor;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 117
    :cond_0
    return-void

    .line 114
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method
