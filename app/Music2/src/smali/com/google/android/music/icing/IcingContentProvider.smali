.class public Lcom/google/android/music/icing/IcingContentProvider;
.super Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;
.source "IcingContentProvider.java"


# static fields
.field private static final LEGACY_CORPUS_NAMES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MUSIC_SongId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MUSIC_AlbumArtistId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MUSIC_AlbumId"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "LISTS_Id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/icing/IcingContentProvider;->LEGACY_CORPUS_NAMES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;-><init>()V

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/icing/IcingContentProvider;->LEGACY_CORPUS_NAMES:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/icing/IcingContentProvider;)Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/icing/IcingContentProvider;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/music/icing/IcingContentProvider;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/icing/IcingContentProvider;)Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/icing/IcingContentProvider;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/music/icing/IcingContentProvider;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/icing/IcingContentProvider;Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/icing/IcingContentProvider;
    .param p1, "x1"    # Ljava/util/concurrent/Callable;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Object;

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/music/icing/IcingContentProvider;->executeWithConnection(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected createDatabase(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;
    .locals 2
    .param p1, "listener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/music/icing/IcingContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    .line 104
    .local v0, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->initializeDatabase()V

    .line 105
    invoke-virtual {v0, p0}, Lcom/google/android/music/store/Store;->setTableChangeListener(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V

    .line 106
    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->getDatabaseHelper()Lcom/google/android/music/store/Store$DatabaseHelper;

    move-result-object v1

    return-object v1
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected doGetType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 111
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected doOnCreate()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/music/icing/IcingContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 42
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "com.google.android.music.icing.IcingContentProvider.LEGACY_CORPORA_REMOVED"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    const-string v2, "com.google.android.music.icing.IcingContentProvider.LEGACY_CORPORA_REMOVE_RETRY_COUNT"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 44
    .local v1, "retriesCount":I
    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 45
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "com.google.android.music.icing.IcingContentProvider.LEGACY_CORPORA_REMOVE_RETRY_COUNT"

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 47
    new-instance v2, Lcom/google/android/music/icing/IcingContentProvider$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/icing/IcingContentProvider$1;-><init>(Lcom/google/android/music/icing/IcingContentProvider;Landroid/content/SharedPreferences;)V

    new-array v3, v5, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/google/android/music/icing/IcingContentProvider$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 83
    .end local v1    # "retriesCount":I
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method protected doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # [Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;

    .prologue
    .line 116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected getContentProviderAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "com.google.android.music.icing"

    return-object v0
.end method

.method public getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/music/icing/IcingContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    .line 96
    .local v0, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v0, p0}, Lcom/google/android/music/store/Store;->setTableChangeListener(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/music/icing/IcingContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/icing/SearchCorpora;->getInstance(Landroid/content/Context;)Lcom/google/android/music/icing/SearchCorpora;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/music/icing/SearchCorpora;->tableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    return-object v1
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 131
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
