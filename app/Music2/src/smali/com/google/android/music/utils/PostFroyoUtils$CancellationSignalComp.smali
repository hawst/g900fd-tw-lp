.class public Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;
.super Ljava/lang/Object;
.source "PostFroyoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/PostFroyoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CancellationSignalComp"
.end annotation


# instance fields
.field private final mSignal:Landroid/os/CancellationSignal;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->mSignal:Landroid/os/CancellationSignal;

    .line 408
    return-void
.end method

.method public constructor <init>(Landroid/os/CancellationSignal;)V
    .locals 0
    .param p1, "signal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    iput-object p1, p0, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->mSignal:Landroid/os/CancellationSignal;

    .line 404
    return-void
.end method


# virtual methods
.method public getCancellationSignal()Landroid/os/CancellationSignal;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->mSignal:Landroid/os/CancellationSignal;

    return-object v0
.end method

.method public hasSignal()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 411
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 412
    iget-object v1, p0, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;->mSignal:Landroid/os/CancellationSignal;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 414
    :cond_0
    return v0
.end method
