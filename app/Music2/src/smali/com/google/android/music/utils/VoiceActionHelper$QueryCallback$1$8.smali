.class Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;
.super Ljava/lang/Object;
.source "VoiceActionHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->backgroundTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;

.field final synthetic val$finalGenreArtUrl:Ljava/lang/String;

.field final synthetic val$finalGenreId:Ljava/lang/String;

.field final synthetic val$finalGenreName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->this$2:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;

    iput-object p2, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->val$finalGenreName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->val$finalGenreId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->val$finalGenreArtUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->this$2:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;

    iget-object v0, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v0, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/utils/VoiceActionHelper;->access$300(Lcom/google/android/music/utils/VoiceActionHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->val$finalGenreName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->val$finalGenreId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;->val$finalGenreArtUrl:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->playGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    return-void
.end method
