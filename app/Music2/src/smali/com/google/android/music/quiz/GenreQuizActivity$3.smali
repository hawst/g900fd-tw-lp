.class Lcom/google/android/music/quiz/GenreQuizActivity$3;
.super Ljava/lang/Object;
.source "GenreQuizActivity.java"

# interfaces
.implements Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/quiz/GenreQuizActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/quiz/GenreQuizActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/quiz/GenreQuizActivity;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$3;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Lcom/google/android/music/widgets/ArtImageCheckbox;Z)V
    .locals 3
    .param p1, "checkbox"    # Lcom/google/android/music/widgets/ArtImageCheckbox;
    .param p2, "isChecked"    # Z

    .prologue
    .line 236
    invoke-virtual {p1}, Lcom/google/android/music/widgets/ArtImageCheckbox;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;

    .line 237
    .local v0, "vh":Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;
    if-eqz p2, :cond_0

    .line 238
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$3;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$600(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/HashSet;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mGenreId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$3;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$600(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/HashSet;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mGenreId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
