.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeekRequest"
.end annotation


# instance fields
.field private final mItemId:Ljava/lang/String;

.field private final mPositionMillis:J

.field private final mQueueVersion:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method public constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p2, "queueVersion"    # Ljava/lang/String;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J

    .prologue
    .line 1140
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1141
    iput-object p2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->mQueueVersion:Ljava/lang/String;

    .line 1142
    iput-object p3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->mItemId:Ljava/lang/String;

    .line 1143
    iput-wide p4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->mPositionMillis:J

    .line 1144
    return-void
.end method


# virtual methods
.method public getItemId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->mItemId:Ljava/lang/String;

    return-object v0
.end method

.method public getPositionMillis()J
    .locals 2

    .prologue
    .line 1159
    iget-wide v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->mPositionMillis:J

    return-wide v0
.end method

.method public getQueueVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1147
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->mQueueVersion:Ljava/lang/String;

    return-object v0
.end method
