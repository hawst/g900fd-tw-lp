.class public Lcom/google/android/music/activities/SharedSongsActivity;
.super Landroid/app/Activity;
.source "SharedSongsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;
    }
.end annotation


# instance fields
.field private final LOGV:Z

.field private mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

.field private mButton:Landroid/widget/Button;

.field private volatile mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

.field private mIsSharedPlaylist:Z

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSecondaryButton:Landroid/widget/Button;

.field private mTextView:Landroid/widget/TextView;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->LOGV:Z

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mIsSharedPlaylist:Z

    .line 126
    new-instance v0, Lcom/google/android/music/activities/SharedSongsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/activities/SharedSongsActivity$1;-><init>(Lcom/google/android/music/activities/SharedSongsActivity;)V

    iput-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 176
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/activities/SharedSongsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->LOGV:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/music/activities/SharedSongsActivity;)Lcom/google/android/music/cloudclient/MusicCloud;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/activities/SharedSongsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/SharedSongsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mSecondaryButton:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 120
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 121
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    .line 124
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 434
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 435
    invoke-virtual {p0}, Lcom/google/android/music/activities/SharedSongsActivity;->finish()V

    .line 440
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mSecondaryButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 437
    invoke-static {p0, v1, v1}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialToChooseAccountForResult(Landroid/app/Activity;ZI)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 83
    new-instance v2, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v2, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    .line 85
    new-instance v2, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    invoke-direct {v2, p0}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;-><init>(Lcom/google/android/music/activities/SharedSongsActivity;)V

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    .line 87
    const v2, 0x7f040109

    invoke-virtual {p0, v2}, Lcom/google/android/music/activities/SharedSongsActivity;->setContentView(I)V

    .line 88
    const v2, 0x7f0e00df

    invoke-virtual {p0, v2}, Lcom/google/android/music/activities/SharedSongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 89
    const v2, 0x7f0e007d

    invoke-virtual {p0, v2}, Lcom/google/android/music/activities/SharedSongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mTextView:Landroid/widget/TextView;

    .line 90
    const v2, 0x7f0e0290

    invoke-virtual {p0, v2}, Lcom/google/android/music/activities/SharedSongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mButton:Landroid/widget/Button;

    .line 91
    iget-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    const v2, 0x7f0e0291

    invoke-virtual {p0, v2}, Lcom/google/android/music/activities/SharedSongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mSecondaryButton:Landroid/widget/Button;

    .line 93
    iget-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mSecondaryButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/music/activities/SharedSongsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 97
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "url"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "url":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 99
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    .line 103
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    if-nez v2, :cond_1

    .line 104
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    .line 105
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mIsSharedPlaylist:Z

    .line 108
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 109
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMusicAppLink(Ljava/lang/String;)V

    .line 113
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/music/activities/SharedSongsActivity;->LOGV:Z

    if-eqz v2, :cond_3

    .line 114
    const-string v2, "SharedSongsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Shared url="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 164
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    invoke-virtual {v0}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->quit()V

    .line 166
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 158
    iget-object v0, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/activities/SharedSongsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 159
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 143
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 145
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.music.VERIFIED_ACCOUNTS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 146
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/activities/SharedSongsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 148
    iget-boolean v1, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mIsSharedPlaylist:Z

    if-eqz v1, :cond_0

    .line 149
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/activities/SharedSongsActivity;->mAsyncWorker:Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/music/activities/SharedSongsActivity$AsyncWorker;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
