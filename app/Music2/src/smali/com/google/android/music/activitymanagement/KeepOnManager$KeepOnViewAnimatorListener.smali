.class Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;
.super Ljava/lang/Object;
.source "KeepOnManager.java"

# interfaces
.implements Lcom/google/android/music/animator/AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/activitymanagement/KeepOnManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeepOnViewAnimatorListener"
.end annotation


# instance fields
.field private mKeepOnState:Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

.field private mKey:J

.field private mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;)V
    .locals 0
    .param p2, "key"    # J
    .param p4, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;J",
            "Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 541
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/google/android/music/animator/Animator;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542
    iput-object p1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;->mMap:Ljava/util/Map;

    .line 543
    iput-wide p2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;->mKey:J

    .line 544
    iput-object p4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;->mKeepOnState:Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    .line 545
    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/google/android/music/animator/Animator;)V
    .locals 4
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;->mMap:Ljava/util/Map;

    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;->mKey:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;->mKeepOnState:Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setIsUnwindAnimationInProcess(Z)V

    .line 553
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;->mKeepOnState:Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    invoke-interface {v0}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->invalidate()V

    .line 554
    return-void
.end method

.method public onAnimationRepeat(Lcom/google/android/music/animator/Animator;)V
    .locals 0
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 561
    return-void
.end method

.method public onAnimationStart(Lcom/google/android/music/animator/Animator;)V
    .locals 0
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 547
    return-void
.end method
