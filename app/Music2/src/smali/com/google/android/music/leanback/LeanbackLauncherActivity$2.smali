.class Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;
.super Ljava/lang/Object;
.source "LeanbackLauncherActivity.java"

# interfaces
.implements Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackLauncherActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountStatusUpdate(Landroid/accounts/Account;Lcom/google/android/music/NautilusStatus;)V
    .locals 4
    .param p1, "selectedAccount"    # Landroid/accounts/Account;
    .param p2, "nautilusStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$300(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUpdateNautilusRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$200(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNautilusEnabled:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$400(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/music/NautilusStatus;->isNautilusEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$300(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUpdateNautilusRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$200(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUpdateNautilusRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$200(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
