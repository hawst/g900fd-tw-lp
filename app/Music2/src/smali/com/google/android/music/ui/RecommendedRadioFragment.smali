.class public Lcom/google/android/music/ui/RecommendedRadioFragment;
.super Lcom/google/android/music/ui/MediaListGridFragment;
.source "RecommendedRadioFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/RecommendedRadioFragment$1;,
        Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "playlist_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/RecommendedRadioFragment;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/music/medialist/RecommendedRadioList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/RecommendedRadioList;-><init>()V

    sget-object v1, Lcom/google/android/music/ui/RecommendedRadioFragment;->PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/ui/MediaListGridFragment;-><init>(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 38
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/RecommendedRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 42
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/RecommendedRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/RecommendedRadioFragment;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/music/ui/RecommendedRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initEmptyScreen()V

    .line 113
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const v0, 0x7f0b02a8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/RecommendedRadioFragment;->setEmptyScreenText(I)V

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    const v0, 0x7f0b02a9

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/RecommendedRadioFragment;->setEmptyScreenText(I)V

    goto :goto_0
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;-><init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/RecommendedRadioFragment$1;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;-><init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment$1;)V

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/music/ui/RecommendedRadioFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 53
    return-void
.end method
