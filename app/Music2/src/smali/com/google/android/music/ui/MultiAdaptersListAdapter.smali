.class public Lcom/google/android/music/ui/MultiAdaptersListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MultiAdaptersListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/MultiAdaptersListAdapter$1;,
        Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;,
        Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    }
.end annotation


# instance fields
.field private final mHeadersLayoutId:I

.field private final mLayoutsToType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutsToTypeInvalid:Z

.field private final mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

.field private final mSections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "initialCapacity"    # I

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 71
    new-instance v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;-><init>(Lcom/google/android/music/ui/MultiAdaptersListAdapter$1;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    .line 73
    const v0, 0x7f040018

    iput v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mHeadersLayoutId:I

    .line 78
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToType:Ljava/util/Map;

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToTypeInvalid:Z

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mSections:Ljava/util/ArrayList;

    .line 86
    return-void
.end method

.method private getLayoutsToType()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-boolean v5, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToTypeInvalid:Z

    if-eqz v5, :cond_2

    .line 131
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .line 132
    .local v4, "viewIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .line 133
    .local v0, "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    invoke-interface {v0, v4}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->addLayoutIds(Ljava/util/Set;)V

    goto :goto_0

    .line 135
    .end local v0    # "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    :cond_0
    const v5, 0x7f040018

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v5, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToType:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 138
    const/4 v3, 0x0

    .line 139
    .local v3, "index":I
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 140
    .local v2, "id":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToType:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    add-int/lit8 v3, v3, 0x1

    .line 142
    goto :goto_1

    .line 143
    .end local v2    # "id":Ljava/lang/Integer;
    :cond_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToTypeInvalid:Z

    .line 145
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "index":I
    .end local v4    # "viewIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToType:Ljava/util/Map;

    return-object v5
.end method

.method private getPositionInAdapter(ILcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "result"    # Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    .prologue
    .line 110
    const/4 v3, 0x0

    iput-object v3, p2, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .line 111
    const/4 v3, -0x1

    iput v3, p2, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    .line 113
    iget-object v3, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .line 114
    .local v0, "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    invoke-interface {v0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getCount()I

    move-result v2

    .line 115
    .local v2, "size":I
    if-eqz v2, :cond_0

    .line 117
    invoke-interface {v0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getHeader()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    add-int/lit8 p1, p1, -0x1

    .line 119
    :cond_1
    if-ge p1, v2, :cond_3

    .line 120
    iput-object v0, p2, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .line 121
    iput p1, p2, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    .line 127
    .end local v0    # "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    .end local v2    # "size":I
    :cond_2
    return-void

    .line 125
    .restart local v0    # "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    .restart local v2    # "size":I
    :cond_3
    sub-int/2addr p1, v2

    .line 126
    goto :goto_0
.end method


# virtual methods
.method public addSection(Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToTypeInvalid:Z

    .line 97
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 5

    .prologue
    .line 163
    const/4 v3, 0x0

    .line 164
    .local v3, "total":I
    iget-object v4, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .line 165
    .local v0, "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    invoke-interface {v0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getCount()I

    move-result v1

    .line 166
    .local v1, "count":I
    if-eqz v1, :cond_0

    .line 168
    add-int/2addr v3, v1

    .line 169
    invoke-interface {v0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getHeader()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 171
    .end local v0    # "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    .end local v1    # "count":I
    :cond_1
    return v3
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getPositionInAdapter(ILcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v0, v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget v0, v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    if-ltz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v0, v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    invoke-interface {v0, v1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 158
    :goto_0
    return-object v0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v0, v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    invoke-interface {v0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getHeader()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 230
    int-to-long v0, p1

    return-wide v0
.end method

.method protected getItemLayoutId(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 180
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    invoke-direct {p0, p1, v1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getPositionInAdapter(ILcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;)V

    .line 182
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    if-ltz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    iget-object v2, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget v2, v2, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    invoke-interface {v1, v2}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getItemLayoutId(I)I

    move-result v0

    .line 187
    .local v0, "layoutId":I
    :goto_0
    return v0

    .line 185
    .end local v0    # "layoutId":I
    :cond_0
    const v0, 0x7f040018

    .restart local v0    # "layoutId":I
    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 192
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getItemLayoutId(I)I

    move-result v0

    .line 193
    .local v0, "layoutId":I
    invoke-direct {p0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getLayoutsToType()Ljava/util/Map;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 194
    .local v1, "result":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 195
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 198
    :goto_0
    return v2

    .line 197
    :cond_0
    const-string v2, "MultiAdaptersListAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected layoutId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 214
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    invoke-direct {p0, p1, v1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getPositionInAdapter(ILcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;)V

    .line 215
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    if-eqz v1, :cond_1

    .line 216
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    if-ltz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    iget-object v2, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget v2, v2, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->position:I

    invoke-interface {v1, v2, p2, p3}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 225
    :goto_0
    return-object v0

    .line 219
    :cond_0
    const v1, 0x7f040018

    invoke-static {p2, p3, v1}, Lcom/google/android/music/utils/ViewUtils;->createOrReuseView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 221
    .local v0, "view":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mPosHelper:Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;

    iget-object v1, v1, Lcom/google/android/music/ui/MultiAdaptersListAdapter$PositionInAdapter;->adapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    invoke-interface {v1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;->getHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 225
    .end local v0    # "view":Landroid/widget/TextView;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getLayoutsToType()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public hasAllAdapters()Z
    .locals 3

    .prologue
    .line 103
    iget-object v2, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .line 104
    .local v0, "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    if-nez v0, :cond_0

    const/4 v2, 0x0

    .line 106
    .end local v0    # "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getItemLayoutId(I)I

    move-result v0

    const v1, 0x7f040018

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSection(ILcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "adapter"    # Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->mLayoutsToTypeInvalid:Z

    .line 91
    invoke-virtual {p0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->notifyDataSetChanged()V

    .line 92
    return-void
.end method
