.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NavbarItemClickedInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;


# instance fields
.field public section:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2133
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2134
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2137
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    .line 2134
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2149
    const/4 v0, 0x0

    .line 2150
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2151
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2154
    :cond_0
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->cachedSize:I

    .line 2155
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2163
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2164
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2168
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2169
    :sswitch_0
    return-object p0

    .line 2174
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2175
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    .line 2183
    :cond_1
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    goto :goto_0

    .line 2185
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    goto :goto_0

    .line 2164
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2130
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2142
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2143
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2145
    :cond_0
    return-void
.end method
