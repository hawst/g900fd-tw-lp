.class public Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;
.super Ljava/lang/Object;
.source "PlayTrackDocumentsClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private mTrackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)V
    .locals 0
    .param p2, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;->mTrackList:Ljava/util/List;

    .line 36
    iput-object p2, p0, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 37
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/google/android/music/ui/cardlib/model/Document;

    if-eqz v8, :cond_4

    .line 42
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 44
    .local v6, "selected":Lcom/google/android/music/ui/cardlib/model/Document;
    const/4 v5, 0x0

    .line 45
    .local v5, "position":I
    const/4 v1, 0x0

    .line 46
    .local v1, "found":Z
    iget-object v8, p0, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;->mTrackList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 47
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    if-ne v0, v6, :cond_1

    .line 49
    const/4 v1, 0x1

    .line 55
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    if-eqz v1, :cond_3

    .line 56
    iget-object v8, p0, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;->mTrackList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    new-array v4, v8, [Ljava/lang/String;

    .line 57
    .local v4, "ids":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v4

    if-ge v2, v8, :cond_2

    .line 58
    iget-object v8, p0, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;->mTrackList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v2

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 52
    .end local v2    # "i":I
    .end local v4    # "ids":[Ljava/lang/String;
    .restart local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 53
    goto :goto_0

    .line 60
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v2    # "i":I
    .restart local v4    # "ids":[Ljava/lang/String;
    :cond_2
    new-instance v7, Lcom/google/android/music/medialist/NautilusSelectedSongList;

    iget-object v8, p0, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-direct {v7, v8, v4}, Lcom/google/android/music/medialist/NautilusSelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[Ljava/lang/String;)V

    .line 63
    .local v7, "songList":Lcom/google/android/music/medialist/NautilusSelectedSongList;
    invoke-static {v7, v5}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    .line 70
    .end local v1    # "found":Z
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "ids":[Ljava/lang/String;
    .end local v5    # "position":I
    .end local v6    # "selected":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v7    # "songList":Lcom/google/android/music/medialist/NautilusSelectedSongList;
    :goto_2
    return-void

    .line 65
    .restart local v1    # "found":Z
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "position":I
    .restart local v6    # "selected":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_3
    const-string v8, "PlayTrackDocumentsClickListener"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t find the selected Document in the list: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 68
    .end local v1    # "found":Z
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "position":I
    .end local v6    # "selected":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_4
    const-string v8, "PlayTrackDocumentsClickListener"

    const-string v9, "The clicked view\'s tag wasn\'t a Document"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
