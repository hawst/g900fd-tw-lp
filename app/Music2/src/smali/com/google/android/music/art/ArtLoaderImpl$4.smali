.class Lcom/google/android/music/art/ArtLoaderImpl$4;
.super Ljava/lang/Object;
.source "ArtLoaderImpl.java"

# interfaces
.implements Lcom/google/android/music/art/ArtLoader$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtLoaderImpl;->getArtFileDescriptorSync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtLoaderImpl;

.field final synthetic val$latch:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$pfdHolder:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$4;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    iput-object p2, p0, Lcom/google/android/music/art/ArtLoaderImpl$4;->val$pfdHolder:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lcom/google/android/music/art/ArtLoaderImpl$4;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "remoteArtUrl"    # Ljava/lang/String;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$4;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 154
    return-void
.end method

.method public onSuccess(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .locals 1
    .param p1, "remoteArtUrl"    # Ljava/lang/String;
    .param p2, "pfd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$4;->val$pfdHolder:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$4;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 149
    return-void
.end method
