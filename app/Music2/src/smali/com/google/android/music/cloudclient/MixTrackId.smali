.class public Lcom/google/android/music/cloudclient/MixTrackId;
.super Ljava/lang/Object;
.source "MixTrackId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/MixTrackId$1;,
        Lcom/google/android/music/cloudclient/MixTrackId$Type;
    }
.end annotation


# instance fields
.field private final mRemoteId:Ljava/lang/String;

.field private final mType:Lcom/google/android/music/cloudclient/MixTrackId$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/music/cloudclient/MixTrackId$Type;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/android/music/cloudclient/MixTrackId$Type;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mRemoteId:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mType:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    .line 27
    return-void
.end method

.method public static createTrackId(Ljava/lang/String;I)Lcom/google/android/music/cloudclient/MixTrackId;
    .locals 3
    .param p0, "remoteId"    # Ljava/lang/String;
    .param p1, "sourceType"    # I

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    :goto_0
    return-object v1

    .line 75
    :cond_0
    sget-object v0, Lcom/google/android/music/cloudclient/MixTrackId$Type;->LOCKER:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    .line 76
    .local v0, "type":Lcom/google/android/music/cloudclient/MixTrackId$Type;
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 82
    :pswitch_0
    sget-object v0, Lcom/google/android/music/cloudclient/MixTrackId$Type;->LOCKER:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    .line 89
    :goto_1
    new-instance v1, Lcom/google/android/music/cloudclient/MixTrackId;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/cloudclient/MixTrackId;-><init>(Ljava/lang/String;Lcom/google/android/music/cloudclient/MixTrackId$Type;)V

    goto :goto_0

    .line 78
    :pswitch_1
    sget-object v0, Lcom/google/android/music/cloudclient/MixTrackId$Type;->NAUTILUS:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    .line 79
    goto :goto_1

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static trackIdTypeToServerType(Lcom/google/android/music/cloudclient/MixTrackId$Type;)I
    .locals 2
    .param p0, "type"    # Lcom/google/android/music/cloudclient/MixTrackId$Type;

    .prologue
    .line 98
    sget-object v0, Lcom/google/android/music/cloudclient/MixTrackId$1;->$SwitchMap$com$google$android$music$cloudclient$MixTrackId$Type:[I

    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/MixTrackId$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unhandled track id type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_0
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 50
    instance-of v2, p1, Lcom/google/android/music/cloudclient/MixTrackId;

    if-nez v2, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 53
    check-cast v0, Lcom/google/android/music/cloudclient/MixTrackId;

    .line 54
    .local v0, "tid":Lcom/google/android/music/cloudclient/MixTrackId;
    iget-object v2, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/MixTrackId;->getRemoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mType:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/MixTrackId;->getType()Lcom/google/android/music/cloudclient/MixTrackId$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/cloudclient/MixTrackId$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/google/android/music/cloudclient/MixTrackId$Type;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mType:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 59
    const/4 v0, 0x0

    .line 60
    .local v0, "h":I
    iget-object v1, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    add-int/2addr v0, v1

    .line 61
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mType:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    invoke-virtual {v2}, Lcom/google/android/music/cloudclient/MixTrackId$Type;->ordinal()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 62
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    iget-object v1, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    iget-object v1, p0, Lcom/google/android/music/cloudclient/MixTrackId;->mType:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 44
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
