.class Lcom/google/android/music/download/BaseDownloadQueueManager$1;
.super Ljava/lang/Thread;
.source "BaseDownloadQueueManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/BaseDownloadQueueManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/BaseDownloadQueueManager;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 45
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager$1;, "Lcom/google/android/music/download/BaseDownloadQueueManager.1;"
    iput-object p1, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 48
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager$1;, "Lcom/google/android/music/download/BaseDownloadQueueManager.1;"
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->mShutdown:Z
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$000(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 51
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$100(Lcom/google/android/music/download/BaseDownloadQueueManager;)Lcom/google/android/music/download/DownloadQueue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/download/DownloadQueue;->clearCurrentTask()V

    .line 54
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 57
    const/4 v2, 0x0

    .line 59
    .local v2, "task":Lcom/google/android/music/download/DownloadTask;
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$100(Lcom/google/android/music/download/BaseDownloadQueueManager;)Lcom/google/android/music/download/DownloadQueue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/download/DownloadQueue;->getNextTask()Lcom/google/android/music/download/DownloadTask;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 67
    :cond_1
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->mShutdown:Z
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$000(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 68
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->LOGV:Z
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$200(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 69
    const-string v3, "DownloadQueueManager"

    const-string v4, "Shutting down"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 100
    .end local v2    # "task":Lcom/google/android/music/download/DownloadTask;
    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->LOGV:Z
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$200(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 101
    const-string v3, "DownloadQueueManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Download thread finished: mShutdown="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->mShutdown:Z
    invoke-static {v5}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$000(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_3
    return-void

    .line 60
    .restart local v2    # "task":Lcom/google/android/music/download/DownloadTask;
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->LOGV:Z
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$200(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 62
    const-string v3, "DownloadQueueManager"

    const-string v4, "Interrupted:"

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 96
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "task":Lcom/google/android/music/download/DownloadTask;
    :catch_1
    move-exception v1

    .line 97
    .local v1, "t":Ljava/lang/Throwable;
    const-string v3, "DownloadQueueManager"

    const-string v4, "Unhandled exception: "

    invoke-static {v3, v4, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 75
    .end local v1    # "t":Ljava/lang/Throwable;
    .restart local v2    # "task":Lcom/google/android/music/download/DownloadTask;
    :cond_4
    if-eqz v2, :cond_0

    .line 80
    :try_start_4
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 82
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->mShutdown:Z
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$000(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 83
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->LOGV:Z
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$200(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 84
    const-string v3, "DownloadQueueManager"

    const-string v4, "We have a task but need to shutdown"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 88
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # getter for: Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;
    invoke-static {v3}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$100(Lcom/google/android/music/download/BaseDownloadQueueManager;)Lcom/google/android/music/download/DownloadQueue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/download/DownloadQueue;->clearCurrentTask()V

    goto/16 :goto_0

    .line 94
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;->this$0:Lcom/google/android/music/download/BaseDownloadQueueManager;

    # invokes: Lcom/google/android/music/download/BaseDownloadQueueManager;->runTask(Lcom/google/android/music/download/DownloadTask;)V
    invoke-static {v3, v2}, Lcom/google/android/music/download/BaseDownloadQueueManager;->access$300(Lcom/google/android/music/download/BaseDownloadQueueManager;Lcom/google/android/music/download/DownloadTask;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0
.end method
