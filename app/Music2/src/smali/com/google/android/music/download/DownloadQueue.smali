.class public Lcom/google/android/music/download/DownloadQueue;
.super Ljava/lang/Object;
.source "DownloadQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/DownloadQueue$1;,
        Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TaskType::",
        "Lcom/google/android/music/download/DownloadTask;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mCurrentTask:Lcom/google/android/music/download/DownloadTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTaskType;"
        }
    .end annotation
.end field

.field private final mDownloadThread:Ljava/lang/Thread;

.field private final mQueue:Ljava/util/AbstractQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/AbstractQueue",
            "<TTaskType;>;"
        }
    .end annotation
.end field

.field private final mQueueLock:Ljava/lang/Object;

.field private mTaskSubmissionOrder:J

.field private final mTaskSubmissionOrderMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TTaskType;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    return-void
.end method

.method constructor <init>()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    const/4 v3, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    .line 80
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xa

    new-instance v2, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;

    invoke-direct {v2, p0, v3}, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;-><init>(Lcom/google/android/music/download/DownloadQueue;Lcom/google/android/music/download/DownloadQueue$1;)V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrderMap:Ljava/util/Map;

    .line 82
    iput-object v3, p0, Lcom/google/android/music/download/DownloadQueue;->mDownloadThread:Ljava/lang/Thread;

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/Thread;)V
    .locals 4
    .param p1, "downloadThread"    # Ljava/lang/Thread;

    .prologue
    .line 92
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    if-nez p1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The download thread passed is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    .line 97
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xa

    new-instance v2, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/music/download/DownloadQueue$DownloadTaskComparator;-><init>(Lcom/google/android/music/download/DownloadQueue;Lcom/google/android/music/download/DownloadQueue$1;)V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrderMap:Ljava/util/Map;

    .line 99
    iput-object p1, p0, Lcom/google/android/music/download/DownloadQueue;->mDownloadThread:Ljava/lang/Thread;

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/DownloadQueue;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/DownloadQueue;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrderMap:Ljava/util/Map;

    return-object v0
.end method

.method private cancelCurrentTask()V
    .locals 3

    .prologue
    .line 288
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    if-nez v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    sget-boolean v0, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    if-eqz v0, :cond_2

    .line 292
    const-string v0, "DownloadQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Canceling current: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    invoke-interface {v0}, Lcom/google/android/music/download/DownloadTask;->cancel()V

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    .line 296
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mDownloadThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mDownloadThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private purgeTasks(Lcom/google/android/music/download/DownloadRequest$Owner;ILjava/util/List;)V
    .locals 8
    .param p1, "owner"    # Lcom/google/android/music/download/DownloadRequest$Owner;
    .param p2, "purgePolicy"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/DownloadRequest$Owner;",
            "I",
            "Ljava/util/List",
            "<TTaskType;>;)V"
        }
    .end annotation

    .prologue
    .line 200
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    .local p3, "tasks":Ljava/util/List;, "Ljava/util/List<TTaskType;>;"
    packed-switch p2, :pswitch_data_0

    .line 241
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 203
    :pswitch_1
    iget-object v5, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v5}, Ljava/util/AbstractQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 204
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TTaskType;>;"
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 205
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/DownloadTask;

    .line 206
    .local v4, "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    invoke-interface {v4}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/download/DownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v5

    if-ne v5, p1, :cond_1

    .line 207
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 214
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TTaskType;>;"
    .end local v4    # "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    :pswitch_2
    if-eqz p3, :cond_5

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_5

    .line 216
    const/4 v5, 0x0

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/DownloadTask;

    invoke-interface {v5}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v0

    .line 217
    .local v0, "highestPriority":I
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/DownloadTask;

    .line 218
    .local v3, "t":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    invoke-interface {v3}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v5

    if-ge v5, v0, :cond_2

    .line 219
    invoke-interface {v3}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v0

    goto :goto_2

    .line 223
    .end local v3    # "t":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v5}, Ljava/util/AbstractQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 224
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TTaskType;>;"
    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 225
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/DownloadTask;

    .line 226
    .restart local v4    # "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    invoke-interface {v4}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/download/DownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v5

    if-ne v5, p1, :cond_4

    invoke-interface {v4}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v5

    if-lt v5, v0, :cond_4

    .line 228
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 232
    .end local v0    # "highestPriority":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<TTaskType;>;"
    .end local v4    # "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    :cond_5
    const-string v5, "DownloadQueue"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "List of tasks required for purge policy "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static queueToString(Ljava/util/AbstractQueue;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TaskType::",
            "Lcom/google/android/music/download/DownloadTask;",
            ">(",
            "Ljava/util/AbstractQueue",
            "<TTaskType;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 317
    .local p0, "queue":Ljava/util/AbstractQueue;, "Ljava/util/AbstractQueue<TTaskType;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 318
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v4, "["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    invoke-virtual {p0}, Ljava/util/AbstractQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/DownloadTask;

    .line 320
    .local v3, "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 321
    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 323
    .end local v3    # "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 324
    .local v2, "length":I
    const/4 v4, 0x2

    if-le v2, v4, :cond_1

    .line 325
    add-int/lit8 v4, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 327
    :cond_1
    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public addTasks(Ljava/util/List;I)Z
    .locals 10
    .param p2, "purgePolicy"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TTaskType;>;I)Z"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<TTaskType;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 111
    :cond_0
    sget-boolean v4, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    if-eqz v4, :cond_1

    .line 112
    const-string v4, "DownloadQueue"

    const-string v5, "addTasks: Empty list of tasks provided"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    const/4 v4, 0x0

    .line 157
    :goto_0
    return v4

    .line 118
    :cond_2
    const/4 v4, 0x0

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/DownloadTask;

    invoke-interface {v4}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/music/download/DownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    .line 120
    .local v1, "owner":Lcom/google/android/music/download/DownloadRequest$Owner;
    iget-object v5, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    monitor-enter v5

    .line 123
    :try_start_0
    invoke-direct {p0, v1, p2, p1}, Lcom/google/android/music/download/DownloadQueue;->purgeTasks(Lcom/google/android/music/download/DownloadRequest$Owner;ILjava/util/List;)V

    .line 126
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/DownloadTask;

    .line 128
    .local v2, "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    iget-wide v6, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrder:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-gez v4, :cond_3

    .line 129
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrder:J

    .line 131
    :cond_3
    iget-object v4, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrderMap:Ljava/util/Map;

    iget-wide v6, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrder:J

    const-wide/16 v8, 0x1

    add-long/2addr v8, v6

    iput-wide v8, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrder:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v4, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v4, v2}, Ljava/util/AbstractQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 155
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "task":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 136
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_1
    iget-object v4, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v4}, Ljava/util/AbstractQueue;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/DownloadTask;

    .line 137
    .local v3, "topTask":Lcom/google/android/music/download/DownloadTask;, "TTaskType;"
    if-eqz v3, :cond_5

    iget-object v4, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    if-eqz v4, :cond_5

    .line 138
    invoke-interface {v3}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v4

    iget-object v6, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    invoke-interface {v6}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v6

    if-gt v4, v6, :cond_5

    .line 139
    iget-object v4, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    invoke-interface {v4, v3}, Lcom/google/android/music/download/DownloadTask;->upgrade(Lcom/google/android/music/download/DownloadTask;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 141
    iget-object v4, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v4}, Ljava/util/AbstractQueue;->poll()Ljava/lang/Object;

    .line 151
    :cond_5
    :goto_2
    sget-boolean v4, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    if-eqz v4, :cond_6

    .line 152
    const-string v4, "DownloadQueue"

    const-string v6, "addTasks: queue=%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-static {v9}, Lcom/google/android/music/download/DownloadQueue;->queueToString(Ljava/util/AbstractQueue;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_6
    iget-object v4, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 155
    monitor-exit v5

    .line 157
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 144
    :cond_7
    const/4 v4, 0x3

    if-eq p2, v4, :cond_5

    .line 145
    invoke-direct {p0}, Lcom/google/android/music/download/DownloadQueue;->cancelCurrentTask()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public cancelAndPurge(Lcom/google/android/music/download/DownloadRequest$Owner;I)V
    .locals 5
    .param p1, "owner"    # Lcom/google/android/music/download/DownloadRequest$Owner;
    .param p2, "purgePolicy"    # I

    .prologue
    .line 168
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    sget-boolean v0, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    if-eqz v0, :cond_0

    .line 169
    const-string v0, "DownloadQueue"

    const-string v1, "cancelAndPurge: owner=%s, policy=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    monitor-enter v1

    .line 173
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    invoke-interface {v0}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/download/DownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v0

    if-ne v0, p1, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    .line 175
    invoke-direct {p0}, Lcom/google/android/music/download/DownloadQueue;->cancelCurrentTask()V

    .line 178
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/download/DownloadQueue;->purgeTasks(Lcom/google/android/music/download/DownloadRequest$Owner;ILjava/util/List;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 181
    monitor-exit v1

    .line 182
    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method clearCurrentTask()V
    .locals 2

    .prologue
    .line 276
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    sget-boolean v0, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    if-eqz v0, :cond_0

    .line 277
    const-string v0, "DownloadQueue"

    const-string v1, "clearCurrentTask"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    monitor-enter v1

    .line 280
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    .line 281
    monitor-exit v1

    .line 282
    return-void

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getNextTask()Lcom/google/android/music/download/DownloadTask;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTaskType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 251
    .local p0, "this":Lcom/google/android/music/download/DownloadQueue;, "Lcom/google/android/music/download/DownloadQueue<TTaskType;>;"
    iget-object v1, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    monitor-enter v1

    .line 252
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v0}, Ljava/util/AbstractQueue;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 253
    sget-boolean v0, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    if-eqz v0, :cond_0

    .line 254
    const-string v0, "DownloadQueue"

    const-string v2, "getNextTask(): wait"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueueLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 259
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v0}, Ljava/util/AbstractQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/DownloadTask;

    iput-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    .line 260
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mTaskSubmissionOrderMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-boolean v0, Lcom/google/android/music/download/DownloadQueue;->LOGV:Z

    if-eqz v0, :cond_2

    .line 263
    const-string v0, "DownloadQueue"

    const-string v2, "currentTask=%s mQueue.size=%d queue=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-virtual {v5}, Ljava/util/AbstractQueue;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/music/download/DownloadQueue;->mQueue:Ljava/util/AbstractQueue;

    invoke-static {v5}, Lcom/google/android/music/download/DownloadQueue;->queueToString(Ljava/util/AbstractQueue;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268
    iget-object v0, p0, Lcom/google/android/music/download/DownloadQueue;->mCurrentTask:Lcom/google/android/music/download/DownloadTask;

    return-object v0
.end method
