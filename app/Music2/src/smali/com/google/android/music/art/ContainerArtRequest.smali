.class public Lcom/google/android/music/art/ContainerArtRequest;
.super Lcom/google/android/music/art/ArtRequest;
.source "ContainerArtRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/art/ArtRequest",
        "<",
        "Lcom/google/android/music/art/ContainerMixDescriptorPair;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ContainerMixDescriptorPair;Lcom/google/android/music/art/ArtType;)V
    .locals 0
    .param p1, "pair"    # Lcom/google/android/music/art/ContainerMixDescriptorPair;
    .param p2, "artType"    # Lcom/google/android/music/art/ArtType;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtRequest;-><init>(Ljava/lang/Object;Lcom/google/android/music/art/ArtType;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/art/ArtType;)V
    .locals 1
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "artType"    # Lcom/google/android/music/art/ArtType;

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;

    invoke-direct {v0, p1, p2}, Lcom/google/android/music/art/ContainerMixDescriptorPair;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/music/art/ContainerArtRequest;-><init>(Lcom/google/android/music/art/ContainerMixDescriptorPair;Lcom/google/android/music/art/ArtType;)V

    .line 18
    return-void
.end method


# virtual methods
.method public container()Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/music/art/ContainerArtRequest;->mDescriptor:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;

    iget-object v0, v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method
