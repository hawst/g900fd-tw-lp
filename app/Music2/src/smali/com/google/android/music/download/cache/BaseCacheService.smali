.class abstract Lcom/google/android/music/download/cache/BaseCacheService;
.super Landroid/app/Service;
.source "BaseCacheService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/cache/BaseCacheService$StorageLowBroadcastReceiver;,
        Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private volatile mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;

.field private mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

.field private volatile mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private final mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/BaseCacheService;->LOGV:Z

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    const-string v1, "CacheService"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;-><init>(Lcom/google/android/music/download/cache/BaseCacheService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    .line 87
    new-instance v0, Lcom/google/android/music/download/cache/BaseCacheService$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/cache/BaseCacheService$1;-><init>(Lcom/google/android/music/download/cache/BaseCacheService;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/cache/BaseCacheService;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheService;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/BaseCacheService;->handleClearOrphaned(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/download/cache/BaseCacheService;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheService;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/BaseCacheService;->handleClearCache(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/download/cache/BaseCacheService;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheService;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/BaseCacheService;->handleLowStorage(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/download/cache/BaseCacheService;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheService;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/download/cache/BaseCacheService;)Lcom/google/android/music/download/cache/BaseCacheManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheService;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/download/cache/BaseCacheService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheService;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheService;->cancelClearCacheMessage()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/download/cache/BaseCacheService;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheService;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/download/cache/BaseCacheService;->sendClearCachedMessage(IJ)V

    return-void
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 24
    sget-boolean v0, Lcom/google/android/music/download/cache/BaseCacheService;->LOGV:Z

    return v0
.end method

.method private cancelClearCacheMessage()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->removeMessages(I)V

    .line 187
    return-void
.end method

.method private handleClearCache(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/BaseCacheManager;->clearCachedFiles()V

    .line 216
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 217
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/cache/BaseCacheService;->stopSelf(I)V

    .line 219
    :cond_0
    return-void
.end method

.method private handleClearOrphaned(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/BaseCacheManager;->clearOrphanedFiles()V

    .line 209
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 210
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/cache/BaseCacheService;->stopSelf(I)V

    .line 212
    :cond_0
    return-void
.end method

.method private handleLowStorage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/BaseCacheManager;->handleLowStorage()V

    .line 223
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 224
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/cache/BaseCacheService;->stopSelf(I)V

    .line 226
    :cond_0
    return-void
.end method

.method private sendClearCachedMessage(IJ)V
    .locals 4
    .param p1, "startId"    # I
    .param p2, "delayMs"    # J

    .prologue
    .line 196
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 197
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 198
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 199
    return-void
.end method

.method private sendClearOrphanedMessage(I)V
    .locals 3
    .param p1, "startId"    # I

    .prologue
    .line 190
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 191
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 192
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->sendMessage(Landroid/os/Message;)Z

    .line 193
    return-void
.end method

.method private sendLowStorageMessage(I)V
    .locals 3
    .param p1, "startId"    # I

    .prologue
    .line 202
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 203
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 204
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->sendMessage(Landroid/os/Message;)Z

    .line 205
    return-void
.end method


# virtual methods
.method protected abstract createCacheManager(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/download/cache/BaseCacheManager;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 156
    sget-boolean v0, Lcom/google/android/music/download/cache/BaseCacheService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "CacheService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 135
    sget-boolean v0, Lcom/google/android/music/download/cache/BaseCacheService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "CacheService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_0
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 139
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/cache/BaseCacheService;->createCacheManager(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/download/cache/BaseCacheManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;

    .line 140
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 141
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 146
    sget-boolean v0, Lcom/google/android/music/download/cache/BaseCacheService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "CacheService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mCacheWorker:Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->quit()V

    .line 150
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 151
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 152
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 166
    sget-boolean v1, Lcom/google/android/music/download/cache/BaseCacheService;->LOGV:Z

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "CacheService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "action":Ljava/lang/String;
    :goto_0
    const-string v1, "com.google.android.music.download.cache.CacheService.CLEAR_ORPHANED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 173
    invoke-direct {p0, p3}, Lcom/google/android/music/download/cache/BaseCacheService;->sendClearOrphanedMessage(I)V

    .line 182
    :goto_1
    const/4 v1, 0x3

    return v1

    .line 170
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 174
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    const-string v1, "com.google.android.music.download.cache.CacheService.CLEAR_CACHE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 175
    const-wide/16 v2, 0x0

    invoke-direct {p0, p3, v2, v3}, Lcom/google/android/music/download/cache/BaseCacheService;->sendClearCachedMessage(IJ)V

    goto :goto_1

    .line 176
    :cond_3
    const-string v1, "com.google.android.music.download.cache.CacheService.HANDLE_LOW_STORAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 177
    invoke-direct {p0, p3}, Lcom/google/android/music/download/cache/BaseCacheService;->sendLowStorageMessage(I)V

    goto :goto_1

    .line 179
    :cond_4
    const-string v1, "CacheService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0, p3}, Lcom/google/android/music/download/cache/BaseCacheService;->stopSelf(I)V

    goto :goto_1
.end method
