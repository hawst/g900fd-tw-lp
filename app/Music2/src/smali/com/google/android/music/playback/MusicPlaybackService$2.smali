.class Lcom/google/android/music/playback/MusicPlaybackService$2;
.super Landroid/content/BroadcastReceiver;
.source "MusicPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$2;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 207
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 208
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    .line 211
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 212
    const-string v2, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService$2;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v2}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/DevicePlayback;->isPlayingLocally()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 214
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.android.music.musicservicecommand"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const-string v2, "command"

    const-string v3, "pause"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    const-string v2, "device"

    const-string v3, "local"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService$2;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v2, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 221
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method
