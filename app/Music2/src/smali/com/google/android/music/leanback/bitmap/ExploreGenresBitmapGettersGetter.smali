.class public Lcom/google/android/music/leanback/bitmap/ExploreGenresBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;
.source "ExploreGenresBitmapGettersGetter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J

    .prologue
    .line 19
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 20
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/ExploreGenresBitmapGettersGetter;->startLoading()V

    .line 21
    return-void
.end method


# virtual methods
.method protected addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    const/4 v2, 0x0

    .line 36
    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 37
    .local v0, "artUrl":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;

    invoke-direct {v1, v0}, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    return-void

    .line 36
    .end local v0    # "artUrl":Ljava/lang/String;
    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 15
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/music/ui/GenreExploreList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/GenreExploreList;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "genreArtUris"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 15
    invoke-super {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    return-void
.end method

.method public bridge synthetic startLoading()V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->startLoading()V

    return-void
.end method
