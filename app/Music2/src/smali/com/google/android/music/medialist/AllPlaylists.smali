.class public Lcom/google/android/music/medialist/AllPlaylists;
.super Lcom/google/android/music/medialist/PlaylistsList;
.source "AllPlaylists.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/AllPlaylists;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/music/medialist/AllPlaylists$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllPlaylists$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/AllPlaylists;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/music/medialist/PlaylistsList;-><init>()V

    .line 15
    return-void
.end method


# virtual methods
.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
