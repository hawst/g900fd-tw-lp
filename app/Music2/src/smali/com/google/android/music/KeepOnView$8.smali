.class Lcom/google/android/music/KeepOnView$8;
.super Ljava/lang/Object;
.source "KeepOnView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/KeepOnView;->handleOnKeepOnClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final mAppContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/music/KeepOnView;


# direct methods
.method constructor <init>(Lcom/google/android/music/KeepOnView;)V
    .locals 1

    .prologue
    .line 575
    iput-object p1, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView$8;->mAppContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 581
    iget-object v1, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    # getter for: Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/KeepOnView;->access$100(Lcom/google/android/music/KeepOnView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .line 582
    .local v0, "publicPlaylist":Lcom/google/android/music/medialist/SharedWithMeSongList;
    iget-object v1, p0, Lcom/google/android/music/KeepOnView$8;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->canFollow(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 583
    iget-object v1, p0, Lcom/google/android/music/KeepOnView$8;->mAppContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/music/ui/FollowPlaylistButton;->followPlaylist(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V

    .line 585
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 589
    iget-object v1, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v1}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v1}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/music/ui/BaseActivity;

    if-eqz v1, :cond_2

    .line 590
    iget-object v1, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v1}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseActivity;

    .line 591
    .local v0, "activity":Lcom/google/android/music/ui/BaseActivity;
    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    # getter for: Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/KeepOnView;->access$100(Lcom/google/android/music/KeepOnView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    iget-object v3, p0, Lcom/google/android/music/KeepOnView$8;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleSonglistKeepOn(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    goto :goto_0

    .line 595
    .end local v0    # "activity":Lcom/google/android/music/ui/BaseActivity;
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "KeepOnView is not added to BaseActivity"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
