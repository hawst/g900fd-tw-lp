.class public abstract Lcom/google/android/music/download/BaseDownloadTask;
.super Ljava/lang/Object;
.source "BaseDownloadTask.java"

# interfaces
.implements Lcom/google/android/music/download/DownloadTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DownloadRequestType::",
        "Lcom/google/android/music/download/DownloadRequest;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/music/download/DownloadTask",
        "<TDownloadRequestType;>;"
    }
.end annotation


# instance fields
.field private final LOGV:Z

.field private final mContext:Landroid/content/Context;

.field private final mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

.field private final mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDownloadRequestType;"
        }
    .end annotation
.end field

.field private final mDownloadState:Lcom/google/android/music/download/DownloadState;

.field private final mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

.field private final mDownloadingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mDownloadingEnabledChangedTime:J

.field private final mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private final mLock:Ljava/lang/Object;

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

.field private mServerSpecifiedRetryTime:J

.field private mStarted:Z

.field private volatile mStopDownload:Z

.field private final mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

.field private final mStreamingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mStreamingEnabledChangedTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "downloadProgressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p4, "musicPreferences"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p5, "networkMonitorServiceConnection"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TDownloadRequestType;",
            "Lcom/google/android/music/download/IDownloadProgressListener;",
            "Lcom/google/android/music/preferences/MusicPreferences;",
            "Lcom/google/android/music/net/NetworkMonitorServiceConnection;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    .local p2, "downloadRequest":Lcom/google/android/music/download/DownloadRequest;, "TDownloadRequestType;"
    const/4 v4, 0x1

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    .line 91
    iput-boolean v1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStopDownload:Z

    .line 99
    iput-boolean v1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStarted:Z

    .line 101
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 102
    iput-wide v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabledChangedTime:J

    .line 103
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 104
    iput-wide v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabledChangedTime:J

    .line 105
    iput-wide v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mServerSpecifiedRetryTime:J

    .line 107
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;

    .line 353
    new-instance v0, Lcom/google/android/music/download/BaseDownloadTask$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/BaseDownloadTask$1;-><init>(Lcom/google/android/music/download/BaseDownloadTask;)V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    .line 368
    new-instance v0, Lcom/google/android/music/download/BaseDownloadTask$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/BaseDownloadTask$2;-><init>(Lcom/google/android/music/download/BaseDownloadTask;)V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    .line 113
    iput-object p1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mContext:Landroid/content/Context;

    .line 114
    invoke-static {p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 115
    iput-object p2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    .line 116
    new-instance v0, Lcom/google/android/music/download/DownloadState;

    invoke-direct {v0}, Lcom/google/android/music/download/DownloadState;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    .line 117
    iput-object p3, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    .line 118
    iput-object p5, p0, Lcom/google/android/music/download/BaseDownloadTask;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 119
    iput-object p4, p0, Lcom/google/android/music/download/BaseDownloadTask;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadTask;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadTask;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/download/BaseDownloadTask;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadTask;
    .param p1, "x1"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabledChangedTime:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadTask;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/music/download/BaseDownloadTask;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadTask;
    .param p1, "x1"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabledChangedTime:J

    return-wide p1
.end method

.method private handleRun()V
    .locals 34

    .prologue
    .line 157
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;

    move-object/from16 v26, v0

    monitor-enter v26

    .line 158
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mStarted:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 159
    new-instance v19, Ljava/lang/RuntimeException;

    const-string v27, "The same DownloadTask should never be ran twice"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 162
    :catchall_0
    move-exception v19

    monitor-exit v26
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v19

    .line 161
    :cond_0
    const/16 v19, 0x1

    :try_start_1
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/music/download/BaseDownloadTask;->mStarted:Z

    .line 162
    monitor-exit v26
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v26, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/Store;->requiresDownload(Lcom/google/android/music/download/DownloadRequest;)Landroid/util/Pair;

    move-result-object v18

    .line 165
    .local v18, "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Long;>;"
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-nez v19, :cond_3

    .line 166
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 167
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Track already downloaded. Not starting download for request: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/BaseDownloadTask;->updateCompleted()V

    .line 347
    :cond_2
    :goto_0
    return-void

    .line 174
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v19, v0

    invoke-static {}, Lcom/google/android/music/net/NetworkMonitor;->getRecommendedBitrate()I

    move-result v26

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/DownloadState;->setRecommendedBitrate(I)V

    .line 176
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/BaseDownloadTask;->updateDownloading()V

    .line 178
    const/4 v7, 0x1

    .line 179
    .local v7, "downloadStatus":I
    const-wide/16 v22, 0x0

    .line 180
    .local v22, "transferTime":J
    const-wide/16 v8, 0x0

    .line 181
    .local v8, "downloadLength":J
    const/4 v13, 0x0

    .line 182
    .local v13, "failedAttemptsWithNoProgress":I
    const-wide/16 v24, 0x3e8

    .line 183
    .local v24, "waitBeforeRetryingMs":J
    const/4 v4, 0x1

    .line 186
    .local v4, "continueRetries":Z
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v16

    .line 187
    .local v16, "monitor":Lcom/google/android/music/net/INetworkMonitor;
    if-eqz v16, :cond_4

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/google/android/music/net/INetworkMonitor;->registerStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/google/android/music/net/INetworkMonitor;->registerDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 196
    .end local v16    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    :cond_4
    :goto_1
    const/4 v5, 0x1

    .local v5, "downloadAttempt":I
    move v6, v5

    .line 197
    .end local v5    # "downloadAttempt":I
    .local v6, "downloadAttempt":I
    :goto_2
    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mStopDownload:Z

    move/from16 v19, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    if-nez v19, :cond_1c

    if-eqz v4, :cond_1c

    .line 199
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1b

    .line 200
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Attempt #"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result-object v26

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "downloadAttempt":I
    .restart local v5    # "downloadAttempt":I
    :try_start_5
    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " to download "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 204
    .local v20, "startTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-wide v14

    .line 206
    .local v14, "initialCompleted":J
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/BaseDownloadTask;->canDownload()Z

    move-result v19

    if-eqz v19, :cond_a

    .line 207
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/BaseDownloadTask;->download()I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result v7

    .line 219
    :goto_4
    :try_start_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    sub-long v22, v26, v20

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v26

    sub-long v8, v26, v14

    .line 221
    const-wide/16 v26, 0x0

    cmp-long v19, v8, v26

    if-lez v19, :cond_5

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-static {v0, v8, v9, v1, v2}, Lcom/google/android/music/net/NetworkMonitor;->reportBitrate(Landroid/content/Context;JJ)V

    .line 226
    :cond_5
    :goto_5
    const/16 v17, 0x0

    .line 227
    .local v17, "nonRetriableFailure":Z
    const/16 v19, 0x4

    move/from16 v0, v19

    if-eq v7, v0, :cond_6

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/music/download/DownloadRequest;->isRetryAllowed()Z

    move-result v19

    if-nez v19, :cond_e

    const/16 v17, 0x1

    .line 230
    :cond_6
    :goto_6
    if-eqz v17, :cond_f

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/DownloadState;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v19

    sget-object v26, Lcom/google/android/music/download/DownloadState$State;->FAILED:Lcom/google/android/music/download/DownloadState$State;

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_7

    .line 234
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/BaseDownloadTask;->updateFailed(I)V

    .line 236
    :cond_7
    const/4 v4, 0x0

    .line 324
    :cond_8
    :goto_7
    if-eqz v4, :cond_1a

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v19

    if-eqz v19, :cond_1a

    .line 325
    new-instance v19, Ljava/lang/InterruptedException;

    invoke-direct/range {v19 .. v19}, Ljava/lang/InterruptedException;-><init>()V

    throw v19
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 327
    .end local v14    # "initialCompleted":J
    .end local v17    # "nonRetriableFailure":Z
    .end local v20    # "startTime":J
    :catch_0
    move-exception v10

    .line 328
    .local v10, "e":Ljava/lang/InterruptedException;
    :goto_8
    :try_start_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    move/from16 v19, v0

    if-eqz v19, :cond_9

    .line 329
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Download ("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ") was interupted.  Exiting."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_9
    const/4 v4, 0x0

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/DownloadState;->setExperiencedGlitch()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 338
    .end local v10    # "e":Ljava/lang/InterruptedException;
    :goto_9
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v16

    .line 339
    .restart local v16    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    if-eqz v16, :cond_2

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/google/android/music/net/INetworkMonitor;->unregisterStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/google/android/music/net/INetworkMonitor;->unregisterDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_1

    goto/16 :goto_0

    .line 343
    .end local v16    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    :catch_1
    move-exception v11

    .line 344
    .local v11, "e1":Landroid/os/RemoteException;
    const-string v19, "DownloadTaskImpl"

    invoke-virtual {v11}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 191
    .end local v5    # "downloadAttempt":I
    .end local v11    # "e1":Landroid/os/RemoteException;
    :catch_2
    move-exception v12

    .line 192
    .local v12, "e2":Landroid/os/RemoteException;
    const-string v19, "DownloadTaskImpl"

    invoke-virtual {v12}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1, v12}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 209
    .end local v12    # "e2":Landroid/os/RemoteException;
    .restart local v5    # "downloadAttempt":I
    .restart local v14    # "initialCompleted":J
    .restart local v20    # "startTime":J
    :cond_a
    :try_start_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    move/from16 v19, v0

    if-eqz v19, :cond_b

    .line 210
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Not attempting download - not enabled for DownloadRequest.Owner="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Lcom/google/android/music/download/DownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 214
    :cond_b
    const/4 v7, 0x1

    goto/16 :goto_4

    .line 216
    :catch_3
    move-exception v10

    .line 217
    .local v10, "e":Ljava/io/IOException;
    const/4 v7, 0x1

    .line 219
    :try_start_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    sub-long v22, v26, v20

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v26

    sub-long v8, v26, v14

    .line 221
    const-wide/16 v26, 0x0

    cmp-long v19, v8, v26

    if-lez v19, :cond_5

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-static {v0, v8, v9, v1, v2}, Lcom/google/android/music/net/NetworkMonitor;->reportBitrate(Landroid/content/Context;JJ)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_5

    .line 337
    .end local v10    # "e":Ljava/io/IOException;
    .end local v14    # "initialCompleted":J
    .end local v20    # "startTime":J
    :catchall_1
    move-exception v19

    .line 338
    :goto_a
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v16

    .line 339
    .restart local v16    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    if-eqz v16, :cond_c

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamabilityChangeListener:Lcom/google/android/music/net/IStreamabilityChangeListener;

    move-object/from16 v26, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/google/android/music/net/INetworkMonitor;->unregisterStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadabilityChangeListener:Lcom/google/android/music/net/IDownloadabilityChangeListener;

    move-object/from16 v26, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/google/android/music/net/INetworkMonitor;->unregisterDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_4

    .line 345
    .end local v16    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    :cond_c
    :goto_b
    throw v19

    .line 219
    .restart local v14    # "initialCompleted":J
    .restart local v20    # "startTime":J
    :catchall_2
    move-exception v19

    :try_start_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    sub-long v22, v26, v20

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v26

    sub-long v8, v26, v14

    .line 221
    const-wide/16 v26, 0x0

    cmp-long v26, v8, v26

    if-lez v26, :cond_d

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-wide/from16 v1, v22

    invoke-static {v0, v8, v9, v1, v2}, Lcom/google/android/music/net/NetworkMonitor;->reportBitrate(Landroid/content/Context;JJ)V

    :cond_d
    throw v19

    .line 228
    .restart local v17    # "nonRetriableFailure":Z
    :cond_e
    const/16 v17, 0x0

    goto/16 :goto_6

    .line 238
    :cond_f
    const-wide/16 v24, 0x3e8

    .line 239
    packed-switch v7, :pswitch_data_0

    .line 277
    new-instance v19, Ljava/lang/IllegalStateException;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Unknown Download status: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 241
    :pswitch_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/BaseDownloadTask;->updateCompleted()V

    .line 242
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    move/from16 v19, v0

    if-eqz v19, :cond_10

    .line 243
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Download suceeded for: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " Avg speed: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-wide/16 v28, 0x8

    mul-long v28, v28, v8

    move-wide/from16 v0, v28

    long-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide v30, 0x408f400000000000L    # 1000.0

    div-double v28, v28, v30

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v30, v0

    const-wide v32, 0x408f400000000000L    # 1000.0

    div-double v30, v30, v32

    div-double v28, v28, v30

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " kbps"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_10
    const/4 v4, 0x0

    .line 281
    :goto_c
    if-eqz v4, :cond_8

    .line 282
    const-wide/16 v26, 0x0

    cmp-long v19, v24, v26

    if-gtz v19, :cond_14

    .line 283
    new-instance v19, Ljava/lang/IllegalArgumentException;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "waitBeforeRetryingMs was not initialized to a valid value: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 251
    :pswitch_1
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Download failed for: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const/4 v4, 0x0

    .line 253
    goto :goto_c

    .line 256
    :pswitch_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mServerSpecifiedRetryTime:J

    move-wide/from16 v26, v0

    const-wide/16 v28, 0x0

    cmp-long v19, v26, v28

    if-lez v19, :cond_11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mServerSpecifiedRetryTime:J

    move-wide/from16 v26, v0

    const-wide/16 v28, 0x4e20

    cmp-long v19, v26, v28

    if-lez v19, :cond_12

    .line 258
    :cond_11
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Server said to wait too long: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mServerSpecifiedRetryTime:J

    move-wide/from16 v28, v0

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const/16 v19, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/BaseDownloadTask;->updateFailed(I)V

    .line 261
    const/4 v4, 0x0

    goto/16 :goto_c

    .line 263
    :cond_12
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mServerSpecifiedRetryTime:J

    move-wide/from16 v24, v0

    .line 265
    goto/16 :goto_c

    .line 267
    :pswitch_3
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/download/BaseDownloadTask;->networkChangedDuringDownload(J)Z

    move-result v19

    if-eqz v19, :cond_13

    .line 268
    const-wide/16 v24, 0x1

    goto/16 :goto_c

    .line 270
    :cond_13
    const-wide/16 v24, 0x3e8

    .line 272
    goto/16 :goto_c

    .line 274
    :pswitch_4
    const-wide/16 v24, 0x3e8

    .line 275
    goto/16 :goto_c

    .line 287
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/DownloadState;->setExperiencedGlitch()V

    .line 289
    const-string v19, "DownloadTaskImpl"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Experienced error when trying to download: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-wide/16 v26, 0x400

    cmp-long v19, v8, v26

    if-gez v19, :cond_15

    .line 293
    add-int/lit8 v13, v13, 0x1

    .line 294
    const/16 v19, 0x5

    move/from16 v0, v19

    if-lt v13, v0, :cond_16

    .line 295
    const-string v19, "DownloadTaskImpl"

    const-string v26, "Too many failures with no download progress"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const/16 v19, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/BaseDownloadTask;->updateFailed(I)V

    goto/16 :goto_9

    .line 300
    :cond_15
    const/4 v13, 0x0

    .line 302
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;

    move-object/from16 v26, v0

    monitor-enter v26
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 303
    :try_start_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/BaseDownloadTask;->canDownload()Z

    move-result v19

    if-eqz v19, :cond_18

    .line 307
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    move/from16 v19, v0

    if-eqz v19, :cond_17

    const-wide/16 v28, 0x3e8

    cmp-long v19, v24, v28

    if-lez v19, :cond_17

    .line 308
    const-string v19, "DownloadTaskImpl"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Waiting "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-wide/16 v28, 0x3e8

    div-long v28, v24, v28

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " seconds before retrying"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V

    .line 320
    :goto_d
    monitor-exit v26

    goto/16 :goto_7

    :catchall_3
    move-exception v19

    monitor-exit v26
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :try_start_f
    throw v19
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 314
    :cond_18
    :try_start_10
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    move/from16 v19, v0

    if-eqz v19, :cond_19

    .line 315
    const-string v19, "DownloadTaskImpl"

    const-string v27, "Waiting until streaming comes backonline (or gets interrupted)"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;

    move-object/from16 v19, v0

    const-wide/16 v28, 0x2710

    move-object/from16 v0, v19

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    goto :goto_d

    :cond_1a
    move v6, v5

    .line 334
    .end local v5    # "downloadAttempt":I
    .restart local v6    # "downloadAttempt":I
    goto/16 :goto_2

    .line 343
    .end local v6    # "downloadAttempt":I
    .end local v14    # "initialCompleted":J
    .end local v17    # "nonRetriableFailure":Z
    .end local v20    # "startTime":J
    .restart local v5    # "downloadAttempt":I
    :catch_4
    move-exception v11

    .line 344
    .restart local v11    # "e1":Landroid/os/RemoteException;
    const-string v26, "DownloadTaskImpl"

    invoke-virtual {v11}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_b

    .line 337
    .end local v5    # "downloadAttempt":I
    .end local v11    # "e1":Landroid/os/RemoteException;
    .restart local v6    # "downloadAttempt":I
    :catchall_4
    move-exception v19

    move v5, v6

    .end local v6    # "downloadAttempt":I
    .restart local v5    # "downloadAttempt":I
    goto/16 :goto_a

    .line 327
    .end local v5    # "downloadAttempt":I
    .restart local v6    # "downloadAttempt":I
    :catch_5
    move-exception v10

    move v5, v6

    .end local v6    # "downloadAttempt":I
    .restart local v5    # "downloadAttempt":I
    goto/16 :goto_8

    .end local v5    # "downloadAttempt":I
    .restart local v6    # "downloadAttempt":I
    :cond_1b
    move v5, v6

    .end local v6    # "downloadAttempt":I
    .restart local v5    # "downloadAttempt":I
    goto/16 :goto_3

    .end local v5    # "downloadAttempt":I
    .restart local v6    # "downloadAttempt":I
    :cond_1c
    move v5, v6

    .end local v6    # "downloadAttempt":I
    .restart local v5    # "downloadAttempt":I
    goto/16 :goto_9

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateCanceled()V
    .locals 1

    .prologue
    .line 389
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v0}, Lcom/google/android/music/download/DownloadState;->setCanceledState()V

    .line 390
    return-void
.end method

.method private updateCompleted()V
    .locals 1

    .prologue
    .line 393
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v0}, Lcom/google/android/music/download/DownloadState;->setCompletedState()V

    .line 394
    return-void
.end method

.method private updateDownloading()V
    .locals 1

    .prologue
    .line 384
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v0}, Lcom/google/android/music/download/DownloadState;->setDownloadingState()V

    .line 385
    invoke-direct {p0}, Lcom/google/android/music/download/BaseDownloadTask;->updateProgress()V

    .line 386
    return-void
.end method

.method private updateProgress()V
    .locals 7

    .prologue
    .line 402
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/download/BaseDownloadTask;->createDownloadProgress(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;

    move-result-object v1

    .line 403
    .local v1, "progress":Lcom/google/android/music/download/DownloadProgress;
    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    invoke-interface {v2, v1}, Lcom/google/android/music/download/IDownloadProgressListener;->onDownloadProgress(Lcom/google/android/music/download/DownloadProgress;)V

    .line 404
    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v2}, Lcom/google/android/music/download/DownloadState;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/DownloadState$State;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 405
    iget-boolean v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->LOGV:Z

    if-eqz v2, :cond_0

    .line 406
    const-string v2, "DownloadTaskImpl"

    const-string v3, "Download finished: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    .end local v1    # "progress":Lcom/google/android/music/download/DownloadProgress;
    :cond_0
    :goto_0
    return-void

    .line 409
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "DownloadTaskImpl"

    const-string v3, "Failed to call download progress callback"

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract canDownload()Z
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 138
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStopDownload:Z

    .line 139
    invoke-direct {p0}, Lcom/google/android/music/download/BaseDownloadTask;->updateCanceled()V

    .line 140
    return-void
.end method

.method protected abstract createDownloadProgress(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDownloadRequestType;",
            "Lcom/google/android/music/download/DownloadState;",
            ")",
            "Lcom/google/android/music/download/DownloadProgress;"
        }
    .end annotation
.end method

.method protected abstract download()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 455
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected getDownloadProgressListener()Lcom/google/android/music/download/IDownloadProgressListener;
    .locals 1

    .prologue
    .line 443
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    return-object v0
.end method

.method public getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDownloadRequestType;"
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    return-object v0
.end method

.method protected getDownloadState()Lcom/google/android/music/download/DownloadState;
    .locals 1

    .prologue
    .line 423
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    return-object v0
.end method

.method protected getMusicEventLogger()Lcom/google/android/music/eventlog/MusicEventLogger;
    .locals 1

    .prologue
    .line 459
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    return-object v0
.end method

.method protected getOfflineDownloadingEnabledChangedTime()J
    .locals 2

    .prologue
    .line 451
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-wide v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabledChangedTime:J

    return-wide v0
.end method

.method protected getStreamingEnabledChangedTime()J
    .locals 2

    .prologue
    .line 447
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-wide v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabledChangedTime:J

    return-wide v0
.end method

.method protected isOfflineDownloadingEnabled()Z
    .locals 1

    .prologue
    .line 435
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method protected isStreamingEnabled()Z
    .locals 1

    .prologue
    .line 431
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method protected abstract networkChangedDuringDownload(J)Z
.end method

.method public run()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    const/16 v3, 0xb

    .line 144
    const/4 v0, 0x0

    .line 146
    .local v0, "success":Z
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/download/BaseDownloadTask;->handleRun()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    const/4 v0, 0x1

    .line 149
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v1}, Lcom/google/android/music/download/DownloadState;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/download/DownloadState$State;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 150
    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/music/download/BaseDownloadTask;->updateFailed(I)V

    .line 152
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/download/BaseDownloadTask;->updateProgress()V

    .line 154
    return-void

    .line 149
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v2}, Lcom/google/android/music/download/DownloadState;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/DownloadState$State;->isFinished()Z

    move-result v2

    if-nez v2, :cond_3

    .line 150
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/music/download/BaseDownloadTask;->updateFailed(I)V

    .line 152
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/download/BaseDownloadTask;->updateProgress()V

    throw v1
.end method

.method protected setServerSpecificRetryTime(J)V
    .locals 1
    .param p1, "serverSpecifiedRetryTime"    # J

    .prologue
    .line 439
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iput-wide p1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mServerSpecifiedRetryTime:J

    .line 440
    return-void
.end method

.method protected shouldStopDownload()Z
    .locals 1

    .prologue
    .line 427
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mStopDownload:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "task("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    iget-object v1, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected updateFailed(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 397
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/DownloadState;->setFailedState(I)V

    .line 398
    return-void
.end method

.method public upgrade(Lcom/google/android/music/download/DownloadTask;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/DownloadTask",
            "<TDownloadRequestType;>;)Z"
        }
    .end annotation

    .prologue
    .line 419
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask;, "Lcom/google/android/music/download/BaseDownloadTask<TDownloadRequestType;>;"
    .local p1, "downloadTask":Lcom/google/android/music/download/DownloadTask;, "Lcom/google/android/music/download/DownloadTask<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask;->mDownloadRequest:Lcom/google/android/music/download/DownloadRequest;

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/download/DownloadRequest;->upgrade(Lcom/google/android/music/download/DownloadRequest;)Z

    move-result v0

    return v0
.end method
