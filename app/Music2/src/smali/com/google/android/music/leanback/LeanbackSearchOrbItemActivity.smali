.class public abstract Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;
.super Lcom/google/android/music/leanback/LeanbackItemActivity;
.source "LeanbackSearchOrbItemActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;-><init>()V

    return-void
.end method

.method static getSearchOrbViewColors(Landroid/content/res/Resources;)Landroid/support/v17/leanback/widget/SearchOrbView$Colors;
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 25
    new-instance v0, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    const v1, 0x7f0c00bc

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 21
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;->startActivity(Landroid/content/Intent;)V

    .line 22
    return-void
.end method
