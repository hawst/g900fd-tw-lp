.class public Lcom/google/android/music/cloudclient/ExploreEntityJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ExploreEntityJson.java"


# instance fields
.field public mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "album"
    .end annotation
.end field

.field public mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlist"
    .end annotation
.end field

.field public mTrack:Lcom/google/android/music/sync/google/model/Track;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "track"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
