.class public Lcom/google/android/music/ui/DocumentListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DocumentListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/DocumentListAdapter$1;
    }
.end annotation


# static fields
.field private static TILE_METADATA:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private static TILE_METADATA_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private static TILE_METADATA_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# instance fields
.field private final mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private final mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private final mContext:Landroid/content/Context;

.field private final mDocList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    sput-object v0, Lcom/google/android/music/ui/DocumentListAdapter;->TILE_METADATA:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 28
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    sput-object v0, Lcom/google/android/music/ui/DocumentListAdapter;->TILE_METADATA_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 30
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    sput-object v0, Lcom/google/android/music/ui/DocumentListAdapter;->TILE_METADATA_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "type"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p4, "cardContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p5, "isEmulatedRadio"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/DocumentListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;ZLandroid/view/View$OnClickListener;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;ZLandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "type"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p4, "cardContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p5, "isEmulatedRadio"    # Z
    .param p6, "listener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Z",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    .local p2, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mDocList:Ljava/util/List;

    .line 64
    iput-object p6, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 65
    iput-object p4, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 67
    if-eqz p5, :cond_0

    .line 68
    sget-object v0, Lcom/google/android/music/ui/DocumentListAdapter;->TILE_METADATA:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 90
    :goto_0
    return-void

    .line 70
    :cond_0
    sget-object v0, Lcom/google/android/music/ui/DocumentListAdapter$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {p3}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 87
    sget-object v0, Lcom/google/android/music/ui/DocumentListAdapter;->TILE_METADATA:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    goto :goto_0

    .line 72
    :pswitch_0
    sget-object v0, Lcom/google/android/music/ui/DocumentListAdapter;->TILE_METADATA_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    goto :goto_0

    .line 75
    :pswitch_1
    sget-object v0, Lcom/google/android/music/ui/DocumentListAdapter;->TILE_METADATA_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    goto :goto_0

    .line 78
    :pswitch_2
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_VIDEO_THUMBNAIL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    goto :goto_0

    .line 81
    :pswitch_3
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED_RADIO_MUTANT:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mDocList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mDocList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 104
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 110
    instance-of v3, p2, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v3, :cond_1

    move-object v2, p2

    .line 111
    check-cast v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 118
    .local v2, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 121
    iget-object v3, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mDocList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 122
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v3, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 123
    iget-object v3, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_0

    iget-object p0, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .end local p0    # "this":Lcom/google/android/music/ui/DocumentListAdapter;
    :cond_0
    invoke-virtual {v2, p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    invoke-virtual {v2, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTag(Ljava/lang/Object;)V

    .line 125
    return-object v2

    .line 113
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v2    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .restart local p0    # "this":Lcom/google/android/music/ui/DocumentListAdapter;
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 115
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mCardMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .restart local v2    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 133
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/music/ui/cardlib/model/Document;

    if-eqz v0, :cond_0

    .line 134
    iget-object v1, p0, Lcom/google/android/music/ui/DocumentListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 137
    :cond_0
    return-void
.end method
