.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppStartedInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;


# instance fields
.field public url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2081
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2082
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2096
    const/4 v0, 0x0

    .line 2097
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->url:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2098
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2101
    :cond_0
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->cachedSize:I

    .line 2102
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2111
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2115
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2116
    :sswitch_0
    return-object p0

    .line 2121
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->url:Ljava/lang/String;

    goto :goto_0

    .line 2111
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2078
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2089
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->url:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2090
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2092
    :cond_0
    return-void
.end method
