.class Lcom/google/android/music/leanback/LeanbackItemActivity$1;
.super Ljava/lang/Object;
.source "LeanbackItemActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackItemActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackItemActivity;

.field final synthetic val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$mediaList:Lcom/google/android/music/medialist/MediaList;

.field final synthetic val$projection:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/MediaList;Landroid/content/Context;[Ljava/lang/String;Landroid/support/v17/leanback/widget/CursorObjectAdapter;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$mediaList:Lcom/google/android/music/medialist/MediaList;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$projection:[Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$mediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/MediaList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 125
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 126
    new-instance v1, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$mediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$projection:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 128
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/music/ui/MediaListCursorLoader;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$mediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$projection:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-virtual {v0, p2}, Landroid/support/v17/leanback/widget/CursorObjectAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 137
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 121
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;->val$adapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/CursorObjectAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 145
    return-void
.end method
