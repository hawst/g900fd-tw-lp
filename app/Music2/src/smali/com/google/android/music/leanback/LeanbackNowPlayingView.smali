.class public abstract Lcom/google/android/music/leanback/LeanbackNowPlayingView;
.super Landroid/widget/LinearLayout;
.source "LeanbackNowPlayingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackNowPlayingView$DropListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAttachedToWindow:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private final mImage1:Landroid/widget/ImageView;

.field private final mImage2:Landroid/widget/ImageView;

.field private final mImage3:Landroid/widget/ImageView;

.field private final mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

.field private final mObjectAnimator1:Landroid/animation/ObjectAnimator;

.field private final mObjectAnimator2:Landroid/animation/ObjectAnimator;

.field private final mObjectAnimator3:Landroid/animation/ObjectAnimator;

.field private final mPlayImage:Landroid/widget/ImageView;

.field private mReceiverRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v5, -0x2

    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    new-instance v3, Lcom/google/android/music/leanback/LeanbackNowPlayingView$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView$1;-><init>(Lcom/google/android/music/leanback/LeanbackNowPlayingView;)V

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    .line 96
    iput v4, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mCurrentState:I

    .line 97
    iput-boolean v4, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mReceiverRegistered:Z

    .line 102
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mContext:Landroid/content/Context;

    .line 104
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 106
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getBarRightMarginPx(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getBarsTopMarginPx(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getBarsLeftMarginPx(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getBarDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 110
    .local v0, "barDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 111
    .local v2, "pivotY":I
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    .line 112
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 115
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView$DropListener;->setDropScale(Landroid/view/View;)V

    .line 116
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->addView(Landroid/view/View;)V

    .line 118
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 119
    .restart local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getBarRightMarginPx(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getBarsTopMarginPx(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 121
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    .line 122
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 125
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView$DropListener;->setDropScale(Landroid/view/View;)V

    .line 126
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->addView(Landroid/view/View;)V

    .line 128
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 129
    .restart local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getBarsTopMarginPx(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 130
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    .line 131
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 133
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 134
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView$DropListener;->setDropScale(Landroid/view/View;)V

    .line 135
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->addView(Landroid/view/View;)V

    .line 137
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    const-string v4, "scaleY"

    const/16 v5, 0x1e

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator1:Landroid/animation/ObjectAnimator;

    .line 142
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator1:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3, v6}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 143
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator1:Landroid/animation/ObjectAnimator;

    const-wide/16 v4, 0x910

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 144
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator1:Landroid/animation/ObjectAnimator;

    new-instance v4, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v4}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 146
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    const-string v4, "scaleY"

    const/16 v5, 0x1b

    new-array v5, v5, [F

    fill-array-data v5, :array_1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator2:Landroid/animation/ObjectAnimator;

    .line 151
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator2:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3, v6}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 152
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator2:Landroid/animation/ObjectAnimator;

    const-wide/16 v4, 0x820

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 153
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator2:Landroid/animation/ObjectAnimator;

    new-instance v4, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v4}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 155
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    const-string v4, "scaleY"

    const/16 v5, 0x1a

    new-array v5, v5, [F

    fill-array-data v5, :array_2

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator3:Landroid/animation/ObjectAnimator;

    .line 159
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator3:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3, v6}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 160
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator3:Landroid/animation/ObjectAnimator;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 161
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator3:Landroid/animation/ObjectAnimator;

    new-instance v4, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v4}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 163
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    .line 164
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getPlayIconDimensionsPx(Landroid/content/res/Resources;)I

    move-result v5

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getPlayIconDimensionsPx(Landroid/content/res/Resources;)I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 167
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    const v4, 0x7f0201a6

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 168
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->addView(Landroid/view/View;)V

    .line 169
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 170
    return-void

    .line 137
    nop

    :array_0
    .array-data 4
        0x3ed55555
        0x3e800000    # 0.25f
        0x3ed55555
        0x3f155555
        0x3f400000    # 0.75f
        0x3f555555
        0x3f6aaaab
        0x3f800000    # 1.0f
        0x3f6aaaab
        0x3f800000    # 1.0f
        0x3f555555
        0x3f2aaaab
        0x3f000000    # 0.5f
        0x3eaaaaab
        0x3e2aaaab
        0x3eaaaaab
        0x3f000000    # 0.5f
        0x3f155555
        0x3f400000    # 0.75f
        0x3f6aaaab
        0x3f400000    # 0.75f
        0x3f155555
        0x3ed55555
        0x3e800000    # 0.25f
        0x3ed55555
        0x3f2aaaab
        0x3ed55555
        0x3e800000    # 0.25f
        0x3eaaaaab
        0x3ed55555
    .end array-data

    .line 146
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f6aaaab
        0x3f555555
        0x3f6aaaab
        0x3f800000    # 1.0f
        0x3f6aaaab
        0x3f400000    # 0.75f
        0x3f155555
        0x3f400000    # 0.75f
        0x3f6aaaab
        0x3f800000    # 1.0f
        0x3f555555
        0x3f2aaaab
        0x3f555555
        0x3f800000    # 1.0f
        0x3f6aaaab
        0x3f400000    # 0.75f
        0x3ed55555
        0x3e800000    # 0.25f
        0x3ed55555
        0x3f2aaaab
        0x3f555555
        0x3f800000    # 1.0f
        0x3f555555
        0x3f400000    # 0.75f
        0x3f2aaaab
        0x3f800000    # 1.0f
    .end array-data

    .line 155
    :array_2
    .array-data 4
        0x3f2aaaab
        0x3f400000    # 0.75f
        0x3f555555
        0x3f800000    # 1.0f
        0x3f6aaaab
        0x3f400000    # 0.75f
        0x3f155555
        0x3ed55555
        0x3f155555
        0x3f2aaaab
        0x3f400000    # 0.75f
        0x3f800000    # 1.0f
        0x3f6aaaab
        0x3f800000    # 1.0f
        0x3f400000    # 0.75f
        0x3f155555
        0x3f400000    # 0.75f
        0x3f6aaaab
        0x3f800000    # 1.0f
        0x3f555555
        0x3f2aaaab
        0x3f400000    # 0.75f
        0x3f155555
        0x3ed55555
        0x3e800000    # 0.25f
        0x3f2aaaab
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/LeanbackNowPlayingView;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackNowPlayingView;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->updatePlayState(ZZZ)V

    return-void
.end method

.method private registerReceiver()V
    .locals 3

    .prologue
    .line 215
    iget-boolean v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mAttachedToWindow:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mReceiverRegistered:Z

    if-nez v1, :cond_0

    .line 216
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 217
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 218
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 219
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 220
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 221
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 222
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mReceiverRegistered:Z

    .line 224
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private setPlayState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 234
    iput p1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mCurrentState:I

    .line 235
    packed-switch p1, :pswitch_data_0

    .line 249
    sget-object v0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPlayState: unexpected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :goto_0
    return-void

    .line 237
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->unregisterReceiver()V

    .line 238
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->stopAnimation()V

    goto :goto_0

    .line 241
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->registerReceiver()V

    .line 242
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->stopAnimation()V

    goto :goto_0

    .line 245
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->registerReceiver()V

    .line 246
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->startAnimation()V

    goto :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 255
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator1:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->startAnimation(Landroid/animation/Animator;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator2:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->startAnimation(Landroid/animation/Animator;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator3:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->startAnimation(Landroid/animation/Animator;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    return-void
.end method

.method private startAnimation(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 275
    invoke-virtual {p1}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 278
    :cond_0
    return-void
.end method

.method private stopAnimation()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 265
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator1:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->stopAnimation(Landroid/animation/Animator;Landroid/view/View;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator2:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->stopAnimation(Landroid/animation/Animator;Landroid/view/View;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mObjectAnimator3:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->stopAnimation(Landroid/animation/Animator;Landroid/view/View;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mPlayImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 272
    return-void
.end method

.method private stopAnimation(Landroid/animation/Animator;Landroid/view/View;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 281
    invoke-virtual {p1}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    .line 283
    invoke-static {p2}, Lcom/google/android/music/leanback/LeanbackNowPlayingView$DropListener;->setDropScale(Landroid/view/View;)V

    .line 285
    :cond_0
    return-void
.end method

.method private unregisterReceiver()V
    .locals 2

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mReceiverRegistered:Z

    .line 231
    :cond_0
    return-void
.end method

.method private updatePlayState(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 204
    if-nez p1, :cond_1

    .line 205
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v0

    .line 206
    .local v0, "state":Lcom/google/android/music/playback/PlaybackState;
    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {v0}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/music/playback/PlaybackState;->isStreaming()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->updatePlayState(ZZZ)V

    .line 212
    .end local v0    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 210
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->setPlayState(I)V

    goto :goto_0
.end method

.method private updatePlayState(ZZZ)V
    .locals 1
    .param p1, "isPlaying"    # Z
    .param p2, "isStreaming"    # Z
    .param p3, "isPreparing"    # Z

    .prologue
    const/4 v0, 0x1

    .line 288
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 289
    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->setPlayState(I)V

    .line 295
    :goto_0
    return-void

    .line 290
    :cond_0
    if-eqz p1, :cond_1

    .line 291
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->setPlayState(I)V

    goto :goto_0

    .line 293
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->setPlayState(I)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract getBarDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
.end method

.method protected abstract getBarRightMarginPx(Landroid/content/res/Resources;)I
.end method

.method protected abstract getBarsLeftMarginPx(Landroid/content/res/Resources;)I
.end method

.method protected abstract getBarsTopMarginPx(Landroid/content/res/Resources;)I
.end method

.method protected abstract getPlayIconDimensionsPx(Landroid/content/res/Resources;)I
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 180
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 181
    iput-boolean v1, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mAttachedToWindow:Z

    .line 182
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->getVisibility()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->updatePlayState(I)V

    .line 183
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mCurrentState:I

    if-eqz v0, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->registerReceiver()V

    .line 187
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mCurrentState:I

    if-ne v0, v1, :cond_1

    .line 188
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->stopAnimation()V

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->startAnimation()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->unregisterReceiver()V

    .line 198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->mAttachedToWindow:Z

    .line 199
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->stopAnimation()V

    .line 200
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 201
    return-void
.end method

.method public setVisibility(I)V
    .locals 0
    .param p1, "visibility"    # I

    .prologue
    .line 174
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 175
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackNowPlayingView;->updatePlayState(I)V

    .line 176
    return-void
.end method
