.class public Lcom/google/android/music/playback/session/MediaSessionLegacyHelper;
.super Ljava/lang/Object;
.source "MediaSessionLegacyHelper.java"


# direct methods
.method public static getRccStateFromState(I)I
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 107
    packed-switch p0, :pswitch_data_0

    .line 127
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 109
    :pswitch_1
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_BUFFERING:I

    goto :goto_0

    .line 111
    :pswitch_2
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_ERROR:I

    goto :goto_0

    .line 113
    :pswitch_3
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_FAST_FORWARDING:I

    goto :goto_0

    .line 115
    :pswitch_4
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_PAUSED:I

    goto :goto_0

    .line 117
    :pswitch_5
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_PLAYING:I

    goto :goto_0

    .line 119
    :pswitch_6
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_REWINDING:I

    goto :goto_0

    .line 121
    :pswitch_7
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_SKIPPING_BACKWARDS:I

    goto :goto_0

    .line 123
    :pswitch_8
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_SKIPPING_FORWARDS:I

    goto :goto_0

    .line 125
    :pswitch_9
    sget v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_STOPPED:I

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getRccTransportControlFlags(J)I
    .locals 6
    .param p0, "flags"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "transportFlags":I
    const-wide/16 v2, 0x4

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 139
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_PLAY:I

    or-int/2addr v0, v1

    .line 142
    :cond_0
    const-wide/16 v2, 0x2

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 143
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_PAUSE:I

    or-int/2addr v0, v1

    .line 146
    :cond_1
    const-wide/16 v2, 0x20

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 147
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_NEXT:I

    or-int/2addr v0, v1

    .line 150
    :cond_2
    const-wide/16 v2, 0x10

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 151
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_PREVIOUS:I

    or-int/2addr v0, v1

    .line 154
    :cond_3
    const-wide/16 v2, 0x40

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 155
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_FAST_FORWARD:I

    or-int/2addr v0, v1

    .line 158
    :cond_4
    const-wide/16 v2, 0x8

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 159
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_REWIND:I

    or-int/2addr v0, v1

    .line 162
    :cond_5
    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 163
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_STOP:I

    or-int/2addr v0, v1

    .line 166
    :cond_6
    const-wide/16 v2, 0x80

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 167
    sget v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_RATING:I

    or-int/2addr v0, v1

    .line 170
    :cond_7
    return v0
.end method

.method public static setOldMetadata(Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;Lcom/google/android/music/playback/session/MediaMetadataCompat;)V
    .locals 5
    .param p0, "editor"    # Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;
    .param p1, "metadata"    # Lcom/google/android/music/playback/session/MediaMetadataCompat;

    .prologue
    const v4, 0x10000001

    .line 16
    if-nez p1, :cond_0

    .line 17
    invoke-virtual {p0}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->apply()V

    .line 100
    :goto_0
    return-void

    .line 20
    :cond_0
    const-string v1, "android.media.metadata.ALBUM"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 21
    const/4 v1, 0x1

    const-string v2, "android.media.metadata.ALBUM"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 24
    :cond_1
    const/4 v0, 0x0

    .line 25
    .local v0, "art":Landroid/graphics/Bitmap;
    const-string v1, "android.media.metadata.ART"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 26
    const-string v1, "android.media.metadata.ART"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 31
    :cond_2
    :goto_1
    const/16 v1, 0x64

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putBitmap(ILandroid/graphics/Bitmap;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 33
    const-string v1, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 34
    const/16 v1, 0xd

    const-string v2, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 37
    :cond_3
    const-string v1, "android.media.metadata.ARTIST"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 38
    const/4 v1, 0x2

    const-string v2, "android.media.metadata.ARTIST"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 41
    :cond_4
    const-string v1, "android.media.metadata.AUTHOR"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 42
    const/4 v1, 0x3

    const-string v2, "android.media.metadata.AUTHOR"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 45
    :cond_5
    const-string v1, "android.media.metadata.COMPILATION"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 46
    const/16 v1, 0xf

    const-string v2, "android.media.metadata.COMPILATION"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 49
    :cond_6
    const-string v1, "android.media.metadata.COMPOSER"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 50
    const/4 v1, 0x4

    const-string v2, "android.media.metadata.COMPOSER"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 53
    :cond_7
    const-string v1, "android.media.metadata.DATE"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 54
    const/4 v1, 0x5

    const-string v2, "android.media.metadata.DATE"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 57
    :cond_8
    const-string v1, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 58
    const/16 v1, 0xe

    const-string v2, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putLong(IJ)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 61
    :cond_9
    const-string v1, "android.media.metadata.DURATION"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 62
    const/16 v1, 0x9

    const-string v2, "android.media.metadata.DURATION"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putLong(IJ)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 65
    :cond_a
    const-string v1, "android.media.metadata.GENRE"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 66
    const/4 v1, 0x6

    const-string v2, "android.media.metadata.GENRE"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 69
    :cond_b
    const-string v1, "android.media.metadata.NUM_TRACKS"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 70
    const/16 v1, 0xa

    const-string v2, "android.media.metadata.NUM_TRACKS"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putLong(IJ)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 73
    :cond_c
    const-string v1, "android.media.metadata.TITLE"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 74
    const/4 v1, 0x7

    const-string v2, "android.media.metadata.TITLE"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 77
    :cond_d
    const-string v1, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 78
    const/4 v1, 0x0

    const-string v2, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putLong(IJ)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 81
    :cond_e
    const-string v1, "android.media.metadata.WRITER"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 82
    const/16 v1, 0xb

    const-string v2, "android.media.metadata.WRITER"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 85
    :cond_f
    const-string v1, "android.media.metadata.YEAR"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 86
    const/16 v1, 0x8

    const-string v2, "android.media.metadata.YEAR"

    invoke-virtual {p1, v2}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putString(ILjava/lang/String;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 90
    :cond_10
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "android.media.metadata.USER_RATING"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "android.media.metadata.USER_RATING"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getRating(Ljava/lang/String;)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 93
    const-string v1, "android.media.metadata.USER_RATING"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getRating(Ljava/lang/String;)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/playback/session/RatingCompat;->getRating()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->putObject(ILjava/lang/Object;)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    .line 96
    invoke-virtual {p0, v4}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->addEditableKey(I)V

    .line 99
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;->apply()V

    goto/16 :goto_0

    .line 27
    :cond_12
    const-string v1, "android.media.metadata.ALBUM_ART"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 29
    const-string v1, "android.media.metadata.ALBUM_ART"

    invoke-virtual {p1, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_1
.end method
