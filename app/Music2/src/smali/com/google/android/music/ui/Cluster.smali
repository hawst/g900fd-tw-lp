.class public Lcom/google/android/music/ui/Cluster;
.super Ljava/lang/Object;
.source "Cluster.java"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private final mContent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private final mDocType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field private final mIsEmulateRadio:Z

.field private final mItemClickListener:Landroid/view/View$OnClickListener;

.field private final mMoreTitle:Ljava/lang/String;

.field private final mNbColumns:I

.field private final mNbRows:I

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;Lcom/google/android/music/store/ContainerDescriptor;)V
    .locals 12
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "cardType"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "moreTitle"    # Ljava/lang/String;
    .param p6, "docType"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p7, "nbColumns"    # I
    .param p8, "nbRows"    # I
    .param p9, "itemClickListener"    # Landroid/view/View$OnClickListener;
    .param p10, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "II",
            "Landroid/view/View$OnClickListener;",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p5, "content":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/google/android/music/ui/Cluster;-><init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;ZLcom/google/android/music/store/ContainerDescriptor;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;ZLcom/google/android/music/store/ContainerDescriptor;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "cardType"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "moreTitle"    # Ljava/lang/String;
    .param p6, "docType"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p7, "nbColumns"    # I
    .param p8, "nbRows"    # I
    .param p9, "itemClickListener"    # Landroid/view/View$OnClickListener;
    .param p10, "isEmulateRadio"    # Z
    .param p11, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "II",
            "Landroid/view/View$OnClickListener;",
            "Z",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    .local p5, "content":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/google/android/music/ui/Cluster;->mActivity:Landroid/app/Activity;

    .line 66
    iput-object p2, p0, Lcom/google/android/music/ui/Cluster;->mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 67
    iput-object p3, p0, Lcom/google/android/music/ui/Cluster;->mTitle:Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lcom/google/android/music/ui/Cluster;->mMoreTitle:Ljava/lang/String;

    .line 69
    iput-object p5, p0, Lcom/google/android/music/ui/Cluster;->mContent:Ljava/util/ArrayList;

    .line 70
    iput-object p6, p0, Lcom/google/android/music/ui/Cluster;->mDocType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 71
    iput p7, p0, Lcom/google/android/music/ui/Cluster;->mNbColumns:I

    .line 72
    iput p8, p0, Lcom/google/android/music/ui/Cluster;->mNbRows:I

    .line 73
    iput-object p9, p0, Lcom/google/android/music/ui/Cluster;->mItemClickListener:Landroid/view/View$OnClickListener;

    .line 74
    iput-boolean p10, p0, Lcom/google/android/music/ui/Cluster;->mIsEmulateRadio:Z

    .line 75
    iput-object p11, p0, Lcom/google/android/music/ui/Cluster;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 76
    return-void
.end method


# virtual methods
.method public getCardType()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/music/ui/Cluster;->mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method public getFullContent()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/Cluster;->mContent:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/music/ui/Cluster;->mItemClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getMoreOnClickListener()Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;
    .locals 8

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/ui/Cluster;->mItemClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    const/4 v6, 0x1

    .line 112
    .local v6, "playAll":Z
    :goto_0
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;

    iget-object v1, p0, Lcom/google/android/music/ui/Cluster;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/music/ui/Cluster;->mMoreTitle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/ui/Cluster;->mMoreTitle:Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Lcom/google/android/music/ui/Cluster;->mContent:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/music/ui/Cluster;->mDocType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    iget-boolean v5, p0, Lcom/google/android/music/ui/Cluster;->mIsEmulateRadio:Z

    iget-object v7, p0, Lcom/google/android/music/ui/Cluster;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;ZZLcom/google/android/music/store/ContainerDescriptor;)V

    return-object v0

    .line 111
    .end local v6    # "playAll":Z
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 112
    .restart local v6    # "playAll":Z
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/Cluster;->mTitle:Ljava/lang/String;

    goto :goto_1
.end method

.method public getNbColumns()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/google/android/music/ui/Cluster;->mNbColumns:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/music/ui/Cluster;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getVisibleContent()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    iget v2, p0, Lcom/google/android/music/ui/Cluster;->mNbColumns:I

    iget v3, p0, Lcom/google/android/music/ui/Cluster;->mNbRows:I

    mul-int v1, v2, v3

    .line 92
    .local v1, "numberOfVisibleItems":I
    iget-object v2, p0, Lcom/google/android/music/ui/Cluster;->mContent:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 93
    .local v0, "listEndIndex":I
    iget-object v2, p0, Lcom/google/android/music/ui/Cluster;->mContent:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    return-object v2
.end method
