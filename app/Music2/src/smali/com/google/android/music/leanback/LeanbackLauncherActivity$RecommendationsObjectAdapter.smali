.class Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackLauncherActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackLauncherActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RecommendationsObjectAdapter"
.end annotation


# instance fields
.field private final mFeelingLuckyItem:Lcom/google/android/music/leanback/Item;

.field private final mMainstageAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mOffset:I

.field private mShowFeelingLucky:Z

.field private mSize:I


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 2
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;
    .param p2, "feelingLuckyItem"    # Lcom/google/android/music/leanback/Item;
    .param p3, "mainstageAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 791
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 786
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mShowFeelingLucky:Z

    .line 792
    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mFeelingLuckyItem:Lcom/google/android/music/leanback/Item;

    .line 793
    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mMainstageAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 794
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->recalculateSize()V

    .line 795
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mMainstageAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 819
    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    .prologue
    .line 781
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->recalculateSize()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    .prologue
    .line 781
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    .prologue
    .line 781
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 781
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 781
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 781
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method private recalculateSize()V
    .locals 2

    .prologue
    .line 852
    iget-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mShowFeelingLucky:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I

    .line 853
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mMainstageAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mSize:I

    .line 854
    return-void

    .line 852
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 828
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mSize:I

    if-ge p1, v0, :cond_1

    .line 829
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I

    if-lt p1, v0, :cond_0

    .line 830
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mMainstageAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 835
    :goto_0
    return-object v0

    .line 833
    :cond_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mOffset:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 834
    if-nez p1, :cond_1

    .line 835
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mFeelingLuckyItem:Lcom/google/android/music/leanback/Item;

    goto :goto_0

    .line 839
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get unexpected position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public showFeelingLuckyVisibile(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 844
    iget-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mShowFeelingLucky:Z

    if-eq v0, p1, :cond_0

    .line 845
    iput-boolean p1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mShowFeelingLucky:Z

    .line 846
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->recalculateSize()V

    .line 847
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->notifyChanged()V

    .line 849
    :cond_0
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 823
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->mSize:I

    return v0
.end method
