.class Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "ExploreGenresHeadersCursor.java"


# instance fields
.field private final mItemHeight:I

.field private final mItemWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "genreId"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p1, p3}, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->getCursorForHeader(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 33
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 34
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mItemWidth:I

    .line 35
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mItemHeight:I

    .line 36
    return-void
.end method

.method private static getCursorForHeader(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 39
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Explore;->getGenresUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_GENRES:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 10
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 47
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 48
    .local v0, "context":Landroid/content/Context;
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "genreId":Ljava/lang/String;
    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 54
    .local v3, "genreName":Ljava/lang/String;
    sget-object v5, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "explore/genres"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 58
    .local v4, "uri":Landroid/net/Uri;
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "_id"

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, p1, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 59
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "name"

    invoke-virtual {v5, p1, v6, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 60
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "display_name"

    invoke-virtual {v5, p1, v6, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "background_image_uri"

    invoke-virtual {v5, p1, v6, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "bg_image_uri"

    invoke-virtual {v5, p1, v6, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "icon_uri"

    invoke-virtual {v5, p1, v6, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "default_item_width"

    iget v7, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mItemWidth:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, p1, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "default_item_height"

    iget v7, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mItemHeight:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, p1, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "color_hint"

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c00a6

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, p1, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "badge_uri"

    invoke-static {v0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, p1, v6, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v6, "items_uri"

    invoke-virtual {v5, p1, v6, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    return v9
.end method
