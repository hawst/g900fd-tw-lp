.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Share"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1172
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1173
    const v0, 0x7f0e002a

    const v1, 0x7f0b0245

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1174
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 12

    .prologue
    const/4 v8, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1178
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1179
    const-string v2, ""

    .line 1180
    .local v2, "metajamId":Ljava/lang/String;
    const-string v1, ""

    .line 1181
    .local v1, "emailSubject":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v6, v7, :cond_0

    .line 1182
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v2

    .line 1183
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b031d

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1201
    :goto_0
    const-string v6, "https://play.google.com/music/m/"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1204
    .local v0, "builder":Landroid/net/Uri$Builder;
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->shareWithExternalApps(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v1, v7}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$600(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .end local v1    # "emailSubject":Ljava/lang/String;
    .end local v2    # "metajamId":Ljava/lang/String;
    :goto_1
    return-void

    .line 1186
    .restart local v1    # "emailSubject":Ljava/lang/String;
    .restart local v2    # "metajamId":Ljava/lang/String;
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v6, v7, :cond_1

    .line 1187
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v2

    .line 1188
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b031e

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1191
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v6, v7, :cond_2

    .line 1192
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v2

    .line 1193
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b031f

    new-array v8, v11, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1197
    :cond_2
    const-string v6, "DocumentMenuHandler"

    const-string v7, "Unexpected doc type"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1208
    .end local v1    # "emailSubject":Ljava/lang/String;
    .end local v2    # "metajamId":Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v6, v7, :cond_4

    .line 1209
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v7, v7, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v4

    .line 1210
    .local v4, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v4, v6}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1211
    .local v3, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongStoreId()Ljava/lang/String;

    move-result-object v5

    .line 1212
    .local v5, "storeId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v6, v5, v3}, Lcom/google/android/music/GPlusShareActivity;->share(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1213
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v5    # "storeId":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v6, v7, :cond_5

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v6, v7, :cond_6

    .line 1216
    :cond_5
    const-string v6, "DocumentMenuHandler"

    const-string v7, "not implemented"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1218
    :cond_6
    const-string v6, "DocumentMenuHandler"

    const-string v7, "Unexpected doc type"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
