.class Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;
.super Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;
.source "MaterialRecentFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialRecentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecyclerAdapter"
.end annotation


# instance fields
.field private final VIEW_TYPE_RECENT_CARD:I

.field private final luckyDocument:Lcom/google/android/music/ui/cardlib/model/Document;

.field private final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mCursorLoaded:Z

.field private final mFragment:Lcom/google/android/music/ui/MaterialRecentFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/MaterialRecentFragment;)V
    .locals 3
    .param p1, "fragment"    # Lcom/google/android/music/ui/MaterialRecentFragment;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;-><init>()V

    .line 118
    iput v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->VIEW_TYPE_RECENT_CARD:I

    .line 121
    iput-boolean v1, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursorLoaded:Z

    .line 128
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialRecentFragment;

    .line 129
    invoke-virtual {p1}, Lcom/google/android/music/ui/MaterialRecentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    .line 130
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v1, v1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getImFeelingLuckyDocument(Landroid/content/Context;ZZZZ)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->luckyDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 132
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/MaterialRecentFragment;Lcom/google/android/music/ui/MaterialRecentFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/MaterialRecentFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/MaterialRecentFragment$1;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;-><init>(Lcom/google/android/music/ui/MaterialRecentFragment;)V

    return-void
.end method

.method private setupMatrixAdapterItems()V
    .locals 4

    .prologue
    .line 193
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_1

    .line 209
    :cond_0
    return-void

    .line 196
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->clearItems()V

    .line 198
    const/4 v1, 0x0

    .line 200
    .local v1, "index":I
    const/4 v0, 0x0

    .line 201
    .local v0, "cursorIndex":I
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, -0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 202
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 203
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 204
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->setItemId(JI)V

    .line 205
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->setItemData(Landroid/database/Cursor;II)V

    .line 206
    add-int/lit8 v1, v1, 0x1

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private updateEmptyViewVisibility()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    iget-boolean v3, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursorLoaded:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 185
    .local v0, "hasContent":Z
    :goto_0
    if-nez v0, :cond_1

    .line 186
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialRecentFragment;

    # invokes: Lcom/google/android/music/ui/MaterialRecentFragment;->setEmptyViewVisibility(Z)V
    invoke-static {v2, v1}, Lcom/google/android/music/ui/MaterialRecentFragment;->access$200(Lcom/google/android/music/ui/MaterialRecentFragment;Z)V

    .line 190
    :goto_1
    return-void

    .end local v0    # "hasContent":Z
    :cond_0
    move v0, v2

    .line 184
    goto :goto_0

    .line 188
    .restart local v0    # "hasContent":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialRecentFragment;

    # invokes: Lcom/google/android/music/ui/MaterialRecentFragment;->setEmptyViewVisibility(Z)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/MaterialRecentFragment;->access$200(Lcom/google/android/music/ui/MaterialRecentFragment;Z)V

    goto :goto_1
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 6
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 154
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 162
    const-string v1, "MusicMaterialRecent"

    const-string v2, "wrong type"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    .line 156
    check-cast v0, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 157
    .local v0, "playCardViewHolder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    iget-object v3, v0, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    invoke-virtual {p0, p2}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->getItemCursorOffset(I)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialRecentFragment;

    iget-object v5, v5, Lcom/google/android/music/ui/MaterialRecentFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/music/ui/RecentCardDocumentHelper;->populateRecentPlayCard(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;ILcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 214
    .local v0, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 221
    const-string v2, "MusicMaterialRecent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown view type clicked: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :goto_0
    return-void

    .line 216
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 217
    .local v1, "playCardViewHolderholder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    iget-object v3, v1, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v2, v3, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    goto :goto_0

    .line 214
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 6
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 136
    const-string v3, "MusicMaterialRecent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreateViewHolder viewType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 139
    .local v1, "inflater":Landroid/view/LayoutInflater;
    packed-switch p2, :pswitch_data_0

    .line 149
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 141
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialRecentFragment;

    # getter for: Lcom/google/android/music/ui/MaterialRecentFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v3}, Lcom/google/android/music/ui/MaterialRecentFragment;->access$100(Lcom/google/android/music/ui/MaterialRecentFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 143
    .local v2, "v":Landroid/view/View;
    new-instance v0, Lcom/google/android/music/ui/PlayCardViewHolder;

    invoke-direct {v0, v2}, Lcom/google/android/music/ui/PlayCardViewHolder;-><init>(Landroid/view/View;)V

    .line 144
    .local v0, "holder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v3, v0, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    invoke-virtual {v3, p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v3, v0, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialRecentFragment;

    # getter for: Lcom/google/android/music/ui/MaterialRecentFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v4}, Lcom/google/android/music/ui/MaterialRecentFragment;->access$100(Lcom/google/android/music/ui/MaterialRecentFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursorLoaded:Z

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    .line 180
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->notifyDataSetChanged()V

    .line 181
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 167
    const-string v0, "MusicMaterialRecent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapCursor newCursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursor:Landroid/database/Cursor;

    .line 169
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursorLoaded:Z

    .line 170
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->mCursorLoaded:Z

    if-eqz v0, :cond_0

    .line 171
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->setupMatrixAdapterItems()V

    .line 172
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->updateEmptyViewVisibility()V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialRecentFragment$RecyclerAdapter;->notifyDataSetChanged()V

    .line 175
    :cond_0
    return-void

    .line 169
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
