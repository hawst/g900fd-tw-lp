.class public final enum Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
.super Ljava/lang/Enum;
.source "RadioSeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/model/RadioSeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RemoteSeedType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum ALBUM:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum ARTIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum ARTIST_SHUFFLE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum CURATED:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum GENRE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum LOCKER_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum LUCKY:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum METAJAM_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum PLAYLIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field public static final enum UNKNOWN:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

.field private static final sRemoteValues:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;",
            ">;"
        }
    .end annotation
.end field

.field private static final sSchemaValues:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 21
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "UNKNOWN"

    invoke-direct {v4, v5, v8, v8}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->UNKNOWN:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 22
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "LOCKER_TRACK"

    invoke-direct {v4, v5, v9, v9}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->LOCKER_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 23
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "METAJAM_TRACK"

    invoke-direct {v4, v5, v10, v10}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->METAJAM_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 24
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "ARTIST"

    invoke-direct {v4, v5, v11, v11}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ARTIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 25
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "ALBUM"

    invoke-direct {v4, v5, v12, v12}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ALBUM:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 26
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "GENRE"

    const/4 v6, 0x5

    const/4 v7, 0x5

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->GENRE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 27
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "LUCKY"

    const/4 v6, 0x6

    const/4 v7, 0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->LUCKY:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 28
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "ARTIST_SHUFFLE"

    const/4 v6, 0x7

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ARTIST_SHUFFLE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 29
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "PLAYLIST"

    const/16 v6, 0x8

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->PLAYLIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 30
    new-instance v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    const-string v5, "CURATED"

    const/16 v6, 0x9

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->CURATED:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 20
    const/16 v4, 0xa

    new-array v4, v4, [Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->UNKNOWN:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->LOCKER_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->METAJAM_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ARTIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ALBUM:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v5, v4, v12

    const/4 v5, 0x5

    sget-object v6, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->GENRE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->LUCKY:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ARTIST_SHUFFLE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->PLAYLIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->CURATED:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->$VALUES:[Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 33
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->sRemoteValues:Landroid/util/SparseArray;

    .line 35
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    sput-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->sSchemaValues:Landroid/util/SparseArray;

    .line 39
    invoke-static {}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->values()[Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 40
    .local v3, "rst":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->sRemoteValues:Landroid/util/SparseArray;

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getRemoteValue()I

    move-result v5

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 41
    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->sSchemaValues:Landroid/util/SparseArray;

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v5

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 39
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    .end local v3    # "rst":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->mValue:I

    .line 46
    return-void
.end method

.method public static fromRemoteValue(I)Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    .locals 2
    .param p0, "remoteValue"    # I

    .prologue
    .line 81
    sget-object v1, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->sRemoteValues:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 82
    .local v0, "value":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    if-eqz v0, :cond_0

    .end local v0    # "value":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    :goto_0
    return-object v0

    .restart local v0    # "value":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    :cond_0
    sget-object v0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->UNKNOWN:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    goto :goto_0
.end method

.method public static fromSchemaValue(I)Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    .locals 2
    .param p0, "schemaValue"    # I

    .prologue
    .line 77
    sget-object v1, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->sSchemaValues:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    .line 78
    .local v0, "value":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    if-eqz v0, :cond_0

    .end local v0    # "value":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    :goto_0
    return-object v0

    .restart local v0    # "value":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    :cond_0
    sget-object v0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->UNKNOWN:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->$VALUES:[Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v0}, [Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    return-object v0
.end method


# virtual methods
.method public getRemoteValue()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->mValue:I

    return v0
.end method

.method public getSchemaValue()I
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/music/sync/google/model/RadioSeed$1;->$SwitchMap$com$google$android$music$sync$google$model$RadioSeed$RemoteSeedType:[I

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 54
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 56
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 58
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 60
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 62
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 64
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 66
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 68
    :pswitch_7
    const/16 v0, 0x8

    goto :goto_0

    .line 70
    :pswitch_8
    const/16 v0, 0x9

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
