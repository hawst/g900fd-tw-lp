.class public final Lcom/google/android/music/xdi/XdiContract;
.super Ljava/lang/Object;
.source "XdiContract.java"


# direct methods
.method public static getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 2
    .param p0, "root"    # Landroid/net/Uri;
    .param p1, "start"    # I

    .prologue
    .line 1649
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.xdi.action.BROWSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1650
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1651
    const-string v1, "start_index"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1652
    return-object v0
.end method

.method public static getBrowseIntentByName(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "root"    # Landroid/net/Uri;
    .param p1, "startName"    # Ljava/lang/String;

    .prologue
    .line 1667
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.xdi.action.BROWSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1668
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1669
    const-string v1, "start_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1670
    return-object v0
.end method

.method public static getDetailsIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2
    .param p0, "root"    # Landroid/net/Uri;

    .prologue
    .line 1730
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.xdi.action.DETAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1731
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1732
    return-object v0
.end method

.method public static getGridIntent(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 2
    .param p0, "root"    # Landroid/net/Uri;
    .param p1, "start"    # I

    .prologue
    .line 1700
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.xdi.action.VIEW_GRID"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1701
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1702
    const-string v1, "start_index"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1703
    return-object v0
.end method
