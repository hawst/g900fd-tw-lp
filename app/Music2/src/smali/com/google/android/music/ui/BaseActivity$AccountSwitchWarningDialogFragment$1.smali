.class Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;)V
    .locals 0

    .prologue
    .line 1579
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;->this$0:Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1583
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;->this$0:Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "account"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1584
    .local v5, "selectedAccountName":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;->this$0:Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->getAvailableAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    .line 1586
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 1587
    .local v0, "account":Landroid/accounts/Account;
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1588
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;->this$0:Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->launchAccountChooserWithAccount(Landroid/app/Activity;ZLandroid/accounts/Account;)Z

    .line 1590
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment$1;->this$0:Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;->dismiss()V

    .line 1597
    .end local v0    # "account":Landroid/accounts/Account;
    :goto_1
    return-void

    .line 1586
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1594
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    const-string v6, "MusicBaseActivity"

    const-string v7, "The user clicked an account in the account switcher which does not appear in the available accounts."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
