.class Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;
.super Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter;
.source "LeanbackDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackDetailsActivity;->addClassPresenters(Landroid/support/v17/leanback/widget/ClassPresenterSelector;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackDetailsActivity;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackDetailsActivity;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onBindDescription(Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 3
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;
    .param p2, "itemObject"    # Ljava/lang/Object;

    .prologue
    .line 77
    move-object v0, p2

    check-cast v0, Lcom/google/android/music/leanback/Item;

    .line 78
    .local v0, "item":Lcom/google/android/music/leanback/Item;
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 80
    new-instance v1, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;-><init>(Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getSubtitle()Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
