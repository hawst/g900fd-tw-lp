.class public final Lcom/google/android/music/store/MusicContent$AutoPlaylists$Members;
.super Ljava/lang/Object;
.source "MusicContent.java"

# interfaces
.implements Lcom/google/android/music/store/MusicContent$PlaylistMemberColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent$AutoPlaylists;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Members"
.end annotation


# direct methods
.method public static getAutoPlaylistItemUri(JLjava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0, "playlistId"    # J
    .param p2, "storeSongId"    # Ljava/lang/String;

    .prologue
    .line 810
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 811
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Missing song store id"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 813
    :cond_0
    const/4 v2, 0x0

    invoke-static {p0, p1, v2}, Lcom/google/android/music/store/MusicContent$AutoPlaylists$Members;->getAutoPlaylistItemsUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 814
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 815
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "storeSongId"

    invoke-virtual {v0, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 816
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public static getAutoPlaylistItemsUri(JLjava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0, "playlistId"    # J
    .param p2, "sortParam"    # Ljava/lang/String;

    .prologue
    .line 794
    invoke-static {p0, p1}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->getAutoPlaylistUri(J)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "members"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 795
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 796
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 797
    const-string v2, "order"

    invoke-virtual {v0, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 799
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method
