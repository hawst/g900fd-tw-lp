.class public Lcom/google/android/music/leanback/LeanbackSuggestedMixesActivity;
.super Lcom/google/android/music/leanback/LeanbackGridActivity;
.source "LeanbackSuggestedMixesActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected bindCursor(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 24
    .param p1, "leanbackCursorObjectAdapter"    # Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 42
    const/4 v9, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 43
    .local v6, "id":J
    const/4 v9, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 44
    .local v8, "name":Ljava/lang/String;
    const/4 v9, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 46
    .local v20, "type":I
    new-instance v5, Lcom/google/android/music/medialist/PlaylistSongList;

    const/16 v9, 0x32

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v5 .. v15}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 50
    .local v5, "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v17

    .line 57
    .local v17, "intent":Landroid/content/Intent;
    move-object/from16 v19, p0

    .line 58
    .local v19, "ref":Lcom/google/android/music/leanback/LeanbackSuggestedMixesActivity;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v18

    .line 60
    .local v18, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 62
    .local v4, "description":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 65
    new-instance v10, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/leanback/LeanbackSuggestedMixesActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v12

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    add-int/lit8 v13, v9, 0x1

    const-wide/32 v14, 0x6ccc0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    int-to-long v0, v9

    move-wide/from16 v22, v0

    add-long v14, v14, v22

    move-object/from16 v11, p0

    move-object/from16 v16, v5

    invoke-direct/range {v10 .. v16}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLcom/google/android/music/medialist/SongList;)V

    .line 68
    .local v10, "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    invoke-virtual {v10}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;->startLoading()V

    .line 69
    new-instance v9, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v9}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v9, v8}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v10}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    const v11, 0x7f0200cf

    invoke-virtual {v9, v11}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v9

    return-object v9

    .line 62
    .end local v4    # "description":Ljava/lang/String;
    .end local v10    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    :catchall_0
    move-exception v9

    invoke-static/range {v19 .. v19}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v9
.end method

.method protected createItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method protected getGridTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSuggestedMixesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/music/medialist/RecommendedRadioList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/RecommendedRadioList;-><init>()V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/ui/RecommendedRadioFragment;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method
