.class public Lcom/google/android/music/art/MultiImageCompositePostProcessor;
.super Ljava/lang/Object;
.source "MultiImageCompositePostProcessor.java"

# interfaces
.implements Lcom/google/android/music/art/ArtPostProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/MultiImageCompositePostProcessor$1;,
        Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;
    }
.end annotation


# static fields
.field private static final sInstanceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;",
            "Lcom/google/android/music/art/MultiImageCompositePostProcessor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAntiAlias:Landroid/graphics/Paint;

.field private final mColumns:I

.field private final mRows:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->sInstanceMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(II)V
    .locals 2
    .param p1, "rows"    # I
    .param p2, "columns"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mAntiAlias:Landroid/graphics/Paint;

    .line 35
    iput p1, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mRows:I

    .line 36
    iput p2, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mColumns:I

    .line 37
    return-void
.end method

.method private extendImagesIfNeeded([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V
    .locals 2
    .param p1, "source"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .param p2, "destination"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    const/4 v1, 0x2

    .line 106
    array-length v0, p1

    if-nez v0, :cond_0

    .line 113
    :goto_0
    return-void

    .line 107
    :cond_0
    iget v0, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mColumns:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mRows:I

    if-ne v0, v1, :cond_1

    .line 108
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->extendImagesIfNeeded2x2([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    goto :goto_0

    .line 110
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->extendImagesRepeating([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    goto :goto_0
.end method

.method private extendImagesIfNeeded2x2([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V
    .locals 7
    .param p1, "source"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .param p2, "destination"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 125
    array-length v1, p2

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(Z)V

    .line 126
    array-length v1, p1

    if-ne v1, v2, :cond_2

    .line 127
    aget-object v1, p1, v3

    aput-object v1, p2, v6

    aput-object v1, p2, v5

    aput-object v1, p2, v2

    aput-object v1, p2, v3

    .line 143
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v3

    .line 125
    goto :goto_0

    .line 128
    :cond_2
    array-length v1, p1

    if-ne v1, v5, :cond_3

    .line 131
    aget-object v1, p1, v3

    aput-object v1, p2, v6

    aput-object v1, p2, v3

    .line 132
    aget-object v1, p1, v2

    aput-object v1, p2, v5

    aput-object v1, p2, v2

    goto :goto_1

    .line 133
    :cond_3
    array-length v1, p1

    if-ne v1, v6, :cond_4

    .line 135
    aget-object v1, p1, v3

    aput-object v1, p2, v6

    aput-object v1, p2, v3

    .line 136
    aget-object v1, p1, v2

    aput-object v1, p2, v2

    .line 137
    aget-object v1, p1, v5

    aput-object v1, p2, v5

    goto :goto_1

    .line 139
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 140
    aget-object v1, p1, v0

    aput-object v1, p2, v0

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private extendImagesRepeating([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V
    .locals 2
    .param p1, "source"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .param p2, "destination"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 153
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 154
    array-length v1, p1

    rem-int v1, v0, v1

    aget-object v1, p1, v1

    aput-object v1, p2, v0

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 156
    :cond_0
    return-void
.end method

.method public static getInstance(II)Lcom/google/android/music/art/MultiImageCompositePostProcessor;
    .locals 6
    .param p0, "rows"    # I
    .param p1, "columns"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 40
    if-lez p0, :cond_1

    move v2, v3

    :goto_0
    const-string v5, "Rows must be > 0"

    invoke-static {v2, v5}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 41
    if-lez p1, :cond_2

    :goto_1
    const-string v2, "Columns must be > 0"

    invoke-static {v3, v2}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 43
    new-instance v1, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;-><init>(IILcom/google/android/music/art/MultiImageCompositePostProcessor$1;)V

    .line 44
    .local v1, "key":Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;
    sget-object v3, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->sInstanceMap:Ljava/util/Map;

    monitor-enter v3

    .line 45
    :try_start_0
    sget-object v2, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->sInstanceMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    .line 46
    .local v0, "instance":Lcom/google/android/music/art/MultiImageCompositePostProcessor;
    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;

    .end local v0    # "instance":Lcom/google/android/music/art/MultiImageCompositePostProcessor;
    invoke-direct {v0, p0, p1}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;-><init>(II)V

    .line 48
    .restart local v0    # "instance":Lcom/google/android/music/art/MultiImageCompositePostProcessor;
    sget-object v2, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->sInstanceMap:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :cond_0
    monitor-exit v3

    return-object v0

    .end local v0    # "instance":Lcom/google/android/music/art/MultiImageCompositePostProcessor;
    .end local v1    # "key":Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;
    :cond_1
    move v2, v4

    .line 40
    goto :goto_0

    :cond_2
    move v3, v4

    .line 41
    goto :goto_1

    .line 51
    .restart local v1    # "key":Lcom/google/android/music/art/MultiImageCompositePostProcessor$InstanceKey;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method


# virtual methods
.method public aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 100
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "MultiImageCompositePostProcessor does not support alias mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getConfig(Lcom/google/android/music/art/ArtDescriptor;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 170
    sget-object v0, Lcom/google/android/music/art/ArtResolver2;->DEFAULT_CONFIG:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public getMaxNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 2
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 160
    iget v0, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mRows:I

    iget v1, p0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mColumns:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 165
    const/4 v0, 0x1

    return v0
.end method

.method public getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z
    .locals 25
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "images"    # [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .param p3, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 62
    const-string v21, "images must not be null"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v21, v0

    if-lez v21, :cond_0

    const/16 v21, 0x1

    :goto_0
    const-string v22, "images must contain at least one image."

    invoke-static/range {v21 .. v22}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 64
    move-object/from16 v5, p2

    .local v5, "arr$":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    array-length v14, v5

    .local v14, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    if-ge v11, v14, :cond_1

    aget-object v6, v5, v11

    .line 65
    .local v6, "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    const-string v21, "images array must not have null elements"

    move-object/from16 v0, v21

    invoke-static {v6, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 63
    .end local v5    # "arr$":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v6    # "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v11    # "i$":I
    .end local v14    # "len$":I
    :cond_0
    const/16 v21, 0x0

    goto :goto_0

    .line 67
    .restart local v5    # "arr$":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .restart local v11    # "i$":I
    .restart local v14    # "len$":I
    :cond_1
    const-string v21, "result must not be null"

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v21

    const-string v22, "result must be a mutable bitmap."

    invoke-static/range {v21 .. v22}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 70
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mColumns:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v18, v21, v22

    .line 71
    .local v18, "sliceWidth":F
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mRows:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v17, v21, v22

    .line 72
    .local v17, "sliceHeight":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mRows:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mColumns:I

    move/from16 v22, v0

    mul-int v21, v21, v22

    move/from16 v0, v21

    new-array v7, v0, [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .line 73
    .local v7, "bitmaps":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->extendImagesIfNeeded([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    .line 74
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 75
    .local v19, "srcRect":Landroid/graphics/Rect;
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 76
    .local v10, "destRect":Landroid/graphics/Rect;
    new-instance v8, Landroid/graphics/Canvas;

    move-object/from16 v0, p3

    invoke-direct {v8, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    .local v8, "canvas":Landroid/graphics/Canvas;
    const/high16 v21, -0x1000000

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 81
    const/16 v16, 0x0

    .local v16, "row":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mRows:I

    move/from16 v21, v0

    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    .line 82
    const/4 v9, 0x0

    .local v9, "column":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mColumns:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v9, v0, :cond_2

    .line 83
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mRows:I

    move/from16 v21, v0

    mul-int v21, v21, v16

    add-int v21, v21, v9

    aget-object v15, v7, v21

    .line 84
    .local v15, "managedBitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    iget-object v12, v15, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    .line 85
    .local v12, "image":Landroid/graphics/Bitmap;
    int-to-float v0, v9

    move/from16 v21, v0

    mul-float v13, v21, v18

    .line 86
    .local v13, "left":F
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v20, v21, v17

    .line 87
    .local v20, "top":F
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v22

    add-float v23, v13, v18

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v23

    add-float v24, v20, v17

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->round(F)I

    move-result v24

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 89
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/art/ArtRenderingUtils;->calculateSourceSlice(Landroid/graphics/Rect;IIII)V

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/art/MultiImageCompositePostProcessor;->mAntiAlias:Landroid/graphics/Paint;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v8, v12, v0, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 82
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 81
    .end local v12    # "image":Landroid/graphics/Bitmap;
    .end local v13    # "left":F
    .end local v15    # "managedBitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v20    # "top":F
    :cond_2
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2

    .line 94
    .end local v9    # "column":I
    :cond_3
    const/16 v21, 0x1

    return v21
.end method
