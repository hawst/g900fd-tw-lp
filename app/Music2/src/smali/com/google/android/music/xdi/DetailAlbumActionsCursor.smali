.class Lcom/google/android/music/xdi/DetailAlbumActionsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailAlbumActionsCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "albumId"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 53
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mImageWidth:I

    .line 54
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mImageHeight:I

    .line 56
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addActions(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method private addActions(Ljava/lang/String;)V
    .locals 24
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 60
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v4, v0, v5}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 62
    .local v21, "cursor":Landroid/database/Cursor;
    if-nez v21, :cond_0

    .line 120
    :goto_0
    return-void

    .line 67
    :cond_0
    :try_start_0
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 68
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v22

    .line 69
    .local v22, "isNautilusAlbum":Z
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 70
    .local v23, "name":Ljava/lang/String;
    const/16 v16, 0x0

    .line 72
    .local v16, "artUriString":Ljava/lang/String;
    if-eqz v22, :cond_6

    .line 73
    const/4 v4, 0x2

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 74
    const/4 v4, 0x2

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 76
    :cond_1
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 77
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v16

    .line 84
    :cond_2
    :goto_1
    const/4 v9, 0x0

    .line 85
    .local v9, "id":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00c8

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02db

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "play"

    add-int/lit8 v15, v9, 0x1

    .end local v9    # "id":I
    .local v15, "id":I
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addPlayAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0056

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02db

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "shuffle"

    add-int/lit8 v9, v15, 0x1

    .end local v15    # "id":I
    .restart local v9    # "id":I
    move-object/from16 v10, p0

    move-object/from16 v11, p1

    invoke-direct/range {v10 .. v15}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addPlayAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 91
    add-int/lit8 v15, v9, 0x1

    .end local v9    # "id":I
    .restart local v15    # "id":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    move-object/from16 v3, v16

    invoke-direct {v0, v1, v2, v3, v9}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addStartRadioAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    const/16 v20, 0x0

    .line 94
    .local v20, "artistMetajamId":Ljava/lang/String;
    const-wide/16 v18, 0x0

    .line 95
    .local v18, "artistIdLong":J
    const/4 v4, 0x4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 96
    const/4 v4, 0x4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 98
    :cond_3
    const/4 v4, 0x3

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 99
    const/4 v4, 0x3

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 102
    :cond_4
    const/16 v17, 0x0

    .line 103
    .local v17, "artistId":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 104
    move-object/from16 v17, v20

    .line 112
    :goto_2
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 113
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0244

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v9, v15, 0x1

    .end local v15    # "id":I
    .restart local v9    # "id":I
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v4, v15}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addGoToArtistAction(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    .end local v9    # "id":I
    .end local v16    # "artUriString":Ljava/lang/String;
    .end local v17    # "artistId":Ljava/lang/String;
    .end local v18    # "artistIdLong":J
    .end local v20    # "artistMetajamId":Ljava/lang/String;
    .end local v22    # "isNautilusAlbum":Z
    .end local v23    # "name":Ljava/lang/String;
    :cond_5
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 80
    .restart local v16    # "artUriString":Ljava/lang/String;
    .restart local v22    # "isNautilusAlbum":Z
    .restart local v23    # "name":Ljava/lang/String;
    :cond_6
    :try_start_1
    invoke-static/range {p1 .. p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mImageWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mImageHeight:I

    invoke-static {v4, v5, v6, v7, v8}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_1

    .line 105
    .restart local v15    # "id":I
    .restart local v17    # "artistId":Ljava/lang/String;
    .restart local v18    # "artistIdLong":J
    .restart local v20    # "artistMetajamId":Ljava/lang/String;
    :cond_7
    const-wide/16 v4, 0x0

    cmp-long v4, v18, v4

    if-lez v4, :cond_8

    .line 106
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    goto :goto_2

    .line 108
    :cond_8
    const-string v4, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid artist ID in album "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 118
    .end local v15    # "id":I
    .end local v16    # "artUriString":Ljava/lang/String;
    .end local v17    # "artistId":Ljava/lang/String;
    .end local v18    # "artistIdLong":J
    .end local v20    # "artistMetajamId":Ljava/lang/String;
    .end local v22    # "isNautilusAlbum":Z
    .end local v23    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v4

    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4
.end method

.method private addGoToArtistAction(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "actionId"    # I

    .prologue
    .line 188
    sget-object v1, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "details/artists"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/xdi/XdiContract;->getDetailsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 192
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-direct {p0, p2, v1, p3, v0}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    .line 193
    return-void
.end method

.method private addPlayAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "actionName"    # Ljava/lang/String;
    .param p3, "actionSubName"    # Ljava/lang/String;
    .param p4, "playMode"    # Ljava/lang/String;
    .param p5, "actionId"    # I

    .prologue
    .line 133
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v1

    .line 135
    .local v1, "isNautilusAlbum":Z
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v0

    .line 136
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 137
    const-string v2, "container"

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 139
    const-string v2, "id_string"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    :goto_0
    const-string v2, "play_mode"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    invoke-direct {p0, p2, p3, p5, v0}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    .line 147
    return-void

    .line 141
    :cond_0
    const-string v2, "container"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 142
    const-string v2, "id"

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "subName"    # Ljava/lang/String;
    .param p3, "actionId"    # I
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 196
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v1}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 197
    .local v0, "values":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "_id"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 198
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_name"

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 199
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_subname"

    invoke-virtual {v1, v0, v2, p2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 200
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "intent_uri"

    const/4 v3, 0x1

    invoke-virtual {p4, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addRow([Ljava/lang/Object;)V

    .line 203
    return-void
.end method

.method private addStartRadioAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "albumName"    # Ljava/lang/String;
    .param p3, "artUri"    # Ljava/lang/String;
    .param p4, "actionId"    # I

    .prologue
    .line 159
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v2

    .line 161
    .local v2, "isNautilusAlbum":Z
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v1

    .line 162
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 163
    const-string v3, "container"

    const/16 v4, 0x9

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 165
    const-string v3, "id_string"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    :goto_0
    const-string v3, "name"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v3, "art_uri"

    invoke-virtual {v1, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    iget-object v3, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b023a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "actionTitle":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b02da

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3, p4, v1}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    .line 178
    return-void

    .line 167
    .end local v0    # "actionTitle":Ljava/lang/String;
    :cond_0
    const-string v3, "container"

    const/16 v4, 0xe

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 169
    const-string v3, "id"

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 174
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b023b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
