.class Lcom/google/android/music/art/ArtRequest$1;
.super Ljava/lang/Object;
.source "ArtRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtRequest;->removeListener(Lcom/google/android/music/art/ArtRequest$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtRequest;

.field final synthetic val$resolver:Lcom/google/android/music/art/ArtResolver;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtRequest;Lcom/google/android/music/art/ArtResolver;)V
    .locals 0

    .prologue
    .line 125
    .local p0, "this":Lcom/google/android/music/art/ArtRequest$1;, "Lcom/google/android/music/art/ArtRequest.1;"
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest$1;->this$0:Lcom/google/android/music/art/ArtRequest;

    iput-object p2, p0, Lcom/google/android/music/art/ArtRequest$1;->val$resolver:Lcom/google/android/music/art/ArtResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 128
    .local p0, "this":Lcom/google/android/music/art/ArtRequest$1;, "Lcom/google/android/music/art/ArtRequest.1;"
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest$1;->this$0:Lcom/google/android/music/art/ArtRequest;

    iget-object v2, v2, Lcom/google/android/music/art/ArtRequest;->mImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 129
    .local v0, "bmp":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest$1;->val$resolver:Lcom/google/android/music/art/ArtResolver;

    invoke-virtual {v2, v0}, Lcom/google/android/music/art/ArtResolver;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 131
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtRequest$1;->this$0:Lcom/google/android/music/art/ArtRequest;

    iget-object v2, v2, Lcom/google/android/music/art/ArtRequest;->mImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 132
    return-void
.end method
