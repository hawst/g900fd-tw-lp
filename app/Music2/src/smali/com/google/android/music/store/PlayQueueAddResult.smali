.class public Lcom/google/android/music/store/PlayQueueAddResult;
.super Ljava/lang/Object;
.source "PlayQueueAddResult.java"


# instance fields
.field private final mAddedSize:I

.field private final mPlayPosition:I

.field private final mRequestedSize:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "howManyAdded"    # I
    .param p2, "howManyRequested"    # I
    .param p3, "pos"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p2, p0, Lcom/google/android/music/store/PlayQueueAddResult;->mRequestedSize:I

    .line 20
    iput p1, p0, Lcom/google/android/music/store/PlayQueueAddResult;->mAddedSize:I

    .line 21
    iput p3, p0, Lcom/google/android/music/store/PlayQueueAddResult;->mPlayPosition:I

    .line 22
    return-void
.end method


# virtual methods
.method public getAddedSize()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/music/store/PlayQueueAddResult;->mAddedSize:I

    return v0
.end method

.method public getPlayPosition()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/music/store/PlayQueueAddResult;->mPlayPosition:I

    return v0
.end method

.method public getRequestedSize()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/music/store/PlayQueueAddResult;->mRequestedSize:I

    return v0
.end method
