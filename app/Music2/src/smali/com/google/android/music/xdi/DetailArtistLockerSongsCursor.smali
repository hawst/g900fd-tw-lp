.class Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistLockerSongsCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mContext:Landroid/content/Context;

    .line 41
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 43
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->addRowsForArtist(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method private addRowsForArtist(Ljava/lang/String;)V
    .locals 18
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 47
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v14, v0}, Lcom/google/android/music/xdi/XdiUtils;->getLockerSongsByArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 48
    .local v10, "songDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 49
    const-string v14, "MusicXdi"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "No locker songs found for artist "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_0
    return-void

    .line 52
    :cond_1
    invoke-static {v10}, Lcom/google/android/music/xdi/XdiUtils;->extractTrackIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 53
    .local v12, "songIdsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {v12}, Lcom/google/common/primitives/Longs;->toArray(Ljava/util/Collection;)[J

    move-result-object v11

    .line 55
    .local v11, "songIds":[J
    const-string v2, ""

    .line 56
    .local v2, "artistName":Ljava/lang/String;
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 57
    .local v5, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 58
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v2

    .line 64
    .end local v5    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_3
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    .line 65
    .local v4, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v14, Lcom/google/android/music/medialist/SelectedSongList;

    invoke-direct {v14, v4, v11}, Lcom/google/android/music/medialist/SelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[J)V

    invoke-virtual {v14}, Lcom/google/android/music/medialist/SelectedSongList;->freeze()Ljava/lang/String;

    move-result-object v9

    .line 66
    .local v9, "selectedSongList":Ljava/lang/String;
    const/4 v7, 0x0

    .line 67
    .local v7, "index":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v14}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v14

    new-array v13, v14, [Ljava/lang/Object;

    .line 70
    .local v13, "values":[Ljava/lang/Object;
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 71
    .restart local v5    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "artworkUrl":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 73
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 76
    :cond_4
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v14

    const-string v15, "container"

    const/16 v16, 0x18

    invoke-virtual/range {v14 .. v16}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v14

    const-string v15, "song_list"

    invoke-virtual {v14, v15, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v14

    const-string v15, "offset"

    invoke-virtual {v14, v15, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    .line 82
    .local v8, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "display_name"

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "display_subname"

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 86
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "display_description"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "display_number"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "image_uri"

    invoke-virtual {v14, v13, v15, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "item_display_type"

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 91
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "user_rating_count"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "user_rating"

    const/16 v16, -0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "action_uri"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 95
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "music_duration"

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getDuration()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "music_trackArtist"

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v15, "music_album"

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v13, v15, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;->addRow([Ljava/lang/Object;)V

    .line 101
    add-int/lit8 v7, v7, 0x1

    .line 102
    goto/16 :goto_0
.end method
