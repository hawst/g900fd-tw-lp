.class public Lcom/google/android/music/playback/CompatMediaPlayer;
.super Landroid/media/MediaPlayer;
.source "CompatMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field private mCompatMode:Z

.field private mCompletion:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mNextPlayer:Landroid/media/MediaPlayer;

.field private mSetNextPlayer:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 33
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 28
    iput-boolean v6, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompatMode:Z

    .line 35
    :try_start_0
    const-class v1, Landroid/media/MediaPlayer;

    const-string v2, "setNextMediaPlayer"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/media/MediaPlayer;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mSetNextPlayer:Ljava/lang/reflect/Method;

    .line 37
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompatMode:Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iput-boolean v6, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompatMode:Z

    .line 40
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    goto :goto_0
.end method


# virtual methods
.method public getNextPlayer()Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mNextPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mNextPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 83
    const-wide/16 v0, 0x32

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    .line 84
    iget-object v0, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mNextPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompletion:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompletion:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-interface {v0, p0}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 89
    :cond_1
    return-void
.end method

.method public setNextMediaPlayerCompat(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "next"    # Landroid/media/MediaPlayer;

    .prologue
    .line 50
    iget-boolean v1, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompatMode:Z

    if-eqz v1, :cond_0

    .line 51
    iput-object p1, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mNextPlayer:Landroid/media/MediaPlayer;

    .line 63
    :goto_0
    return-void

    .line 54
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mSetNextPlayer:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 57
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 59
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompatMode:Z

    if-eqz v0, :cond_0

    .line 71
    iput-object p1, p0, Lcom/google/android/music/playback/CompatMediaPlayer;->mCompletion:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    goto :goto_0
.end method
