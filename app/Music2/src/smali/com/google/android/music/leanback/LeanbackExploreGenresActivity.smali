.class public Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
.super Lcom/google/android/music/leanback/LeanbackBrowseActivity;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;,
        Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;,
        Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mGenreId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;-><init>()V

    .line 287
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getRadioItem(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # J

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getFeaturedAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # J

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getNewReleasesAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
    .param p1, "x1"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->prepareSubgenresItem(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->mGenreId:Ljava/lang/String;

    return-object v0
.end method

.method private getArtUriGetterForGenre(Landroid/content/Context;Ljava/lang/String;J)Lcom/google/android/music/leanback/Item$StringGetter;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "genreId"    # Ljava/lang/String;
    .param p3, "seed"    # J

    .prologue
    .line 211
    new-instance v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Landroid/content/Context;Ljava/lang/String;J)V

    return-object v0
.end method

.method private getFeaturedAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .locals 8
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "genreName"    # Ljava/lang/String;
    .param p3, "baseLoaderId"    # I
    .param p4, "baseSeed"    # J

    .prologue
    .line 232
    new-instance v1, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move v5, p3

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$4;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 259
    .local v1, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    return-object v1
.end method

.method private getNewReleasesAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .locals 8
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "genreName"    # Ljava/lang/String;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J

    .prologue
    .line 264
    new-instance v1, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move v5, p3

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$5;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 284
    .local v1, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    return-object v1
.end method

.method private getRadioItem(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/leanback/Item;
    .locals 10
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "genreName"    # Ljava/lang/String;
    .param p3, "position"    # I

    .prologue
    .line 184
    move-object v2, p0

    .line 185
    .local v2, "context":Landroid/content/Context;
    const v0, 0x7f0b0099

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 186
    .local v6, "name":Ljava/lang/String;
    const-wide/16 v0, 0x4d2

    int-to-long v8, p3

    add-long/2addr v0, v8

    invoke-direct {p0, v2, p1, v0, v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getArtUriGetterForGenre(Landroid/content/Context;Ljava/lang/String;J)Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v5

    .line 188
    .local v5, "artUriGetter":Lcom/google/android/music/leanback/Item$StringGetter;
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v0, v6}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v7

    new-instance v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/leanback/Item$StringGetter;)V

    invoke-virtual {v7, v0}, Lcom/google/android/music/leanback/Item$Builder;->intent(Lcom/google/android/music/leanback/Item$IntentGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    const v1, 0x7f0200cf

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method private prepareSubgenresItem(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p1, "genreItemsObjectAdapter"    # Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    .param p2, "genreId"    # Ljava/lang/String;
    .param p3, "genreName"    # Ljava/lang/String;
    .param p4, "position"    # I

    .prologue
    .line 200
    const-wide v4, -0x14bd8d02fL

    int-to-long v6, p4

    add-long/2addr v4, v6

    invoke-direct {p0, p0, p2, v4, v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getArtUriGetterForGenre(Landroid/content/Context;Ljava/lang/String;J)Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v0

    .line 202
    .local v0, "artUriGetter":Lcom/google/android/music/leanback/Item$StringGetter;
    const v3, 0x7f0b00a8

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 203
    .local v2, "name":Ljava/lang/String;
    const v3, 0x7f0b036d

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, p2}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreGenresIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 205
    .local v1, "intent":Landroid/content/Intent;
    new-instance v3, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;->setSubgenreItem(Lcom/google/android/music/leanback/Item;)V

    .line 207
    return-void
.end method


# virtual methods
.method protected getBrowseTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 167
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/music/ui/GenreExploreList;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->mGenreId:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/google/android/music/ui/GenreExploreList;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/music/ui/GenresExploreFragment;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 169
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "genre_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->mGenreId:Ljava/lang/String;

    .line 180
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 181
    return-void
.end method
