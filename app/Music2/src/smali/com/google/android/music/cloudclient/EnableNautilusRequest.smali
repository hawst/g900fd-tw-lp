.class public Lcom/google/android/music/cloudclient/EnableNautilusRequest;
.super Lcom/google/api/client/json/GenericJson;
.source "EnableNautilusRequest.java"


# instance fields
.field public mOfferId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "offer_id"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static serialize(Ljava/lang/String;)[B
    .locals 2
    .param p0, "offerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/music/cloudclient/EnableNautilusRequest;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/EnableNautilusRequest;-><init>()V

    .line 15
    .local v0, "request":Lcom/google/android/music/cloudclient/EnableNautilusRequest;
    iput-object p0, v0, Lcom/google/android/music/cloudclient/EnableNautilusRequest;->mOfferId:Ljava/lang/String;

    .line 17
    invoke-static {v0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v1

    return-object v1
.end method
