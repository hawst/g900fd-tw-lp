.class final Lcom/google/android/music/ui/cardlib/model/Document$1;
.super Ljava/lang/Object;
.source "Document.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/Document;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/ui/cardlib/model/Document;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1156
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 1157
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$002(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$102(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1159
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$202(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$302(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mReason1:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$402(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1163
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mId:J
    invoke-static {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->access$502(Lcom/google/android/music/ui/cardlib/model/Document;J)J

    .line 1164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1165
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J
    invoke-static {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->access$702(Lcom/google/android/music/ui/cardlib/model/Document;J)J

    .line 1166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$802(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1168
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J
    invoke-static {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->access$902(Lcom/google/android/music/ui/cardlib/model/Document;J)J

    .line 1169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1002(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1172
    .local v1, "typeIndex":I
    invoke-static {v1}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->fromOrdinal(I)Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1102(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/model/Document$Type;)Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 1174
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1202(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1302(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1176
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J
    invoke-static {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1402(Lcom/google/android/music/ui/cardlib/model/Document;J)J

    .line 1178
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1502(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1702(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1181
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistSharedState:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1802(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1182
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_0

    move v2, v3

    :goto_0
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$1902(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2002(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2102(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1185
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mSubgenreCount:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2202(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1186
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    :goto_1
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2302(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1187
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_2

    move v2, v3

    :goto_2
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mIsNautilus:Z
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2402(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2502(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2702(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1192
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_3

    move v2, v3

    :goto_3
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mCanAddtoLibrary:Z
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2802(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1193
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_4

    move v2, v3

    :goto_4
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mCanRemoveFromLibrary:Z
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$2902(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1194
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_5

    move v2, v3

    :goto_5
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mCanFollow:Z
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3002(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1196
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mDuration:J
    invoke-static {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3102(Lcom/google/android/music/ui/cardlib/model/Document;J)J

    .line 1198
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J
    invoke-static {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3202(Lcom/google/android/music/ui/cardlib/model/Document;J)J

    .line 1199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemState:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3302(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1200
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerId:J
    invoke-static {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3402(Lcom/google/android/music/ui/cardlib/model/Document;J)J

    .line 1201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerType:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3502(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1202
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerName:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1203
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_6

    move v2, v3

    :goto_6
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mIsFromSearch:Z
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3702(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3802(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1205
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPosition:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$3902(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1206
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4002(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4102(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1208
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mMainstageReason:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4202(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1209
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4302(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1210
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mSourceAccount:I
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4402(Lcom/google/android/music/ui/cardlib/model/Document;I)I

    .line 1211
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mVideoId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4502(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1212
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mVideoThumbnailUrl:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1213
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistEditorArtwork:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4702(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mRadioHighlightColor:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4802(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->access$4902(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;

    .line 1216
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_7

    :goto_7
    # setter for: Lcom/google/android/music/ui/cardlib/model/Document;->mHasSubSituations:Z
    invoke-static {v0, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->access$5002(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z

    .line 1217
    return-object v0

    :cond_0
    move v2, v4

    .line 1182
    goto/16 :goto_0

    :cond_1
    move v2, v4

    .line 1186
    goto/16 :goto_1

    :cond_2
    move v2, v4

    .line 1187
    goto/16 :goto_2

    :cond_3
    move v2, v4

    .line 1192
    goto/16 :goto_3

    :cond_4
    move v2, v4

    .line 1193
    goto/16 :goto_4

    :cond_5
    move v2, v4

    .line 1194
    goto/16 :goto_5

    :cond_6
    move v2, v4

    .line 1203
    goto :goto_6

    :cond_7
    move v3, v4

    .line 1216
    goto :goto_7
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 1153
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/cardlib/model/Document$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 1222
    new-array v0, p1, [Lcom/google/android/music/ui/cardlib/model/Document;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 1153
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/cardlib/model/Document$1;->newArray(I)[Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    return-object v0
.end method
