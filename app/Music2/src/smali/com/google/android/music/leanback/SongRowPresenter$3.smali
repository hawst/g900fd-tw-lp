.class Lcom/google/android/music/leanback/SongRowPresenter$3;
.super Ljava/lang/Object;
.source "SongRowPresenter.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/SongRowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/SongRowPresenter;

.field final synthetic val$selectorView:Landroid/view/View;

.field final synthetic val$songDetailsView:Landroid/view/View;

.field final synthetic val$songRadioView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/SongRowPresenter;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->this$0:Lcom/google/android/music/leanback/SongRowPresenter;

    iput-object p2, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->val$selectorView:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->val$songDetailsView:Landroid/view/View;

    iput-object p4, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->val$songRadioView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->this$0:Lcom/google/android/music/leanback/SongRowPresenter;

    iget-object v1, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->val$selectorView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->val$songDetailsView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/music/leanback/SongRowPresenter$3;->val$songRadioView:Landroid/view/View;

    # invokes: Lcom/google/android/music/leanback/SongRowPresenter;->updateSelector(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/leanback/SongRowPresenter;->access$100(Lcom/google/android/music/leanback/SongRowPresenter;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 117
    return-void
.end method
