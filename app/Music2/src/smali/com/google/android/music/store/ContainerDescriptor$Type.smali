.class public final enum Lcom/google/android/music/store/ContainerDescriptor$Type;
.super Ljava/lang/Enum;
.source "ContainerDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/ContainerDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/store/ContainerDescriptor$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum ALL_SONGS_IN_A_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum ALL_SONGS_MY_LIBRARY:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum ARTIST_SHUFFLE:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum LUCKY_RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum NAUTILUS_ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum NAUTILUS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum NAUTILUS_SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum RECENTLY_ADDED_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum SEARCH_RESULTS:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum SHARED_WITH_ME_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum STORE_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum THUMBS_UP_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum TOP_SONGS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum TOP_SONGS_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum TOP_SONGS_IN_ALL_ACCESS:Lcom/google/android/music/store/ContainerDescriptor$Type;

.field public static final enum UNKNOWN:Lcom/google/android/music/store/ContainerDescriptor$Type;


# instance fields
.field private mDbValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "PLAYLIST"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 28
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "THUMBS_UP_PLAYLIST"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->THUMBS_UP_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 29
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "RECENTLY_ADDED_PLAYLIST"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->RECENTLY_ADDED_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 30
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "STORE_PLAYLIST"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->STORE_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 31
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "SHARED_WITH_ME_PLAYLIST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->SHARED_WITH_ME_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 32
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "ALBUM"

    const/4 v2, 0x5

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 33
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "NAUTILUS_ALBUM"

    const/4 v2, 0x6

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 34
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "ARTIST"

    const/4 v2, 0x7

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 35
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "NAUTILUS_ARTIST"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 36
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "RADIO"

    const/16 v2, 0x9

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 37
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "LUCKY_RADIO"

    const/16 v2, 0xa

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 38
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "ALL_SONGS_MY_LIBRARY"

    const/16 v2, 0xb

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALL_SONGS_MY_LIBRARY:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 39
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "ALL_SONGS_IN_A_GENRE"

    const/16 v2, 0xc

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALL_SONGS_IN_A_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 40
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "ARTIST_SHUFFLE"

    const/16 v2, 0xd

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->ARTIST_SHUFFLE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 41
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "TOP_SONGS_ARTIST"

    const/16 v2, 0xe

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 42
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "TOP_SONGS_GENRE"

    const/16 v2, 0xf

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 43
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "TOP_SONGS_IN_ALL_ACCESS"

    const/16 v2, 0x10

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_IN_ALL_ACCESS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 44
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "SEARCH_RESULTS"

    const/16 v2, 0x11

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->SEARCH_RESULTS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 45
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "SINGLE_SONG"

    const/16 v2, 0x12

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 46
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "NAUTILUS_SINGLE_SONG"

    const/16 v2, 0x13

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 47
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/ContainerDescriptor$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->UNKNOWN:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 26
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/google/android/music/store/ContainerDescriptor$Type;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->THUMBS_UP_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->RECENTLY_ADDED_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->STORE_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->SHARED_WITH_ME_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALL_SONGS_MY_LIBRARY:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALL_SONGS_IN_A_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->ARTIST_SHUFFLE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_IN_ALL_ACCESS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->SEARCH_RESULTS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->UNKNOWN:Lcom/google/android/music/store/ContainerDescriptor$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->$VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "dbValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lcom/google/android/music/store/ContainerDescriptor$Type;->mDbValue:I

    .line 53
    return-void
.end method

.method public static fromDBValue(I)Lcom/google/android/music/store/ContainerDescriptor$Type;
    .locals 7
    .param p0, "value"    # I

    .prologue
    .line 60
    # getter for: Lcom/google/android/music/store/ContainerDescriptor;->TYPE_VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->access$000()[Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/store/ContainerDescriptor$Type;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 61
    .local v3, "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    iget v4, v3, Lcom/google/android/music/store/ContainerDescriptor$Type;->mDbValue:I

    if-ne v4, p0, :cond_0

    .line 62
    return-object v3

    .line 60
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    .end local v3    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static isDynamic(I)Z
    .locals 1
    .param p0, "dbValue"    # I

    .prologue
    .line 69
    packed-switch p0, :pswitch_data_0

    .line 79
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 77
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/store/ContainerDescriptor$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/store/ContainerDescriptor$Type;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->$VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-virtual {v0}, [Lcom/google/android/music/store/ContainerDescriptor$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/store/ContainerDescriptor$Type;

    return-object v0
.end method


# virtual methods
.method public getDBValue()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/music/store/ContainerDescriptor$Type;->mDbValue:I

    return v0
.end method

.method public isRadio()Z
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/music/store/ContainerDescriptor$2;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {p0}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 89
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 87
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
