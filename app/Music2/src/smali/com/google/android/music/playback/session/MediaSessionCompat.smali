.class public Lcom/google/android/music/playback/session/MediaSessionCompat;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/session/MediaSessionCompat$1;,
        Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;,
        Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;,
        Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;,
        Lcom/google/android/music/playback/session/MediaSessionCompat$Token;,
        Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;,
        Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
    }
.end annotation


# instance fields
.field private final mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/os/Looper;Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/app/PendingIntent;
    .param p4, "looper"    # Landroid/os/Looper;
    .param p5, "mediaButtonReceiver"    # Landroid/content/ComponentName;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    if-nez p1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "tag must not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    .line 62
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplApi21;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)V

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_2
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;

    invoke-direct {v0, p1, p3, p4, p5}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;-><init>(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/os/Looper;Landroid/content/ComponentName;)V

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    goto :goto_0
.end method


# virtual methods
.method public getMediaSession()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->getMediaSession()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteControlClient()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->getRemoteControlClient()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSessionToken()Lcom/google/android/music/playback/session/MediaSessionCompat$Token;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->getSessionToken()Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    move-result-object v0

    return-object v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->release()V

    .line 178
    return-void
.end method

.method public setActive(Z)V
    .locals 1
    .param p1, "active"    # Z

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setActive(Z)V

    .line 142
    return-void
.end method

.method public setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;)V
    .locals 1
    .param p1, "callback"    # Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Landroid/os/Handler;)V

    .line 78
    return-void
.end method

.method public setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Landroid/os/Handler;)V
    .locals 1
    .param p1, "callback"    # Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    if-eqz p2, :cond_0

    .end local p2    # "handler":Landroid/os/Handler;
    :goto_0
    invoke-interface {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Landroid/os/Handler;)V

    .line 90
    return-void

    .line 89
    .restart local p2    # "handler":Landroid/os/Handler;
    :cond_0
    new-instance p2, Landroid/os/Handler;

    .end local p2    # "handler":Landroid/os/Handler;
    invoke-direct {p2}, Landroid/os/Handler;-><init>()V

    goto :goto_0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setExtras(Landroid/os/Bundle;)V

    .line 227
    return-void
.end method

.method public setFlags(I)V
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setFlags(I)V

    .line 102
    return-void
.end method

.method public setMetadata(Lcom/google/android/music/playback/session/MediaMetadataCompat;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/music/playback/session/MediaMetadataCompat;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setMetadata(Lcom/google/android/music/playback/session/MediaMetadataCompat;)V

    .line 236
    return-void
.end method

.method public setPlaybackState(Lcom/google/android/music/playback/session/PlaybackStateCompat;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/music/playback/session/PlaybackStateCompat;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setPlaybackState(Lcom/google/android/music/playback/session/PlaybackStateCompat;)V

    .line 202
    return-void
.end method

.method public setQueue(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;>;"
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setQueue(Ljava/util/List;)V

    .line 212
    return-void
.end method

.method public setQueueTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setQueueTitle(Ljava/lang/CharSequence;)V

    .line 220
    return-void
.end method

.method public setSessionActivity(Landroid/app/PendingIntent;)V
    .locals 1
    .param p1, "pi"    # Landroid/app/PendingIntent;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat;->mImpl:Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;->setSessionActivity(Landroid/app/PendingIntent;)V

    .line 275
    return-void
.end method
