.class public Lcom/google/android/music/dial/model/SkipToItemCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "SkipToItemCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 39
    iget-object v0, p0, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "session:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "skipToItem"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 42
    new-instance v0, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    .line 43
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
