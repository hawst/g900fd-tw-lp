.class public interface abstract Lcom/google/android/music/download/cache/FilteredFileDeleter;
.super Ljava/lang/Object;
.source "FilteredFileDeleter.java"


# virtual methods
.method public abstract getFilteredIds()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation
.end method

.method public abstract requestDeleteFile(Ljava/io/File;)Z
.end method
