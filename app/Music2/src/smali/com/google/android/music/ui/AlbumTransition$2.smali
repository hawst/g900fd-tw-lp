.class Lcom/google/android/music/ui/AlbumTransition$2;
.super Ljava/lang/Object;
.source "AlbumTransition.java"

# interfaces
.implements Landroid/transition/Transition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AlbumTransition;->customizeEnterTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/AlbumTransition;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/AlbumTransition;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionCancel(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "arg0"    # Landroid/transition/Transition;

    .prologue
    .line 414
    return-void
.end method

.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 9
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 370
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 371
    .local v0, "cardIn":Landroid/transition/TransitionSet;
    const-wide/16 v6, 0xc8

    invoke-virtual {v0, v6, v7}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    .line 373
    new-instance v2, Landroid/transition/Fade;

    invoke-direct {v2}, Landroid/transition/Fade;-><init>()V

    .line 374
    .local v2, "fadeInRest":Landroid/transition/Fade;
    iget-object v5, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    iget-object v6, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I
    invoke-static {v6}, Lcom/google/android/music/ui/AlbumTransition;->access$1000(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v6

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static {v5, v6}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v3

    .line 375
    .local v3, "metadataParent":Landroid/view/View;
    invoke-virtual {v2, v3}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 376
    iget-object v5, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mActionBarId:I
    invoke-static {v5}, Lcom/google/android/music/ui/AlbumTransition;->access$1100(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/transition/Fade;->addTarget(I)Landroid/transition/Transition;

    .line 377
    invoke-virtual {v0, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 380
    iget-object v5, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    iget-object v6, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mFabId:I
    invoke-static {v6}, Lcom/google/android/music/ui/AlbumTransition;->access$600(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v6

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static {v5, v6}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v1

    .line 381
    .local v1, "fab":Landroid/view/View;
    new-instance v4, Landroid/transition/ChangeTransform;

    invoke-direct {v4}, Landroid/transition/ChangeTransform;-><init>()V

    .line 382
    .local v4, "scaleFab":Landroid/transition/ChangeTransform;
    invoke-virtual {v4, v1}, Landroid/transition/ChangeTransform;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 383
    invoke-virtual {v0, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 386
    new-instance v5, Lcom/google/android/music/ui/AlbumTransition$2$1;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/AlbumTransition$2$1;-><init>(Lcom/google/android/music/ui/AlbumTransition$2;)V

    invoke-virtual {v0, v5}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 405
    iget-object v5, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;
    invoke-static {v5}, Lcom/google/android/music/ui/AlbumTransition;->access$400(Lcom/google/android/music/ui/AlbumTransition;)Lcom/google/android/music/ui/BaseActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/ui/BaseActivity;->getContentContainer()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-static {v5, v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 408
    iget-object v5, p0, Lcom/google/android/music/ui/AlbumTransition$2;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    const/4 v6, 0x1

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V
    invoke-static {v5, v3, v6}, Lcom/google/android/music/ui/AlbumTransition;->access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V

    .line 409
    invoke-static {v1, v8}, Landroid/support/v4/view/ViewCompat;->setScaleX(Landroid/view/View;F)V

    .line 410
    invoke-static {v1, v8}, Landroid/support/v4/view/ViewCompat;->setScaleY(Landroid/view/View;F)V

    .line 411
    return-void
.end method

.method public onTransitionPause(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "arg0"    # Landroid/transition/Transition;

    .prologue
    .line 366
    return-void
.end method

.method public onTransitionResume(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "arg0"    # Landroid/transition/Transition;

    .prologue
    .line 362
    return-void
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 0
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 358
    return-void
.end method
