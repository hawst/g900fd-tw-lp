.class final Lcom/google/android/music/dial/RemoteService$1;
.super Ljava/lang/Object;
.source "RemoteService.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/RemoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/dial/RemoteService;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/dial/RemoteService;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/music/dial/RemoteService$Builder;

    invoke-direct {v0}, Lcom/google/android/music/dial/RemoteService$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/dial/RemoteService$Builder;->setServiceType(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/dial/RemoteService$Builder;->setServiceId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/dial/RemoteService$Builder;->setControlUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/dial/RemoteService$Builder;->setEventSubUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/dial/RemoteService$Builder;->setScpdUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/dial/RemoteService$Builder;->build()Lcom/google/android/music/dial/RemoteService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/google/android/music/dial/RemoteService$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/dial/RemoteService;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/dial/RemoteService;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 96
    new-array v0, p1, [Lcom/google/android/music/dial/RemoteService;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/google/android/music/dial/RemoteService$1;->newArray(I)[Lcom/google/android/music/dial/RemoteService;

    move-result-object v0

    return-object v0
.end method
