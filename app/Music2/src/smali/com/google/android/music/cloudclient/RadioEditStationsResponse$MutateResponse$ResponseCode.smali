.class public final enum Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;
.super Ljava/lang/Enum;
.source "RadioEditStationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResponseCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

.field public static final enum CONFLICT:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

.field public static final enum INVALID_REQUEST:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

.field public static final enum OK:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

.field public static final enum TOO_MANY_ITEMS:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->OK:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    .line 19
    new-instance v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    const-string v1, "CONFLICT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->CONFLICT:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    .line 20
    new-instance v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    const-string v1, "INVALID_REQUEST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->INVALID_REQUEST:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    .line 21
    new-instance v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    const-string v1, "TOO_MANY_ITEMS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->TOO_MANY_ITEMS:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    .line 17
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    sget-object v1, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->OK:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->CONFLICT:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->INVALID_REQUEST:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->TOO_MANY_ITEMS:Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->$VALUES:[Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->$VALUES:[Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    invoke-virtual {v0}, [Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse$ResponseCode;

    return-object v0
.end method
