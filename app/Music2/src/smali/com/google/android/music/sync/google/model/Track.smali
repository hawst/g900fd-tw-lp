.class public Lcom/google/android/music/sync/google/model/Track;
.super Lcom/google/android/music/cloudclient/TrackJson;
.source "Track.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;


# instance fields
.field private mMusicFile:Lcom/google/android/music/store/MusicFile;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/music/cloudclient/TrackJson;-><init>()V

    return-void
.end method

.method public static createTrackFromTrackJson(Lcom/google/android/music/cloudclient/TrackJson;)Lcom/google/android/music/sync/google/model/Track;
    .locals 4
    .param p0, "trackJson"    # Lcom/google/android/music/cloudclient/TrackJson;

    .prologue
    .line 192
    new-instance v0, Lcom/google/android/music/sync/google/model/Track;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/Track;-><init>()V

    .line 193
    .local v0, "track":Lcom/google/android/music/sync/google/model/Track;
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbum:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mAlbum:Ljava/lang/String;

    .line 194
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtist:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtist:Ljava/lang/String;

    .line 195
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtRef:Ljava/util/List;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtRef:Ljava/util/List;

    .line 196
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mAlbumId:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtist:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mArtist:Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistArtRef:Ljava/util/List;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mArtistArtRef:Ljava/util/List;

    .line 199
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistId:Ljava/util/List;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mArtistId:Ljava/util/List;

    .line 200
    iget v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mBeatsPerMinute:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mBeatsPerMinute:I

    .line 201
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mClientId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mClientId:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mComment:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mComment:Ljava/lang/String;

    .line 203
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mComposer:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mComposer:Ljava/lang/String;

    .line 204
    iget-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mCreationTimestamp:J

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/Track;->mCreationTimestamp:J

    .line 205
    iget v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDiscNumber:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mDiscNumber:I

    .line 206
    iget-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDurationMillis:J

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/Track;->mDurationMillis:J

    .line 207
    iget-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mEstimatedSize:J

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/Track;->mEstimatedSize:J

    .line 208
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mGenre:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mGenre:Ljava/lang/String;

    .line 209
    iget-boolean v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mIsDeleted:Z

    iput-boolean v1, v0, Lcom/google/android/music/sync/google/model/Track;->mIsDeleted:Z

    .line 210
    iget-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mLastModifiedTimestamp:J

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    .line 211
    iget-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mLastRatingChangeTimestamp:J

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/Track;->mLastRatingChangeTimestamp:J

    .line 212
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mNautilusId:Ljava/lang/String;

    .line 213
    iget v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPlayCount:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mPlayCount:I

    .line 214
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    .line 215
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRating:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mRating:Ljava/lang/String;

    .line 216
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRemoteId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    .line 217
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mStoreId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mStoreId:Ljava/lang/String;

    .line 218
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTitle:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTitle:Ljava/lang/String;

    .line 219
    iget v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTotalDiscCount:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTotalDiscCount:I

    .line 220
    iget-boolean v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForPurchase:Z

    iput-boolean v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTrackAvailableForPurchase:Z

    .line 221
    iget-boolean v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForSubscription:Z

    iput-boolean v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTrackAvailableForSubscription:Z

    .line 222
    iget v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackNumber:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTrackNumber:I

    .line 223
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackOrigin:Ljava/util/List;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTrackOrigin:Ljava/util/List;

    .line 224
    iget v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackType:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    .line 225
    iget v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mYear:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mYear:I

    .line 226
    return-object v0
.end method

.method public static filterForRatingsUpdate(Lcom/google/android/music/sync/google/model/Track;)Lcom/google/android/music/sync/google/model/Track;
    .locals 4
    .param p0, "track"    # Lcom/google/android/music/sync/google/model/Track;

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->validateForUpstreamUpdate()V

    .line 295
    new-instance v0, Lcom/google/android/music/sync/google/model/Track;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/Track;-><init>()V

    .line 296
    .local v0, "updateTrack":Lcom/google/android/music/sync/google/model/Track;
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    .line 301
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/sync/google/model/Track;->mRating:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mRating:Ljava/lang/String;

    .line 304
    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    .line 305
    iget v1, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    iput v1, v0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    .line 306
    return-object v0

    .line 299
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/Track;->mNautilusId:Ljava/lang/String;

    goto :goto_0
.end method

.method public static parse(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/sync/google/model/Track;
    .locals 14
    .param p0, "file"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    const-wide/16 v12, 0x0

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 232
    new-instance v2, Lcom/google/android/music/sync/google/model/Track;

    invoke-direct {v2}, Lcom/google/android/music/sync/google/model/Track;-><init>()V

    .line 233
    .local v2, "track":Lcom/google/android/music/sync/google/model/Track;
    iput-object p0, v2, Lcom/google/android/music/sync/google/model/Track;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 234
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSourceType()I

    move-result v3

    if-ne v3, v9, :cond_2

    .line 235
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    .line 240
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSourceVersion()Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "sourceVersion":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 242
    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    .end local v1    # "sourceVersion":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mTitle:Ljava/lang/String;

    .line 254
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackArtist()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mArtist:Ljava/lang/String;

    .line 255
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getComposer()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mComposer:Ljava/lang/String;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mAlbum:Ljava/lang/String;

    .line 257
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtist:Ljava/lang/String;

    .line 258
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getYear()S

    move-result v3

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mYear:I

    .line 259
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackPositionInAlbum()S

    move-result v3

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mTrackNumber:I

    .line 260
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getGenre()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mGenre:Ljava/lang/String;

    .line 261
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/music/sync/google/model/Track;->mDurationMillis:J

    .line 262
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSize()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/music/sync/google/model/Track;->mEstimatedSize:J

    .line 263
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getDiscPosition()S

    move-result v3

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mDiscNumber:I

    .line 264
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getDiscCount()S

    move-result v3

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mTotalDiscCount:I

    .line 265
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v3

    if-eq v3, v9, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v3

    if-ne v3, v10, :cond_4

    .line 268
    :cond_1
    const/4 v3, 0x6

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    .line 278
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getRating()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mRating:Ljava/lang/String;

    .line 279
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getStoreSongId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mStoreId:Ljava/lang/String;

    .line 280
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mAlbumId:Ljava/lang/String;

    .line 281
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getClientId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mClientId:Ljava/lang/String;

    .line 282
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getRatingTimestampMicrosec()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/music/sync/google/model/Track;->mLastRatingChangeTimestamp:J

    .line 283
    return-object v2

    .line 236
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSourceType()I

    move-result v3

    if-ne v3, v10, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/sync/google/model/Track;->mNautilusId:Ljava/lang/String;

    goto/16 :goto_0

    .line 244
    .restart local v1    # "sourceVersion":Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 246
    .end local v1    # "sourceVersion":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v3, "MusicSyncAdapter"

    const-string v4, "Non-numeric version for music file with remoteId=%s.  Replacing with 0."

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 250
    iput-wide v12, v2, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    goto/16 :goto_1

    .line 269
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v3

    if-ne v3, v8, :cond_5

    .line 270
    iput v11, v2, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    goto :goto_2

    .line 271
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v3

    if-ne v3, v11, :cond_6

    .line 272
    const/4 v3, 0x7

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    goto :goto_2

    .line 273
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_7

    .line 274
    const/16 v3, 0x8

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    goto :goto_2

    .line 276
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v3

    iput v3, v2, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    goto :goto_2
.end method


# virtual methods
.method public formatAsMusicFile(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;
    .locals 10
    .param p1, "file"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 31
    if-nez p1, :cond_0

    .line 32
    new-instance p1, Lcom/google/android/music/store/MusicFile;

    .end local p1    # "file":Lcom/google/android/music/store/MusicFile;
    invoke-direct {p1}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 34
    .restart local p1    # "file":Lcom/google/android/music/store/MusicFile;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getEffectiveRemoteId()Ljava/lang/String;

    move-result-object v4

    .line 35
    .local v4, "remoteId":Ljava/lang/String;
    if-eqz v4, :cond_18

    .line 36
    invoke-virtual {p1, v4}, Lcom/google/android/music/store/MusicFile;->setSourceId(Ljava/lang/String;)V

    .line 41
    :goto_0
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTitle:Ljava/lang/String;

    if-eqz v5, :cond_19

    .line 42
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTitle(Ljava/lang/String;)V

    .line 46
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtist:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 47
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtist:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackArtist(Ljava/lang/String;)V

    .line 49
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mComposer:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 50
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mComposer:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setComposer(Ljava/lang/String;)V

    .line 52
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbum:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 53
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbum:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setAlbumName(Ljava/lang/String;)V

    .line 55
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtist:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 56
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;)V

    .line 58
    :cond_4
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mYear:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_5

    .line 59
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mYear:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 60
    .local v0, "n":Ljava/lang/Number;
    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setYear(S)V

    .line 62
    .end local v0    # "n":Ljava/lang/Number;
    :cond_5
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackNumber:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_6

    .line 63
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackNumber:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 64
    .restart local v0    # "n":Ljava/lang/Number;
    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackPositionInAlbum(S)V

    .line 66
    .end local v0    # "n":Ljava/lang/Number;
    :cond_6
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mGenre:Ljava/lang/String;

    if-eqz v5, :cond_7

    .line 67
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mGenre:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setGenre(Ljava/lang/String;)V

    .line 69
    :cond_7
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mDurationMillis:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_8

    .line 70
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mDurationMillis:J

    invoke-virtual {p1, v6, v7}, Lcom/google/android/music/store/MusicFile;->setDurationInMilliSec(J)V

    .line 72
    :cond_8
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mCreationTimestamp:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_9

    .line 75
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mCreationTimestamp:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {p1, v6, v7}, Lcom/google/android/music/store/MusicFile;->setAddedTime(J)V

    .line 77
    :cond_9
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_a

    .line 78
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setSourceVersion(Ljava/lang/String;)V

    .line 81
    :cond_a
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mEstimatedSize:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_b

    .line 82
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mEstimatedSize:J

    invoke-virtual {p1, v6, v7}, Lcom/google/android/music/store/MusicFile;->setSize(J)V

    .line 85
    :cond_b
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTotalDiscCount:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_c

    .line 86
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTotalDiscCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 87
    .restart local v0    # "n":Ljava/lang/Number;
    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setDiscCount(S)V

    .line 90
    .end local v0    # "n":Ljava/lang/Number;
    :cond_c
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mDiscNumber:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_d

    .line 91
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mDiscNumber:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 92
    .restart local v0    # "n":Ljava/lang/Number;
    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setDiscPosition(S)V

    .line 95
    .end local v0    # "n":Ljava/lang/Number;
    :cond_d
    const/4 v2, -0x1

    .line 96
    .local v2, "originValue":I
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackOrigin:Ljava/util/List;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackOrigin:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_e

    .line 97
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackOrigin:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/TrackJson$TrackOrigin;

    .line 98
    .local v1, "origin":Lcom/google/android/music/cloudclient/TrackJson$TrackOrigin;
    iget v5, v1, Lcom/google/android/music/cloudclient/TrackJson$TrackOrigin;->mValue:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_e

    .line 99
    const/4 v2, 0x4

    .line 103
    .end local v1    # "origin":Lcom/google/android/music/cloudclient/TrackJson$TrackOrigin;
    :cond_e
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1f

    .line 108
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    const/4 v6, 0x6

    if-ne v5, v6, :cond_1b

    .line 109
    const/4 v5, 0x4

    if-ne v2, v5, :cond_1a

    const/4 v5, 0x3

    :goto_2
    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackType(I)V

    .line 129
    :cond_f
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getRatingAsInt()I

    move-result v3

    .line 130
    .local v3, "rating":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_10

    .line 131
    invoke-virtual {p1, v3}, Lcom/google/android/music/store/MusicFile;->setRating(I)V

    .line 136
    :cond_10
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtRef:Ljava/util/List;

    if-eqz v5, :cond_11

    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtRef:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_11

    .line 137
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtRef:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setAlbumArtLocation(Ljava/lang/String;)V

    .line 140
    :cond_11
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtistArtRef:Ljava/util/List;

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtistArtRef:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_12

    .line 141
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtistArtRef:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setArtistArtLocation(Ljava/lang/String;)V

    .line 144
    :cond_12
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mStoreId:Ljava/lang/String;

    if-eqz v5, :cond_13

    .line 145
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mStoreId:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setStoreSongId(Ljava/lang/String;)V

    .line 148
    :cond_13
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbumId:Ljava/lang/String;

    if-eqz v5, :cond_14

    .line 149
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mAlbumId:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 152
    :cond_14
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setFileType(I)V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setDomain(Lcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 155
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mClientId:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setClientId(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v5

    if-eqz v5, :cond_20

    .line 158
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setSourceType(I)V

    .line 165
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackMetajamId(Ljava/lang/String;)V

    .line 168
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtistId:Ljava/util/List;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtistId:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_15

    .line 169
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mArtistId:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setArtistMetajamId(Ljava/lang/String;)V

    .line 172
    :cond_15
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mLastRatingChangeTimestamp:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_16

    .line 173
    iget-wide v6, p0, Lcom/google/android/music/sync/google/model/Track;->mLastRatingChangeTimestamp:J

    invoke-virtual {p1, v6, v7}, Lcom/google/android/music/store/MusicFile;->setRatingTimestampMicrosec(J)V

    .line 175
    :cond_16
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    if-eqz v5, :cond_17

    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_17

    .line 176
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mId:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setVid(Ljava/lang/String;)V

    .line 178
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    if-eqz v5, :cond_17

    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_17

    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_17

    .line 180
    iget-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setVThumbnailUrl(Ljava/lang/String;)V

    .line 183
    :cond_17
    return-object p1

    .line 38
    .end local v2    # "originValue":I
    .end local v3    # "rating":I
    :cond_18
    const-string v5, "MusicSyncAdapter"

    const-string v6, "Exporting a track to a music file, but no canonical id defined."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 44
    :cond_19
    const-string v5, ""

    iput-object v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTitle:Ljava/lang/String;

    goto/16 :goto_1

    .line 109
    .restart local v2    # "originValue":I
    :cond_1a
    const/4 v5, 0x2

    goto/16 :goto_2

    .line 112
    :cond_1b
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_1d

    .line 113
    const/4 v5, 0x4

    if-ne v2, v5, :cond_1c

    const/4 v5, 0x3

    :goto_5
    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackType(I)V

    goto/16 :goto_3

    :cond_1c
    const/4 v5, 0x1

    goto :goto_5

    .line 116
    :cond_1d
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    const/4 v6, 0x7

    if-ne v5, v6, :cond_1e

    .line 117
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackType(I)V

    goto/16 :goto_3

    .line 118
    :cond_1e
    iget v5, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    const/16 v6, 0x8

    if-ne v5, v6, :cond_f

    .line 119
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackType(I)V

    goto/16 :goto_3

    .line 124
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v5, v6, :cond_f

    .line 125
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setTrackType(I)V

    goto/16 :goto_3

    .line 162
    .restart local v3    # "rating":I
    :cond_20
    const/4 v5, 0x3

    invoke-virtual {p1, v5}, Lcom/google/android/music/store/MusicFile;->setSourceType(I)V

    goto/16 :goto_4
.end method

.method public getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 359
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forTracksBatchMutation()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getCreationTimestamp()J
    .locals 2

    .prologue
    .line 387
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/Track;->mCreationTimestamp:J

    return-wide v0
.end method

.method public getEncapsulatedMusicFile()Lcom/google/android/music/store/MusicFile;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/Track;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    return-object v0
.end method

.method public getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 349
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->getMaxAlbumArtSize(Landroid/content/Context;)I

    move-result v0

    .line 350
    .local v0, "dimension":I
    invoke-static {v0}, Lcom/google/android/music/sync/api/MusicUrl;->forTracksFeed(I)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    return-object v1
.end method

.method public getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 354
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->getMaxAlbumArtSize(Landroid/content/Context;)I

    move-result v0

    .line 355
    .local v0, "dimension":I
    invoke-static {v0}, Lcom/google/android/music/sync/api/MusicUrl;->forTracksFeedAsPost(I)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    return-object v1
.end method

.method public getLastModifiedTimestamp()J
    .locals 2

    .prologue
    .line 379
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    return-wide v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/Track;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/Track;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v0}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getRatingTimestamp()J
    .locals 2

    .prologue
    .line 395
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/Track;->mLastRatingChangeTimestamp:J

    return-wide v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 344
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->getMaxAlbumArtSize(Landroid/content/Context;)I

    move-result v0

    .line 345
    .local v0, "dimension":I
    invoke-static {p2, v0}, Lcom/google/android/music/sync/api/MusicUrl;->forTrack(Ljava/lang/String;I)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    return-object v1
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/Track;->mIsDeleted:Z

    return v0
.end method

.method public isInsert()Z
    .locals 2

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/sync/google/model/Track;->mTrackType:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdate()Z
    .locals 1

    .prologue
    .line 415
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/Track;->mIsDeleted:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasNautilusId()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serializeAsJson()[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 336
    :try_start_0
    invoke-static {p0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MusicSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to serialize a track as JSON: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 339
    new-instance v1, Lcom/google/android/music/store/InvalidDataException;

    const-string v2, "Unable to serialize track for upstream sync."

    invoke-direct {v1, v2, v0}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setCreationTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 391
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/Track;->mCreationTimestamp:J

    .line 392
    return-void
.end method

.method public setIsDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 407
    iput-boolean p1, p0, Lcom/google/android/music/sync/google/model/Track;->mIsDeleted:Z

    .line 408
    return-void
.end method

.method public setLastModifiedTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 383
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    .line 384
    return-void
.end method

.method public setNautilusId(Ljava/lang/String;)V
    .locals 0
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/Track;->mNautilusId:Ljava/lang/String;

    .line 376
    return-void
.end method

.method public setRatingTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 399
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/Track;->mLastRatingChangeTimestamp:J

    .line 400
    return-void
.end method

.method public setRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 371
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    .line 372
    return-void
.end method

.method public validateForUpstreamDelete()V
    .locals 2

    .prologue
    .line 329
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Track deletes should be done through TrackTombstone."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public validateForUpstreamInsert()V
    .locals 2

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasNautilusId()Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only nautilus track can sync upstream."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_1
    return-void
.end method

.method public validateForUpstreamUpdate()V
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->hasNautilusId()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getRatingAsInt()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/Track;->getRatingAsInt()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_2

    .line 324
    :cond_1
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid track for upstream update."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_2
    return-void
.end method

.method public wipeAllFields()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Lcom/google/android/music/cloudclient/TrackJson;->wipeAllFields()V

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/Track;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 189
    return-void
.end method
