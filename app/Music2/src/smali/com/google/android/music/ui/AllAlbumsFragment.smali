.class public Lcom/google/android/music/ui/AllAlbumsFragment;
.super Lcom/google/android/music/ui/AlbumGridFragment;
.source "AllAlbumsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/music/ui/AlbumGridFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getSupportsFastScroll()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method protected init(Lcom/google/android/music/medialist/AlbumList;)V
    .locals 2
    .param p1, "albumlist"    # Lcom/google/android/music/medialist/AlbumList;

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->PROJECTION:[Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/ui/AllAlbumsFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 30
    return-void
.end method

.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Lcom/google/android/music/ui/AlbumGridFragment;->initEmptyScreen()V

    .line 45
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const v0, 0x7f0b02a4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllAlbumsFragment;->setEmptyScreenText(I)V

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllAlbumsFragment;->setEmptyScreenLearnMoreVisible(Z)V

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    const v0, 0x7f0b02a5

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllAlbumsFragment;->setEmptyScreenText(I)V

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllAlbumsFragment;->setEmptyScreenLearnMoreVisible(Z)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllAlbumsFragment;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/google/android/music/medialist/AllAlbumsList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllAlbumsList;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllAlbumsFragment;->init(Lcom/google/android/music/medialist/AlbumList;)V

    .line 22
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/music/ui/AlbumGridFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 23
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/AlbumGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllAlbumsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/ui/AllAlbumsFragment;->getSupportsFastScroll()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllAlbumsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllAlbumsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    .line 40
    :cond_0
    return-void
.end method
