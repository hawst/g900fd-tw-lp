.class Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
.super Ljava/lang/Object;
.source "NowPlayingWidgetUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/NowPlayingWidgetUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MetaData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;,
        Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;
    }
.end annotation


# instance fields
.field public final album:Ljava/lang/String;

.field public final albumId:J

.field public final art:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

.field public final artist:Ljava/lang/String;

.field public final isEmpty:Z

.field public final state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

.field public final thumbs:I

.field public final track:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Intent;Landroid/content/Context;I)V
    .locals 6
    .param p1, "musicState"    # Landroid/content/Intent;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layoutId"    # I

    .prologue
    .line 397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399
    const-string v2, "track"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->track:Ljava/lang/String;

    .line 401
    iget-object v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->track:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->isEmpty:Z

    .line 403
    const-string v2, "artist"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 404
    .local v1, "artist":Ljava/lang/String;
    const-string v2, "album"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406
    .local v0, "album":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 407
    const v2, 0x7f0b00c4

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 409
    :cond_0
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 410
    const v2, 0x7f0b00c5

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 413
    :cond_1
    iput-object v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->artist:Ljava/lang/String;

    .line 414
    iput-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->album:Ljava/lang/String;

    .line 416
    const-string v2, "albumId"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->albumId:J

    .line 417
    const-string v2, "rating"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/music/RatingSelector;->convertRatingToThumbs(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->thumbs:I

    .line 421
    new-instance v2, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

    invoke-direct {v2, p1}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;-><init>(Landroid/content/Intent;)V

    iput-object v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->art:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

    .line 422
    new-instance v2, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;-><init>(Landroid/content/Intent;Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    .line 423
    return-void
.end method
