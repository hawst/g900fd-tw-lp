.class Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor$1;
.super Ljava/lang/Object;
.source "ArtLoaderImpl.java"

# interfaces
.implements Ljava/util/concurrent/RejectedExecutionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public rejectedExecution(Ljava/lang/Runnable;Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "executor"    # Ljava/util/concurrent/ThreadPoolExecutor;

    .prologue
    .line 315
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;

    .line 316
    .local v0, "requestRunnable":Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;->notifyListener(Landroid/os/ParcelFileDescriptor;)V

    .line 317
    return-void
.end method
