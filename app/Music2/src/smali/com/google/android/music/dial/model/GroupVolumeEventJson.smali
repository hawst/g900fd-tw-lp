.class public Lcom/google/android/music/dial/model/GroupVolumeEventJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GroupVolumeEventJson.java"


# instance fields
.field public mMuted:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "muted"
    .end annotation
.end field

.field public mVolume:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "volume"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
