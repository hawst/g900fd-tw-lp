.class public final Lcom/google/android/music/MusicUrlHandler;
.super Lcom/google/android/music/ui/BaseActivity;
.source "MusicUrlHandler.java"

# interfaces
.implements Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;


# instance fields
.field private mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method private setShowSongzaCard(Landroid/net/Uri;)V
    .locals 3
    .param p1, "linkUri"    # Landroid/net/Uri;

    .prologue
    .line 162
    const-string v1, "1"

    const-string v2, "songza"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 164
    .local v0, "showSignUpCard":Z
    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/music/MusicUrlHandler;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setShowSongzaWelcomeCard(Z)V

    .line 167
    :cond_0
    return-void
.end method


# virtual methods
.method protected doesSupportDownloadOnlyBanner()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public onAlbumError()V
    .locals 2

    .prologue
    .line 178
    const v0, 0x7f0b0313

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 180
    invoke-virtual {p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    .line 181
    return-void
.end method

.method public onArtistError()V
    .locals 2

    .prologue
    .line 185
    const v0, 0x7f0b0314

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    .line 188
    return-void
.end method

.method public onConnectionError()V
    .locals 2

    .prologue
    .line 192
    const v0, 0x7f0b0310

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    .line 195
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super/range {p0 .. p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 54
    .local v5, "i":Landroid/content/Intent;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 56
    :cond_0
    const-string v15, "MusicUrlHandler"

    const-string v16, "Received intent doesn\'t have a URL."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    .line 59
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/music/MusicUrlHandler;->mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 61
    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 62
    .local v7, "linkUri":Landroid/net/Uri;
    invoke-virtual {v7}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v10

    .line 63
    .local v10, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/MusicUrlHandler;->mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMusicAppLink(Ljava/lang/String;)V

    .line 65
    const-string v16, "music"

    const/4 v15, 0x0

    invoke-interface {v10, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 66
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 68
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;Z)V

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    .line 158
    :cond_2
    :goto_0
    return-void

    .line 70
    :cond_3
    const-string v16, "listen"

    const/4 v15, 0x1

    invoke-interface {v10, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 72
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/music/MusicUrlHandler;->setShowSongzaCard(Landroid/net/Uri;)V

    .line 74
    const-string v15, "1"

    const-string v16, "signup"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 77
    const-string v15, "coupon"

    invoke-virtual {v7, v15}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "coupon":Ljava/lang/String;
    const-string v15, "coupontype"

    invoke-virtual {v7, v15}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 81
    .local v4, "couponType":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 84
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 87
    const v15, 0x7f0b0319

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/music/MusicUrlHandler;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v15, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v15

    invoke-virtual {v15}, Landroid/widget/Toast;->show()V

    .line 90
    :cond_4
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    .line 91
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    goto :goto_0

    .line 94
    :cond_5
    sget-object v15, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->LINK:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    move-object/from16 v0, p0

    invoke-static {v0, v15, v3, v4}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 96
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    goto :goto_0

    .line 101
    .end local v3    # "coupon":Ljava/lang/String;
    .end local v4    # "couponType":Ljava/lang/String;
    :cond_6
    const-string v15, "settings_pl"

    const-string v16, "view"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 104
    new-instance v6, Landroid/content/Intent;

    const-class v15, Lcom/google/android/music/ui/ManageDevicesActivity;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    .local v6, "intent":Landroid/content/Intent;
    const/high16 v15, 0x14000000

    invoke-virtual {v6, v15}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 106
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/music/MusicUrlHandler;->startActivity(Landroid/content/Intent;)V

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    goto/16 :goto_0

    .line 111
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_7
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    .line 112
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    goto/16 :goto_0

    .line 115
    :cond_8
    const-string v16, "m"

    const/4 v15, 0x1

    invoke-interface {v10, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 118
    invoke-virtual {v7}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 120
    .local v8, "metajamId":Ljava/lang/String;
    const-string v15, "toStore"

    invoke-virtual {v7, v15}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 122
    .local v14, "toStoreParam":Ljava/lang/String;
    if-eqz v14, :cond_9

    const-string v15, "true"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    :cond_9
    const/4 v2, 0x1

    .line 124
    .local v2, "canSendToStore":Z
    :goto_1
    const-string v15, "1"

    const-string v16, "signup"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 126
    .local v12, "signUpForced":Z
    const-string v15, "1"

    const-string v16, "signup_if_needed"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    .line 128
    .local v13, "signUpIfNeeded":Z
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 130
    .local v11, "shareLink":Ljava/lang/String;
    new-instance v9, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-direct {v9}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;-><init>()V

    .line 131
    .local v9, "openMetajamItemInfo":Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    invoke-virtual {v9, v8}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setMetajamId(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v9, v2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setCanSendToStore(Z)V

    .line 133
    invoke-virtual {v9, v12}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setSignUpForced(Z)V

    .line 134
    invoke-virtual {v9, v13}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setSignUpIfNeeded(Z)V

    .line 135
    invoke-virtual {v9, v11}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setLink(Ljava/lang/String;)V

    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v15

    invoke-virtual {v9, v15}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setIsNautilusEnabled(Z)V

    .line 138
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v9, v1}, Lcom/google/android/music/ui/AppNavigation;->openMetajamItem(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    goto/16 :goto_0

    .line 122
    .end local v2    # "canSendToStore":Z
    .end local v9    # "openMetajamItemInfo":Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .end local v11    # "shareLink":Ljava/lang/String;
    .end local v12    # "signUpForced":Z
    .end local v13    # "signUpIfNeeded":Z
    :cond_a
    const/4 v2, 0x0

    goto :goto_1

    .line 139
    .end local v8    # "metajamId":Ljava/lang/String;
    .end local v14    # "toStoreParam":Ljava/lang/String;
    :cond_b
    const-string v16, "uq"

    const/4 v15, 0x1

    invoke-interface {v10, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 141
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/music/MusicUrlHandler;->setShowSongzaCard(Landroid/net/Uri;)V

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v15

    if-eqz v15, :cond_c

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isQuizEnabledOnDemand()Z

    move-result v15

    if-eqz v15, :cond_c

    .line 145
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/AppNavigation;->startQuiz(Landroid/content/Context;)V

    .line 149
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    goto/16 :goto_0

    .line 147
    :cond_c
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    goto :goto_2

    .line 151
    :cond_d
    const-string v15, "MusicUrlHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Unknown path: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    goto/16 :goto_0

    .line 155
    :cond_e
    const-string v15, "MusicUrlHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Unknown path: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    goto/16 :goto_0
.end method

.method public onTrackError()V
    .locals 2

    .prologue
    .line 171
    const v0, 0x7f0b0312

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/music/MusicUrlHandler;->finish()V

    .line 174
    return-void
.end method
