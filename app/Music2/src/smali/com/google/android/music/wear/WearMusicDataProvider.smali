.class public interface abstract Lcom/google/android/music/wear/WearMusicDataProvider;
.super Ljava/lang/Object;
.source "WearMusicDataProvider.java"


# virtual methods
.method public abstract getArtworkFile(J)Ljava/io/File;
.end method

.method public abstract getDecryptedInputStream(J)Ljava/io/InputStream;
.end method

.method public abstract getSyncedMediaLists()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearMediaList;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSyncedTracks(Lcom/google/android/music/wear/WearMediaList;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/wear/WearMediaList;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isWearSyncEnabled()Z
.end method

.method public abstract isWearSyncMarkedAvailable()Z
.end method

.method public abstract logPlayEvent(JLcom/google/android/music/wear/WearMediaList;ZJ)V
.end method

.method public abstract markSongPlayed(JJ)V
.end method

.method public abstract markWearSyncAvailable()V
.end method
