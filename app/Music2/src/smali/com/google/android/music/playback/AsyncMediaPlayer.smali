.class public interface abstract Lcom/google/android/music/playback/AsyncMediaPlayer;
.super Ljava/lang/Object;
.source "AsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;
    }
.end annotation


# virtual methods
.method public abstract duration()J
.end method

.method public abstract getAudioSessionId()I
.end method

.method public abstract getErrorType()I
.end method

.method public abstract getRemoteSongId()Ljava/lang/String;
.end method

.method public abstract isInErrorState()Z
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract isPreparing()Z
.end method

.method public abstract isRenderingAudioLocally()Z
.end method

.method public abstract isStreaming()Z
.end method

.method public abstract pause()V
.end method

.method public abstract position()J
.end method

.method public abstract release()V
.end method

.method public abstract seek(J)J
.end method

.method public abstract setAsCurrentPlayer()V
.end method

.method public abstract setAudioSessionId(I)V
.end method

.method public abstract setDataSource(Lcom/google/android/music/download/ContentIdentifier;JJZZLcom/google/android/music/store/MusicFile;Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;)V
.end method

.method public abstract setNextPlayer(Lcom/google/android/music/playback/AsyncMediaPlayer;)V
.end method

.method public abstract setVolume(F)V
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method
