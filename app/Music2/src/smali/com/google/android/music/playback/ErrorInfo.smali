.class public Lcom/google/android/music/playback/ErrorInfo;
.super Ljava/lang/Object;
.source "ErrorInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;
    }
.end annotation


# instance fields
.field private mAlertMessage:Ljava/lang/String;

.field private mAlertTitle:Ljava/lang/String;

.field private mHelpUriResId:I

.field private mHttpParamAccountName:Ljava/lang/String;

.field private mInlineResId:I

.field private mNeutralButtonActionUriId:I

.field private mNeutralButtonResId:I

.field private mNotificationMessage:Ljava/lang/String;

.field private mNotificationTitle:Ljava/lang/String;

.field private mPositiveButtonResId:I

.field private mPositivetButtonActionUriId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mInlineResId:I

    .line 50
    iput v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mHelpUriResId:I

    .line 52
    iput v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNeutralButtonActionUriId:I

    .line 53
    iput v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNeutralButtonResId:I

    .line 55
    iput v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mPositivetButtonActionUriId:I

    .line 59
    const v0, 0x7f0b013c

    iput v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mPositiveButtonResId:I

    .line 403
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/playback/ErrorInfo;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/ErrorInfo;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/ErrorInfo;->startManageDevicesActivity(Landroid/app/Activity;)V

    return-void
.end method

.method public static createErrorInfo(ILandroid/content/Context;Lcom/google/android/music/net/INetworkMonitor;)Lcom/google/android/music/playback/ErrorInfo;
    .locals 13
    .param p0, "errorType"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkMonitor"    # Lcom/google/android/music/net/INetworkMonitor;

    .prologue
    .line 67
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .line 68
    .local v5, "refObject":Ljava/lang/Object;
    invoke-static {p1, v5}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    .line 70
    .local v3, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v2, 0x0

    .line 71
    .local v2, "isStreamOnlyOnWifi":Z
    const/4 v7, 0x0

    .line 74
    .local v7, "syncAccount":Landroid/accounts/Account;
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isStreamOnlyOnWifi()Z

    move-result v2

    .line 75
    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 77
    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 80
    new-instance v1, Lcom/google/android/music/playback/ErrorInfo;

    invoke-direct {v1}, Lcom/google/android/music/playback/ErrorInfo;-><init>()V

    .line 81
    .local v1, "errorInfo":Lcom/google/android/music/playback/ErrorInfo;
    packed-switch p0, :pswitch_data_0

    .line 227
    :pswitch_0
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isStreaming()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 228
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b00f8

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b00f8

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const v8, 0x7f0b00f8

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    .line 246
    :goto_0
    return-object v1

    .line 77
    .end local v1    # "errorInfo":Lcom/google/android/music/playback/ErrorInfo;
    :catchall_0
    move-exception v8

    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v8

    .line 83
    .restart local v1    # "errorInfo":Lcom/google/android/music/playback/ErrorInfo;
    :pswitch_1
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b0169

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b0169

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const v8, 0x7f0b0169

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto :goto_0

    .line 92
    :pswitch_2
    const/4 v4, 0x0

    .line 94
    .local v4, "onlyOnWifi":Z
    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    :try_start_1
    invoke-interface {p2}, Lcom/google/android/music/net/INetworkMonitor;->hasHighSpeedConnection()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v8

    if-nez v8, :cond_0

    .line 96
    const/4 v4, 0x1

    .line 101
    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    .line 102
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016a

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016a

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const v8, 0x7f0b016a

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Landroid/os/RemoteException;
    const-string v8, "ErrorInfo"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 110
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016b

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016b

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const v8, 0x7f0b016b

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 121
    .end local v4    # "onlyOnWifi":Z
    :pswitch_3
    const v8, 0x7f0b016c

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016d

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const v8, 0x7f0b016c

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016d

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const v8, 0x7f0b016d

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 130
    :pswitch_4
    const v8, 0x7f0b0177

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016e

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const v8, 0x7f0b017d

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b017a

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const v8, 0x7f0b0176

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setHelp(I)V

    .line 141
    const v8, 0x7f0b0177

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 145
    :pswitch_5
    const v8, 0x7f0b0179

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b0170

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const v8, 0x7f0b017f

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b017c

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v8, -0x1

    const v9, 0x7f0b0173

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNeutralButtonAction(II)V

    .line 156
    const v8, 0x7f0b0171

    const v9, 0x7f0b0174

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setPositiveButtonAction(II)V

    .line 160
    const v8, 0x7f0b0179

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    .line 162
    if-eqz v7, :cond_2

    .line 163
    iget-object v8, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setHttpParamAccountName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 165
    :cond_2
    const-string v8, "ErrorInfo"

    const-string v9, "Failed to retrieve account name"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 170
    :pswitch_6
    const v8, 0x7f0b0178

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b016f

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const v8, 0x7f0b017e

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b017b

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const v8, 0x7f0b0176

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setHelp(I)V

    .line 178
    const v8, 0x7f0b0178

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 181
    :pswitch_7
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b00f8

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const v8, 0x7f0b0168

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b00f8

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const v8, 0x7f0b00f8

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 190
    :pswitch_8
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "music_nautlus_offline_status_ttl_days"

    const/16 v10, 0x1e

    invoke-static {v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 193
    .local v6, "staleDays":I
    const v8, 0x7f0b0181

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b0180

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p1, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const v8, 0x7f0b0183

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b0182

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p1, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const v8, 0x7f0b0181

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    .line 203
    const v8, 0x7f0b0185

    const v9, 0x7f0b0184

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setPositiveButtonAction(II)V

    goto/16 :goto_0

    .line 208
    .end local v6    # "staleDays":I
    :pswitch_9
    const v8, 0x7f0b0187

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b0186

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const v8, 0x7f0b0189

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b0188

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const v8, 0x7f0b0187

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 218
    :pswitch_a
    const v8, 0x7f0b018b

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b018a

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const v8, 0x7f0b018d

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b018c

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const v8, 0x7f0b018b

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 236
    :cond_3
    const v8, 0x7f0b00e6

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b00e7

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setAlertText(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const v8, 0x7f0b00e6

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0b00e7

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/google/android/music/playback/ErrorInfo;->setNotificationText(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const v8, 0x7f0b00e7

    invoke-virtual {v1, v8}, Lcom/google/android/music/playback/ErrorInfo;->setInlineText(I)V

    goto/16 :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method private startManageDevicesActivity(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 420
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/ManageDevicesActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 421
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 422
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 423
    return-void
.end method


# virtual methods
.method public canShowAlert()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mAlertMessage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mAlertTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canShowNotification()Z
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNotificationTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNotificationMessage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createAlert(Landroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)Landroid/app/AlertDialog;
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "dismissedListener"    # Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

    .prologue
    const/4 v6, -0x1

    .line 428
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->canShowAlert()Z

    move-result v5

    if-nez v5, :cond_0

    .line 429
    const/4 v5, 0x0

    .line 508
    :goto_0
    return-object v5

    .line 432
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 434
    .local v0, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getAlertMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 435
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getAlertTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 438
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getPositiveButtonResId()I

    move-result v4

    .line 439
    .local v4, "positiveButtonId":I
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getPositiveButtonActionUri()I

    move-result v3

    .line 441
    .local v3, "positiveActionUriId":I
    if-eq v4, v6, :cond_1

    .line 442
    new-instance v5, Lcom/google/android/music/playback/ErrorInfo$1;

    invoke-direct {v5, p0, v3, p1, p2}, Lcom/google/android/music/playback/ErrorInfo$1;-><init>(Lcom/google/android/music/playback/ErrorInfo;ILandroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 475
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->hasNeutrualAction()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 476
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getNeutralButtonResId()I

    move-result v2

    .line 477
    .local v2, "neutralButtonId":I
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getNeutralButtonActionUri()I

    move-result v1

    .line 486
    .local v1, "neutralActionUriId":I
    :goto_1
    if-eq v2, v6, :cond_2

    .line 487
    new-instance v5, Lcom/google/android/music/playback/ErrorInfo$2;

    invoke-direct {v5, p0, v1, p1, p2}, Lcom/google/android/music/playback/ErrorInfo$2;-><init>(Lcom/google/android/music/playback/ErrorInfo;ILandroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)V

    invoke-virtual {v0, v2, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 501
    :cond_2
    new-instance v5, Lcom/google/android/music/playback/ErrorInfo$3;

    invoke-direct {v5, p0, p2}, Lcom/google/android/music/playback/ErrorInfo$3;-><init>(Lcom/google/android/music/playback/ErrorInfo;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 508
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    goto :goto_0

    .line 478
    .end local v1    # "neutralActionUriId":I
    .end local v2    # "neutralButtonId":I
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->hasHelp()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 479
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getHelpButtonResId()I

    move-result v2

    .line 480
    .restart local v2    # "neutralButtonId":I
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getHelpUriResId()I

    move-result v1

    .restart local v1    # "neutralActionUriId":I
    goto :goto_1

    .line 482
    .end local v1    # "neutralActionUriId":I
    .end local v2    # "neutralButtonId":I
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getNeutralButtonResId()I

    move-result v2

    .line 483
    .restart local v2    # "neutralButtonId":I
    const/4 v1, -0x1

    .restart local v1    # "neutralActionUriId":I
    goto :goto_1
.end method

.method public createNotification(Landroid/content/ContextWrapper;)Landroid/app/Notification;
    .locals 7
    .param p1, "context"    # Landroid/content/ContextWrapper;

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->canShowNotification()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x0

    .line 399
    :goto_0
    return-object v3

    .line 365
    :cond_0
    const/4 v0, 0x0

    .line 366
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    const/4 v1, 0x0

    .line 367
    .local v1, "contentUri":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->hasNeutrualAction()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 368
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getNeutralButtonActionUri()I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/content/ContextWrapper;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 372
    :cond_1
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->hasHelp()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 373
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getHelpUriResId()I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/content/ContextWrapper;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 376
    :cond_2
    if-eqz v1, :cond_4

    .line 377
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 379
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->geHttpParamAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/music/utils/MusicUtils;->buildUriWithAccountName(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 382
    const/4 v4, 0x0

    const/high16 v5, 0x10000000

    invoke-static {p1, v4, v2, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 389
    .end local v2    # "i":Landroid/content/Intent;
    :goto_2
    new-instance v3, Landroid/app/Notification;

    invoke-direct {v3}, Landroid/app/Notification;-><init>()V

    .line 390
    .local v3, "notification":Landroid/app/Notification;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Landroid/app/Notification;->when:J

    .line 391
    const/16 v4, 0x18

    iput v4, v3, Landroid/app/Notification;->flags:I

    .line 392
    const v4, 0x1080078

    iput v4, v3, Landroid/app/Notification;->icon:I

    .line 393
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getNotificationTitle()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 394
    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getNotificationTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getNotificationMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 369
    .end local v3    # "notification":Landroid/app/Notification;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->hasPositiveAction()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 370
    invoke-virtual {p0}, Lcom/google/android/music/playback/ErrorInfo;->getPositiveButtonActionUri()I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/content/ContextWrapper;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 386
    :cond_4
    invoke-static {p1}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenApp(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_2
.end method

.method public geHttpParamAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mHttpParamAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getAlertMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mAlertMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getAlertTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mAlertTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getHelpButtonResId()I
    .locals 1

    .prologue
    .line 327
    const v0, 0x7f0b0175

    return v0
.end method

.method public getHelpUriResId()I
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mHelpUriResId:I

    return v0
.end method

.method public getNeutralButtonActionUri()I
    .locals 1

    .prologue
    .line 331
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNeutralButtonActionUriId:I

    return v0
.end method

.method public getNeutralButtonResId()I
    .locals 1

    .prologue
    .line 335
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNeutralButtonResId:I

    return v0
.end method

.method public getNotificationMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNotificationMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNotificationTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getPositiveButtonActionUri()I
    .locals 1

    .prologue
    .line 339
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mPositivetButtonActionUriId:I

    return v0
.end method

.method public getPositiveButtonResId()I
    .locals 1

    .prologue
    .line 347
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mPositiveButtonResId:I

    return v0
.end method

.method public hasHelp()Z
    .locals 2

    .prologue
    .line 291
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mHelpUriResId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNeutrualAction()Z
    .locals 2

    .prologue
    .line 295
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mNeutralButtonActionUriId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPositiveAction()Z
    .locals 2

    .prologue
    .line 299
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo;->mPositivetButtonActionUriId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlertText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/google/android/music/playback/ErrorInfo;->mAlertTitle:Ljava/lang/String;

    .line 252
    iput-object p2, p0, Lcom/google/android/music/playback/ErrorInfo;->mAlertMessage:Ljava/lang/String;

    .line 253
    return-void
.end method

.method public setHelp(I)V
    .locals 0
    .param p1, "helpUriResId"    # I

    .prologue
    .line 261
    iput p1, p0, Lcom/google/android/music/playback/ErrorInfo;->mHelpUriResId:I

    .line 262
    return-void
.end method

.method public setHttpParamAccountName(Ljava/lang/String;)V
    .locals 0
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/android/music/playback/ErrorInfo;->mHttpParamAccountName:Ljava/lang/String;

    .line 276
    return-void
.end method

.method public setInlineText(I)V
    .locals 0
    .param p1, "messageId"    # I

    .prologue
    .line 279
    iput p1, p0, Lcom/google/android/music/playback/ErrorInfo;->mInlineResId:I

    .line 280
    return-void
.end method

.method public setNeutralButtonAction(II)V
    .locals 0
    .param p1, "actionUriId"    # I
    .param p2, "buttonId"    # I

    .prologue
    .line 265
    iput p1, p0, Lcom/google/android/music/playback/ErrorInfo;->mNeutralButtonActionUriId:I

    .line 266
    iput p2, p0, Lcom/google/android/music/playback/ErrorInfo;->mNeutralButtonResId:I

    .line 267
    return-void
.end method

.method public setNotificationText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/music/playback/ErrorInfo;->mNotificationTitle:Ljava/lang/String;

    .line 257
    iput-object p2, p0, Lcom/google/android/music/playback/ErrorInfo;->mNotificationMessage:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public setPositiveButtonAction(II)V
    .locals 0
    .param p1, "actionUriId"    # I
    .param p2, "buttonId"    # I

    .prologue
    .line 270
    iput p1, p0, Lcom/google/android/music/playback/ErrorInfo;->mPositivetButtonActionUriId:I

    .line 271
    iput p2, p0, Lcom/google/android/music/playback/ErrorInfo;->mPositiveButtonResId:I

    .line 272
    return-void
.end method
