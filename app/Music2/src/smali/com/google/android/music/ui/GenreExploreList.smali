.class public Lcom/google/android/music/ui/GenreExploreList;
.super Lcom/google/android/music/medialist/GenreList;
.source "GenreExploreList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/ui/GenreExploreList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mGenreNautilusId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/music/ui/GenreExploreList$1;

    invoke-direct {v0}, Lcom/google/android/music/ui/GenreExploreList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/ui/GenreExploreList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "genreId"    # Ljava/lang/String;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/medialist/GenreList;-><init>(Z)V

    .line 18
    iput-object p1, p0, Lcom/google/android/music/ui/GenreExploreList;->mGenreNautilusId:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/ui/GenreExploreList;->mGenreNautilusId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/ui/GenreExploreList;->mGenreNautilusId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Explore;->getGenresUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Lcom/google/android/music/medialist/GenreList;->writeToParcel(Landroid/os/Parcel;I)V

    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/GenreExploreList;->mGenreNautilusId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    return-void
.end method
