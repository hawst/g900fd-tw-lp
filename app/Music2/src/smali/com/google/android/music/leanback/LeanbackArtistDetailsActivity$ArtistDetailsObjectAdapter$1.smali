.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

.field final synthetic val$this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->val$this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$500(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    .line 384
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$600(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    .line 385
    return-void
.end method

.method public onItemRangeChanged(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 389
    if-nez p1, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$700(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V

    .line 392
    :cond_0
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 396
    if-nez p1, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$500(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    .line 398
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeInserted(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$800(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V

    .line 400
    :cond_0
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$500(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    .line 406
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeRemoved(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$900(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V

    .line 408
    :cond_0
    return-void
.end method
