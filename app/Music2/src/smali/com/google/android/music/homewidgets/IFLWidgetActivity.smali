.class public Lcom/google/android/music/homewidgets/IFLWidgetActivity;
.super Landroid/app/Activity;
.source "IFLWidgetActivity.java"


# instance fields
.field private mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

.field private mIsActivityDestroyed:Z

.field mMixCreationCallbacks:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mIsActivityDestroyed:Z

    .line 39
    new-instance v0, Lcom/google/android/music/homewidgets/IFLWidgetActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity$1;-><init>(Lcom/google/android/music/homewidgets/IFLWidgetActivity;)V

    iput-object v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 60
    new-instance v0, Lcom/google/android/music/homewidgets/IFLWidgetActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity$2;-><init>(Lcom/google/android/music/homewidgets/IFLWidgetActivity;)V

    iput-object v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mMixCreationCallbacks:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/homewidgets/IFLWidgetActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/homewidgets/IFLWidgetActivity;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mIsActivityDestroyed:Z

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 106
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 109
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.music.IFL_WIDGET_CANCEL"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 111
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->canUseRadio(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    .line 115
    .local v1, "shouldStartRadio":Z
    if-eqz v1, :cond_0

    .line 117
    new-instance v3, Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-virtual {p0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "mixType"

    sget-object v6, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v6}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mMixCreationCallbacks:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;-><init>(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;)V

    iput-object v3, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .line 120
    iget-object v3, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-virtual {v3}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->start()V

    .line 134
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->shuffleOnDeviceEarly(Landroid/content/Context;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_enable_ifl_toast"

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    .line 128
    .local v2, "toastEnabled":Z
    if-eqz v2, :cond_1

    .line 129
    invoke-virtual {p0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0364

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mIsActivityDestroyed:Z

    .line 139
    iget-object v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-virtual {v0}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->quit()V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .line 144
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 145
    return-void
.end method
