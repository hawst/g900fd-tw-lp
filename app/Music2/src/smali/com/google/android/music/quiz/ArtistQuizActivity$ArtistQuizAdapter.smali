.class Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;
.super Landroid/widget/BaseAdapter;
.source "ArtistQuizActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/quiz/ArtistQuizActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArtistQuizAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;
    .param p2, "x1"    # Lcom/google/android/music/quiz/ArtistQuizActivity$1;

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$1300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/google/android/music/cloudclient/QuizArtistJson;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mAllArtistsResponseList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$1300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 379
    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizArtistJson;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 396
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 402
    const/4 v1, 0x0

    .line 403
    .local v1, "vh":Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;

    if-nez v2, :cond_1

    .line 405
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 406
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f04001c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/view/ViewGroup;

    .line 407
    .restart local p2    # "convertView":Landroid/view/View;
    new-instance v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;

    .end local v1    # "vh":Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-direct {v1, v2, v5}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V

    .line 408
    .restart local v1    # "vh":Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;
    const v2, 0x7f0e00db

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/widgets/ArtImageCheckbox;

    iput-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    .line 409
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mDefaultViewBgColor:I
    invoke-static {v3}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$1500(Lcom/google/android/music/quiz/ArtistQuizActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setBackgroundArtColor(I)V

    .line 410
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setBackgroundMode(I)V

    .line 411
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    const/16 v3, 0xca

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setDefaultArtId(I)V

    .line 412
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setExpectedAspectRatio(F)V

    .line 413
    const v2, 0x7f0e00dc

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mName:Landroid/widget/TextView;

    .line 414
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;
    invoke-static {v3}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$1600(Lcom/google/android/music/quiz/ArtistQuizActivity;)Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setOnCheckedChangeListener(Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;)V

    .line 415
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 420
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizArtistJson;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistId:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArtistId:Ljava/lang/String;

    .line 421
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizArtistJson;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->bind(Lcom/google/android/music/cloudclient/QuizArtistJson;)V

    .line 422
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    invoke-virtual {v2, v1}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setTag(Ljava/lang/Object;)V

    .line 423
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$200(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArtistId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setChecked(Z)V

    .line 424
    iget-object v2, v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->getItem(I)Lcom/google/android/music/cloudclient/QuizArtistJson;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/music/cloudclient/QuizArtistJson;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    new-instance v2, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistQuizAdapter;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-direct {v2, v3, v5}, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 427
    return-object p2

    .line 417
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "vh":Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;
    check-cast v1, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;

    .restart local v1    # "vh":Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;
    goto :goto_0
.end method
