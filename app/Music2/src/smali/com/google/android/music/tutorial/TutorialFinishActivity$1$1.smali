.class Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;
.super Ljava/lang/Object;
.source "TutorialFinishActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/TutorialFinishActivity$1;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 96
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-object v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialFinishActivity;

    invoke-virtual {v1}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 97
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    if-eqz v0, :cond_1

    .line 98
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-boolean v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->val$turnOffTutorial:Z

    if-eqz v1, :cond_0

    .line 99
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setTutorialViewed(Z)V

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-boolean v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->val$returnToCaller:Z

    if-eqz v1, :cond_2

    .line 102
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-object v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialFinishActivity;

    invoke-virtual {v1}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->finish()V

    .line 115
    :cond_1
    :goto_0
    return-void

    .line 105
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-boolean v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->val$forceNautilus:Z

    if-eqz v1, :cond_3

    .line 106
    sget-object v1, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setTempNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 109
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-boolean v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->val$launchQuiz:Z

    if-eqz v1, :cond_4

    .line 110
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-object v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialFinishActivity;

    # invokes: Lcom/google/android/music/tutorial/TutorialFinishActivity;->launchQuiz()V
    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->access$000(Lcom/google/android/music/tutorial/TutorialFinishActivity;)V

    goto :goto_0

    .line 112
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1$1;->this$1:Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    iget-object v1, v1, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialFinishActivity;

    # invokes: Lcom/google/android/music/tutorial/TutorialFinishActivity;->launchHome()V
    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->access$100(Lcom/google/android/music/tutorial/TutorialFinishActivity;)V

    goto :goto_0
.end method
