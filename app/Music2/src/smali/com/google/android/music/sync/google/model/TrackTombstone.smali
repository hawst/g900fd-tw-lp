.class public Lcom/google/android/music/sync/google/model/TrackTombstone;
.super Ljava/lang/Object;
.source "TrackTombstone.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;


# instance fields
.field private mLastModifiedTimestamp:J

.field private mLocalId:J

.field private mRemoteId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/TrackTombstone;->mLocalId:J

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/TrackTombstone;->mRemoteId:Ljava/lang/String;

    .line 21
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/TrackTombstone;->mLastModifiedTimestamp:J

    return-void
.end method

.method public static parse(Lcom/google/android/music/store/MusicFileTombstone;)Lcom/google/android/music/sync/google/model/TrackTombstone;
    .locals 4
    .param p0, "tombstone"    # Lcom/google/android/music/store/MusicFileTombstone;

    .prologue
    .line 106
    new-instance v1, Lcom/google/android/music/sync/google/model/TrackTombstone;

    invoke-direct {v1}, Lcom/google/android/music/sync/google/model/TrackTombstone;-><init>()V

    .line 107
    .local v1, "track":Lcom/google/android/music/sync/google/model/TrackTombstone;
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFileTombstone;->getSourceId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/sync/google/model/TrackTombstone;->mRemoteId:Ljava/lang/String;

    .line 108
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFileTombstone;->getLocalId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/TrackTombstone;->mLocalId:J

    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFileTombstone;->getSourceVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/TrackTombstone;->mLastModifiedTimestamp:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-object v1

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "MusicSyncAdapter"

    const-string v3, "Non-numeric version for music tombstone.  Replacing with 0."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 113
    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/TrackTombstone;->mLastModifiedTimestamp:J

    goto :goto_0
.end method


# virtual methods
.method public getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forTracksBatchMutation()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TrackTombstone: getFeedUrl not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TrackTombstone: getFeedUrlAsPost not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/TrackTombstone;->mLocalId:J

    return-wide v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/TrackTombstone;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->getMaxAlbumArtSize(Landroid/content/Context;)I

    move-result v0

    .line 67
    .local v0, "dimension":I
    invoke-static {p2, v0}, Lcom/google/android/music/sync/api/MusicUrl;->forTrack(Ljava/lang/String;I)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    return-object v1
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public isUpdate()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public serializeAsJson()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TrackTombstone: serializeAsJson not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setIsDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 63
    return-void
.end method

.method public setRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/TrackTombstone;->mRemoteId:Ljava/lang/String;

    return-void
.end method

.method public validateForUpstreamDelete()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/TrackTombstone;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid track for upstream delete."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    return-void
.end method

.method public validateForUpstreamInsert()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TrackTombstone: validateForUpstreamInsert not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public validateForUpstreamUpdate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TrackTombstone: validateForUpstreamInsert not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
