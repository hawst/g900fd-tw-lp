.class public Lcom/google/android/music/ui/AllTracksFragment;
.super Lcom/google/android/music/ui/BaseTrackListFragment;
.source "AllTracksFragment.java"


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

.field private mShuffleRow:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/AllTracksFragment;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AllTracksFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/AllTracksFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AllTracksFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment;->mShuffleRow:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method protected getSupportsFastScroll()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->initEmptyScreen()V

    .line 89
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const v0, 0x7f0b02a4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllTracksFragment;->setEmptyScreenText(I)V

    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllTracksFragment;->setEmptyScreenLearnMoreVisible(Z)V

    .line 96
    :goto_0
    return-void

    .line 93
    :cond_0
    const v0, 0x7f0b02a5

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllTracksFragment;->setEmptyScreenText(I)V

    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AllTracksFragment;->setEmptyScreenVisible(Z)V

    goto :goto_0
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseTrackListFragment;->newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    .line 62
    if-eqz p1, :cond_0

    .line 63
    new-instance v0, Lcom/google/android/music/ui/AllTracksFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/AllTracksFragment$1;-><init>(Lcom/google/android/music/ui/AllTracksFragment;)V

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 26
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 27
    new-instance v1, Lcom/google/android/music/medialist/AllSongsList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->getAllSongsSortOrder()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/google/android/music/medialist/AllSongsList;-><init>(I)V

    sget-object v2, Lcom/google/android/music/ui/TrackListAdapter;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/ui/AllTracksFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 32
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->getTrackListView()Lcom/google/android/music/ui/BaseTrackListView;

    move-result-object v0

    .line 34
    .local v0, "lv":Lcom/google/android/music/ui/BaseTrackListView;
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400d7

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/AllTracksFragment;->mShuffleRow:Landroid/view/View;

    .line 36
    iget-object v1, p0, Lcom/google/android/music/ui/AllTracksFragment;->mShuffleRow:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 37
    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/BaseTrackListView;->setHeaderDividersEnabled(Z)V

    .line 38
    iget-object v1, p0, Lcom/google/android/music/ui/AllTracksFragment;->mShuffleRow:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/ui/BaseTrackListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 40
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseTrackListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/BaseTrackListView;->showTrackArtist(Z)V

    .line 44
    return-void
.end method

.method public final onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment;->mShuffleRow:Landroid/view/View;

    if-ne p2, v0, :cond_0

    .line 80
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->shuffleAll()V

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/google/android/music/ui/BaseTrackListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseTrackListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->getSupportsFastScroll()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/music/ui/AllTracksFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    .line 57
    :cond_0
    return-void
.end method
