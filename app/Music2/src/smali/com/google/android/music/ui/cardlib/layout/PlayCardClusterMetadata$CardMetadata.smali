.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CardMetadata"
.end annotation


# instance fields
.field private final mHSpan:I

.field private final mLayoutId:I

.field private final mThumbnailAspectRatio:F

.field private final mVSpan:I


# direct methods
.method public constructor <init>(IIIF)V
    .locals 0
    .param p1, "layoutId"    # I
    .param p2, "hSpan"    # I
    .param p3, "vSpan"    # I
    .param p4, "thumbnailAspectRatio"    # F

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    .line 37
    iput p2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    .line 38
    iput p3, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    .line 39
    iput p4, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    .line 40
    return-void
.end method


# virtual methods
.method public getHSpan()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    return v0
.end method

.method public getThumbnailAspectRatio()F
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    return v0
.end method

.method public getVSpan()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    return v0
.end method
