.class Lcom/google/android/music/playback/ErrorInfo$1;
.super Ljava/lang/Object;
.source "ErrorInfo.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/ErrorInfo;->createAlert(Landroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/ErrorInfo;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

.field final synthetic val$positiveActionUriId:I


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/ErrorInfo;ILandroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/google/android/music/playback/ErrorInfo$1;->this$0:Lcom/google/android/music/playback/ErrorInfo;

    iput p2, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$positiveActionUriId:I

    iput-object p3, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$activity:Landroid/app/Activity;

    iput-object p4, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 445
    iget v2, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$positiveActionUriId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 446
    iget v2, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$positiveActionUriId:I

    sparse-switch v2, :sswitch_data_0

    .line 461
    const-string v2, "ErrorInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown positiveActionUriId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$positiveActionUriId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

    if-eqz v2, :cond_1

    .line 465
    iget-object v2, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

    invoke-interface {v2}, Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;->onErrorAlertDismissed()V

    .line 467
    :cond_1
    return-void

    .line 448
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/music/playback/ErrorInfo$1;->this$0:Lcom/google/android/music/playback/ErrorInfo;

    iget-object v3, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$activity:Landroid/app/Activity;

    # invokes: Lcom/google/android/music/playback/ErrorInfo;->startManageDevicesActivity(Landroid/app/Activity;)V
    invoke-static {v2, v3}, Lcom/google/android/music/playback/ErrorInfo;->access$000(Lcom/google/android/music/playback/ErrorInfo;Landroid/app/Activity;)V

    goto :goto_0

    .line 451
    :sswitch_1
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 452
    .local v1, "refobject":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/playback/ErrorInfo$1;->val$activity:Landroid/app/Activity;

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 455
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v2, 0x1

    :try_start_0
    invoke-static {v0, v2}, Lcom/google/android/music/utils/MusicUtils;->requestSync(Lcom/google/android/music/preferences/MusicPreferences;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2

    .line 446
    :sswitch_data_0
    .sparse-switch
        0x7f0b0171 -> :sswitch_0
        0x7f0b0185 -> :sswitch_1
    .end sparse-switch
.end method
