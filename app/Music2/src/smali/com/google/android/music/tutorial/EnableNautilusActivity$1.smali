.class Lcom/google/android/music/tutorial/EnableNautilusActivity$1;
.super Ljava/lang/Object;
.source "EnableNautilusActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/tutorial/EnableNautilusActivity;->enableNautilus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/EnableNautilusActivity;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 60
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    iget-object v2, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    invoke-direct {v0, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 61
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    iget-object v2, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/EnableNautilusActivity;->mOfferId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->access$000(Lcom/google/android/music/tutorial/EnableNautilusActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/music/cloudclient/MusicCloud;->enableNautilus(Ljava/lang/String;)Lcom/google/android/music/cloudclient/EnableNautilusResponse;

    .line 62
    iget-object v2, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/music/tutorial/EnableNautilusActivity;->mSuccess:Z
    invoke-static {v2, v3}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->access$102(Lcom/google/android/music/tutorial/EnableNautilusActivity;Z)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 68
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v2, "MusicEnableNautilus"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 65
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 66
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "MusicEnableNautilus"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/EnableNautilusActivity;->mSuccess:Z
    invoke-static {v0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->access$100(Lcom/google/android/music/tutorial/EnableNautilusActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    iget-object v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->finish()V

    goto :goto_0

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/tutorial/EnableNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/EnableNautilusActivity;

    # invokes: Lcom/google/android/music/tutorial/EnableNautilusActivity;->buildRetryDialog()V
    invoke-static {v0}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->access$200(Lcom/google/android/music/tutorial/EnableNautilusActivity;)V

    goto :goto_0
.end method
