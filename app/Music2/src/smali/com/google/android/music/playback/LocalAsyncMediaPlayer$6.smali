.class Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;
.super Lcom/google/android/music/download/IDownloadProgressListener$Stub;
.source "LocalAsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-direct {p0}, Lcom/google/android/music/download/IDownloadProgressListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadProgress(Lcom/google/android/music/download/DownloadProgress;)V
    .locals 4
    .param p1, "progress"    # Lcom/google/android/music/download/DownloadProgress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1200(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1079
    const-string v0, "LocalAsyncMediaPlayer"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$500(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-interface {v1, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->onDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V

    .line 1084
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$600(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$600(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1099
    :cond_1
    :goto_0
    return-void

    .line 1088
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getError()I

    move-result v1

    # setter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1302(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;I)I

    .line 1089
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v1

    # setter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1402(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Lcom/google/android/music/download/DownloadState$State;)Lcom/google/android/music/download/DownloadState$State;

    .line 1091
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->handleStreamingChanges()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1500(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    .line 1093
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getCompletedBytes()J

    move-result-wide v2

    long-to-float v1, v2

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getDownloadByteLength()J

    move-result-wide v2

    long-to-float v2, v2

    div-float/2addr v1, v2

    # setter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadCompletedPercent:F
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1602(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;F)F

    .line 1096
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->FAILED:Lcom/google/android/music/download/DownloadState$State;

    if-ne v0, v1, :cond_1

    .line 1097
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    const-wide/16 v2, 0x0

    # invokes: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->updateDownloadStatus(J)V
    invoke-static {v0, v2, v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$1700(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;J)V

    goto :goto_0
.end method
