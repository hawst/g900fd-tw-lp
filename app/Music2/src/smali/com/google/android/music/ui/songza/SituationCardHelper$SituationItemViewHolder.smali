.class Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SituationCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/songza/SituationCardHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SituationItemViewHolder"
.end annotation


# instance fields
.field mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

.field mOverlay:Landroid/view/View;

.field mSituationArt:Lcom/google/android/music/art/SimpleArtView;

.field mSituationTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 291
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 292
    const v0, 0x7f0e0265

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/SimpleArtView;

    iput-object v0, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mSituationArt:Lcom/google/android/music/art/SimpleArtView;

    .line 293
    const v0, 0x7f0e0266

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mSituationTitleView:Landroid/widget/TextView;

    .line 294
    const v0, 0x7f0e010a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;->mOverlay:Landroid/view/View;

    .line 295
    invoke-virtual {p1, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 296
    return-void
.end method
