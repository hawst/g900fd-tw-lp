.class public final enum Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
.super Ljava/lang/Enum;
.source "ListenNowSuggestionReason.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/ListenNowSuggestionReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SuggestionReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum ARTIST_PLAYING_NEAR_YOU:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum FREE_FROM_GOOGLE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum IDENTIFIED_ON_SOUND_SEARCH:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECENTLY_ADDED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECENTLY_CREATED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECENTLY_MODIFIED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECENTLY_PLAYED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECENTLY_PURCHASED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECENTLY_SUBSCRIBED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECOMMENDED_FOR_YOU_FROM_LOCKER:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum RECOMMENDED_FOR_YOU_RADIO:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum SUGGESTED_NEW_RELEASE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum UNKNOWN_REASON:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field public static final enum YOUTUBE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

.field private static final sValues:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 15
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "UNKNOWN_REASON"

    invoke-direct {v4, v5, v8, v8}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->UNKNOWN_REASON:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 16
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECENTLY_PURCHASED"

    invoke-direct {v4, v5, v9, v9}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_PURCHASED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 17
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECENTLY_ADDED"

    invoke-direct {v4, v5, v10, v10}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_ADDED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 18
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECENTLY_PLAYED"

    invoke-direct {v4, v5, v11, v11}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_PLAYED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 19
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECENTLY_SUBSCRIBED"

    invoke-direct {v4, v5, v12, v12}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_SUBSCRIBED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 20
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECENTLY_CREATED"

    const/4 v6, 0x5

    const/4 v7, 0x5

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_CREATED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 21
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECENTLY_MODIFIED"

    const/4 v6, 0x6

    const/4 v7, 0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_MODIFIED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 22
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "SUGGESTED_NEW_RELEASE"

    const/4 v6, 0x7

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->SUGGESTED_NEW_RELEASE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 23
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECOMMENDED_FOR_YOU_RADIO"

    const/16 v6, 0x8

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECOMMENDED_FOR_YOU_RADIO:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 24
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "RECOMMENDED_FOR_YOU_FROM_LOCKER"

    const/16 v6, 0x9

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECOMMENDED_FOR_YOU_FROM_LOCKER:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 25
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "IDENTIFIED_ON_SOUND_SEARCH"

    const/16 v6, 0xa

    const/16 v7, 0xa

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->IDENTIFIED_ON_SOUND_SEARCH:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 26
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "ARTIST_PLAYING_NEAR_YOU"

    const/16 v6, 0xb

    const/16 v7, 0xb

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->ARTIST_PLAYING_NEAR_YOU:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 27
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "FREE_FROM_GOOGLE"

    const/16 v6, 0xc

    const/16 v7, 0xc

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->FREE_FROM_GOOGLE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 28
    new-instance v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    const-string v5, "YOUTUBE"

    const/16 v6, 0xd

    const/16 v7, 0xd

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->YOUTUBE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 14
    const/16 v4, 0xe

    new-array v4, v4, [Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    sget-object v5, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->UNKNOWN_REASON:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v5, v4, v8

    sget-object v5, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_PURCHASED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v5, v4, v9

    sget-object v5, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_ADDED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v5, v4, v10

    sget-object v5, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_PLAYED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v5, v4, v11

    sget-object v5, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_SUBSCRIBED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v5, v4, v12

    const/4 v5, 0x5

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_CREATED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_MODIFIED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->SUGGESTED_NEW_RELEASE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECOMMENDED_FOR_YOU_RADIO:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECOMMENDED_FOR_YOU_FROM_LOCKER:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->IDENTIFIED_ON_SOUND_SEARCH:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->ARTIST_PLAYING_NEAR_YOU:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->FREE_FROM_GOOGLE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->YOUTUBE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    aput-object v6, v4, v5

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->$VALUES:[Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 31
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    sput-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->sValues:Landroid/util/SparseArray;

    .line 35
    invoke-static {}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->values()[Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 36
    .local v3, "sr":Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    sget-object v4, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->sValues:Landroid/util/SparseArray;

    invoke-virtual {v3}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v5

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 35
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    .end local v3    # "sr":Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->mValue:I

    .line 42
    return-void
.end method

.method public static fromValue(I)Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 49
    sget-object v1, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->sValues:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    .line 50
    .local v0, "sr":Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    if-eqz v0, :cond_0

    .end local v0    # "sr":Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    :goto_0
    return-object v0

    .restart local v0    # "sr":Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    :cond_0
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->UNKNOWN_REASON:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->$VALUES:[Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, [Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->mValue:I

    return v0
.end method
