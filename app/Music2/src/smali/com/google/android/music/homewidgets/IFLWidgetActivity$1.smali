.class Lcom/google/android/music/homewidgets/IFLWidgetActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "IFLWidgetActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/homewidgets/IFLWidgetActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/homewidgets/IFLWidgetActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/homewidgets/IFLWidgetActivity;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity$1;->this$0:Lcom/google/android/music/homewidgets/IFLWidgetActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 42
    const-string v1, "com.google.android.music.IFL_WIDGET_CANCEL"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 45
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->cancelMix()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity$1;->this$0:Lcom/google/android/music/homewidgets/IFLWidgetActivity;

    invoke-static {v1, v3}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->setLoadingIndicatorVisibility(Landroid/content/Context;Z)V

    .line 52
    iget-object v1, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity$1;->this$0:Lcom/google/android/music/homewidgets/IFLWidgetActivity;

    invoke-virtual {v1, v3}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->setResult(I)V

    .line 53
    iget-object v1, p0, Lcom/google/android/music/homewidgets/IFLWidgetActivity$1;->this$0:Lcom/google/android/music/homewidgets/IFLWidgetActivity;

    invoke-virtual {v1}, Lcom/google/android/music/homewidgets/IFLWidgetActivity;->finish()V

    .line 57
    :goto_1
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicIFLWidget"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const-string v1, "MusicIFLWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OnReceive: action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was not expected."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
