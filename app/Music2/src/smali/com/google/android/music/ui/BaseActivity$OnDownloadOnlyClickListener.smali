.class public Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "OnDownloadOnlyClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 1510
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # getter for: Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;
    invoke-static {v0}, Lcom/google/android/music/ui/BaseActivity;->access$1300(Lcom/google/android/music/ui/BaseActivity;)Landroid/widget/TextSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1519
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # getter for: Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;
    invoke-static {v0}, Lcom/google/android/music/ui/BaseActivity;->access$1300(Lcom/google/android/music/ui/BaseActivity;)Landroid/widget/TextSwitcher;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b035f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 1522
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener$1;-><init>(Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;)V

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;->this$0:Lcom/google/android/music/ui/BaseActivity;

    const/16 v2, 0xbb8

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->runDelayedOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;I)Landroid/os/Handler;

    .line 1533
    return-void
.end method
