.class public Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;
.super Ljava/lang/Object;
.source "VideoDocumentClusterBuilder.java"


# static fields
.field public static final SONG_COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Vid"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "VThumbnailUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public static buildVideoDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "showArtist"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 36
    .local v1, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v2, -0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 37
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 38
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 39
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;->getVideoDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Z)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getVideoThumbnailUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 42
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_1
    return-object v1
.end method

.method public static getVideoDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Z)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "showArtist"    # Z

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 50
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 51
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;->populateDocumentFromSongCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 52
    if-eqz p2, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 55
    :cond_0
    return-object p0
.end method

.method private static populateDocumentFromSongCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 5
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 59
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 60
    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTrackMetajamId(Ljava/lang/String;)V

    .line 63
    :cond_0
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 66
    invoke-interface {p1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setVideoId(Ljava/lang/String;)V

    .line 69
    :cond_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 70
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setVideoThumbnailUrl(Ljava/lang/String;)V

    .line 72
    :cond_2
    return-object p0
.end method
