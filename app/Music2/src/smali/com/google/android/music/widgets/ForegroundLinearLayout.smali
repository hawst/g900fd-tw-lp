.class public abstract Lcom/google/android/music/widgets/ForegroundLinearLayout;
.super Landroid/widget/LinearLayout;
.source "ForegroundLinearLayout.java"


# static fields
.field private static IS_HC_OR_ABOVE:Z

.field private static IS_JBMR1_OR_ABOVE:Z


# instance fields
.field private mForegroundBoundsChanged:Z

.field private mForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mForegroundPaddingBottom:I

.field private mForegroundPaddingLeft:I

.field private mForegroundPaddingRight:I

.field private mForegroundPaddingTop:I

.field private final mOverlayBounds:Landroid/graphics/Rect;

.field private final mSelfBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->IS_HC_OR_ABOVE:Z

    .line 27
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/music/widgets/ForegroundLinearLayout;->IS_JBMR1_OR_ABOVE:Z

    return-void

    :cond_0
    move v0, v2

    .line 25
    goto :goto_0

    :cond_1
    move v1, v2

    .line 27
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput v4, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    .line 32
    iput v4, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingTop:I

    .line 33
    iput v4, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingRight:I

    .line 34
    iput v4, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    .line 36
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mSelfBounds:Landroid/graphics/Rect;

    .line 37
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mOverlayBounds:Landroid/graphics/Rect;

    .line 38
    iput-boolean v4, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 47
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010109

    aput v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 50
    .local v1, "foregroundDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 53
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 54
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 161
    iget-object v6, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 163
    .local v6, "foreground":Landroid/graphics/drawable/Drawable;
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 165
    iget-object v3, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mSelfBounds:Landroid/graphics/Rect;

    .line 166
    .local v3, "selfBounds":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mOverlayBounds:Landroid/graphics/Rect;

    .line 168
    .local v4, "overlayBounds":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->getWidth()I

    move-result v8

    .line 169
    .local v8, "w":I
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->getHeight()I

    move-result v7

    .line 171
    .local v7, "h":I
    iget v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    iget v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingTop:I

    iget v2, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingRight:I

    sub-int v2, v8, v2

    iget v9, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    sub-int v9, v7, v9

    invoke-virtual {v3, v0, v1, v2, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 175
    sget-boolean v0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->IS_JBMR1_OR_ABOVE:Z

    if-eqz v0, :cond_2

    .line 176
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->getLayoutDirection()I

    move-result v5

    .line 177
    .local v5, "layoutDirection":I
    const/16 v0, 0x77

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/GravityCompat;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 183
    .end local v5    # "layoutDirection":I
    :goto_0
    invoke-virtual {v6, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 186
    .end local v3    # "selfBounds":Landroid/graphics/Rect;
    .end local v4    # "overlayBounds":Landroid/graphics/Rect;
    .end local v7    # "h":I
    .end local v8    # "w":I
    :cond_0
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 188
    .end local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    :cond_1
    return-void

    .line 181
    .restart local v3    # "selfBounds":Landroid/graphics/Rect;
    .restart local v4    # "overlayBounds":Landroid/graphics/Rect;
    .restart local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "h":I
    .restart local v8    # "w":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mOverlayBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 126
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->drawableHotspotChanged(FF)V

    .line 128
    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 131
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 119
    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 122
    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 110
    invoke-super {p0}, Landroid/widget/LinearLayout;->jumpDrawablesToCurrentState()V

    .line 111
    sget-boolean v0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 114
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 145
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 148
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 152
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 155
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v3, 0x0

    .line 57
    iget-object v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v1, p1, :cond_3

    .line 58
    iget-object v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 60
    iget-object v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 64
    iput v3, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    .line 65
    iput v3, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingTop:I

    .line 66
    iput v3, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingRight:I

    .line 67
    iput v3, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    .line 69
    if-eqz p1, :cond_4

    .line 70
    invoke-virtual {p0, v3}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->setWillNotDraw(Z)V

    .line 71
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 72
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 75
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 76
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    .line 78
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingTop:I

    .line 79
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingRight:I

    .line 80
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    .line 85
    .end local v0    # "padding":Landroid/graphics/Rect;
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->requestLayout()V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->invalidate()V

    .line 88
    :cond_3
    return-void

    .line 83
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->setWillNotDraw(Z)V

    goto :goto_0
.end method

.method public setForegroundPadding(IIII)V
    .locals 0
    .param p1, "foregroundPaddingLeft"    # I
    .param p2, "foregroundPaddingTop"    # I
    .param p3, "foregroundPaddingRight"    # I
    .param p4, "foregroundPaddingBottom"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    .line 94
    iput p2, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingTop:I

    .line 95
    iput p3, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingRight:I

    .line 96
    iput p4, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    .line 98
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->requestLayout()V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ForegroundLinearLayout;->invalidate()V

    .line 100
    return-void
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    const/4 v1, 0x0

    .line 135
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 137
    iget-object v2, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 138
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 139
    .local v0, "isVisible":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 141
    .end local v0    # "isVisible":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 138
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/widgets/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
