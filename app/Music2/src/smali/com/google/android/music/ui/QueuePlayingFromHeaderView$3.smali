.class Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;
.super Ljava/lang/Object;
.source "QueuePlayingFromHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doAlbumArtUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mArtistName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$request:Lcom/google/android/music/art/ContainerArtRequest;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/art/ContainerArtRequest;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iput-object p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    iput-object p3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 341
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    invoke-virtual {v0}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v8

    .line 342
    .local v8, "id":J
    const-wide/16 v2, 0x0

    cmp-long v0, v8, v2

    if-gez v0, :cond_0

    .line 343
    const-string v0, "QueueHeaderView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid album id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " container="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v3}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$300(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->mArtistName:Ljava/lang/String;

    .line 358
    :goto_0
    return-void

    .line 347
    :cond_0
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 348
    .local v1, "albumUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 350
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->val$appContext:Landroid/content/Context;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->ALBUM_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$400()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 352
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v7

    :goto_1
    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->mArtistName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 353
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 356
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    invoke-virtual {v1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->mArtistName:Ljava/lang/String;

    # invokes: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$500(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$3;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    .line 364
    return-void
.end method
