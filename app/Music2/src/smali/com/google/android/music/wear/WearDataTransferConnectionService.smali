.class public final Lcom/google/android/music/wear/WearDataTransferConnectionService;
.super Lcom/google/android/wearable/datatransfer/ConnectionService;
.source "WearDataTransferConnectionService.java"


# instance fields
.field private mController:Lcom/google/android/music/wear/WearDataTransferConnectionController;

.field private mExecutorService:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/ConnectionService;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 5

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/wearable/datatransfer/ConnectionService;->onCreate()V

    .line 19
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionService;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 20
    new-instance v0, Lcom/google/android/music/wear/WearDataTransferConnectionController;

    new-instance v1, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;

    invoke-direct {v1, p0}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/music/wear/WearDataTransferConnectionService;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/google/android/music/wear/DefaultImageResizer;

    const/16 v4, 0x140

    invoke-direct {v3, v4}, Lcom/google/android/music/wear/DefaultImageResizer;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/wear/WearDataTransferConnectionController;-><init>(Lcom/google/android/music/wear/WearMusicDataProvider;Ljava/util/concurrent/Executor;Lcom/google/android/music/wear/ImageResizer;)V

    iput-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionService;->mController:Lcom/google/android/music/wear/WearDataTransferConnectionController;

    .line 24
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionService;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 34
    invoke-super {p0}, Lcom/google/android/wearable/datatransfer/ConnectionService;->onDestroy()V

    .line 35
    return-void
.end method

.method public openFileDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "sourceNodeId"    # Ljava/lang/String;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionService;->mController:Lcom/google/android/music/wear/WearDataTransferConnectionController;

    invoke-virtual {v0, p1}, Lcom/google/android/music/wear/WearDataTransferConnectionController;->openFileDescriptor(Ljava/lang/String;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v0

    return-object v0
.end method
