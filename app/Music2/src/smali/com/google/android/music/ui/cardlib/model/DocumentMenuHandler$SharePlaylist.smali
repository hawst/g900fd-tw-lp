.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SharePlaylist"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1225
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1226
    const v0, 0x7f0e002b

    const v1, 0x7f0b00cb

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1227
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 5

    .prologue
    .line 1229
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareState()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v2

    const/16 v3, 0x46

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v2

    const/16 v3, 0x47

    if-ne v2, v3, :cond_1

    .line 1232
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->sharePlaylistWithExternalApps()V
    invoke-static {v2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$700(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V

    .line 1259
    :goto_0
    return-void

    .line 1235
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b034a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 1238
    .local v1, "span":Landroid/text/Spanned;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1239
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b034b

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b034c

    new-instance v4, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$2;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$2;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b034d

    new-instance v4, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$1;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist$1;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
