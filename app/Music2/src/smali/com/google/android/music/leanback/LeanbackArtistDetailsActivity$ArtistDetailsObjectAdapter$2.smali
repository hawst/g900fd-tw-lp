.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;
.super Ljava/lang/Object;
.source "LeanbackArtistDetailsActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->loadExtraData(Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAction:Landroid/support/v17/leanback/widget/Action;

.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

.field final synthetic val$artistItem:Lcom/google/android/music/leanback/Item;

.field final synthetic val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->val$artistItem:Lcom/google/android/music/leanback/Item;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 3

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iget-object v1, v1, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "artist_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->val$artistItem:Lcom/google/android/music/leanback/Item;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createStartRadioAction(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;
    invoke-static {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->mAction:Landroid/support/v17/leanback/widget/Action;

    .line 502
    return-void
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 506
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->mAction:Landroid/support/v17/leanback/widget/Action;

    invoke-virtual {v0, v2, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(ILandroid/support/v17/leanback/widget/Action;)V

    .line 507
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$1100(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V

    .line 508
    return-void
.end method
