.class Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;
.super Landroid/os/Handler;
.source "RemoteAsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;


# direct methods
.method public constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 1

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .line 188
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 189
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 193
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "MusicCast"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage: what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 294
    :cond_1
    :goto_0
    return-void

    .line 199
    :pswitch_0
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler$1;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 237
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v1, "Timed out waiting for cast token.  Aborting playback."

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v0, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    goto :goto_0

    .line 244
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemotePlaybackOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 248
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemotePauseOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 251
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoteResumeOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1200(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 254
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemotePauseOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 257
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemotePauseOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 260
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoteSeekOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 263
    :pswitch_8
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoteSetVolumeOnMainThread(F)V
    invoke-static {v1, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;F)V

    goto :goto_0

    .line 267
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1500(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1500(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    move-result-object v0

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->enqueue()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 273
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoveOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto :goto_0

    .line 276
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/CastSessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isWaitingForPlayback()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v1, "Timed out waiting for session id.  Aborting playback."

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 279
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v0, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    goto/16 :goto_0

    .line 283
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestCastV2RemoteSyncStatusOnMainThread()V
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    goto/16 :goto_0

    .line 286
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/CastSessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/cast/CastSessionManager;->isCastV2ReceiverLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    invoke-static {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isWaitingForPlayback()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v1, "Timed out waiting for cast v2 receiver.  Aborting playback."

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v0, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    goto/16 :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
