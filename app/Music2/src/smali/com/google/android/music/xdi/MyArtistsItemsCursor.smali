.class Lcom/google/android/music/xdi/MyArtistsItemsCursor;
.super Landroid/database/MatrixCursor;
.source "MyArtistsItemsCursor.java"


# static fields
.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;


# instance fields
.field private final mArtistItemHeight:I

.field private final mArtistItemWidth:I

.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 38
    iput-object p1, p0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mContext:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 40
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemWidthDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mArtistItemWidth:I

    .line 41
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemHeightDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mArtistItemHeight:I

    .line 43
    invoke-direct {p0}, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->addRowsForArtists()V

    .line 44
    return-void
.end method

.method private addRowsForArtists()V
    .locals 19

    .prologue
    .line 47
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/google/android/music/store/MusicContent$Artists;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v13

    .line 52
    .local v13, "cursor":Landroid/database/Cursor;
    if-nez v13, :cond_0

    .line 92
    :goto_0
    return-void

    .line 57
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 59
    .local v14, "id":J
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 60
    .local v17, "name":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 61
    .local v11, "artUri":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 65
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v14, v15, v3}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistCount(Landroid/content/Context;JZ)I

    move-result v12

    .line 68
    .local v12, "count":I
    sget-object v2, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "details/artists"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiContract;->getDetailsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v16

    .line 73
    .local v16, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v18, v0

    .line 74
    .local v18, "values":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "parent_id"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120003

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v12, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 80
    .local v10, "albumCountText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_description"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 82
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "image_uri"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "width"

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mArtistItemWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "height"

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mArtistItemHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "intent_uri"

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/MyArtistsItemsCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 90
    .end local v10    # "albumCountText":Ljava/lang/String;
    .end local v11    # "artUri":Ljava/lang/String;
    .end local v12    # "count":I
    .end local v14    # "id":J
    .end local v16    # "intent":Landroid/content/Intent;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "values":[Ljava/lang/Object;
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    :cond_2
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method
