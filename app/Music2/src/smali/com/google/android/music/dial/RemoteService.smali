.class public Lcom/google/android/music/dial/RemoteService;
.super Ljava/lang/Object;
.source "RemoteService.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/RemoteService$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/dial/RemoteService;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mControlUrl:Ljava/lang/String;

.field private final mEventSubUrl:Ljava/lang/String;

.field private final mScpdUrl:Ljava/lang/String;

.field private final mServiceId:Ljava/lang/String;

.field private final mServiceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/music/dial/RemoteService$1;

    invoke-direct {v0}, Lcom/google/android/music/dial/RemoteService$1;-><init>()V

    sput-object v0, Lcom/google/android/music/dial/RemoteService;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/dial/RemoteService$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/google/android/music/dial/RemoteService$Builder;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    # getter for: Lcom/google/android/music/dial/RemoteService$Builder;->mServiceType:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteService$Builder;->access$100(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    .line 60
    # getter for: Lcom/google/android/music/dial/RemoteService$Builder;->mServiceId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteService$Builder;->access$200(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    .line 61
    # getter for: Lcom/google/android/music/dial/RemoteService$Builder;->mControlUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteService$Builder;->access$300(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    .line 62
    # getter for: Lcom/google/android/music/dial/RemoteService$Builder;->mEventSubUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteService$Builder;->access$400(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    .line 63
    # getter for: Lcom/google/android/music/dial/RemoteService$Builder;->mScpdUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteService$Builder;->access$500(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/dial/RemoteService$Builder;Lcom/google/android/music/dial/RemoteService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/dial/RemoteService$Builder;
    .param p2, "x1"    # Lcom/google/android/music/dial/RemoteService$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/RemoteService;-><init>(Lcom/google/android/music/dial/RemoteService$Builder;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    if-ne p0, p1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v1

    .line 133
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 134
    goto :goto_0

    .line 136
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 137
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 139
    check-cast v0, Lcom/google/android/music/dial/RemoteService;

    .line 140
    .local v0, "other":Lcom/google/android/music/dial/RemoteService;
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 141
    iget-object v3, v0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 142
    goto :goto_0

    .line 144
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 145
    goto :goto_0

    .line 147
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 148
    iget-object v3, v0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    .line 149
    goto :goto_0

    .line 151
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 152
    goto :goto_0

    .line 154
    :cond_7
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 155
    iget-object v3, v0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 156
    goto :goto_0

    .line 158
    :cond_8
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 159
    goto :goto_0

    .line 161
    :cond_9
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 162
    iget-object v3, v0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v1, v2

    .line 163
    goto :goto_0

    .line 165
    :cond_a
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 166
    goto :goto_0

    .line 168
    :cond_b
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    if-nez v3, :cond_c

    .line 169
    iget-object v3, v0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 170
    goto :goto_0

    .line 172
    :cond_c
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 173
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 118
    const/16 v0, 0x1f

    .line 119
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 120
    .local v1, "result":I
    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 121
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 122
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 123
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 124
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    if-nez v4, :cond_4

    :goto_4
    add-int v1, v2, v3

    .line 125
    return v1

    .line 120
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 122
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 123
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 124
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mServiceType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mServiceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mControlUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mEventSubUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService;->mScpdUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    return-void
.end method
