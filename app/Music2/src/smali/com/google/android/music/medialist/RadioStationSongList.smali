.class public Lcom/google/android/music/medialist/RadioStationSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "RadioStationSongList.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RadioSongList"


# instance fields
.field private final mArtUrl:Ljava/lang/String;

.field private final mDescription:Ljava/lang/String;

.field private final mName:Ljava/lang/String;

.field private final mRadioStationId:J

.field private final mRemoteId:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "radioStationId"    # J
    .param p3, "remoteId"    # Ljava/lang/String;
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "artUrl"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 54
    const/4 v0, 0x1

    invoke-direct {p0, v1, v1, v0}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 58
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Radio station id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    iput-wide p1, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    .line 63
    iput-object p3, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRemoteId:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mName:Ljava/lang/String;

    .line 65
    iput-object p5, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mDescription:Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mArtUrl:Ljava/lang/String;

    .line 68
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/RadioStationSongList;->clearFlag(I)V

    .line 69
    return-void
.end method


# virtual methods
.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    const/4 v0, 0x0

    return v0
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 181
    const/4 v0, 0x1

    return v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 84
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRemoteId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mDescription:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mArtUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getArtUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/RadioStationSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "name":Ljava/lang/String;
    iget-wide v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-static {v2, v3, v0}, Lcom/google/android/music/store/ContainerDescriptor;->newRadioDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    return-object v1
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RadioStationSongList does not support getContentUri() yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadedSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 198
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponDownloadedSongCount"

    aput-object v0, v2, v4

    .line 201
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v4, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v1

    .local v1, "uri":Landroid/net/Uri;
    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    .line 202
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 205
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    :cond_0
    const-string v0, "RadioSongList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown radio station id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    const/4 v0, -0x1

    .line 212
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 209
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 212
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 147
    iget-wide v0, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    return-wide v0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RadioStationSongList does not support getImage() yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getKeepOnSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 218
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponSongCount"

    aput-object v0, v2, v4

    .line 221
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v4, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v1

    .local v1, "uri":Landroid/net/Uri;
    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    .line 222
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 224
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    :cond_0
    const-string v0, "RadioSongList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown radio station id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    const/4 v0, -0x1

    .line 231
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 228
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 231
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getRadioStationId()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    return-wide v0
.end method

.method public getRadioStationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasMetaData()Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public hasUniqueAudioId()Z
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return v0
.end method

.method public isAllLocal(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/AvailableSpaceTracker;Lcom/google/android/music/store/IStoreService;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "offlineMusicManager"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p3, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 170
    iget-wide v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-virtual {p2, v2, v3}, Lcom/google/android/music/AvailableSpaceTracker;->isRadioStationSelected(J)Ljava/lang/Boolean;

    move-result-object v0

    .line 171
    .local v0, "result":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 174
    invoke-virtual {p0, p1, p3}, Lcom/google/android/music/medialist/RadioStationSongList;->isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z

    move-result v1

    .line 176
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 159
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-interface {p2, v2, v3}, Lcom/google/android/music/store/IStoreService;->isRadioStationSelectedAsKeepOn(J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 163
    :goto_0
    return v1

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "RadioSongList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error trying to get offline status for radio station: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 163
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public refreshMetaData(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 138
    return-void
.end method

.method public registerMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 142
    iget-wide v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 143
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 144
    return-void
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected supportsOfflineCaching()Z
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method public toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keepOnManager"    # Lcom/google/android/music/activitymanagement/KeepOnManager;
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 241
    iget-wide v2, p0, Lcom/google/android/music/medialist/RadioStationSongList;->mRadioStationId:J

    move-object v1, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleRadioKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V

    .line 242
    return-void
.end method
