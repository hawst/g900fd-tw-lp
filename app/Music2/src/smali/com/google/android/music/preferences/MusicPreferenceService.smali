.class public Lcom/google/android/music/preferences/MusicPreferenceService;
.super Lcom/google/android/music/preferences/IPreferenceService$Stub;
.source "MusicPreferenceService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/preferences/MusicPreferenceService$MusicPreferenceServiceBinder;
    }
.end annotation


# instance fields
.field private final mChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/preferences/IPreferenceChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/android/music/preferences/IPreferenceService$Stub;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    .line 34
    iput-object p1, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mContext:Landroid/content/Context;

    .line 36
    const/4 v1, 0x0

    .line 37
    .local v1, "mode":I
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    or-int/lit8 v1, v1, 0x4

    .line 40
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mContext:Landroid/content/Context;

    const-string v3, "MusicPreferences"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 43
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "StreamOnlyHighQuality"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 44
    .local v0, "highQuality":Z
    if-eqz v0, :cond_1

    .line 45
    const-string v2, "StreamQuality"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferenceService;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    const-string v2, "StreamOnlyHighQuality"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferenceService;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    :cond_1
    return-void
.end method

.method private notifyBooleanPreferenceChanged(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 131
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    monitor-enter v3

    .line 132
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    .local v1, "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_start_1
    invoke-interface {v1, p1, p2}, Lcom/google/android/music/preferences/IPreferenceChangeListener;->onBooleanChanged(Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v2

    goto :goto_0

    .line 139
    .end local v1    # "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :cond_0
    :try_start_2
    monitor-exit v3

    .line 140
    return-void

    .line 139
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method private notifyIntPreferenceChanged(Ljava/lang/String;I)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 120
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    monitor-enter v3

    .line 121
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    .local v1, "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_start_1
    invoke-interface {v1, p1, p2}, Lcom/google/android/music/preferences/IPreferenceChangeListener;->onIntChanged(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v2

    goto :goto_0

    .line 128
    .end local v1    # "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :cond_0
    :try_start_2
    monitor-exit v3

    .line 129
    return-void

    .line 128
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method private notifyLongPreferenceChanged(Ljava/lang/String;J)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 142
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    monitor-enter v3

    .line 143
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    .local v1, "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_start_1
    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/music/preferences/IPreferenceChangeListener;->onLongChanged(Ljava/lang/String;J)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v2

    goto :goto_0

    .line 150
    .end local v1    # "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :cond_0
    :try_start_2
    monitor-exit v3

    .line 151
    return-void

    .line 150
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method private notifyPreferenceRemoved(Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    monitor-enter v3

    .line 110
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    .local v1, "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_start_1
    invoke-interface {v1, p1}, Lcom/google/android/music/preferences/IPreferenceChangeListener;->onPreferenceRemoved(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 113
    :catch_0
    move-exception v2

    goto :goto_0

    .line 117
    .end local v1    # "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :cond_0
    :try_start_2
    monitor-exit v3

    .line 118
    return-void

    .line 117
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method private notifyStringPreferenceChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 153
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    monitor-enter v3

    .line 154
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    .local v1, "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :try_start_1
    invoke-interface {v1, p1, p2}, Lcom/google/android/music/preferences/IPreferenceChangeListener;->onStringChanged(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v2

    goto :goto_0

    .line 161
    .end local v1    # "listener":Lcom/google/android/music/preferences/IPreferenceChangeListener;
    :cond_0
    :try_start_2
    monitor-exit v3

    .line 162
    return-void

    .line 161
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method private setPreference(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 79
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-nez p2, :cond_0

    .line 80
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 92
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    .line 93
    return-void

    .line 81
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 82
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 83
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v1, p2, Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 84
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 85
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 86
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 87
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_3
    instance-of v1, p2, Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 88
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 90
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown value type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getCurrentPreferences()Ljava/util/Map;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public registerPreferenceChangeListener(Lcom/google/android/music/preferences/IPreferenceChangeListener;)V
    .locals 2
    .param p1, "l"    # Lcom/google/android/music/preferences/IPreferenceChangeListener;

    .prologue
    .line 96
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    monitor-enter v1

    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    monitor-exit v1

    .line 99
    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/preferences/MusicPreferenceService;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/music/preferences/MusicPreferenceService;->notifyPreferenceRemoved(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public setBooleanPreference(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 56
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/preferences/MusicPreferenceService;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferenceService;->notifyBooleanPreferenceChanged(Ljava/lang/String;Z)V

    .line 58
    return-void
.end method

.method public setIntPreference(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 68
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/preferences/MusicPreferenceService;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferenceService;->notifyIntPreferenceChanged(Ljava/lang/String;I)V

    .line 70
    return-void
.end method

.method public setLongPreference(Ljava/lang/String;J)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 64
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/preferences/MusicPreferenceService;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/preferences/MusicPreferenceService;->notifyLongPreferenceChanged(Ljava/lang/String;J)V

    .line 66
    return-void
.end method

.method public setStringPreference(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferenceService;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferenceService;->notifyStringPreferenceChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public unregisterPreferenceChangeListener(Lcom/google/android/music/preferences/IPreferenceChangeListener;)V
    .locals 2
    .param p1, "l"    # Lcom/google/android/music/preferences/IPreferenceChangeListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    monitor-enter v1

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferenceService;->mChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 105
    monitor-exit v1

    .line 106
    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
