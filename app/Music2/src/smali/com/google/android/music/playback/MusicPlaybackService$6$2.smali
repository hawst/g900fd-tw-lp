.class Lcom/google/android/music/playback/MusicPlaybackService$6$2;
.super Ljava/lang/Object;
.source "MusicPlaybackService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/MusicPlaybackService$6;->onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$extras:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService$6;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iput-object p2, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$action:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$extras:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 651
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$action:Ljava/lang/String;

    const-string v4, "thumbs_up"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 652
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$extras:Landroid/os/Bundle;

    const-string v4, "thumbed_up"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 653
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/google/android/music/playback/MusicPlaybackService;->setRating(I)V

    .line 692
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3, v5}, Lcom/google/android/music/playback/MusicPlaybackService;->setRating(I)V

    goto :goto_0

    .line 657
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$action:Ljava/lang/String;

    const-string v4, "thumbs_down"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 658
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$extras:Landroid/os/Bundle;

    const-string v4, "thumbed_down"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    .line 659
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3, v6}, Lcom/google/android/music/playback/MusicPlaybackService;->setRating(I)V

    goto :goto_0

    .line 661
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3, v5}, Lcom/google/android/music/playback/MusicPlaybackService;->setRating(I)V

    goto :goto_0

    .line 663
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$action:Ljava/lang/String;

    const-string v4, "shuffle"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 664
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->toggleShuffle()V

    goto :goto_0

    .line 665
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$action:Ljava/lang/String;

    const-string v4, "start_radio"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 666
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$extras:Landroid/os/Bundle;

    const-class v4, Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 668
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->val$extras:Landroid/os/Bundle;

    const-string v4, "mix_description"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/mix/MixDescriptor;->unmarshall([B)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    .line 671
    .local v0, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    if-eqz v0, :cond_6

    .line 672
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    goto :goto_0

    .line 674
    :cond_6
    const-string v3, "MusicPlaybackService"

    const-string v4, "Failed to get mix descriptor."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 678
    .local v2, "refObject":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-static {v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 680
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01d6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 686
    .local v1, "error_message":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const/4 v4, 0x1

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V
    invoke-static {v3, v4, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$800(Lcom/google/android/music/playback/MusicPlaybackService;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 688
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 683
    .end local v1    # "error_message":Ljava/lang/String;
    :cond_7
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v3, v3, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01d7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .restart local v1    # "error_message":Ljava/lang/String;
    goto :goto_1

    .line 688
    .end local v1    # "error_message":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method
