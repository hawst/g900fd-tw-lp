.class public abstract Lcom/google/android/music/menu/MusicListMenu;
.super Lcom/google/android/music/menu/MusicMenu;
.source "MusicListMenu.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field protected mRadioButtonsEnabled:Z


# virtual methods
.method public getRadioButtonsEnabled()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/music/menu/MusicListMenu;->mRadioButtonsEnabled:Z

    return v0
.end method

.method protected onItemChecked(Lcom/google/android/music/menu/MusicMenuItem;Z)V
    .locals 4
    .param p1, "item"    # Lcom/google/android/music/menu/MusicMenuItem;
    .param p2, "checked"    # Z

    .prologue
    .line 63
    if-eqz p2, :cond_1

    iget-boolean v3, p0, Lcom/google/android/music/menu/MusicListMenu;->mRadioButtonsEnabled:Z

    if-eqz v3, :cond_1

    .line 64
    iget-object v3, p0, Lcom/google/android/music/menu/MusicListMenu;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 65
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 66
    iget-object v3, p0, Lcom/google/android/music/menu/MusicListMenu;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/menu/MusicListMenuItem;

    .line 67
    .local v2, "tmp":Lcom/google/android/music/menu/MusicListMenuItem;
    if-eq v2, p1, :cond_0

    .line 68
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/menu/MusicListMenuItem;->setCheckedInternal(Z)V

    .line 65
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    .end local v0    # "i":I
    .end local v1    # "size":I
    .end local v2    # "tmp":Lcom/google/android/music/menu/MusicListMenuItem;
    :cond_1
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 171
    const/4 v1, 0x4

    if-eq p2, v1, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_2

    .line 172
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenu;->close()V

    .line 177
    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performItemAction(Lcom/google/android/music/menu/MusicMenuItem;)Z
    .locals 2
    .param p1, "item"    # Lcom/google/android/music/menu/MusicMenuItem;

    .prologue
    .line 182
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/music/menu/MusicMenuItem;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 183
    :cond_0
    const/4 v0, 0x0

    .line 198
    :goto_0
    return v0

    .line 186
    :cond_1
    const/4 v0, 0x0

    .line 187
    .local v0, "invoked":Z
    invoke-virtual {p1}, Lcom/google/android/music/menu/MusicMenuItem;->getSubMenu()Lcom/google/android/music/menu/MusicListMenu;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 188
    const/4 v0, 0x1

    .line 189
    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenu;->close()V

    .line 190
    invoke-virtual {p1}, Lcom/google/android/music/menu/MusicMenuItem;->getSubMenu()Lcom/google/android/music/menu/MusicListMenu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/menu/MusicListMenu;->show()V

    goto :goto_0

    .line 192
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenu;->mCallback:Lcom/google/android/music/menu/MusicMenu$Callback;

    if-eqz v1, :cond_3

    .line 193
    iget-object v1, p0, Lcom/google/android/music/menu/MusicListMenu;->mCallback:Lcom/google/android/music/menu/MusicMenu$Callback;

    invoke-interface {v1, p1}, Lcom/google/android/music/menu/MusicMenu$Callback;->onMusicMenuItemSelected(Lcom/google/android/music/menu/MusicMenuItem;)Z

    move-result v0

    .line 196
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/menu/MusicListMenu;->close()V

    goto :goto_0
.end method

.method public abstract show()V
.end method
