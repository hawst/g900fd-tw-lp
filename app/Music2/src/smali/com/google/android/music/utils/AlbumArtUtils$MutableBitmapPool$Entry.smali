.class Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Entry"
.end annotation


# instance fields
.field public mBitmap:Landroid/graphics/Bitmap;

.field public mKey:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

.field private mNext:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

.field private mPrevious:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;


# direct methods
.method public constructor <init>(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "key"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mKey:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    .line 289
    iput-object p2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mBitmap:Landroid/graphics/Bitmap;

    .line 290
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mPrevious:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    .param p1, "x1"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mPrevious:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mNext:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;)Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;
    .param p1, "x1"    # Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mNext:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;

    return-object p1
.end method


# virtual methods
.method public getSizeBytes()I
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mBitmap:Landroid/graphics/Bitmap;

    # invokes: Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->getBitmapSizeBytes(Landroid/graphics/Bitmap;)I
    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->access$000(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->mKey:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    invoke-virtual {v1}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Entry;->getSizeBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
