.class public Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;
.super Lcom/google/android/music/download/keepon/IKeeponCallbackManager$Stub;
.source "KeeponCallbackManagerImpl.java"


# instance fields
.field final mProgressListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/google/android/music/download/IDownloadProgressListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/IKeeponCallbackManager$Stub;-><init>()V

    .line 18
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->mProgressListeners:Landroid/os/RemoteCallbackList;

    return-void
.end method


# virtual methods
.method public addDownloadProgressListener(Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/download/IDownloadProgressListener;)Z
    .locals 1
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "listener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 24
    if-eqz p2, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    move-result v0

    .line 27
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyListeners(Lcom/google/android/music/download/DownloadProgress;)V
    .locals 5
    .param p1, "progress"    # Lcom/google/android/music/download/DownloadProgress;

    .prologue
    .line 39
    iget-object v3, p0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 41
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 43
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/IDownloadProgressListener;

    invoke-interface {v3, p1}, Lcom/google/android/music/download/IDownloadProgressListener;->onDownloadProgress(Lcom/google/android/music/download/DownloadProgress;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "KeeponCallback"

    const-string v4, "Failed to call the download progress"

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 49
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    throw v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 51
    return-void
.end method

.method public removeDownloadProgressListener(Lcom/google/android/music/download/IDownloadProgressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 33
    if-eqz p1, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->mProgressListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 36
    :cond_0
    return-void
.end method
