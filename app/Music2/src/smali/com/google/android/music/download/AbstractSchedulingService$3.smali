.class Lcom/google/android/music/download/AbstractSchedulingService$3;
.super Lcom/google/android/music/download/IDownloadProgressListener$Stub;
.source "AbstractSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/AbstractSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/AbstractSchedulingService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService$3;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-direct {p0}, Lcom/google/android/music/download/IDownloadProgressListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadProgress(Lcom/google/android/music/download/DownloadProgress;)V
    .locals 2
    .param p1, "progress"    # Lcom/google/android/music/download/DownloadProgress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$3;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->FINISHED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v0, v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$3;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    check-cast p1, Lcom/google/android/music/download/TrackDownloadProgress;

    .end local p1    # "progress":Lcom/google/android/music/download/DownloadProgress;
    invoke-virtual {v0, p1}, Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateProgressMessage(Lcom/google/android/music/download/TrackDownloadProgress;)V

    .line 155
    :cond_0
    return-void
.end method
