.class interface abstract Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "MediaSessionImpl"
.end annotation


# virtual methods
.method public abstract getMediaSession()Ljava/lang/Object;
.end method

.method public abstract getRemoteControlClient()Ljava/lang/Object;
.end method

.method public abstract getSessionToken()Lcom/google/android/music/playback/session/MediaSessionCompat$Token;
.end method

.method public abstract release()V
.end method

.method public abstract setActive(Z)V
.end method

.method public abstract setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Landroid/os/Handler;)V
.end method

.method public abstract setExtras(Landroid/os/Bundle;)V
.end method

.method public abstract setFlags(I)V
.end method

.method public abstract setMetadata(Lcom/google/android/music/playback/session/MediaMetadataCompat;)V
.end method

.method public abstract setPlaybackState(Lcom/google/android/music/playback/session/PlaybackStateCompat;)V
.end method

.method public abstract setQueue(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setQueueTitle(Ljava/lang/CharSequence;)V
.end method

.method public abstract setSessionActivity(Landroid/app/PendingIntent;)V
.end method
