.class public Lcom/google/android/music/ui/GenresExploreFragment;
.super Lcom/google/android/music/ui/MediaListFragment;
.source "GenresExploreFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/GenresExploreFragment$1;,
        Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mGenreLevel:I

.field private mParentGenre:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "genreServerId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "subgenreCount"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/GenresExploreFragment;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListFragment;-><init>()V

    .line 104
    return-void
.end method

.method public static buildArguments(I)Landroid/os/Bundle;
    .locals 2
    .param p0, "genreLevel"    # I

    .prologue
    .line 37
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "level"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 39
    return-object v0
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListFragment;->initEmptyScreen()V

    .line 100
    const v0, 0x7f0b02a6

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenresExploreFragment;->setEmptyScreenText(I)V

    .line 101
    const v0, 0x7f0200f2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenresExploreFragment;->setEmptyImageView(I)V

    .line 102
    return-void
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;-><init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/GenresExploreFragment$1;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/GenresExploreFragment$GenresExploreAdapter;-><init>(Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment;Lcom/google/android/music/ui/GenresExploreFragment$1;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    .local v0, "extra":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 46
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Level of genres should be provided to explore genres"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 49
    :cond_0
    const-string v1, "nautilusId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/GenresExploreFragment;->mParentGenre:Ljava/lang/String;

    .line 50
    const-string v1, "level"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/GenresExploreFragment;->mGenreLevel:I

    .line 51
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreFragment;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 52
    new-instance v1, Lcom/google/android/music/ui/GenreExploreList;

    iget-object v2, p0, Lcom/google/android/music/ui/GenresExploreFragment;->mParentGenre:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/music/ui/GenreExploreList;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/music/ui/GenresExploreFragment;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/ui/GenresExploreFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 56
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 57
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 8
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 77
    invoke-super/range {p0 .. p5}, Lcom/google/android/music/ui/MediaListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 79
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 80
    .local v1, "extra":Landroid/os/Bundle;
    iget v2, p0, Lcom/google/android/music/ui/GenresExploreFragment;->mGenreLevel:I

    if-nez v2, :cond_0

    .line 81
    const-string v2, "nautilusId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "subgenreCount"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v0, v2, v3, v4, v6}, Lcom/google/android/music/ui/GenresExploreActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 94
    :goto_0
    invoke-virtual {v0, v5, v5}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 95
    return-void

    .line 86
    :cond_0
    iget v2, p0, Lcom/google/android/music/ui/GenresExploreFragment;->mGenreLevel:I

    if-ne v2, v6, :cond_1

    .line 87
    const-string v2, "nautilusId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/GenresExploreFragment;->mParentGenre:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/music/ui/SubGenresExploreActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 92
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Genres Explore only have two levels."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 73
    return-void
.end method
