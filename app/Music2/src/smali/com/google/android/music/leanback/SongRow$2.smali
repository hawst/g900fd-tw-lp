.class Lcom/google/android/music/leanback/SongRow$2;
.super Ljava/lang/Object;
.source "SongRow.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/SongRow;->loadRadioIntent(Lcom/google/android/music/leanback/SongRow$RadioIntentListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

.field final synthetic this$0:Lcom/google/android/music/leanback/SongRow;

.field final synthetic val$radioIntentListener:Lcom/google/android/music/leanback/SongRow$RadioIntentListener;

.field final synthetic val$songList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/SongRow;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/leanback/SongRow$RadioIntentListener;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/android/music/leanback/SongRow$2;->this$0:Lcom/google/android/music/leanback/SongRow;

    iput-object p2, p0, Lcom/google/android/music/leanback/SongRow$2;->val$songList:Lcom/google/android/music/medialist/SongList;

    iput-object p3, p0, Lcom/google/android/music/leanback/SongRow$2;->val$radioIntentListener:Lcom/google/android/music/leanback/SongRow$RadioIntentListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow$2;->this$0:Lcom/google/android/music/leanback/SongRow;

    # getter for: Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/leanback/SongRow;->access$000(Lcom/google/android/music/leanback/SongRow;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow$2;->val$songList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/SongRow$2;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 139
    return-void
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow$2;->this$0:Lcom/google/android/music/leanback/SongRow;

    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow$2;->this$0:Lcom/google/android/music/leanback/SongRow;

    # getter for: Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/leanback/SongRow;->access$000(Lcom/google/android/music/leanback/SongRow;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/SongRow$2;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    invoke-static {v1, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v1

    # setter for: Lcom/google/android/music/leanback/SongRow;->mRadioIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/SongRow;->access$102(Lcom/google/android/music/leanback/SongRow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 144
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow$2;->val$radioIntentListener:Lcom/google/android/music/leanback/SongRow$RadioIntentListener;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow$2;->val$radioIntentListener:Lcom/google/android/music/leanback/SongRow$RadioIntentListener;

    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow$2;->this$0:Lcom/google/android/music/leanback/SongRow;

    # getter for: Lcom/google/android/music/leanback/SongRow;->mRadioIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/music/leanback/SongRow;->access$100(Lcom/google/android/music/leanback/SongRow;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/leanback/SongRow$RadioIntentListener;->onRadioIntentLoaded(Landroid/content/Intent;)V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow$2;->this$0:Lcom/google/android/music/leanback/SongRow;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/leanback/SongRow;->mRadioClicked:Z
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/SongRow;->access$202(Lcom/google/android/music/leanback/SongRow;Z)Z

    .line 148
    return-void
.end method
