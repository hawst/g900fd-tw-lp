.class Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;
.super Ljava/lang/Object;
.source "ArtistAlbumsGridFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

.field final synthetic val$albumlist:Lcom/google/android/music/medialist/AlbumList;

.field final synthetic val$inflater:Landroid/view/LayoutInflater;

.field final synthetic val$lv:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/medialist/AlbumList;Landroid/view/LayoutInflater;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    iput-object p3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$inflater:Landroid/view/LayoutInflater;

    iput-object p4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 152
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 155
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    invoke-virtual {v2, v0}, Lcom/google/android/music/medialist/AlbumList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$202(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    instance-of v1, v1, Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    if-eqz v1, :cond_2

    .line 160
    iget-object v8, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    check-cast v8, Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    .line 162
    .local v8, "list":Lcom/google/android/music/medialist/NautilusArtistAlbumList;
    new-instance v9, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v9}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 163
    .local v9, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Lcom/google/android/music/store/Store;->canonicalizeAndGenerateId(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    .line 167
    .local v7, "idAndCanonical":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    new-instance v3, Lcom/google/android/music/medialist/ArtistAlbumList;

    iget-object v1, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v5, v1, v10}, Lcom/google/android/music/medialist/ArtistAlbumList;-><init>(JLjava/lang/String;Z)V

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;
    invoke-static {v2, v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$302(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/medialist/AlbumList;)Lcom/google/android/music/medialist/AlbumList;

    .line 168
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v1, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J
    invoke-static {v2, v4, v5}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$402(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;J)J

    .line 169
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v8}, Lcom/google/android/music/medialist/NautilusArtistAlbumList;->getNautilusId()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$502(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 170
    .end local v7    # "idAndCanonical":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v8    # "list":Lcom/google/android/music/medialist/NautilusArtistAlbumList;
    .end local v9    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    instance-of v1, v1, Lcom/google/android/music/medialist/ArtistAlbumList;

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$302(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/medialist/AlbumList;)Lcom/google/android/music/medialist/AlbumList;

    .line 172
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    invoke-virtual {v2, v0}, Lcom/google/android/music/medialist/AlbumList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v2

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J
    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$402(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;J)J

    .line 173
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 174
    const/4 v6, 0x0

    .line 176
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    invoke-virtual {v1, v0}, Lcom/google/android/music/medialist/AlbumList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ArtistMetajamId"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 181
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 182
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$502(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :cond_3
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1
.end method

.method public taskCompleted()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 194
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 195
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/medialist/AlbumList;

    move-result-object v2

    if-nez v2, :cond_1

    .line 200
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Local AlbumList was null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 204
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/music/ui/ActionBarController;->setActionBarTitle(Ljava/lang/String;)V

    .line 207
    new-instance v1, Lcom/google/android/music/medialist/ArtistSongList;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/medialist/AlbumList;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/music/medialist/AlbumList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v5}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->getArtistSongsSortOrder()I

    move-result v5

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    .line 210
    .local v1, "allSongList":Lcom/google/android/music/medialist/SongList;
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$600(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/AllSongsView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/music/ui/AllSongsView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 213
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$500(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 216
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$inflater:Landroid/view/LayoutInflater;

    iget-object v5, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3, v4, v5}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$800(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v2, v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$702(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 217
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$700(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    invoke-virtual {v2, v3, v8, v9}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 220
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$inflater:Landroid/view/LayoutInflater;

    iget-object v5, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3, v4, v5}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$800(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v2, v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$902(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 221
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$900(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    invoke-virtual {v2, v3, v8, v9}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 224
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$inflater:Landroid/view/LayoutInflater;

    iget-object v5, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3, v4, v5}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$800(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v2, v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1002(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 225
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1000(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    invoke-virtual {v2, v3, v8, v9}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 228
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$inflater:Landroid/view/LayoutInflater;

    iget-object v5, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3, v4, v5}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$800(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v2, v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1102(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 229
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    invoke-virtual {v2, v3, v8, v9}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 232
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$inflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400e8

    iget-object v5, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    invoke-virtual {v2, v4, v5, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/SpinnerHeaderView;

    # setter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mSpinner:Lcom/google/android/music/ui/SpinnerHeaderView;
    invoke-static {v3, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1202(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/SpinnerHeaderView;)Lcom/google/android/music/ui/SpinnerHeaderView;

    .line 234
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->val$lv:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mSpinner:Lcom/google/android/music/ui/SpinnerHeaderView;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/SpinnerHeaderView;

    move-result-object v3

    invoke-virtual {v2, v3, v8, v9}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 237
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v7

    .line 238
    .local v7, "lm":Landroid/support/v4/app/LoaderManager;
    const/16 v2, 0x64

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

    move-result-object v3

    invoke-virtual {v7, v2, v8, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 239
    const/16 v2, 0x65

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

    move-result-object v3

    invoke-virtual {v7, v2, v8, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 240
    const/16 v2, 0x66

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

    move-result-object v3

    invoke-virtual {v7, v2, v8, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 241
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->isYouTubeAvailable(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 242
    const/16 v2, 0x67

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

    move-result-object v3

    invoke-virtual {v7, v2, v8, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 248
    .end local v7    # "lm":Landroid/support/v4/app/LoaderManager;
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/medialist/AlbumList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->init(Lcom/google/android/music/medialist/AlbumList;)V

    goto/16 :goto_0
.end method
