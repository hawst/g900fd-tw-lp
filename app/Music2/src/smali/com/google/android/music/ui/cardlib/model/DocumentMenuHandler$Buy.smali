.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Buy"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1263
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1264
    const v0, 0x7f0e002b

    const v1, 0x7f0b0246

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1265
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 7

    .prologue
    .line 1269
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_1

    .line 1271
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 1272
    .local v0, "albumId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongStoreId()Ljava/lang/String;

    move-result-object v2

    .line 1273
    .local v2, "trackId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v5, v0, v2}, Lcom/google/android/music/purchase/Finsky;->getBuyAlbumUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1274
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1275
    .local v4, "url":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v5, v4}, Lcom/google/android/music/download/IntentConstants;->getStoreBuyIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1276
    .local v1, "shopIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v5, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1280
    .end local v0    # "albumId":Ljava/lang/String;
    .end local v1    # "shopIntent":Landroid/content/Intent;
    .end local v2    # "trackId":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v4    # "url":Ljava/lang/String;
    :goto_0
    return-void

    .line 1278
    :cond_1
    const-string v5, "DocumentMenuHandler"

    const-string v6, "Unexpected doc type"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
