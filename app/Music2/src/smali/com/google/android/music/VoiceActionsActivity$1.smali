.class Lcom/google/android/music/VoiceActionsActivity$1;
.super Ljava/lang/Object;
.source "VoiceActionsActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/VoiceActionsActivity;->processVoiceQuery()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/VoiceActionsActivity;

.field final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/VoiceActionsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/music/VoiceActionsActivity$1;->this$0:Lcom/google/android/music/VoiceActionsActivity;

    iput-object p2, p0, Lcom/google/android/music/VoiceActionsActivity$1;->val$query:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchComplete(ILcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 2
    .param p1, "code"    # I
    .param p2, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p3, "mix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 117
    packed-switch p1, :pswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 120
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/VoiceActionsActivity$1;->this$0:Lcom/google/android/music/VoiceActionsActivity;

    iget-object v1, p0, Lcom/google/android/music/VoiceActionsActivity$1;->val$query:Ljava/lang/String;

    # invokes: Lcom/google/android/music/VoiceActionsActivity;->launchSearchResults(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/music/VoiceActionsActivity;->access$000(Lcom/google/android/music/VoiceActionsActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/VoiceActionsActivity$1;->this$0:Lcom/google/android/music/VoiceActionsActivity;

    # invokes: Lcom/google/android/music/VoiceActionsActivity;->playIFLRadio()V
    invoke-static {v0}, Lcom/google/android/music/VoiceActionsActivity;->access$100(Lcom/google/android/music/VoiceActionsActivity;)V

    goto :goto_0

    .line 117
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
