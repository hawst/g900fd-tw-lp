.class public Lcom/google/android/music/cast/CastSessionManager;
.super Ljava/lang/Object;
.source "CastSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;,
        Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;
    }
.end annotation


# instance fields
.field private volatile mCastV2ReceiverLoaded:Z

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private volatile mSessionId:Ljava/lang/String;

.field private final mSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

.field private final mSessionStatusReceiver:Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sessionManagerCallback"    # Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v1, Lcom/google/android/music/cast/CastSessionManager$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/music/cast/CastSessionManager$1;-><init>(Lcom/google/android/music/cast/CastSessionManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mHandler:Landroid/os/Handler;

    .line 88
    new-instance v1, Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;-><init>(Lcom/google/android/music/cast/CastSessionManager;)V

    iput-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionStatusReceiver:Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;

    .line 91
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 92
    iput-object p1, p0, Lcom/google/android/music/cast/CastSessionManager;->mContext:Landroid/content/Context;

    .line 93
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 94
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.google.android.music.cast.ACTION_SESSION_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionStatusReceiver:Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 96
    iput-object p2, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/cast/CastSessionManager;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;
    .param p1, "x1"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/CastSessionManager;->handleStartSession(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/cast/CastSessionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->handleEndSession()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/cast/CastSessionManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->isLogVerbose()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/cast/CastSessionManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/cast/CastSessionManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/music/cast/CastSessionManager;Landroid/support/v7/media/MediaSessionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;
    .param p1, "x1"    # Landroid/support/v7/media/MediaSessionStatus;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/CastSessionManager;->processMediaSessionStatus(Landroid/support/v7/media/MediaSessionStatus;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/cast/CastSessionManager;)Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/cast/CastSessionManager;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/CastSessionManager;->processControlRequestResultBundle(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/cast/CastSessionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/CastSessionManager;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->reportSessionEnded()V

    return-void
.end method

.method private handleEndSession()V
    .locals 7

    .prologue
    .line 192
    iget-object v2, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    const-string v2, "MusicCastSession"

    const-string v3, "Missing session id"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :goto_0
    return-void

    .line 196
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.intent.action.END_SESSION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 197
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    const-string v2, "android.media.intent.extra.SESSION_ID"

    iget-object v3, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    iget-object v2, p0, Lcom/google/android/music/cast/CastSessionManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 202
    .local v1, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    const-string v2, "MusicCastSession"

    const-string v3, "Sending intent=%s extras=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_1
    new-instance v2, Lcom/google/android/music/cast/CastSessionManager$3;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/CastSessionManager$3;-><init>(Lcom/google/android/music/cast/CastSessionManager;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto :goto_0
.end method

.method private handleStartSession(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 8
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 134
    if-nez p1, :cond_0

    .line 135
    const-string v3, "MusicCastSession"

    const-string v4, "Invalid route: null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :goto_0
    return-void

    .line 139
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.media.intent.action.START_SESSION"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v3, "com.google.android.gms.cast.EXTRA_CAST_APPLICATION_ID"

    iget-object v4, p0, Lcom/google/android/music/cast/CastSessionManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/cast/CastUtils;->getCastV2AppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string v3, "com.google.android.gms.cast.EXTRA_CAST_STOP_APPLICATION_WHEN_SESSION_ENDS"

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 145
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->isLogVerbose()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 146
    const-string v3, "com.google.android.gms.cast.EXTRA_DEBUG_LOGGING_ENABLED"

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 148
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.music.cast.ACTION_SESSION_STATUS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 149
    .local v2, "statusIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/music/cast/CastSessionManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v2, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 152
    .local v1, "receiveUpdatesIntent":Landroid/app/PendingIntent;
    const-string v3, "android.media.intent.extra.SESSION_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 156
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->isLogVerbose()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 157
    const-string v3, "MusicCastSession"

    const-string v4, "Sending intent=%s extras=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_2
    new-instance v3, Lcom/google/android/music/cast/CastSessionManager$2;

    invoke-direct {v3, p0}, Lcom/google/android/music/cast/CastSessionManager$2;-><init>(Lcom/google/android/music/cast/CastSessionManager;)V

    invoke-virtual {p1, v0, v3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto :goto_0
.end method

.method private isLogVerbose()Z
    .locals 2

    .prologue
    .line 259
    const-string v0, "MusicCast"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isLogFilesEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processControlRequestResultBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 256
    return-void
.end method

.method private processMediaSessionStatus(Landroid/support/v7/media/MediaSessionStatus;)V
    .locals 4
    .param p1, "status"    # Landroid/support/v7/media/MediaSessionStatus;

    .prologue
    .line 230
    invoke-virtual {p1}, Landroid/support/v7/media/MediaSessionStatus;->getSessionState()I

    move-result v0

    .line 231
    .local v0, "state":I
    packed-switch v0, :pswitch_data_0

    .line 241
    const-string v1, "MusicCastSession"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unhandled session state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :goto_0
    :pswitch_0
    return-void

    .line 235
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->reportSessionEnded()V

    goto :goto_0

    .line 238
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/cast/CastSessionManager;->reportSessionEnded()V

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private reportSessionEnded()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;->onSessionEnded(Ljava/lang/String;)V

    .line 250
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/CastSessionManager;->setSessionId(Ljava/lang/String;)V

    .line 252
    return-void
.end method


# virtual methods
.method public endSession()V
    .locals 3

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 111
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 112
    return-void
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public isCastV2ReceiverLoaded()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mCastV2ReceiverLoaded:Z

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 100
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionStatusReceiver:Lcom/google/android/music/cast/CastSessionManager$SessionStatusReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    return-void
.end method

.method public setCastV2ReceiverLoaded()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mCastV2ReceiverLoaded:Z

    .line 127
    return-void
.end method

.method public setSessionId(Ljava/lang/String;)V
    .locals 1
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;

    .line 120
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/cast/CastSessionManager;->mCastV2ReceiverLoaded:Z

    .line 123
    :cond_0
    return-void
.end method

.method public startSession(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 105
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 106
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 107
    return-void
.end method
