.class Lcom/google/android/music/leanback/LeanbackPlayActivity$8;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;->createSongList(Lcom/google/android/music/medialist/SongList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

.field final synthetic val$songList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/medialist/SongList;)V
    .locals 0

    .prologue
    .line 781
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->val$songList:Lcom/google/android/music/medialist/SongList;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 792
    new-instance v3, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v3}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 793
    .local v3, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/music/xdi/TrackAdapter;->populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V

    .line 794
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 795
    .local v4, "position":I
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentQueuePosition:I
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2000(Lcom/google/android/music/leanback/LeanbackPlayActivity;)I

    move-result v2

    if-ne v4, v2, :cond_0

    move v6, v0

    .line 796
    .local v6, "isPlaying":Z
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v2, v4, :cond_1

    move v7, v0

    .line 797
    .local v7, "isLast":Z
    :goto_1
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Getting track "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " playing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v5, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->val$songList:Lcom/google/android/music/medialist/SongList;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILcom/google/android/music/medialist/SongList;ZZ)V

    return-object v0

    .end local v6    # "isPlaying":Z
    .end local v7    # "isLast":Z
    :cond_0
    move v6, v1

    .line 795
    goto :goto_0

    .restart local v6    # "isPlaying":Z
    :cond_1
    move v7, v1

    .line 796
    goto :goto_1
.end method

.method protected bindColumns(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 786
    new-instance v0, Lcom/google/android/music/xdi/TrackAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->val$songList:Lcom/google/android/music/medialist/SongList;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/music/xdi/TrackAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;->mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

    .line 788
    return-void
.end method
