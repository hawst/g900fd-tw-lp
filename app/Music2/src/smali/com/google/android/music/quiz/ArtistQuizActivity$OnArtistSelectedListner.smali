.class Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;
.super Ljava/lang/Object;
.source "ArtistQuizActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/quiz/ArtistQuizActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnArtistSelectedListner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;
    .param p2, "x1"    # Lcom/google/android/music/quiz/ArtistQuizActivity$1;

    .prologue
    .line 355
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 359
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;

    iget-object v1, v2, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArtistId:Ljava/lang/String;

    .line 360
    .local v1, "selectedId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;

    iget-object v0, v2, Lcom/google/android/music/quiz/ArtistQuizActivity$ArtistEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    .line 361
    .local v0, "mArt":Lcom/google/android/music/widgets/ArtImageCheckbox;
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$200(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 362
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$200(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 363
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 364
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setChecked(Z)V

    .line 370
    :goto_0
    return-void

    .line 366
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$200(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 367
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$OnArtistSelectedListner;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 368
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setChecked(Z)V

    goto :goto_0
.end method
