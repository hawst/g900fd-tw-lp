.class Lcom/google/android/music/ui/DownloadContainerFragment$1;
.super Ljava/lang/Object;
.source "DownloadContainerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/DownloadContainerFragment;->applyDifferences(Landroid/util/Pair;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

.field final synthetic val$toBeRemoved:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/DownloadContainerFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->val$toBeRemoved:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # getter for: Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;
    invoke-static {v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$200(Lcom/google/android/music/ui/DownloadContainerFragment;)Ljava/util/LinkedHashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->val$toBeRemoved:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 448
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # getter for: Lcom/google/android/music/ui/DownloadContainerFragment;->mProcessedData:Ljava/util/LinkedHashSet;
    invoke-static {v2}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$200(Lcom/google/android/music/ui/DownloadContainerFragment;)Ljava/util/LinkedHashSet;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/google/android/music/ui/DownloadContainerFragment;->mListBackingData:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$302(Lcom/google/android/music/ui/DownloadContainerFragment;Ljava/util/List;)Ljava/util/List;

    .line 449
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # getter for: Lcom/google/android/music/ui/DownloadContainerFragment;->mAdapter:Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;
    invoke-static {v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$400(Lcom/google/android/music/ui/DownloadContainerFragment;)Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/DownloadContainerFragment$DownloadListAdapter;->notifyDataSetChanged()V

    .line 450
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment$1;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    # invokes: Lcom/google/android/music/ui/DownloadContainerFragment;->updateButtonAndMenuVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/DownloadContainerFragment;->access$500(Lcom/google/android/music/ui/DownloadContainerFragment;)V

    .line 451
    return-void
.end method
