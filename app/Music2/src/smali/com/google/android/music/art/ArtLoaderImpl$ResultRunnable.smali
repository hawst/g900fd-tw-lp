.class Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;
.super Ljava/lang/Object;
.source "ArtLoaderImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtLoaderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResultRunnable"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/music/art/ArtLoader$Listener;

.field private final mPfd:Landroid/os/ParcelFileDescriptor;

.field private final mRemoteUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/ParcelFileDescriptor;Lcom/google/android/music/art/ArtLoader$Listener;Ljava/lang/String;)V
    .locals 0
    .param p1, "pfd"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "listener"    # Lcom/google/android/music/art/ArtLoader$Listener;
    .param p3, "remoteArtUrl"    # Ljava/lang/String;

    .prologue
    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mPfd:Landroid/os/ParcelFileDescriptor;

    .line 290
    iput-object p2, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;

    .line 291
    iput-object p3, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mRemoteUrl:Ljava/lang/String;

    .line 292
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mPfd:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;

    iget-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mRemoteUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mPfd:Landroid/os/ParcelFileDescriptor;

    invoke-interface {v0, v1, v2}, Lcom/google/android/music/art/ArtLoader$Listener;->onSuccess(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    .line 301
    :goto_0
    return-void

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mListener:Lcom/google/android/music/art/ArtLoader$Listener;

    iget-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;->mRemoteUrl:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/music/art/ArtLoader$Listener;->onFailed(Ljava/lang/String;)V

    goto :goto_0
.end method
