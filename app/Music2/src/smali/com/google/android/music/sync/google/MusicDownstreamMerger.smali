.class Lcom/google/android/music/sync/google/MusicDownstreamMerger;
.super Lcom/google/android/music/sync/common/DownstreamMerger;
.source "MusicDownstreamMerger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mBlockMerger:Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

.field private final mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mLastNotificationTimeNs:J

.field private mNotificationPeriodNs:J

.field private final mProtocolState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoteAccount:I

.field private final mStore:Lcom/google/android/music/store/Store;

.field private mUpdateRecentItemsIncrementally:Z

.field private final mUseVerboseLogging:Z


# direct methods
.method public constructor <init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;ILandroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V
    .locals 2
    .param p1, "mergeQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;
    .param p2, "maxBlockSize"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p5, "logTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;",
            "I",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    .local p4, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/music/sync/common/DownstreamMerger;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;ILjava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mUpdateRecentItemsIncrementally:Z

    .line 55
    const-wide/32 v0, 0x3b9aca00

    iput-wide v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mNotificationPeriodNs:J

    .line 73
    const-class v0, Ljava/lang/Integer;

    const-string v1, "remote_account"

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mRemoteAccount:I

    .line 74
    invoke-static {p3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mStore:Lcom/google/android/music/store/Store;

    .line 75
    iput-object p4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    .line 76
    const-class v0, Landroid/accounts/Account;

    const-string v1, "account"

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mAccount:Landroid/accounts/Account;

    .line 77
    iput-object p3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mContext:Landroid/content/Context;

    .line 78
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mUseVerboseLogging:Z

    .line 79
    return-void
.end method

.method private saveLastModifiedTimeIfLatest(Lcom/google/android/music/sync/common/QueueableSyncEntity;)V
    .locals 5
    .param p1, "serverEntity"    # Lcom/google/android/music/sync/common/QueueableSyncEntity;

    .prologue
    .line 90
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v2, :cond_1

    .line 91
    const-class v2, Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/Track;

    iget-wide v0, v2, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    .line 92
    .local v0, "serverLastModTime":J
    const-class v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v4, "merger_track_version"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v3, "merger_track_version"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    .end local v0    # "serverLastModTime":J
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v2, :cond_2

    .line 97
    const-class v2, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    iget-wide v0, v2, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    .line 99
    .restart local v0    # "serverLastModTime":J
    const-class v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v4, "merger_playlist_version"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v3, "merger_playlist_version"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 104
    .end local v0    # "serverLastModTime":J
    :cond_2
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v2, :cond_3

    .line 105
    const-class v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    iget-wide v0, v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    .line 107
    .restart local v0    # "serverLastModTime":J
    const-class v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v4, "merger_plentry_version"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v3, "merger_plentry_version"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 111
    .end local v0    # "serverLastModTime":J
    :cond_3
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v2, :cond_4

    .line 112
    const-class v2, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    iget-wide v0, v2, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    .line 114
    .restart local v0    # "serverLastModTime":J
    const-class v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v4, "merger_radio_station_version"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v3, "merger_radio_station_version"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 120
    .end local v0    # "serverLastModTime":J
    :cond_4
    instance-of v2, p1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    if-eqz v2, :cond_0

    .line 121
    const-class v2, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    iget-wide v0, v2, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    .line 123
    .restart local v0    # "serverLastModTime":J
    const-class v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v4, "merger_blacklist_item_version"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    .line 126
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v3, "merger_blacklist_item_version"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method


# virtual methods
.method public onEndMergeBlock(Z)V
    .locals 14
    .param p1, "isBlockMergeSuccessful"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 147
    if-eqz p1, :cond_0

    .line 148
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mAccount:Landroid/accounts/Account;

    invoke-static {}, Lcom/google/android/music/sync/google/ClientSyncState;->newBuilder()Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/Integer;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "remote_account"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v8, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteAccount(I)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/Long;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "merger_track_version"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteTrackVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/Long;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "merger_playlist_version"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlaylistVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/Long;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "merger_plentry_version"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlentryVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/Long;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "merger_radio_station_version"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteRadioStationVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/Long;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "merger_blacklist_item_version"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteBlacklistItemVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "etag_track"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v8, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setEtagTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "etag_playlist"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v8, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setEtagPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v8

    const-class v3, Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    const-string v10, "etag_playlist_entry"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v8, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setEtagPlaylistEntry(Ljava/lang/String;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->build()Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v3

    invoke-static {v6, v7, v3}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->set(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Lcom/google/android/music/sync/google/ClientSyncState;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mBlockMerger:Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->safelyCloseStatements()V

    .line 178
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mStore:Lcom/google/android/music/store/Store;

    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3, v6, p1}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 179
    iput-object v12, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 180
    if-eqz p1, :cond_1

    .line 181
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mBlockMerger:Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->cleanupLocallyCachedFiles()V

    .line 184
    :cond_1
    const-string v3, "MusicSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 185
    const-string v3, "MusicSyncAdapter"

    const-string v6, "Merger: End of block."

    invoke-static {v3, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_2
    if-eqz p1, :cond_4

    .line 188
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mUpdateRecentItemsIncrementally:Z

    if-eqz v3, :cond_3

    .line 189
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/RecentItemsManager;->countRecentItems(Landroid/content/Context;)I

    move-result v3

    const/16 v6, 0x32

    if-lt v3, v6, :cond_6

    .line 193
    iput-boolean v13, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mUpdateRecentItemsIncrementally:Z

    .line 200
    :cond_3
    :goto_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 201
    .local v4, "nowNs":J
    iget-wide v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mLastNotificationTimeNs:J

    sub-long v0, v4, v6

    .line 202
    .local v0, "deltaNs":J
    iget-wide v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mNotificationPeriodNs:J

    cmp-long v3, v0, v6

    if-ltz v3, :cond_4

    .line 203
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v6, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v6, v12, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 205
    iput-wide v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mLastNotificationTimeNs:J

    .line 206
    const-wide v6, 0x2540be400L

    iget-wide v8, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mNotificationPeriodNs:J

    const-wide/32 v10, 0x3b9aca00

    add-long/2addr v8, v10

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mNotificationPeriodNs:J

    .line 210
    .end local v0    # "deltaNs":J
    .end local v4    # "nowNs":J
    :cond_4
    return-void

    .line 174
    :catch_0
    move-exception v2

    .line 175
    .local v2, "e":Lcom/google/android/music/sync/common/ProviderException;
    :try_start_1
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v6, "Unable to set the sync state: "

    invoke-direct {v3, v6, v2}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    .end local v2    # "e":Lcom/google/android/music/sync/common/ProviderException;
    :catchall_0
    move-exception v3

    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mBlockMerger:Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    invoke-virtual {v6}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->safelyCloseStatements()V

    .line 178
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mStore:Lcom/google/android/music/store/Store;

    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v6, v7, p1}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 179
    iput-object v12, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 180
    if-eqz p1, :cond_5

    .line 181
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mBlockMerger:Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    invoke-virtual {v6}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->cleanupLocallyCachedFiles()V

    :cond_5
    throw v3

    .line 197
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/RecentItemsManager;->updateRecentItemsAsync(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onStartMergeBlock()V
    .locals 7

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 85
    new-instance v0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mProtocolState:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mUseVerboseLogging:Z

    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mTag:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mRemoteAccount:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;-><init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;ZLjava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mBlockMerger:Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    .line 87
    return-void
.end method

.method public processMergeItem(Lcom/google/android/music/sync/common/QueueableSyncEntity;Lcom/google/android/music/sync/common/QueueableSyncEntity;)V
    .locals 2
    .param p1, "serverEntity"    # Lcom/google/android/music/sync/common/QueueableSyncEntity;
    .param p2, "clientEntity"    # Lcom/google/android/music/sync/common/QueueableSyncEntity;

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->mBlockMerger:Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->processMergeItem(Lcom/google/android/music/sync/common/QueueableSyncEntity;Lcom/google/android/music/sync/common/QueueableSyncEntity;)Z

    move-result v0

    .line 137
    .local v0, "processSuccessful":Z
    if-eqz v0, :cond_0

    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicDownstreamMerger;->saveLastModifiedTimeIfLatest(Lcom/google/android/music/sync/common/QueueableSyncEntity;)V

    .line 142
    :cond_0
    return-void
.end method
