.class public Lcom/google/android/music/RatingSelector;
.super Landroid/widget/FrameLayout;
.source "RatingSelector.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/RatingSelector$Asset;
    }
.end annotation


# static fields
.field private static final NORMAL_ASSETS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/RatingSelector$Asset;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TABLET_ASSETS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/RatingSelector$Asset;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAssets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/RatingSelector$Asset;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mIsAttached:Z

.field private mRating:I

.field private mRatingsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/widget/ImageView;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Lcom/google/android/music/playback/IMusicPlaybackService;

.field private mStars:[Landroid/widget/ImageView;

.field private mStarsView:Landroid/view/View;

.field private mThumbsDown:Landroid/widget/ImageView;

.field private mThumbsUp:Landroid/widget/ImageView;

.field private mThumbsView:Landroid/view/View;

.field private mToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

.field private mUseStars:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/android/music/RatingSelector$Asset;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/music/RatingSelector;->TABLET_ASSETS:Ljava/util/Map;

    .line 38
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/android/music/RatingSelector$Asset;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/music/RatingSelector;->NORMAL_ASSETS:Ljava/util/Map;

    .line 49
    sget-object v0, Lcom/google/android/music/RatingSelector;->TABLET_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020166

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/google/android/music/RatingSelector;->TABLET_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020167

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/google/android/music/RatingSelector;->TABLET_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020162

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/music/RatingSelector;->TABLET_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020163

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/google/android/music/RatingSelector;->NORMAL_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020168

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/google/android/music/RatingSelector;->NORMAL_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020169

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/android/music/RatingSelector;->NORMAL_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020164

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/google/android/music/RatingSelector;->NORMAL_ASSETS:Ljava/util/Map;

    sget-object v1, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    const v2, 0x7f020165

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/RatingSelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/RatingSelector;->mIsAttached:Z

    .line 96
    iput-object p1, p0, Lcom/google/android/music/RatingSelector;->mContext:Landroid/content/Context;

    .line 97
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 98
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    sget-object v1, Lcom/google/android/music/RatingSelector;->TABLET_ASSETS:Ljava/util/Map;

    iput-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    sget-object v1, Lcom/google/android/music/RatingSelector;->NORMAL_ASSETS:Ljava/util/Map;

    iput-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/music/RatingSelector;)Lcom/google/android/music/playback/IMusicPlaybackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/RatingSelector;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/RatingSelector;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/RatingSelector;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/RatingSelector;

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/music/RatingSelector;->mRating:I

    return v0
.end method

.method private changeRating(I)V
    .locals 2
    .param p1, "rating"    # I

    .prologue
    .line 214
    iget v0, p0, Lcom/google/android/music/RatingSelector;->mRating:I

    if-ne v0, p1, :cond_0

    .line 216
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/RatingSelector;->mRating:I

    .line 222
    :goto_0
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/RatingSelector$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/RatingSelector$1;-><init>(Lcom/google/android/music/RatingSelector;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 232
    invoke-direct {p0}, Lcom/google/android/music/RatingSelector;->updateImages()V

    .line 233
    return-void

    .line 219
    :cond_0
    iput p1, p0, Lcom/google/android/music/RatingSelector;->mRating:I

    goto :goto_0
.end method

.method public static convertRatingToThumbs(I)I
    .locals 0
    .param p0, "rating"    # I

    .prologue
    .line 190
    packed-switch p0, :pswitch_data_0

    .line 202
    :pswitch_0
    const/4 p0, 0x0

    .line 205
    :goto_0
    return p0

    .line 193
    :pswitch_1
    const/4 p0, 0x1

    .line 194
    goto :goto_0

    .line 197
    :pswitch_2
    const/4 p0, 0x5

    .line 198
    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private prepButton(Landroid/widget/ImageView;ILcom/google/android/music/preferences/MusicPreferences;)V
    .locals 1
    .param p1, "button"    # Landroid/widget/ImageView;
    .param p2, "padding"    # I
    .param p3, "preferences"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 160
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    invoke-virtual {p3}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    invoke-virtual {p1, p2, p2, p2, p2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 164
    :cond_0
    return-void
.end method

.method private updateImages()V
    .locals 4

    .prologue
    .line 239
    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mThumbsView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 241
    iget v1, p0, Lcom/google/android/music/RatingSelector;->mRating:I

    packed-switch v1, :pswitch_data_0

    .line 259
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/music/RatingSelector;->mThumbsDown:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    sget-object v3, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 261
    iget-object v2, p0, Lcom/google/android/music/RatingSelector;->mThumbsUp:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    sget-object v3, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 266
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mStarsView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 268
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    .line 269
    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    aget-object v2, v1, v0

    iget v1, p0, Lcom/google/android/music/RatingSelector;->mRating:I

    if-le v1, v0, :cond_1

    const v1, 0x7f020070

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 244
    .end local v0    # "i":I
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/music/RatingSelector;->mThumbsDown:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    sget-object v3, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 246
    iget-object v2, p0, Lcom/google/android/music/RatingSelector;->mThumbsUp:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    sget-object v3, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 251
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/music/RatingSelector;->mThumbsDown:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    sget-object v3, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_DOWN_DEFAULT:Lcom/google/android/music/RatingSelector$Asset;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 253
    iget-object v2, p0, Lcom/google/android/music/RatingSelector;->mThumbsUp:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mAssets:Ljava/util/Map;

    sget-object v3, Lcom/google/android/music/RatingSelector$Asset;->THUMBS_UP_SELECTED:Lcom/google/android/music/RatingSelector$Asset;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 269
    .restart local v0    # "i":I
    :cond_1
    const v1, 0x7f02006f

    goto :goto_2

    .line 273
    .end local v0    # "i":I
    :cond_2
    return-void

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 289
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/RatingSelector;->mIsAttached:Z

    .line 291
    invoke-virtual {p0}, Lcom/google/android/music/RatingSelector;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/RatingSelector;->mToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 292
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 169
    iget-object v1, p0, Lcom/google/android/music/RatingSelector;->mRatingsMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 170
    .local v0, "rating":I
    if-eqz p1, :cond_0

    .line 171
    invoke-direct {p0, v0}, Lcom/google/android/music/RatingSelector;->changeRating(I)V

    .line 175
    return-void

    .line 173
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown view clicked on: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/google/android/music/RatingSelector;->mIsAttached:Z

    if-eqz v0, :cond_0

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/RatingSelector;->mIsAttached:Z

    .line 301
    iget-object v0, p0, Lcom/google/android/music/RatingSelector;->mToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V

    .line 303
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 304
    return-void
.end method

.method protected onFinishInflate()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x5

    const/4 v6, 0x0

    .line 111
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 112
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/music/RatingSelector;->mRatingsMap:Ljava/util/Map;

    .line 115
    const v3, 0x7f0e0249

    invoke-virtual {p0, v3}, Lcom/google/android/music/RatingSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsView:Landroid/view/View;

    .line 116
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsView:Landroid/view/View;

    const v4, 0x7f0e0202

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsUp:Landroid/widget/ImageView;

    .line 117
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsView:Landroid/view/View;

    const v4, 0x7f0e024a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsDown:Landroid/widget/ImageView;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/music/RatingSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00b0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 120
    .local v1, "imagePadding":I
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mContext:Landroid/content/Context;

    invoke-static {v3, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 122
    .local v2, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsUp:Landroid/widget/ImageView;

    invoke-direct {p0, v3, v1, v2}, Lcom/google/android/music/RatingSelector;->prepButton(Landroid/widget/ImageView;ILcom/google/android/music/preferences/MusicPreferences;)V

    .line 123
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mRatingsMap:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mThumbsUp:Landroid/widget/ImageView;

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsDown:Landroid/widget/ImageView;

    invoke-direct {p0, v3, v1, v2}, Lcom/google/android/music/RatingSelector;->prepButton(Landroid/widget/ImageView;ILcom/google/android/music/preferences/MusicPreferences;)V

    .line 125
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mRatingsMap:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mThumbsDown:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const v3, 0x7f0e0243

    invoke-virtual {p0, v3}, Lcom/google/android/music/RatingSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/RatingSelector;->mStarsView:Landroid/view/View;

    .line 129
    const/4 v3, 0x5

    new-array v3, v3, [Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    .line 130
    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    const/4 v5, 0x0

    const v3, 0x7f0e0244

    invoke-virtual {p0, v3}, Lcom/google/android/music/RatingSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 131
    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    const/4 v5, 0x1

    const v3, 0x7f0e0245

    invoke-virtual {p0, v3}, Lcom/google/android/music/RatingSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 132
    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    const/4 v5, 0x2

    const v3, 0x7f0e0246

    invoke-virtual {p0, v3}, Lcom/google/android/music/RatingSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 133
    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    const/4 v5, 0x3

    const v3, 0x7f0e0247

    invoke-virtual {p0, v3}, Lcom/google/android/music/RatingSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 134
    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    const/4 v5, 0x4

    const v3, 0x7f0e0248

    invoke-virtual {p0, v3}, Lcom/google/android/music/RatingSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 135
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v7, :cond_0

    .line 136
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4, v2}, Lcom/google/android/music/RatingSelector;->prepButton(Landroid/widget/ImageView;ILcom/google/android/music/preferences/MusicPreferences;)V

    .line 137
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mRatingsMap:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/music/RatingSelector;->mStars:[Landroid/widget/ImageView;

    aget-object v4, v4, v0

    add-int/lit8 v5, v0, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 144
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_use_star_ratings"

    invoke-static {v3, v4, v6}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/music/RatingSelector;->mUseStars:Z

    .line 146
    iget-boolean v3, p0, Lcom/google/android/music/RatingSelector;->mUseStars:Z

    if-eqz v3, :cond_1

    .line 147
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mStarsView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :goto_1
    return-void

    .line 140
    .end local v0    # "i":I
    :catchall_0
    move-exception v3

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3

    .line 150
    .restart local v0    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mThumbsView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v3, p0, Lcom/google/android/music/RatingSelector;->mStarsView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 278
    invoke-static {p2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/RatingSelector;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 279
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/RatingSelector;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 285
    return-void
.end method

.method public setRating(I)V
    .locals 1
    .param p1, "rating"    # I

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/google/android/music/RatingSelector;->mUseStars:Z

    if-eqz v0, :cond_0

    .end local p1    # "rating":I
    :goto_0
    iput p1, p0, Lcom/google/android/music/RatingSelector;->mRating:I

    .line 183
    invoke-direct {p0}, Lcom/google/android/music/RatingSelector;->updateImages()V

    .line 184
    return-void

    .line 182
    .restart local p1    # "rating":I
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/RatingSelector;->convertRatingToThumbs(I)I

    move-result p1

    goto :goto_0
.end method
