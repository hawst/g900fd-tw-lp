.class public final enum Lcom/google/android/music/ui/HomeActivity$Screen;
.super Ljava/lang/Enum;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Screen"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/ui/HomeActivity$Screen;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum ACCOUNT:Lcom/google/android/music/ui/HomeActivity$Screen;

.field private static final DEFAULT_FRAGMENTS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final EXPERIMENTAL_FRAGMENTS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final enum EXPLORE:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum FEEDBACK:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum HELP:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum INSTANT_MIXES:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum MY_LIBRARY:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum NO_CONTENT:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum PLAYLISTS:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum RADIO:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum SHOP:Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final enum SYNCING:Lcom/google/android/music/ui/HomeActivity$Screen;

.field private static final TAG_TO_SCREEN:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mIconResourceId:I

.field private final mInDownloadOnly:Z

.field private final mSelectedIconResourceId:I

.field private final mTag:Ljava/lang/String;

.field private final mTitleResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "MAINSTAGE"

    const/4 v2, 0x0

    const-string v3, "mainstage"

    const v4, 0x7f0b0096

    const/4 v5, 0x1

    const v6, 0x7f0200e7

    const v7, 0x7f0200e8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 54
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "MY_LIBRARY"

    const/4 v2, 0x1

    const-string v3, "library"

    const v4, 0x7f0b0097

    const/4 v5, 0x1

    const v6, 0x7f0200e9

    const v7, 0x7f0200ea

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->MY_LIBRARY:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 56
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "PLAYLISTS"

    const/4 v2, 0x2

    const-string v3, "playlists"

    const v4, 0x7f0b0098

    const/4 v5, 0x1

    const v6, 0x7f0200eb

    const v7, 0x7f0200ec

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->PLAYLISTS:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 58
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "INSTANT_MIXES"

    const/4 v2, 0x3

    const-string v3, "mixes"

    const v4, 0x7f0b009a

    const/4 v5, 0x1

    const v6, 0x7f0200ed

    const v7, 0x7f0200ee

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->INSTANT_MIXES:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 60
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "RADIO"

    const/4 v2, 0x4

    const-string v3, "radio"

    const v4, 0x7f0b0099

    const/4 v5, 0x1

    const v6, 0x7f0200ed

    const v7, 0x7f0200ee

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->RADIO:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 62
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "EXPLORE"

    const/4 v2, 0x5

    const-string v3, "explore"

    const v4, 0x7f0b009b

    const/4 v5, 0x0

    const v6, 0x7f0200e5

    const v7, 0x7f0200e6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPLORE:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 64
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "SHOP"

    const/4 v2, 0x6

    const-string v3, "shop"

    const v4, 0x7f0b009c

    const/4 v5, 0x0

    const v6, 0x7f0200ef

    const v7, 0x7f0200f0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->SHOP:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 66
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "NO_CONTENT"

    const/4 v2, 0x7

    const-string v3, "nocontent"

    const v4, 0x7f0b00e3

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->NO_CONTENT:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 67
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "SYNCING"

    const/16 v2, 0x8

    const-string v3, "syncing"

    const v4, 0x7f0b00e3

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->SYNCING:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 68
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "SETTINGS"

    const/16 v2, 0x9

    const-string v3, "settings"

    const v4, 0x7f0b0058

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 69
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "HELP"

    const/16 v2, 0xa

    const-string v3, "help"

    const v4, 0x7f0b008d

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->HELP:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 70
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "FEEDBACK"

    const/16 v2, 0xb

    const-string v3, "feedback"

    const v4, 0x7f0b008e

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->FEEDBACK:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 71
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    const-string v1, "ACCOUNT"

    const/16 v2, 0xc

    const-string v3, "account"

    const v4, 0x7f0b00e3

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/ui/HomeActivity$Screen;-><init>(Ljava/lang/String;ILjava/lang/String;IZII)V

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->ACCOUNT:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 51
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/android/music/ui/HomeActivity$Screen;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->MY_LIBRARY:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->PLAYLISTS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->INSTANT_MIXES:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->RADIO:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPLORE:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->SHOP:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->NO_CONTENT:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->SYNCING:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->HELP:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->FEEDBACK:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->ACCOUNT:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->$VALUES:[Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 73
    invoke-static {}, Lcom/google/android/music/ui/HomeActivity$Screen;->createTagToScreenMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->TAG_TO_SCREEN:Ljava/util/Map;

    .line 101
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v8

    .line 102
    .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/music/ui/HomeActivity$Screen;Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;>;"
    invoke-static {v8}, Lcom/google/android/music/ui/HomeActivity$Screen;->addCommonFragments(Ljava/util/Map;)V

    .line 103
    invoke-static {v8}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->DEFAULT_FRAGMENTS:Ljava/util/Map;

    .line 105
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v8

    .line 106
    invoke-static {v8}, Lcom/google/android/music/ui/HomeActivity$Screen;->addCommonFragments(Ljava/util/Map;)V

    .line 108
    invoke-static {v8}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPERIMENTAL_FRAGMENTS:Ljava/util/Map;

    .line 109
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IZII)V
    .locals 0
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "titleResId"    # I
    .param p5, "inDownloadOnly"    # Z
    .param p6, "iconResourceId"    # I
    .param p7, "selectedIconResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZII)V"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 132
    iput-object p3, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mTag:Ljava/lang/String;

    .line 133
    iput p4, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mTitleResId:I

    .line 134
    iput-boolean p5, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mInDownloadOnly:Z

    .line 135
    iput p6, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mIconResourceId:I

    .line 136
    iput p7, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mSelectedIconResourceId:I

    .line 137
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/HomeActivity$Screen;Landroid/content/Context;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/HomeActivity$Screen;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->createFragment(Landroid/content/Context;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/HomeActivity$Screen;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mTitleResId:I

    return v0
.end method

.method private static addCommonFragments(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/music/ui/HomeActivity$Screen;Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;>;"
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/MaterialMainstageFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->MY_LIBRARY:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/MyLibraryFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->PLAYLISTS:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/PlaylistClustersFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->INSTANT_MIXES:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/InstantMixesFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->RADIO:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/RadioFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPLORE:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/ExploreFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->NO_CONTENT:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/EmptyLibraryFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->SYNCING:Lcom/google/android/music/ui/HomeActivity$Screen;

    const-class v1, Lcom/google/android/music/ui/SyncingLibraryFragment;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    return-void
.end method

.method private createFragment(Landroid/content/Context;)Landroid/support/v4/app/Fragment;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getCurrentMap(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v3

    .line 203
    .local v3, "fragmentMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/music/ui/HomeActivity$Screen;Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;>;"
    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 204
    .local v2, "fragment":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;"
    invoke-static {v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/Fragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 214
    :goto_0
    return-object v4

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Ljava/lang/InstantiationException;
    move-object v1, v0

    .line 213
    .end local v0    # "e":Ljava/lang/InstantiationException;
    .local v1, "error":Ljava/lang/Exception;
    :goto_1
    const-string v4, "MusicHomeActivity"

    const-string v5, "showSingleFragmentScreen"

    invoke-static {v4, v5, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 214
    const/4 v4, 0x0

    goto :goto_0

    .line 210
    .end local v1    # "error":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/IllegalAccessException;
    move-object v1, v0

    .restart local v1    # "error":Ljava/lang/Exception;
    goto :goto_1
.end method

.method private static createTagToScreenMap()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    .line 124
    .local v3, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/music/ui/HomeActivity$Screen;>;"
    invoke-static {}, Lcom/google/android/music/ui/HomeActivity$Screen;->values()[Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/ui/HomeActivity$Screen;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 125
    .local v4, "screen":Lcom/google/android/music/ui/HomeActivity$Screen;
    iget-object v5, v4, Lcom/google/android/music/ui/HomeActivity$Screen;->mTag:Ljava/lang/String;

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 127
    .end local v4    # "screen":Lcom/google/android/music/ui/HomeActivity$Screen;
    :cond_0
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    return-object v5
.end method

.method public static fromTag(Ljava/lang/String;Lcom/google/android/music/ui/HomeActivity$Screen;)Lcom/google/android/music/ui/HomeActivity$Screen;
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 147
    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->TAG_TO_SCREEN:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 148
    .local v0, "result":Lcom/google/android/music/ui/HomeActivity$Screen;
    if-eqz v0, :cond_0

    .end local v0    # "result":Lcom/google/android/music/ui/HomeActivity$Screen;
    :goto_0
    return-object v0

    .restart local v0    # "result":Lcom/google/android/music/ui/HomeActivity$Screen;
    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public static getActiveScreen(Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/HomeActivity$Screen;
    .locals 5
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 224
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 225
    .local v1, "extras":Landroid/os/Bundle;
    if-nez v1, :cond_1

    .line 226
    instance-of v4, p0, Lcom/google/android/music/ui/HomeActivity;

    if-eqz v4, :cond_0

    .line 227
    # getter for: Lcom/google/android/music/ui/HomeActivity;->DEFAULT_SCREEN:Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-static {}, Lcom/google/android/music/ui/HomeActivity;->access$000()Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v4

    .line 238
    :goto_0
    return-object v4

    .line 229
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 232
    :cond_1
    const-string v4, "music:home:screen"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 233
    .local v3, "tag":Ljava/lang/String;
    const/4 v0, 0x0

    .line 234
    .local v0, "defaultScreen":Lcom/google/android/music/ui/HomeActivity$Screen;
    instance-of v4, p0, Lcom/google/android/music/ui/HomeActivity;

    if-eqz v4, :cond_2

    .line 236
    # getter for: Lcom/google/android/music/ui/HomeActivity;->DEFAULT_SCREEN:Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-static {}, Lcom/google/android/music/ui/HomeActivity;->access$000()Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v0

    .line 238
    :cond_2
    invoke-static {v3, v0}, Lcom/google/android/music/ui/HomeActivity$Screen;->fromTag(Ljava/lang/String;Lcom/google/android/music/ui/HomeActivity$Screen;)Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v4

    goto :goto_0
.end method

.method private getCurrentMap(Landroid/content/Context;)Ljava/util/Map;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_enable_experimental_ui"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 178
    .local v0, "enableMaterial":Z
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPERIMENTAL_FRAGMENTS:Ljava/util/Map;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->DEFAULT_FRAGMENTS:Ljava/util/Map;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/ui/HomeActivity$Screen;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/HomeActivity$Screen;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/ui/HomeActivity$Screen;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->$VALUES:[Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-virtual {v0}, [Lcom/google/android/music/ui/HomeActivity$Screen;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/ui/HomeActivity$Screen;

    return-object v0
.end method


# virtual methods
.method public getIconResourceId()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mIconResourceId:I

    return v0
.end method

.method public getSelectedIconResourceId()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mSelectedIconResourceId:I

    return v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mTitleResId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isExternalLink(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getCurrentMap(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v0

    .line 198
    .local v0, "fragmentMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/music/ui/HomeActivity$Screen;Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;>;"
    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public isInDownloadOnly()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity$Screen;->mInDownloadOnly:Z

    return v0
.end method

.method public isPrimary()Z
    .locals 2

    .prologue
    .line 242
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$5;->$SwitchMap$com$google$android$music$ui$HomeActivity$Screen:[I

    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity$Screen;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 252
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 250
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isSecondary()Z
    .locals 2

    .prologue
    .line 256
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$5;->$SwitchMap$com$google$android$music$ui$HomeActivity$Screen:[I

    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity$Screen;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 262
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 260
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
