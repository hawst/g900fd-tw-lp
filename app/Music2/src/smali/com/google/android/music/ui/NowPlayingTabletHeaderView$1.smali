.class Lcom/google/android/music/ui/NowPlayingTabletHeaderView$1;
.super Landroid/content/BroadcastReceiver;
.source "NowPlayingTabletHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingTabletHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingTabletHeaderView;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView$1;->this$0:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 62
    const-string v3, "inErrorState"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 64
    .local v0, "isInErrorState":Z
    const-string v3, "streaming"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 66
    .local v2, "isStreaming":Z
    const-string v3, "preparing"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 68
    .local v1, "isPreparing":Z
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView$1;->this$0:Lcom/google/android/music/ui/NowPlayingTabletHeaderView;

    # invokes: Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->updateStreaming(ZZZ)V
    invoke-static {v3, v0, v2, v1}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->access$000(Lcom/google/android/music/ui/NowPlayingTabletHeaderView;ZZZ)V

    .line 69
    return-void
.end method
