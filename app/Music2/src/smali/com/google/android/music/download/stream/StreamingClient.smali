.class public Lcom/google/android/music/download/stream/StreamingClient;
.super Ljava/lang/Object;
.source "StreamingClient.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mAllStreamingContents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/stream/StreamingContent;",
            ">;"
        }
    .end annotation
.end field

.field private final mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

.field private final mContext:Landroid/content/Context;

.field private mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

.field private final mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

.field private mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

.field private final mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

.field private final mRequestsLock:Ljava/lang/Object;

.field private volatile mStreamingHttpServer:Lcom/google/android/music/download/stream/StreamingHttpServer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/IDownloadQueueManager;Lcom/google/android/music/download/cache/ICacheManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadQueueManager"    # Lcom/google/android/music/download/IDownloadQueueManager;
    .param p3, "cacheManager"    # Lcom/google/android/music/download/cache/ICacheManager;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    .line 50
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    .line 51
    new-instance v0, Lcom/google/android/music/download/stream/PrefetchedContentCache;

    invoke-direct {v0}, Lcom/google/android/music/download/stream/PrefetchedContentCache;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    .line 65
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamingClient;->mContext:Landroid/content/Context;

    .line 66
    if-nez p2, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IDownloadQueueManager is null "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    iput-object p2, p0, Lcom/google/android/music/download/stream/StreamingClient;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    .line 71
    if-nez p3, :cond_1

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ICacheManager is null "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    iput-object p3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/stream/StreamingClient;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/stream/StreamingClient;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/stream/StreamingClient;Lcom/google/android/music/download/stream/StreamingContent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/stream/StreamingClient;
    .param p1, "x1"    # Lcom/google/android/music/download/stream/StreamingContent;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/music/download/stream/StreamingClient;->handleCancelNextStreamIfMatching(Lcom/google/android/music/download/stream/StreamingContent;)V

    return-void
.end method

.method private clearContents(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/stream/StreamingContent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 515
    .local p1, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 516
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    sget-boolean v2, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 517
    const-string v2, "StreamingClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearContents: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingContent;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    invoke-virtual {v0, v2}, Lcom/google/android/music/download/stream/StreamingContent;->clearFileIfNotSavable(Lcom/google/android/music/download/cache/ICacheManager;)V

    goto :goto_0

    .line 521
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_1
    return-void
.end method

.method private createDownloadRequest(Lcom/google/android/music/download/ContentIdentifier;IJZLcom/google/android/music/medialist/SongList;[Ljava/lang/String;)Lcom/google/android/music/download/TrackDownloadRequest;
    .locals 27
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "priority"    # I
    .param p3, "seekMs"    # J
    .param p5, "fromUserAction"    # Z
    .param p6, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p7, "cursorColumns"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/stream/DownloadRequestException;,
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 433
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mContext:Landroid/content/Context;

    move-object/from16 v0, p6

    move-object/from16 v1, p1

    move-object/from16 v2, p7

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/android/music/medialist/SongList;->getSongCursor(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 434
    .local v4, "cursor":Landroid/database/Cursor;
    if-nez v4, :cond_0

    .line 435
    const-string v5, "StreamingClient"

    const-string v6, "createDownloadRequest: song cursor is null"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    new-instance v5, Lcom/google/android/music/download/stream/DownloadRequestException;

    const-string v6, "null song cursor"

    invoke-direct {v5, v6}, Lcom/google/android/music/download/stream/DownloadRequestException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 439
    :cond_0
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_1

    .line 440
    const-string v5, "StreamingClient"

    const-string v6, "createDownloadRequest: failed to move cursor to first"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    new-instance v5, Lcom/google/android/music/download/stream/DownloadRequestException;

    const-string v6, "empty song cursor"

    invoke-direct {v5, v6}, Lcom/google/android/music/download/stream/DownloadRequestException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    :catchall_0
    move-exception v5

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v5

    .line 444
    :cond_1
    :try_start_1
    const-string v5, "SourceId"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 446
    .local v14, "sourceId":Ljava/lang/String;
    const-string v5, "domainParam"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 448
    .local v22, "domainParam":Ljava/lang/String;
    const-string v5, "SourceAccount"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 450
    .local v15, "sourceAccount":I
    const-string v5, "Size"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 452
    .local v8, "size":J
    const-string v5, "title"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 455
    .local v13, "title":Ljava/lang/String;
    const/16 v21, 0x0

    .line 457
    .local v21, "fileLocation":Lcom/google/android/music/download/cache/FileLocation;
    const/4 v10, 0x2

    .line 458
    .local v10, "cacheType":I
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v5

    if-nez v5, :cond_2

    .line 459
    const/4 v10, 0x1

    .line 462
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    sget-object v6, Lcom/google/android/music/download/TrackOwner;->MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v6}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v7

    move-object/from16 v6, p1

    invoke-interface/range {v5 .. v10}, Lcom/google/android/music/download/cache/ICacheManager;->getTempFileLocation(Lcom/google/android/music/download/ContentIdentifier;IJI)Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v21

    .line 465
    if-nez v21, :cond_3

    .line 466
    const-string v25, " file location is null"

    .line 467
    .local v25, "msg":Ljava/lang/String;
    const-string v5, "StreamingClient"

    const-string v6, "createDownloadRequest:%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v25, v7, v12

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    new-instance v5, Lcom/google/android/music/download/cache/OutOfSpaceException;

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Lcom/google/android/music/download/cache/OutOfSpaceException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 470
    .end local v25    # "msg":Ljava/lang/String;
    :catch_0
    move-exception v24

    .line 471
    .local v24, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v5, "StreamingClient"

    const-string v6, "Failed to get temp file location"

    move-object/from16 v0, v24

    invoke-static {v5, v6, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 472
    new-instance v5, Lcom/google/android/music/download/stream/DownloadRequestException;

    move-object/from16 v0, v24

    invoke-direct {v5, v0}, Lcom/google/android/music/download/stream/DownloadRequestException;-><init>(Ljava/lang/Exception;)V

    throw v5

    .line 475
    .end local v24    # "e":Landroid/os/RemoteException;
    :cond_3
    new-instance v11, Lcom/google/android/music/download/TrackDownloadRequest;

    sget-object v17, Lcom/google/android/music/download/TrackOwner;->MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;

    const/16 v20, 0x1

    move-object/from16 v12, p1

    move/from16 v16, p2

    move-wide/from16 v18, p3

    move/from16 v23, p5

    invoke-direct/range {v11 .. v23}, Lcom/google/android/music/download/TrackDownloadRequest;-><init>(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/music/download/TrackOwner;JZLcom/google/android/music/download/cache/FileLocation;Ljava/lang/String;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 481
    .local v11, "downloadRequest":Lcom/google/android/music/download/TrackDownloadRequest;
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    return-object v11
.end method

.method private handleCancelCurrentStream()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 489
    sget-boolean v0, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v0, :cond_0

    .line 490
    const-string v0, "StreamingClient"

    const-string v1, "handleCancelCurrentStream: current=%s next=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v0, :cond_1

    .line 494
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v0, v5}, Lcom/google/android/music/download/stream/StreamingContent;->setWaitingContentTypeAllowed(Z)V

    .line 495
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 497
    :cond_1
    return-void
.end method

.method private handleCancelNextStreamIfMatching(Lcom/google/android/music/download/stream/StreamingContent;)V
    .locals 6
    .param p1, "nextStream"    # Lcom/google/android/music/download/stream/StreamingContent;

    .prologue
    const/4 v5, 0x0

    .line 504
    sget-boolean v0, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v0, :cond_0

    .line 505
    const-string v0, "StreamingClient"

    const-string v1, "handleCancelNextStreamIfMatching: current=%s next=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-ne v0, p1, :cond_1

    .line 509
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v0, v5}, Lcom/google/android/music/download/stream/StreamingContent;->setWaitingContentTypeAllowed(Z)V

    .line 510
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 512
    :cond_1
    return-void
.end method

.method private prefetchIndexToPriority(I)I
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 524
    packed-switch p1, :pswitch_data_0

    .line 534
    sget v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH4:I

    :goto_0
    return v0

    .line 526
    :pswitch_0
    sget v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH1:I

    goto :goto_0

    .line 528
    :pswitch_1
    sget v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH2:I

    goto :goto_0

    .line 530
    :pswitch_2
    sget v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH3:I

    goto :goto_0

    .line 532
    :pswitch_3
    sget v0, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH4:I

    goto :goto_0

    .line 524
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private declared-synchronized streamTrackSynchronized(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZLcom/google/android/music/medialist/SongList;[Ljava/lang/String;[Lcom/google/android/music/download/ContentIdentifier;)Ljava/lang/String;
    .locals 32
    .param p1, "trackId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "seekMs"    # J
    .param p4, "progressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p5, "isCurrentPlayer"    # Z
    .param p6, "fromUserAction"    # Z
    .param p7, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p8, "cursorColumns"    # [Ljava/lang/String;
    .param p9, "prefetchList"    # [Lcom/google/android/music/download/ContentIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/stream/DownloadRequestException;,
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 268
    monitor-enter p0

    :try_start_0
    sget-boolean v5, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v5, :cond_0

    .line 269
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 270
    :try_start_1
    const-string v5, "StreamingClient"

    const-string v9, "streamTrackSynchronized trackId=%s %s prefetchCache.size=%d current=%s"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    const/4 v11, 0x1

    const-string v12, "all"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v12, v0}, Lcom/google/android/music/download/stream/StreamingContent;->contentListToString(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    invoke-virtual {v12}, Lcom/google/android/music/download/stream/PrefetchedContentCache;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279
    :cond_0
    :try_start_2
    sget v7, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_STREAM:I

    .line 280
    .local v7, "priority":I
    const/16 v25, 0x2

    .line 281
    .local v25, "purgeFlag":I
    if-nez p5, :cond_1

    .line 282
    sget v7, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_PREFETCH1:I

    .line 283
    const/16 v25, 0x3

    .line 286
    :cond_1
    new-instance v27, Ljava/util/LinkedList;

    invoke-direct/range {v27 .. v27}, Ljava/util/LinkedList;-><init>()V

    .local v27, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-wide/from16 v8, p2

    move/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    .line 291
    invoke-direct/range {v5 .. v12}, Lcom/google/android/music/download/stream/StreamingClient;->createDownloadRequest(Lcom/google/android/music/download/ContentIdentifier;IJZLcom/google/android/music/medialist/SongList;[Ljava/lang/String;)Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v29

    .line 294
    .local v29, "streamingRequest":Lcom/google/android/music/download/TrackDownloadRequest;
    const/16 v28, 0x0

    .line 295
    .local v28, "streamingContent":Lcom/google/android/music/download/stream/StreamingContent;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 296
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Lcom/google/android/music/download/stream/PrefetchedContentCache;->findContent(Lcom/google/android/music/download/TrackDownloadRequest;)Lcom/google/android/music/download/stream/StreamingContent;

    move-result-object v28

    .line 299
    if-nez p5, :cond_2

    .line 300
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v5}, Lcom/google/android/music/download/stream/StreamingContent;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Lcom/google/android/music/download/TrackDownloadRequest;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v28, v0

    .line 305
    :cond_2
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 306
    if-nez v28, :cond_3

    .line 307
    :try_start_4
    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    new-instance v28, Lcom/google/android/music/download/stream/StreamingContent;

    .end local v28    # "streamingContent":Lcom/google/android/music/download/stream/StreamingContent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mContext:Landroid/content/Context;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-direct {v0, v5, v1}, Lcom/google/android/music/download/stream/StreamingContent;-><init>(Landroid/content/Context;Lcom/google/android/music/download/TrackDownloadRequest;)V

    .line 311
    .restart local v28    # "streamingContent":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_3
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 313
    .local v4, "allStreamingContents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    const/4 v15, 0x0

    .line 314
    .local v15, "current":Lcom/google/android/music/download/stream/StreamingContent;
    const/16 v22, 0x0

    .line 315
    .local v22, "next":Lcom/google/android/music/download/stream/StreamingContent;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 316
    if-eqz p5, :cond_8

    .line 317
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v0, v28

    if-eq v5, v0, :cond_4

    .line 318
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/stream/StreamingClient;->handleCancelCurrentStream()V

    .line 320
    :cond_4
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 321
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Lcom/google/android/music/download/stream/StreamingContent;->setWaitingContentTypeAllowed(Z)V

    .line 329
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v5, :cond_5

    .line 330
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v5, :cond_6

    .line 333
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v22, v0

    .line 337
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 340
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamingClient;->mStreamingHttpServer:Lcom/google/android/music/download/stream/StreamingHttpServer;

    move-object/from16 v17, v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 342
    .local v17, "httpServer":Lcom/google/android/music/download/stream/StreamingHttpServer;
    if-nez v17, :cond_7

    .line 343
    :try_start_7
    new-instance v5, Lcom/google/android/music/download/stream/StreamingHttpServer;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/download/stream/StreamingClient;->mContext:Landroid/content/Context;

    invoke-direct {v5, v8}, Lcom/google/android/music/download/stream/StreamingHttpServer;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mStreamingHttpServer:Lcom/google/android/music/download/stream/StreamingHttpServer;

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamingClient;->mStreamingHttpServer:Lcom/google/android/music/download/stream/StreamingHttpServer;

    move-object/from16 v17, v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 351
    :cond_7
    const/16 v30, 0x0

    .line 352
    .local v30, "streamingUrl":Ljava/lang/String;
    if-eqz v17, :cond_a

    .line 353
    :try_start_8
    move-object/from16 v0, v17

    move-object/from16 v1, v28

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v15, v2}, Lcom/google/android/music/download/stream/StreamingHttpServer;->serveStream(Lcom/google/android/music/download/stream/StreamingContent;Lcom/google/android/music/download/stream/StreamingContent;Lcom/google/android/music/download/stream/StreamingContent;)Ljava/lang/String;

    move-result-object v30

    .line 358
    :goto_1
    new-instance v23, Ljava/util/LinkedList;

    invoke-direct/range {v23 .. v23}, Ljava/util/LinkedList;-><init>()V

    .line 359
    .local v23, "oldContent":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    const/16 v19, 0x0

    .line 361
    .local v19, "index":I
    if-eqz p9, :cond_f

    .line 362
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 363
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    move-object/from16 v0, p9

    invoke-virtual {v5, v0}, Lcom/google/android/music/download/stream/PrefetchedContentCache;->pruneNotMatching([Lcom/google/android/music/download/ContentIdentifier;)Ljava/util/List;

    move-result-object v24

    .line 364
    .local v24, "pruned":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    invoke-interface/range {v23 .. v24}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 365
    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 367
    move-object/from16 v13, p9

    .local v13, "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    :try_start_a
    array-length v0, v13

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    move/from16 v20, v19

    .end local v19    # "index":I
    .local v20, "index":I
    :goto_2
    move/from16 v0, v18

    move/from16 v1, v21

    if-ge v0, v1, :cond_e

    aget-object v6, v13, v18

    .line 369
    .local v6, "id":Lcom/google/android/music/download/ContentIdentifier;
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "index":I
    .restart local v19    # "index":I
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/music/download/stream/StreamingClient;->prefetchIndexToPriority(I)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v7

    .line 370
    const/16 v26, 0x0

    .line 372
    .local v26, "request":Lcom/google/android/music/download/TrackDownloadRequest;
    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p0

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    :try_start_b
    invoke-direct/range {v5 .. v12}, Lcom/google/android/music/download/stream/StreamingClient;->createDownloadRequest(Lcom/google/android/music/download/ContentIdentifier;IJZLcom/google/android/music/medialist/SongList;[Ljava/lang/String;)Lcom/google/android/music/download/TrackDownloadRequest;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v26

    .line 380
    const/4 v14, 0x0

    .line 381
    .local v14, "content":Lcom/google/android/music/download/stream/StreamingContent;
    :try_start_c
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 382
    :try_start_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v5, :cond_b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v5, v6}, Lcom/google/android/music/download/stream/StreamingContent;->hasId(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 383
    monitor-exit v8
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 367
    .end local v14    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :goto_3
    add-int/lit8 v18, v18, 0x1

    move/from16 v20, v19

    .end local v19    # "index":I
    .restart local v20    # "index":I
    goto :goto_2

    .line 276
    .end local v4    # "allStreamingContents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    .end local v6    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v7    # "priority":I
    .end local v13    # "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    .end local v15    # "current":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v17    # "httpServer":Lcom/google/android/music/download/stream/StreamingHttpServer;
    .end local v18    # "i$":I
    .end local v20    # "index":I
    .end local v21    # "len$":I
    .end local v22    # "next":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v23    # "oldContent":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    .end local v24    # "pruned":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    .end local v25    # "purgeFlag":I
    .end local v26    # "request":Lcom/google/android/music/download/TrackDownloadRequest;
    .end local v27    # "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    .end local v28    # "streamingContent":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v29    # "streamingRequest":Lcom/google/android/music/download/TrackDownloadRequest;
    .end local v30    # "streamingUrl":Ljava/lang/String;
    :catchall_0
    move-exception v5

    :try_start_e
    monitor-exit v8
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :try_start_f
    throw v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 268
    :catchall_1
    move-exception v5

    monitor-exit p0

    throw v5

    .line 305
    .restart local v7    # "priority":I
    .restart local v25    # "purgeFlag":I
    .restart local v27    # "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    .restart local v28    # "streamingContent":Lcom/google/android/music/download/stream/StreamingContent;
    .restart local v29    # "streamingRequest":Lcom/google/android/music/download/TrackDownloadRequest;
    :catchall_2
    move-exception v5

    :try_start_10
    monitor-exit v8
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :try_start_11
    throw v5
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 323
    .restart local v4    # "allStreamingContents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    .restart local v15    # "current":Lcom/google/android/music/download/stream/StreamingContent;
    .restart local v22    # "next":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_8
    :try_start_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v0, v28

    if-eq v5, v0, :cond_9

    .line 324
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/download/stream/StreamingClient;->handleCancelNextStreamIfMatching(Lcom/google/android/music/download/stream/StreamingContent;)V

    .line 326
    :cond_9
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 327
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Lcom/google/android/music/download/stream/StreamingContent;->setWaitingContentTypeAllowed(Z)V

    goto/16 :goto_0

    .line 337
    :catchall_3
    move-exception v5

    monitor-exit v8
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :try_start_13
    throw v5

    .line 346
    .restart local v17    # "httpServer":Lcom/google/android/music/download/stream/StreamingHttpServer;
    :catch_0
    move-exception v16

    .line 347
    .local v16, "e":Ljava/io/IOException;
    const-string v5, "StreamingClient"

    const-string v8, "Failed to create streaming http server"

    move-object/from16 v0, v16

    invoke-static {v5, v8, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 348
    const/16 v30, 0x0

    .line 421
    .end local v16    # "e":Ljava/io/IOException;
    :goto_4
    monitor-exit p0

    return-object v30

    .line 355
    .restart local v30    # "streamingUrl":Ljava/lang/String;
    :cond_a
    :try_start_14
    const-string v5, "StreamingClient"

    const-string v8, "Failed to request to serve stream"

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto/16 :goto_1

    .line 365
    .restart local v19    # "index":I
    .restart local v23    # "oldContent":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    :catchall_4
    move-exception v5

    :try_start_15
    monitor-exit v8
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    :try_start_16
    throw v5

    .line 373
    .restart local v6    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v13    # "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    .restart local v18    # "i$":I
    .restart local v21    # "len$":I
    .restart local v24    # "pruned":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    .restart local v26    # "request":Lcom/google/android/music/download/TrackDownloadRequest;
    :catch_1
    move-exception v16

    .line 374
    .local v16, "e":Ljava/lang/Exception;
    const-string v5, "StreamingClient"

    const-string v8, "Failed to create prefetch request for id=%s e=%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v10, 0x1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    goto :goto_3

    .line 386
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v14    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_b
    :try_start_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v5, :cond_c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v5, v6}, Lcom/google/android/music/download/stream/StreamingContent;->hasId(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 387
    monitor-exit v8

    goto :goto_3

    .line 390
    :catchall_5
    move-exception v5

    monitor-exit v8
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_5

    :try_start_18
    throw v5
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    .line 389
    :cond_c
    :try_start_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Lcom/google/android/music/download/stream/PrefetchedContentCache;->findContent(Lcom/google/android/music/download/TrackDownloadRequest;)Lcom/google/android/music/download/stream/StreamingContent;

    move-result-object v14

    .line 390
    monitor-exit v8
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_5

    .line 392
    if-nez v14, :cond_d

    .line 393
    :try_start_1a
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    new-instance v14, Lcom/google/android/music/download/stream/StreamingContent;

    .end local v14    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mContext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v14, v5, v0}, Lcom/google/android/music/download/stream/StreamingContent;-><init>(Landroid/content/Context;Lcom/google/android/music/download/TrackDownloadRequest;)V

    .line 396
    .restart local v14    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_d
    invoke-interface {v4, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .end local v6    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v14    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v19    # "index":I
    .end local v26    # "request":Lcom/google/android/music/download/TrackDownloadRequest;
    .restart local v20    # "index":I
    :cond_e
    move/from16 v19, v20

    .line 400
    .end local v13    # "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    .end local v18    # "i$":I
    .end local v20    # "index":I
    .end local v21    # "len$":I
    .end local v24    # "pruned":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    .restart local v19    # "index":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    .line 401
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 402
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 403
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 404
    sget-boolean v5, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v5, :cond_10

    .line 405
    const-string v5, "StreamingClient"

    const-string v9, "streamTrackSynchronized END: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "all"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v12, v0}, Lcom/google/android/music/download/stream/StreamingContent;->contentListToString(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_10
    monitor-exit v8
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_6

    .line 410
    :try_start_1c
    sget-boolean v5, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v5, :cond_11

    .line 411
    const-string v5, "StreamingClient"

    const-string v8, "oldContent"

    move-object/from16 v0, v23

    invoke-static {v8, v0}, Lcom/google/android/music/download/stream/StreamingContent;->contentListToString(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/music/download/stream/StreamingClient;->clearContents(Ljava/util/List;)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    .line 416
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/stream/StreamingClient;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    move-object/from16 v0, v27

    move-object/from16 v1, p4

    move/from16 v2, v25

    invoke-interface {v5, v0, v1, v2}, Lcom/google/android/music/download/IDownloadQueueManager;->download(Ljava/util/List;Lcom/google/android/music/download/IDownloadProgressListener;I)V
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_1d} :catch_2
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    goto/16 :goto_4

    .line 417
    :catch_2
    move-exception v16

    .line 418
    .local v16, "e":Landroid/os/RemoteException;
    :try_start_1e
    const-string v5, "StreamingClient"

    const-string v8, "Failed to call the download queue manager"

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    new-instance v5, Lcom/google/android/music/download/stream/DownloadRequestException;

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Lcom/google/android/music/download/stream/DownloadRequestException;-><init>(Ljava/lang/Exception;)V

    throw v5
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    .line 408
    .end local v16    # "e":Landroid/os/RemoteException;
    :catchall_6
    move-exception v5

    :try_start_1f
    monitor-exit v8
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_6

    :try_start_20
    throw v5
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_1
.end method


# virtual methods
.method public cancelAndPurgeAllStreamingTracks()V
    .locals 4

    .prologue
    .line 245
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingClient;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;

    sget-object v2, Lcom/google/android/music/download/TrackOwner;->MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/google/android/music/download/IDownloadQueueManager;->cancelAndPurge(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_0
    return-void

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "StreamingClient"

    const-string v2, "Failed to call the download queue manager"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cancelNextStream()V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 230
    .local v0, "streamingContent":Lcom/google/android/music/download/stream/StreamingContent;
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/download/stream/StreamingClient$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/download/stream/StreamingClient$1;-><init>(Lcom/google/android/music/download/stream/StreamingClient;Lcom/google/android/music/download/stream/StreamingContent;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 238
    return-void
.end method

.method public clearPrefetchedCache()V
    .locals 4

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "oldContent":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 134
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/music/download/stream/PrefetchedContentCache;->pruneNotMatching([Lcom/google/android/music/download/ContentIdentifier;)Ljava/util/List;

    move-result-object v0

    .line 135
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    invoke-direct {p0, v0}, Lcom/google/android/music/download/stream/StreamingClient;->clearContents(Ljava/util/List;)V

    .line 137
    return-void

    .line 135
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public currenStreamingPlayEnded()V
    .locals 8

    .prologue
    .line 143
    sget-boolean v3, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v3, :cond_0

    .line 144
    const-string v3, "StreamingClient"

    const-string v4, "currenStreamingPlayEnded: current=%s next=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v4

    .line 149
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v3, :cond_3

    .line 150
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 151
    .local v0, "currentContent":Lcom/google/android/music/download/stream/StreamingContent;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 152
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 153
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/stream/StreamingContent;>;"
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 154
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/stream/StreamingContent;

    .line 155
    .local v2, "request":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v2}, Lcom/google/android/music/download/stream/StreamingContent;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/music/download/stream/StreamingContent;->hasRequest(Lcom/google/android/music/download/TrackDownloadRequest;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 163
    .end local v0    # "currentContent":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/stream/StreamingContent;>;"
    .end local v2    # "request":Lcom/google/android/music/download/stream/StreamingContent;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 159
    .restart local v0    # "currentContent":Lcom/google/android/music/download/stream/StreamingContent;
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/stream/StreamingContent;>;"
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    invoke-virtual {v0, v3}, Lcom/google/android/music/download/stream/StreamingContent;->clearFileIfNotSavable(Lcom/google/android/music/download/cache/ICacheManager;)V

    .line 161
    .end local v0    # "currentContent":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/stream/StreamingContent;>;"
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    iput-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 162
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 163
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mStreamingHttpServer:Lcom/google/android/music/download/stream/StreamingHttpServer;

    .line 102
    .local v0, "httpServer":Lcom/google/android/music/download/stream/StreamingHttpServer;
    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingHttpServer;->shutdown()V

    .line 105
    :cond_0
    return-void
.end method

.method public getFilteredIds()[Lcom/google/android/music/download/ContentIdentifier;
    .locals 5

    .prologue
    .line 170
    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v4

    .line 171
    :try_start_0
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 172
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v3, :cond_0

    .line 173
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v3}, Lcom/google/android/music/download/stream/StreamingContent;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v3, :cond_1

    .line 176
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mNextStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v3}, Lcom/google/android/music/download/stream/StreamingContent;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 179
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingContent;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 185
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 181
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    :cond_2
    :try_start_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 182
    const/4 v3, 0x0

    monitor-exit v4

    .line 184
    :goto_1
    return-object v3

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/google/android/music/download/ContentIdentifier;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/music/download/ContentIdentifier;

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public handleDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 4
    .param p1, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 193
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 194
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 195
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v0, p1}, Lcom/google/android/music/download/stream/StreamingContent;->isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/music/download/stream/StreamingContent;->notifyDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;Lcom/google/android/music/download/cache/ICacheManager;)V

    .line 197
    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadProgress;->isFullCopy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    invoke-virtual {v2, v0}, Lcom/google/android/music/download/stream/PrefetchedContentCache;->store(Lcom/google/android/music/download/stream/StreamingContent;)V

    goto :goto_0

    .line 211
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    return-void
.end method

.method public isCurrentStreamingFullyBuffered()Z
    .locals 2

    .prologue
    .line 219
    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingClient;->mCurrentStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingContent;->isCompleted()Z

    move-result v0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public shouldFilter(Ljava/lang/String;)Z
    .locals 10
    .param p1, "fullFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 113
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingClient;->mRequestsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 114
    :try_start_0
    sget-boolean v4, Lcom/google/android/music/download/stream/StreamingClient;->LOGV:Z

    if-eqz v4, :cond_0

    .line 115
    const-string v4, "StreamingClient"

    const-string v5, "shouldFilter %s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    const-string v8, "all"

    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    invoke-static {v8, v9}, Lcom/google/android/music/download/stream/StreamingContent;->contentListToString(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingClient;->mAllStreamingContents:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/stream/StreamingContent;

    .line 120
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v0, p1}, Lcom/google/android/music/download/stream/StreamingContent;->shouldFilter(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 121
    monitor-exit v3

    .line 124
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :goto_0
    return v2

    :cond_2
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingClient;->mPrefetchedContentCache:Lcom/google/android/music/download/stream/PrefetchedContentCache;

    invoke-virtual {v2, p1}, Lcom/google/android/music/download/stream/PrefetchedContentCache;->shouldFilter(Ljava/lang/String;)Z

    move-result v2

    monitor-exit v3

    goto :goto_0

    .line 125
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public streamTrack(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZLcom/google/android/music/medialist/SongList;[Ljava/lang/String;[Lcom/google/android/music/download/ContentIdentifier;)Ljava/lang/String;
    .locals 2
    .param p1, "trackId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "seekMs"    # J
    .param p4, "progressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p5, "isCurrentPlayer"    # Z
    .param p6, "fromUserAction"    # Z
    .param p7, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p8, "cursorColumns"    # [Ljava/lang/String;
    .param p9, "prefetchList"    # [Lcom/google/android/music/download/ContentIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/stream/DownloadRequestException;,
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct/range {p0 .. p9}, Lcom/google/android/music/download/stream/StreamingClient;->streamTrackSynchronized(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZLcom/google/android/music/medialist/SongList;[Ljava/lang/String;[Lcom/google/android/music/download/ContentIdentifier;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
