.class public Lcom/google/android/music/leanback/LeanbackSearchActivity;
.super Lcom/google/android/music/leanback/LeanbackItemActivity;
.source "LeanbackSearchActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;,
        Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field private mArtistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field private mBestMatchAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field private mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

.field private mLastHandledQuery:Ljava/lang/String;

.field private mListRows:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

.field private mNoResultsView:Landroid/view/View;

.field private mPendingAdapters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/CursorObjectAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingSearchQuery:Ljava/lang/String;

.field private mPlaylistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field private mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

.field private mTrackAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->LOGV:Z

    .line 63
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;-><init>()V

    .line 185
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Lcom/google/android/music/leanback/ItemPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mListRows:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForArtist(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForTrack(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForPlaylist(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->showNoResults()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->hideNoResults()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/music/leanback/LeanbackSearchActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Lcom/google/android/music/leanback/LeanbackSearchFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startSpeechRecognizerActivity()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForBestMatch(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackSearchActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForAlbum(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method private extractDataForAlbum(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v10, 0x7

    .line 632
    const/16 v9, 0x11

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 634
    .local v6, "itemType":I
    const/16 v9, 0x8

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 638
    .local v4, "artworkUrl":Ljava/lang/String;
    const/4 v8, 0x0

    .line 639
    .local v8, "trackCount":I
    if-ne v6, v10, :cond_1

    .line 640
    const/16 v9, 0xa

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 641
    .local v1, "albumId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 642
    const/4 v4, 0x0

    .line 653
    :cond_0
    :goto_0
    const/4 v9, 0x2

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 654
    .local v7, "name":Ljava/lang/String;
    const/4 v9, 0x4

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 655
    .local v0, "albumArtist":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 657
    .local v5, "intent":Landroid/content/Intent;
    new-instance v9, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v9}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v9, v7}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v9

    return-object v9

    .line 645
    .end local v0    # "albumArtist":Ljava/lang/String;
    .end local v1    # "albumId":Ljava/lang/String;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v7    # "name":Ljava/lang/String;
    :cond_1
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 646
    .local v2, "albumIdLong":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 647
    .restart local v1    # "albumId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 648
    const/4 v9, 0x1

    invoke-static {p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v10

    invoke-static {p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v11

    invoke-static {v2, v3, v9, v10, v11}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private extractDataForArtist(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 662
    const/16 v9, 0x11

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 663
    .local v6, "itemType":I
    const/16 v9, 0x8

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 669
    .local v3, "artworkUrl":Ljava/lang/String;
    const/4 v0, 0x0

    .line 670
    .local v0, "albumCount":I
    const/4 v8, 0x0

    .line 671
    .local v8, "trackCount":I
    const/4 v9, 0x6

    if-ne v6, v9, :cond_1

    .line 672
    const/16 v9, 0xb

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 673
    .local v2, "artistId":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 674
    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistArtUrlUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 683
    :cond_0
    :goto_0
    const/4 v9, 0x2

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 685
    .local v7, "name":Ljava/lang/String;
    const/4 v1, 0x0

    .line 690
    .local v1, "albumCountText":Ljava/lang/String;
    new-instance v9, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v9}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v9, v7}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-static {p0, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->getArtistDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v9

    return-object v9

    .line 677
    .end local v1    # "albumCountText":Ljava/lang/String;
    .end local v2    # "artistId":Ljava/lang/String;
    .end local v7    # "name":Ljava/lang/String;
    :cond_1
    const/4 v9, 0x5

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 678
    .local v4, "artistIdLong":J
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 679
    .restart local v2    # "artistId":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 680
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private extractDataForBestMatch(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 612
    const/16 v1, 0x11

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 613
    .local v0, "itemType":I
    packed-switch v0, :pswitch_data_0

    .line 626
    :pswitch_0
    sget-object v1, Lcom/google/android/music/leanback/LeanbackSearchActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported best match type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 616
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForAlbum(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v1

    goto :goto_0

    .line 620
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForArtist(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v1

    goto :goto_0

    .line 623
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForTrack(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v1

    goto :goto_0

    .line 613
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private extractDataForPlaylist(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 724
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 725
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {p0, v0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->getPlaylistDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 727
    const/4 v1, 0x0

    .line 733
    .local v1, "songCountDesc":Ljava/lang/String;
    new-instance v2, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v2}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    const v3, 0x7f0200ce

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v2

    return-object v2
.end method

.method private extractDataForTrack(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 14
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/16 v13, 0x8

    const/4 v11, 0x0

    .line 695
    const/16 v12, 0x11

    invoke-interface {p1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 697
    .local v7, "itemType":I
    invoke-interface {p1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 698
    .local v3, "artworkUrl":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 699
    const/4 v3, 0x0

    .line 702
    :cond_0
    const/4 v12, 0x2

    invoke-interface {p1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 703
    .local v8, "name":Ljava/lang/String;
    const/4 v12, 0x3

    invoke-interface {p1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 704
    .local v2, "albumName":Ljava/lang/String;
    const/4 v12, 0x4

    invoke-interface {p1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 705
    .local v0, "albumArtist":Ljava/lang/String;
    const/4 v12, 0x6

    invoke-interface {p1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 707
    .local v10, "trackArtist":Ljava/lang/String;
    if-ne v7, v13, :cond_1

    const/4 v6, 0x1

    .line 708
    .local v6, "isNautilus":Z
    :goto_0
    if-eqz v6, :cond_2

    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    .line 711
    .local v4, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :goto_1
    if-eqz v6, :cond_3

    new-instance v9, Lcom/google/android/music/medialist/NautilusSingleSongList;

    const/16 v11, 0x9

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v4, v11, v8}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    .local v9, "songList":Lcom/google/android/music/medialist/SongList;
    :goto_2
    invoke-static {p0, v9}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v5

    .line 716
    .local v5, "intent":Landroid/content/Intent;
    if-eqz v6, :cond_4

    const/16 v11, 0xa

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 719
    .local v1, "albumId":Ljava/lang/String;
    :goto_3
    new-instance v11, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v11}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v11, v8}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v2}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v3}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v5}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11, v1}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v11

    return-object v11

    .end local v1    # "albumId":Ljava/lang/String;
    .end local v4    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "isNautilus":Z
    .end local v9    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    move v6, v11

    .line 707
    goto :goto_0

    .line 708
    .restart local v6    # "isNautilus":Z
    :cond_2
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13, v8}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    goto :goto_1

    .line 711
    .restart local v4    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_3
    new-instance v9, Lcom/google/android/music/medialist/SingleSongList;

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-direct {v9, v4, v12, v13, v8}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    goto :goto_2

    .line 716
    .restart local v5    # "intent":Landroid/content/Intent;
    .restart local v9    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_4
    const/4 v11, 0x7

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method private getNoResultsView()Landroid/view/View;
    .locals 4

    .prologue
    .line 384
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mNoResultsView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 385
    const v1, 0x7f0e0164

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 386
    .local v0, "frame":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04005c

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 387
    const v1, 0x7f0e0165

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mNoResultsView:Landroid/view/View;

    .line 388
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mNoResultsView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 390
    .end local v0    # "frame":Landroid/view/ViewGroup;
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mNoResultsView:Landroid/view/View;

    return-object v1
.end method

.method private handleQuery(Ljava/lang/String;)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mLastHandledQuery:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    :goto_0
    return-void

    .line 407
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mLastHandledQuery:Ljava/lang/String;

    .line 408
    sget-boolean v0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->LOGV:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleQuery "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mListRows:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->clear()V

    .line 410
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mAlbumAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    const-string v2, "album"

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->restartLoading(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mArtistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    const-string v2, "artist"

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->restartLoading(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mTrackAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    const-string v2, "track"

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->restartLoading(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPlaylistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    const-string v2, "playlist"

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->restartLoading(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mBestMatchAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    const-string v2, "bestmatch"

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->restartLoading(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->hideNoResults()V

    .line 422
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 423
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mAlbumAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mArtistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mTrackAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPlaylistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mBestMatchAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private handleSearchResult(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v13, 0x0

    .line 431
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v11

    .line 432
    .local v11, "uri":Landroid/net/Uri;
    if-nez v11, :cond_1

    .line 434
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->launchLeanbackInterfaceIfNeeded(Landroid/app/Activity;)Z

    .line 435
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->finish()V

    .line 602
    :cond_0
    :goto_0
    return-void

    .line 439
    :cond_1
    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 440
    .local v6, "path":Ljava/lang/String;
    const-string v12, "autoPlay"

    invoke-virtual {p1, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 446
    .local v7, "shouldAutoPlay":Z
    :try_start_0
    invoke-virtual {v11}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 452
    .local v4, "id":J
    sget-object v12, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_TRACK:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 453
    const-string v12, "query"

    invoke-virtual {p1, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v4, v5, v12}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    .line 456
    .local v2, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v12, Lcom/google/android/music/medialist/SingleSongList;

    const-string v13, ""

    invoke-direct {v12, v2, v4, v5, v13}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    const/4 v13, -0x1

    invoke-static {p0, v12, v13}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)V

    .line 461
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->launchLeanbackInterfaceIfNeeded(Landroid/app/Activity;)Z

    .line 462
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->finish()V

    goto :goto_0

    .line 447
    .end local v2    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v4    # "id":J
    :catch_0
    move-exception v3

    .line 448
    .local v3, "nfe":Ljava/lang/NumberFormatException;
    sget-object v12, Lcom/google/android/music/leanback/LeanbackSearchActivity;->TAG:Ljava/lang/String;

    const-string v13, "Invalid id"

    invoke-static {v12, v13, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 463
    .end local v3    # "nfe":Ljava/lang/NumberFormatException;
    .restart local v4    # "id":J
    :cond_2
    sget-object v12, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM_ARTIST:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 464
    move-wide v0, v4

    .line 467
    .local v0, "artistId":J
    new-instance v12, Lcom/google/android/music/leanback/LeanbackSearchActivity$8;

    invoke-direct {v12, p0, v0, v1, v7}, Lcom/google/android/music/leanback/LeanbackSearchActivity$8;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;JZ)V

    invoke-static {v12}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0

    .line 505
    .end local v0    # "artistId":J
    :cond_3
    sget-object v12, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 506
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-static {p0, v12}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 509
    if-eqz v7, :cond_4

    .line 510
    new-instance v10, Lcom/google/android/music/medialist/AlbumSongList;

    invoke-direct {v10, v4, v5, v13}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .line 511
    .local v10, "songlist":Lcom/google/android/music/medialist/SongList;
    invoke-static {p0, v10}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 513
    .end local v10    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->finish()V

    goto :goto_0

    .line 514
    :cond_5
    sget-object v12, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ARTIST:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 515
    move-wide v0, v4

    .line 518
    .restart local v0    # "artistId":J
    new-instance v12, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;

    invoke-direct {v12, p0, v0, v1, v7}, Lcom/google/android/music/leanback/LeanbackSearchActivity$9;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;JZ)V

    invoke-static {v12}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto/16 :goto_0

    .line 554
    .end local v0    # "artistId":J
    :cond_6
    sget-object v12, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_PLAYLIST:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 555
    move-wide v8, v4

    .line 558
    .local v8, "playlistId":J
    new-instance v12, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;

    invoke-direct {v12, p0, v8, v9, v7}, Lcom/google/android/music/leanback/LeanbackSearchActivity$10;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;JZ)V

    invoke-static {v12}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto/16 :goto_0
.end method

.method private hideNoResults()V
    .locals 2

    .prologue
    .line 399
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getNoResultsView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 400
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getNoResultsView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 401
    return-void
.end method

.method private restartLoading(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "loaderId"    # I
    .param p2, "cursorObjectAdapter"    # Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;

    .prologue
    .line 606
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/support/v17/leanback/widget/CursorObjectAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 607
    invoke-static {p3, p4}, Lcom/google/android/music/store/MusicContent$Search;->getSearchUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->CURSOR_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 609
    return-void
.end method

.method private setupFromIntent()V
    .locals 3

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 228
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "action":Ljava/lang/String;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;

    .line 230
    const-string v2, "android.intent.action.SEARCH"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    const-string v2, "query"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    const-string v2, "android.intent.action.SEARCH_RESULT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    invoke-direct {p0, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->handleSearchResult(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private showNoResults()V
    .locals 2

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getNoResultsView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 395
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getNoResultsView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 396
    return-void
.end method

.method private startSpeechRecognizerActivity()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->getRecognizerIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 224
    return-void
.end method


# virtual methods
.method protected createFragment()Landroid/app/Fragment;
    .locals 3

    .prologue
    .line 194
    new-instance v2, Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-direct {v2}, Lcom/google/android/music/leanback/LeanbackSearchFragment;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    .line 195
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-virtual {p0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->setupFragment(Landroid/app/Fragment;)V

    .line 197
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 198
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_0

    const-string v0, ""

    .line 199
    .local v0, "action":Ljava/lang/String;
    :goto_0
    const-string v2, "android.intent.action.SEARCH_RESULT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 201
    invoke-direct {p0, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->handleSearchResult(Landroid/content/Intent;)V

    .line 202
    new-instance v2, Landroid/app/Fragment;

    invoke-direct {v2}, Landroid/app/Fragment;-><init>()V

    .line 204
    :goto_1
    return-object v2

    .line 198
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 204
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    goto :goto_1
.end method

.method public getResultsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mListRows:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 368
    sget-boolean v0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->LOGV:Z

    if-eqz v0, :cond_0

    .line 369
    sget-object v0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_0
    if-ne v3, p1, :cond_1

    .line 375
    packed-switch p2, :pswitch_data_0

    .line 381
    :cond_1
    :goto_0
    return-void

    .line 377
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-virtual {v0, p3, v3}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setSearchQuery(Landroid/content/Intent;Z)V

    goto :goto_0

    .line 375
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 260
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 263
    .local v9, "intent":Landroid/content/Intent;
    const-string v0, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    const-string v0, "query"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 266
    .local v10, "query":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-static {p0}, Lcom/google/android/music/mix/MixDescriptor;->getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->finish()V

    .line 294
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->onCreate(Landroid/os/Bundle;)V

    .line 341
    .end local v10    # "query":Ljava/lang/String;
    :goto_1
    return-void

    .line 270
    .restart local v10    # "query":Ljava/lang/String;
    :cond_0
    const-string v0, "bestmatch"

    invoke-static {v10, v0}, Lcom/google/android/music/store/MusicContent$Search;->getSearchUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 272
    .local v1, "uri":Landroid/net/Uri;
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->CURSOR_COLUMNS:[Ljava/lang/String;

    new-instance v8, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;

    invoke-direct {v8, p0, v9}, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/content/Intent;)V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/music/utils/MusicUtils$QueryCallback;)V

    goto :goto_0

    .line 298
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v10    # "query":Ljava/lang/String;
    :cond_1
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    new-instance v2, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v2}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-direct {v0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mListRows:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    .line 299
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;

    .line 300
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSearchActivity$3;

    const v2, 0x7f0b009f

    invoke-direct {v0, p0, v6, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$3;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;II)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mAlbumAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    .line 308
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSearchActivity$4;

    const v2, 0x7f0b009e

    invoke-direct {v0, p0, v7, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$4;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;II)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mArtistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    .line 316
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSearchActivity$5;

    const/4 v2, 0x2

    const v3, 0x7f0b00a1

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackSearchActivity$5;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;II)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mTrackAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    .line 324
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSearchActivity$6;

    const/4 v2, 0x3

    const v3, 0x7f0b00a2

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackSearchActivity$6;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;II)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPlaylistAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    .line 332
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSearchActivity$7;

    const/4 v2, 0x4

    const v3, 0x7f0b00a3

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackSearchActivity$7;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;II)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mBestMatchAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    .line 340
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->onCreate(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 210
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->setIntent(Landroid/content/Intent;)V

    .line 211
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->setupFromIntent()V

    .line 212
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setSearchQuery(Ljava/lang/String;Z)V

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->startRecognition()V

    goto :goto_0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 350
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->handleQuery(Ljava/lang/String;)V

    .line 351
    const/4 v0, 0x1

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 356
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->handleQuery(Ljava/lang/String;)V

    .line 357
    const/4 v0, 0x1

    return v0
.end method

.method protected setupFragment(Landroid/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 239
    check-cast p1, Lcom/google/android/music/leanback/LeanbackSearchFragment;

    .end local p1    # "fragment":Landroid/app/Fragment;
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    .line 240
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setSearchResultProvider(Landroid/support/v17/leanback/app/SearchFragment$SearchResultProvider;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 244
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->setupFromIntent()V

    .line 245
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setSpeechRecognitionCallback(Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;)V

    .line 256
    return-void
.end method

.method protected showUi()Z
    .locals 2

    .prologue
    .line 362
    const-string v0, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
