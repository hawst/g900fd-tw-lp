.class Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistTopSongsCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mContext:Landroid/content/Context;

    .line 36
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 38
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->addRowsForArtist(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method private addRowsForArtist(Ljava/lang/String;)V
    .locals 18
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 42
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v13}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v13

    new-array v12, v13, [Ljava/lang/Object;

    .line 44
    .local v12, "values":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/google/android/music/xdi/XdiUtils;->getTopSongsByArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 45
    .local v9, "songDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 46
    const-string v13, "MusicXdi"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "No top songs found for artist "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    return-void

    .line 49
    :cond_1
    invoke-static {v9}, Lcom/google/android/music/xdi/XdiUtils;->extractTrackMetajamIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .line 50
    .local v11, "songIdsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    new-array v10, v13, [Ljava/lang/String;

    .line 51
    .local v10, "songIds":[Ljava/lang/String;
    invoke-interface {v11, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    .line 55
    .local v3, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v13, Lcom/google/android/music/medialist/NautilusSelectedSongList;

    invoke-direct {v13, v3, v10}, Lcom/google/android/music/medialist/NautilusSelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/music/medialist/NautilusSelectedSongList;->freeze()Ljava/lang/String;

    move-result-object v8

    .line 56
    .local v8, "selectedSongList":Ljava/lang/String;
    const/4 v6, 0x0

    .line 59
    .local v6, "index":I
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 60
    .local v4, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "artworkUrl":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 62
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 65
    :cond_2
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "container"

    const/16 v15, 0xc

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v13

    const-string v14, "song_list"

    invoke-virtual {v13, v14, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    const-string v14, "offset"

    invoke-virtual {v13, v14, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    .line 71
    .local v7, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_name"

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_subname"

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_description"

    const/4 v15, 0x0

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 76
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "display_number"

    const/4 v15, 0x0

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 77
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "image_uri"

    invoke-virtual {v13, v12, v14, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "item_display_type"

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 80
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "user_rating_count"

    const/4 v15, 0x0

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 81
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "user_rating"

    const/4 v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 82
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "action_uri"

    const/4 v15, 0x1

    invoke-virtual {v7, v15}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_duration"

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getDuration()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_trackArtist"

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v14, "music_album"

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v12, v14, v15}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;->addRow([Ljava/lang/Object;)V

    .line 90
    add-int/lit8 v6, v6, 0x1

    .line 91
    goto/16 :goto_0
.end method
