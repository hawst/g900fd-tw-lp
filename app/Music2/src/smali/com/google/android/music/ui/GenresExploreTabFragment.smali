.class public Lcom/google/android/music/ui/GenresExploreTabFragment;
.super Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.source "GenresExploreTabFragment.java"


# instance fields
.field private mGenreId:Ljava/lang/String;

.field private mGenreName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super/range {p0 .. p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreTabFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v12

    .line 35
    .local v12, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v12}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v5, 0x3f000000    # 0.5f

    .line 37
    .local v5, "genreWidth":F
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreTabFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 38
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v0, "subgenreCount"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 39
    .local v13, "subgenreCount":I
    const-string v0, "nautilusId"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GenresExploreTabFragment;->mGenreId:Ljava/lang/String;

    .line 40
    const-string v0, "name"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GenresExploreTabFragment;->mGenreName:Ljava/lang/String;

    .line 42
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v14, "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    if-lez v13, :cond_0

    .line 44
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "genres"

    const v2, 0x7f0b00a8

    const-class v3, Lcom/google/android/music/ui/GenresExploreFragment;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;FZ)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_0
    new-instance v6, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v7, "top_charts"

    const v8, 0x7f0b00a6

    const-class v9, Lcom/google/android/music/ui/TopChartsExploreFragment;

    const/4 v11, 0x1

    move-object v10, v4

    invoke-direct/range {v6 .. v11}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v6, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v7, "new_releases"

    const v8, 0x7f0b00a5

    const-class v9, Lcom/google/android/music/ui/NewReleasesExploreFragment;

    const/4 v11, 0x1

    move-object v10, v4

    invoke-direct/range {v6 .. v11}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    const-string v0, "top_charts"

    invoke-virtual {p0, v14, v0}, Lcom/google/android/music/ui/GenresExploreTabFragment;->init(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenresExploreTabFragment;->setHasOptionsMenu(Z)V

    .line 53
    return-void

    .line 35
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "genreWidth":F
    .end local v13    # "subgenreCount":I
    .end local v14    # "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    :cond_1
    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 57
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 58
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->showStartRadioButtonsInActionBar()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreTabFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    const v1, 0x7f140001

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 62
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 66
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 91
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    :goto_0
    return v3

    .line 68
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenresExploreTabFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 69
    .local v0, "context":Landroid/content/Context;
    iget-object v2, p0, Lcom/google/android/music/ui/GenresExploreTabFragment;->mGenreName:Ljava/lang/String;

    .line 70
    .local v2, "genreName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/GenresExploreTabFragment;->mGenreId:Ljava/lang/String;

    .line 71
    .local v1, "genreId":Ljava/lang/String;
    new-instance v3, Lcom/google/android/music/ui/GenresExploreTabFragment$1;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/google/android/music/ui/GenresExploreTabFragment$1;-><init>(Lcom/google/android/music/ui/GenresExploreTabFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 89
    const/4 v3, 0x1

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x7f0e02a1
        :pswitch_0
    .end packed-switch
.end method
