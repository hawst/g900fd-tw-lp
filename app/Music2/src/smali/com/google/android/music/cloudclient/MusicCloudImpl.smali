.class public Lcom/google/android/music/cloudclient/MusicCloudImpl;
.super Ljava/lang/Object;
.source "MusicCloudImpl.java"

# interfaces
.implements Lcom/google/android/music/cloudclient/MusicCloud;


# static fields
.field private static final DEFAULT_SEARCH_CONTENT_TYPE:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 71
    const-string v0, ","

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->DEFAULT_SEARCH_CONTENT_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "context is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    .line 92
    invoke-static {p1}, Lcom/google/android/music/cloudclient/MusicRequest;->getSharedHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    .line 93
    return-void
.end method

.method private executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 189
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 190
    .local v2, "prefsHolder":Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 193
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    new-instance v1, Lcom/google/android/music/cloudclient/MusicRequest;

    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3, v0}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 194
    .local v1, "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/music/cloudclient/MusicRequest;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 196
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v3

    .end local v1    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method

.method private executeGet(Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/music/cloudclient/MusicRequest;)Ljava/lang/Object;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "musicRequest"    # Lcom/google/android/music/cloudclient/MusicRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/music/cloudclient/MusicRequest;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 219
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 220
    .local v0, "httpRequest":Lorg/apache/http/client/methods/HttpGet;
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    invoke-virtual {p3, v0, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->sendRequest(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/google/android/music/cloudclient/MusicHttpClient;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 221
    .local v1, "response":Lorg/apache/http/HttpResponse;
    const/high16 v3, 0x500000

    invoke-static {v0, v1, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->readAndReleaseShortResponse(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;I)[B

    move-result-object v2

    .line 223
    .local v2, "responseData":[B
    invoke-static {p2, v2}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonData(Ljava/lang/Class;[B)Ljava/lang/Object;

    move-result-object v3

    return-object v3
.end method

.method private executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "jsonData"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 267
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v1, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2, v1}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v0

    .line 269
    .local v0, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    const-string v1, "application/json"

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 270
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executePost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;Landroid/accounts/Account;)Ljava/lang/Object;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "jsonData"    # [B
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/accounts/Account;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 313
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v1, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2, v1}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v0

    .line 315
    .local v0, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    const-string v1, "application/json"

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 316
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executePost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;Landroid/accounts/Account;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private executeJsonPut(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "jsonData"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 301
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v1, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2, v1}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v0

    .line 303
    .local v0, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    const-string v1, "application/json"

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 304
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executePut(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private executePost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpEntity;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 228
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 229
    .local v2, "prefsHolder":Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 232
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    new-instance v1, Lcom/google/android/music/cloudclient/MusicRequest;

    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3, v0}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 233
    .local v1, "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executePost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;Lcom/google/android/music/cloudclient/MusicRequest;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 235
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v3

    .end local v1    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method

.method private executePost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;Landroid/accounts/Account;)Ljava/lang/Object;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/HttpEntity;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpEntity;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/accounts/Account;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 241
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 242
    .local v2, "prefsHolder":Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 245
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    new-instance v1, Lcom/google/android/music/cloudclient/MusicRequest;

    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3, v0, p4}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;Landroid/accounts/Account;)V

    .line 246
    .local v1, "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executePost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;Lcom/google/android/music/cloudclient/MusicRequest;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 248
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v3

    .end local v1    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method

.method private executePost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;Lcom/google/android/music/cloudclient/MusicRequest;)Ljava/lang/Object;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/HttpEntity;
    .param p4, "musicRequest"    # Lcom/google/android/music/cloudclient/MusicRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpEntity;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/music/cloudclient/MusicRequest;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 256
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 257
    .local v0, "httpRequest":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v0, p2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 258
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    invoke-virtual {p4, v0, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->sendRequest(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/google/android/music/cloudclient/MusicHttpClient;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 259
    .local v1, "response":Lorg/apache/http/HttpResponse;
    const/high16 v3, 0x500000

    invoke-static {v0, v1, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->readAndReleaseShortResponse(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;I)[B

    move-result-object v2

    .line 261
    .local v2, "responseData":[B
    invoke-static {p3, v2}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonData(Ljava/lang/Class;[B)Ljava/lang/Object;

    move-result-object v3

    return-object v3
.end method

.method private executePut(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpEntity;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 287
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 288
    .local v2, "prefsHolder":Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 291
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    new-instance v1, Lcom/google/android/music/cloudclient/MusicRequest;

    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3, v0}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 292
    .local v1, "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executePut(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;Lcom/google/android/music/cloudclient/MusicRequest;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 294
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v3

    .end local v1    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3
.end method

.method private executePut(Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/Class;Lcom/google/android/music/cloudclient/MusicRequest;)Ljava/lang/Object;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/HttpEntity;
    .param p4, "musicRequest"    # Lcom/google/android/music/cloudclient/MusicRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpEntity;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/music/cloudclient/MusicRequest;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 277
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 278
    .local v0, "httpRequest":Lorg/apache/http/client/methods/HttpPut;
    invoke-virtual {v0, p2}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 279
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    invoke-virtual {p4, v0, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->sendRequest(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/google/android/music/cloudclient/MusicHttpClient;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 280
    .local v1, "response":Lorg/apache/http/HttpResponse;
    const/high16 v3, 0x500000

    invoke-static {v0, v1, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->readAndReleaseShortResponse(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;I)[B

    move-result-object v2

    .line 282
    .local v2, "responseData":[B
    invoke-static {p3, v2}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonData(Ljava/lang/Class;[B)Ljava/lang/Object;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public createPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 5
    .param p1, "playlist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 589
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v3}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v2

    .line 591
    .local v2, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v0

    .line 593
    .local v0, "entityBytes":[B
    invoke-virtual {v2}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-direct {p0, v3, v0, v4}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 595
    .local v1, "response":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    return-object v1
.end method

.method public createRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;ZII)Lcom/google/android/music/cloudclient/RadioEditStationsResponse;
    .locals 5
    .param p1, "syncableRadiostation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .param p2, "includeFeed"    # Z
    .param p3, "maxEntries"    # I
    .param p4, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    if-nez p1, :cond_0

    .line 388
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Missing radio station"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 391
    :cond_0
    const/16 v2, 0x64

    if-le p3, v2, :cond_1

    .line 392
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many entries requested: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 395
    :cond_1
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forRadioEditStations()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 397
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;->serialize(Lcom/google/android/music/sync/google/model/SyncableRadioStation;ZII)[B

    move-result-object v0

    .line 400
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;

    return-object v2
.end method

.method public deleteUserDevice(Ljava/lang/String;)Z
    .locals 10
    .param p1, "deviceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 501
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "https://mclients.googleapis.com/sj/v1.10/devicemanagementinfo?delete-id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 504
    .local v7, "url":Ljava/lang/String;
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    .line 505
    .local v3, "prefsHolder":Ljava/lang/Object;
    iget-object v8, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v8, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 508
    .local v1, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    new-instance v2, Lcom/google/android/music/cloudclient/MusicRequest;

    iget-object v8, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-direct {v2, v8, v1}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 509
    .local v2, "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, v7}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 510
    .local v0, "httpRequest":Lorg/apache/http/client/methods/HttpDelete;
    iget-object v8, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/music/cloudclient/MusicRequest;->getSharedHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v8

    invoke-virtual {v2, v0, v8}, Lcom/google/android/music/cloudclient/MusicRequest;->sendRequest(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/google/android/music/cloudclient/MusicHttpClient;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 515
    .local v4, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    .line 516
    .local v6, "statusCode":I
    const/16 v8, 0xcc

    if-eq v6, v8, :cond_0

    const/16 v8, 0xc8

    if-eq v6, v8, :cond_0

    .line 517
    const/16 v8, 0x400

    invoke-static {v0, v4, v8}, Lcom/google/android/music/cloudclient/MusicRequest;->readAndReleaseShortResponse(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;I)[B

    move-result-object v5

    .line 519
    .local v5, "responseData":[B
    sget-object v8, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/api/client/json/Json;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    const/4 v8, 0x0

    .line 525
    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .end local v5    # "responseData":[B
    :goto_0
    return v8

    .line 522
    :cond_0
    const/4 v8, 0x1

    .line 525
    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    .end local v0    # "httpRequest":Lorg/apache/http/client/methods/HttpDelete;
    .end local v2    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    .end local v4    # "response":Lorg/apache/http/HttpResponse;
    .end local v6    # "statusCode":I
    :catchall_0
    move-exception v8

    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v8
.end method

.method public enableNautilus(Ljava/lang/String;)Lcom/google/android/music/cloudclient/EnableNautilusResponse;
    .locals 4
    .param p1, "offerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 601
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forEnableNautilusRequest()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 602
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1}, Lcom/google/android/music/cloudclient/EnableNautilusRequest;->serialize(Ljava/lang/String;)[B

    move-result-object v0

    .line 603
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/EnableNautilusResponse;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/EnableNautilusResponse;

    return-object v2
.end method

.method public getArtistShuffleFeed(Ljava/lang/String;II)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .locals 5
    .param p1, "remoteSeedId"    # Ljava/lang/String;
    .param p2, "maxEntries"    # I
    .param p3, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    const/16 v2, 0x64

    if-le p2, v2, :cond_0

    .line 336
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many entries requested: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 338
    :cond_0
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forRadioStationFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 339
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->serializeForArtistShuffle(Ljava/lang/String;II)[B

    move-result-object v0

    .line 342
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    return-object v2
.end method

.method public getCloudQueue()Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 625
    iget-object v1, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/sync/api/MusicUrl;->forCloudQueueRequest(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 626
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;

    return-object v1
.end method

.method public getEphemeralTopTracks(ILjava/lang/String;I)Lcom/google/android/music/sync/google/model/TrackFeed;
    .locals 5
    .param p1, "maxResults"    # I
    .param p2, "continuationToken"    # Ljava/lang/String;
    .param p3, "updateMin"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 472
    new-instance v1, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;

    invoke-direct {v1}, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;-><init>()V

    .line 473
    .local v1, "feedAsPostRequest":Lcom/google/android/music/sync/google/model/FeedAsPostRequest;
    if-eqz p2, :cond_0

    .line 474
    iput-object p2, v1, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;->mStartToken:Ljava/lang/String;

    .line 476
    :cond_0
    iput p1, v1, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;->mMaxResults:I

    .line 477
    invoke-virtual {v1}, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;->serializeAsJson()[B

    move-result-object v0

    .line 478
    .local v0, "entityBytes":[B
    invoke-static {p3}, Lcom/google/android/music/sync/api/MusicUrl;->forEphemeralTop(I)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v2

    .line 479
    .local v2, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v2}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/google/android/music/sync/google/model/TrackFeed;

    invoke-direct {p0, v3, v0, v4}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/TrackFeed;

    return-object v3
.end method

.method public getGenres(Ljava/lang/String;)Lcom/google/android/music/cloudclient/MusicGenresResponseJson;
    .locals 3
    .param p1, "parentGenreId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-static {p1}, Lcom/google/android/music/sync/api/MusicUrl;->forGenres(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 181
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;

    return-object v1
.end method

.method public getListenNowRecommendations()Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 560
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forListenNowRecommendations()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 561
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    return-object v1
.end method

.method public getLuckyRadioFeed(ILjava/util/List;I)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .locals 5
    .param p1, "maxEntries"    # I
    .param p3, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;I)",
            "Lcom/google/android/music/cloudclient/RadioFeedResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 322
    .local p2, "recentlyPlayedIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    const/16 v2, 0x64

    if-le p1, v2, :cond_0

    .line 323
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many entries requested: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 325
    :cond_0
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forRadioStationFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 326
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->serializeForLuckyRadio(ILjava/util/List;I)[B

    move-result-object v0

    .line 329
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    return-object v2
.end method

.method public getMixesFeed(III)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .locals 5
    .param p1, "maxMixes"    # I
    .param p2, "maxEntries"    # I
    .param p3, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x64

    .line 369
    if-le p1, v2, :cond_0

    .line 370
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many seeds requested: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 373
    :cond_0
    if-le p2, v2, :cond_1

    .line 374
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many entries requested: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 377
    :cond_1
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forRadioStationFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 378
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->serialize(III)[B

    move-result-object v0

    .line 379
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    return-object v2
.end method

.method public getNautilusAlbum(Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    .locals 3
    .param p1, "nid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Nautilus id must be specified."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 142
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/sync/api/MusicUrl;->forNautilusAlbum(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 143
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/cloudclient/AlbumJson;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/AlbumJson;

    return-object v1
.end method

.method public getNautilusArtist(Ljava/lang/String;IIZ)Lcom/google/android/music/cloudclient/ArtistJson;
    .locals 3
    .param p1, "nid"    # Ljava/lang/String;
    .param p2, "numTopTracks"    # I
    .param p3, "numRelatedArtists"    # I
    .param p4, "includeAlbums"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x64

    .line 123
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Nautilus id must be specified."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 126
    :cond_0
    if-ltz p2, :cond_1

    if-gt p2, v2, :cond_1

    if-ltz p3, :cond_1

    if-le p3, v2, :cond_2

    .line 128
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid top tracks or related artists params"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :cond_2
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/music/sync/api/MusicUrl;->forNautilusArtist(Ljava/lang/String;IIZ)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 133
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/cloudclient/ArtistJson;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/ArtistJson;

    return-object v1
.end method

.method public getNautilusTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    .locals 3
    .param p1, "nid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Nautilus id must be specified."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 152
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/sync/api/MusicUrl;->forNautilusTrack(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 153
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/sync/google/model/Track;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/Track;

    return-object v1
.end method

.method public getOffersForAccount(Landroid/accounts/Account;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 463
    invoke-static {v2, v2}, Lcom/google/android/music/cloudclient/OffersRequestJson;->serialize(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 465
    .local v0, "entityBytes":[B
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forOffers()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 466
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/OffersResponseJson;

    invoke-direct {p0, v2, v0, v3, p1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;Landroid/accounts/Account;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/OffersResponseJson;

    return-object v2
.end method

.method public getOffersForAccountAndRedeemCoupon(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "optionalCouponCode"    # Ljava/lang/String;
    .param p3, "optionalCouponType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 454
    invoke-static {p2, p3}, Lcom/google/android/music/cloudclient/OffersRequestJson;->serialize(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 456
    .local v0, "entityBytes":[B
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forOffers()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 457
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/OffersResponseJson;

    invoke-direct {p0, v2, v0, v3, p1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;Landroid/accounts/Account;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/OffersResponseJson;

    return-object v2
.end method

.method public getPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 430
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Playlist id must be specified."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 433
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 435
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    return-object v1
.end method

.method public getRadioFeed(Ljava/lang/String;ILjava/util/List;I)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .locals 5
    .param p1, "radioRemoteId"    # Ljava/lang/String;
    .param p2, "maxEntries"    # I
    .param p4, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;I)",
            "Lcom/google/android/music/cloudclient/RadioFeedResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 349
    .local p3, "recentlyPlayedIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    if-nez p1, :cond_0

    .line 350
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Null radio station id"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 353
    :cond_0
    const/16 v2, 0x64

    if-le p2, v2, :cond_1

    .line 354
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many entries requested: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 357
    :cond_1
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forRadioStationFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 359
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/music/cloudclient/RadioFeedRequest;->serialize(Ljava/lang/String;ILjava/util/List;I)[B

    move-result-object v0

    .line 362
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;

    return-object v2
.end method

.method public getSharedEntries(Ljava/lang/String;ILjava/lang/String;J)Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
    .locals 4
    .param p1, "sharedToken"    # Ljava/lang/String;
    .param p2, "maxEntries"    # I
    .param p3, "continuationToken"    # Ljava/lang/String;
    .param p4, "updateMin"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 418
    invoke-static {p2, p3, p4, p5}, Lcom/google/android/music/sync/api/MusicUrl;->forGetSharedPlaylistEntries(ILjava/lang/String;J)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 420
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;->serialize(Ljava/lang/String;ILjava/lang/String;J)[B

    move-result-object v0

    .line 423
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;

    return-object v2
.end method

.method public getSituations()Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 639
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isConciergeListenNowEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 640
    :cond_0
    const-string v5, "MusicCloud"

    const-string v6, "Shouldn\'t be calling getSituations"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    :cond_1
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forListenNowSituations()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v4

    .line 645
    .local v4, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 647
    .local v2, "now":J
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v5

    div-int/lit16 v1, v5, 0x3e8

    .line 648
    .local v1, "tzOffsetSec":I
    sget-boolean v5, Lcom/google/android/music/cloudclient/MusicCloudImpl;->LOGV:Z

    if-eqz v5, :cond_2

    .line 649
    const-string v5, "MusicCloud"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSituations with tzOffsetSec="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    :cond_2
    invoke-static {v1}, Lcom/google/android/music/cloudclient/GetSituationsRequest;->serialize(I)[B

    move-result-object v0

    .line 653
    .local v0, "entityBytes":[B
    invoke-virtual {v4}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/google/android/music/cloudclient/GetSituationsResponse;

    invoke-direct {p0, v5, v0, v6}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/GetSituationsResponse;

    return-object v5
.end method

.method public getSoundSearchEntries(Ljava/lang/String;I)Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;
    .locals 4
    .param p1, "continuationToken"    # Ljava/lang/String;
    .param p2, "numResults"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 485
    invoke-static {p1, p2}, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistRequestJson;->serialize(Ljava/lang/String;I)[B

    move-result-object v0

    .line 487
    .local v0, "entityBytes":[B
    invoke-static {p1, p2}, Lcom/google/android/music/sync/api/MusicUrl;->forSoundSearchAutoPlaylist(Ljava/lang/String;I)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 488
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;

    return-object v2
.end method

.method public getTab(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;ILjava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;
    .locals 5
    .param p1, "tabType"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p2, "numItems"    # I
    .param p3, "genreId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    const/4 v2, 0x1

    if-lt p2, v2, :cond_0

    const/16 v2, 0x64

    if-le p2, v2, :cond_1

    .line 160
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid max entities: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 162
    :cond_1
    sget-object v2, Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;->RECOMMENDED:Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;

    if-eq p1, v2, :cond_2

    sget-object v2, Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;->GENRES:Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;

    if-ne p1, v2, :cond_3

    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 164
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Scoping by genre is not allowed for tab typeRECOMMENDED and GENRES"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 168
    :cond_3
    invoke-static {p1, p2, p3}, Lcom/google/android/music/sync/api/MusicUrl;->forTab(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;ILjava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 169
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/TabsResponseJson;

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/TabsResponseJson;

    .line 170
    .local v0, "response":Lcom/google/android/music/cloudclient/TabsResponseJson;
    if-eqz v0, :cond_4

    iget-object v2, v0, Lcom/google/android/music/cloudclient/TabsResponseJson;->mTabs:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/music/cloudclient/TabsResponseJson;->mTabs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 171
    iget-object v2, v0, Lcom/google/android/music/cloudclient/TabsResponseJson;->mTabs:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/TabJson;

    .line 173
    :goto_0
    return-object v2

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getUserDevicesResponseJson()Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 495
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forUserDevicesList()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 496
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

    return-object v1
.end method

.method public getUserQuizArtistList(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 540
    .local p1, "selectedGenresIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "selectedArtistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p3, "artistBlackListIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forQuizArtistList()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 541
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3}, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;->serialize(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)[B

    move-result-object v0

    .line 544
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/RecommendedArtistsResponseJson;

    return-object v2
.end method

.method public getUserQuizGenresList()Lcom/google/android/music/cloudclient/QuizGenresResponseJson;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 531
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forQuizGenreList()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 532
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v0}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    return-object v1
.end method

.method public getUserQuizResults(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 551
    .local p1, "selectedGenresIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "selectedArtistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p3, "artistBlackListIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forQuizResultsList()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 552
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1, p2, p3}, Lcom/google/android/music/cloudclient/GetQuizResultsRequestJson;->serialize(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)[B

    move-result-object v0

    .line 554
    .local v0, "entityBytes":[B
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    return-object v2
.end method

.method public isPlaylistShared(Ljava/lang/String;)Z
    .locals 4
    .param p1, "remoteId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 567
    invoke-static {p1}, Lcom/google/android/music/sync/api/MusicUrl;->forIsPlaylistShared(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 568
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/IsSharedResponseJson;

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/IsSharedResponseJson;

    .line 569
    .local v0, "response":Lcom/google/android/music/cloudclient/IsSharedResponseJson;
    iget-boolean v2, v0, Lcom/google/android/music/cloudclient/IsSharedResponseJson;->mIsShared:Z

    return v2
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .param p3, "maxResults"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Query string must be specified."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 101
    :cond_0
    const/4 v2, 0x1

    if-lt p3, v2, :cond_1

    const/16 v2, 0x64

    if-le p3, v2, :cond_2

    .line 102
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid max results: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 106
    :cond_2
    const-string v2, "https://mclients.googleapis.com/sj/v1.10/query"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 107
    .local v1, "url":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 108
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "q"

    invoke-virtual {v0, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 109
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 110
    const-string v2, "start-token"

    invoke-virtual {v0, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 112
    :cond_3
    const-string v2, "ct"

    sget-object v3, Lcom/google/android/music/cloudclient/MusicCloudImpl;->DEFAULT_SEARCH_CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 113
    const-string v2, "max-results"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 114
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 116
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeGet(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    return-object v2
.end method

.method public setCloudQueue(Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;
    .locals 4
    .param p1, "request"    # Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 609
    invoke-static {p1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v0

    .line 610
    .local v0, "entityBytes":[B
    iget-object v2, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/sync/api/MusicUrl;->forCloudQueueRequest(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 611
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;

    return-object v2
.end method

.method public syncCloudQueue(Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
    .locals 4
    .param p1, "request"    # Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 632
    invoke-static {p1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v0

    .line 633
    .local v0, "entityBytes":[B
    iget-object v2, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/sync/api/MusicUrl;->forCloudQueueSyncRequest(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 634
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPost(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;

    return-object v2
.end method

.method public updateCloudQueue(Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    .locals 4
    .param p1, "request"    # Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 617
    invoke-static {p1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v0

    .line 618
    .local v0, "entityBytes":[B
    iget-object v2, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/sync/api/MusicUrl;->forCloudQueueRequest(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v1

    .line 619
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPut(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;

    return-object v2
.end method

.method public updatePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 5
    .param p1, "playlist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 576
    iget-object v3, p0, Lcom/google/android/music/cloudclient/MusicCloudImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v2

    .line 578
    .local v2, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {p1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v0

    .line 580
    .local v0, "entityBytes":[B
    invoke-virtual {v2}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-direct {p0, v3, v0, v4}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->executeJsonPut(Ljava/lang/String;[BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 582
    .local v1, "response":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    return-object v1
.end method
