.class final enum Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;
.super Ljava/lang/Enum;
.source "DownloadContainerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/DownloadContainerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ContainerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

.field public static final enum ALBUM:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

.field public static final enum AUTO_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

.field public static final enum RADIO_STATION:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

.field public static final enum USER_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->ALBUM:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    .line 56
    new-instance v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    const-string v1, "USER_PLAYLIST"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->USER_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    .line 57
    new-instance v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    const-string v1, "AUTO_PLAYLIST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->AUTO_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    .line 58
    new-instance v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    const-string v1, "RADIO_STATION"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->RADIO_STATION:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    sget-object v1, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->ALBUM:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->USER_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->AUTO_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->RADIO_STATION:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->$VALUES:[Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static fromDbValue(I)Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;
    .locals 3
    .param p0, "dbValue"    # I

    .prologue
    .line 61
    packed-switch p0, :pswitch_data_0

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid download container type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :pswitch_0
    sget-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->ALBUM:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    .line 69
    :goto_0
    return-object v0

    .line 65
    :pswitch_1
    sget-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->USER_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    goto :goto_0

    .line 67
    :pswitch_2
    sget-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->AUTO_PLAYLIST:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    goto :goto_0

    .line 69
    :pswitch_3
    sget-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->RADIO_STATION:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    const-class v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->$VALUES:[Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    invoke-virtual {v0}, [Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    return-object v0
.end method
