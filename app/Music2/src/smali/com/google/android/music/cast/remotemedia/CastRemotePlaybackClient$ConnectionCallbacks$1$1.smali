.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1$1;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;->onResult(Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;)V
    .locals 0

    .prologue
    .line 718
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 718
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1$1;->onResult(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method public onResult(Lcom/google/android/gms/common/api/Status;)V
    .locals 3
    .param p1, "status"    # Lcom/google/android/gms/common/api/Status;

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    const-string v0, "MusicCastRemote"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "joinSessionExtras message status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 727
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsSessionInitialized:Z
    invoke-static {v0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1602(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 738
    :goto_0
    return-void

    .line 732
    :cond_1
    const-string v0, "MusicCastRemote"

    const-string v1, "Invalidating route due to joinSessionExtras error"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$1;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    goto :goto_0
.end method
