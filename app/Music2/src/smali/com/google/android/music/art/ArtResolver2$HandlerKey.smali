.class final Lcom/google/android/music/art/ArtResolver2$HandlerKey;
.super Ljava/lang/Object;
.source "ArtResolver2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtResolver2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "HandlerKey"
.end annotation


# instance fields
.field final artType:Lcom/google/android/music/art/ArtType;

.field final descriptorClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/music/art/ArtDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Class;Lcom/google/android/music/art/ArtType;)V
    .locals 0
    .param p2, "artType"    # Lcom/google/android/music/art/ArtType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/music/art/ArtDescriptor;",
            ">;",
            "Lcom/google/android/music/art/ArtType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 456
    .local p1, "descriptorClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/android/music/art/ArtDescriptor;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->descriptorClass:Ljava/lang/Class;

    .line 458
    iput-object p2, p0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->artType:Lcom/google/android/music/art/ArtType;

    .line 459
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Class;Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtResolver2$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Class;
    .param p2, "x1"    # Lcom/google/android/music/art/ArtType;
    .param p3, "x2"    # Lcom/google/android/music/art/ArtResolver2$1;

    .prologue
    .line 452
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtResolver2$HandlerKey;-><init>(Ljava/lang/Class;Lcom/google/android/music/art/ArtType;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 463
    if-ne p0, p1, :cond_1

    .line 471
    :cond_0
    :goto_0
    return v1

    .line 464
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 466
    check-cast v0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;

    .line 468
    .local v0, "that":Lcom/google/android/music/art/ArtResolver2$HandlerKey;
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->artType:Lcom/google/android/music/art/ArtType;

    iget-object v4, v0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->artType:Lcom/google/android/music/art/ArtType;

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 469
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->descriptorClass:Ljava/lang/Class;

    iget-object v4, v0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->descriptorClass:Ljava/lang/Class;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 476
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->descriptorClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 477
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;->artType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtType;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 478
    return v0
.end method
