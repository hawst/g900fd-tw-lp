.class public Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
.super Lcom/google/api/client/json/GenericJson;
.source "SyncablePlaylistEntry.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;


# static fields
.field public static final LOCKER_SOURCE:I = 0x1

.field public static final NAUTILUS_SOURCE:I = 0x2


# instance fields
.field public mAbsolutePosition:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "absolutePosition"
    .end annotation
.end field

.field public mClientId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "clientId"
    .end annotation
.end field

.field public mCreationTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "creationTimestamp"
    .end annotation
.end field

.field public mFollowingEntryId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "followingEntryId"
    .end annotation
.end field

.field public mIsDeleted:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "deleted"
    .end annotation
.end field

.field public mItem:Lcom/google/android/music/store/PlayList$Item;

.field public mLastModifiedTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lastModifiedTimestamp"
    .end annotation
.end field

.field public mLocalId:J

.field public mLocalMusicId:J

.field public mLocalPlaylistId:J

.field public mPrecedingEntryId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "precedingEntryId"
    .end annotation
.end field

.field public mRemoteId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mRemotePlaylistId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlistId"
    .end annotation
.end field

.field public mSource:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "source"
    .end annotation
.end field

.field public mTrack:Lcom/google/android/music/sync/google/model/Track;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "track"
    .end annotation
.end field

.field public mTrackId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 27
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 54
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mCreationTimestamp:J

    .line 57
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mSource:I

    .line 69
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalId:J

    .line 71
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalMusicId:J

    .line 73
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalPlaylistId:J

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mItem:Lcom/google/android/music/store/PlayList$Item;

    return-void
.end method

.method private static convertClientSourceTypeToServerSource(I)I
    .locals 3
    .param p0, "sourceType"    # I

    .prologue
    .line 304
    packed-switch p0, :pswitch_data_0

    .line 310
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a syncable sourceType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :pswitch_0
    const/4 v0, 0x1

    .line 308
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 304
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static convertServerSourceToClientSourceType(I)I
    .locals 3
    .param p0, "source"    # I

    .prologue
    .line 315
    packed-switch p0, :pswitch_data_0

    .line 321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :pswitch_0
    const/4 v0, 0x2

    .line 319
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 315
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static parse(Lcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .locals 2
    .param p0, "item"    # Lcom/google/android/music/store/PlayList$Item;

    .prologue
    .line 151
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {v0, v1, p0}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->parse(Ljava/lang/String;ILcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;ILcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .locals 4
    .param p0, "sourceId"    # Ljava/lang/String;
    .param p1, "clientSourceType"    # I
    .param p2, "item"    # Lcom/google/android/music/store/PlayList$Item;

    .prologue
    .line 158
    new-instance v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-direct {v1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;-><init>()V

    .line 159
    .local v1, "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    iput-object p2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mItem:Lcom/google/android/music/store/PlayList$Item;

    .line 160
    iput-object p0, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    .line 161
    invoke-virtual {p2}, Lcom/google/android/music/store/PlayList$Item;->getSourceId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    .line 162
    invoke-virtual {p2}, Lcom/google/android/music/store/PlayList$Item;->getId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalId:J

    .line 163
    invoke-virtual {p2}, Lcom/google/android/music/store/PlayList$Item;->getMusicId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalMusicId:J

    .line 164
    invoke-virtual {p2}, Lcom/google/android/music/store/PlayList$Item;->getClientId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 165
    invoke-virtual {p2}, Lcom/google/android/music/store/PlayList$Item;->getClientId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mClientId:Ljava/lang/String;

    .line 168
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/music/store/PlayList$Item;->getSourceVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/music/store/PlayList$Item;->getListId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalPlaylistId:J

    .line 174
    invoke-virtual {v1, p1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->setServerSourceFromClientSourceType(I)V

    .line 176
    return-object v1

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    goto :goto_0
.end method

.method public static parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    :try_start_0
    sget-object v2, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    invoke-virtual {v2, p0}, Lorg/codehaus/jackson/JsonFactory;->createJsonParser(Ljava/io/InputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    .line 99
    .local v1, "parser":Lorg/codehaus/jackson/JsonParser;
    invoke-virtual {v1}, Lorg/codehaus/jackson/JsonParser;->nextToken()Lorg/codehaus/jackson/JsonToken;

    .line 100
    const-class v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/api/client/json/Json;->parse(Lorg/codehaus/jackson/JsonParser;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    :try_end_0
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 101
    .end local v1    # "parser":Lorg/codehaus/jackson/JsonParser;
    :catch_0
    move-exception v0

    .line 102
    .local v0, "je":Lorg/codehaus/jackson/JsonParseException;
    new-instance v2, Ljava/io/IOException;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public formatAsPlayListItem(Lcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/store/PlayList$Item;
    .locals 4
    .param p1, "item"    # Lcom/google/android/music/store/PlayList$Item;

    .prologue
    .line 124
    if-nez p1, :cond_0

    .line 125
    new-instance p1, Lcom/google/android/music/store/PlayList$Item;

    .end local p1    # "item":Lcom/google/android/music/store/PlayList$Item;
    invoke-direct {p1}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 127
    .restart local p1    # "item":Lcom/google/android/music/store/PlayList$Item;
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalMusicId:J

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/store/PlayList$Item;->setMusicId(J)V

    .line 129
    iget-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/android/music/store/PlayList$Item;->setSourceId(Ljava/lang/String;)V

    .line 130
    const-string v0, ""

    .line 131
    .local v0, "position":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mAbsolutePosition:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 132
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mAbsolutePosition:Ljava/lang/String;

    .line 137
    invoke-virtual {p1, v0}, Lcom/google/android/music/store/PlayList$Item;->setServerPosition(Ljava/lang/String;)V

    .line 138
    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/store/PlayList$Item;->setSourceVersion(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList$Item;->getClientId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 140
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList$Item;->getClientId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mClientId:Ljava/lang/String;

    .line 142
    :cond_1
    return-object p1

    .line 134
    :cond_2
    new-instance v1, Lcom/google/android/music/store/InvalidDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server provided no position for id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in playlist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 234
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistEntriesBatchMutation()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getCreationTimestamp()J
    .locals 2

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mCreationTimestamp:J

    return-wide v0
.end method

.method public getEncapsulatedItem()Lcom/google/android/music/store/PlayList$Item;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mItem:Lcom/google/android/music/store/PlayList$Item;

    return-object v0
.end method

.method public getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 226
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistEntriesFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 230
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistEntriesFeedAsPost()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getLastModifiedTimestamp()J
    .locals 2

    .prologue
    .line 250
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    return-wide v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalId:J

    return-wide v0
.end method

.method public getLocalMusicId()J
    .locals 2

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalMusicId:J

    return-wide v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mSource:I

    return v0
.end method

.method public getTrack()Lcom/google/android/music/sync/google/model/Track;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-static {p2}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistEntry(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    return v0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdate()Z
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serializeAsJson()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 80
    .local v0, "byteStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v3, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    sget-object v4, Lorg/codehaus/jackson/JsonEncoding;->UTF8:Lorg/codehaus/jackson/JsonEncoding;

    invoke-virtual {v3, v0, v4}, Lorg/codehaus/jackson/JsonFactory;->createJsonGenerator(Ljava/io/OutputStream;Lorg/codehaus/jackson/JsonEncoding;)Lorg/codehaus/jackson/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 83
    .local v2, "generator":Lorg/codehaus/jackson/JsonGenerator;
    :try_start_1
    invoke-static {v2, p0}, Lcom/google/api/client/json/Json;->serialize(Lorg/codehaus/jackson/JsonGenerator;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :try_start_2
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 92
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 85
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V

    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 87
    .end local v2    # "generator":Lorg/codehaus/jackson/JsonGenerator;
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "MusicSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to serialize a playlist entry as JSON: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 89
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    const-string v4, "Unable to serialize playlist entry for upstream sync."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public setCreationTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 262
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mCreationTimestamp:J

    .line 263
    return-void
.end method

.method public setIsDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 274
    iput-boolean p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    .line 275
    return-void
.end method

.method public setLastModifiedTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 254
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    .line 255
    return-void
.end method

.method public setRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    .line 247
    return-void
.end method

.method public setServerSourceFromClientSourceType(I)V
    .locals 1
    .param p1, "clientSourceType"    # I

    .prologue
    .line 282
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 283
    invoke-static {p1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->convertClientSourceTypeToServerSource(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mSource:I

    .line 285
    :cond_0
    return-void
.end method

.method public setTrackId(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackId"    # Ljava/lang/String;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    .line 279
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 203
    .local v0, "s":Ljava/lang/StringBuffer;
    const-string v1, "; remoteid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; ctime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mCreationTimestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; mtime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; isDeleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; remote entry id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; remote playlist id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; local playlist id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalPlaylistId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; absolute position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mAbsolutePosition:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; preceding id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mPrecedingEntryId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; following id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mFollowingEntryId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; remote track id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; source id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; client id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mClientId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; source: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mSource:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; track: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 218
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public validateForUpstreamDelete()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid playlist entry for upstream delete."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_0
    return-void
.end method

.method public validateForUpstreamInsert()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid playlist entry for upstream insert."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_1
    return-void
.end method

.method public validateForUpstreamUpdate()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid playlist entry for upstream update."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_1
    return-void
.end method

.method public wipeAllFields()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 107
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    .line 108
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    .line 109
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mAbsolutePosition:Ljava/lang/String;

    .line 110
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    .line 111
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mCreationTimestamp:J

    .line 112
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    .line 114
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalPlaylistId:J

    .line 115
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mItem:Lcom/google/android/music/store/PlayList$Item;

    .line 116
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalId:J

    .line 117
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLocalMusicId:J

    .line 118
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mClientId:Ljava/lang/String;

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mSource:I

    .line 120
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    .line 121
    return-void
.end method
