.class public Lcom/google/android/music/dial/DialMediaRouteProviderService;
.super Landroid/support/v7/media/MediaRouteProviderService;
.source "DialMediaRouteProviderService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/support/v7/media/MediaRouteProviderService;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateMediaRouteProvider()Landroid/support/v7/media/MediaRouteProvider;
    .locals 2

    .prologue
    .line 17
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isDialMediaRouteSupportEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    .line 22
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/music/dial/DialMediaRouteProvider;

    const-string v1, "urn:sonos-com:service:SonosGroup:1"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/dial/DialMediaRouteProvider;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
