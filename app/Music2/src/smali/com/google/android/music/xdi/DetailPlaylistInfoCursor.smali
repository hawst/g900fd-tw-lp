.class Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;
.super Landroid/database/MatrixCursor;
.source "DetailPlaylistInfoCursor.java"


# static fields
.field private static final PROJECTION_PLAYLISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "playlist_owner_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "playlistId"    # J

    .prologue
    .line 40
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mContext:Landroid/content/Context;

    .line 42
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 44
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->addPlaylistInfo(J)V

    .line 45
    return-void
.end method

.method private addPlaylistInfo(J)V
    .locals 13
    .param p1, "playlistId"    # J

    .prologue
    .line 48
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mContext:Landroid/content/Context;

    sget-object v9, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    invoke-static {v8, p1, p2, v9}, Lcom/google/android/music/xdi/XdiUtils;->getPlaylistCursor(Landroid/content/Context;J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 50
    .local v1, "cursor":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 55
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 56
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v8}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v8

    new-array v7, v8, [Ljava/lang/Object;

    .line 57
    .local v7, "values":[Ljava/lang/Object;
    const/4 v8, 0x0

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 58
    .local v2, "name":Ljava/lang/String;
    const/4 v8, 0x1

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 59
    .local v4, "ownerName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 60
    .local v3, "ownerLabel":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 61
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mContext:Landroid/content/Context;

    const v9, 0x7f0b0102

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 64
    :cond_1
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mContext:Landroid/content/Context;

    const/4 v9, 0x0

    invoke-static {v8, p1, p2, v9}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsCount(Landroid/content/Context;JZ)I

    move-result v5

    .line 66
    .local v5, "songCount":I
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/high16 v9, 0x7f120000

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v5, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 69
    .local v6, "songCountString":Ljava/lang/String;
    sget-object v8, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "details/playlists"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "section"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "playlist"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "actions"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 77
    .local v0, "actionsUri":Landroid/net/Uri;
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "_id"

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v7, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_name"

    invoke-virtual {v8, v7, v9, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 79
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_subname"

    invoke-virtual {v8, v7, v9, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 80
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_description"

    invoke-virtual {v8, v7, v9, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 82
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_category"

    const/4 v10, 0x0

    invoke-virtual {v8, v7, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_date"

    const/4 v10, 0x0

    invoke-virtual {v8, v7, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "action_uri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v7, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    invoke-virtual {p0, v7}, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    .end local v0    # "actionsUri":Landroid/net/Uri;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "ownerLabel":Ljava/lang/String;
    .end local v4    # "ownerName":Ljava/lang/String;
    .end local v5    # "songCount":I
    .end local v6    # "songCountString":Ljava/lang/String;
    .end local v7    # "values":[Ljava/lang/Object;
    :cond_2
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v8

    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v8
.end method
