.class public Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "NowPlayingWidgetProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private handleTouchWiz(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 161
    const-string v8, "widgetId"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 165
    .local v5, "widgetId":I
    const-string v8, "widgetspany"

    const/4 v9, 0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    mul-int/lit8 v4, v8, 0x5a

    .line 166
    .local v4, "widgetHeight":I
    const-string v8, "widgetspanx"

    const/4 v9, 0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    mul-int/lit8 v6, v8, 0x5a

    .line 172
    .local v6, "widgetWidth":I
    if-lez v4, :cond_0

    .line 173
    const-string v8, "pref_music_widget"

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 175
    .local v3, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 176
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v8, "%s//%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "pref_music_widget_height"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "heightKey":Ljava/lang/String;
    const-string v8, "%s//%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "pref_music_widget_width"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 178
    .local v7, "widthKey":Ljava/lang/String;
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 179
    invoke-interface {v0, v7, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 183
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "heightKey":Ljava/lang/String;
    .end local v3    # "preferences":Landroid/content/SharedPreferences;
    .end local v7    # "widthKey":Ljava/lang/String;
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 184
    .local v2, "musicState":Landroid/content/Intent;
    invoke-static {p1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtrasFromSharedPreferences(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    .line 186
    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    aput v5, v8, v9

    const/4 v9, 0x0

    invoke-static {p1, v2, v8, v9}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->performUpdate(Landroid/content/Context;Landroid/content/Intent;[ILandroid/os/Bundle;)V

    .line 188
    return-void
.end method

.method public static notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "musicState"    # Landroid/content/Intent;
    .param p2, "what"    # Ljava/lang/String;

    .prologue
    .line 107
    const-string v2, "com.android.music.playstatechanged"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "playing"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 118
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 114
    .local v1, "awm":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/android/music/MediaAppWidgetProvider;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 117
    .local v0, "appWidgetIds":[I
    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v2}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->performUpdate(Landroid/content/Context;Landroid/content/Intent;[ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private static performUpdate(Landroid/content/Context;Landroid/content/Intent;[ILandroid/os/Bundle;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "musicState"    # Landroid/content/Intent;
    .param p2, "appWidgetIds"    # [I
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 89
    .local v1, "awm":Landroid/appwidget/AppWidgetManager;
    move-object v0, p2

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget v6, v0, v3

    .line 91
    .local v6, "widgetId":I
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p0, p1, v7, p3}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->createViews(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Integer;Landroid/os/Bundle;)Landroid/widget/RemoteViews;

    move-result-object v5

    .line 93
    .local v5, "views":Landroid/widget/RemoteViews;
    invoke-virtual {v1, v6, v5}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    .end local v5    # "views":Landroid/widget/RemoteViews;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 94
    :catch_0
    move-exception v2

    .line 95
    .local v2, "ex":Ljava/lang/Exception;
    const-string v7, "MusicWidget"

    const-string v8, "Failed to create views for Music widget"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 98
    .end local v2    # "ex":Ljava/lang/Exception;
    .end local v6    # "widgetId":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "awm"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "widgetId"    # I
    .param p4, "newOptions"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 126
    .local v0, "musicState":Landroid/content/Intent;
    invoke-static {p1, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtrasFromSharedPreferences(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    .line 128
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p3, v1, v2

    invoke-static {p1, v0, v1, p4}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->performUpdate(Landroid/content/Context;Landroid/content/Intent;[ILandroid/os/Bundle;)V

    .line 131
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 134
    :cond_0
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 148
    const-string v6, "pref_music_widget"

    invoke-virtual {p1, v6, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 150
    .local v4, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 151
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    move-object v0, p2

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v5, v0, v2

    .line 152
    .local v5, "widgetId":I
    const-string v6, "%s//%s"

    new-array v7, v11, [Ljava/lang/Object;

    const-string v8, "pref_music_widget_height"

    aput-object v8, v7, v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 153
    const-string v6, "%s//%s"

    new-array v7, v11, [Ljava/lang/Object;

    const-string v8, "pref_music_widget_width"

    aput-object v8, v7, v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 151
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    .end local v5    # "widgetId":I
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 156
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 157
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->handleTouchWiz(Landroid/content/Context;Landroid/content/Intent;)V

    .line 143
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 144
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "awm"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 66
    .local v0, "musicState":Landroid/content/Intent;
    invoke-static {p1, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtrasFromSharedPreferences(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    .line 68
    const/4 v2, 0x0

    invoke-static {p1, v0, p3, v2}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->performUpdate(Landroid/content/Context;Landroid/content/Intent;[ILandroid/os/Bundle;)V

    .line 72
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.music.musicservicecommand"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v1, "updateIntent":Landroid/content/Intent;
    const-string v2, "command"

    const-string v3, "musicwidgetupdate"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const-string v2, "device"

    const-string v3, "any"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v2, "appWidgetIds"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 77
    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 78
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 79
    return-void
.end method
