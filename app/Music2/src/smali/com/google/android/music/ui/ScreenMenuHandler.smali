.class public Lcom/google/android/music/ui/ScreenMenuHandler;
.super Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.source "ScreenMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ScreenMenuHandler$1;,
        Lcom/google/android/music/ui/ScreenMenuHandler$SaveQueue;,
        Lcom/google/android/music/ui/ScreenMenuHandler$ClearQueue;,
        Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;
    }
.end annotation


# instance fields
.field private final mScreenMenuType:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "type"    # Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;)V

    .line 42
    iput-object p3, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mScreenMenuType:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/ScreenMenuHandler;)Lcom/google/android/music/ui/MusicFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ScreenMenuHandler;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    return-object v0
.end method

.method private addClearQueue(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    new-instance v0, Lcom/google/android/music/ui/ScreenMenuHandler$ClearQueue;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/ScreenMenuHandler$ClearQueue;-><init>(Lcom/google/android/music/ui/ScreenMenuHandler;Landroid/content/res/Resources;)V

    .line 149
    .local v0, "clearQ":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method private addSaveQueue(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    new-instance v0, Lcom/google/android/music/ui/ScreenMenuHandler$SaveQueue;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/ScreenMenuHandler$SaveQueue;-><init>(Lcom/google/android/music/ui/ScreenMenuHandler;Landroid/content/res/Resources;)V

    .line 154
    .local v0, "saveQ":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    return-void
.end method


# virtual methods
.method public addMenuEntries(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v3, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 48
    .local v2, "res":Landroid/content/res/Resources;
    sget-object v3, Lcom/google/android/music/ui/ScreenMenuHandler$1;->$SwitchMap$com$google$android$music$ui$ScreenMenuHandler$ScreenMenuType:[I

    iget-object v4, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mScreenMenuType:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    invoke-virtual {v4}, Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 143
    const-string v3, "ScreenMenuHandler"

    const-string v4, "Unsupported screen type"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 50
    :pswitch_0
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 51
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 52
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addGoToArtist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 53
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addGoToAlbum(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 54
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToMyLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 55
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShareTrackEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 56
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addClearQueue(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 57
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addSaveQueue(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 58
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isFindVideoEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addFindVideo(Landroid/content/res/Resources;Ljava/util/List;)V

    goto :goto_0

    .line 63
    :pswitch_1
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addClearQueue(Landroid/content/res/Resources;Ljava/util/List;)V

    goto :goto_0

    .line 66
    :pswitch_2
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v0

    .line 67
    .local v0, "isXLarge":Z
    sget-object v3, Lcom/google/android/music/ui/ScreenMenuHandler$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    iget-object v4, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 139
    const-string v3, "ScreenMenuHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unhandled document type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 72
    :pswitch_3
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 73
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShuffleEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 75
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 76
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 77
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 78
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addGoToArtist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 79
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToMyLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 80
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addRemoveFromLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 81
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShareAlbumEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 82
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShopArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 83
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addBuyEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 90
    :pswitch_4
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 91
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShuffleEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 93
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 94
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 95
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 96
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShopArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 100
    :pswitch_5
    iget-object v3, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v1

    .line 104
    .local v1, "playlistType":I
    iget-object v3, p0, Lcom/google/android/music/ui/ScreenMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v3, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->isRadioSupportedForPlaylistType(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 106
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 110
    :cond_1
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShuffleEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 112
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 113
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 114
    const/16 v3, 0x64

    if-eq v1, v3, :cond_2

    .line 115
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 117
    :cond_2
    if-nez v1, :cond_3

    .line 118
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addEditPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 120
    :cond_3
    const/16 v3, 0x46

    if-ne v1, v3, :cond_4

    .line 121
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addFollowSharedPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 123
    :cond_4
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addSharePlaylistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 124
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addDeleteEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 131
    .end local v1    # "playlistType":I
    :pswitch_6
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addShuffleEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 133
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 134
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 135
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 67
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
