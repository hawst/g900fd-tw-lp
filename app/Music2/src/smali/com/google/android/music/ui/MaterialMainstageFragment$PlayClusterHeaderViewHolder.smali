.class Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MaterialMainstageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialMainstageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlayClusterHeaderViewHolder"
.end annotation


# instance fields
.field public final headerView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;)V
    .locals 1
    .param p1, "headerView"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    .prologue
    .line 269
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 270
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;->headerView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    .line 271
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;->headerView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setTag(Ljava/lang/Object;)V

    .line 272
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;Lcom/google/android/music/ui/MaterialMainstageFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;
    .param p2, "x1"    # Lcom/google/android/music/ui/MaterialMainstageFragment$1;

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;-><init>(Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;)V

    return-void
.end method
