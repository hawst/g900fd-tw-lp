.class Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;
.super Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
.source "ItemPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/ItemPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ItemViewHolder"
.end annotation


# instance fields
.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

.field private mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;


# direct methods
.method constructor <init>(Landroid/view/View;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 66
    const v0, 0x7f0e015f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/leanback/ImageCardView;

    iput-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

    .line 67
    iput-object p2, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mExecutor:Ljava/util/concurrent/Executor;

    .line 68
    return-void
.end method


# virtual methods
.method bind(Lcom/google/android/music/leanback/Item;IIZ)V
    .locals 6
    .param p1, "item"    # Lcom/google/android/music/leanback/Item;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "cropToSquare"    # Z

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->cancel(Z)Z

    .line 74
    :cond_0
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    iget-object v1, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mImageCardView:Lcom/google/android/music/leanback/ImageCardView;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;-><init>(Lcom/google/android/music/leanback/ImageCardView;Lcom/google/android/music/leanback/Item;IIZ)V

    iput-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    .line 76
    iget-object v1, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    iget-object v2, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mExecutor:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 77
    return-void
.end method

.method unbind()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;->cancel(Z)Z

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->mItemViewHolderLoadBitmapAsyncTask:Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;

    .line 84
    :cond_0
    return-void
.end method
