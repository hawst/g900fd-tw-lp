.class public Lcom/google/android/music/cloudclient/QuizResultsJson;
.super Lcom/google/api/client/json/GenericJson;
.source "QuizResultsJson.java"


# instance fields
.field mSelectedArtistIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSelectedGenres:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            ">;"
        }
    .end annotation
.end field

.field mUnselectedArtistIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static serialize(Ljava/util/List;Ljava/util/List;Ljava/util/List;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "selectedGenres":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/QuizGenreJson;>;"
    .local p1, "selectedArtistIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "unselectedArtistIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/music/cloudclient/QuizResultsJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/QuizResultsJson;-><init>()V

    .line 26
    .local v0, "result":Lcom/google/android/music/cloudclient/QuizResultsJson;
    iput-object p0, v0, Lcom/google/android/music/cloudclient/QuizResultsJson;->mSelectedGenres:Ljava/util/List;

    .line 27
    iput-object p1, v0, Lcom/google/android/music/cloudclient/QuizResultsJson;->mSelectedArtistIds:Ljava/util/List;

    .line 28
    iput-object p2, v0, Lcom/google/android/music/cloudclient/QuizResultsJson;->mUnselectedArtistIds:Ljava/util/List;

    .line 29
    invoke-static {v0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v1

    return-object v1
.end method
