.class public Lcom/google/android/music/store/DownloadQueueContentProviderHelper;
.super Ljava/lang/Object;
.source "DownloadQueueContentProviderHelper.java"


# static fields
.field private static final AUTOPLAYLIST_PROJECTION:[Ljava/lang/String;

.field private static sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAutoplaylistProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 31
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "playlist_id"

    aput-object v1, v0, v6

    const-string v1, "playlist_name"

    aput-object v1, v0, v3

    const-string v1, "playlist_type"

    aput-object v1, v0, v4

    const-string v1, "DateAdded"

    aput-object v1, v0, v7

    const-string v1, "ContainerSizeBytes"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->AUTOPLAYLIST_PROJECTION:[Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    .line 54
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "container_type"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "MUSIC.AlbumId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "Album"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "subtitle"

    const-string v2, "AlbumArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "subtype"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 65
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "artUrls"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 67
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "DateAdded"

    const-string v2, "DateAdded"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "remoteId"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 71
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "containerSizeBytes"

    const-string v2, "ContainerSizeBytes"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    .line 76
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "container_type"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "LISTS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "LISTS.Name"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "subtitle"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "subtype"

    const-string v2, "ListType"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "artUrls"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "DateAdded"

    const-string v2, "DateAdded"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "remoteId"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "containerSizeBytes"

    const-string v2, "ContainerSizeBytes"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    .line 98
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "container_type"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "RADIO_STATIONS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "RADIO_STATIONS.Name"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "subtitle"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 107
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "subtype"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 109
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "artUrls"

    const-string v2, "RADIO_STATIONS.ArtworkLocation"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "DateAdded"

    const-string v2, "DateAdded"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "remoteId"

    const-string v2, "RADIO_STATIONS.SourceId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    const-string v1, "containerSizeBytes"

    const-string v2, "ContainerSizeBytes"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAutoplaylistProjectionMap:Ljava/util/HashMap;

    .line 124
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAutoplaylistProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAutoplaylistProjectionMap:Ljava/util/HashMap;

    const-string v1, "title"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAutoplaylistProjectionMap:Ljava/util/HashMap;

    const-string v1, "subtype"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAutoplaylistProjectionMap:Ljava/util/HashMap;

    const-string v1, "DateAdded"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAutoplaylistProjectionMap:Ljava/util/HashMap;

    const-string v1, "containerSizeBytes"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    return-void
.end method

.method private static getAlbumsCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "inColumns"    # [Ljava/lang/String;
    .param p2, "condition"    # Ljava/lang/String;
    .param p3, "order"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 138
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 140
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "MUSIC JOIN SHOULDKEEPON JOIN KEEPON ON (MUSIC.Id = SHOULDKEEPON.MusicId AND SHOULDKEEPON.KeepOnId = KEEPON.KeepOnId)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145
    sget-object v1, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAlbumDownloadQueueProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 146
    const-string v3, "KEEPON.AlbumId IS NOT NULL"

    .line 147
    .local v3, "where":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 152
    :cond_0
    const-string v5, "MUSIC.AlbumId"

    move-object v1, p0

    move-object v2, p1

    move-object v6, v4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private static getAutoPlaylistsCursor(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "inColumns"    # [Ljava/lang/String;
    .param p3, "cancellationSignal"    # Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;
    .param p4, "condition"    # Ljava/lang/String;
    .param p5, "order"    # Ljava/lang/String;

    .prologue
    .line 316
    const-string v12, " FROM MUSIC JOIN SHOULDKEEPON JOIN KEEPON"

    .line 319
    .local v12, "baseFromStatement":Ljava/lang/String;
    const-string v13, " ON MUSIC.Id = SHOULDKEEPON.MusicId AND SHOULDKEEPON.KeepOnId = KEEPON.KeepOnId"

    .line 321
    .local v13, "baseOnStatement":Ljava/lang/String;
    const-string v14, " WHERE KEEPON.AutoListId IS NOT NULL"

    .line 323
    .local v14, "baseWhereStatement":Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_0

    .line 324
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " AND "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 327
    :cond_0
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, " SELECT DISTINCT AutoListId , DateAdded"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " ORDER BY "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 332
    .local v5, "autoPlayListQuery":Ljava/lang/String;
    const/16 v22, 0x0

    .line 333
    .local v22, "success":Z
    const/4 v10, 0x0

    .line 334
    .local v10, "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    const/4 v6, 0x0

    .line 337
    .local v6, "autoPlaylistCursor":Landroid/database/Cursor;
    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->rawQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v7

    .line 341
    .local v7, "autoPlaylistIdCursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 343
    .local v8, "autolistCount":I
    if-eqz v7, :cond_1

    .line 344
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 346
    :cond_1
    new-array v9, v8, [J

    .line 348
    .local v9, "autolistId":[J
    const/16 v17, 0x0

    .line 349
    .local v17, "i":I
    :goto_0
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v24

    if-eqz v24, :cond_2

    .line 351
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    aput-wide v24, v9, v17

    .line 352
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 355
    :cond_2
    new-instance v16, Lcom/google/android/music/store/AutoPlaylistCursorFactory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;-><init>(Landroid/content/Context;[J)V

    .line 356
    .local v16, "factory":Lcom/google/android/music/store/AutoPlaylistCursorFactory;
    sget-object v24, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->AUTOPLAYLIST_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->buildCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v6

    .line 358
    new-instance v11, Landroid/database/MatrixCursor;

    move-object/from16 v0, p2

    invoke-direct {v11, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 361
    .end local v10    # "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    .local v11, "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    if-eqz v6, :cond_8

    .line 362
    :goto_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v24

    if-eqz v24, :cond_8

    .line 363
    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 364
    .local v21, "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v4, p2

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_2
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_7

    aget-object v15, v4, v18

    .line 365
    .local v15, "columnName":Ljava/lang/String;
    sget-object v24, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sAutoplaylistProjectionMap:Ljava/util/HashMap;

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    .line 366
    .local v20, "mappedIndex":Ljava/lang/Integer;
    if-nez v20, :cond_5

    .line 368
    const-string v24, "container_type"

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 369
    const/16 v24, 0x3

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    :goto_3
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 371
    :cond_3
    const/16 v24, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 390
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v15    # "columnName":Ljava/lang/String;
    .end local v18    # "i$":I
    .end local v19    # "len$":I
    .end local v20    # "mappedIndex":Ljava/lang/Integer;
    .end local v21    # "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :catchall_0
    move-exception v24

    move-object v10, v11

    .end local v9    # "autolistId":[J
    .end local v11    # "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    .end local v16    # "factory":Lcom/google/android/music/store/AutoPlaylistCursorFactory;
    .end local v17    # "i":I
    .restart local v10    # "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    :goto_4
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 391
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 392
    if-nez v22, :cond_4

    .line 393
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :cond_4
    throw v24

    .line 373
    .end local v10    # "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v9    # "autolistId":[J
    .restart local v11    # "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    .restart local v15    # "columnName":Ljava/lang/String;
    .restart local v16    # "factory":Lcom/google/android/music/store/AutoPlaylistCursorFactory;
    .restart local v17    # "i":I
    .restart local v18    # "i$":I
    .restart local v19    # "len$":I
    .restart local v20    # "mappedIndex":Ljava/lang/Integer;
    .restart local v21    # "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_5
    :try_start_2
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move/from16 v0, v24

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 374
    const/16 v24, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 378
    :cond_6
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move/from16 v0, v24

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 379
    .local v23, "value":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 382
    .end local v15    # "columnName":Ljava/lang/String;
    .end local v20    # "mappedIndex":Ljava/lang/Integer;
    .end local v23    # "value":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 386
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v18    # "i$":I
    .end local v19    # "len$":I
    .end local v21    # "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_8
    const/16 v22, 0x1

    .line 390
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 391
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 392
    if-nez v22, :cond_9

    .line 393
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :cond_9
    return-object v11

    .line 390
    .end local v9    # "autolistId":[J
    .end local v11    # "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    .end local v16    # "factory":Lcom/google/android/music/store/AutoPlaylistCursorFactory;
    .end local v17    # "i":I
    .restart local v10    # "autoplaylistDownloadQueueCursor":Landroid/database/MatrixCursor;
    :catchall_1
    move-exception v24

    goto :goto_4
.end method

.method static getDownloadQueueContent(Landroid/content/Context;Lcom/google/android/music/store/Store;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "inColumns"    # [Ljava/lang/String;
    .param p3, "cancellationSignal"    # Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    .prologue
    .line 196
    const/16 v20, 0x0

    .line 197
    .local v20, "success":Z
    const/4 v7, 0x0

    .line 198
    .local v7, "albumCursorCompleted":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 199
    .local v8, "albumCursorNotCompleted":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 200
    .local v16, "playlistCursorCompleted":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 201
    .local v17, "playlistCursorNotCompleted":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 202
    .local v11, "autoPlaylistCursorCompleted":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 203
    .local v12, "autoPlaylistCursorNotCompleted":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 204
    .local v18, "radioCursorCompleted":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 205
    .local v19, "radioCursorNotCompleted":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 207
    .local v14, "downloadNotificationCursor":Landroid/database/MatrixCursor;
    const/4 v9, 0x0

    .line 208
    .local v9, "allContainersCursor":Lcom/google/android/music/store/CustomMergeCursor;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 214
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v1, "KEEPON.SongCount==KEEPON.DownloadedSongCount"

    const-string v3, "KEEPON.KeepOnId DESC "

    move-object/from16 v0, p2

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getAlbumsCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 217
    const-string v1, "KEEPON.SongCount!=KEEPON.DownloadedSongCount"

    const-string v3, "KEEPON.KeepOnId ASC "

    move-object/from16 v0, p2

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getAlbumsCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 221
    const-string v1, "KEEPON.SongCount==KEEPON.DownloadedSongCount"

    const-string v3, "KEEPON.KeepOnId DESC "

    move-object/from16 v0, p2

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getPlaylistsCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 224
    const-string v1, "KEEPON.SongCount!=KEEPON.DownloadedSongCount"

    const-string v3, "KEEPON.KeepOnId ASC "

    move-object/from16 v0, p2

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getPlaylistsCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 228
    const-string v1, "KEEPON.SongCount==KEEPON.DownloadedSongCount"

    const-string v3, "KEEPON.KeepOnId DESC "

    move-object/from16 v0, p2

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getRadiosCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 231
    const-string v1, "KEEPON.SongCount!=KEEPON.DownloadedSongCount"

    const-string v3, "KEEPON.KeepOnId ASC "

    move-object/from16 v0, p2

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getRadiosCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 235
    const-string v5, "KEEPON.SongCount==KEEPON.DownloadedSongCount"

    const-string v6, "KEEPON.KeepOnId DESC "

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getAutoPlaylistsCursor(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 238
    const-string v5, "KEEPON.SongCount!=KEEPON.DownloadedSongCount"

    const-string v6, "KEEPON.KeepOnId ASC "

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getAutoPlaylistsCursor(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 242
    new-instance v13, Ljava/util/ArrayList;

    const/16 v1, 0x9

    invoke-direct {v13, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 245
    .local v13, "cursors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    if-eqz v8, :cond_0

    .line 246
    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    :cond_0
    if-eqz v17, :cond_1

    .line 249
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    :cond_1
    if-eqz v19, :cond_2

    .line 252
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_2
    if-eqz v12, :cond_3

    .line 255
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    :cond_3
    if-eqz v7, :cond_4

    .line 260
    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    :cond_4
    if-eqz v16, :cond_5

    .line 263
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    :cond_5
    if-eqz v18, :cond_6

    .line 266
    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_6
    if-eqz v11, :cond_7

    .line 269
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    :cond_7
    new-instance v15, Landroid/database/MatrixCursor;

    move-object/from16 v0, p2

    invoke-direct {v15, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    .end local v14    # "downloadNotificationCursor":Landroid/database/MatrixCursor;
    .local v15, "downloadNotificationCursor":Landroid/database/MatrixCursor;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/google/android/music/store/MusicContent;->DOWNLOAD_QUEUE_URI:Landroid/net/Uri;

    invoke-virtual {v15, v1, v3}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 281
    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    new-instance v10, Lcom/google/android/music/store/CustomMergeCursor;

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/database/Cursor;

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/database/Cursor;

    invoke-direct {v10, v1}, Lcom/google/android/music/store/CustomMergeCursor;-><init>([Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 285
    .end local v9    # "allContainersCursor":Lcom/google/android/music/store/CustomMergeCursor;
    .local v10, "allContainersCursor":Lcom/google/android/music/store/CustomMergeCursor;
    const/16 v20, 0x1

    .line 288
    if-nez v20, :cond_9

    .line 292
    if-nez v10, :cond_8

    .line 293
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 294
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 295
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 296
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 297
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 298
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 299
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 300
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 301
    invoke-static {v15}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 303
    :cond_8
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 306
    :cond_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v10

    .line 288
    .end local v10    # "allContainersCursor":Lcom/google/android/music/store/CustomMergeCursor;
    .end local v13    # "cursors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    .end local v15    # "downloadNotificationCursor":Landroid/database/MatrixCursor;
    .restart local v9    # "allContainersCursor":Lcom/google/android/music/store/CustomMergeCursor;
    .restart local v14    # "downloadNotificationCursor":Landroid/database/MatrixCursor;
    :catchall_0
    move-exception v1

    :goto_0
    if-nez v20, :cond_b

    .line 292
    if-nez v9, :cond_a

    .line 293
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 294
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 295
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 296
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 297
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 298
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 299
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 300
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 301
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 303
    :cond_a
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 306
    :cond_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v1

    .line 288
    .end local v14    # "downloadNotificationCursor":Landroid/database/MatrixCursor;
    .restart local v13    # "cursors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    .restart local v15    # "downloadNotificationCursor":Landroid/database/MatrixCursor;
    :catchall_1
    move-exception v1

    move-object v14, v15

    .end local v15    # "downloadNotificationCursor":Landroid/database/MatrixCursor;
    .restart local v14    # "downloadNotificationCursor":Landroid/database/MatrixCursor;
    goto :goto_0
.end method

.method private static getPlaylistsCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "inColumns"    # [Ljava/lang/String;
    .param p2, "condition"    # Ljava/lang/String;
    .param p3, "order"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 158
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 160
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "MUSIC JOIN SHOULDKEEPON JOIN KEEPON JOIN LISTS ON (MUSIC.Id = SHOULDKEEPON.MusicId AND SHOULDKEEPON.KeepOnId = KEEPON.KeepOnId AND LISTS.Id = KEEPON.ListId)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 165
    sget-object v1, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sPlaylistDownloadQueueProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 167
    const-string v3, "KEEPON.ListId IS NOT NULL"

    .line 168
    .local v3, "where":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 172
    :cond_0
    const-string v5, "LISTS.Id"

    move-object v1, p0

    move-object v2, p1

    move-object v6, v4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private static getRadiosCursor(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "inColumns"    # [Ljava/lang/String;
    .param p2, "condition"    # Ljava/lang/String;
    .param p3, "order"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 177
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 179
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "MUSIC JOIN SHOULDKEEPON JOIN KEEPON JOIN RADIO_STATIONS ON (MUSIC.Id = SHOULDKEEPON.MusicId AND SHOULDKEEPON.KeepOnId = KEEPON.KeepOnId AND RADIO_STATIONS.Id = KEEPON.RadioStationId)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 184
    sget-object v1, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->sRadioDownloadQueueProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 185
    const-string v3, "KEEPON.RadioStationId IS NOT NULL"

    .line 186
    .local v3, "where":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 189
    :cond_0
    const-string v5, "RADIO_STATIONS.Id"

    move-object v1, p0

    move-object v2, p1

    move-object v6, v4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method
