.class public Lcom/google/android/music/dial/DialMediaRouteProvider;
.super Landroid/support/v7/media/MediaRouteProvider;
.source "DialMediaRouteProvider.java"


# static fields
.field private static final CONTROL_FILTERS_DIAL:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOGV:Z


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;

.field private final mSearchTarget:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v1

    sput-boolean v1, Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z

    .line 50
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 51
    .local v0, "dialFilter":Landroid/content/IntentFilter;
    const-string v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 53
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/dial/DialMediaRouteProvider;->CONTROL_FILTERS_DIAL:Ljava/util/ArrayList;

    .line 54
    sget-object v1, Lcom/google/android/music/dial/DialMediaRouteProvider;->CONTROL_FILTERS_DIAL:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchTarget"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProvider;-><init>(Landroid/content/Context;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mHandler:Landroid/os/Handler;

    .line 74
    iput-object p2, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mSearchTarget:Ljava/lang/String;

    .line 75
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 35
    sget-boolean v0, Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/dial/DialMediaRouteProvider;Ljava/lang/String;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialMediaRouteProvider;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/dial/DialMediaRouteProvider;->updateDescriptorWithVolumeChange(Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/dial/DialMediaRouteProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialMediaRouteProvider;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->publishRoutes()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/dial/DialMediaRouteProvider;)Lcom/google/android/music/dial/RemoteDeviceManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialMediaRouteProvider;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/dial/DialMediaRouteProvider;Lcom/google/android/music/dial/RemoteDeviceInfo;)Landroid/support/v7/media/MediaRouteDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialMediaRouteProvider;
    .param p1, "x1"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getRouteDescriptorForDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)Landroid/support/v7/media/MediaRouteDescriptor;

    move-result-object v0

    return-object v0
.end method

.method private clearRoutes()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialMediaRouteProvider$4;

    invoke-direct {v1, p0}, Lcom/google/android/music/dial/DialMediaRouteProvider$4;-><init>(Lcom/google/android/music/dial/DialMediaRouteProvider;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 261
    return-void
.end method

.method private createRemoteDeviceManager()V
    .locals 4

    .prologue
    .line 187
    iget-object v1, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;

    if-eqz v1, :cond_0

    .line 217
    :goto_0
    return-void

    .line 191
    :cond_0
    new-instance v0, Lcom/google/android/music/dial/DialMediaRouteProvider$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/dial/DialMediaRouteProvider$2;-><init>(Lcom/google/android/music/dial/DialMediaRouteProvider;)V

    .line 216
    .local v0, "listener":Lcom/google/android/music/dial/RemoteDeviceManager$Listener;
    new-instance v1, Lcom/google/android/music/dial/RemoteDeviceManager;

    invoke-virtual {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mSearchTarget:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/dial/RemoteDeviceManager$Listener;)V

    iput-object v1, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;

    goto :goto_0
.end method

.method private getRouteDescription(Lcom/google/android/music/dial/RemoteDeviceInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "deviceInfo"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 270
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    .line 277
    :goto_0
    return-object v0

    .line 272
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getModelDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 273
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getModelDescription()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 274
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getManufacturer()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 275
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getManufacturer()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 277
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private getRouteDescriptorForDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)Landroid/support/v7/media/MediaRouteDescriptor;
    .locals 10
    .param p1, "deviceInfo"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    const/4 v9, 0x1

    .line 284
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    .line 287
    .local v2, "deviceId":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getFriendlyName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " (Cloud Queue)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 289
    .local v4, "friendlyName":Ljava/lang/String;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 290
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v7, "com.google.android.music.dial.RemoteDevice"

    invoke-virtual {v0, v7, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 294
    const/4 v3, 0x0

    .line 295
    .local v3, "existingVolume":I
    invoke-virtual {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getDescriptor()Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v1

    .line 296
    .local v1, "descriptor":Landroid/support/v7/media/MediaRouteProviderDescriptor;
    if-eqz v1, :cond_1

    .line 297
    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouteProviderDescriptor;->getRoutes()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v7/media/MediaRouteDescriptor;

    .line 298
    .local v6, "route":Landroid/support/v7/media/MediaRouteDescriptor;
    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouteDescriptor;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 299
    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouteDescriptor;->getVolume()I

    move-result v3

    .line 305
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "route":Landroid/support/v7/media/MediaRouteDescriptor;
    :cond_1
    new-instance v7, Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    invoke-direct {v7, v2, v4}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getRouteDescription(Lcom/google/android/music/dial/RemoteDeviceInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setDescription(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setConnecting(Z)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setVolumeHandling(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setVolume(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    const/16 v8, 0x64

    invoke-virtual {v7, v8}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setVolumeMax(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setPlaybackType(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/dial/DialMediaRouteProvider;->CONTROL_FILTERS_DIAL:Ljava/util/ArrayList;

    invoke-virtual {v7, v8}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->addControlFilters(Ljava/util/Collection;)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setExtras(Landroid/os/Bundle;)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setEnabled(Z)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->build()Landroid/support/v7/media/MediaRouteDescriptor;

    move-result-object v7

    return-object v7
.end method

.method private static isConnectedToNetwork(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    .line 326
    const-string v8, "connectivity"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 329
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v5

    .line 331
    .local v5, "networkInfos":[Landroid/net/NetworkInfo;
    if-eqz v5, :cond_2

    array-length v8, v5

    if-lez v8, :cond_2

    .line 332
    move-object v0, v5

    .local v0, "arr$":[Landroid/net/NetworkInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v4, v0, v2

    .line 333
    .local v4, "networkInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    .line 337
    .local v6, "networkType":I
    if-eq v6, v7, :cond_0

    const/16 v8, 0x9

    if-ne v6, v8, :cond_1

    .line 339
    :cond_0
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 349
    .end local v0    # "arr$":[Landroid/net/NetworkInfo;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v6    # "networkType":I
    :goto_1
    return v7

    .line 332
    .restart local v0    # "arr$":[Landroid/net/NetworkInfo;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "networkInfo":Landroid/net/NetworkInfo;
    .restart local v6    # "networkType":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 345
    .end local v0    # "arr$":[Landroid/net/NetworkInfo;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v6    # "networkType":I
    :cond_2
    sget-boolean v7, Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z

    if-eqz v7, :cond_3

    .line 346
    const-string v7, "DialMediaRouteProvider"

    const-string v8, "No active network info records found."

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_3
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private publishRoutes()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/dial/DialMediaRouteProvider$3;

    invoke-direct {v1, p0}, Lcom/google/android/music/dial/DialMediaRouteProvider$3;-><init>(Lcom/google/android/music/dial/DialMediaRouteProvider;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 247
    return-void
.end method

.method private updateDescriptorWithVolumeChange(Ljava/lang/String;IZ)V
    .locals 10
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "volume"    # I
    .param p3, "adjust"    # Z

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getDescriptor()Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v0

    .line 112
    .local v0, "descriptor":Landroid/support/v7/media/MediaRouteProviderDescriptor;
    if-nez v0, :cond_1

    .line 113
    sget-boolean v7, Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z

    if-eqz v7, :cond_0

    .line 114
    const-string v7, "DialMediaRouteProvider"

    const-string v8, "getDescriptor() returned null on volume change"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteProviderDescriptor;->getRoutes()Ljava/util/List;

    move-result-object v4

    .line 120
    .local v4, "routes":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/media/MediaRouteDescriptor;>;"
    const/4 v5, 0x0

    .line 121
    .local v5, "targetRoute":Landroid/support/v7/media/MediaRouteDescriptor;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/media/MediaRouteDescriptor;

    .line 122
    .local v3, "route":Landroid/support/v7/media/MediaRouteDescriptor;
    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouteDescriptor;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 123
    move-object v5, v3

    .line 128
    .end local v3    # "route":Landroid/support/v7/media/MediaRouteDescriptor;
    :cond_3
    if-nez v5, :cond_4

    .line 129
    sget-boolean v7, Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z

    if-eqz v7, :cond_0

    .line 130
    const-string v7, "DialMediaRouteProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not find route on volume change: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 135
    :cond_4
    move v2, p2

    .line 136
    .local v2, "newVolume":I
    if-eqz p3, :cond_5

    .line 137
    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouteDescriptor;->getVolume()I

    move-result v7

    add-int/2addr v2, v7

    .line 139
    :cond_5
    const/4 v7, 0x0

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 140
    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouteDescriptor;->getVolumeMax()I

    move-result v7

    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 142
    new-instance v7, Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    invoke-direct {v7, v5}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;-><init>(Landroid/support/v7/media/MediaRouteDescriptor;)V

    invoke-virtual {v7, v2}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setVolume(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->build()Landroid/support/v7/media/MediaRouteDescriptor;

    move-result-object v6

    .line 146
    .local v6, "updatedTargetRoute":Landroid/support/v7/media/MediaRouteDescriptor;
    invoke-interface {v4, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 147
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v7, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;

    invoke-direct {v7, v0}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;-><init>(Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    invoke-virtual {v7}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;->build()Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/music/dial/DialMediaRouteProvider;->setDescriptor(Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateRouteController(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteProvider$RouteController;
    .locals 1
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/music/dial/DialMediaRouteProvider$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/dial/DialMediaRouteProvider$1;-><init>(Lcom/google/android/music/dial/DialMediaRouteProvider;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDiscoveryRequestChanged(Landroid/support/v7/media/MediaRouteDiscoveryRequest;)V
    .locals 7
    .param p1, "request"    # Landroid/support/v7/media/MediaRouteDiscoveryRequest;

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->createRemoteDeviceManager()V

    .line 162
    const/4 v4, 0x0

    .line 163
    .local v4, "shouldScan":Z
    invoke-virtual {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/dial/DialMediaRouteProvider;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v3

    .line 165
    .local v3, "isConnected":Z
    if-eqz p1, :cond_1

    if-eqz v3, :cond_1

    .line 166
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouteDiscoveryRequest;->getSelector()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouteSelector;->getControlCategories()Ljava/util/List;

    move-result-object v0

    .line 168
    .local v0, "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 169
    .local v1, "category":Ljava/lang/String;
    const-string v5, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 170
    const/4 v4, 0x1

    .line 176
    .end local v0    # "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "category":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    if-eqz v4, :cond_3

    .line 177
    iget-object v5, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;

    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouteDiscoveryRequest;->isActiveScan()Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/music/dial/RemoteDeviceManager;->startScan(Z)V

    .line 184
    :cond_2
    :goto_0
    return-void

    .line 179
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/dial/DialMediaRouteProvider;->mRemoteDeviceManager:Lcom/google/android/music/dial/RemoteDeviceManager;

    invoke-virtual {v5}, Lcom/google/android/music/dial/RemoteDeviceManager;->stopScan()V

    .line 180
    if-nez v3, :cond_2

    .line 181
    invoke-direct {p0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->clearRoutes()V

    goto :goto_0
.end method
