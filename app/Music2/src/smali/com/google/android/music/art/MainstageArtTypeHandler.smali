.class public Lcom/google/android/music/art/MainstageArtTypeHandler;
.super Lcom/google/android/music/art/ArtTypeHandler;
.source "MainstageArtTypeHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/MainstageArtTypeHandler$1;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-boolean v0, Lcom/google/android/music/art/ArtTypeHandler;->LOGV:Z

    sput-boolean v0, Lcom/google/android/music/art/MainstageArtTypeHandler;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/art/ArtResolver2;Landroid/content/Context;)V
    .locals 0
    .param p1, "resolver"    # Lcom/google/android/music/art/ArtResolver2;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtTypeHandler;-><init>(Lcom/google/android/music/art/ArtResolver2;Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method private lookupPlaylistRadioFallbackId(Ljava/lang/String;)J
    .locals 10
    .param p1, "seedSourceId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 133
    const-wide/16 v8, -0x1

    .line 134
    .local v8, "playlistId":J
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "playlist_id"

    aput-object v0, v2, v3

    .line 138
    .local v2, "listCols":[Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUriByRemoteSourceId(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 140
    .local v1, "listUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 142
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/MainstageArtTypeHandler;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 143
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 147
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 149
    return-wide v8

    .line 147
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public fillNeededItemsImpl(Lcom/google/android/music/art/ArtRequest2;)V
    .locals 22
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 39
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v5

    instance-of v5, v5, Lcom/google/android/music/art/DocumentArtDescriptor;

    invoke-static {v5}, Lcom/google/android/common/base/Preconditions;->checkArgument(Z)V

    .line 40
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v18

    check-cast v18, Lcom/google/android/music/art/DocumentArtDescriptor;

    .line 41
    .local v18, "descriptor":Lcom/google/android/music/art/DocumentArtDescriptor;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/music/art/DocumentArtDescriptor;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v19, v0

    .line 43
    .local v19, "document":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v20

    .line 44
    .local v20, "documentUrl":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 45
    sget-object v5, Lcom/google/android/music/art/MainstageArtTypeHandler$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 66
    new-instance v17, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    const/4 v5, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 67
    .local v17, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtRequest2;->addNeededItem(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 130
    .end local v17    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :cond_0
    :goto_0
    return-void

    .line 47
    :pswitch_0
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedType()I

    move-result v21

    .line 48
    .local v21, "seedSourceType":I
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedId()Ljava/lang/String;

    move-result-object v10

    .line 50
    .local v10, "seedSourceId":Ljava/lang/String;
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/music/art/MainstageArtTypeHandler;->addMultiUrlStringItems(Lcom/google/android/music/art/ArtRequest2;Ljava/lang/String;Z)I

    move-result v4

    .line 51
    .local v4, "addedCount":I
    const/16 v5, 0x8

    move/from16 v0, v21

    if-ne v0, v5, :cond_0

    if-nez v4, :cond_0

    .line 53
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/music/art/MainstageArtTypeHandler;->lookupPlaylistRadioFallbackId(Ljava/lang/String;)J

    move-result-wide v8

    .line 54
    .local v8, "playlistId":J
    sget-object v7, Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;->USER:Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/music/art/MainstageArtTypeHandler;->playlistLookup(Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;JLjava/lang/String;)V

    goto :goto_0

    .line 61
    .end local v4    # "addedCount":I
    .end local v8    # "playlistId":J
    .end local v10    # "seedSourceId":Ljava/lang/String;
    .end local v21    # "seedSourceType":I
    :pswitch_1
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/music/art/MainstageArtTypeHandler;->addMultiUrlStringItems(Lcom/google/android/music/art/ArtRequest2;Ljava/lang/String;Z)I

    goto :goto_0

    .line 71
    :cond_1
    sget-object v5, Lcom/google/android/music/art/MainstageArtTypeHandler$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    .line 82
    :pswitch_3
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/music/art/MainstageArtTypeHandler;->localRadioLookup(Lcom/google/android/music/art/ArtRequest2;J)V

    goto :goto_0

    .line 73
    :pswitch_4
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/music/art/MainstageArtTypeHandler;->addArtByAlbumId(Lcom/google/android/music/art/ArtRequest2;J)V

    goto :goto_0

    .line 76
    :pswitch_5
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/music/art/MainstageArtTypeHandler;->addArtByAlbumId(Lcom/google/android/music/art/ArtRequest2;J)V

    goto :goto_0

    .line 79
    :pswitch_6
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v7, v5}, Lcom/google/android/music/art/MainstageArtTypeHandler;->addArtByArtistId(Lcom/google/android/music/art/ArtRequest2;JLjava/lang/String;)V

    goto :goto_0

    .line 85
    :pswitch_7
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/music/art/MainstageArtTypeHandler;->genreLookup(Lcom/google/android/music/art/ArtRequest2;J)V

    goto/16 :goto_0

    .line 90
    :pswitch_8
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v8

    .line 91
    .restart local v8    # "playlistId":J
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareToken()Ljava/lang/String;

    move-result-object v16

    .line 92
    .local v16, "extId":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 119
    const-string v5, "ArtTypeHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported playlist type="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", no art"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 94
    :sswitch_0
    sget-object v13, Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;->SHARED_WITH_ME:Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-wide v14, v8

    invoke-virtual/range {v11 .. v16}, Lcom/google/android/music/art/MainstageArtTypeHandler;->playlistLookup(Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 99
    :sswitch_1
    sget-object v13, Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;->USER:Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-wide v14, v8

    invoke-virtual/range {v11 .. v16}, Lcom/google/android/music/art/MainstageArtTypeHandler;->playlistLookup(Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 104
    :sswitch_2
    sget-object v13, Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;->AUTOPLAYLIST:Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-wide v14, v8

    invoke-virtual/range {v11 .. v16}, Lcom/google/android/music/art/MainstageArtTypeHandler;->playlistLookup(Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 71
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 92
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x32 -> :sswitch_1
        0x47 -> :sswitch_0
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public getPostProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/music/art/MainstageCardPostProcessor;->getInstance()Lcom/google/android/music/art/MainstageCardPostProcessor;

    move-result-object v0

    return-object v0
.end method
