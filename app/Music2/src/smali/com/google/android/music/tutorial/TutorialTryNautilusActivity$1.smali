.class Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;
.super Ljava/lang/Object;
.source "TutorialTryNautilusActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$000(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPagerAdapter:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;
    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$100(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I
    invoke-static {v0, v1}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$002(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;I)I

    .line 118
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$200(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I
    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$000(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 119
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$400(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$300(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 120
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # operator++ for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mCurrentPosition:I
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$008(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)I

    goto :goto_0
.end method
