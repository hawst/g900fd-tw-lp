.class public Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;
.super Lcom/google/api/client/json/GenericJson;
.source "SetGroupVolumeCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SetGroupVolumeCommandBody"
.end annotation


# instance fields
.field public mGroupId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groupId"
    .end annotation
.end field

.field public mVolume:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "volume"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
