.class Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Key"
.end annotation


# instance fields
.field mConfig:Landroid/graphics/Bitmap$Config;

.field mHeight:I

.field mWidth:I


# direct methods
.method constructor <init>(IILandroid/graphics/Bitmap$Config;)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    iput p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mWidth:I

    .line 253
    iput p2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mHeight:I

    .line 254
    if-nez p3, :cond_0

    .line 255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "config must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    iput-object p3, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mConfig:Landroid/graphics/Bitmap$Config;

    .line 258
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 267
    instance-of v2, p1, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 268
    check-cast v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;

    .line 269
    .local v0, "key":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    iget v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mWidth:I

    iget v3, v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mWidth:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mHeight:I

    iget v3, v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mHeight:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mConfig:Landroid/graphics/Bitmap$Config;

    iget-object v3, v0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap$Config;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 272
    .end local v0    # "key":Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 262
    iget v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mWidth:I

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mHeight:I

    mul-int/lit8 v1, v1, 0xb

    xor-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1}, Landroid/graphics/Bitmap$Config;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool$Key;->mConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
