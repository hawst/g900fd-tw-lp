.class Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;
.super Ljava/lang/Object;
.source "VoiceActionHelper.java"

# interfaces
.implements Lcom/google/android/music/utils/MusicUtils$QueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/VoiceActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryCallback"
.end annotation


# instance fields
.field currentCategoryIndex:I

.field final eyesFree:Z

.field mCallback:Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;

.field query:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/utils/VoiceActionHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/music/utils/VoiceActionHelper;Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V
    .locals 0
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "index"    # I
    .param p4, "callback"    # Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;
    .param p5, "eyesFree"    # Z

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p4, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->mCallback:Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;

    .line 176
    iput p3, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->currentCategoryIndex:I

    .line 177
    iput-object p2, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->query:Ljava/lang/String;

    .line 178
    iput-boolean p5, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    .line 179
    return-void
.end method


# virtual methods
.method public onQueryComplete(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "data"    # Landroid/database/Cursor;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mIsCancelled:Z
    invoke-static {v0}, Lcom/google/android/music/utils/VoiceActionHelper;->access$000(Lcom/google/android/music/utils/VoiceActionHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 484
    :goto_0
    return-void

    .line 186
    :cond_0
    new-instance v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;Landroid/database/Cursor;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0
.end method
