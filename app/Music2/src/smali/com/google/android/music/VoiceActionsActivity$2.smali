.class Lcom/google/android/music/VoiceActionsActivity$2;
.super Ljava/lang/Object;
.source "VoiceActionsActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/VoiceActionsActivity;->playIFLRadio()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field hasServerAudio:Z

.field final synthetic this$0:Lcom/google/android/music/VoiceActionsActivity;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$hasNautilus:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/VoiceActionsActivity;ZLandroid/content/Context;)V
    .locals 1

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/music/VoiceActionsActivity$2;->this$0:Lcom/google/android/music/VoiceActionsActivity;

    iput-boolean p2, p0, Lcom/google/android/music/VoiceActionsActivity$2;->val$hasNautilus:Z

    iput-object p3, p0, Lcom/google/android/music/VoiceActionsActivity$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/VoiceActionsActivity$2;->hasServerAudio:Z

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/google/android/music/VoiceActionsActivity$2;->val$hasNautilus:Z

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/music/VoiceActionsActivity$2;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$XAudio;->hasServerAudio(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/VoiceActionsActivity$2;->hasServerAudio:Z

    .line 165
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/music/VoiceActionsActivity$2;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/VoiceActionsActivity$2$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/VoiceActionsActivity$2$1;-><init>(Lcom/google/android/music/VoiceActionsActivity$2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->addRunOnPlaybackServiceConnected(Ljava/lang/Runnable;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/music/VoiceActionsActivity$2;->this$0:Lcom/google/android/music/VoiceActionsActivity;

    invoke-virtual {v0}, Lcom/google/android/music/VoiceActionsActivity;->finish()V

    .line 187
    return-void
.end method
