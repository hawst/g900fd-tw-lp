.class Lcom/google/android/music/leanback/LeanbackPlayActivity$5;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnActionClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;->setupFragment(Landroid/app/Fragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

.field final synthetic val$nextAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;

.field final synthetic val$prevAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->val$prevAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->val$nextAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionClicked(Landroid/support/v17/leanback/widget/Action;)V
    .locals 8
    .param p1, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 314
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 315
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    if-nez v1, :cond_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->isPreparing()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->isStreaming()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 318
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 319
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->stop()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1100(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v1

    invoke-virtual {v1, v6, v6}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    goto :goto_0

    .line 320
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 321
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 322
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->pause()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 327
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 324
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 325
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->play()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 331
    :cond_4
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->val$prevAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_5

    .line 332
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 334
    :try_start_3
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->prev()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 335
    :catch_1
    move-exception v0

    .line 336
    .restart local v0    # "e":Landroid/os/RemoteException;
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 338
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_5
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->val$nextAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_6

    .line 339
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 341
    :try_start_4
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->next()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 342
    :catch_2
    move-exception v0

    .line 343
    .restart local v0    # "e":Landroid/os/RemoteException;
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 345
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1200(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_7

    .line 346
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->toggleShuffle()V
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    goto/16 :goto_0

    .line 347
    :cond_7
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1400(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_8

    .line 348
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->cycleRepeat()V
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1500(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    goto/16 :goto_0

    .line 349
    :cond_8
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1600(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/IntentAction;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1600(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/IntentAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/leanback/IntentAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_9

    .line 350
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    const v3, 0x7f0b0372

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;
    invoke-static {v5}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1600(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/IntentAction;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/leanback/IntentAction;->getLabel1()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 353
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1600(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/IntentAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/leanback/IntentAction;->getIntent()Landroid/content/Intent;

    move-result-object v2

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->playMusic(Landroid/content/Intent;)V
    invoke-static {v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$000(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 354
    :cond_9
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1700(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_a

    .line 355
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->changeRating(I)V
    invoke-static {v1, v6}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1800(Lcom/google/android/music/leanback/LeanbackPlayActivity;I)V

    goto/16 :goto_0

    .line 356
    :cond_a
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/Action;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1900(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 357
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    const/4 v2, 0x5

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->changeRating(I)V
    invoke-static {v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1800(Lcom/google/android/music/leanback/LeanbackPlayActivity;I)V

    goto/16 :goto_0
.end method
