.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->loadExtraData(Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

.field final synthetic val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 551
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$500(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    .line 552
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$1900(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    .line 553
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlayTopSongsAdded:Z
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$2000(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    # getter for: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$2100(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    # setter for: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlayTopSongsAdded:Z
    invoke-static {v0, v2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$2002(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Z)Z

    .line 555
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->val$detailsOverviewRow:Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    iget-object v1, v1, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createPlayTopSongsAction()Landroid/support/v17/leanback/widget/Action;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$2200(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)Landroid/support/v17/leanback/widget/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 556
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->access$2300(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V

    .line 558
    :cond_0
    return-void
.end method
