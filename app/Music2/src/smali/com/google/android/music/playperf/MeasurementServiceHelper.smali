.class public Lcom/google/android/music/playperf/MeasurementServiceHelper;
.super Ljava/lang/Object;
.source "MeasurementServiceHelper.java"


# direct methods
.method public static destroy(Landroid/content/Context;)V
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-static {}, Lcom/google/android/music/utils/SystemUtils;->isTestDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 42
    :cond_0
    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-static {}, Lcom/google/android/music/utils/SystemUtils;->isTestDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const-string v0, "MUSIC_UI_MEMORY_STATISTICS"

    new-instance v1, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;

    invoke-direct {v1}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->registerMeasurerFactory(Ljava/lang/String;Lcom/google/android/play/playperf/measurements/MeasurerFactory;)V

    .line 32
    const-string v0, "MUSIC_UI_MEMORY_MEASURER"

    new-instance v1, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurerFactory;

    invoke-direct {v1}, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurerFactory;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->registerMeasurerFactory(Ljava/lang/String;Lcom/google/android/play/playperf/measurements/MeasurerFactory;)V

    .line 35
    :cond_0
    return-void
.end method
