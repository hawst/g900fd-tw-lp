.class public Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSoundSearchPlaylistRequestJson.java"


# instance fields
.field public mNumResults:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "num-results"
    .end annotation
.end field

.field public mStartToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "start-token"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static serialize(Ljava/lang/String;I)[B
    .locals 2
    .param p0, "startToken"    # Ljava/lang/String;
    .param p1, "numResults"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistRequestJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistRequestJson;-><init>()V

    .line 19
    .local v0, "request":Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistRequestJson;
    iput-object p0, v0, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistRequestJson;->mStartToken:Ljava/lang/String;

    .line 20
    iput p1, v0, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistRequestJson;->mNumResults:I

    .line 21
    invoke-static {v0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v1

    return-object v1
.end method
