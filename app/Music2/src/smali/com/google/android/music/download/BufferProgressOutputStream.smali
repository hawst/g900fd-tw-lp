.class public Lcom/google/android/music/download/BufferProgressOutputStream;
.super Ljava/io/OutputStream;
.source "BufferProgressOutputStream.java"


# instance fields
.field private final LOGV:Z

.field private final mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

.field private final mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

.field private final mDownloadState:Lcom/google/android/music/download/DownloadState;

.field private mNextNotification:J

.field private mNextNotificationBytes:J

.field private final mNotificationByteGap:J

.field private final mOut:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/IDownloadProgressListener;Ljava/io/OutputStream;Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)V
    .locals 4
    .param p1, "downloadProgressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p2, "out"    # Ljava/io/OutputStream;
    .param p3, "downloadRequest"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p4, "downloadState"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 19
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->LOGV:Z

    .line 38
    iput-object p1, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    .line 39
    iput-object p2, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mOut:Ljava/io/OutputStream;

    .line 40
    iput-object p3, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    .line 41
    iput-object p4, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNextNotification:J

    .line 43
    iget-object v0, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v0}, Lcom/google/android/music/download/DownloadState;->getDownloadByteLength()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNotificationByteGap:J

    .line 44
    iget-wide v0, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNotificationByteGap:J

    iput-wide v0, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNextNotificationBytes:J

    .line 45
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v1, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mOut:Ljava/io/OutputStream;

    instance-of v1, v1, Ljava/io/FileOutputStream;

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mOut:Ljava/io/OutputStream;

    check-cast v1, Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V

    .line 86
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mOut:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_1
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/io/IOException;
    iget-boolean v1, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->LOGV:Z

    if-eqz v1, :cond_1

    .line 89
    const-string v1, "BufferProgress"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to close output stream:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mOut:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 97
    return-void
.end method

.method public sendBufferProgress()V
    .locals 8

    .prologue
    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    new-instance v2, Lcom/google/android/music/download/TrackDownloadProgress;

    iget-object v3, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    iget-object v4, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    iget-object v5, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v5}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/google/android/music/download/TrackDownloadProgress;-><init>(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;J)V

    invoke-interface {v1, v2}, Lcom/google/android/music/download/IDownloadProgressListener;->onDownloadProgress(Lcom/google/android/music/download/DownloadProgress;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNextNotification:J

    .line 77
    iget-object v1, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v1}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNotificationByteGap:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNextNotificationBytes:J

    .line 78
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BufferProgress"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to update progress for request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public write(I)V
    .locals 1
    .param p1, "a"    # I

    .prologue
    .line 49
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public write([B)V
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/download/BufferProgressOutputStream;->write([BII)V

    .line 55
    return-void
.end method

.method public write([BII)V
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v2, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mOut:Ljava/io/OutputStream;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 61
    iget-object v2, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    sub-int v3, p3, p2

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/google/android/music/download/DownloadState;->incrementCompletedBytes(J)J

    move-result-wide v0

    .line 63
    .local v0, "completedLength":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNextNotification:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/music/download/BufferProgressOutputStream;->mNextNotificationBytes:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/download/BufferProgressOutputStream;->sendBufferProgress()V

    .line 67
    :cond_1
    return-void
.end method
