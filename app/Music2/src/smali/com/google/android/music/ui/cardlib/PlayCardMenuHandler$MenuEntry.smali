.class public abstract Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
.super Ljava/lang/Object;
.source "PlayCardMenuHandler.java"

# interfaces
.implements Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MenuEntry"
.end annotation


# instance fields
.field public final isEnabled:Z

.field public final menuId:I

.field public final menuTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 47
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;Z)V

    .line 48
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "enabled"    # Z

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput p1, p0, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->menuId:I

    .line 52
    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->menuTitle:Ljava/lang/String;

    .line 53
    iput-boolean p3, p0, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->isEnabled:Z

    .line 54
    return-void
.end method


# virtual methods
.method public abstract shouldRunAsync()Z
.end method
