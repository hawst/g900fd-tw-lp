.class final enum Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
.super Ljava/lang/Enum;
.source "DialRemotePlaybackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/DialRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PlaybackState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

.field public static final enum BUFFERING:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

.field public static final enum ERROR_CLOUD_QUEUE_SERVER:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

.field public static final enum ERROR_PLAYBACK_FAILED:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

.field public static final enum IDLE:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

.field public static final enum PAUSED:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

.field public static final enum PLAYING:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 84
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    const-string v1, "IDLE"

    const-string v2, "PLAYBACK_STATE_IDLE"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->IDLE:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    .line 85
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    const-string v1, "BUFFERING"

    const-string v2, "PLAYBACK_STATE_BUFFERING"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->BUFFERING:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    .line 86
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    const-string v1, "PAUSED"

    const-string v2, "PLAYBACK_STATE_PAUSED"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->PAUSED:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    .line 87
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    const-string v1, "PLAYING"

    const-string v2, "PLAYBACK_STATE_PLAYING"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->PLAYING:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    .line 88
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    const-string v1, "ERROR_PLAYBACK_FAILED"

    const-string v2, "ERROR_PLAYBACK_FAILED"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->ERROR_PLAYBACK_FAILED:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    .line 89
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    const-string v1, "ERROR_CLOUD_QUEUE_SERVER"

    const/4 v2, 0x5

    const-string v3, "ERROR_CLOUD_QUEUE_SERVER"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->ERROR_CLOUD_QUEUE_SERVER:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    .line 83
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->IDLE:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->BUFFERING:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->PAUSED:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->PLAYING:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->ERROR_PLAYBACK_FAILED:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->ERROR_CLOUD_QUEUE_SERVER:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->$VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "v"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 94
    iput-object p3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->value:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    .locals 7
    .param p0, "v"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-static {}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->values()[Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 99
    .local v3, "p":Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    iget-object v4, v3, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    return-object v3

    .line 98
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v3    # "p":Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    const-class v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->$VALUES:[Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    invoke-virtual {v0}, [Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    return-object v0
.end method
