.class public Lcom/google/android/music/quiz/GenreQuizActivity;
.super Lcom/google/android/music/quiz/QuizActivity;
.source "GenreQuizActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;,
        Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;,
        Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;
    }
.end annotation


# instance fields
.field private mGenresList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            ">;"
        }
    .end annotation
.end field

.field private mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

.field private mSelectedGenreIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSourceAdapter:Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;

.field private mWrappedAdapter:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/music/quiz/QuizActivity;-><init>()V

    .line 230
    new-instance v0, Lcom/google/android/music/quiz/GenreQuizActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/quiz/GenreQuizActivity$3;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;)V

    iput-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/quiz/GenreQuizActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/GenreQuizActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/music/quiz/GenreQuizActivity;->startArtistQuiz()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/cloudclient/QuizGenresResponseJson;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/quiz/GenreQuizActivity;
    .param p1, "x1"    # Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/GenreQuizActivity;->initAdapter(Lcom/google/android/music/cloudclient/QuizGenresResponseJson;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/GenreQuizActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mGenresList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/quiz/GenreQuizActivity;)Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/GenreQuizActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mOnCheckedChangeListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/quiz/GenreQuizActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;

    return-object v0
.end method

.method private fetchGenreList()V
    .locals 2

    .prologue
    .line 99
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/music/quiz/GenreQuizActivity;->setLoadingProgressVisibility(I)V

    .line 100
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 102
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    new-instance v1, Lcom/google/android/music/quiz/GenreQuizActivity$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/quiz/GenreQuizActivity$2;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/cloudclient/MusicCloud;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 134
    return-void
.end method

.method private initAdapter(Lcom/google/android/music/cloudclient/QuizGenresResponseJson;)V
    .locals 3
    .param p1, "response"    # Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    .prologue
    .line 138
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/google/android/music/cloudclient/QuizGenresResponseJson;->mGenres:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 139
    iget-object v1, p1, Lcom/google/android/music/cloudclient/QuizGenresResponseJson;->mGenres:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mGenresList:Ljava/util/List;

    .line 144
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;

    if-nez v1, :cond_0

    .line 145
    new-instance v1, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/quiz/GenreQuizActivity$1;)V

    iput-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;

    .line 146
    invoke-virtual {p0}, Lcom/google/android/music/quiz/GenreQuizActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 147
    .local v0, "numOfRows":I
    new-instance v1, Lcom/google/android/music/ui/GridAdapterWrapper;

    iget-object v2, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSourceAdapter:Lcom/google/android/music/quiz/GenreQuizActivity$GenreQuizAdapter;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/music/ui/GridAdapterWrapper;-><init>(Landroid/content/Context;Landroid/widget/ListAdapter;I)V

    iput-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mWrappedAdapter:Landroid/widget/ListAdapter;

    .line 150
    .end local v0    # "numOfRows":I
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/music/quiz/GenreQuizActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 152
    return-void

    .line 141
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mGenresList:Ljava/util/List;

    goto :goto_0
.end method

.method private startArtistQuiz()V
    .locals 2

    .prologue
    .line 155
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 156
    .local v0, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p0, v0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->startArtistQuiz(Landroid/app/Activity;Ljava/util/ArrayList;)V

    .line 157
    return-void
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/google/android/music/quiz/QuizActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/music/quiz/QuizActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v1, 0x7f050007

    const v2, 0x7f050008

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/quiz/GenreQuizActivity;->overridePendingTransition(II)V

    .line 52
    if-eqz p1, :cond_0

    .line 53
    const-string v1, "selected_genres"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 54
    .local v0, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;

    .line 59
    .end local v0    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mDescription:Landroid/widget/TextView;

    const v2, 0x7f0b033a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 60
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mNextButton:Landroid/widget/Button;

    const v2, 0x7f0b033c

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 61
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mNextButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/music/quiz/GenreQuizActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/quiz/GenreQuizActivity$1;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    invoke-direct {p0}, Lcom/google/android/music/quiz/GenreQuizActivity;->fetchGenreList()V

    .line 70
    return-void

    .line 56
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/google/android/music/quiz/QuizActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 90
    .local v0, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "selected_genres"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 91
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/android/music/quiz/QuizActivity;->onStart()V

    .line 75
    invoke-static {p0}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v0

    .line 76
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver;
    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver;->requestMemoryCaching()V

    .line 77
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 81
    invoke-static {p0}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v0

    .line 82
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver;
    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver;->releaseMemoryCaching()V

    .line 83
    invoke-super {p0}, Lcom/google/android/music/quiz/QuizActivity;->onStop()V

    .line 84
    return-void
.end method
