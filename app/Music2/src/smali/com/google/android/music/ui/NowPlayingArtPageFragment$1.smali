.class Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;
.super Ljava/lang/Object;
.source "NowPlayingArtPageFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingArtPageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->stopPanning()V
    invoke-static {v3}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$000(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 107
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v2

    .line 108
    .local v2, "state":Lcom/google/android/music/playback/PlaybackState;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v0

    .line 109
    .local v0, "isPlaying":Z
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v1

    .line 110
    .local v1, "isPreparing":Z
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z
    invoke-static {v3}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$100(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->isDrawerExpanded()Z
    invoke-static {v3}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$200(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 112
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mPanningLeftOrTop:Z
    invoke-static {v3}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$300(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 113
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->panLeftOrTop()V
    invoke-static {v3}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$400(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 118
    :cond_1
    :goto_1
    return-void

    .end local v0    # "isPlaying":Z
    .end local v1    # "isPreparing":Z
    :cond_2
    move v0, v1

    .line 108
    goto :goto_0

    .line 115
    .restart local v0    # "isPlaying":Z
    .restart local v1    # "isPreparing":Z
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->panRightOrBottom()V
    invoke-static {v3}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$500(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    goto :goto_1
.end method
