.class public Lcom/google/android/music/icing/WriteTxnHandler;
.super Ljava/lang/Object;
.source "WriteTxnHandler.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Lcom/google/android/music/utils/LoggableHandler;

.field private final mListener:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotifyIcingTask:Ljava/lang/Runnable;

.field private mWriteTxnNum:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mListener:Ljava/util/concurrent/atomic/AtomicReference;

    .line 29
    new-instance v0, Lcom/google/android/music/icing/WriteTxnHandler$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/icing/WriteTxnHandler$1;-><init>(Lcom/google/android/music/icing/WriteTxnHandler;)V

    iput-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mNotifyIcingTask:Ljava/lang/Runnable;

    .line 46
    iput-object p1, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mContext:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler;

    const-string v1, "IcingNotificationHandlerThread"

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mHandler:Lcom/google/android/music/utils/LoggableHandler;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/icing/WriteTxnHandler;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/icing/WriteTxnHandler;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mListener:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/icing/WriteTxnHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/icing/WriteTxnHandler;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getNotificationDelayMs()J
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_icing_notification_delay_ms"

    sget-wide v2, Lcom/google/android/music/MusicGservicesKeys;->DEFAULT_MUSIC_ICING_NOTIFICATION_DELAY_MS:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public declared-synchronized onTxnBegin()V
    .locals 2

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mHandler:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mNotifyIcingTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 56
    iget v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mWriteTxnNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mWriteTxnNum:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onTxnEnd()V
    .locals 4

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mWriteTxnNum:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mWriteTxnNum:I

    .line 61
    iget v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mWriteTxnNum:I

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mHandler:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mNotifyIcingTask:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/android/music/icing/WriteTxnHandler;->getNotificationDelayMs()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/utils/LoggableHandler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :cond_0
    monitor-exit p0

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setTableChangeListener(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/icing/WriteTxnHandler;->mListener:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 52
    return-void
.end method
