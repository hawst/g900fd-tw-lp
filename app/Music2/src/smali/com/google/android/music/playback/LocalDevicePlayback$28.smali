.class Lcom/google/android/music/playback/LocalDevicePlayback$28;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->playNext(Lcom/google/android/music/medialist/SongList;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$queuePlayPos:I

.field final synthetic val$songList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;I)V
    .locals 0

    .prologue
    .line 3628
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->val$songList:Lcom/google/android/music/medialist/SongList;

    iput p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->val$queuePlayPos:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 3632
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->val$songList:Lcom/google/android/music/medialist/SongList;

    iget v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->val$queuePlayPos:I

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->playNextSync(Lcom/google/android/music/medialist/SongList;I)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3634
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 3636
    return-void

    .line 3634
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$28;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
