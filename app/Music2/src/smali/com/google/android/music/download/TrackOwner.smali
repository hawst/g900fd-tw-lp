.class public final enum Lcom/google/android/music/download/TrackOwner;
.super Ljava/lang/Enum;
.source "TrackOwner.java"

# interfaces
.implements Lcom/google/android/music/download/DownloadRequest$Owner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/TrackOwner$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/download/TrackOwner;",
        ">;",
        "Lcom/google/android/music/download/DownloadRequest$Owner;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/download/TrackOwner;

.field public static final enum AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

.field public static final enum KEEPON:Lcom/google/android/music/download/TrackOwner;

.field public static final enum MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/google/android/music/download/TrackOwner;

    const-string v1, "MUSIC_PLAYBACK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/download/TrackOwner;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/TrackOwner;->MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;

    .line 14
    new-instance v0, Lcom/google/android/music/download/TrackOwner;

    const-string v1, "KEEPON"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/download/TrackOwner;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/TrackOwner;->KEEPON:Lcom/google/android/music/download/TrackOwner;

    .line 19
    new-instance v0, Lcom/google/android/music/download/TrackOwner;

    const-string v1, "AUTOCACHE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/download/TrackOwner;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    .line 5
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/download/TrackOwner;

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->KEEPON:Lcom/google/android/music/download/TrackOwner;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/download/TrackOwner;->$VALUES:[Lcom/google/android/music/download/TrackOwner;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/download/TrackOwner;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/android/music/download/TrackOwner;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/TrackOwner;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/download/TrackOwner;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/android/music/download/TrackOwner;->$VALUES:[Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v0}, [Lcom/google/android/music/download/TrackOwner;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/download/TrackOwner;

    return-object v0
.end method


# virtual methods
.method public isAuto()Z
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/TrackOwner;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toFileSystemString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/download/TrackOwner$1;->$SwitchMap$com$google$android$music$download$TrackOwner:[I

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 34
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing file system string for owner: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :pswitch_0
    const-string v0, "mplayback"

    .line 32
    :goto_0
    return-object v0

    .line 30
    :pswitch_1
    const-string v0, "keepon"

    goto :goto_0

    .line 32
    :pswitch_2
    const-string v0, "autocache"

    goto :goto_0

    .line 26
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public toInt()I
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v0

    return v0
.end method
