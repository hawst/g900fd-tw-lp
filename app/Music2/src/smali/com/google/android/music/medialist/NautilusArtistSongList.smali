.class public Lcom/google/android/music/medialist/NautilusArtistSongList;
.super Lcom/google/android/music/medialist/NautilusSongList;
.source "NautilusArtistSongList.java"


# instance fields
.field private mArtistArtUrl:Ljava/lang/String;

.field private final mArtistName:Ljava/lang/String;

.field private final mNautilusArtistId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "nautilusArtistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/medialist/NautilusSongList;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mNautilusArtistId:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mArtistName:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mArtistArtUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mNautilusArtistId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/music/utils/MusicUtils;->getNautilusArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mArtistArtUrl:Ljava/lang/String;

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mArtistArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mNautilusArtistId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mArtistName:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v0

    .line 77
    .local v0, "artistId":J
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "extId":Ljava/lang/String;
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor;->newNautuilusArtistDescriptor(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    return-object v4
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mNautilusArtistId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getAudioByNautilusArtistUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getNautilusId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusArtistSongList;->mNautilusArtistId:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasDifferentTrackArtists(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public isAllInLibrary(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method
