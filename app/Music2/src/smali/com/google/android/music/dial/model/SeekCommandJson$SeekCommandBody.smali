.class public Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;
.super Lcom/google/api/client/json/GenericJson;
.source "SeekCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/model/SeekCommandJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SeekCommandBody"
.end annotation


# instance fields
.field public mItemId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemId"
    .end annotation
.end field

.field public mPositionMillis:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "positionMillis"
    .end annotation
.end field

.field public mSessionId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "sessionId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
