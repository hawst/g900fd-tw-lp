.class Lcom/google/android/music/store/SearchHandler$1;
.super Ljava/lang/Object;
.source "SearchHandler.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/store/SearchHandler;->getServerResults(Ljava/lang/String;I)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/android/music/cloudclient/SearchClientResponseJson;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/store/SearchHandler;

.field final synthetic val$numSearchResults:I

.field final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/store/SearchHandler;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/music/store/SearchHandler$1;->this$0:Lcom/google/android/music/store/SearchHandler;

    iput-object p2, p0, Lcom/google/android/music/store/SearchHandler$1;->val$query:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/music/store/SearchHandler$1;->val$numSearchResults:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 194
    iget-object v5, p0, Lcom/google/android/music/store/SearchHandler$1;->this$0:Lcom/google/android/music/store/SearchHandler;

    # getter for: Lcom/google/android/music/store/SearchHandler;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->access$000(Lcom/google/android/music/store/SearchHandler;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/music/store/SearchHandler$1;->val$query:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/google/android/music/store/NautilusContentCache;->getSearchResponse(Ljava/lang/String;)Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    move-result-object v1

    .line 196
    .local v1, "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    if-eqz v1, :cond_0

    move-object v4, v1

    .line 214
    .end local v1    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .local v4, "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    :goto_0
    return-object v4

    .line 200
    .end local v4    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .restart local v1    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    :cond_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    iget-object v5, p0, Lcom/google/android/music/store/SearchHandler$1;->this$0:Lcom/google/android/music/store/SearchHandler;

    # getter for: Lcom/google/android/music/store/SearchHandler;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->access$000(Lcom/google/android/music/store/SearchHandler;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 202
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 204
    .local v6, "start":J
    iget-object v5, p0, Lcom/google/android/music/store/SearchHandler$1;->val$query:Ljava/lang/String;

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/music/store/SearchHandler$1;->val$numSearchResults:I

    invoke-interface {v0, v5, v8, v9}, Lcom/google/android/music/cloudclient/MusicCloud;->search(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    move-result-object v1

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 208
    .local v2, "end":J
    # getter for: Lcom/google/android/music/store/SearchHandler;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/store/SearchHandler;->access$100()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 209
    const-string v5, "Search"

    const-string v8, "Search took: %d ms"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sub-long v12, v2, v6

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/store/SearchHandler$1;->this$0:Lcom/google/android/music/store/SearchHandler;

    # getter for: Lcom/google/android/music/store/SearchHandler;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/store/SearchHandler;->access$000(Lcom/google/android/music/store/SearchHandler;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/music/store/SearchHandler$1;->val$query:Ljava/lang/String;

    invoke-virtual {v5, v8, v1}, Lcom/google/android/music/store/NautilusContentCache;->putSearchResponse(Ljava/lang/String;Lcom/google/android/music/cloudclient/SearchClientResponseJson;)V

    move-object v4, v1

    .line 214
    .end local v1    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .restart local v4    # "response":Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/music/store/SearchHandler$1;->call()Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    move-result-object v0

    return-object v0
.end method
