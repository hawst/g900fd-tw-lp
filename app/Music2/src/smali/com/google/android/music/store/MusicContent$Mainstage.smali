.class public final Lcom/google/android/music/store/MusicContent$Mainstage;
.super Ljava/lang/Object;
.source "MusicContent.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/google/android/music/store/MusicContent$PlaylistColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Mainstage"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2623
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "Mainstage"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public static dismissAlbum(Landroid/content/ContentResolver;IILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "pos"    # I
    .param p2, "mainstageReason"    # I
    .param p3, "albumTitle"    # Ljava/lang/String;
    .param p4, "albumArtist"    # Ljava/lang/String;
    .param p5, "albumId"    # J
    .param p7, "albumMetajamId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 2823
    sget-object v1, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2824
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "reasonType"

    invoke-static {p2}, Lcom/google/android/music/store/MusicContent$Mainstage;->getBlacklistReasonType(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2828
    const-string v1, "dismissPos"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2830
    const-wide/16 v2, 0x0

    cmp-long v1, p5, v2

    if-ltz v1, :cond_0

    .line 2831
    const-string v1, "albumLocalId"

    invoke-static {p5, p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2834
    :cond_0
    if-eqz p7, :cond_1

    .line 2835
    const-string v1, "albumMetajamId"

    invoke-virtual {v0, v1, p7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2837
    :cond_1
    if-eqz p4, :cond_2

    .line 2838
    const-string v1, "albumArtist"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2840
    :cond_2
    if-eqz p3, :cond_3

    .line 2841
    const-string v1, "albumTitle"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2843
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    invoke-virtual {p0, v1, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2844
    return-void
.end method

.method public static dismissList(Landroid/content/ContentResolver;IIJLjava/lang/String;)V
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "pos"    # I
    .param p2, "mainstageReason"    # I
    .param p3, "listId"    # J
    .param p5, "listShareToken"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 2856
    sget-object v1, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2857
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "reasonType"

    invoke-static {p2}, Lcom/google/android/music/store/MusicContent$Mainstage;->getBlacklistReasonType(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2861
    const-string v1, "dismissPos"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2863
    const-string v1, "listLocalId"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2865
    if-eqz p5, :cond_0

    .line 2866
    const-string v1, "listShareToken"

    invoke-virtual {v0, v1, p5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2868
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    invoke-virtual {p0, v1, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2869
    return-void
.end method

.method public static dismissRadioStation(Landroid/content/ContentResolver;IILjava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "pos"    # I
    .param p2, "mainstageReason"    # I
    .param p3, "radioRemoteId"    # Ljava/lang/String;
    .param p4, "radioSeedId"    # Ljava/lang/String;
    .param p5, "radioSeedType"    # I

    .prologue
    const/4 v4, 0x0

    .line 2882
    sget-object v1, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2883
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "reasonType"

    invoke-static {p2}, Lcom/google/android/music/store/MusicContent$Mainstage;->getBlacklistReasonType(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2887
    const-string v1, "dismissPos"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2889
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2891
    const-string v1, "radioRemoteId"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2893
    :cond_0
    const-string v1, "radioSeedId"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2894
    const-string v1, "radioSeedType"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2896
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    invoke-virtual {p0, v1, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2897
    return-void
.end method

.method public static getBlacklistReasonType(I)I
    .locals 3
    .param p0, "mainstageReason"    # I

    .prologue
    .line 2767
    sparse-switch p0, :sswitch_data_0

    .line 2783
    const-string v0, "MusicContent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown mainstage reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 2784
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_UNKNOWN:I

    :goto_0
    return v0

    .line 2769
    :sswitch_0
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_SUGGESTED_NEW_RELEASE:I

    goto :goto_0

    .line 2771
    :sswitch_1
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_ADDED:I

    goto :goto_0

    .line 2773
    :sswitch_2
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_PLAYED:I

    goto :goto_0

    .line 2775
    :sswitch_3
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_PURCHASED:I

    goto :goto_0

    .line 2777
    :sswitch_4
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECOMMENDED_FOR_YOU_FROM_LOCKER:I

    goto :goto_0

    .line 2779
    :sswitch_5
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECOMMENDED_FOR_YOU_RADIO:I

    goto :goto_0

    .line 2781
    :sswitch_6
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_YOUTUBE:I

    goto :goto_0

    .line 2767
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_1
        0x4 -> :sswitch_0
        0x7 -> :sswitch_4
        0x8 -> :sswitch_6
        0x64 -> :sswitch_5
    .end sparse-switch
.end method

.method public static throwIfInvalidReason(I)V
    .locals 3
    .param p0, "mainstageReason"    # I

    .prologue
    .line 2793
    sparse-switch p0, :sswitch_data_0

    .line 2805
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting unknown mainstage reason to document: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2808
    :sswitch_0
    return-void

    .line 2793
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x7 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0x64 -> :sswitch_0
    .end sparse-switch
.end method

.method public static translateLockerRecommendationReason(I)I
    .locals 4
    .param p0, "lockerRecReason"    # I

    .prologue
    const/4 v0, 0x7

    .line 2912
    sget-object v1, Lcom/google/android/music/store/MusicContent$1;->$SwitchMap$com$google$android$music$cloudclient$ListenNowSuggestionReason$SuggestionReason:[I

    invoke-static {p0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->fromValue(I)Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2921
    const-string v1, "MusicContent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected recommendation reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2922
    :goto_0
    :pswitch_0
    return v0

    .line 2917
    :pswitch_1
    const/16 v0, 0x8

    goto :goto_0

    .line 2919
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 2912
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
