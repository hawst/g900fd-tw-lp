.class Lcom/google/android/music/art/ArtLoaderImpl$AsyncRequestRunnable;
.super Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
.source "ArtLoaderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtLoaderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncRequestRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtLoaderImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V
    .locals 0
    .param p2, "remoteArtUrl"    # Ljava/lang/String;
    .param p3, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .param p4, "listener"    # Lcom/google/android/music/art/ArtLoader$Listener;
    .param p5, "listenerHandler"    # Landroid/os/Handler;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$AsyncRequestRunnable;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    .line 232
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V

    .line 233
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/google/android/music/art/ArtLoaderImpl$AsyncRequestRunnable;->getFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/art/ArtLoaderImpl$AsyncRequestRunnable;->notifyListener(Landroid/os/ParcelFileDescriptor;)V

    .line 238
    return-void
.end method
