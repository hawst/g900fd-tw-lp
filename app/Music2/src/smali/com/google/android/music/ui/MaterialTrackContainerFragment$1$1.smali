.class Lcom/google/android/music/ui/MaterialTrackContainerFragment$1$1;
.super Ljava/lang/Object;
.source "MaterialTrackContainerFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;->onChange(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1$1;->this$1:Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1$1;->this$1:Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1$1;->this$1:Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    iget-object v1, v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;->val$appContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->refreshMetaData(Landroid/content/Context;)V

    .line 139
    return-void
.end method

.method public taskCompleted()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1$1;->this$1:Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1$1;->this$1:Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1$1;->this$1:Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    # invokes: Lcom/google/android/music/ui/MaterialTrackContainerFragment;->asyncPopulateContainerMetadata()V
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->access$000(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)V

    goto :goto_0
.end method
