.class public Lcom/google/android/music/ui/MediaListCursorLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "MediaListCursorLoader.java"


# instance fields
.field private final mMediaList:Lcom/google/android/music/medialist/MediaList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/music/ui/MediaListCursorLoader;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 27
    invoke-virtual {p0, p3}, Lcom/google/android/music/ui/MediaListCursorLoader;->setProjection([Ljava/lang/String;)V

    .line 28
    return-void
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 33
    iget-object v2, p0, Lcom/google/android/music/ui/MediaListCursorLoader;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/medialist/MediaList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    .line 34
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 35
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "MediaList must be content uri based"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 38
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/MediaListCursorLoader;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    instance-of v2, v2, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    if-eqz v2, :cond_1

    .line 39
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 40
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "isInCloudQueueMode"

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isInCloudQueueMode(Landroid/content/Context;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 43
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/MediaListCursorLoader;->setUri(Landroid/net/Uri;)V

    .line 48
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    :goto_0
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 45
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/MediaListCursorLoader;->setUri(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
