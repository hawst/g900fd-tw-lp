.class public Lcom/google/android/music/tutorial/TutorialFinishActivity;
.super Lcom/google/android/music/tutorial/TutorialActivity;
.source "TutorialFinishActivity.java"


# instance fields
.field private mMetajamIdDestination:Ljava/lang/String;

.field private mPlaylistShareTokenDestination:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/tutorial/TutorialFinishActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialFinishActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->launchQuiz()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/tutorial/TutorialFinishActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialFinishActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->launchHome()V

    return-void
.end method

.method private static finishTutorial(Landroid/app/Activity;ZZZ)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "turnOffTutorial"    # Z
    .param p2, "forceNatilus"    # Z
    .param p3, "launchQuiz"    # Z

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/tutorial/TutorialFinishActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 60
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 61
    const-string v1, "turnOffTutorial"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 62
    const-string v1, "forceNautilus"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 63
    const-string v1, "launchQuiz"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 64
    const-string v1, "metajamIdDestination"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "metajamIdDestination"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v1, "playlistShareTokenDestination"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "playlistShareTokenDestination"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method static final finishTutorialPermanently(Landroid/app/Activity;ZZ)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "forceNaulilus"    # Z
    .param p2, "launchQuiz"    # Z

    .prologue
    .line 53
    const/4 v0, 0x1

    invoke-static {p0, v0, p1, p2}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->finishTutorial(Landroid/app/Activity;ZZZ)V

    .line 54
    return-void
.end method

.method static final finishTutorialTemporary(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-static {p0, v0, v0, v0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->finishTutorial(Landroid/app/Activity;ZZZ)V

    .line 49
    return-void
.end method

.method private launchHome()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mMetajamIdDestination:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mPlaylistShareTokenDestination:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mMetajamIdDestination:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mPlaylistShareTokenDestination:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/music/ui/AppNavigation;->goListenNow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->finish()V

    .line 137
    return-void

    .line 134
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->goListenNow(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private launchQuiz()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mMetajamIdDestination:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mPlaylistShareTokenDestination:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mMetajamIdDestination:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mPlaylistShareTokenDestination:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/music/ui/AppNavigation;->startQuiz(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->finish()V

    .line 147
    return-void

    .line 144
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->startQuiz(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/google/android/music/tutorial/TutorialActivity;->isActivityDestroyed()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 73
    invoke-super {p0, p1}, Lcom/google/android/music/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "turnOffTutorial"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 78
    .local v2, "turnOffTutorial":Z
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "forceNautilus"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 81
    .local v4, "forceNautilus":Z
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "launchQuiz"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 82
    .local v5, "launchQuiz":Z
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "returnToCaller"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 83
    .local v3, "returnToCaller":Z
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "metajamIdDestination"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mMetajamIdDestination:Ljava/lang/String;

    .line 85
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "playlistShareTokenDestination"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialFinishActivity;->mPlaylistShareTokenDestination:Ljava/lang/String;

    .line 89
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    new-instance v0, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/tutorial/TutorialFinishActivity$1;-><init>(Lcom/google/android/music/tutorial/TutorialFinishActivity;ZZZZ)V

    invoke-virtual {v6, v0}, Lcom/google/android/music/preferences/MusicPreferences;->runWithPreferenceService(Ljava/lang/Runnable;)V

    .line 121
    return-void
.end method
