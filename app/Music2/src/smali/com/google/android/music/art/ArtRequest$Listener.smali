.class public interface abstract Lcom/google/android/music/art/ArtRequest$Listener;
.super Ljava/lang/Object;
.source "ArtRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onLoadError(Lcom/google/android/music/art/ArtRequest;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract onLoadFinished(Lcom/google/android/music/art/ArtRequest;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract showDefaultArt(Lcom/google/android/music/art/ArtRequest;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<TT;>;)V"
        }
    .end annotation
.end method
