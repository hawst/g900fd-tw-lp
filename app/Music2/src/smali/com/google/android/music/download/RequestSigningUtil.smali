.class public Lcom/google/android/music/download/RequestSigningUtil;
.super Ljava/lang/Object;
.source "RequestSigningUtil.java"


# static fields
.field private static final s1:[B

.field private static final s2:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    const-string v0, "VzeC4H4h+T2f0VI180nVX8x+Mb5HiTtGnKgH52Otj8ZCGDz9jRWyHb6QXK0JskSiOgzQfwTY5xgLLSdUSreaLVMsVVWfxfa8Rw=="

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/RequestSigningUtil;->s1:[B

    .line 25
    const-string v0, "ZAPnhUkYwQ6y5DdQxWThbvhJHN8msQ1rqJw0ggKdufQjelrKuiGGJI30aswkgCWTDyHkTGK9ynlqTkJ5L4CiGGUabGeo8M6JTQ=="

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/RequestSigningUtil;->s2:[B

    return-void
.end method

.method public static appendMplayUrlSignatureParams(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 10
    .param p0, "songId"    # Ljava/lang/String;
    .param p1, "url"    # Ljava/lang/StringBuilder;

    .prologue
    .line 42
    :try_start_0
    sget-object v8, Lcom/google/android/music/download/RequestSigningUtil;->s1:[B

    array-length v8, v8

    new-array v3, v8, [B

    .line 43
    .local v3, "key":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v8, Lcom/google/android/music/download/RequestSigningUtil;->s1:[B

    array-length v8, v8

    if-ge v2, v8, :cond_0

    .line 44
    sget-object v8, Lcom/google/android/music/download/RequestSigningUtil;->s1:[B

    aget-byte v8, v8, v2

    sget-object v9, Lcom/google/android/music/download/RequestSigningUtil;->s2:[B

    aget-byte v9, v9, v2

    xor-int/2addr v8, v9

    int-to-byte v8, v8

    aput-byte v8, v3, v2

    .line 43
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 49
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 52
    .local v5, "salt":Ljava/lang/String;
    const-string v8, "HmacSHA1"

    invoke-static {v8}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v4

    .line 54
    .local v4, "mac":Ljavax/crypto/Mac;
    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v4}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v3, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 55
    .local v6, "secret":Ljavax/crypto/spec/SecretKeySpec;
    invoke-virtual {v4, v6}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 56
    invoke-static {p0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v8

    invoke-virtual {v4, v8}, Ljavax/crypto/Mac;->update([B)V

    .line 57
    invoke-static {v5}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v8

    invoke-virtual {v4, v8}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    .line 58
    .local v0, "digest":[B
    const/16 v8, 0xb

    invoke-static {v0, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v7

    .line 60
    .local v7, "sig":Ljava/lang/String;
    const-string v8, "&slt="

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    const-string v8, "&sig="

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    return-void

    .line 62
    .end local v0    # "digest":[B
    .end local v2    # "i":I
    .end local v3    # "key":[B
    .end local v4    # "mac":Ljavax/crypto/Mac;
    .end local v5    # "salt":Ljava/lang/String;
    .end local v6    # "secret":Ljavax/crypto/spec/SecretKeySpec;
    .end local v7    # "sig":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/security/GeneralSecurityException;
    const-string v8, "RequestSigningUtil"

    const-string v9, "Url signing failed"

    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 64
    new-instance v8, Ljava/lang/RuntimeException;

    const-string v9, "Url signing failed"

    invoke-direct {v8, v9, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
.end method
