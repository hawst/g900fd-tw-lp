.class public final Lcom/google/android/music/download/cache/CacheLocation;
.super Ljava/lang/Object;
.source "CacheLocation.java"


# static fields
.field private static final UUID_LOCK:Ljava/lang/Object;


# instance fields
.field private final mPath:Ljava/io/File;

.field private final mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/download/cache/CacheLocation;->UUID_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V
    .locals 2
    .param p1, "path"    # Ljava/io/File;
    .param p2, "storageType"    # Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    if-nez p1, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The path provided is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    if-nez p2, :cond_1

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The storageType provided is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_1
    iput-object p1, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    .line 45
    iput-object p2, p0, Lcom/google/android/music/download/cache/CacheLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    .line 46
    return-void
.end method


# virtual methods
.method public createVolumeIdIfMissing()Ljava/util/UUID;
    .locals 15

    .prologue
    .line 137
    sget-object v8, Lcom/google/android/music/download/cache/CacheLocation;->UUID_LOCK:Ljava/lang/Object;

    monitor-enter v8

    .line 139
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v3

    .line 140
    .local v3, "volId":Ljava/util/UUID;
    if-eqz v3, :cond_0

    monitor-exit v8

    move-object v4, v3

    .line 160
    .end local v3    # "volId":Ljava/util/UUID;
    .local v4, "volId":Ljava/util/UUID;
    :goto_0
    return-object v4

    .line 142
    .end local v4    # "volId":Ljava/util/UUID;
    .restart local v3    # "volId":Ljava/util/UUID;
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    .line 143
    iget-object v7, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 144
    new-instance v7, Ljava/lang/UnsupportedOperationException;

    const-string v9, "Directory needs to be created before attempting to add a volume ID to it"

    invoke-direct {v7, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 161
    .end local v3    # "volId":Ljava/util/UUID;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 147
    .restart local v3    # "volId":Ljava/util/UUID;
    :cond_1
    const/4 v5, 0x0

    .line 149
    .local v5, "writer":Ljava/io/OutputStreamWriter;
    :try_start_1
    new-instance v7, Ljava/io/File;

    iget-object v9, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    const-string v10, "._playmusicid"

    invoke-direct {v7, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v2

    .line 150
    .local v2, "outFile":Ljava/io/File;
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 151
    .local v1, "fos":Ljava/io/FileOutputStream;
    new-instance v6, Ljava/io/OutputStreamWriter;

    const-string v7, "UTF-8"

    invoke-direct {v6, v1, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 152
    .end local v5    # "writer":Ljava/io/OutputStreamWriter;
    .local v6, "writer":Ljava/io/OutputStreamWriter;
    :try_start_2
    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 158
    :try_start_3
    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    move-object v5, v6

    .line 160
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "outFile":Ljava/io/File;
    .end local v6    # "writer":Ljava/io/OutputStreamWriter;
    .restart local v5    # "writer":Ljava/io/OutputStreamWriter;
    :goto_1
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v4, v3

    .end local v3    # "volId":Ljava/util/UUID;
    .restart local v4    # "volId":Ljava/util/UUID;
    goto :goto_0

    .line 153
    .end local v4    # "volId":Ljava/util/UUID;
    .restart local v3    # "volId":Ljava/util/UUID;
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_4
    const-string v7, "CacheLocation"

    const-string v9, "Unable to write volume ID file at path: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    new-instance v12, Ljava/io/File;

    iget-object v13, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    const-string v14, "._playmusicid"

    invoke-direct {v12, v13, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 156
    const/4 v3, 0x0

    .line 158
    :try_start_5
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    :goto_3
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .end local v5    # "writer":Ljava/io/OutputStreamWriter;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "outFile":Ljava/io/File;
    .restart local v6    # "writer":Ljava/io/OutputStreamWriter;
    :catchall_2
    move-exception v7

    move-object v5, v6

    .end local v6    # "writer":Ljava/io/OutputStreamWriter;
    .restart local v5    # "writer":Ljava/io/OutputStreamWriter;
    goto :goto_3

    .line 153
    .end local v5    # "writer":Ljava/io/OutputStreamWriter;
    .restart local v6    # "writer":Ljava/io/OutputStreamWriter;
    :catch_1
    move-exception v0

    move-object v5, v6

    .end local v6    # "writer":Ljava/io/OutputStreamWriter;
    .restart local v5    # "writer":Ljava/io/OutputStreamWriter;
    goto :goto_2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p0, p1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 58
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 60
    check-cast v0, Lcom/google/android/music/download/cache/CacheLocation;

    .line 62
    .local v0, "that":Lcom/google/android/music/download/cache/CacheLocation;
    iget-object v3, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    iget-object v4, v0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 63
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/download/cache/CacheLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    iget-object v4, v0, Lcom/google/android/music/download/cache/CacheLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getCacheFile(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1, "relativePath"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    return-object v0
.end method

.method public getSchemaValueForStorageType()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-static {v0}, Lcom/google/android/music/download/cache/CacheUtils;->getSchemaValueForStorageType(Lcom/google/android/music/download/cache/CacheUtils$StorageType;)I

    move-result v0

    return v0
.end method

.method public getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    return-object v0
.end method

.method public getVolumeId()Ljava/util/UUID;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 100
    const/4 v3, 0x0

    .line 101
    .local v3, "resultId":Ljava/util/UUID;
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 102
    new-instance v2, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    const-string v7, "._playmusicid"

    invoke-direct {v2, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 103
    .local v2, "idFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 105
    new-instance v2, Ljava/io/File;

    .end local v2    # "idFile":Ljava/io/File;
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    const-string v7, "._playmusicid"

    invoke-direct {v2, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 107
    .restart local v2    # "idFile":Ljava/io/File;
    :cond_0
    const/4 v4, 0x0

    .line 110
    .local v4, "uuid":Ljava/lang/String;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v7, 0x64

    invoke-static {v6, v7}, Lcom/google/android/music/utils/IOUtils;->readSmallStream(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 111
    .local v0, "data":[B
    new-instance v5, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-direct {v5, v0, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 113
    .end local v4    # "uuid":Ljava/lang/String;
    .local v5, "uuid":Ljava/lang/String;
    :try_start_1
    invoke-static {v5}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v3

    .line 129
    .end local v0    # "data":[B
    .end local v2    # "idFile":Ljava/io/File;
    .end local v5    # "uuid":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v3

    .line 114
    .restart local v0    # "data":[B
    .restart local v2    # "idFile":Ljava/io/File;
    .restart local v5    # "uuid":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    const-string v6, "CacheLocation"

    const-string v7, "Error converting the string %s into a UUID from file %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    aput-object v2, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 121
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "uuid":Ljava/lang/String;
    .restart local v4    # "uuid":Ljava/lang/String;
    goto :goto_0

    .line 123
    .end local v0    # "data":[B
    :catch_2
    move-exception v1

    .line 125
    .local v1, "e":Ljava/io/IOException;
    :goto_1
    const-string v6, "CacheLocation"

    const-string v7, "Error getting volumeId for path %s due to IOException"

    new-array v8, v11, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 123
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "uuid":Ljava/lang/String;
    .restart local v0    # "data":[B
    .restart local v5    # "uuid":Ljava/lang/String;
    :catch_3
    move-exception v1

    move-object v4, v5

    .end local v5    # "uuid":Ljava/lang/String;
    .restart local v4    # "uuid":Ljava/lang/String;
    goto :goto_1

    .line 121
    .end local v0    # "data":[B
    :catch_4
    move-exception v6

    goto :goto_0
.end method

.method public hasMusicFiles()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 170
    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 171
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v1

    .line 177
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 178
    .local v0, "files":[Ljava/io/File;
    if-eqz v0, :cond_2

    array-length v2, v0

    if-gtz v2, :cond_0

    .line 183
    .end local v0    # "files":[Ljava/io/File;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 70
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->hashCode()I

    move-result v0

    .line 71
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 72
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "path="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocation;->mPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string v1, "type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocation;->mStorageType:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
