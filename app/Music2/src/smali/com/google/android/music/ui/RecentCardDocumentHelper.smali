.class public Lcom/google/android/music/ui/RecentCardDocumentHelper;
.super Ljava/lang/Object;
.source "RecentCardDocumentHelper.java"


# static fields
.field public static final RECENT_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_art"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "playlist_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "playlist_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "playlist_share_token"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "playlist_art_url"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "playlist_owner_name"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "radio_seed_source_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "radio_seed_source_type"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "radio_source_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "radio_id"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "radio_name"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "radio_art"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "RecentReason"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/RecentCardDocumentHelper;->RECENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static buildSingleDocument(Landroid/content/Context;Landroid/database/Cursor;I)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 28
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "position"    # I

    .prologue
    .line 107
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v25

    if-nez v25, :cond_1

    :cond_0
    const/4 v12, 0x0

    .line 200
    :goto_0
    return-object v12

    .line 108
    :cond_1
    new-instance v12, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v12}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 109
    .local v12, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-nez v25, :cond_4

    .line 110
    const/16 v25, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 111
    .local v8, "albumName":Ljava/lang/String;
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 112
    .local v6, "albumId":J
    const/16 v25, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 113
    .local v10, "artistId":J
    const/16 v25, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 114
    .local v4, "albumArtistName":Ljava/lang/String;
    sget-object v25, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 115
    invoke-virtual {v12, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 116
    invoke-virtual {v12, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v12, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v12, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 119
    invoke-virtual {v12, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v12, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v12, v10, v11}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 122
    const/16 v25, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-nez v25, :cond_2

    .line 123
    const/16 v25, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 125
    :cond_2
    const/16 v25, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-nez v25, :cond_3

    .line 126
    const/16 v25, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 196
    .end local v4    # "albumArtistName":Ljava/lang/String;
    .end local v6    # "albumId":J
    .end local v8    # "albumName":Ljava/lang/String;
    .end local v10    # "artistId":J
    :cond_3
    :goto_1
    const/16 v25, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    if-eqz v25, :cond_e

    const/16 v25, 0x1

    :goto_2
    move/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 197
    const/16 v25, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setMainstageReason(I)V

    .line 198
    move/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPosition(I)V

    goto/16 :goto_0

    .line 128
    :cond_4
    const/16 v25, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-nez v25, :cond_6

    .line 129
    const/16 v25, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 130
    .local v5, "albumMetajamId":Ljava/lang/String;
    const/16 v25, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 131
    .restart local v8    # "albumName":Ljava/lang/String;
    const/16 v25, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 132
    .restart local v4    # "albumArtistName":Ljava/lang/String;
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsNautilus(Z)V

    .line 133
    sget-object v25, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 134
    invoke-virtual {v12, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 135
    const/16 v25, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-nez v25, :cond_5

    .line 136
    const/16 v25, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 138
    :cond_5
    invoke-virtual {v12, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v12, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 140
    invoke-virtual {v12, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v12, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 142
    const/16 v25, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-nez v25, :cond_3

    .line 143
    const/16 v25, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 145
    .end local v4    # "albumArtistName":Ljava/lang/String;
    .end local v5    # "albumMetajamId":Ljava/lang/String;
    .end local v8    # "albumName":Ljava/lang/String;
    :cond_6
    const/16 v25, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-nez v25, :cond_9

    .line 146
    const/16 v25, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 147
    .local v16, "playlistId":J
    const/16 v25, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 148
    .local v18, "playlistName":Ljava/lang/String;
    move-wide/from16 v0, v16

    invoke-virtual {v12, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 149
    sget-object v25, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 150
    const/16 v25, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 151
    .local v19, "playlistType":I
    const/16 v25, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 152
    .local v13, "ownerName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistPrimaryLabel(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 155
    .local v24, "title":Ljava/lang/String;
    const/16 v25, 0x47

    move/from16 v0, v19

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    const/16 v25, 0x46

    move/from16 v0, v19

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    const/16 v25, 0x33

    move/from16 v0, v19

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    :cond_7
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_8

    .line 159
    const v25, 0x7f0b0102

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v13, v26, v27

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    .line 166
    .local v23, "subtitle":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 167
    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    .line 168
    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 169
    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    .line 170
    const/16 v25, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareToken(Ljava/lang/String;)V

    .line 171
    const/16 v25, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v12, v13}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistOwnerName(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 161
    .end local v23    # "subtitle":Ljava/lang/String;
    :cond_8
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;

    move-result-object v23

    .restart local v23    # "subtitle":Ljava/lang/String;
    goto :goto_3

    .line 174
    .end local v13    # "ownerName":Ljava/lang/String;
    .end local v16    # "playlistId":J
    .end local v18    # "playlistName":Ljava/lang/String;
    .end local v19    # "playlistType":I
    .end local v23    # "subtitle":Ljava/lang/String;
    .end local v24    # "title":Ljava/lang/String;
    :cond_9
    const/16 v25, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 175
    .local v14, "id":J
    const/16 v25, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 176
    .restart local v24    # "title":Ljava/lang/String;
    const/16 v25, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 177
    .local v9, "artUrl":Ljava/lang/String;
    sget-object v25, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 178
    invoke-virtual {v12, v14, v15}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 179
    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v12, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 181
    const/16 v25, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-eqz v25, :cond_a

    const/16 v21, 0x0

    .line 183
    .local v21, "radioSeedId":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedId(Ljava/lang/String;)V

    .line 184
    const/16 v25, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-eqz v25, :cond_b

    const/16 v22, -0x1

    .line 186
    .local v22, "radioSeedType":I
    :goto_5
    move/from16 v0, v22

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedType(I)V

    .line 187
    const/16 v25, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v25

    if-eqz v25, :cond_c

    const/16 v20, 0x0

    .line 189
    .local v20, "radioRemoteId":Ljava/lang/String;
    :goto_6
    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioRemoteId(Ljava/lang/String;)V

    .line 190
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v25

    if-eqz v25, :cond_d

    .line 191
    const v25, 0x7f0b0264

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 181
    .end local v20    # "radioRemoteId":Ljava/lang/String;
    .end local v21    # "radioSeedId":Ljava/lang/String;
    .end local v22    # "radioSeedType":I
    :cond_a
    const/16 v25, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    goto :goto_4

    .line 184
    .restart local v21    # "radioSeedId":Ljava/lang/String;
    :cond_b
    const/16 v25, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    goto :goto_5

    .line 187
    .restart local v22    # "radioSeedType":I
    :cond_c
    const/16 v25, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    goto :goto_6

    .line 193
    .restart local v20    # "radioRemoteId":Ljava/lang/String;
    :cond_d
    const v25, 0x7f0b0265

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 196
    .end local v9    # "artUrl":Ljava/lang/String;
    .end local v14    # "id":J
    .end local v20    # "radioRemoteId":Ljava/lang/String;
    .end local v21    # "radioSeedId":Ljava/lang/String;
    .end local v22    # "radioSeedType":I
    .end local v24    # "title":Ljava/lang/String;
    :cond_e
    const/16 v25, 0x0

    goto/16 :goto_2
.end method

.method public static populateIFLPlayCard(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "card"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 75
    invoke-static {p0, v2, v1, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getImFeelingLuckyDocument(Landroid/content/Context;ZZZZ)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 77
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 78
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 79
    new-instance v1, Lcom/google/android/music/ui/RecentCardDocumentHelper$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/ui/RecentCardDocumentHelper$1;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-void
.end method

.method public static populateRecentPlayCard(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;ILcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "card"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .param p3, "position"    # I
    .param p4, "delegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 90
    invoke-static {p0, p1, p3}, Lcom/google/android/music/ui/RecentCardDocumentHelper;->buildSingleDocument(Landroid/content/Context;Landroid/database/Cursor;I)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 91
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    if-nez v0, :cond_0

    .line 92
    const/4 v1, 0x4

    invoke-virtual {p2, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 104
    :goto_0
    return-void

    .line 95
    :cond_0
    sget-object v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 96
    invoke-virtual {p2, v0, p4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 97
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageDocumentDisplayed(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 98
    new-instance v1, Lcom/google/android/music/ui/RecentCardDocumentHelper$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/ui/RecentCardDocumentHelper$2;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-virtual {p2, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
