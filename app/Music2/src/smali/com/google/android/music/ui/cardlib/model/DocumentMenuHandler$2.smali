.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;
.super Ljava/lang/Object;
.source "DocumentMenuHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->setShareStateTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

.field final synthetic val$playlistId:J


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;J)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iput-wide p2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;->val$playlistId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 494
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;->val$playlistId:J

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->setShareState(Landroid/content/ContentResolver;JZ)I

    move-result v0

    .line 496
    .local v0, "res":I
    if-nez v0, :cond_0

    .line 497
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const v2, 0x7f0b0350

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 500
    :cond_0
    return-void
.end method
