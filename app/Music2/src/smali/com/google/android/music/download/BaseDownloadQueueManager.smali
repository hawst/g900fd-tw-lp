.class public abstract Lcom/google/android/music/download/BaseDownloadQueueManager;
.super Lcom/google/android/music/download/IDownloadQueueManager$Stub;
.source "BaseDownloadQueueManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RequestType::",
        "Lcom/google/android/music/download/DownloadRequest;",
        "TaskType::",
        "Lcom/google/android/music/download/DownloadTask;",
        ">",
        "Lcom/google/android/music/download/IDownloadQueueManager$Stub;"
    }
.end annotation


# instance fields
.field private final LOGV:Z

.field private final mContext:Landroid/content/Context;

.field private final mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/music/download/DownloadQueue",
            "<TTaskType;>;"
        }
    .end annotation
.end field

.field private final mDownloadThread:Ljava/lang/Thread;

.field private final mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private final mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

.field private volatile mShutdown:Z

.field private mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    invoke-direct {p0}, Lcom/google/android/music/download/IDownloadQueueManager$Stub;-><init>()V

    .line 28
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->LOGV:Z

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mShutdown:Z

    .line 37
    new-instance v0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-direct {v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 45
    new-instance v0, Lcom/google/android/music/download/BaseDownloadQueueManager$1;

    const-string v1, "DownloadThread"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/download/BaseDownloadQueueManager$1;-><init>(Lcom/google/android/music/download/BaseDownloadQueueManager;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadThread:Ljava/lang/Thread;

    .line 108
    iput-object p1, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mContext:Landroid/content/Context;

    .line 109
    new-instance v0, Lcom/google/android/music/download/DownloadQueue;

    iget-object v1, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadThread:Ljava/lang/Thread;

    invoke-direct {v0, v1}, Lcom/google/android/music/download/DownloadQueue;-><init>(Ljava/lang/Thread;)V

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;

    .line 110
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/cloudclient/MusicRequest;->getSharedHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    .line 111
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    iget-object v1, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->bindToService(Landroid/content/Context;)V

    .line 112
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 113
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 115
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 116
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadQueueManager;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mShutdown:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/BaseDownloadQueueManager;)Lcom/google/android/music/download/DownloadQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadQueueManager;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/download/BaseDownloadQueueManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadQueueManager;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->LOGV:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/download/BaseDownloadQueueManager;Lcom/google/android/music/download/DownloadTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/BaseDownloadQueueManager;
    .param p1, "x1"    # Lcom/google/android/music/download/DownloadTask;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/music/download/BaseDownloadQueueManager;->runTask(Lcom/google/android/music/download/DownloadTask;)V

    return-void
.end method

.method private acquireWifiLock()V
    .locals 5

    .prologue
    .line 204
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v3}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v1

    .line 205
    .local v1, "monitor":Lcom/google/android/music/net/INetworkMonitor;
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/music/net/INetworkMonitor;->hasWifiConnection()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xc

    if-lt v3, v4, :cond_1

    .line 209
    const/4 v2, 0x3

    .line 213
    .local v2, "wifiLockMode":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    const-string v4, "DownloadQueueManager"

    invoke-virtual {v3, v2, v4}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 214
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 215
    iget-object v3, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    .end local v1    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    .end local v2    # "wifiLockMode":I
    :cond_0
    :goto_1
    return-void

    .line 211
    .restart local v1    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    :cond_1
    const/4 v2, 0x1

    .restart local v2    # "wifiLockMode":I
    goto :goto_0

    .line 217
    .end local v1    # "monitor":Lcom/google/android/music/net/INetworkMonitor;
    .end local v2    # "wifiLockMode":I
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "DownloadQueueManager"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private afterExecute(Lcom/google/android/music/download/DownloadTask;Z)V
    .locals 0
    .param p1, "task"    # Lcom/google/android/music/download/DownloadTask;
    .param p2, "success"    # Z

    .prologue
    .line 180
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    invoke-direct {p0}, Lcom/google/android/music/download/BaseDownloadQueueManager;->releaseWiFiLock()V

    .line 181
    return-void
.end method

.method private beforeExecute(Lcom/google/android/music/download/DownloadTask;)V
    .locals 0
    .param p1, "task"    # Lcom/google/android/music/download/DownloadTask;

    .prologue
    .line 176
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    invoke-direct {p0}, Lcom/google/android/music/download/BaseDownloadQueueManager;->acquireWifiLock()V

    .line 177
    return-void
.end method

.method private convertListType(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/DownloadRequest;",
            ">;)",
            "Ljava/util/List",
            "<TRequestType;>;"
        }
    .end annotation

    .prologue
    .line 140
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    .local p1, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 141
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<TRequestType;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/DownloadRequest;

    .line 142
    .local v1, "request":Lcom/google/android/music/download/DownloadRequest;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v1    # "request":Lcom/google/android/music/download/DownloadRequest;
    :cond_0
    return-object v2
.end method

.method private failRequests(Ljava/util/List;Lcom/google/android/music/download/IDownloadProgressListener;I)V
    .locals 7
    .param p2, "downloadProgressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p3, "error"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/DownloadRequest;",
            ">;",
            "Lcom/google/android/music/download/IDownloadProgressListener;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    .local p1, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    new-instance v1, Lcom/google/android/music/download/DownloadState;

    invoke-direct {v1}, Lcom/google/android/music/download/DownloadState;-><init>()V

    .line 187
    .local v1, "failedDownloadState":Lcom/google/android/music/download/DownloadState;
    invoke-virtual {v1, p3}, Lcom/google/android/music/download/DownloadState;->setFailedState(I)V

    .line 189
    invoke-direct {p0, p1}, Lcom/google/android/music/download/BaseDownloadQueueManager;->convertListType(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/DownloadRequest;

    .line 191
    .local v3, "request":Lcom/google/android/music/download/DownloadRequest;, "TRequestType;"
    :try_start_0
    invoke-virtual {p0, v3, v1}, Lcom/google/android/music/download/BaseDownloadQueueManager;->createDownloadProgress(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;

    move-result-object v4

    invoke-interface {p2, v4}, Lcom/google/android/music/download/IDownloadProgressListener;->onDownloadProgress(Lcom/google/android/music/download/DownloadProgress;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "DownloadQueueManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to call progress callback for request: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v3    # "request":Lcom/google/android/music/download/DownloadRequest;, "TRequestType;"
    :cond_0
    return-void
.end method

.method private releaseWiFiLock()V
    .locals 1

    .prologue
    .line 223
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 227
    :cond_0
    return-void
.end method

.method private runTask(Lcom/google/android/music/download/DownloadTask;)V
    .locals 2
    .param p1, "task"    # Lcom/google/android/music/download/DownloadTask;

    .prologue
    .line 165
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    const/4 v0, 0x0

    .line 167
    .local v0, "success":Z
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/download/BaseDownloadQueueManager;->beforeExecute(Lcom/google/android/music/download/DownloadTask;)V

    .line 168
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadTask;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    const/4 v0, 0x1

    .line 171
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/download/BaseDownloadQueueManager;->afterExecute(Lcom/google/android/music/download/DownloadTask;Z)V

    .line 173
    return-void

    .line 171
    :catchall_0
    move-exception v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/download/BaseDownloadQueueManager;->afterExecute(Lcom/google/android/music/download/DownloadTask;Z)V

    throw v1
.end method


# virtual methods
.method public cancelAndPurge(II)V
    .locals 2
    .param p1, "ownerOrdinal"    # I
    .param p2, "purgePolicy"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 149
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;

    invoke-virtual {p0, p1}, Lcom/google/android/music/download/BaseDownloadQueueManager;->getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/music/download/DownloadQueue;->cancelAndPurge(Lcom/google/android/music/download/DownloadRequest$Owner;I)V

    .line 150
    return-void
.end method

.method protected abstract createDownloadProgress(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequestType;",
            "Lcom/google/android/music/download/DownloadState;",
            ")",
            "Lcom/google/android/music/download/DownloadProgress;"
        }
    .end annotation
.end method

.method public download(Ljava/util/List;Lcom/google/android/music/download/IDownloadProgressListener;I)V
    .locals 8
    .param p2, "progressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p3, "purgePolicy"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/DownloadRequest;",
            ">;",
            "Lcom/google/android/music/download/IDownloadProgressListener;",
            "I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 121
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    .local p1, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    iget-object v4, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    const-wide/16 v6, 0x2710

    invoke-virtual {v4, v6, v7}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->waitForServiceConnection(J)Z

    move-result v4

    if-nez v4, :cond_0

    .line 122
    const/16 v4, 0xb

    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/music/download/BaseDownloadQueueManager;->failRequests(Ljava/util/List;Lcom/google/android/music/download/IDownloadProgressListener;I)V

    .line 123
    const-string v4, "DownloadQueueManager"

    const-string v5, "Failed to connect to network monitor service"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/download/BaseDownloadQueueManager;->convertListType(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 127
    .local v2, "requestsCopy":Ljava/util/List;, "Ljava/util/List<TRequestType;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v3, "tasks":Ljava/util/List;, "Ljava/util/List<TTaskType;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/DownloadRequest;

    .line 129
    .local v1, "request":Lcom/google/android/music/download/DownloadRequest;, "TRequestType;"
    invoke-virtual {p0, v1, p2}, Lcom/google/android/music/download/BaseDownloadQueueManager;->getDownloadTask(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;)Lcom/google/android/music/download/DownloadTask;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 131
    .end local v1    # "request":Lcom/google/android/music/download/DownloadRequest;, "TRequestType;"
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadQueue:Lcom/google/android/music/download/DownloadQueue;

    invoke-virtual {v4, v3, p3}, Lcom/google/android/music/download/DownloadQueue;->addTasks(Ljava/util/List;I)Z

    goto :goto_0
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 232
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    .line 233
    const-string v0, "DownloadQueueManager"

    const-string v1, "The wifi lock was never released... now releasing in finalizer"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/download/IDownloadQueueManager$Stub;->finalize()V

    .line 240
    return-void

    .line 238
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/android/music/download/IDownloadQueueManager$Stub;->finalize()V

    throw v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 243
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected abstract getDownloadTask(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;)Lcom/google/android/music/download/DownloadTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequestType;",
            "Lcom/google/android/music/download/IDownloadProgressListener;",
            ")TTaskType;"
        }
    .end annotation
.end method

.method protected getHttpClient()Lcom/google/android/music/cloudclient/MusicHttpClient;
    .locals 1

    .prologue
    .line 251
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    return-object v0
.end method

.method protected getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 247
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method protected getNetworkMonitorServiceConnection()Lcom/google/android/music/net/NetworkMonitorServiceConnection;
    .locals 1

    .prologue
    .line 255
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    return-object v0
.end method

.method protected abstract getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 155
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadQueueManager;, "Lcom/google/android/music/download/BaseDownloadQueueManager<TRequestType;TTaskType;>;"
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "DownloadQueueManager"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mShutdown:Z

    .line 159
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mDownloadThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 160
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    iget-object v1, p0, Lcom/google/android/music/download/BaseDownloadQueueManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->unbindFromService(Landroid/content/Context;)V

    .line 161
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 162
    return-void
.end method
