.class public Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioFeedRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/RadioFeedRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RadioStationRequest"
.end annotation


# instance fields
.field public mNumEntries:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "numEntries"
    .end annotation
.end field

.field public mRadioId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "radioId"
    .end annotation
.end field

.field public mRecentlyPlayed:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "recentlyPlayed"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;",
            ">;"
        }
    .end annotation
.end field

.field public mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seed"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mRecentlyPlayed:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public setRecentlyPlayedFromMixList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "in":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    iget-object v3, p0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mRecentlyPlayed:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 39
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/MixTrackId;

    .line 40
    .local v2, "trackId":Lcom/google/android/music/cloudclient/MixTrackId;
    new-instance v0, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;-><init>()V

    .line 41
    .local v0, "entry":Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;
    invoke-virtual {v2}, Lcom/google/android/music/cloudclient/MixTrackId;->getRemoteId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;->mId:Ljava/lang/String;

    .line 42
    invoke-virtual {v2}, Lcom/google/android/music/cloudclient/MixTrackId;->getType()Lcom/google/android/music/cloudclient/MixTrackId$Type;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/cloudclient/MixTrackId;->trackIdTypeToServerType(Lcom/google/android/music/cloudclient/MixTrackId$Type;)I

    move-result v3

    iput v3, v0, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;->mType:I

    .line 43
    iget-object v3, p0, Lcom/google/android/music/cloudclient/RadioFeedRequest$RadioStationRequest;->mRecentlyPlayed:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    .end local v0    # "entry":Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;
    .end local v2    # "trackId":Lcom/google/android/music/cloudclient/MixTrackId;
    :cond_0
    return-void
.end method
