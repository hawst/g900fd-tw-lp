.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleLoadCloudQueue(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;)V
    .locals 6
    .param p1, "result"    # Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 433
    invoke-interface {p1}, Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "MusicCastRemote"

    const-string v1, "Media loaded successfully"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    const-wide/16 v2, 0x0

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updatePosition(J)V
    invoke-static {v0, v2, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$600(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;J)V

    .line 439
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/StopWatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->start()V

    .line 441
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v0, v5}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 442
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z
    invoke-static {v0, v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$902(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 443
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z
    invoke-static {v0, v5}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$502(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 448
    :goto_0
    return-void

    .line 445
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z
    invoke-static {v0, v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 446
    const-string v0, "MusicCastRemote"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Load unsuccessful: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 430
    check-cast p1, Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;->onResult(Lcom/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult;)V

    return-void
.end method
