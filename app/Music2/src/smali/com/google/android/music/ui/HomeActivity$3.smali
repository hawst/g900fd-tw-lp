.class Lcom/google/android/music/ui/HomeActivity$3;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Landroid/content/SyncStatusObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/HomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/HomeActivity;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/google/android/music/ui/HomeActivity$3;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChanged(I)V
    .locals 2
    .param p1, "which"    # I

    .prologue
    .line 550
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity$3;->this$0:Lcom/google/android/music/ui/HomeActivity;

    # invokes: Lcom/google/android/music/ui/HomeActivity;->updateHasAudioAndSyncState()V
    invoke-static {v0}, Lcom/google/android/music/ui/HomeActivity;->access$300(Lcom/google/android/music/ui/HomeActivity;)V

    .line 560
    :goto_0
    return-void

    .line 553
    :cond_0
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$3$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/HomeActivity$3$1;-><init>(Lcom/google/android/music/ui/HomeActivity$3;)V

    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$3;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    goto :goto_0
.end method
