.class Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity$4;->onBindDescription(Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDescription:Ljava/lang/String;

.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$4;

.field final synthetic val$getter:Lcom/google/android/music/leanback/Item$StringGetter;

.field final synthetic val$vh:Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$4;Lcom/google/android/music/leanback/Item$StringGetter;Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$4;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;->val$getter:Lcom/google/android/music/leanback/Item$StringGetter;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;->val$vh:Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;->val$getter:Lcom/google/android/music/leanback/Item$StringGetter;

    invoke-interface {v0}, Lcom/google/android/music/leanback/Item$StringGetter;->getString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;->mDescription:Ljava/lang/String;

    .line 299
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;->val$vh:Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getSubtitle()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$4$1;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    return-void
.end method
