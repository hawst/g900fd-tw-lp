.class public abstract Lcom/google/android/music/sync/common/AbstractSyncAdapter;
.super Lcom/google/android/common/LoggingThreadedSyncAdapter;
.source "AbstractSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;,
        Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;,
        Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueueEntry;,
        Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;,
        Lcom/google/android/music/sync/common/AbstractSyncAdapter$Builder;
    }
.end annotation


# instance fields
.field private mActionOnInitialization:I

.field protected mAuthInfo:Lcom/google/android/music/sync/common/AuthInfo;

.field protected final mContext:Landroid/content/Context;

.field private mMaxDownstreamLoops:I

.field private mMaxQueueSize:I

.field private final mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected mTag:Ljava/lang/String;

.field private final mThreadPool:Ljava/util/concurrent/ExecutorService;

.field private final mUseVerboseLogging:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0, p1, v1}, Lcom/google/android/common/LoggingThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 54
    const-string v0, "AbstractSyncAdapter"

    iput-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 81
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 82
    iput-object p1, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    .line 83
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    .line 84
    return-void
.end method

.method private final download(Landroid/content/SyncResult;Landroid/accounts/Account;Ljava/util/HashMap;)V
    .locals 9
    .param p1, "syncResult"    # Landroid/content/SyncResult;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SyncResult;",
            "Landroid/accounts/Account;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 478
    .local p3, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p0, p2, p3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->onDownstreamStart(Landroid/accounts/Account;Ljava/util/HashMap;)V

    .line 480
    new-instance v4, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;

    iget v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mMaxQueueSize:I

    invoke-direct {v4, v6}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;-><init>(I)V

    .line 481
    .local v4, "fetchQueue":Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;
    const/4 v1, 0x0

    .line 482
    .local v1, "downstreamMergerFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Void;>;"
    const/4 v3, 0x0

    .line 485
    .local v3, "downstreamReaderFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Void;>;"
    const/16 v6, 0x64

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v8

    invoke-virtual {p0, v4, v6, v7, v8}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->createDownstreamReader(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;ILandroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/DownstreamReader;

    move-result-object v2

    .line 488
    .local v2, "downstreamReader":Lcom/google/android/music/sync/common/DownstreamReader;
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v3

    .line 490
    invoke-virtual {v2}, Lcom/google/android/music/sync/common/DownstreamReader;->getMergeQueue()Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0, v6, v7, p3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->createDownstreamMerger(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/DownstreamMerger;

    move-result-object v0

    .line 492
    .local v0, "downstreamMerger":Lcom/google/android/music/sync/common/DownstreamMerger;
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 498
    const/4 v5, 0x0

    .line 499
    .local v5, "loopCount":I
    :cond_0
    add-int/lit8 v5, v5, 0x1

    :try_start_1
    iget v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mMaxDownstreamLoops:I

    if-gt v5, v6, :cond_2

    .line 500
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_1

    .line 501
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Downstream loop "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_1
    invoke-virtual {p0, p2, v4, p3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->fetchDataFromServer(Landroid/accounts/Account;Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;Ljava/util/HashMap;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 517
    :cond_2
    :goto_0
    iget v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mMaxDownstreamLoops:I

    if-le v5, v6, :cond_4

    .line 518
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_3

    .line 519
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Exceeded maximum number of downstream loops."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :cond_3
    const/4 v6, 0x1

    iput-boolean v6, p1, Landroid/content/SyncResult;->tooManyRetries:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 524
    :cond_4
    :try_start_2
    invoke-virtual {v4}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;->close()V

    .line 528
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_5

    .line 529
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Waiting on downstream reader thread to finish..."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :cond_5
    invoke-direct {p0, v3, p1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->throwIfFutureTaskFailed(Ljava/util/concurrent/Future;Landroid/content/SyncResult;)V

    .line 532
    const/4 v3, 0x0

    .line 535
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_6

    .line 536
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Waiting on downstream merger thread to finish merging..."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    :cond_6
    invoke-direct {p0, v1, p1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->throwIfFutureTaskFailed(Ljava/util/concurrent/Future;Landroid/content/SyncResult;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 539
    const/4 v1, 0x0

    .line 542
    if-nez v3, :cond_7

    if-eqz v1, :cond_9

    .line 543
    :cond_7
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_8

    .line 544
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Error occurred when dowloading. Waiting for download threads."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_8
    invoke-virtual {v4}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;->kill()V

    .line 547
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    .line 548
    invoke-direct {p0, v1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    .line 553
    :cond_9
    invoke-virtual {p0, p2, p3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->onDownstreamComplete(Landroid/accounts/Account;Ljava/util/HashMap;)V

    .line 555
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_a

    .line 556
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Downstream sync complete."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    :cond_a
    return-void

    .line 508
    :cond_b
    :try_start_3
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->hasFutureTaskReportedAnError(Ljava/util/concurrent/Future;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 509
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Bailing on downstream reader thread due to an error."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 524
    :catchall_0
    move-exception v6

    :try_start_4
    invoke-virtual {v4}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;->close()V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 542
    .end local v0    # "downstreamMerger":Lcom/google/android/music/sync/common/DownstreamMerger;
    .end local v2    # "downstreamReader":Lcom/google/android/music/sync/common/DownstreamReader;
    .end local v5    # "loopCount":I
    :catchall_1
    move-exception v6

    if-nez v3, :cond_c

    if-eqz v1, :cond_e

    .line 543
    :cond_c
    iget-boolean v7, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v7, :cond_d

    .line 544
    iget-object v7, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v8, "Error occurred when dowloading. Waiting for download threads."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_d
    invoke-virtual {v4}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;->kill()V

    .line 547
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    .line 548
    invoke-direct {p0, v1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    :cond_e
    throw v6

    .line 512
    .restart local v0    # "downstreamMerger":Lcom/google/android/music/sync/common/DownstreamMerger;
    .restart local v2    # "downstreamReader":Lcom/google/android/music/sync/common/DownstreamReader;
    .restart local v5    # "loopCount":I
    :cond_f
    :try_start_5
    invoke-direct {p0, v1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->hasFutureTaskReportedAnError(Ljava/util/concurrent/Future;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 513
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Bailing on downstream merger thread due to an error."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method private hasFutureTaskReportedAnError(Ljava/util/concurrent/Future;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    const/4 v1, 0x0

    .line 577
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 578
    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 587
    :cond_0
    :goto_0
    return v1

    .line 581
    :catch_0
    move-exception v0

    .line 582
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const/4 v1, 0x1

    goto :goto_0

    .line 583
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_1
    move-exception v0

    .line 586
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private final safeWait(Ljava/util/concurrent/Future;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 562
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    if-eqz p1, :cond_0

    .line 564
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 565
    :catch_0
    move-exception v0

    .line 566
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v2, "Exception while waiting for completion."

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private throwIfFutureTaskFailed(Ljava/util/concurrent/Future;Landroid/content/SyncResult;)V
    .locals 8
    .param p2, "result"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;",
            "Landroid/content/SyncResult;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 598
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 622
    :goto_0
    return-void

    .line 599
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    .line 601
    .local v2, "t":Ljava/lang/Throwable;
    instance-of v3, v2, Lcom/google/android/music/sync/api/ServiceUnavailableException;

    if-eqz v3, :cond_1

    .line 602
    const-class v3, Lcom/google/android/music/sync/api/ServiceUnavailableException;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/api/ServiceUnavailableException;

    .line 603
    .local v1, "s":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/ServiceUnavailableException;->getRetryAfter()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 604
    invoke-virtual {v1}, Lcom/google/android/music/sync/api/ServiceUnavailableException;->getRetryAfter()J

    move-result-wide v4

    iput-wide v4, p2, Landroid/content/SyncResult;->delayUntil:J

    .line 606
    :cond_0
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    invoke-direct {v3, v1}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 607
    .end local v1    # "s":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    :cond_1
    instance-of v3, v2, Ljava/io/IOException;

    if-eqz v3, :cond_2

    .line 608
    new-instance v3, Lcom/google/android/music/sync/common/SoftSyncException;

    invoke-direct {v3, v2}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 609
    :cond_2
    instance-of v3, v2, Lcom/google/android/music/sync/common/SoftSyncException;

    if-eqz v3, :cond_3

    .line 610
    const-class v3, Lcom/google/android/music/sync/common/SoftSyncException;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/common/SoftSyncException;

    throw v3

    .line 611
    :cond_3
    instance-of v3, v2, Landroid/accounts/AuthenticatorException;

    if-eqz v3, :cond_4

    .line 612
    new-instance v3, Landroid/accounts/AuthenticatorException;

    invoke-direct {v3, v2}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 614
    :cond_4
    new-instance v3, Lcom/google/android/music/sync/common/HardSyncException;

    invoke-direct {v3, v2}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 616
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    .end local v2    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v0

    .line 619
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private final upload(Landroid/content/SyncResult;Ljava/util/HashMap;)V
    .locals 10
    .param p1, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SyncResult;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 415
    .local p2, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_0

    .line 416
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Commencing upstream sync."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_0
    new-instance v1, Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;

    iget v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mMaxQueueSize:I

    invoke-direct {v1, v6}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;-><init>(I)V

    .line 419
    .local v1, "upstreamQueue":Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;
    const/4 v3, 0x0

    .line 420
    .local v3, "upstreamReaderFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Void;>;"
    const/4 v5, 0x0

    .line 422
    .local v5, "upstreamSenderFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/sync/common/UpstreamSender$UpstreamSenderResult;>;"
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1, v6, p2}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->createUpstreamReader(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/UpstreamReader;

    move-result-object v2

    .line 424
    .local v2, "upstreamReader":Lcom/google/android/music/sync/common/UpstreamReader;
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v3

    .line 425
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1, v6, p2}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->createUpstreamSender(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/UpstreamSender;

    move-result-object v4

    .line 429
    .local v4, "upstreamSender":Lcom/google/android/music/sync/common/UpstreamSender;
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v5

    .line 432
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_1

    .line 433
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Waiting on upstream reader thread to finish..."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_1
    invoke-direct {p0, v3, p1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->throwIfFutureTaskFailed(Ljava/util/concurrent/Future;Landroid/content/SyncResult;)V

    .line 436
    const/4 v3, 0x0

    .line 439
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_2

    .line 440
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Waiting on upstream merger thread to finish merging..."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_2
    invoke-direct {p0, v5, p1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->throwIfFutureTaskFailed(Ljava/util/concurrent/Future;Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    :try_start_1
    iget-object v7, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-interface {v5}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/sync/common/UpstreamSender$UpstreamSenderResult;

    iget v6, v6, Lcom/google/android/music/sync/common/UpstreamSender$UpstreamSenderResult;->mNumConflicts:I

    int-to-long v8, v6

    iput-wide v8, v7, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452
    :goto_0
    const/4 v5, 0x0

    .line 455
    if-nez v3, :cond_3

    if-eqz v5, :cond_5

    .line 456
    :cond_3
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_4

    .line 457
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Error occurred when downloading. Waiting for download threads."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;->kill()V

    .line 460
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    .line 461
    invoke-direct {p0, v5}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    .line 465
    :cond_5
    iget-boolean v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v6, :cond_6

    .line 466
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "Upstream sync done."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_6
    return-void

    .line 447
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 455
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "upstreamReader":Lcom/google/android/music/sync/common/UpstreamReader;
    .end local v4    # "upstreamSender":Lcom/google/android/music/sync/common/UpstreamSender;
    :catchall_0
    move-exception v6

    if-nez v3, :cond_7

    if-eqz v5, :cond_9

    .line 456
    :cond_7
    iget-boolean v7, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v7, :cond_8

    .line 457
    iget-object v7, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v8, "Error occurred when downloading. Waiting for download threads."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_8
    invoke-virtual {v1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;->kill()V

    .line 460
    invoke-direct {p0, v3}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    .line 461
    invoke-direct {p0, v5}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->safeWait(Ljava/util/concurrent/Future;)V

    :cond_9
    throw v6

    .line 449
    .restart local v2    # "upstreamReader":Lcom/google/android/music/sync/common/UpstreamReader;
    .restart local v4    # "upstreamSender":Lcom/google/android/music/sync/common/UpstreamSender;
    :catch_1
    move-exception v0

    .line 450
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_3
    iget-object v6, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v7, "We cannot get here, as the exception would have detected earlier."

    invoke-static {v6, v7, v0}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method protected abstract createDownstreamMerger(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/DownstreamMerger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/DownstreamMerger;"
        }
    .end annotation
.end method

.method protected abstract createDownstreamReader(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;ILandroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/DownstreamReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;",
            "I",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/DownstreamReader;"
        }
    .end annotation
.end method

.method protected abstract createUpstreamReader(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/UpstreamReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/UpstreamReader;"
        }
    .end annotation
.end method

.method protected abstract createUpstreamSender(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/UpstreamSender;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/UpstreamSender;"
        }
    .end annotation
.end method

.method protected abstract fetchDataFromServer(Landroid/accounts/Account;Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;Ljava/util/HashMap;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation
.end method

.method innerPerformSync(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;Landroid/accounts/Account;)V
    .locals 18
    .param p1, "extras"    # Landroid/os/Bundle;
    .param p2, "authority"    # Ljava/lang/String;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .param p5, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 302
    move-object/from16 v0, p5

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 303
    .local v4, "accountName":Ljava/lang/String;
    move-object/from16 v0, p5

    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 304
    .local v5, "accountType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v12, :cond_0

    .line 305
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Commencing sync for "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "; "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_0
    if-eqz v4, :cond_1

    if-nez v5, :cond_3

    .line 309
    :cond_1
    move-object/from16 v0, p4

    iget-object v12, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v12, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v12, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 310
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v12, :cond_2

    .line 311
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v13, "The account is either missing a name or type."

    invoke-static {v12, v13}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_2
    :goto_0
    return-void

    .line 319
    :cond_3
    const-string v12, "initialize"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 320
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v12

    if-gez v12, :cond_5

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mActionOnInitialization:I

    const/4 v13, -0x1

    if-eq v12, v13, :cond_5

    .line 322
    move-object/from16 v0, p5

    iget-object v12, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v13, "@youtube.com"

    invoke-virtual {v12, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v12, 0x0

    :goto_1
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v0, v1, v12}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mActionOnInitialization:I

    goto :goto_1

    .line 334
    :cond_5
    new-instance v8, Ljava/lang/Object;

    invoke-direct {v8}, Ljava/lang/Object;-><init>()V

    .line 335
    .local v8, "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v12, v8}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    .line 339
    .local v6, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isValidAccount()Z

    move-result v12

    if-nez v12, :cond_7

    .line 342
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->getUpgradeSyncDone()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 343
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "music_sync_invalid_account"

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v10

    .line 347
    .local v10, "syncInvalidAccountFlag":Z
    if-nez v10, :cond_6

    .line 350
    const-string v12, "force"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 351
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v13, "Allowing manual request to sync invalid account."

    invoke-static {v12, v13}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_6
    const-string v12, "feed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    if-nez v12, :cond_b

    .line 375
    .end local v10    # "syncInvalidAccountFlag":Z
    :cond_7
    :goto_2
    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 380
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v7

    .line 381
    .local v7, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v9, 0x0

    .line 383
    .local v9, "success":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v12, v7, v2}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->onSyncStart(Landroid/accounts/Account;Landroid/content/Context;Ljava/util/Map;Landroid/os/Bundle;)V

    .line 385
    new-instance v8, Ljava/lang/Object;

    .end local v8    # "refObject":Ljava/lang/Object;
    invoke-direct {v8}, Ljava/lang/Object;-><init>()V

    .line 386
    .restart local v8    # "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v12, v8}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    .line 388
    :try_start_1
    const-string v12, "upload"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 389
    .local v11, "uploadOnly":Z
    if-eqz v11, :cond_d

    .line 390
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v12, :cond_8

    .line 391
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v13, "Skipping downstream sync."

    invoke-static {v12, v13}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->upload(Landroid/content/SyncResult;Ljava/util/HashMap;)V

    .line 398
    const/4 v9, 0x1

    .line 399
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->getUpgradeSyncDone()Z

    move-result v12

    if-nez v12, :cond_9

    .line 400
    const/4 v12, 0x1

    invoke-virtual {v6, v12}, Lcom/google/android/music/preferences/MusicPreferences;->setUpgradeSyncDone(Z)V

    .line 401
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->setLastUpgradeSyncVersion()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 404
    :cond_9
    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 405
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1, v12, v7, v9}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->onSyncEnd(Landroid/accounts/Account;Landroid/content/Context;Ljava/util/Map;Z)V

    goto/16 :goto_0

    .line 353
    .end local v7    # "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v9    # "success":Z
    .end local v11    # "uploadOnly":Z
    .restart local v10    # "syncInvalidAccountFlag":Z
    :cond_a
    :try_start_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v13, "Skipping invalid account sync."

    invoke-static {v12, v13}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 375
    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 361
    :cond_b
    :try_start_3
    const-string v12, "config-update"

    const-string v13, "feed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 364
    const-string v12, "configAlarm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 367
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v13, "Skipping syncing of non-config request for invalid account."

    invoke-static {v12, v13}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 375
    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 371
    .end local v10    # "syncInvalidAccountFlag":Z
    :cond_c
    :try_start_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v13, "Doing one time upgrade sync for invalid account."

    invoke-static {v12, v13}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 375
    :catchall_0
    move-exception v12

    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v12

    .line 394
    .restart local v7    # "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v9    # "success":Z
    .restart local v11    # "uploadOnly":Z
    :cond_d
    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v7}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->download(Landroid/content/SyncResult;Landroid/accounts/Account;Ljava/util/HashMap;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    .line 404
    .end local v11    # "uploadOnly":Z
    :catchall_1
    move-exception v12

    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 405
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1, v13, v7, v9}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->onSyncEnd(Landroid/accounts/Account;Landroid/content/Context;Ljava/util/Map;Z)V

    throw v12
.end method

.method protected abstract onDownstreamComplete(Landroid/accounts/Account;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation
.end method

.method protected abstract onDownstreamStart(Landroid/accounts/Account;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation
.end method

.method public onPerformLoggedSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    const-wide/16 v8, 0x1

    const/4 v7, 0x0

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p1

    .line 271
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->innerPerformSync(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;Landroid/accounts/Account;)V
    :try_end_0
    .catch Lcom/google/android/music/sync/common/SoftSyncException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/common/HardSyncException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 293
    :goto_0
    return-void

    .line 272
    :catch_0
    move-exception v6

    .line 273
    .local v6, "e":Lcom/google/android/music/sync/common/SoftSyncException;
    :try_start_1
    invoke-virtual {v6}, Lcom/google/android/music/sync/common/SoftSyncException;->getRetryAfter()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {v6}, Lcom/google/android/music/sync/common/SoftSyncException;->getRetryAfter()J

    move-result-wide v0

    iput-wide v0, p5, Landroid/content/SyncResult;->delayUntil:J

    .line 276
    :cond_0
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 277
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v1, "Sync failed due to soft error."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-boolean v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/music/sync/common/SoftSyncException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 281
    .end local v6    # "e":Lcom/google/android/music/sync/common/SoftSyncException;
    :catch_1
    move-exception v6

    .line 282
    .local v6, "e":Lcom/google/android/music/sync/common/HardSyncException;
    :try_start_2
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    .line 283
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v1, "Sync failed due to a hard error."

    invoke-static {v0, v1, v6}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 291
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 284
    .end local v6    # "e":Lcom/google/android/music/sync/common/HardSyncException;
    :catch_2
    move-exception v6

    .line 285
    .local v6, "e":Landroid/accounts/AuthenticatorException;
    :try_start_3
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 286
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    const-string v1, "Sync failed due to an authentication issue."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-boolean v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mUseVerboseLogging:Z

    if-eqz v0, :cond_2

    .line 288
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mTag:Ljava/lang/String;

    invoke-virtual {v6}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .end local v6    # "e":Landroid/accounts/AuthenticatorException;
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mSyncActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
.end method

.method protected abstract onSyncEnd(Landroid/accounts/Account;Landroid/content/Context;Ljava/util/Map;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation
.end method

.method protected abstract onSyncStart(Landroid/accounts/Account;Landroid/content/Context;Ljava/util/Map;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation
.end method

.method final setActionOnInitialization(I)V
    .locals 0
    .param p1, "actionOnInitialization"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mActionOnInitialization:I

    .line 99
    return-void
.end method

.method final setMaxDownstreamLoops(I)V
    .locals 0
    .param p1, "maxDownstreamLoops"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mMaxDownstreamLoops:I

    .line 113
    return-void
.end method

.method final setMaxQueueSize(I)V
    .locals 0
    .param p1, "maxQueueSize"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/google/android/music/sync/common/AbstractSyncAdapter;->mMaxQueueSize:I

    .line 106
    return-void
.end method
