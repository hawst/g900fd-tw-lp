.class Lcom/google/android/music/ui/ContainerHeaderView$2;
.super Ljava/lang/Object;
.source "ContainerHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ContainerHeaderView;->setSongList(Lcom/google/android/music/medialist/SongList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mSavedAlbumYearValue:I

.field private mSavedArtistArtUrl:Ljava/lang/String;

.field private mSavedArtistId:J

.field private mSavedArtistMetajamId:Ljava/lang/String;

.field private mSavedPrimaryTitle:Ljava/lang/String;

.field private mSavedSecondaryTitle:Ljava/lang/String;

.field private final mSavedSongList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/ui/ContainerHeaderView;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ContainerHeaderView;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 303
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iput-object p2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 304
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    .line 307
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistId:J

    .line 311
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedAlbumYearValue:I

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 8

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedPrimaryTitle:Ljava/lang/String;

    .line 322
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->getSecondaryName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSecondaryTitle:Ljava/lang/String;

    .line 323
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistId:J

    .line 324
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    iget-wide v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistId:J

    invoke-static {v0, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->getArtistArtUrl(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistArtUrl:Ljava/lang/String;

    .line 326
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v0, :cond_3

    .line 328
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v6

    .line 329
    .local v6, "albumMetajamId":Ljava/lang/String;
    const/4 v7, 0x0

    .line 331
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/store/MusicContent$Albums;->getNautilusAlbumsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ArtistMetajamId"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 335
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 336
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistMetajamId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :cond_2
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 341
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistMetajamId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->getNautilusArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistArtUrl:Ljava/lang/String;

    .line 343
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumYear(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedAlbumYearValue:I

    goto/16 :goto_0

    .line 339
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 344
    .end local v6    # "albumMetajamId":Ljava/lang/String;
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/AlbumSongList;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/AlbumSongList;->getAlbumYear(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedAlbumYearValue:I

    goto/16 :goto_0
.end method

.method public taskCompleted()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 351
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/BaseListFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedPrimaryTitle:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mPrimaryTitle:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$202(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;

    .line 356
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedSecondaryTitle:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSecondaryTitle:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$302(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;

    .line 357
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-wide v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistId:J

    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistId:J
    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$402(Lcom/google/android/music/ui/ContainerHeaderView;J)J

    .line 358
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistMetajamId:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$502(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;

    .line 359
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistArtUrl:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrl:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$602(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;

    .line 360
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iget v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedAlbumYearValue:I

    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearValue:I
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$702(Lcom/google/android/music/ui/ContainerHeaderView;I)I

    .line 363
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v1, :cond_5

    .line 364
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 365
    .local v0, "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v1

    const/16 v2, 0x47

    if-ne v1, v2, :cond_2

    .line 367
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getOwnerProfilePhotoUrl()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/music/ui/ContainerHeaderView;->showOwnerProfilePicture(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$800(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)V

    .line 374
    .end local v0    # "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->isArtistArtShown()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/medialist/SongList;->shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 377
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->mSavedArtistArtUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 378
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$900(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$600(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->requestBitmap(Ljava/lang/String;)V

    .line 382
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f0201f5

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 383
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/ui/ContainerHeaderView$2$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/ui/ContainerHeaderView$2$1;-><init>(Lcom/google/android/music/ui/ContainerHeaderView$2;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 411
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/ui/ContainerHeaderView$2$2;

    invoke-direct {v2, p0}, Lcom/google/android/music/ui/ContainerHeaderView$2$2;-><init>(Lcom/google/android/music/ui/ContainerHeaderView$2;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0360

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 428
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v1, :cond_6

    .line 429
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/AsyncAlbumArtImageView;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setSharedPlaylistArt(Ljava/lang/String;)V

    .line 434
    :goto_2
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mTitle:Lcom/google/android/music/widgets/MarqueeTextView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1200(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/widgets/MarqueeTextView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mPrimaryTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$200(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/widgets/MarqueeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSubtitle:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1300(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSecondaryTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$300(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 437
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mTitle:Lcom/google/android/music/widgets/MarqueeTextView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1200(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/widgets/MarqueeTextView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/widgets/MarqueeTextView;->doMarquee(Z)V

    .line 440
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/BaseListFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseListFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mPrimaryTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$200(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/music/ui/ActionBarController;->setActionBarTitle(Ljava/lang/String;)V

    .line 442
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearValue:I
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$700(Lcom/google/android/music/ui/ContainerHeaderView;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYear:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1400(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 444
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearHyphen:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1500(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 445
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYear:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1400(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearValue:I
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$700(Lcom/google/android/music/ui/ContainerHeaderView;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearHyphen:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1500(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b003e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 369
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v1, :cond_2

    .line 370
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .line 371
    .local v0, "playlist":Lcom/google/android/music/medialist/SharedWithMeSongList;
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getOwnerProfilePhotoUrl()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/music/ui/ContainerHeaderView;->showOwnerProfilePicture(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$800(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 431
    .end local v0    # "playlist":Lcom/google/android/music/medialist/SharedWithMeSongList;
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/AsyncAlbumArtImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->setArtForSonglist(Lcom/google/android/music/medialist/SongList;)V

    goto/16 :goto_2
.end method
