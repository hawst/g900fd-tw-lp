.class public Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;
.super Lcom/google/api/client/json/GenericJson;
.source "TrackStat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/model/TrackStat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackEvent"
.end annotation


# instance fields
.field public mContextId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "context_id"
    .end annotation
.end field

.field public mContextType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "context_type"
    .end annotation
.end field

.field public mEventTimestampMicros:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "event_timestamp_micros"
    .end annotation
.end field

.field public mEventType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "event_type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;->mEventTimestampMicros:J

    return-void
.end method

.method public static parse(Landroid/database/Cursor;)Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;
    .locals 6
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;-><init>()V

    .line 71
    .local v0, "event":Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;
    const-string v2, "EventType"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;->mEventType:I

    .line 72
    const-string v2, "EventContextType"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;->mContextType:I

    .line 73
    const-string v2, "EventContextSourceId"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 74
    .local v1, "eventContextIdIdx":I
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;->mContextId:Ljava/lang/String;

    .line 77
    :cond_0
    const-string v2, "EventTimestampMillisec"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;->mEventTimestampMicros:J

    .line 79
    return-object v0
.end method
