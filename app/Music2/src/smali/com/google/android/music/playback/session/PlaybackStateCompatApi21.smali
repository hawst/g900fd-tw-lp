.class Lcom/google/android/music/playback/session/PlaybackStateCompatApi21;
.super Ljava/lang/Object;
.source "PlaybackStateCompatApi21.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# direct methods
.method public static newInstance(IJJFJJLjava/util/List;JLjava/lang/CharSequence;)Ljava/lang/Object;
    .locals 13
    .param p0, "state"    # I
    .param p1, "position"    # J
    .param p3, "updateTime"    # J
    .param p5, "speed"    # F
    .param p6, "bufferedPosition"    # J
    .param p8, "transportControls"    # J
    .param p11, "activeItemId"    # J
    .param p13, "error"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJFJJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;",
            ">;J",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 65
    .local p10, "customActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;>;"
    new-instance v2, Landroid/media/session/PlaybackState$Builder;

    invoke-direct {v2}, Landroid/media/session/PlaybackState$Builder;-><init>()V

    .local v2, "stateObj":Landroid/media/session/PlaybackState$Builder;
    move v3, p0

    move-wide v4, p1

    move/from16 v6, p5

    move-wide/from16 v7, p3

    .line 66
    invoke-virtual/range {v2 .. v8}, Landroid/media/session/PlaybackState$Builder;->setState(IJFJ)Landroid/media/session/PlaybackState$Builder;

    move-result-object v3

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, Landroid/media/session/PlaybackState$Builder;->setBufferedPosition(J)Landroid/media/session/PlaybackState$Builder;

    move-result-object v3

    move-wide/from16 v0, p8

    invoke-virtual {v3, v0, v1}, Landroid/media/session/PlaybackState$Builder;->setActions(J)Landroid/media/session/PlaybackState$Builder;

    move-result-object v3

    move-object/from16 v0, p13

    invoke-virtual {v3, v0}, Landroid/media/session/PlaybackState$Builder;->setErrorMessage(Ljava/lang/CharSequence;)Landroid/media/session/PlaybackState$Builder;

    .line 70
    invoke-interface/range {p10 .. p10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;

    .line 71
    .local v9, "action":Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;
    new-instance v3, Landroid/media/session/PlaybackState$CustomAction$Builder;

    invoke-virtual {v9}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;->getName()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;->getIcon()I

    move-result v6

    invoke-direct {v3, v4, v5, v6}, Landroid/media/session/PlaybackState$CustomAction$Builder;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {v9}, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/media/session/PlaybackState$CustomAction$Builder;->setExtras(Landroid/os/Bundle;)Landroid/media/session/PlaybackState$CustomAction$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/session/PlaybackState$CustomAction$Builder;->build()Landroid/media/session/PlaybackState$CustomAction;

    move-result-object v10

    .line 73
    .local v10, "customAction":Landroid/media/session/PlaybackState$CustomAction;
    invoke-virtual {v2, v10}, Landroid/media/session/PlaybackState$Builder;->addCustomAction(Landroid/media/session/PlaybackState$CustomAction;)Landroid/media/session/PlaybackState$Builder;

    goto :goto_0

    .line 75
    .end local v9    # "action":Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;
    .end local v10    # "customAction":Landroid/media/session/PlaybackState$CustomAction;
    :cond_0
    move-wide/from16 v0, p11

    invoke-virtual {v2, v0, v1}, Landroid/media/session/PlaybackState$Builder;->setActiveQueueItemId(J)Landroid/media/session/PlaybackState$Builder;

    .line 76
    invoke-virtual {v2}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v3

    return-object v3
.end method
