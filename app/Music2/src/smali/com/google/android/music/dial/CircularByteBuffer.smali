.class Lcom/google/android/music/dial/CircularByteBuffer;
.super Ljava/lang/Object;
.source "CircularByteBuffer.java"


# instance fields
.field private final mBuffer1:[Ljava/nio/ByteBuffer;

.field private final mBuffer2:[Ljava/nio/ByteBuffer;

.field private final mData:[B

.field private mEmpty:Z

.field private mReadMark:I

.field private mReadPos:I

.field private mWritePos:I


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "capacity"    # I

    .prologue
    const/4 v1, 0x1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-ge p1, v1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid capacity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; must be > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    .line 51
    new-array v0, v1, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer1:[Ljava/nio/ByteBuffer;

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer2:[Ljava/nio/ByteBuffer;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    .line 54
    iput-boolean v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    .line 55
    return-void
.end method

.method private bumpReadPos(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 626
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    .line 627
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 628
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    .line 632
    :cond_0
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    if-ne v0, v1, :cond_1

    .line 633
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 636
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->clear()V

    .line 642
    :cond_1
    :goto_0
    return-void

    .line 639
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    goto :goto_0
.end method

.method private bumpWritePos(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 649
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    .line 650
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 651
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    .line 653
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    .line 654
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    .line 655
    return-void
.end method

.method private decodeString([BIILjava/nio/charset/CharsetDecoder;)Ljava/lang/String;
    .locals 5
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "decoder"    # Ljava/nio/charset/CharsetDecoder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Lcom/google/android/music/dial/DataFormatException;
        }
    .end annotation

    .prologue
    .line 763
    invoke-virtual {p4}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 765
    :try_start_0
    invoke-static {p1, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 766
    .local v0, "byteBuf":Ljava/nio/ByteBuffer;
    sget-object v3, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {p4, v3}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 767
    sget-object v3, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {p4, v3}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 768
    invoke-virtual {p4, v0}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v1

    .line 769
    .local v1, "charBuf":Ljava/nio/CharBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 770
    new-instance v3, Lcom/google/android/music/dial/DataFormatException;

    const-string v4, "unexpected data after end of string"

    invoke-direct {v3, v4}, Lcom/google/android/music/dial/DataFormatException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/nio/charset/MalformedInputException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/nio/charset/UnmappableCharacterException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/nio/charset/CharacterCodingException; {:try_start_0 .. :try_end_0} :catch_5

    .line 773
    .end local v0    # "byteBuf":Ljava/nio/ByteBuffer;
    .end local v1    # "charBuf":Ljava/nio/CharBuffer;
    :catch_0
    move-exception v2

    .line 774
    .local v2, "ex":Ljava/nio/charset/IllegalCharsetNameException;
    new-instance v3, Ljava/io/UnsupportedEncodingException;

    invoke-virtual {v2}, Ljava/nio/charset/IllegalCharsetNameException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 772
    .end local v2    # "ex":Ljava/nio/charset/IllegalCharsetNameException;
    .restart local v0    # "byteBuf":Ljava/nio/ByteBuffer;
    .restart local v1    # "charBuf":Ljava/nio/CharBuffer;
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/nio/charset/MalformedInputException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/nio/charset/UnmappableCharacterException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/nio/charset/CharacterCodingException; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v3

    return-object v3

    .line 775
    .end local v0    # "byteBuf":Ljava/nio/ByteBuffer;
    .end local v1    # "charBuf":Ljava/nio/CharBuffer;
    :catch_1
    move-exception v2

    .line 776
    .local v2, "ex":Ljava/nio/charset/UnsupportedCharsetException;
    new-instance v3, Ljava/io/UnsupportedEncodingException;

    invoke-virtual {v2}, Ljava/nio/charset/UnsupportedCharsetException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 777
    .end local v2    # "ex":Ljava/nio/charset/UnsupportedCharsetException;
    :catch_2
    move-exception v2

    .line 778
    .local v2, "ex":Ljava/lang/IllegalStateException;
    new-instance v3, Lcom/google/android/music/dial/DataFormatException;

    invoke-direct {v3, v2}, Lcom/google/android/music/dial/DataFormatException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 779
    .end local v2    # "ex":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v2

    .line 780
    .local v2, "ex":Ljava/nio/charset/MalformedInputException;
    new-instance v3, Lcom/google/android/music/dial/DataFormatException;

    invoke-direct {v3, v2}, Lcom/google/android/music/dial/DataFormatException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 781
    .end local v2    # "ex":Ljava/nio/charset/MalformedInputException;
    :catch_4
    move-exception v2

    .line 782
    .local v2, "ex":Ljava/nio/charset/UnmappableCharacterException;
    new-instance v3, Lcom/google/android/music/dial/DataFormatException;

    invoke-direct {v3, v2}, Lcom/google/android/music/dial/DataFormatException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 783
    .end local v2    # "ex":Ljava/nio/charset/UnmappableCharacterException;
    :catch_5
    move-exception v2

    .line 784
    .local v2, "ex":Ljava/nio/charset/CharacterCodingException;
    new-instance v3, Lcom/google/android/music/dial/DataFormatException;

    invoke-direct {v3, v2}, Lcom/google/android/music/dial/DataFormatException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private readExtent()I
    .locals 2

    .prologue
    .line 598
    iget-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    if-eqz v0, :cond_0

    .line 599
    const/4 v0, 0x0

    .line 603
    :goto_0
    return v0

    .line 600
    :cond_0
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    if-ge v0, v1, :cond_1

    .line 601
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 603
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private readNextByteUnchecked()B
    .locals 3

    .prologue
    .line 661
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    aget-byte v0, v1, v2

    .line 662
    .local v0, "b":B
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    .line 663
    return v0
.end method

.method private writeExtent()I
    .locals 2

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    .line 617
    :goto_0
    return v0

    .line 614
    :cond_0
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    if-ge v0, v1, :cond_1

    .line 615
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private writeNextByteUnchecked(B)V
    .locals 2
    .param p1, "b"    # B

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    aput-byte p1, v0, v1

    .line 671
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpWritePos(I)V

    .line 672
    return-void
.end method


# virtual methods
.method public availableToRead()I
    .locals 2

    .prologue
    .line 570
    iget-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    if-eqz v0, :cond_0

    .line 571
    const/4 v0, 0x0

    .line 575
    :goto_0
    return v0

    .line 572
    :cond_0
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    if-ge v0, v1, :cond_1

    .line 573
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 575
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public availableToWrite()I
    .locals 2

    .prologue
    .line 557
    iget-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    .line 562
    :goto_0
    return v0

    .line 559
    :cond_0
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    if-ge v0, v1, :cond_1

    .line 560
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 517
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    .line 518
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    .line 519
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    .line 520
    return-void
.end method

.method public clearReadPosition()V
    .locals 1

    .prologue
    .line 534
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    .line 535
    iget-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    if-eqz v0, :cond_0

    .line 536
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    .line 538
    :cond_0
    return-void
.end method

.method public getReadableRegions()[Ljava/nio/ByteBuffer;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->isFull()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 185
    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    if-nez v1, :cond_1

    .line 187
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer1:[Ljava/nio/ByteBuffer;

    .line 188
    .local v0, "sources":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget-object v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v2, v2

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 191
    .end local v0    # "sources":[Ljava/nio/ByteBuffer;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer2:[Ljava/nio/ByteBuffer;

    .line 192
    .restart local v0    # "sources":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 193
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_0

    .line 195
    .end local v0    # "sources":[Ljava/nio/ByteBuffer;
    :cond_2
    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    if-ge v1, v2, :cond_3

    .line 197
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer1:[Ljava/nio/ByteBuffer;

    .line 198
    .restart local v0    # "sources":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 200
    .end local v0    # "sources":[Ljava/nio/ByteBuffer;
    :cond_3
    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    if-nez v1, :cond_4

    .line 201
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer1:[Ljava/nio/ByteBuffer;

    .line 202
    .restart local v0    # "sources":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 205
    .end local v0    # "sources":[Ljava/nio/ByteBuffer;
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer2:[Ljava/nio/ByteBuffer;

    .line 206
    .restart local v0    # "sources":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 207
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto/16 :goto_0
.end method

.method public getWritableRegions()[Ljava/nio/ByteBuffer;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 438
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->isFull()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439
    const/4 v0, 0x0

    .line 469
    :goto_0
    return-object v0

    .line 442
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 443
    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    if-nez v1, :cond_1

    .line 445
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer1:[Ljava/nio/ByteBuffer;

    .line 446
    .local v0, "targets":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget-object v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v2, v2

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 449
    .end local v0    # "targets":[Ljava/nio/ByteBuffer;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer2:[Ljava/nio/ByteBuffer;

    .line 450
    .restart local v0    # "targets":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 451
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_0

    .line 453
    .end local v0    # "targets":[Ljava/nio/ByteBuffer;
    :cond_2
    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    if-ge v1, v2, :cond_3

    .line 455
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer1:[Ljava/nio/ByteBuffer;

    .line 456
    .restart local v0    # "targets":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 458
    .end local v0    # "targets":[Ljava/nio/ByteBuffer;
    :cond_3
    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    if-nez v1, :cond_4

    .line 460
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer1:[Ljava/nio/ByteBuffer;

    .line 461
    .restart local v0    # "targets":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 464
    .end local v0    # "targets":[Ljava/nio/ByteBuffer;
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mBuffer2:[Ljava/nio/ByteBuffer;

    .line 465
    .restart local v0    # "targets":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 466
    iget-object v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto/16 :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 583
    iget-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    return v0
.end method

.method public isFull()Z
    .locals 2

    .prologue
    .line 590
    iget-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public markReadPosition()V
    .locals 1

    .prologue
    .line 527
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    .line 528
    return-void
.end method

.method public peekBytes([B)I
    .locals 9
    .param p1, "data"    # [B

    .prologue
    const/4 v5, -0x1

    .line 682
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v6

    array-length v7, p1

    if-ge v6, v7, :cond_1

    .line 709
    :cond_0
    :goto_0
    return v5

    .line 685
    :cond_1
    const/4 v0, 0x0

    .line 687
    .local v0, "found":Z
    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    .line 689
    .local v4, "pos":I
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v6

    array-length v7, p1

    add-int/lit8 v7, v7, -0x1

    sub-int v2, v6, v7

    .line 690
    .local v2, "last":I
    const/4 v3, 0x0

    .local v3, "len":I
    :goto_1
    if-ge v3, v2, :cond_3

    .line 691
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    array-length v6, p1

    if-ge v1, v6, :cond_2

    .line 692
    const/4 v0, 0x1

    .line 693
    iget-object v6, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    add-int v7, v4, v1

    iget-object v8, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v8, v8

    rem-int/2addr v7, v8

    aget-byte v6, v6, v7

    aget-byte v7, p1, v1

    if-eq v6, v7, :cond_4

    .line 694
    const/4 v0, 0x0

    .line 699
    :cond_2
    if-eqz v0, :cond_5

    .line 709
    .end local v1    # "j":I
    :cond_3
    if-eqz v0, :cond_0

    array-length v5, p1

    add-int/2addr v5, v3

    goto :goto_0

    .line 691
    .restart local v1    # "j":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 703
    :cond_5
    add-int/lit8 v4, v4, 0x1

    .line 704
    iget-object v6, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v6, v6

    if-ne v4, v6, :cond_6

    .line 705
    const/4 v4, 0x0

    .line 690
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public readByte()Ljava/lang/Byte;
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0
.end method

.method public readBytes([BII[B)Z
    .locals 5
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .param p4, "mask"    # [B

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v3

    if-ge v3, p3, :cond_0

    .line 88
    const/4 v3, 0x0

    .line 107
    :goto_0
    return v3

    .line 91
    :cond_0
    move v2, p3

    .line 92
    .local v2, "remain":I
    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readExtent()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 93
    .local v1, "n":I
    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    invoke-static {v3, v4, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    invoke-direct {p0, v1}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    .line 95
    sub-int/2addr v2, v1

    .line 96
    if-lez v2, :cond_1

    .line 97
    add-int/2addr p2, v1

    .line 98
    iget-object v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    invoke-static {v3, v4, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    invoke-direct {p0, v2}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    .line 102
    :cond_1
    if-eqz p4, :cond_2

    .line 103
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, p3, :cond_2

    .line 104
    aget-byte v3, p1, v0

    array-length v4, p4

    rem-int v4, v0, v4

    aget-byte v4, p4, v4

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, p1, v0

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 107
    .end local v0    # "i":I
    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public readBytes([B[B)Z
    .locals 2
    .param p1, "data"    # [B
    .param p2, "mask"    # [B

    .prologue
    .line 73
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/google/android/music/dial/CircularByteBuffer;->readBytes([BII[B)Z

    move-result v0

    return v0
.end method

.method public readFromSocketChannel(Ljava/nio/channels/SocketChannel;)I
    .locals 4
    .param p1, "channel"    # Ljava/nio/channels/SocketChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 496
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->getWritableRegions()[Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 497
    .local v1, "targets":[Ljava/nio/ByteBuffer;
    if-nez v1, :cond_0

    .line 499
    const/4 v0, 0x0

    .line 504
    :goto_0
    return v0

    .line 501
    :cond_0
    invoke-virtual {p1, v1}, Ljava/nio/channels/SocketChannel;->read([Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    long-to-int v0, v2

    .line 502
    .local v0, "count":I
    if-lez v0, :cond_1

    .line 503
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpWritePos(I)V

    goto :goto_0

    .line 509
    :cond_1
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2
.end method

.method public readLong()Ljava/lang/Long;
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 809
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v0

    if-ge v0, v5, :cond_0

    .line 810
    const/4 v0, 0x0

    .line 812
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-long v0, v0

    const/16 v2, 0x38

    shl-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    shl-long/2addr v2, v5

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    or-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public readShort()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 795
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 796
    const/4 v0, 0x0

    .line 798
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readNextByteUnchecked()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public readString(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;
    .locals 9
    .param p1, "byteCount"    # I
    .param p2, "mask"    # [B
    .param p3, "decoder"    # Ljava/nio/charset/CharsetDecoder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Lcom/google/android/music/dial/DataFormatException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 725
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v5

    if-ge v5, p1, :cond_0

    .line 726
    const/4 v4, 0x0

    .line 754
    :goto_0
    return-object v4

    .line 729
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->readExtent()I

    move-result v2

    .line 730
    .local v2, "n":I
    if-lt v2, p1, :cond_2

    .line 732
    if-eqz p2, :cond_1

    .line 733
    iget v3, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    .line 734
    .local v3, "pos":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, p1, :cond_1

    .line 735
    iget-object v5, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    aget-byte v6, v5, v3

    array-length v7, p2

    rem-int v7, v1, v7

    aget-byte v7, p2, v7

    xor-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v5, v3

    .line 736
    add-int/lit8 v3, v3, 0x1

    .line 734
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 739
    .end local v1    # "i":I
    .end local v3    # "pos":I
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v6, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    invoke-direct {p0, v5, v6, p1, p3}, Lcom/google/android/music/dial/CircularByteBuffer;->decodeString([BIILjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v4

    .line 740
    .local v4, "r":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    goto :goto_0

    .line 744
    .end local v4    # "r":Ljava/lang/String;
    :cond_2
    new-array v0, p1, [B

    .line 745
    .local v0, "data":[B
    iget-object v5, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v6, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    invoke-static {v5, v6, v0, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 746
    invoke-direct {p0, v2}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    .line 747
    iget-object v5, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v6, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    sub-int v7, p1, v2

    invoke-static {v5, v6, v0, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 748
    sub-int v5, p1, v2

    invoke-direct {p0, v5}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    .line 749
    if-eqz p2, :cond_3

    .line 750
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, p1, :cond_3

    .line 751
    aget-byte v5, v0, v1

    array-length v6, p2

    rem-int v6, v1, v6

    aget-byte v6, p2, v6

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v0, v1

    .line 750
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 754
    .end local v1    # "i":I
    :cond_3
    array-length v5, v0

    invoke-direct {p0, v0, v8, v5, p3}, Lcom/google/android/music/dial/CircularByteBuffer;->decodeString([BIILjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public rewindReadPosition()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 544
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    if-eq v0, v2, :cond_1

    .line 545
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    iget v1, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    if-eq v0, v1, :cond_0

    .line 546
    iget v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    iput v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadPos:I

    .line 547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mEmpty:Z

    .line 549
    :cond_0
    iput v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mReadMark:I

    .line 551
    :cond_1
    return-void
.end method

.method public skipBytes(I)Z
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 280
    :goto_0
    return v0

    .line 279
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    .line 280
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public updateReadPos(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 234
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    if-le p1, v0, :cond_1

    .line 235
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid value for count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_1
    if-lez p1, :cond_2

    .line 238
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    .line 240
    :cond_2
    return-void
.end method

.method public updateWritePos(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 479
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    array-length v0, v0

    if-le p1, v0, :cond_1

    .line 480
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid value for count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :cond_1
    if-lez p1, :cond_2

    .line 483
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpWritePos(I)V

    .line 485
    :cond_2
    return-void
.end method

.method public writeByte(B)Z
    .locals 2
    .param p1, "value"    # B

    .prologue
    const/4 v0, 0x1

    .line 359
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToWrite()I

    move-result v1

    if-ge v1, v0, :cond_0

    .line 360
    const/4 v0, 0x0

    .line 363
    :goto_0
    return v0

    .line 362
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    goto :goto_0
.end method

.method public writeBytes([BII[B)Z
    .locals 7
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .param p4, "mask"    # [B

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToWrite()I

    move-result v4

    if-ge v4, p3, :cond_0

    .line 321
    const/4 v4, 0x0

    .line 348
    :goto_0
    return v4

    .line 324
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeExtent()I

    move-result v4

    invoke-static {p3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 325
    .local v1, "n":I
    const/4 v0, 0x0

    .line 326
    .local v0, "i":I
    iget-object v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v5, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    invoke-static {p1, p2, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 327
    if-eqz p4, :cond_1

    .line 328
    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    .line 329
    .local v2, "pos":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 330
    iget-object v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    aget-byte v5, v4, v2

    array-length v6, p4

    rem-int v6, v0, v6

    aget-byte v6, p4, v6

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v4, v2

    .line 331
    add-int/lit8 v2, v2, 0x1

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 334
    .end local v2    # "pos":I
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpWritePos(I)V

    .line 335
    add-int/2addr p2, v1

    .line 336
    sub-int v3, p3, v1

    .line 337
    .local v3, "remain":I
    if-lez v3, :cond_3

    .line 338
    iget-object v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    iget v5, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    invoke-static {p1, p2, v4, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 339
    if-eqz p4, :cond_2

    .line 340
    iget v2, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mWritePos:I

    .line 341
    .restart local v2    # "pos":I
    :goto_2
    if-ge v0, p3, :cond_2

    .line 342
    iget-object v4, p0, Lcom/google/android/music/dial/CircularByteBuffer;->mData:[B

    aget-byte v5, v4, v2

    array-length v6, p4

    rem-int v6, v0, v6

    aget-byte v6, p4, v6

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v4, v2

    .line 343
    add-int/lit8 v2, v2, 0x1

    .line 341
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 346
    .end local v2    # "pos":I
    :cond_2
    invoke-direct {p0, v3}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpWritePos(I)V

    .line 348
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public writeBytes([B[B)Z
    .locals 2
    .param p1, "data"    # [B
    .param p2, "mask"    # [B

    .prologue
    .line 306
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/google/android/music/dial/CircularByteBuffer;->writeBytes([BII[B)Z

    move-result v0

    return v0
.end method

.method public writeLong(J)Z
    .locals 5
    .param p1, "value"    # J

    .prologue
    const/16 v4, 0x8

    const-wide/16 v2, 0xff

    .line 846
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToWrite()I

    move-result v0

    if-ge v0, v4, :cond_0

    .line 847
    const/4 v0, 0x0

    .line 857
    :goto_0
    return v0

    .line 849
    :cond_0
    const/16 v0, 0x38

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 850
    const/16 v0, 0x30

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 851
    const/16 v0, 0x28

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 852
    const/16 v0, 0x20

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 853
    const/16 v0, 0x18

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 854
    const/16 v0, 0x10

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 855
    shr-long v0, p1, v4

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 856
    and-long v0, p1, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 857
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeShort(I)Z
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 830
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToWrite()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 831
    const/4 v0, 0x0

    .line 835
    :goto_0
    return v0

    .line 833
    :cond_0
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 834
    and-int/lit16 v0, p1, 0xff

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeNextByteUnchecked(B)V

    .line 835
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeToSocketChannel(Ljava/nio/channels/SocketChannel;)I
    .locals 4
    .param p1, "channel"    # Ljava/nio/channels/SocketChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/google/android/music/dial/CircularByteBuffer;->getReadableRegions()[Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 252
    .local v1, "sources":[Ljava/nio/ByteBuffer;
    if-nez v1, :cond_0

    .line 254
    const/4 v0, 0x0

    .line 259
    :goto_0
    return v0

    .line 256
    :cond_0
    invoke-virtual {p1, v1}, Ljava/nio/channels/SocketChannel;->write([Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    long-to-int v0, v2

    .line 257
    .local v0, "count":I
    if-lez v0, :cond_1

    .line 258
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->bumpReadPos(I)V

    goto :goto_0

    .line 264
    :cond_1
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2
.end method
