.class public Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetRecommendedArtistsRequestJson.java"


# instance fields
.field mArtistBlackListIds:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist_blacklist"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSelectedArtistIds:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "selected_artists"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSelectedGenres:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "selected_genres"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static serialize(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)[B
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "selectedGenresIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p1, "selectedArtistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "artistBlackListIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v4, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;

    invoke-direct {v4}, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;-><init>()V

    .line 34
    .local v4, "result":Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v6, "selectedGenres":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/QuizGenreJson;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 36
    .local v7, "type":Ljava/lang/String;
    new-instance v3, Lcom/google/android/music/cloudclient/QuizGenreJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/QuizGenreJson;-><init>()V

    .line 37
    .local v3, "quizGenreJson":Lcom/google/android/music/cloudclient/QuizGenreJson;
    iput-object v7, v3, Lcom/google/android/music/cloudclient/QuizGenreJson;->mType:Ljava/lang/String;

    .line 38
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 40
    .end local v3    # "quizGenreJson":Lcom/google/android/music/cloudclient/QuizGenreJson;
    .end local v7    # "type":Ljava/lang/String;
    :cond_0
    iput-object v6, v4, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;->mSelectedGenres:Ljava/util/List;

    .line 42
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v5, "selectedArtists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 44
    .local v1, "artistId":Ljava/lang/String;
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 46
    .end local v1    # "artistId":Ljava/lang/String;
    :cond_1
    iput-object v5, v4, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;->mSelectedArtistIds:Ljava/util/List;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v0, "artistBlackLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 50
    .restart local v1    # "artistId":Ljava/lang/String;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 52
    .end local v1    # "artistId":Ljava/lang/String;
    :cond_2
    iput-object v0, v4, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;->mArtistBlackListIds:Ljava/util/List;

    .line 53
    invoke-static {v4}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v8

    return-object v8
.end method
