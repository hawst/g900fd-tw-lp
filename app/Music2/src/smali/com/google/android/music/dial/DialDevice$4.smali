.class Lcom/google/android/music/dial/DialDevice$4;
.super Ljava/lang/Object;
.source "DialDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialDevice;->sendMessage(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialDevice;

.field final synthetic val$callback:Lcom/google/android/music/dial/DialDevice$Callback;

.field final synthetic val$payload:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/DialDevice$Callback;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice$4;->this$0:Lcom/google/android/music/dial/DialDevice;

    iput-object p2, p0, Lcom/google/android/music/dial/DialDevice$4;->val$callback:Lcom/google/android/music/dial/DialDevice$Callback;

    iput-object p3, p0, Lcom/google/android/music/dial/DialDevice$4;->val$payload:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 286
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$4;->this$0:Lcom/google/android/music/dial/DialDevice;

    # invokes: Lcom/google/android/music/dial/DialDevice;->getSocketInternal()Lcom/google/android/music/dial/WebSocket;
    invoke-static {v1}, Lcom/google/android/music/dial/DialDevice;->access$000(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;

    move-result-object v0

    .line 287
    .local v0, "socket":Lcom/google/android/music/dial/WebSocket;
    if-nez v0, :cond_2

    .line 288
    # getter for: Lcom/google/android/music/dial/DialDevice;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialDevice;->access$400()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    const-string v1, "DialDevice"

    const-string v2, "sendMessage failed - socket is null."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$4;->val$callback:Lcom/google/android/music/dial/DialDevice$Callback;

    if-eqz v1, :cond_1

    .line 292
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$4;->val$callback:Lcom/google/android/music/dial/DialDevice$Callback;

    const-string v2, "Error connecting to remote device"

    invoke-interface {v1, v2}, Lcom/google/android/music/dial/DialDevice$Callback;->onError(Ljava/lang/String;)V

    .line 305
    :cond_1
    :goto_0
    return-void

    .line 295
    :cond_2
    # getter for: Lcom/google/android/music/dial/DialDevice;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialDevice;->access$400()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 296
    const-string v1, "DialDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessage - socket is valid.  connected. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/dial/WebSocket;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$4;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice$4;->val$payload:Ljava/lang/String;

    # invokes: Lcom/google/android/music/dial/DialDevice;->sendMessage(Lcom/google/android/music/dial/WebSocket;Ljava/lang/String;)Z
    invoke-static {v1, v0, v2}, Lcom/google/android/music/dial/DialDevice;->access$700(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/WebSocket;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$4;->val$callback:Lcom/google/android/music/dial/DialDevice$Callback;

    if-eqz v1, :cond_1

    .line 301
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$4;->val$callback:Lcom/google/android/music/dial/DialDevice$Callback;

    const-string v2, "Error sending request to remote device"

    invoke-interface {v1, v2}, Lcom/google/android/music/dial/DialDevice$Callback;->onError(Ljava/lang/String;)V

    goto :goto_0
.end method
