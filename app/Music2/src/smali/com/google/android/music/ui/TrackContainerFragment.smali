.class public Lcom/google/android/music/ui/TrackContainerFragment;
.super Lcom/google/android/music/ui/BaseTrackListFragment;
.source "TrackContainerFragment.java"


# instance fields
.field protected mAlbumId:J

.field protected mAlbumMetajamId:Ljava/lang/String;

.field private mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

.field protected mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

.field private mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

.field protected mHighlightTrackMetajamId:Ljava/lang/String;

.field protected mHighlightTrackSongId:J

.field protected mSongList:Lcom/google/android/music/medialist/SongList;

.field protected mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

.field private mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

.field protected mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 64
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    .line 59
    iput-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumId:J

    .line 61
    iput-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/TrackContainerFragment;)Lcom/google/android/music/ui/ContainerHeaderView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackContainerFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    return-object v0
.end method

.method private static buildEmptyVideoCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 6
    .param p0, "inflater"    # Landroid/view/LayoutInflater;
    .param p1, "lv"    # Landroid/widget/ListView;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 329
    const v2, 0x7f040092

    const/4 v3, 0x0

    invoke-virtual {p0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 332
    .local v0, "cluster":Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-direct {v1, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 333
    .local v1, "clusterMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    invoke-virtual {v0, v1, v5, v5}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 334
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 335
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hideHeader()V

    .line 336
    return-object v0
.end method

.method private extractIds()V
    .locals 1

    .prologue
    .line 282
    new-instance v0, Lcom/google/android/music/ui/TrackContainerFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/TrackContainerFragment$2;-><init>(Lcom/google/android/music/ui/TrackContainerFragment;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 307
    return-void
.end method

.method private getVideoClusterInfo(Landroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 388
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;->buildVideoDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;

    move-result-object v5

    .line 390
    .local v5, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->getScreenColumnCount(Landroid/content/res/Resources;Lcom/google/android/music/preferences/MusicPreferences;)I

    move-result v7

    .line 391
    .local v7, "nbColumns":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 393
    .local v8, "nbRows":I
    new-instance v0, Lcom/google/android/music/ui/Cluster;

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_VIDEO_THUMBNAIL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v3, 0x7f0b00b3

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/TrackContainerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v0 .. v10}, Lcom/google/android/music/ui/Cluster;-><init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;Lcom/google/android/music/store/ContainerDescriptor;)V

    return-object v0
.end method

.method private populateVideoCluster(Landroid/database/Cursor;)V
    .locals 19
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 340
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-nez v14, :cond_0

    .line 341
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 384
    :goto_0
    return-void

    .line 344
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/ui/TrackContainerFragment;->getVideoClusterInfo(Landroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;

    move-result-object v2

    .line 345
    .local v2, "clusterInfo":Lcom/google/android/music/ui/Cluster;
    invoke-virtual {v2}, Lcom/google/android/music/ui/Cluster;->getNbColumns()I

    move-result v10

    .line 346
    .local v10, "nbColumns":I
    invoke-virtual {v2}, Lcom/google/android/music/ui/Cluster;->getFullContent()Ljava/util/List;

    move-result-object v6

    .line 347
    .local v6, "fullDocList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {v2}, Lcom/google/android/music/ui/Cluster;->getVisibleContent()Ljava/util/List;

    move-result-object v13

    .line 349
    .local v13, "visibleDocList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {v2}, Lcom/google/android/music/ui/Cluster;->getCardType()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    .line 351
    .local v1, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    add-int/2addr v14, v10

    add-int/lit8 v14, v14, -0x1

    div-int v11, v14, v10

    .line 352
    .local v11, "nbRows":I
    new-instance v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v14

    mul-int/2addr v14, v10

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v15

    mul-int/2addr v15, v11

    invoke-direct {v3, v14, v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 356
    .local v3, "clusterMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v5

    .line 357
    .local v5, "docCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v5, :cond_1

    .line 358
    div-int v14, v7, v10

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v15

    mul-int v12, v14, v15

    .line 359
    .local v12, "row":I
    rem-int v14, v7, v10

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v15

    mul-int v4, v14, v15

    .line 360
    .local v4, "column":I
    invoke-virtual {v3, v1, v4, v12}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    .line 357
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 363
    .end local v4    # "column":I
    .end local v12    # "row":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/4 v15, 0x0

    invoke-virtual {v2}, Lcom/google/android/music/ui/Cluster;->getItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v3, v13, v15, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;)V

    .line 367
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v14

    if-nez v14, :cond_2

    .line 368
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    invoke-virtual {v14, v15}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V

    .line 371
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 372
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v14

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v15

    sub-int v9, v14, v15

    .line 373
    .local v9, "moreItems":I
    if-lez v9, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0b0260

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 375
    .local v8, "more":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v2}, Lcom/google/android/music/ui/Cluster;->getTitle()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v8}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->showHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v2}, Lcom/google/android/music/ui/Cluster;->getMoreOnClickListener()Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMoreClickHandler(Landroid/view/View$OnClickListener;)V

    .line 378
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v14

    if-lez v14, :cond_4

    .line 379
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    goto/16 :goto_0

    .line 373
    .end local v8    # "more":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 381
    .restart local v8    # "more":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 382
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hideHeader()V

    goto/16 :goto_0
.end method


# virtual methods
.method public getAlbumId()J
    .locals 2

    .prologue
    .line 278
    iget-wide v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumId:J

    return-wide v0
.end method

.method public getAlbumMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method protected init(Lcom/google/android/music/medialist/MediaList;Lcom/google/android/music/ui/cardlib/model/Document;[Ljava/lang/String;ZJLjava/lang/String;)V
    .locals 1
    .param p1, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p2, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "async"    # Z
    .param p5, "highlightTrackSongId"    # J
    .param p7, "highlightTrackMetajamId"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-super {p0, p1, p3, p4}, Lcom/google/android/music/ui/BaseTrackListFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 85
    iput-wide p5, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    .line 86
    iput-object p7, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 88
    return-void
.end method

.method protected initEmptyScreen()V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method protected initVideoCluster()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 316
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/preferences/MusicPreferences;->isYouTubeAvailable(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 321
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 322
    .local v2, "lv":Landroid/widget/ListView;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/TrackContainerFragment;->buildEmptyVideoCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 323
    iget-object v3, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v5, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 324
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 325
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    const/16 v3, 0x64

    invoke-virtual {v1, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/music/ui/TrackListAdapter;

    const v1, 0x7f040073

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/google/android/music/ui/TrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    .line 191
    .local v0, "adapter":Lcom/google/android/music/ui/TrackListAdapter;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->setMaterialEnabled(Z)V

    .line 193
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v1, p1}, Lcom/google/android/music/ui/ContainerHeaderView;->setCursor(Landroid/database/Cursor;)V

    .line 196
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 197
    iget-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/ui/TrackListAdapter;->setHighlightTrack(J)V

    .line 201
    :cond_1
    :goto_0
    return-object v0

    .line 198
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->setHighlightTrack(Ljava/lang/String;)V

    goto :goto_0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 6

    .prologue
    .line 175
    new-instance v0, Lcom/google/android/music/ui/TrackListAdapter;

    const v1, 0x7f040073

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/music/ui/TrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILcom/google/android/music/medialist/MediaList;)V

    .line 177
    .local v0, "adapter":Lcom/google/android/music/ui/TrackListAdapter;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->setMaterialEnabled(Z)V

    .line 179
    iget-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 180
    iget-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/ui/TrackListAdapter;->setHighlightTrack(J)V

    .line 184
    :cond_0
    :goto_0
    return-object v0

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->setHighlightTrack(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 6
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 206
    const/16 v1, 0x64

    if-ne p1, v1, :cond_2

    .line 210
    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumMetajamId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 211
    new-instance v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumMetajamId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 218
    .local v0, "songList":Lcom/google/android/music/medialist/SongList;
    :goto_0
    new-instance v1, Lcom/google/android/music/ui/MediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 221
    .end local v0    # "songList":Lcom/google/android/music/medialist/SongList;
    :goto_1
    return-object v1

    .line 212
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 213
    new-instance v0, Lcom/google/android/music/medialist/AlbumSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mAlbumId:J

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .restart local v0    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_0

    .line 215
    .end local v0    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    const-string v1, "TrackContainerFragment"

    const-string v2, "Trying to initialize video cluster without a valid video id."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    const/4 v1, 0x0

    goto :goto_1

    .line 221
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseTrackListFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;

    move-result-object v1

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 93
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/BaseTrackListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mView:Landroid/view/View;

    .line 95
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongList;

    iput-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 97
    const v0, 0x7f040026

    invoke-virtual {p1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/ContainerHeaderView;

    iput-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    .line 98
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->setClickable(Z)V

    .line 99
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/ContainerHeaderView;->setFragment(Lcom/google/android/music/ui/BaseListFragment;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ContainerHeaderView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ContainerHeaderView;->setContainerDocument(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ContainerHeaderView;->setMusicPreferences(Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mView:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseTrackListView;

    iput-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    .line 112
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/BaseTrackListView;->setImportantForAccessibility(I)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/music/ui/BaseTrackListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 116
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setHeaderDividersEnabled(Z)V

    .line 117
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    new-instance v1, Lcom/google/android/music/ui/TrackContainerFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/TrackContainerFragment$1;-><init>(Lcom/google/android/music/ui/TrackContainerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/BaseTrackListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setFastScrollEnabled(Z)V

    .line 146
    invoke-direct {p0}, Lcom/google/android/music/ui/TrackContainerFragment;->extractIds()V

    .line 148
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 11
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v10, -0x1

    .line 227
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v6

    const/16 v7, 0x64

    if-ne v6, v7, :cond_2

    .line 228
    if-nez p2, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/TrackContainerFragment;->populateVideoCluster(Landroid/database/Cursor;)V

    goto :goto_0

    .line 231
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseTrackListFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 232
    iget-object v6, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    if-eqz v6, :cond_3

    .line 233
    iget-object v6, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v6, p2}, Lcom/google/android/music/ui/ContainerHeaderView;->setCursor(Landroid/database/Cursor;)V

    .line 235
    :cond_3
    iget-wide v6, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_4
    if-eqz p2, :cond_0

    .line 237
    const-string v6, "Nid"

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 238
    .local v2, "trackMetajamIdIdx":I
    const-string v6, "SongId"

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 239
    .local v3, "trackSongIdIdx":I
    if-eq v2, v10, :cond_7

    if-eq v3, v10, :cond_7

    .line 240
    :cond_5
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 241
    if-eq v2, v10, :cond_8

    invoke-interface {p2, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    move v0, v4

    .line 244
    .local v0, "isMetajamIdToHighlight":Z
    :goto_1
    if-eq v3, v10, :cond_9

    invoke-interface {p2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_9

    iget-wide v6, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHighlightTrackSongId:J

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_9

    move v1, v4

    .line 247
    .local v1, "isSongIdToHighlight":Z
    :goto_2
    if-nez v0, :cond_6

    if-eqz v1, :cond_5

    .line 248
    :cond_6
    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mTrackList:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/music/ui/BaseTrackListView;->scrollToPositionAsCentered(I)V

    .line 254
    .end local v0    # "isMetajamIdToHighlight":Z
    .end local v1    # "isSongIdToHighlight":Z
    :cond_7
    invoke-interface {p2, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    :cond_8
    move v0, v5

    .line 241
    goto :goto_1

    .restart local v0    # "isMetajamIdToHighlight":Z
    :cond_9
    move v1, v5

    .line 244
    goto :goto_2
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 44
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/TrackContainerFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 262
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseTrackListFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ContainerHeaderView;->setCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 153
    invoke-super {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->onStart()V

    .line 154
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->onStart()V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 160
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ContainerHeaderView;->onStop()V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/google/android/music/ui/TrackContainerFragment;->mVideoCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->clearThumbnails()V

    .line 170
    :cond_1
    invoke-super {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->onStop()V

    .line 171
    return-void
.end method
