.class public abstract Lcom/google/android/music/net/INetworkMonitor$Stub;
.super Landroid/os/Binder;
.source "INetworkMonitor.java"

# interfaces
.implements Lcom/google/android/music/net/INetworkMonitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/net/INetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/net/INetworkMonitor$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/INetworkMonitor;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.google.android.music.net.INetworkMonitor"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/music/net/INetworkMonitor;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/google/android/music/net/INetworkMonitor;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/google/android/music/net/INetworkMonitor$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/google/android/music/net/INetworkMonitor$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 148
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 42
    :sswitch_0
    const-string v2, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v4, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->isConnected()Z

    move-result v1

    .line 49
    .local v1, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 55
    .end local v1    # "_result":Z
    :sswitch_2
    const-string v4, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->hasMobileOrMeteredConnection()Z

    move-result v1

    .line 57
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 58
    if-eqz v1, :cond_1

    move v2, v3

    :cond_1
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 63
    .end local v1    # "_result":Z
    :sswitch_3
    const-string v4, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->hasWifiConnection()Z

    move-result v1

    .line 65
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    if-eqz v1, :cond_2

    move v2, v3

    :cond_2
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 71
    .end local v1    # "_result":Z
    :sswitch_4
    const-string v4, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->hasHighSpeedConnection()Z

    move-result v1

    .line 73
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    if-eqz v1, :cond_3

    move v2, v3

    :cond_3
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v1    # "_result":Z
    :sswitch_5
    const-string v4, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->isStreamingAvailable()Z

    move-result v1

    .line 81
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    if-eqz v1, :cond_4

    move v2, v3

    :cond_4
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 87
    .end local v1    # "_result":Z
    :sswitch_6
    const-string v4, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->isDownloadingAvailable()Z

    move-result v1

    .line 89
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 90
    if-eqz v1, :cond_5

    move v2, v3

    :cond_5
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 95
    .end local v1    # "_result":Z
    :sswitch_7
    const-string v2, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/net/IStreamabilityChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/IStreamabilityChangeListener;

    move-result-object v0

    .line 98
    .local v0, "_arg0":Lcom/google/android/music/net/IStreamabilityChangeListener;
    invoke-virtual {p0, v0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->registerStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    .line 99
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 104
    .end local v0    # "_arg0":Lcom/google/android/music/net/IStreamabilityChangeListener;
    :sswitch_8
    const-string v2, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/net/IStreamabilityChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/IStreamabilityChangeListener;

    move-result-object v0

    .line 107
    .restart local v0    # "_arg0":Lcom/google/android/music/net/IStreamabilityChangeListener;
    invoke-virtual {p0, v0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->unregisterStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V

    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 113
    .end local v0    # "_arg0":Lcom/google/android/music/net/IStreamabilityChangeListener;
    :sswitch_9
    const-string v2, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/net/IDownloadabilityChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/IDownloadabilityChangeListener;

    move-result-object v0

    .line 116
    .local v0, "_arg0":Lcom/google/android/music/net/IDownloadabilityChangeListener;
    invoke-virtual {p0, v0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->registerDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V

    .line 117
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 122
    .end local v0    # "_arg0":Lcom/google/android/music/net/IDownloadabilityChangeListener;
    :sswitch_a
    const-string v2, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/net/IDownloadabilityChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/IDownloadabilityChangeListener;

    move-result-object v0

    .line 125
    .restart local v0    # "_arg0":Lcom/google/android/music/net/IDownloadabilityChangeListener;
    invoke-virtual {p0, v0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->unregisterDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V

    .line 126
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 131
    .end local v0    # "_arg0":Lcom/google/android/music/net/IDownloadabilityChangeListener;
    :sswitch_b
    const-string v2, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/net/INetworkChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/INetworkChangeListener;

    move-result-object v0

    .line 134
    .local v0, "_arg0":Lcom/google/android/music/net/INetworkChangeListener;
    invoke-virtual {p0, v0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->registerNetworkChangeListener(Lcom/google/android/music/net/INetworkChangeListener;)V

    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 140
    .end local v0    # "_arg0":Lcom/google/android/music/net/INetworkChangeListener;
    :sswitch_c
    const-string v2, "com.google.android.music.net.INetworkMonitor"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/net/INetworkChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/net/INetworkChangeListener;

    move-result-object v0

    .line 143
    .restart local v0    # "_arg0":Lcom/google/android/music/net/INetworkChangeListener;
    invoke-virtual {p0, v0}, Lcom/google/android/music/net/INetworkMonitor$Stub;->unregisterNetworkChangeListener(Lcom/google/android/music/net/INetworkChangeListener;)V

    .line 144
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
