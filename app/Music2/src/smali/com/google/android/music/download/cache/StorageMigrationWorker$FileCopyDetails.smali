.class Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;
.super Ljava/lang/Object;
.source "StorageMigrationWorker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/StorageMigrationWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileCopyDetails"
.end annotation


# instance fields
.field currentVolume:Ljava/util/UUID;

.field fileSize:J

.field id:J

.field path:Ljava/lang/String;

.field streamFidelity:I

.field type:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/download/cache/StorageMigrationWorker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$1;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;-><init>()V

    return-void
.end method


# virtual methods
.method isValid()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->currentVolume:Ljava/util/UUID;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->path:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FileCopyDetails{currentVolume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->currentVolume:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
