.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SingleTrackRemovedInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;


# instance fields
.field public container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

.field public removedTrackId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1641
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1642
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1647
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1642
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1662
    const/4 v0, 0x0

    .line 1663
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->removedTrackId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1664
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->removedTrackId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1667
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    if-eqz v1, :cond_1

    .line 1668
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1671
    :cond_1
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->cachedSize:I

    .line 1672
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1680
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1681
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1685
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1686
    :sswitch_0
    return-object p0

    .line 1691
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->removedTrackId:Ljava/lang/String;

    goto :goto_0

    .line 1695
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    if-nez v1, :cond_1

    .line 1696
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1698
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1681
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1638
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1652
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->removedTrackId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1653
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->removedTrackId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1655
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    if-eqz v0, :cond_1

    .line 1656
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1658
    :cond_1
    return-void
.end method
