.class Lcom/google/android/music/leanback/LeanbackPlayActivity$1;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "classname"    # Landroid/content/ComponentName;
    .param p2, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 165
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$100()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Service connected"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-static {p2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v3

    # setter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$302(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 170
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfo()V
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$400(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    .line 171
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->refreshNow()J
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$500(Lcom/google/android/music/leanback/LeanbackPlayActivity;)J

    move-result-wide v0

    .line 172
    .local v0, "next":J
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->queueNextRefresh(J)V
    invoke-static {v2, v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$600(Lcom/google/android/music/leanback/LeanbackPlayActivity;J)V

    .line 173
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    new-instance v3, Lcom/google/android/music/leanback/LeanbackPlayActivity$1$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$1$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$1;)V

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 179
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$302(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 184
    return-void
.end method
