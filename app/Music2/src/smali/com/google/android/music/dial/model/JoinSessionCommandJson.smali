.class public Lcom/google/android/music/dial/model/JoinSessionCommandJson;
.super Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;
.source "JoinSessionCommandJson.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;-><init>()V

    .line 14
    iget-object v0, p0, Lcom/google/android/music/dial/model/JoinSessionCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "session:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 15
    iget-object v0, p0, Lcom/google/android/music/dial/model/JoinSessionCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "joinSession"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 16
    return-void
.end method
