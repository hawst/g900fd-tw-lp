.class public Lcom/google/android/music/utils/AlbumArtUtils;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/AlbumArtUtils$4;,
        Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;,
        Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;,
        Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;,
        Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;,
        Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;,
        Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;,
        Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;,
        Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;
    }
.end annotation


# static fields
.field private static final BUCKET_SIZES:[I

.field private static final LOGV:Z

.field private static mBackground_DefaultArt:Landroid/graphics/Bitmap;

.field private static mBackground_DefaultArtistArt:Landroid/graphics/Bitmap;

.field private static mBackground_RadioArt:Landroid/graphics/Bitmap;

.field private static mCacheDir:Ljava/io/File;

.field private static mLuckyBadge:Landroid/graphics/Bitmap;

.field private static mMixBadgeLarge:Landroid/graphics/Bitmap;

.field private static mMixBadgeSmall:Landroid/graphics/Bitmap;

.field private static mRadioBadgeLarge:Landroid/graphics/Bitmap;

.field private static mRadioBadgeSmall:Landroid/graphics/Bitmap;

.field private static mVideoBadge:Landroid/graphics/Bitmap;

.field private static mYouTubeOverlayDrawable:Landroid/graphics/drawable/Drawable;

.field private static final sAlbumIdCols:[Ljava/lang/String;

.field private static final sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

.field private static volatile sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

.field private static sCacheBucketMediumSize:I

.field private static sCacheBucketSmallSize:I

.field private static sCacheMediumBucketMaxDimen:I

.field private static sCacheSmallBucketMaxDimen:I

.field private static final sCachedBitmapLocks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            ">;"
        }
    .end annotation
.end field

.field private static sCachedRezinBitmap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/graphics/Point;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static sCanStartIFL:Z

.field private static final sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

.field private static sHprofDumped:Z

.field private static sMaxAlbumArtSize:I

.field private static sMaxAlbumArtSizeInitalized:Z

.field private static final sSizeCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sSizeCacheRequests:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static sTotalSizeCacheRequests:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 117
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/utils/AlbumArtUtils;->LOGV:Z

    .line 151
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    .line 152
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    .line 201
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    .line 208
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCache:Ljava/util/HashMap;

    .line 211
    sput v3, Lcom/google/android/music/utils/AlbumArtUtils;->sTotalSizeCacheRequests:I

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCacheRequests:Ljava/util/HashMap;

    .line 223
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedRezinBitmap:Ljava/util/HashMap;

    .line 548
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    .line 570
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "album_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sAlbumIdCols:[Ljava/lang/String;

    .line 582
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 583
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 584
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 586
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 587
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 588
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 2866
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->BUCKET_SIZES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x82
        0x100
        0x200
        0x578
    .end array-data
.end method

.method static synthetic access$100(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Landroid/graphics/Bitmap;

    .prologue
    .line 70
    invoke-static {p0}, Lcom/google/android/music/utils/AlbumArtUtils;->throwIfRecycled(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sAlbumIdCols:[Ljava/lang/String;

    return-object v0
.end method

.method private static albumArtStyleToArtCacheEntryType(I)I
    .locals 3
    .param p0, "style"    # I

    .prologue
    const/16 v0, 0x9

    .line 1810
    and-int/lit8 p0, p0, 0xf

    .line 1812
    packed-switch p0, :pswitch_data_0

    .line 1841
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported faux albumart style: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1814
    :pswitch_1
    const/4 v0, 0x2

    .line 1838
    :goto_0
    :pswitch_2
    return v0

    .line 1817
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1820
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 1823
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 1826
    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 1829
    :pswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 1835
    :pswitch_8
    const/16 v0, 0xa

    goto :goto_0

    .line 1812
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_2
        :pswitch_8
        :pswitch_2
    .end packed-switch
.end method

.method private static appendSizeToExternalUrl(Ljava/lang/String;II)Ljava/lang/String;
    .locals 8
    .param p0, "albumArtUrl"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v7, 0x0

    .line 790
    const/16 v3, 0x2f

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 791
    .local v1, "indexOfLastSlash":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 792
    .local v0, "fileName":Ljava/lang/String;
    invoke-virtual {p0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 793
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 794
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/w%d-h%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 796
    :cond_0
    return-object p0
.end method

.method private static cacheable(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "album_id"    # J
    .param p2, "album"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;

    .prologue
    .line 988
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lcom/google/android/music/utils/AlbumArtUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p3}, Lcom/google/android/music/utils/AlbumArtUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static cacheable(Ljava/lang/String;)Z
    .locals 1
    .param p0, "remoteUrl"    # Ljava/lang/String;

    .prologue
    .line 995
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "w"    # I
    .param p1, "h"    # I
    .param p2, "aliased"    # Landroid/graphics/Bitmap;
    .param p3, "cropToSquare"    # Z

    .prologue
    .line 907
    const/4 v1, 0x0

    .line 908
    .local v1, "result":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_1

    .line 911
    if-eqz p3, :cond_0

    .line 912
    sub-int v2, p0, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v3, 0x5

    if-ge v2, v3, :cond_2

    .line 913
    invoke-static {p2}, Lcom/google/android/music/utils/AlbumArtUtils;->cropToSquare(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 919
    :cond_0
    sget-object v2, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-virtual {v2, p0, p1, v3}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 920
    .local v0, "copy":Landroid/graphics/Bitmap;
    invoke-static {p2, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleToFit(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 921
    move-object v1, v0

    .line 923
    .end local v0    # "copy":Landroid/graphics/Bitmap;
    :cond_1
    return-object v1

    .line 916
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Only square cropping is supported."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static createAlbumIdIteratorFactoryForContentUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 692
    if-nez p1, :cond_0

    .line 693
    const/4 v0, 0x0

    .line 695
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/music/utils/AlbumArtUtils$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/utils/AlbumArtUtils$1;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private static createArtCacheKey(IJ)Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .locals 3
    .param p0, "style"    # I
    .param p1, "artwork_id"    # J

    .prologue
    .line 1803
    new-instance v0, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    invoke-static {p0}, Lcom/google/android/music/utils/AlbumArtUtils;->albumArtStyleToArtCacheEntryType(I)I

    move-result v1

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;-><init>(IJ)V

    return-object v0
.end method

.method public static createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "width"    # I
    .param p1, "height"    # I
    .param p2, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 1527
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static createFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "drawLabels"    # Z
    .param p3, "seed"    # J
    .param p5, "w"    # I
    .param p6, "h"    # I
    .param p7, "mainLabel"    # Ljava/lang/String;
    .param p8, "subLabel"    # Ljava/lang/String;
    .param p9, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p10, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;

    .prologue
    .line 1948
    const/16 v2, 0x80

    .line 1949
    .local v2, "MIN_RENDERING_SIZE":I
    move/from16 v0, p5

    move/from16 v1, p6

    if-ne v0, v1, :cond_1

    const/16 v3, 0x80

    move/from16 v0, p5

    if-lt v0, v3, :cond_0

    const/16 v3, 0x80

    move/from16 v0, p6

    if-ge v0, v3, :cond_1

    .line 1951
    :cond_0
    const/16 v8, 0x80

    const/16 v9, 0x80

    move-object v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    invoke-static/range {v3 .. v13}, Lcom/google/android/music/utils/AlbumArtUtils;->createFauxAlbumArt2(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1954
    .local v14, "bm":Landroid/graphics/Bitmap;
    move/from16 v0, p5

    move/from16 v1, p6

    invoke-static {v14, v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleToSize(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1956
    .end local v14    # "bm":Landroid/graphics/Bitmap;
    :goto_0
    return-object v3

    :cond_1
    invoke-static/range {p0 .. p10}, Lcom/google/android/music/utils/AlbumArtUtils;->createFauxAlbumArt2(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0
.end method

.method private static createFauxAlbumArt2(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "drawLabels"    # Z
    .param p3, "seed"    # J
    .param p5, "w"    # I
    .param p6, "h"    # I
    .param p7, "mainLabel"    # Ljava/lang/String;
    .param p8, "subLabel"    # Ljava/lang/String;
    .param p9, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p10, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;

    .prologue
    .line 1963
    sget-object v3, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v3, v0, v1, v4}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1964
    .local v14, "bm":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .local v2, "canvas":Landroid/graphics/Canvas;
    move-object v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    .line 1965
    invoke-static/range {v2 .. v13}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxAlbumArt(Landroid/graphics/Canvas;Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)V

    .line 1967
    return-object v14
.end method

.method private static cropToSquare(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "srcBmp"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 927
    if-nez p0, :cond_1

    .line 928
    const/4 p0, 0x0

    .line 945
    .end local p0    # "srcBmp":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 931
    .restart local p0    # "srcBmp":Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 936
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 937
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {p0, v1, v4, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "dstBmp":Landroid/graphics/Bitmap;
    :goto_1
    move-object p0, v0

    .line 945
    goto :goto_0

    .line 941
    .end local v0    # "dstBmp":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-static {p0, v4, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0    # "dstBmp":Landroid/graphics/Bitmap;
    goto :goto_1
.end method

.method public static drawFauxAlbumArt(Landroid/graphics/Canvas;Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)V
    .locals 14
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "style"    # I
    .param p3, "drawLabels"    # Z
    .param p4, "seed"    # J
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "mainLabel"    # Ljava/lang/String;
    .param p9, "subLabel"    # Ljava/lang/String;
    .param p10, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p11, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;

    .prologue
    .line 2006
    and-int/lit8 v13, p2, 0xf

    .line 2007
    .local v13, "fauxArtType":I
    packed-switch v13, :pswitch_data_0

    .line 2042
    :cond_0
    :goto_0
    :pswitch_0
    const/16 v2, 0x8

    if-ne v13, v2, :cond_2

    const/4 v12, 0x1

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-static/range {v2 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->drawModernFauxAlbumArt(Landroid/graphics/Canvas;Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Z)V

    .line 2044
    :cond_1
    :goto_2
    return-void

    :pswitch_1
    move-object v3, p0

    move-object v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    .line 2012
    invoke-static/range {v3 .. v11}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxPlaylistArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    :pswitch_2
    move-object v3, p0

    move-object v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    .line 2018
    invoke-static/range {v3 .. v11}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxPlaylistSuggestedMixArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    .line 2024
    :pswitch_3
    if-eqz p11, :cond_0

    .line 2025
    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-interface {v0, v2}, Lcom/google/android/music/download/artwork/AlbumIdSink;->report(Ljava/lang/Long;)V

    goto :goto_0

    :pswitch_4
    move-object v3, p0

    move-object v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    .line 2029
    invoke-static/range {v3 .. v11}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxStackedLandscapeArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z

    move-result v2

    if-nez v2, :cond_1

    :pswitch_5
    move-object v3, p0

    move-object v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    .line 2034
    invoke-static/range {v3 .. v11}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxStackedLandscapeArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    .line 2042
    :cond_2
    const/4 v12, 0x0

    goto :goto_1

    .line 2007
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static drawFauxPlaylistArt(Landroid/graphics/Canvas;Landroid/content/Context;IIILjava/util/List;)Z
    .locals 28
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "style"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Landroid/content/Context;",
            "III",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2062
    .local p5, "art":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/16 v24, 0x4

    :try_start_0
    move/from16 v0, v24

    new-array v9, v0, [I

    .line 2063
    .local v9, "artGridIndices":[I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    array-length v0, v9

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v14, v0, :cond_0

    .line 2064
    aput v14, v9, v14

    .line 2063
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 2068
    :cond_0
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v8

    .line 2069
    .local v8, "artCount":I
    if-nez v8, :cond_2

    .line 2083
    :cond_1
    :goto_1
    const/4 v13, 0x2

    .line 2084
    .local v13, "horizontalArtCount":I
    const/16 v21, 0x2

    .line 2086
    .local v21, "verticalArtCount":I
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f0f00c7

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 2089
    .local v11, "borderPx":I
    div-int v24, p3, v13

    sub-int v19, v24, v11

    .line 2090
    .local v19, "squareWidth":I
    div-int v24, p4, v21

    sub-int v18, v24, v11

    .line 2091
    .local v18, "squareHeight":I
    const/16 v23, 0x0

    .line 2093
    .local v23, "y":I
    const/16 v24, 0xff

    const/16 v25, 0xff

    const/16 v26, 0xff

    const/16 v27, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 2095
    new-instance v17, Landroid/graphics/Paint;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Paint;-><init>()V

    .line 2096
    .local v17, "paint":Landroid/graphics/Paint;
    const/16 v24, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2098
    new-instance v20, Landroid/graphics/Rect;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Rect;-><init>()V

    .line 2099
    .local v20, "srcRect":Landroid/graphics/Rect;
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    .line 2101
    .local v12, "dstRect":Landroid/graphics/RectF;
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_2
    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    .line 2102
    const/16 v22, 0x0

    .line 2103
    .local v22, "x":I
    const/4 v14, 0x0

    :goto_3
    if-ge v14, v13, :cond_8

    .line 2105
    if-nez v8, :cond_5

    .line 2107
    invoke-static/range {p1 .. p2}, Lcom/google/android/music/utils/AlbumArtUtils;->getBackgroundBitmapForStyle(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 2111
    .local v5, "albumBitmap":Landroid/graphics/Bitmap;
    :goto_4
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 2112
    .local v7, "albumBitmapWidth":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 2113
    .local v6, "albumBitmapHeight":I
    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2, v7, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 2114
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v27, v0

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ge v14, v0, :cond_6

    add-int v24, v22, v19

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v25, v24

    :goto_5
    const/16 v24, 0x1

    move/from16 v0, v16

    move/from16 v1, v24

    if-ge v0, v1, :cond_7

    add-int v24, v23, v18

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    :goto_6
    move/from16 v0, v26

    move/from16 v1, v27

    move/from16 v2, v25

    move/from16 v3, v24

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2118
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-virtual {v0, v5, v1, v12, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 2119
    mul-int/lit8 v24, v11, 0x2

    add-int v24, v24, v19

    add-int v22, v22, v24

    .line 2103
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 2071
    .end local v5    # "albumBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "albumBitmapHeight":I
    .end local v7    # "albumBitmapWidth":I
    .end local v11    # "borderPx":I
    .end local v12    # "dstRect":Landroid/graphics/RectF;
    .end local v13    # "horizontalArtCount":I
    .end local v16    # "j":I
    .end local v17    # "paint":Landroid/graphics/Paint;
    .end local v18    # "squareHeight":I
    .end local v19    # "squareWidth":I
    .end local v20    # "srcRect":Landroid/graphics/Rect;
    .end local v21    # "verticalArtCount":I
    .end local v22    # "x":I
    .end local v23    # "y":I
    :cond_2
    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v8, v0, :cond_3

    .line 2073
    const/16 v24, 0x1

    const/16 v25, 0x2

    const/16 v26, 0x3

    const/16 v27, 0x0

    aput v27, v9, v26

    aput v27, v9, v25

    aput v27, v9, v24
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 2124
    .end local v8    # "artCount":I
    .end local v9    # "artGridIndices":[I
    .end local v14    # "i":I
    :catchall_0
    move-exception v24

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    .line 2125
    .local v10, "b":Landroid/graphics/Bitmap;
    sget-object v25, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_7

    .line 2074
    .end local v10    # "b":Landroid/graphics/Bitmap;
    .end local v15    # "i$":Ljava/util/Iterator;
    .restart local v8    # "artCount":I
    .restart local v9    # "artGridIndices":[I
    .restart local v14    # "i":I
    :cond_3
    const/16 v24, 0x2

    move/from16 v0, v24

    if-ne v8, v0, :cond_4

    .line 2076
    const/16 v24, 0x3

    const/16 v25, 0x0

    :try_start_1
    aput v25, v9, v24

    .line 2077
    const/16 v24, 0x1

    const/16 v25, 0x2

    const/16 v26, 0x1

    aput v26, v9, v25

    aput v26, v9, v24

    goto/16 :goto_1

    .line 2078
    :cond_4
    const/16 v24, 0x3

    move/from16 v0, v24

    if-ne v8, v0, :cond_1

    .line 2080
    const/16 v24, 0x3

    const/16 v25, 0x0

    aput v25, v9, v24

    goto/16 :goto_1

    .line 2109
    .restart local v11    # "borderPx":I
    .restart local v12    # "dstRect":Landroid/graphics/RectF;
    .restart local v13    # "horizontalArtCount":I
    .restart local v16    # "j":I
    .restart local v17    # "paint":Landroid/graphics/Paint;
    .restart local v18    # "squareHeight":I
    .restart local v19    # "squareWidth":I
    .restart local v20    # "srcRect":Landroid/graphics/Rect;
    .restart local v21    # "verticalArtCount":I
    .restart local v22    # "x":I
    .restart local v23    # "y":I
    :cond_5
    mul-int v24, v16, v13

    add-int v24, v24, v14

    aget v24, v9, v24

    move-object/from16 v0, p5

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v5    # "albumBitmap":Landroid/graphics/Bitmap;
    goto/16 :goto_4

    .line 2114
    .restart local v6    # "albumBitmapHeight":I
    .restart local v7    # "albumBitmapWidth":I
    :cond_6
    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v25, v24

    goto/16 :goto_5

    :cond_7
    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v24, v0

    goto/16 :goto_6

    .line 2121
    .end local v5    # "albumBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "albumBitmapHeight":I
    .end local v7    # "albumBitmapWidth":I
    :cond_8
    mul-int/lit8 v24, v11, 0x2

    add-int v24, v24, v18

    add-int v23, v23, v24

    .line 2101
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2

    .line 2124
    .end local v22    # "x":I
    :cond_9
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .restart local v15    # "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_b

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    .line 2125
    .restart local v10    # "b":Landroid/graphics/Bitmap;
    sget-object v24, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_8

    .line 2126
    .end local v8    # "artCount":I
    .end local v9    # "artGridIndices":[I
    .end local v10    # "b":Landroid/graphics/Bitmap;
    .end local v11    # "borderPx":I
    .end local v12    # "dstRect":Landroid/graphics/RectF;
    .end local v13    # "horizontalArtCount":I
    .end local v14    # "i":I
    .end local v16    # "j":I
    .end local v17    # "paint":Landroid/graphics/Paint;
    .end local v18    # "squareHeight":I
    .end local v19    # "squareWidth":I
    .end local v20    # "srcRect":Landroid/graphics/Rect;
    .end local v21    # "verticalArtCount":I
    .end local v23    # "y":I
    :cond_a
    throw v24

    .line 2128
    .restart local v8    # "artCount":I
    .restart local v9    # "artGridIndices":[I
    .restart local v11    # "borderPx":I
    .restart local v12    # "dstRect":Landroid/graphics/RectF;
    .restart local v13    # "horizontalArtCount":I
    .restart local v14    # "i":I
    .restart local v16    # "j":I
    .restart local v17    # "paint":Landroid/graphics/Paint;
    .restart local v18    # "squareHeight":I
    .restart local v19    # "squareWidth":I
    .restart local v20    # "srcRect":Landroid/graphics/Rect;
    .restart local v21    # "verticalArtCount":I
    .restart local v23    # "y":I
    :cond_b
    const/16 v24, 0x1

    return v24
.end method

.method private static drawFauxPlaylistArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z
    .locals 9
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "style"    # I
    .param p3, "playlistId"    # J
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p8, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;

    .prologue
    .line 2049
    const/4 v4, 0x4

    move-object v0, p1

    move v1, p2

    move-wide v2, p3

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/AlbumArtUtils;->getUniqueArt(Landroid/content/Context;IJIIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Ljava/util/List;

    move-result-object v5

    .local v5, "art":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p5

    move v4, p6

    .line 2051
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxPlaylistArt(Landroid/graphics/Canvas;Landroid/content/Context;IIILjava/util/List;)Z

    move-result v0

    return v0
.end method

.method private static drawFauxPlaylistSuggestedMixArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z
    .locals 1
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "style"    # I
    .param p3, "playlistId"    # J
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p8, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;

    .prologue
    .line 2417
    invoke-static/range {p0 .. p8}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxPlaylistArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z

    .line 2419
    invoke-static {p1, p0, p5, p6}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Canvas;II)Z

    .line 2421
    const/4 v0, 0x1

    return v0
.end method

.method public static drawFauxRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;II)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2277
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2278
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-static {p0, v0, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Canvas;II)Z

    move-result v1

    return v1
.end method

.method private static drawFauxRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Canvas;II)Z
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2284
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 2285
    .local v12, "paint":Landroid/graphics/Paint;
    const/4 v15, 0x1

    invoke-virtual {v12, v15}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2288
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_RadioArt:Landroid/graphics/Bitmap;

    if-nez v15, :cond_0

    .line 2289
    const v15, 0x7f020047

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v15

    sput-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_RadioArt:Landroid/graphics/Bitmap;

    .line 2292
    :cond_0
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_RadioArt:Landroid/graphics/Bitmap;

    const/16 v16, 0x0

    new-instance v17, Landroid/graphics/Rect;

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v15, v1, v2, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2296
    new-instance v14, Ljava/lang/Object;

    invoke-direct {v14}, Ljava/lang/Object;-><init>()V

    .line 2297
    .local v14, "ref":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v13

    .line 2298
    .local v13, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v11, 0x0

    .line 2300
    .local v11, "nautilusEnabled":Z
    :try_start_0
    invoke-virtual {v13}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    .line 2302
    invoke-static {v14}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 2305
    if-eqz v11, :cond_4

    .line 2306
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mRadioBadgeLarge:Landroid/graphics/Bitmap;

    if-nez v15, :cond_1

    .line 2307
    const v15, 0x7f02004e

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v15

    sput-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mRadioBadgeLarge:Landroid/graphics/Bitmap;

    .line 2309
    :cond_1
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mRadioBadgeSmall:Landroid/graphics/Bitmap;

    if-nez v15, :cond_2

    .line 2310
    const v15, 0x7f02013b

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v15

    sput-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mRadioBadgeSmall:Landroid/graphics/Bitmap;

    .line 2313
    :cond_2
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mRadioBadgeLarge:Landroid/graphics/Bitmap;

    move/from16 v0, p3

    invoke-static {v15, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->shouldUseLargeOverlay(Landroid/graphics/Bitmap;I)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2314
    sget-object v5, Lcom/google/android/music/utils/AlbumArtUtils;->mRadioBadgeLarge:Landroid/graphics/Bitmap;

    .line 2332
    .local v5, "badge":Landroid/graphics/Bitmap;
    :goto_0
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 2333
    .local v6, "badgeHeight":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 2334
    .local v10, "badgeWidth":I
    sub-int v15, p3, v6

    div-int/lit8 v9, v15, 0x2

    .line 2335
    .local v9, "badgeTop":I
    sub-int v15, p2, v10

    div-int/lit8 v7, v15, 0x2

    .line 2336
    .local v7, "badgeLeft":I
    new-instance v8, Landroid/graphics/Rect;

    add-int v15, v7, v10

    add-int v16, v9, v6

    move/from16 v0, v16

    invoke-direct {v8, v7, v9, v15, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2338
    .local v8, "badgeRect":Landroid/graphics/Rect;
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15, v8, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2340
    const/4 v15, 0x1

    return v15

    .line 2302
    .end local v5    # "badge":Landroid/graphics/Bitmap;
    .end local v6    # "badgeHeight":I
    .end local v7    # "badgeLeft":I
    .end local v8    # "badgeRect":Landroid/graphics/Rect;
    .end local v9    # "badgeTop":I
    .end local v10    # "badgeWidth":I
    :catchall_0
    move-exception v15

    invoke-static {v14}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v15

    .line 2316
    :cond_3
    sget-object v5, Lcom/google/android/music/utils/AlbumArtUtils;->mRadioBadgeSmall:Landroid/graphics/Bitmap;

    .restart local v5    # "badge":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 2319
    .end local v5    # "badge":Landroid/graphics/Bitmap;
    :cond_4
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mMixBadgeLarge:Landroid/graphics/Bitmap;

    if-nez v15, :cond_5

    .line 2320
    const v15, 0x7f02004d

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v15

    sput-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mMixBadgeLarge:Landroid/graphics/Bitmap;

    .line 2323
    :cond_5
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mMixBadgeSmall:Landroid/graphics/Bitmap;

    if-nez v15, :cond_6

    .line 2324
    const v15, 0x7f020107

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v15

    sput-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mMixBadgeSmall:Landroid/graphics/Bitmap;

    .line 2326
    :cond_6
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->mMixBadgeLarge:Landroid/graphics/Bitmap;

    move/from16 v0, p3

    invoke-static {v15, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->shouldUseLargeOverlay(Landroid/graphics/Bitmap;I)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 2327
    sget-object v5, Lcom/google/android/music/utils/AlbumArtUtils;->mMixBadgeLarge:Landroid/graphics/Bitmap;

    .restart local v5    # "badge":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 2329
    .end local v5    # "badge":Landroid/graphics/Bitmap;
    :cond_7
    sget-object v5, Lcom/google/android/music/utils/AlbumArtUtils;->mMixBadgeSmall:Landroid/graphics/Bitmap;

    .restart local v5    # "badge":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method private static drawFauxStackedLandscapeArt(Landroid/graphics/Canvas;Landroid/content/Context;IJIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Z
    .locals 31
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "style"    # I
    .param p3, "playlistId"    # J
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p8, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;

    .prologue
    .line 2135
    invoke-static/range {p5 .. p6}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 2136
    .local v9, "bitmapDimen":I
    const/4 v8, 0x5

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move v10, v9

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    invoke-static/range {v4 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getUniqueArt(Landroid/content/Context;IJIIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Ljava/util/List;

    move-result-object v16

    .line 2140
    .local v16, "art":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    :try_start_0
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    .line 2141
    .local v17, "artCount":I
    const/4 v4, 0x1

    move/from16 v0, v17

    if-ge v0, v4, :cond_0

    .line 2142
    const/4 v4, 0x0

    .line 2200
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/graphics/Bitmap;

    .line 2201
    .local v19, "b":Landroid/graphics/Bitmap;
    sget-object v5, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 2145
    .end local v19    # "b":Landroid/graphics/Bitmap;
    .end local v23    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    new-instance v25, Landroid/graphics/Paint;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Paint;-><init>()V

    .line 2146
    .local v25, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2147
    new-instance v29, Landroid/graphics/Rect;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/Rect;-><init>()V

    .line 2148
    .local v29, "srcRect":Landroid/graphics/Rect;
    new-instance v21, Landroid/graphics/RectF;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/RectF;-><init>()V

    .line 2150
    .local v21, "dstRect":Landroid/graphics/RectF;
    const/4 v4, 0x1

    move/from16 v0, v17

    if-ne v0, v4, :cond_2

    .line 2152
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/Bitmap;

    .line 2153
    .local v13, "albumBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    .line 2154
    .local v15, "albumBitmapWidth":I
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    .line 2156
    .local v14, "albumBitmapHeight":I
    const/4 v4, 0x0

    const/4 v5, 0x0

    div-int/lit8 v6, v14, 0x2

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5, v15, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 2157
    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, p5

    int-to-float v6, v0

    move/from16 v0, p6

    int-to-float v7, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2158
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v21

    move-object/from16 v3, v25

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2200
    .end local v13    # "albumBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "albumBitmapHeight":I
    .end local v15    # "albumBitmapWidth":I
    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .restart local v23    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/graphics/Bitmap;

    .line 2201
    .restart local v19    # "b":Landroid/graphics/Bitmap;
    sget-object v4, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 2162
    .end local v19    # "b":Landroid/graphics/Bitmap;
    .end local v23    # "i$":Ljava/util/Iterator;
    :cond_2
    const v4, 0x7f0200bf

    :try_start_2
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 2163
    .local v30, "stackShadow":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00d3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v27

    .line 2167
    .local v27, "shadowWidth":I
    move/from16 v18, p6

    .line 2170
    .local v18, "artWidth":I
    sub-int v4, p5, v18

    add-int/lit8 v5, v17, -0x1

    div-int v20, v4, v5

    .line 2172
    .local v20, "dstOffsetWidth":I
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_2
    move/from16 v0, v22

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    .line 2173
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/Bitmap;

    .line 2174
    .restart local v13    # "albumBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    .line 2175
    .restart local v15    # "albumBitmapWidth":I
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    .line 2176
    .restart local v14    # "albumBitmapHeight":I
    if-nez v22, :cond_3

    .line 2178
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5, v15, v14}, Landroid/graphics/Rect;->set(IIII)V

    .line 2179
    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v18

    int-to-float v6, v0

    move/from16 v0, p6

    int-to-float v7, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2180
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v21

    move-object/from16 v3, v25

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 2172
    :goto_3
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 2183
    :cond_3
    add-int/lit8 v4, v17, -0x1

    div-int v28, v15, v4

    .line 2184
    .local v28, "srcOffsetWidth":I
    sub-int v4, v15, v28

    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5, v15, v14}, Landroid/graphics/Rect;->set(IIII)V

    .line 2187
    mul-int v4, v20, v22

    add-int v26, v18, v4

    .line 2188
    .local v26, "right":I
    sub-int v24, v26, v20

    .line 2189
    .local v24, "left":I
    move/from16 v0, v24

    int-to-float v4, v0

    const/4 v5, 0x0

    move/from16 v0, v26

    int-to-float v6, v0

    move/from16 v0, p6

    int-to-float v7, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2190
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v21

    move-object/from16 v3, v25

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 2193
    move/from16 v0, v24

    int-to-float v4, v0

    const/4 v5, 0x0

    add-int v6, v24, v27

    int-to-float v6, v6

    move/from16 v0, p6

    int-to-float v7, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2194
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v21

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 2200
    .end local v13    # "albumBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "albumBitmapHeight":I
    .end local v15    # "albumBitmapWidth":I
    .end local v17    # "artCount":I
    .end local v18    # "artWidth":I
    .end local v20    # "dstOffsetWidth":I
    .end local v21    # "dstRect":Landroid/graphics/RectF;
    .end local v22    # "i":I
    .end local v24    # "left":I
    .end local v25    # "paint":Landroid/graphics/Paint;
    .end local v26    # "right":I
    .end local v27    # "shadowWidth":I
    .end local v28    # "srcOffsetWidth":I
    .end local v29    # "srcRect":Landroid/graphics/Rect;
    .end local v30    # "stackShadow":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v4

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .restart local v23    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/graphics/Bitmap;

    .line 2201
    .restart local v19    # "b":Landroid/graphics/Bitmap;
    sget-object v5, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_4

    .line 2202
    .end local v19    # "b":Landroid/graphics/Bitmap;
    :cond_4
    throw v4

    .line 2204
    .restart local v17    # "artCount":I
    .restart local v21    # "dstRect":Landroid/graphics/RectF;
    .restart local v25    # "paint":Landroid/graphics/Paint;
    .restart local v29    # "srcRect":Landroid/graphics/Rect;
    :cond_5
    const/4 v4, 0x1

    .end local v21    # "dstRect":Landroid/graphics/RectF;
    .end local v25    # "paint":Landroid/graphics/Paint;
    .end local v29    # "srcRect":Landroid/graphics/Rect;
    :cond_6
    return v4
.end method

.method public static drawImFeelingLuckyRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;II)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 2356
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2357
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-static {p0, v0, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->drawImFeelingLuckyRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Canvas;II)V

    .line 2358
    return-void
.end method

.method private static drawImFeelingLuckyRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Canvas;II)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 2362
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 2363
    .local v6, "paint":Landroid/graphics/Paint;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2365
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->canStartImFeelingLucky(Landroid/content/Context;)Z

    move-result v5

    .line 2366
    .local v5, "newCanStartIFL":Z
    sget-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->mLuckyBadge:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_0

    sget-boolean v7, Lcom/google/android/music/utils/AlbumArtUtils;->sCanStartIFL:Z

    if-eq v7, v5, :cond_1

    .line 2367
    :cond_0
    sput-boolean v5, Lcom/google/android/music/utils/AlbumArtUtils;->sCanStartIFL:Z

    .line 2369
    if-eqz v5, :cond_2

    .line 2370
    const v7, 0x7f02004c

    invoke-static {p0, v7}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v7

    sput-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->mLuckyBadge:Landroid/graphics/Bitmap;

    .line 2375
    :cond_1
    :goto_0
    sget-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->mLuckyBadge:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 2376
    .local v4, "badgeWidth":I
    sget-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->mLuckyBadge:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 2377
    .local v0, "badgeHeight":I
    sub-int v7, p3, v0

    div-int/lit8 v3, v7, 0x2

    .line 2378
    .local v3, "badgeTop":I
    sub-int v7, p2, v4

    div-int/lit8 v1, v7, 0x2

    .line 2379
    .local v1, "badgeLeft":I
    new-instance v2, Landroid/graphics/Rect;

    add-int v7, v1, v4

    add-int v8, v3, v0

    invoke-direct {v2, v1, v3, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2381
    .local v2, "badgeRect":Landroid/graphics/Rect;
    sget-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->mLuckyBadge:Landroid/graphics/Bitmap;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2382
    return-void

    .line 2372
    .end local v0    # "badgeHeight":I
    .end local v1    # "badgeLeft":I
    .end local v2    # "badgeRect":Landroid/graphics/Rect;
    .end local v3    # "badgeTop":I
    .end local v4    # "badgeWidth":I
    :cond_2
    const v7, 0x7f020072

    invoke-static {p0, v7}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v7

    sput-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->mLuckyBadge:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static drawModernFauxAlbumArt(Landroid/graphics/Canvas;Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Z)V
    .locals 26
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "style"    # I
    .param p3, "drawLabels"    # Z
    .param p4, "seed"    # J
    .param p6, "w"    # I
    .param p7, "h"    # I
    .param p8, "mainLabel"    # Ljava/lang/String;
    .param p9, "subLabel"    # Ljava/lang/String;
    .param p10, "keepAspect"    # Z

    .prologue
    .line 2545
    new-instance v23, Ljava/util/Random;

    move-object/from16 v0, v23

    move-wide/from16 v1, p4

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    .line 2546
    .local v23, "random":Ljava/util/Random;
    and-int/lit8 v3, p2, 0x10

    const/16 v8, 0x10

    if-ne v3, v8, :cond_4

    const/16 v20, 0x1

    .line 2547
    .local v20, "isStatic":Z
    :goto_0
    and-int/lit8 p2, p2, 0xf

    .line 2557
    if-eqz p3, :cond_0

    if-nez v20, :cond_0

    .line 2558
    const/4 v3, 0x1

    move/from16 v0, p2

    if-ne v0, v3, :cond_5

    .line 2559
    const v3, 0x7f0b0104

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p9

    .line 2571
    :cond_0
    :goto_1
    if-eqz p10, :cond_1

    .line 2572
    invoke-static/range {p6 .. p7}, Ljava/lang/Math;->max(II)I

    move-result p7

    move/from16 p6, p7

    .line 2576
    :cond_1
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 2577
    .local v22, "paint":Landroid/graphics/Paint;
    invoke-static/range {p1 .. p2}, Lcom/google/android/music/utils/AlbumArtUtils;->getBackgroundBitmapForStyle(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 2578
    .local v19, "background":Landroid/graphics/Bitmap;
    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2579
    const/4 v3, 0x0

    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-direct {v8, v9, v10, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v3, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2582
    if-eqz p3, :cond_3

    .line 2583
    const/16 v25, 0x150

    .line 2584
    .local v25, "specWidth":I
    const/16 v24, 0x14

    .line 2585
    .local v24, "specMargin":I
    mul-int/lit8 v3, p6, 0x14

    div-int/lit16 v0, v3, 0x150

    move/from16 v21, v0

    .line 2586
    .local v21, "margin":I
    move/from16 v0, p6

    mul-int/lit16 v3, v0, 0x128

    div-int/lit16 v4, v3, 0x150

    .line 2587
    .local v4, "linew":I
    move/from16 v5, v21

    .line 2588
    .local v5, "x0":I
    if-eqz p8, :cond_2

    .line 2589
    move/from16 v6, v21

    .line 2590
    .local v6, "y0":I
    mul-int/lit8 v3, p7, 0x1c

    div-int/lit16 v7, v3, 0x150

    .line 2591
    .local v7, "fontHeight":I
    mul-int/lit8 v3, p6, 0x1e

    div-int/lit16 v11, v3, 0x150

    .line 2593
    .local v11, "fadeWidth":I
    const/4 v8, 0x0

    invoke-virtual/range {p8 .. p8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    const v10, -0xcccccd

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v18}, Lcom/google/android/music/utils/AlbumArtUtils;->renderFauxLabel(Landroid/graphics/Canvas;IIIIZLjava/lang/String;IIZIIZIIF)V

    .line 2597
    .end local v6    # "y0":I
    .end local v7    # "fontHeight":I
    .end local v11    # "fadeWidth":I
    :cond_2
    if-eqz p9, :cond_3

    .line 2598
    mul-int/lit8 v3, p7, 0x37

    div-int/lit16 v6, v3, 0x150

    .line 2599
    .restart local v6    # "y0":I
    mul-int/lit8 v3, p7, 0x16

    div-int/lit16 v7, v3, 0x150

    .line 2600
    .restart local v7    # "fontHeight":I
    mul-int/lit8 v3, p6, 0x14

    div-int/lit16 v11, v3, 0x150

    .line 2602
    .restart local v11    # "fadeWidth":I
    const/4 v8, 0x0

    const v10, -0x888889

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v3, p0

    move-object/from16 v9, p9

    invoke-static/range {v3 .. v18}, Lcom/google/android/music/utils/AlbumArtUtils;->renderFauxLabel(Landroid/graphics/Canvas;IIIIZLjava/lang/String;IIZIIZIIF)V

    .line 2607
    .end local v4    # "linew":I
    .end local v5    # "x0":I
    .end local v6    # "y0":I
    .end local v7    # "fontHeight":I
    .end local v11    # "fadeWidth":I
    .end local v21    # "margin":I
    .end local v24    # "specMargin":I
    .end local v25    # "specWidth":I
    :cond_3
    return-void

    .line 2546
    .end local v19    # "background":Landroid/graphics/Bitmap;
    .end local v20    # "isStatic":Z
    .end local v22    # "paint":Landroid/graphics/Paint;
    :cond_4
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 2560
    .restart local v20    # "isStatic":Z
    :cond_5
    const/4 v3, 0x4

    move/from16 v0, p2

    if-ne v0, v3, :cond_6

    .line 2561
    const v3, 0x7f0b0107

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p9

    goto/16 :goto_1

    .line 2562
    :cond_6
    const/4 v3, 0x2

    move/from16 v0, p2

    if-ne v0, v3, :cond_7

    .line 2563
    const v3, 0x7f0b0105

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p9

    goto/16 :goto_1

    .line 2564
    :cond_7
    const/4 v3, 0x3

    move/from16 v0, p2

    if-ne v0, v3, :cond_8

    .line 2565
    const v3, 0x7f0b0106

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p8

    goto/16 :goto_1

    .line 2566
    :cond_8
    const/4 v3, 0x5

    move/from16 v0, p2

    if-ne v0, v3, :cond_0

    .line 2567
    const v3, 0x7f0b0103

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p9

    goto/16 :goto_1
.end method

.method public static drawPlayYTVideoOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;II)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 2385
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2386
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-static {p0, v0, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->drawPlayYTVideoOverlay(Landroid/content/Context;Landroid/graphics/Canvas;II)V

    .line 2387
    return-void
.end method

.method private static drawPlayYTVideoOverlay(Landroid/content/Context;Landroid/graphics/Canvas;II)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    const/4 v8, 0x0

    .line 2391
    sget-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mYouTubeOverlayDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_0

    .line 2392
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020049

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    sput-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mYouTubeOverlayDrawable:Landroid/graphics/drawable/Drawable;

    .line 2394
    :cond_0
    sget-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mYouTubeOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v8, v8, p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2395
    sget-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mYouTubeOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2398
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 2399
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2400
    sget-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mVideoBadge:Landroid/graphics/Bitmap;

    if-nez v6, :cond_1

    .line 2401
    const v6, 0x7f0200f7

    invoke-static {p0, v6}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v6

    sput-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mVideoBadge:Landroid/graphics/Bitmap;

    .line 2404
    :cond_1
    sget-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mVideoBadge:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 2405
    .local v4, "badgeWidth":I
    sget-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mVideoBadge:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 2406
    .local v0, "badgeHeight":I
    sub-int v6, p3, v0

    div-int/lit8 v3, v6, 0x2

    .line 2407
    .local v3, "badgeTop":I
    sub-int v6, p2, v4

    div-int/lit8 v1, v6, 0x2

    .line 2408
    .local v1, "badgeLeft":I
    new-instance v2, Landroid/graphics/Rect;

    add-int v6, v1, v4

    add-int v7, v3, v0

    invoke-direct {v2, v1, v3, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2410
    .local v2, "badgeRect":Landroid/graphics/Rect;
    sget-object v6, Lcom/google/android/music/utils/AlbumArtUtils;->mVideoBadge:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2411
    return-void
.end method

.method private static estimatedArtFileSize(I)J
    .locals 4
    .param p0, "size"    # I

    .prologue
    .line 2934
    const-wide/32 v0, 0x19000

    int-to-long v2, p0

    mul-long/2addr v0, v2

    int-to-long v2, p0

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x57e40

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private static findClosest([II)I
    .locals 5
    .param p0, "bucketSizes"    # [I
    .param p1, "goal"    # I

    .prologue
    .line 2938
    const/4 v3, 0x0

    .line 2939
    .local v3, "result":I
    const/4 v4, 0x0

    aget v4, p0, v4

    sub-int/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 2940
    .local v0, "error":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v4, p0

    if-ge v1, v4, :cond_1

    .line 2941
    aget v4, p0, v1

    sub-int/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 2942
    .local v2, "newError":I
    if-ge v2, v0, :cond_0

    .line 2943
    move v0, v2

    .line 2944
    move v3, v1

    .line 2940
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2947
    .end local v2    # "newError":I
    :cond_1
    return v3
.end method

.method public static getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "allowdefault"    # Z
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p9, "allowAlias"    # Z

    .prologue
    .line 1515
    const/4 v10, 0x1

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v11, p9

    invoke-static/range {v1 .. v11}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZ)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "allowdefault"    # Z
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p9, "mayScaleUpOrCrop"    # Z
    .param p10, "allowAlias"    # Z

    .prologue
    .line 1566
    const-string v0, "Getting album art on main thread"

    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 1573
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    .line 1574
    if-eqz p5, :cond_1

    .line 1575
    const/4 v1, 0x1

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p10

    invoke-static/range {v0 .. v9}, Lcom/google/android/music/utils/AlbumArtUtils;->getDefaultArtwork(Landroid/content/Context;ZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1590
    :cond_0
    :goto_0
    return-object v10

    .line 1578
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    :cond_2
    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move/from16 v6, p9

    .line 1581
    invoke-static/range {v1 .. v6}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtwork(Landroid/content/Context;JIIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1582
    .local v10, "bm":Landroid/graphics/Bitmap;
    if-nez v10, :cond_0

    .line 1586
    if-eqz p5, :cond_3

    .line 1587
    const/4 v1, 0x1

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p10

    invoke-static/range {v0 .. v9}, Lcom/google/android/music/utils/AlbumArtUtils;->getDefaultArtwork(Landroid/content/Context;ZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    goto :goto_0

    .line 1590
    :cond_3
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public static getArtworkFromUrl(Landroid/content/Context;Ljava/lang/String;IIZZZZZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumArtUrl"    # Ljava/lang/String;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "mayScaleUp"    # Z
    .param p5, "isSkyjam"    # Z
    .param p6, "allowResolve"    # Z
    .param p7, "allowDefault"    # Z
    .param p8, "allowAlias"    # Z

    .prologue
    .line 803
    if-eqz p5, :cond_1

    .line 804
    const-string v0, "="

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0215

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p6

    move v5, p7

    move v6, p8

    .line 811
    invoke-static/range {v0 .. v6}, Lcom/google/android/music/utils/AlbumArtUtils;->getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 809
    :cond_1
    invoke-static {p1, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->appendSizeToExternalUrl(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static getBackgroundBitmapForStyle(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I

    .prologue
    .line 2611
    packed-switch p1, :pswitch_data_0

    .line 2632
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown style"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2613
    :pswitch_1
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_DefaultArtistArt:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 2614
    const v0, 0x7f02017a

    invoke-static {p0, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_DefaultArtistArt:Landroid/graphics/Bitmap;

    .line 2617
    :cond_0
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_DefaultArtistArt:Landroid/graphics/Bitmap;

    .line 2630
    :goto_0
    return-object v0

    .line 2626
    :pswitch_2
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_DefaultArt:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 2627
    const v0, 0x7f02003a

    invoke-static {p0, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_DefaultArt:Landroid/graphics/Bitmap;

    .line 2630
    :cond_1
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->mBackground_DefaultArt:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 2611
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceId"    # I

    .prologue
    .line 2766
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2767
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 2768
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private static getBitmapFromCache(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "key"    # Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 1128
    .local p0, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    const/4 v1, 0x0

    .line 1129
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p0, :cond_1

    .line 1130
    monitor-enter p0

    .line 1131
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/utils/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    .line 1132
    if-eqz v1, :cond_0

    .line 1133
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1134
    const-string v2, "AlbumArtUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found a recycled bitmap for artwork: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1135
    invoke-virtual {p0, p1}, Lcom/google/android/music/utils/LruCache;->remove(Ljava/lang/Object;)V

    .line 1136
    const/4 v1, 0x0

    .line 1139
    :cond_0
    monitor-exit p0

    .line 1142
    :cond_1
    return-object v1

    .line 1139
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getBitmapFromCacheOrAcquireLock(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "key"    # Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 1024
    .local p0, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    if-nez p0, :cond_0

    .line 1025
    const/4 v0, 0x0

    .line 1040
    :goto_0
    return-object v0

    .line 1027
    :cond_0
    sget-object v2, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    monitor-enter v2

    .line 1028
    :goto_1
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 1030
    :try_start_1
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1031
    :catch_0
    move-exception v1

    goto :goto_1

    .line 1036
    :cond_1
    :try_start_2
    invoke-static {p0, p1}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCache(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1037
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 1038
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1040
    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 1041
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public static getBitmapFromDisk(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1658
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtworkRaw(Landroid/content/Context;Ljava/lang/String;II[IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "sizeKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1107
    const/4 v1, 0x0

    .line 1108
    .local v1, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    const-string v3, "LARGE"

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1109
    sget-object v4, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCache:Ljava/util/HashMap;

    monitor-enter v4

    .line 1110
    :try_start_0
    sget-object v3, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCache:Ljava/util/HashMap;

    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/android/music/utils/LruCache;

    move-object v1, v0

    .line 1111
    if-nez v1, :cond_1

    .line 1112
    const-string v3, "SMALL"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1113
    new-instance v2, Lcom/google/android/music/utils/LruCache;

    sget v3, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheBucketSmallSize:I

    invoke-direct {v2, v3}, Lcom/google/android/music/utils/LruCache;-><init>(I)V

    .end local v1    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    .local v2, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    move-object v1, v2

    .line 1117
    .end local v2    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    .restart local v1    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1118
    sget-object v3, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCache:Ljava/util/HashMap;

    invoke-virtual {v3, p3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121
    :cond_1
    monitor-exit v4

    .line 1123
    :cond_2
    return-object v1

    .line 1114
    :cond_3
    const-string v3, "MEDIUM"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1115
    new-instance v2, Lcom/google/android/music/utils/LruCache;

    sget v3, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheBucketMediumSize:I

    invoke-direct {v2, v3}, Lcom/google/android/music/utils/LruCache;-><init>(I)V

    .end local v1    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    .restart local v2    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    move-object v1, v2

    .end local v2    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    .restart local v1    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    goto :goto_0

    .line 1121
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static getCachedBitmap(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZZ)Landroid/graphics/Bitmap;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "w"    # I
    .param p5, "h"    # I
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p9, "allowResolve"    # Z
    .param p10, "allowDefault"    # Z
    .param p11, "allowAlias"    # Z

    .prologue
    .line 876
    const-wide/16 v6, 0x0

    cmp-long v5, p1, v6

    if-gez v5, :cond_1

    if-nez p3, :cond_1

    .line 877
    const/4 v4, 0x0

    .line 902
    :cond_0
    :goto_0
    return-object v4

    .line 880
    :cond_1
    move-wide/from16 v0, p1

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->cacheable(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move/from16 v14, p10

    move/from16 v15, p11

    .line 881
    invoke-static/range {v5 .. v15}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtworkImp(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZ)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_0

    .line 885
    :cond_2
    const/4 v4, 0x0

    .line 886
    .local v4, "result":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->getSizeKey(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v17

    .line 887
    .local v17, "sizeKey":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, p4

    move/from16 v2, p5

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;

    move-result-object v18

    .line 889
    .local v18, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    if-eqz v18, :cond_3

    .line 890
    const/16 v16, 0x1

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move/from16 v14, p9

    move/from16 v15, p10

    invoke-static/range {v5 .. v18}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedBitmapImpl(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZZLjava/lang/String;Lcom/google/android/music/utils/LruCache;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 892
    if-nez p11, :cond_0

    .line 893
    const/4 v5, 0x0

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_0

    .line 897
    :cond_3
    if-eqz p9, :cond_0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move/from16 v14, p10

    move/from16 v15, p11

    .line 898
    invoke-static/range {v5 .. v15}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtworkImp(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZ)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_0
.end method

.method private static getCachedBitmapImpl(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZZLjava/lang/String;Lcom/google/android/music/utils/LruCache;)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "w"    # I
    .param p5, "h"    # I
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p9, "allowResolve"    # Z
    .param p10, "allowDefault"    # Z
    .param p11, "allowAlias"    # Z
    .param p12, "sizeKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/music/download/artwork/AlbumIdSink;",
            "ZZZ",
            "Ljava/lang/String;",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 953
    .local p13, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    new-instance v15, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    const/4 v3, 0x1

    move-wide/from16 v0, p1

    invoke-direct {v15, v3, v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;-><init>(IJ)V

    .line 957
    .local v15, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    if-eqz p9, :cond_2

    .line 958
    move-object/from16 v0, p13

    invoke-static {v0, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCacheOrAcquireLock(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 959
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    const/4 v14, 0x1

    .line 960
    .local v14, "cacheHit":Z
    :goto_0
    const/16 v16, 0x0

    .line 961
    .local v16, "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-nez v2, :cond_0

    .line 963
    :try_start_0
    new-instance v11, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;

    move-object/from16 v0, p8

    invoke-direct {v11, v0}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;-><init>(Lcom/google/android/music/download/artwork/AlbumIdSink;)V

    .local v11, "tap":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;
    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move/from16 v12, p10

    move/from16 v13, p11

    .line 964
    invoke-static/range {v3 .. v13}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtworkImp(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 966
    invoke-virtual {v11}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->extractIds()Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v16

    .line 968
    move-object/from16 v0, p13

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->putBitmapInCacheAndReleaseLock(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Ljava/util/Set;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V

    .line 976
    .end local v11    # "tap":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;
    .end local v16    # "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_0
    :goto_1
    move-object/from16 v0, p12

    invoke-static {v0, v14}, Lcom/google/android/music/utils/AlbumArtUtils;->trackCacheUsage(Ljava/lang/String;Z)V

    .line 977
    return-object v2

    .line 959
    .end local v14    # "cacheHit":Z
    :cond_1
    const/4 v14, 0x0

    goto :goto_0

    .line 968
    .restart local v14    # "cacheHit":Z
    .restart local v16    # "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v3

    move-object/from16 v0, p13

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->putBitmapInCacheAndReleaseLock(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Ljava/util/Set;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V

    throw v3

    .line 972
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "cacheHit":Z
    .end local v16    # "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_2
    move-object/from16 v0, p13

    invoke-static {v0, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCache(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 973
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_3

    const/4 v14, 0x1

    .restart local v14    # "cacheHit":Z
    :goto_2
    goto :goto_1

    .end local v14    # "cacheHit":Z
    :cond_3
    const/4 v14, 0x0

    goto :goto_2
.end method

.method public static getCachedFauxAlbumArt(Landroid/content/Context;IJIILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "artwork_id"    # J
    .param p4, "w"    # I
    .param p5, "h"    # I
    .param p6, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p7, "allowAlias"    # Z

    .prologue
    .line 1783
    and-int/lit8 p1, p1, 0xf

    .line 1785
    const/4 v2, 0x0

    .line 1787
    .local v2, "result":Landroid/graphics/Bitmap;
    invoke-static {p1, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->createArtCacheKey(IJ)Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    move-result-object v1

    .line 1789
    .local v1, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    invoke-static {p0, p4, p5}, Lcom/google/android/music/utils/AlbumArtUtils;->getSizeKey(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v3

    .line 1790
    .local v3, "sizeKey":Ljava/lang/String;
    invoke-static {p0, p4, p5, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;

    move-result-object v0

    .line 1792
    .local v0, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    if-eqz v0, :cond_0

    .line 1793
    invoke-static {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCache(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1794
    if-nez p7, :cond_0

    .line 1795
    const/4 v4, 0x0

    invoke-static {p4, p5, v2, v4}, Lcom/google/android/music/utils/AlbumArtUtils;->copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1799
    :cond_0
    return-object v2
.end method

.method public static getCachedMultiImageComposite(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artUrls"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "isRadio"    # Z

    .prologue
    .line 2212
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x0

    .line 2219
    :goto_0
    return-object v2

    .line 2214
    :cond_0
    invoke-static {p0, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->getSizeKey(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v3

    .line 2215
    .local v3, "sizeKey":Ljava/lang/String;
    invoke-static {p0, p2, p3, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;

    move-result-object v0

    .line 2216
    .local v0, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    if-eqz p4, :cond_1

    const/16 v4, 0x13

    .line 2217
    .local v4, "type":I
    :goto_1
    new-instance v1, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    invoke-static {p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct {v1, v4, v6, v7}, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;-><init>(IJ)V

    .line 2218
    .local v1, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    invoke-static {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCache(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {p2, p3, v5, v6}, Lcom/google/android/music/utils/AlbumArtUtils;->copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2219
    .local v2, "out":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 2216
    .end local v1    # "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .end local v2    # "out":Landroid/graphics/Bitmap;
    .end local v4    # "type":I
    :cond_1
    const/16 v4, 0x9

    goto :goto_1
.end method

.method private static getCapHeight(Landroid/text/TextPaint;)I
    .locals 5
    .param p0, "paint"    # Landroid/text/TextPaint;

    .prologue
    .line 2759
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2760
    .local v0, "capBounds":Landroid/graphics/Rect;
    const-string v2, "I"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4, v0}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2761
    iget v2, v0, Landroid/graphics/Rect;->top:I

    neg-int v1, v2

    .line 2762
    .local v1, "capHeight":I
    return v1
.end method

.method private static getConfigOrDefault(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "defaultConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 1246
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 1247
    .local v0, "config":Landroid/graphics/Bitmap$Config;
    if-nez v0, :cond_0

    .line 1249
    move-object v0, p1

    .line 1251
    :cond_0
    return-object v0
.end method

.method public static getDefaultArtwork(Landroid/content/Context;ZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "drawLabels"    # Z
    .param p2, "albumId"    # J
    .param p4, "w"    # I
    .param p5, "h"    # I
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p9, "allowAlias"    # Z

    .prologue
    .line 1596
    const/4 v2, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move v3, p1

    move-wide/from16 v4, p2

    move/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumArtUrl"    # Ljava/lang/String;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "allowResolve"    # Z
    .param p5, "allowDefault"    # Z
    .param p6, "allowAlias"    # Z

    .prologue
    .line 817
    sget-boolean v2, Lcom/google/android/music/utils/AlbumArtUtils;->LOGV:Z

    if-eqz v2, :cond_0

    .line 818
    const-string v2, "AlbumArtUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getExternalAlbumArtBitmap: albumArtUrl="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " w="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " h="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    :cond_0
    if-nez p1, :cond_1

    .line 821
    const/4 v2, 0x0

    .line 829
    :goto_0
    return-object v2

    .line 824
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/AlbumArtUtils;->makeDefaultArtId(Ljava/lang/String;)J

    move-result-wide v4

    .line 826
    .local v4, "album_id":J
    sget-boolean v2, Lcom/google/android/music/utils/AlbumArtUtils;->LOGV:Z

    if-eqz v2, :cond_2

    .line 827
    const-string v2, "AlbumArtUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getExternalAlbumArtBitmap: album_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    :cond_2
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v12, p4

    move/from16 v13, p5

    move/from16 v14, p6

    invoke-static/range {v3 .. v14}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedBitmap(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZZ)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public static getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "drawLabels"    # Z
    .param p3, "artwork_id"    # J
    .param p5, "w"    # I
    .param p6, "h"    # I
    .param p7, "mainLabel"    # Ljava/lang/String;
    .param p8, "subLabel"    # Ljava/lang/String;
    .param p9, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p10, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p11, "allowAlias"    # Z

    .prologue
    .line 1730
    const/4 v4, 0x0

    .line 1732
    .local v4, "result":Landroid/graphics/Bitmap;
    and-int/lit8 p1, p1, 0xf

    .line 1733
    if-nez p1, :cond_0

    move-wide/from16 v0, p3

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->cacheable(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1734
    :cond_0
    move/from16 v0, p1

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->createArtCacheKey(IJ)Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    move-result-object v16

    .line 1736
    .local v16, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    move-object/from16 v0, p0

    move/from16 v1, p5

    move/from16 v2, p6

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->getSizeKey(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v17

    .line 1737
    .local v17, "sizeKey":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, p5

    move/from16 v2, p6

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;

    move-result-object v18

    .line 1739
    .local v18, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    if-eqz v18, :cond_1

    move-object/from16 v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move-wide/from16 v8, p3

    move/from16 v10, p5

    move/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    .line 1740
    invoke-static/range {v5 .. v18}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArtImpl(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Ljava/lang/String;Lcom/google/android/music/utils/LruCache;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1743
    if-nez p11, :cond_1

    .line 1744
    const/4 v5, 0x0

    move/from16 v0, p5

    move/from16 v1, p6

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1749
    .end local v16    # "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .end local v17    # "sizeKey":Ljava/lang/String;
    .end local v18    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    :cond_1
    if-nez v4, :cond_2

    .line 1750
    invoke-static/range {p0 .. p10}, Lcom/google/android/music/utils/AlbumArtUtils;->createFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1753
    :cond_2
    return-object v4
.end method

.method private static getFauxAlbumArtImpl(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Ljava/lang/String;Lcom/google/android/music/utils/LruCache;)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "drawLabels"    # Z
    .param p3, "artwork_id"    # J
    .param p5, "w"    # I
    .param p6, "h"    # I
    .param p7, "mainLabel"    # Ljava/lang/String;
    .param p8, "subLabel"    # Ljava/lang/String;
    .param p9, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p10, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p11, "key"    # Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .param p12, "sizeKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZJII",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;",
            "Lcom/google/android/music/download/artwork/AlbumIdSink;",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Ljava/lang/String;",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 1761
    .local p13, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    move-object/from16 v0, p13

    move-object/from16 v1, p11

    invoke-static {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCacheOrAcquireLock(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1762
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_2

    const/4 v14, 0x1

    .line 1763
    .local v14, "cacheHit":Z
    :goto_0
    const/4 v15, 0x0

    .line 1764
    .local v15, "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-nez v2, :cond_0

    .line 1766
    :try_start_0
    new-instance v13, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;

    move-object/from16 v0, p10

    invoke-direct {v13, v0}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;-><init>(Lcom/google/android/music/download/artwork/AlbumIdSink;)V

    .local v13, "tap":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;
    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    .line 1767
    invoke-static/range {v3 .. v13}, Lcom/google/android/music/utils/AlbumArtUtils;->createFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1769
    invoke-virtual {v13}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;->extractIds()Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v15

    .line 1771
    move-object/from16 v0, p13

    move-object/from16 v1, p11

    invoke-static {v0, v2, v15, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->putBitmapInCacheAndReleaseLock(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Ljava/util/Set;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V

    .line 1775
    .end local v13    # "tap":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdTap;
    :cond_0
    if-eqz p13, :cond_1

    .line 1776
    move-object/from16 v0, p12

    invoke-static {v0, v14}, Lcom/google/android/music/utils/AlbumArtUtils;->trackCacheUsage(Ljava/lang/String;Z)V

    .line 1778
    :cond_1
    return-object v2

    .line 1762
    .end local v14    # "cacheHit":Z
    .end local v15    # "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_2
    const/4 v14, 0x0

    goto :goto_0

    .line 1771
    .restart local v14    # "cacheHit":Z
    .restart local v15    # "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v3

    move-object/from16 v0, p13

    move-object/from16 v1, p11

    invoke-static {v0, v2, v15, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->putBitmapInCacheAndReleaseLock(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Ljava/util/Set;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V

    throw v3
.end method

.method public static getLockScreenArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "allowdefault"    # Z
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "allowAlias"    # Z

    .prologue
    .line 1551
    const/16 v2, 0x400

    move/from16 v0, p3

    if-gt v0, v2, :cond_0

    const/16 v2, 0x400

    move/from16 v0, p4

    if-le v0, v2, :cond_1

    .line 1553
    :cond_0
    const/16 p3, 0x400

    .line 1554
    const/16 p4, 0x400

    .line 1556
    :cond_1
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v3, p0

    move-wide/from16 v4, p1

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move/from16 v13, p8

    invoke-static/range {v3 .. v13}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZ)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method public static getMaxAlbumArtSize(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2857
    sget-boolean v0, Lcom/google/android/music/utils/AlbumArtUtils;->sMaxAlbumArtSizeInitalized:Z

    if-nez v0, :cond_0

    .line 2858
    invoke-static {p0}, Lcom/google/android/music/utils/AlbumArtUtils;->initializeMaxAlbumArtSize(Landroid/content/Context;)V

    .line 2860
    :cond_0
    sget v0, Lcom/google/android/music/utils/AlbumArtUtils;->sMaxAlbumArtSize:I

    return v0
.end method

.method public static getMultiImageComposite(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artUrls"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "isRadio"    # Z

    .prologue
    .line 2229
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->getSizeKey(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v17

    .line 2230
    .local v17, "sizeKey":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;

    move-result-object v12

    .line 2231
    .local v12, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    if-eqz p4, :cond_1

    const/16 v18, 0x13

    .line 2232
    .local v18, "type":I
    :goto_0
    new-instance v15, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v20

    move/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v15, v0, v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;-><init>(IJ)V

    .line 2234
    .local v15, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    invoke-static {v12, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCacheOrAcquireLock(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 2235
    .local v16, "result":Landroid/graphics/Bitmap;
    if-eqz v16, :cond_2

    const/4 v13, 0x1

    .line 2236
    .local v13, "cacheHit":Z
    :goto_1
    if-nez v16, :cond_5

    .line 2237
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 2238
    .local v19, "urls":[Ljava/lang/String;
    move-object/from16 v0, v19

    array-length v10, v0

    .line 2239
    .local v10, "artCount":I
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 2240
    .local v9, "bitmaps":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    if-ge v14, v10, :cond_3

    .line 2241
    aget-object v5, v19, v14

    div-int/lit8 v7, p2, 0x2

    div-int/lit8 v8, p3, 0x2

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v5, v7, v8, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 2244
    .local v11, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v11, :cond_0

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2240
    :cond_0
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 2231
    .end local v9    # "bitmaps":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    .end local v10    # "artCount":I
    .end local v11    # "bitmap":Landroid/graphics/Bitmap;
    .end local v13    # "cacheHit":Z
    .end local v14    # "i":I
    .end local v15    # "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .end local v16    # "result":Landroid/graphics/Bitmap;
    .end local v18    # "type":I
    .end local v19    # "urls":[Ljava/lang/String;
    :cond_1
    const/16 v18, 0x9

    goto :goto_0

    .line 2235
    .restart local v15    # "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .restart local v16    # "result":Landroid/graphics/Bitmap;
    .restart local v18    # "type":I
    :cond_2
    const/4 v13, 0x0

    goto :goto_1

    .line 2248
    .restart local v9    # "bitmaps":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    .restart local v10    # "artCount":I
    .restart local v13    # "cacheHit":Z
    .restart local v14    # "i":I
    .restart local v19    # "urls":[Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 2249
    new-instance v4, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2252
    .local v4, "canvas":Landroid/graphics/Canvas;
    const/16 v6, 0x9

    .local v6, "style":I
    move-object/from16 v5, p0

    move/from16 v7, p2

    move/from16 v8, p3

    .line 2253
    invoke-static/range {v4 .. v9}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxPlaylistArt(Landroid/graphics/Canvas;Landroid/content/Context;IIILjava/util/List;)Z

    .line 2255
    if-eqz p4, :cond_4

    .line 2256
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;II)Z

    .line 2258
    :cond_4
    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-static {v12, v0, v5, v15}, Lcom/google/android/music/utils/AlbumArtUtils;->putBitmapInCacheAndReleaseLock(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Ljava/util/Set;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V

    .line 2260
    .end local v4    # "canvas":Landroid/graphics/Canvas;
    .end local v6    # "style":I
    .end local v9    # "bitmaps":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    .end local v10    # "artCount":I
    .end local v14    # "i":I
    .end local v19    # "urls":[Ljava/lang/String;
    :cond_5
    if-eqz v12, :cond_6

    .line 2261
    move-object/from16 v0, v17

    invoke-static {v0, v13}, Lcom/google/android/music/utils/AlbumArtUtils;->trackCacheUsage(Ljava/lang/String;Z)V

    .line 2264
    :cond_6
    if-nez v16, :cond_7

    .line 2265
    const/4 v5, 0x0

    .line 2271
    :goto_3
    return-object v5

    .line 2268
    :cond_7
    if-eqz v12, :cond_8

    .line 2269
    const/4 v5, 0x1

    move/from16 v0, p2

    move/from16 v1, p3

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v16

    :cond_8
    move-object/from16 v5, v16

    .line 2271
    goto :goto_3
.end method

.method public static getMusicUserContentPromoteNautilusPipe(Landroid/content/Context;)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 3003
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0201f4

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3005
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 3006
    const-string v5, "AlbumArtUtils"

    const-string v6, "Could not create bitmap"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3032
    :cond_0
    :goto_0
    return-object v4

    .line 3009
    :cond_1
    const/4 v2, 0x0

    .line 3011
    .local v2, "fd":[Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 3012
    new-instance v3, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    const/4 v5, 0x1

    aget-object v5, v2, v5

    invoke-direct {v3, v5}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 3013
    .local v3, "out":Ljava/io/OutputStream;
    sget-object v5, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v6, Lcom/google/android/music/utils/AlbumArtUtils$3;

    invoke-direct {v6, v0, v3}, Lcom/google/android/music/utils/AlbumArtUtils$3;-><init>(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V

    invoke-static {v5, v6}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3029
    .end local v3    # "out":Ljava/io/OutputStream;
    :goto_1
    if-eqz v2, :cond_0

    array-length v5, v2

    if-lez v5, :cond_0

    .line 3030
    const/4 v4, 0x0

    aget-object v4, v2, v4

    goto :goto_0

    .line 3026
    :catch_0
    move-exception v1

    .line 3027
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "AlbumArtUtils"

    const-string v6, "Could not create pipe"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static getPlaylistMembersCursor(Landroid/content/Context;IJ)Landroid/database/Cursor;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "playlistId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 2502
    and-int/lit8 v9, p1, 0xf

    .line 2503
    .local v9, "fauxArtType":I
    const/4 v1, 0x0

    .line 2504
    .local v1, "uri":Landroid/net/Uri;
    const/4 v7, 0x1

    .line 2510
    .local v7, "includeExternal":Z
    packed-switch v9, :pswitch_data_0

    .line 2533
    :goto_0
    if-eqz v1, :cond_0

    .line 2536
    sget-object v2, Lcom/google/android/music/utils/AlbumArtUtils;->sAlbumIdCols:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v3

    .line 2539
    :cond_0
    :goto_1
    return-object v3

    .line 2514
    :pswitch_0
    const/4 v7, 0x1

    .line 2515
    invoke-static {p2, p3}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 2516
    goto :goto_0

    .line 2518
    :pswitch_1
    invoke-static {p2, p3}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->isAutoPlaylistId(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2519
    const-string v0, "AlbumArtUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid auto-playlist id: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2522
    :cond_1
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v10

    .line 2523
    .local v10, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {p2, p3, v6, v10}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v8

    .line 2525
    .local v8, "autoPlaylist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    invoke-virtual {v8}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mayIncludeExternalContent()Z

    move-result v7

    .line 2526
    invoke-virtual {v8, p0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    .line 2527
    goto :goto_0

    .line 2510
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getPreferredConfig()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 597
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public static getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "cropToSquare"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1674
    if-lez p2, :cond_0

    if-lez p3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1675
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must specify target height and width and url"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1681
    :cond_1
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->cacheable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1682
    new-instance v8, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    const/16 v0, 0x14

    invoke-static {p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v8, v0, v2, v3}, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;-><init>(IJ)V

    .line 1683
    .local v8, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    invoke-static {p0, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->getSizeKey(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v10

    .line 1684
    .local v10, "sizeKey":Ljava/lang/String;
    invoke-static {p0, p2, p3, v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;

    move-result-object v6

    .line 1691
    .local v6, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    :goto_0
    invoke-static {v6, v8}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCacheOrAcquireLock(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1692
    .local v9, "result":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_5

    const/4 v7, 0x1

    .line 1693
    .local v7, "cacheHit":Z
    :goto_1
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    .line 1694
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtworkRaw(Landroid/content/Context;Ljava/lang/String;II[IZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1695
    invoke-static {v6, v9, v4, v8}, Lcom/google/android/music/utils/AlbumArtUtils;->putBitmapInCacheAndReleaseLock(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Ljava/util/Set;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V

    .line 1701
    :cond_2
    if-eqz v6, :cond_3

    .line 1702
    invoke-static {v10, v7}, Lcom/google/android/music/utils/AlbumArtUtils;->trackCacheUsage(Ljava/lang/String;Z)V

    .line 1705
    :cond_3
    if-nez v9, :cond_6

    .line 1715
    :goto_2
    return-object v4

    .line 1686
    .end local v6    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    .end local v7    # "cacheHit":Z
    .end local v8    # "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .end local v9    # "result":Landroid/graphics/Bitmap;
    .end local v10    # "sizeKey":Ljava/lang/String;
    :cond_4
    const/4 v6, 0x0

    .line 1687
    .restart local v6    # "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    const/4 v8, 0x0

    .line 1688
    .restart local v8    # "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    const/4 v10, 0x0

    .restart local v10    # "sizeKey":Ljava/lang/String;
    goto :goto_0

    .line 1692
    .restart local v9    # "result":Landroid/graphics/Bitmap;
    :cond_5
    const/4 v7, 0x0

    goto :goto_1

    .line 1709
    .restart local v7    # "cacheHit":Z
    :cond_6
    if-eqz v6, :cond_7

    .line 1712
    invoke-static {p2, p3, v9, p4}, Lcom/google/android/music/utils/AlbumArtUtils;->copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    :cond_7
    move-object v4, v9

    .line 1715
    goto :goto_2
.end method

.method public static getRealNonAlbumArtCopy(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "cropToSquare"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1636
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1648
    :cond_0
    :goto_0
    return-object v3

    .line 1639
    :cond_1
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->cacheable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1640
    new-instance v1, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;

    const/16 v3, 0x14

    invoke-static {p1}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;-><init>(IJ)V

    .line 1643
    .local v1, "key":Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    invoke-static {p0, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->getSizeKey(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    .line 1644
    .local v2, "sizeKey":Ljava/lang/String;
    invoke-static {p0, p2, p3, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->getCache(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/music/utils/LruCache;

    move-result-object v0

    .line 1646
    .local v0, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    invoke-static {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromCache(Lcom/google/android/music/utils/LruCache;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {p2, p3, v3, p4}, Lcom/google/android/music/utils/AlbumArtUtils;->copyAliasedBitmapHelper(IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0
.end method

.method private static getSizeKey(Landroid/content/Context;II)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 1067
    sget v6, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheSmallBucketMaxDimen:I

    if-nez v6, :cond_1

    .line 1072
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f00f3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheSmallBucketMaxDimen:I

    .line 1074
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f00f4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheMediumBucketMaxDimen:I

    .line 1079
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .line 1080
    .local v5, "ref":Ljava/lang/Object;
    invoke-static {p0, v5}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 1081
    .local v4, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v0, 0x3

    .line 1082
    .local v0, "approxRowNum":I
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isLargeScreen()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1083
    const/4 v0, 0x4

    .line 1087
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d000b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 1088
    .local v1, "cardColNum":I
    mul-int v2, v1, v0

    .line 1091
    .local v2, "cardsPerScreen":I
    int-to-double v6, v2

    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    mul-double/2addr v6, v8

    double-to-int v6, v6

    sput v6, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheBucketMediumSize:I

    .line 1092
    const/16 v6, 0x1e

    sput v6, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheBucketSmallSize:I

    .line 1093
    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 1096
    .end local v0    # "approxRowNum":I
    .end local v1    # "cardColNum":I
    .end local v2    # "cardsPerScreen":I
    .end local v4    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v5    # "ref":Ljava/lang/Object;
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1097
    .local v3, "maxDimen":I
    sget v6, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheSmallBucketMaxDimen:I

    if-gt v3, v6, :cond_3

    .line 1098
    const-string v6, "SMALL"

    .line 1102
    :goto_1
    return-object v6

    .line 1084
    .end local v3    # "maxDimen":I
    .restart local v0    # "approxRowNum":I
    .restart local v4    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v5    # "ref":Ljava/lang/Object;
    :cond_2
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1085
    const/4 v0, 0x5

    goto :goto_0

    .line 1099
    .end local v0    # "approxRowNum":I
    .end local v4    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v5    # "ref":Ljava/lang/Object;
    .restart local v3    # "maxDimen":I
    :cond_3
    sget v6, Lcom/google/android/music/utils/AlbumArtUtils;->sCacheMediumBucketMaxDimen:I

    if-gt v3, v6, :cond_4

    .line 1100
    const-string v6, "MEDIUM"

    goto :goto_1

    .line 1102
    :cond_4
    const-string v6, "LARGE"

    goto :goto_1
.end method

.method private static declared-synchronized getStaticFauxArtCacheDir(Landroid/content/Context;)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1847
    const-class v2, Lcom/google/android/music/utils/AlbumArtUtils;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->mCacheDir:Ljava/io/File;

    if-nez v1, :cond_0

    .line 1848
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "faux_artwork"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->mCacheDir:Ljava/io/File;

    .line 1849
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->mCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1850
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->mCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1852
    :try_start_1
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/google/android/music/utils/AlbumArtUtils;->mCacheDir:Ljava/io/File;

    const-string v4, ".nomedia"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858
    :cond_0
    :goto_0
    :try_start_2
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->mCacheDir:Ljava/io/File;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v2

    return-object v1

    .line 1853
    :catch_0
    move-exception v0

    .line 1854
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    const-string v1, "AlbumArtUtils"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1847
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static getStaticFauxArtFile(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;II)Ljava/io/File;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "albumId"    # J
    .param p4, "mainLabel"    # Ljava/lang/String;
    .param p5, "subLabel"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I

    .prologue
    .line 1872
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fauxart_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p1 .. p3}, Lcom/google/android/music/utils/AlbumArtUtils;->getStaticFauxArtKey(IJ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1873
    .local v16, "fname":Ljava/lang/String;
    new-instance v15, Ljava/io/File;

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/AlbumArtUtils;->getStaticFauxArtCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-direct {v15, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1874
    .local v15, "f":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1876
    const/4 v3, -0x1

    move/from16 v0, p6

    if-eq v0, v3, :cond_0

    const/4 v3, -0x1

    move/from16 v0, p7

    if-ne v0, v3, :cond_2

    .line 1877
    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/AlbumArtUtils;->getMaxAlbumArtSize(Landroid/content/Context;)I

    move-result v8

    .line 1881
    .local v8, "size":I
    :goto_0
    or-int/lit8 v4, p1, 0x10

    const/4 v5, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v3, p0

    move-wide/from16 v6, p2

    move v9, v8

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-static/range {v3 .. v14}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1883
    .local v2, "bm":Landroid/graphics/Bitmap;
    const/16 v17, 0x0

    .line 1885
    .local v17, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v18, Ljava/io/FileOutputStream;

    move-object/from16 v0, v18

    invoke-direct {v0, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1886
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .local v18, "out":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1889
    if-eqz v18, :cond_1

    .line 1891
    :try_start_2
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1897
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v8    # "size":I
    .end local v18    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    return-object v15

    .line 1879
    :cond_2
    invoke-static/range {p6 .. p7}, Ljava/lang/Math;->max(II)I

    move-result v8

    .restart local v8    # "size":I
    goto :goto_0

    .line 1887
    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v3

    .line 1889
    :goto_2
    if-eqz v17, :cond_1

    .line 1891
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 1892
    :catch_1
    move-exception v3

    goto :goto_1

    .line 1889
    :catchall_0
    move-exception v3

    :goto_3
    if-eqz v17, :cond_3

    .line 1891
    :try_start_4
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1893
    :cond_3
    :goto_4
    throw v3

    .line 1892
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v18    # "out":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v3

    goto :goto_1

    .end local v18    # "out":Ljava/io/FileOutputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v4

    goto :goto_4

    .line 1889
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v18    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/FileOutputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1887
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v18    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v3

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/FileOutputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method private static getStaticFauxArtKey(IJ)Ljava/lang/String;
    .locals 2
    .param p0, "style"    # I
    .param p1, "albumId"    # J

    .prologue
    .line 1867
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getStaticFauxArtPipe(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "albumId"    # J
    .param p4, "mainLabel"    # Ljava/lang/String;
    .param p5, "subLabel"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I

    .prologue
    .line 1903
    const/4 v3, -0x1

    move/from16 v0, p6

    if-eq v0, v3, :cond_0

    const/4 v3, -0x1

    move/from16 v0, p7

    if-ne v0, v3, :cond_1

    .line 1904
    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/AlbumArtUtils;->getMaxAlbumArtSize(Landroid/content/Context;)I

    move-result v8

    .line 1908
    .local v8, "size":I
    :goto_0
    or-int/lit8 v4, p1, 0x10

    const/4 v5, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v3, p0

    move-wide/from16 v6, p2

    move v9, v8

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-static/range {v3 .. v14}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1910
    .local v2, "bm":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 1911
    const-string v3, "AlbumArtUtils"

    const-string v4, "Could not create bitmap"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1912
    const/4 v3, 0x0

    .line 1941
    :goto_1
    return-object v3

    .line 1906
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v8    # "size":I
    :cond_1
    invoke-static/range {p6 .. p7}, Ljava/lang/Math;->max(II)I

    move-result v8

    .restart local v8    # "size":I
    goto :goto_0

    .line 1914
    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    :cond_2
    const/16 v16, 0x0

    .line 1916
    .local v16, "fd":[Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v16

    .line 1917
    new-instance v17, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    const/4 v3, 0x1

    aget-object v3, v16, v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 1918
    .local v17, "out":Ljava/io/OutputStream;
    sget-object v3, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v4, Lcom/google/android/music/utils/AlbumArtUtils$2;

    move-object/from16 v0, v17

    invoke-direct {v4, v2, v0}, Lcom/google/android/music/utils/AlbumArtUtils$2;-><init>(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V

    invoke-static {v3, v4}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1938
    .end local v17    # "out":Ljava/io/OutputStream;
    :goto_2
    if-eqz v16, :cond_3

    move-object/from16 v0, v16

    array-length v3, v0

    if-lez v3, :cond_3

    .line 1939
    const/4 v3, 0x0

    aget-object v3, v16, v3

    goto :goto_1

    .line 1935
    :catch_0
    move-exception v15

    .line 1936
    .local v15, "e":Ljava/io/IOException;
    const-string v3, "AlbumArtUtils"

    const-string v4, "Could not create pipe"

    invoke-static {v3, v4, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1941
    .end local v15    # "e":Ljava/io/IOException;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getUniqueArt(Landroid/content/Context;IJIIILcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;)Ljava/util/List;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "style"    # I
    .param p2, "playlistId"    # J
    .param p4, "maxImages"    # I
    .param p5, "w"    # I
    .param p6, "h"    # I
    .param p7, "childAlbums"    # Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    .param p8, "missingArtworkSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IJIII",
            "Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;",
            "Lcom/google/android/music/download/artwork/AlbumIdSink;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2428
    new-instance v9, Ljava/util/HashSet;

    move/from16 v0, p4

    invoke-direct {v9, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 2429
    .local v9, "albumIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 2431
    .local v13, "images":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v12, 0x0

    .line 2433
    .local v12, "childAlbumsIterator":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;
    if-eqz p7, :cond_0

    .line 2434
    invoke-interface/range {p7 .. p7}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;->createIterator()Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;

    move-result-object v12

    .line 2437
    :cond_0
    if-nez v12, :cond_1

    .line 2438
    invoke-static/range {p0 .. p3}, Lcom/google/android/music/utils/AlbumArtUtils;->getPlaylistMembersCursor(Landroid/content/Context;IJ)Landroid/database/Cursor;

    move-result-object v11

    .line 2439
    .local v11, "c":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 2440
    new-instance v12, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;

    .end local v12    # "childAlbumsIterator":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;
    const/4 v3, 0x0

    invoke-direct {v12, v11, v3}, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;-><init>(Landroid/database/Cursor;I)V

    .line 2444
    .end local v11    # "c":Landroid/database/Cursor;
    .restart local v12    # "childAlbumsIterator":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;
    :cond_1
    if-eqz v12, :cond_5

    .line 2446
    :cond_2
    :goto_0
    :try_start_0
    invoke-interface {v12}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2447
    invoke-interface {v12}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;->next()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2448
    .local v2, "albumIdKey":Ljava/lang/Long;
    invoke-interface {v9, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2451
    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2452
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->playlistArtScaleFactor(II)F

    move-result v14

    .line 2453
    .local v14, "scaleFactor":F
    move/from16 v0, p5

    int-to-float v3, v0

    mul-float/2addr v3, v14

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v6, v4

    .line 2454
    .local v6, "scaledWidth":I
    move/from16 v0, p6

    int-to-float v3, v0

    mul-float/2addr v3, v14

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v7, v4

    .line 2455
    .local v7, "scaledHeight":I
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v8}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtwork(Landroid/content/Context;JIIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 2457
    .local v10, "art":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_4

    .line 2458
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2459
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    move/from16 v0, p4

    if-lt v3, v0, :cond_2

    .line 2469
    .end local v2    # "albumIdKey":Ljava/lang/Long;
    .end local v6    # "scaledWidth":I
    .end local v7    # "scaledHeight":I
    .end local v10    # "art":Landroid/graphics/Bitmap;
    .end local v14    # "scaleFactor":F
    :cond_3
    invoke-interface {v12}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;->close()V

    .line 2474
    :goto_1
    return-object v13

    .line 2463
    .restart local v2    # "albumIdKey":Ljava/lang/Long;
    .restart local v6    # "scaledWidth":I
    .restart local v7    # "scaledHeight":I
    .restart local v10    # "art":Landroid/graphics/Bitmap;
    .restart local v14    # "scaleFactor":F
    :cond_4
    if-eqz p8, :cond_2

    .line 2464
    :try_start_1
    move-object/from16 v0, p8

    invoke-interface {v0, v2}, Lcom/google/android/music/download/artwork/AlbumIdSink;->report(Ljava/lang/Long;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2469
    .end local v2    # "albumIdKey":Ljava/lang/Long;
    .end local v6    # "scaledWidth":I
    .end local v7    # "scaledHeight":I
    .end local v10    # "art":Landroid/graphics/Bitmap;
    .end local v14    # "scaleFactor":F
    :catchall_0
    move-exception v3

    invoke-interface {v12}, Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;->close()V

    throw v3

    .line 2472
    :cond_5
    const-string v3, "AlbumArtUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not create cursor for children of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static declared-synchronized initBitmapPoolForMainProcess()V
    .locals 20

    .prologue
    .line 529
    const-class v12, Lcom/google/android/music/utils/AlbumArtUtils;

    monitor-enter v12

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v10

    .line 531
    .local v10, "maxMemory":J
    const/4 v2, 0x4

    .line 532
    .local v2, "BITMAP_POOL_CAPACITY_ITEMS":I
    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    .line 534
    .local v6, "POOL_CAPACITY_FACTOR":D
    const-wide/32 v4, 0x77a100

    .line 536
    .local v4, "MAX_POOL_CAPACITY":J
    const-wide/32 v14, 0x77a100

    const-wide v16, 0x3f947ae147ae147bL    # 0.02

    long-to-double v0, v10

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    long-to-int v3, v14

    .line 539
    .local v3, "capacity":I
    const-wide/32 v14, 0x77a100

    const-wide v16, 0x3fa374bc6a7ef9dbL    # 0.038

    long-to-double v0, v10

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    long-to-int v8, v14

    .line 545
    .local v8, "largestItem":I
    new-instance v9, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    const/4 v13, 0x4

    invoke-direct {v9, v13, v3, v8}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;-><init>(III)V

    sput-object v9, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 546
    monitor-exit v12

    return-void

    .line 529
    .end local v3    # "capacity":I
    .end local v8    # "largestItem":I
    :catchall_0
    move-exception v9

    monitor-exit v12

    throw v9
.end method

.method public static declared-synchronized initBitmapPoolForUIProcess()V
    .locals 20

    .prologue
    .line 489
    const-class v12, Lcom/google/android/music/utils/AlbumArtUtils;

    monitor-enter v12

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v10

    .line 504
    .local v10, "maxMemory":J
    const/16 v2, 0xf

    .line 505
    .local v2, "BITMAP_POOL_CAPACITY_ITEMS":I
    const-wide v6, 0x3fb47ae147ae147bL    # 0.08

    .line 507
    .local v6, "POOL_CAPACITY_FACTOR":D
    const-wide/32 v4, 0x1de8400

    .line 509
    .local v4, "MAX_POOL_CAPACITY":J
    const-wide/32 v14, 0x1de8400

    const-wide v16, 0x3fb47ae147ae147bL    # 0.08

    long-to-double v0, v10

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    long-to-int v3, v14

    .line 512
    .local v3, "capacity":I
    const-wide/32 v14, 0x77a100

    const-wide v16, 0x3fa374bc6a7ef9dbL    # 0.038

    long-to-double v0, v10

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    long-to-int v8, v14

    .line 518
    .local v8, "largestItem":I
    new-instance v9, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    const/16 v13, 0xf

    invoke-direct {v9, v13, v3, v8}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;-><init>(III)V

    sput-object v9, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    monitor-exit v12

    return-void

    .line 489
    .end local v3    # "capacity":I
    .end local v8    # "largestItem":I
    :catchall_0
    move-exception v9

    monitor-exit v12

    throw v9
.end method

.method private static initializeMaxAlbumArtSize(Landroid/content/Context;)V
    .locals 28
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2889
    const-string v23, "window"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/view/WindowManager;

    .line 2890
    .local v22, "wm":Landroid/view/WindowManager;
    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    .line 2894
    .local v9, "display":Landroid/view/Display;
    invoke-virtual {v9}, Landroid/view/Display;->getWidth()I

    move-result v21

    .line 2895
    .local v21, "widthPixels":I
    invoke-virtual {v9}, Landroid/view/Display;->getHeight()I

    move-result v16

    .line 2896
    .local v16, "heightPixels":I
    move/from16 v0, v21

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 2899
    .local v17, "minDimensionPixels":I
    sget-object v23, Lcom/google/android/music/utils/AlbumArtUtils;->BUCKET_SIZES:[I

    move-object/from16 v0, v23

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->findClosest([II)I

    move-result v8

    .line 2902
    .local v8, "bucketIndex":I
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->getCacheLimit(Landroid/content/Context;)J

    move-result-wide v10

    .line 2903
    .local v10, "cacheLimit":J
    :goto_0
    if-lez v8, :cond_0

    .line 2904
    sget-object v23, Lcom/google/android/music/utils/AlbumArtUtils;->BUCKET_SIZES:[I

    aget v23, v23, v8

    invoke-static/range {v23 .. v23}, Lcom/google/android/music/utils/AlbumArtUtils;->estimatedArtFileSize(I)J

    move-result-wide v14

    .line 2905
    .local v14, "fileSize":J
    div-long v12, v10, v14

    .line 2906
    .local v12, "capacity":J
    const-wide/16 v24, 0x12c

    cmp-long v23, v12, v24

    if-ltz v23, :cond_2

    .line 2913
    .end local v12    # "capacity":J
    .end local v14    # "fileSize":J
    :cond_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v18

    .line 2914
    .local v18, "maxMemory":J
    const-wide v24, 0x3fa47ae147ae147bL    # 0.04

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-long v4, v0

    .line 2915
    .local v4, "albumArtSizeLimit":J
    :goto_1
    if-lez v8, :cond_1

    .line 2916
    sget-object v23, Lcom/google/android/music/utils/AlbumArtUtils;->BUCKET_SIZES:[I

    aget v20, v23, v8

    .line 2917
    .local v20, "size":I
    sget-object v23, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v20

    move/from16 v1, v20

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->estimatedSize(IILandroid/graphics/Bitmap$Config;)I

    move-result v23

    move/from16 v0, v23

    int-to-long v6, v0

    .line 2918
    .local v6, "albumSize":J
    cmp-long v23, v6, v4

    if-gtz v23, :cond_3

    .line 2924
    .end local v6    # "albumSize":J
    .end local v20    # "size":I
    :cond_1
    sget-object v23, Lcom/google/android/music/utils/AlbumArtUtils;->BUCKET_SIZES:[I

    aget v23, v23, v8

    sput v23, Lcom/google/android/music/utils/AlbumArtUtils;->sMaxAlbumArtSize:I

    .line 2925
    const/16 v23, 0x1

    sput-boolean v23, Lcom/google/android/music/utils/AlbumArtUtils;->sMaxAlbumArtSizeInitalized:Z

    .line 2926
    return-void

    .line 2909
    .end local v4    # "albumArtSizeLimit":J
    .end local v18    # "maxMemory":J
    .restart local v12    # "capacity":J
    .restart local v14    # "fileSize":J
    :cond_2
    add-int/lit8 v8, v8, -0x1

    .line 2910
    goto :goto_0

    .line 2921
    .end local v12    # "capacity":J
    .end local v14    # "fileSize":J
    .restart local v4    # "albumArtSizeLimit":J
    .restart local v6    # "albumSize":J
    .restart local v18    # "maxMemory":J
    .restart local v20    # "size":I
    :cond_3
    add-int/lit8 v8, v8, -0x1

    .line 2922
    goto :goto_1
.end method

.method private static isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 999
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static loadBitmap(Landroid/content/Context;Ljava/io/FileDescriptor;II[IZ)Landroid/graphics/Bitmap;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "outOriginalBounds"    # [I
    .param p5, "cropToSquare"    # Z

    .prologue
    .line 1376
    const/4 v11, 0x1

    .line 1378
    .local v11, "sampleSize":I
    :try_start_0
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1379
    .local v10, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v12, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iget-object v12, v12, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iput-object v12, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1385
    const/4 v12, 0x1

    iput-boolean v12, v10, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1386
    const/4 v12, 0x1

    iput v12, v10, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1387
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v10}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1389
    iget v12, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v12, :cond_0

    iget v12, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gtz v12, :cond_1

    .line 1390
    :cond_0
    const/4 v5, 0x0

    .line 1457
    .end local v10    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return-object v5

    .line 1395
    .restart local v10    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "music_skip_wbmp_images_before_lmp_mr1"

    const/4 v14, 0x1

    invoke-static {v12, v13, v14}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpMr1OrGreater()Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "image/vnd.wap.wbmp"

    iget-object v13, v10, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1400
    const/4 v5, 0x0

    goto :goto_0

    .line 1403
    :cond_2
    if-eqz p4, :cond_3

    .line 1404
    const/4 v12, 0x0

    iget v13, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v13, p4, v12

    .line 1405
    const/4 v12, 0x1

    iget v13, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v13, p4, v12

    .line 1407
    :cond_3
    iget v12, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    shr-int/lit8 v9, v12, 0x1

    .line 1408
    .local v9, "nextWidth":I
    iget v12, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    shr-int/lit8 v8, v12, 0x1

    .line 1409
    .local v8, "nextHeight":I
    :goto_1
    move/from16 v0, p2

    if-le v9, v0, :cond_4

    move/from16 v0, p3

    if-le v8, v0, :cond_4

    .line 1410
    shl-int/lit8 v11, v11, 0x1

    .line 1411
    shr-int/lit8 v9, v9, 0x1

    .line 1412
    shr-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1416
    :cond_4
    const/4 v7, 0x0

    .line 1417
    .local v7, "inputBufferBitmap":Landroid/graphics/Bitmap;
    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v13, 0xb

    if-lt v12, v13, :cond_5

    sget-object v12, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    iget v13, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v14, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v15

    invoke-virtual {v12, v13, v14, v15}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->willCacheBitmap(IILandroid/graphics/Bitmap$Config;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1419
    const/4 v11, 0x1

    .line 1420
    sget-object v12, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    iget v13, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v14, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v15

    invoke-virtual {v12, v13, v14, v15}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1422
    invoke-static {v10, v7}, Lcom/google/android/music/utils/PostFroyoUtils$BitmapFactoryOptionsCompat;->setInBitmap(Landroid/graphics/BitmapFactory$Options;Landroid/graphics/Bitmap;)V

    .line 1425
    :cond_5
    iput v11, v10, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1426
    const/4 v12, 0x0

    iput-boolean v12, v10, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 1427
    const/4 v5, 0x0

    .line 1429
    .local v5, "b":Landroid/graphics/Bitmap;
    const/4 v12, 0x0

    :try_start_1
    move-object/from16 v0, p1

    invoke-static {v0, v12, v10}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 1438
    :cond_6
    :goto_2
    if-nez v5, :cond_7

    .line 1439
    const/4 v5, 0x0

    goto :goto_0

    .line 1431
    :catch_0
    move-exception v6

    .line 1433
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    if-eqz v7, :cond_6

    .line 1434
    const/4 v12, 0x0

    :try_start_2
    invoke-static {v10, v12}, Lcom/google/android/music/utils/PostFroyoUtils$BitmapFactoryOptionsCompat;->setInBitmap(Landroid/graphics/BitmapFactory$Options;Landroid/graphics/Bitmap;)V

    .line 1435
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v10}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_2

    .line 1444
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :cond_7
    if-eqz p5, :cond_8

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    if-eqz v12, :cond_8

    .line 1445
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v3, v12

    .line 1446
    .local v3, "actualWidth":F
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    int-to-float v2, v12

    .line 1447
    .local v2, "actualHeight":F
    div-float v4, v3, v2

    .line 1448
    .local v4, "aspectRatio":F
    move/from16 v0, p3

    int-to-float v12, v0

    mul-float/2addr v12, v4

    float-to-int v0, v12

    move/from16 p2, v0

    .line 1451
    .end local v2    # "actualHeight":F
    .end local v3    # "actualWidth":F
    .end local v4    # "aspectRatio":F
    :cond_8
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v5, v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleToSize(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v5

    .line 1453
    goto/16 :goto_0

    .line 1454
    .end local v5    # "b":Landroid/graphics/Bitmap;
    .end local v7    # "inputBufferBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "nextHeight":I
    .end local v9    # "nextWidth":I
    .end local v10    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_1
    move-exception v6

    .line 1455
    .local v6, "e":Ljava/lang/OutOfMemoryError;
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v6, v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->reportAndRethrow(Ljava/lang/OutOfMemoryError;II)V

    .line 1457
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method public static makeDefaultArtId(Ljava/lang/String;)J
    .locals 6
    .param p0, "artUrl"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x0

    .line 839
    invoke-static {p0}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v0

    .line 840
    .local v0, "album_id":J
    cmp-long v2, v0, v4

    if-nez v2, :cond_0

    .line 841
    const-wide/16 v0, -0x1

    .line 843
    :cond_0
    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    .line 844
    neg-long v0, v0

    .line 846
    :cond_1
    return-wide v0
.end method

.method private static playlistArtScaleFactor(II)F
    .locals 2
    .param p0, "style"    # I
    .param p1, "imageIndex"    # I

    .prologue
    const v0, 0x3eaaaaab

    .line 2483
    packed-switch p0, :pswitch_data_0

    .line 2491
    const/4 v1, 0x2

    if-ge p1, v1, :cond_1

    .line 2492
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2496
    :cond_0
    :goto_0
    return v0

    .line 2485
    :pswitch_0
    if-nez p1, :cond_0

    .line 2486
    const v0, 0x3f2aaaab

    goto :goto_0

    .line 2493
    :cond_1
    const/4 v1, 0x4

    if-ge p1, v1, :cond_0

    .line 2494
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0

    .line 2483
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public static playlistTypeToArtStyle(I)I
    .locals 2
    .param p0, "listType"    # I

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x1

    .line 1976
    sparse-switch p0, :sswitch_data_0

    .line 1998
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown playlist type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1980
    :sswitch_0
    const/4 v0, 0x2

    .line 1996
    :goto_0
    :sswitch_1
    return v0

    .line 1984
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    move v0, v1

    .line 1986
    goto :goto_0

    :sswitch_4
    move v0, v1

    .line 1989
    goto :goto_0

    .line 1992
    :sswitch_5
    const/16 v0, 0x9

    goto :goto_0

    .line 1976
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0xa -> :sswitch_4
        0x32 -> :sswitch_2
        0x33 -> :sswitch_5
        0x3c -> :sswitch_2
        0x46 -> :sswitch_5
        0x47 -> :sswitch_1
        0x50 -> :sswitch_1
        0x64 -> :sswitch_3
    .end sparse-switch
.end method

.method private static putBitmapInCache(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "key"    # Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1147
    .local p0, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->throwIfRecycled(Landroid/graphics/Bitmap;)V

    .line 1148
    if-eqz p0, :cond_0

    .line 1149
    monitor-enter p0

    .line 1150
    :try_start_0
    invoke-virtual {p0, p2, p1}, Lcom/google/android/music/utils/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151
    monitor-exit p0

    .line 1153
    :cond_0
    return-void

    .line 1151
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static putBitmapInCacheAndReleaseLock(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Ljava/util/Set;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "key"    # Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1047
    .local p0, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;Landroid/graphics/Bitmap;>;"
    .local p2, "missingArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-nez p0, :cond_0

    .line 1059
    :goto_0
    return-void

    .line 1050
    :cond_0
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    monitor-enter v1

    .line 1051
    if-eqz p1, :cond_2

    .line 1052
    if-eqz p2, :cond_1

    :try_start_0
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1053
    :cond_1
    invoke-static {p0, p1, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->putBitmapInCache(Lcom/google/android/music/utils/LruCache;Landroid/graphics/Bitmap;Lcom/google/android/music/utils/AlbumArtUtils$ArtCacheKey;)V

    .line 1056
    :cond_2
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1057
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sCachedBitmapLocks:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1058
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1539
    if-eqz p0, :cond_0

    .line 1540
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    invoke-virtual {v0, p0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1542
    :cond_0
    return-void
.end method

.method private static renderFauxLabel(Landroid/graphics/Canvas;IIIIZLjava/lang/String;IIZIIZIIF)V
    .locals 41
    .param p0, "c"    # Landroid/graphics/Canvas;
    .param p1, "width"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "fontHeight"    # I
    .param p5, "bold"    # Z
    .param p6, "label"    # Ljava/lang/String;
    .param p7, "color"    # I
    .param p8, "fadeWidth"    # I
    .param p9, "rightJustify"    # Z
    .param p10, "rightMargin"    # I
    .param p11, "overrideW"    # I
    .param p12, "centerY"    # Z
    .param p13, "centerBoxHeight"    # I
    .param p14, "shadowColor"    # I
    .param p15, "shadowRadius"    # F

    .prologue
    .line 2662
    if-eqz p6, :cond_0

    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 2747
    :cond_0
    :goto_0
    return-void

    .line 2665
    :cond_1
    new-instance v34, Landroid/text/TextPaint;

    const/16 v7, 0x81

    move-object/from16 v0, v34

    invoke-direct {v0, v7}, Landroid/text/TextPaint;-><init>(I)V

    .line 2666
    .local v34, "paint":Landroid/text/TextPaint;
    move/from16 v0, p4

    int-to-float v7, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2667
    if-eqz p5, :cond_2

    .line 2668
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2671
    :cond_2
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    .line 2672
    .local v15, "bounds":Landroid/graphics/Rect;
    const/4 v7, 0x0

    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->length()I

    move-result v8

    move-object/from16 v0, v34

    move-object/from16 v1, p6

    invoke-virtual {v0, v1, v7, v8, v15}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2674
    invoke-static/range {v34 .. v34}, Lcom/google/android/music/utils/AlbumArtUtils;->getCapHeight(Landroid/text/TextPaint;)I

    move-result v16

    .line 2675
    .local v16, "capHeight":I
    if-eqz p12, :cond_3

    .line 2676
    sub-int v7, p13, v16

    div-int/lit8 v7, v7, 0x2

    add-int p3, p3, v7

    .line 2678
    :cond_3
    add-int v14, p3, v16

    .line 2684
    .local v14, "baselineY":I
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v35, v0

    .line 2685
    .local v35, "rightEdge":I
    move/from16 v27, p1

    .line 2686
    .local v27, "maxOffset":I
    move/from16 v0, v35

    move/from16 v1, p11

    if-lt v0, v1, :cond_4

    .line 2687
    const/16 p9, 0x0

    .line 2689
    :cond_4
    move/from16 v0, v35

    move/from16 v1, v27

    if-le v0, v1, :cond_6

    const/16 v17, 0x1

    .line 2690
    .local v17, "clipped":Z
    :goto_1
    move-object/from16 v0, v34

    move/from16 v1, p7

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 2691
    const/4 v7, 0x0

    cmpl-float v7, p15, v7

    if-lez v7, :cond_5

    .line 2692
    const v7, 0x3f34fdf4    # 0.707f

    mul-float v36, v7, p15

    .line 2693
    .local v36, "shadowOffset":F
    move-object/from16 v0, v34

    move/from16 v1, p15

    move/from16 v2, v36

    move/from16 v3, v36

    move/from16 v4, p14

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 2695
    .end local v36    # "shadowOffset":F
    :cond_5
    if-eqz v17, :cond_7

    .line 2698
    const/16 v24, 0x2

    .line 2699
    .local v24, "marginLeft":I
    const/16 v26, 0x2

    .line 2700
    .local v26, "marginTop":I
    move/from16 v0, p15

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v7, v8

    add-int/lit8 v25, v7, 0x2

    .line 2701
    .local v25, "marginRight":I
    move/from16 v0, p15

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v7, v8

    add-int/lit8 v23, v7, 0x2

    .line 2702
    .local v23, "marginBottom":I
    iget v7, v15, Landroid/graphics/Rect;->left:I

    add-int v7, v7, p2

    sub-int v29, v7, v24

    .line 2703
    .local v29, "originX":I
    iget v7, v15, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v14

    sub-int v31, v7, v26

    .line 2704
    .local v31, "originY":I
    iget v7, v15, Landroid/graphics/Rect;->right:I

    add-int v7, v7, p2

    add-int v30, v7, v25

    .line 2705
    .local v30, "originX2":I
    iget v7, v15, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v14

    add-int v32, v7, v23

    .line 2706
    .local v32, "originY2":I
    sub-int v38, v30, v29

    .line 2707
    .local v38, "tempW":I
    sub-int v37, v32, v31

    .line 2716
    .local v37, "tempH":I
    sget-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v38

    move/from16 v1, v37

    invoke-virtual {v7, v0, v1, v8}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v39

    .line 2717
    .local v39, "textBitmap":Landroid/graphics/Bitmap;
    new-instance v40, Landroid/graphics/Canvas;

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2718
    .local v40, "textCanvas":Landroid/graphics/Canvas;
    sub-int v7, p2, v29

    int-to-float v7, v7

    sub-int v8, v14, v31

    int-to-float v8, v8

    move-object/from16 v0, v40

    move-object/from16 v1, p6

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v7, v8, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2720
    add-int v7, p2, v27

    sub-int v7, v7, p8

    sub-int v22, v7, v29

    .line 2721
    .local v22, "fadeStart":I
    add-int v20, v22, p8

    .line 2723
    .local v20, "fadeEnd":I
    sget-object v7, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v38

    move/from16 v1, v37

    invoke-virtual {v7, v0, v1, v8}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 2724
    .local v18, "fadeBitmap":Landroid/graphics/Bitmap;
    new-instance v19, Landroid/graphics/Canvas;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2725
    .local v19, "fadeCanvas":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/Paint;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Paint;-><init>()V

    .line 2726
    .local v21, "fadePaint":Landroid/graphics/Paint;
    new-instance v6, Landroid/graphics/LinearGradient;

    move/from16 v0, v22

    int-to-float v7, v0

    const/4 v8, 0x0

    move/from16 v0, v20

    int-to-float v9, v0

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    sget-object v13, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v6 .. v13}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 2730
    .local v6, "fadeGradient":Landroid/graphics/LinearGradient;
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2731
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 2732
    new-instance v28, Landroid/graphics/Paint;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Paint;-><init>()V

    .line 2733
    .local v28, "multiplyPaint":Landroid/graphics/Paint;
    new-instance v7, Landroid/graphics/PorterDuffXfermode;

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v7, v8}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2734
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v18

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v7, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2735
    new-instance v33, Landroid/graphics/Paint;

    invoke-direct/range {v33 .. v33}, Landroid/graphics/Paint;-><init>()V

    .line 2736
    .local v33, "overlayPaint":Landroid/graphics/Paint;
    new-instance v7, Landroid/graphics/PorterDuffXfermode;

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v7, v8}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2737
    move/from16 v0, v29

    int-to-float v7, v0

    move/from16 v0, v31

    int-to-float v8, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v7, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2738
    invoke-static/range {v39 .. v39}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 2739
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 2689
    .end local v6    # "fadeGradient":Landroid/graphics/LinearGradient;
    .end local v17    # "clipped":Z
    .end local v18    # "fadeBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "fadeCanvas":Landroid/graphics/Canvas;
    .end local v20    # "fadeEnd":I
    .end local v21    # "fadePaint":Landroid/graphics/Paint;
    .end local v22    # "fadeStart":I
    .end local v23    # "marginBottom":I
    .end local v24    # "marginLeft":I
    .end local v25    # "marginRight":I
    .end local v26    # "marginTop":I
    .end local v28    # "multiplyPaint":Landroid/graphics/Paint;
    .end local v29    # "originX":I
    .end local v30    # "originX2":I
    .end local v31    # "originY":I
    .end local v32    # "originY2":I
    .end local v33    # "overlayPaint":Landroid/graphics/Paint;
    .end local v37    # "tempH":I
    .end local v38    # "tempW":I
    .end local v39    # "textBitmap":Landroid/graphics/Bitmap;
    .end local v40    # "textCanvas":Landroid/graphics/Canvas;
    :cond_6
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 2741
    .restart local v17    # "clipped":Z
    :cond_7
    if-eqz p9, :cond_8

    .line 2742
    iget v7, v15, Landroid/graphics/Rect;->right:I

    sub-int p2, p10, v7

    .line 2744
    :cond_8
    move/from16 v0, p2

    int-to-float v7, v0

    int-to-float v8, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v7, v8, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public static declared-synchronized report(Ljava/lang/OutOfMemoryError;)V
    .locals 5
    .param p0, "e"    # Ljava/lang/OutOfMemoryError;

    .prologue
    .line 1492
    const-class v2, Lcom/google/android/music/utils/AlbumArtUtils;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/google/android/music/utils/AlbumArtUtils;->sHprofDumped:Z

    if-nez v1, :cond_0

    .line 1493
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/music/utils/AlbumArtUtils;->sHprofDumped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1495
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/music2_hprof_data"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1496
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V

    .line 1497
    const-string v1, "AlbumArtUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Out of memory.\nPlease do the following to copy the heap dump to your computer:\n\n  adb pull "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "And attach the file to your bug report."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1505
    .end local v0    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 1492
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 1501
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static reportAndRethrow(Ljava/lang/OutOfMemoryError;II)V
    .locals 3
    .param p0, "e"    # Ljava/lang/OutOfMemoryError;
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1477
    const-string v0, "AlbumArtUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Out of memory allocating a ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") sized texture."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1479
    invoke-static {p0}, Lcom/google/android/music/utils/AlbumArtUtils;->report(Ljava/lang/OutOfMemoryError;)V

    .line 1480
    throw p0
.end method

.method private static resizeHelper(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "mayScaleUp"    # Z

    .prologue
    .line 1259
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 1260
    .local v1, "outWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 1261
    .local v0, "outHeight":I
    if-nez p3, :cond_0

    if-gt v1, p1, :cond_0

    if-gt v0, p2, :cond_0

    .line 1265
    .end local p0    # "b":Landroid/graphics/Bitmap;
    :goto_0
    return-object p0

    .restart local p0    # "b":Landroid/graphics/Bitmap;
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleToSize(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method private static resolveArtwork(Landroid/content/Context;JIIZ)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "mayScaleUpOrCrop"    # Z

    .prologue
    .line 1199
    const/4 v1, 0x2

    new-array v6, v1, [I

    .local v6, "originalBounds":[I
    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    .line 1200
    invoke-static/range {v1 .. v6}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtworkRaw(Landroid/content/Context;JII[I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1203
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1205
    :try_start_0
    invoke-static {v0, p3, p4, p5}, Lcom/google/android/music/utils/AlbumArtUtils;->resizeHelper(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1242
    :cond_0
    :goto_0
    return-object v0

    .line 1239
    :catch_0
    move-exception v7

    .line 1240
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {v7, p3, p4}, Lcom/google/android/music/utils/AlbumArtUtils;->reportAndRethrow(Ljava/lang/OutOfMemoryError;II)V

    goto :goto_0
.end method

.method private static resolveArtworkFromUrl(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 19
    .param p0, "albumArtUrl"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "mayScaleUp"    # Z

    .prologue
    .line 711
    sget-boolean v15, Lcom/google/android/music/utils/AlbumArtUtils;->LOGV:Z

    if-eqz v15, :cond_0

    .line 712
    const-string v15, "AlbumArtUtils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "resolveArtworkFromUrl: albumArtUrl="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    :cond_0
    if-lez p1, :cond_1

    if-gtz p2, :cond_2

    .line 716
    :cond_1
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v16, "must provide a width and height"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 718
    :cond_2
    if-nez p0, :cond_4

    .line 719
    const/4 v6, 0x0

    .line 771
    :cond_3
    :goto_0
    return-object v6

    .line 722
    :cond_4
    const/4 v10, 0x0

    .line 724
    .local v10, "dataStream":Ljava/io/ByteArrayInputStream;
    const/4 v12, 0x0

    .line 725
    .local v12, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 728
    .local v4, "bis":Ljava/io/BufferedInputStream;
    const/16 v3, 0x2800

    .line 729
    .local v3, "IO_BLOCK_SIZE":I
    :try_start_0
    new-instance v15, Ljava/net/URL;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v9

    .line 730
    .local v9, "conn":Ljava/net/URLConnection;
    invoke-virtual {v9}, Ljava/net/URLConnection;->connect()V

    .line 731
    invoke-virtual {v9}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 732
    new-instance v5, Ljava/io/BufferedInputStream;

    const/16 v15, 0x2800

    invoke-direct {v5, v12, v15}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 733
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .local v5, "bis":Ljava/io/BufferedInputStream;
    :try_start_1
    invoke-virtual {v9}, Ljava/net/URLConnection;->getContentLength()I

    move-result v13

    .line 735
    .local v13, "length":I
    new-instance v14, Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;

    const/4 v15, -0x1

    if-ne v13, v15, :cond_5

    const/16 v13, 0x2800

    .end local v13    # "length":I
    :cond_5
    invoke-direct {v14, v13}, Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;-><init>(I)V

    .line 737
    .local v14, "outputStream":Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;
    const/16 v15, 0x2800

    new-array v7, v15, [B

    .line 740
    .local v7, "buffer":[B
    :goto_1
    invoke-virtual {v12, v7}, Ljava/io/InputStream;->read([B)I

    move-result v8

    .local v8, "bytesRead":I
    const/4 v15, -0x1

    if-eq v8, v15, :cond_6

    .line 741
    const/4 v15, 0x0

    invoke-virtual {v14, v7, v15, v8}, Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 744
    .end local v7    # "buffer":[B
    .end local v8    # "bytesRead":I
    .end local v14    # "outputStream":Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;
    :catch_0
    move-exception v11

    move-object v4, v5

    .line 745
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "conn":Ljava/net/URLConnection;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .local v11, "e":Ljava/io/IOException;
    :goto_2
    :try_start_2
    const-string v15, "AlbumArtUtils"

    const-string v16, "Exception: "

    move-object/from16 v0, v16

    invoke-static {v15, v0, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 746
    const/4 v6, 0x0

    .line 748
    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 749
    invoke-static {v12}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 743
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "buffer":[B
    .restart local v8    # "bytesRead":I
    .restart local v9    # "conn":Ljava/net/URLConnection;
    .restart local v14    # "outputStream":Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;
    :cond_6
    :try_start_3
    invoke-virtual {v14}, Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;->toInputStream()Ljava/io/ByteArrayInputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v10

    .line 748
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 749
    invoke-static {v12}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 753
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput-boolean v0, v15, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 754
    const/4 v15, 0x0

    sget-object v16, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    move-object/from16 v0, v16

    invoke-static {v10, v15, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 755
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    const/16 v16, 0x0

    move/from16 v0, v16

    iput-boolean v0, v15, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 756
    invoke-virtual {v10}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 757
    sget-object v15, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    const/16 v16, 0x1

    sget-object v17, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v17, v0

    div-int v17, v17, p1

    sget-object v18, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v18, v0

    div-int v18, v18, p2

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(II)I

    move-result v16

    move/from16 v0, v16

    iput v0, v15, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 760
    const/4 v15, 0x0

    sget-object v16, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    move-object/from16 v0, v16

    invoke-static {v10, v15, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 761
    .local v6, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v10}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 763
    sget-boolean v15, Lcom/google/android/music/utils/AlbumArtUtils;->LOGV:Z

    if-eqz v15, :cond_7

    .line 764
    const-string v15, "AlbumArtUtils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "resolveArtworkFromUrl: Pulled bitmap: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    :cond_7
    if-eqz v6, :cond_3

    .line 768
    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v6, v0, v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->resizeHelper(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    goto/16 :goto_0

    .line 748
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v6    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "buffer":[B
    .end local v8    # "bytesRead":I
    .end local v9    # "conn":Ljava/net/URLConnection;
    .end local v14    # "outputStream":Lcom/google/android/music/utils/AlbumArtUtils$ExtractableByteArrayOutputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    :catchall_0
    move-exception v15

    :goto_3
    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 749
    invoke-static {v12}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v15

    .line 748
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "conn":Ljava/net/URLConnection;
    :catchall_1
    move-exception v15

    move-object v4, v5

    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_3

    .line 744
    .end local v9    # "conn":Ljava/net/URLConnection;
    :catch_1
    move-exception v11

    goto/16 :goto_2
.end method

.method private static resolveArtworkImp(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZ)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "w"    # I
    .param p5, "h"    # I
    .param p6, "album"    # Ljava/lang/String;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p9, "allowDefault"    # Z
    .param p10, "allowAlias"    # Z

    .prologue
    .line 1005
    const/4 v14, 0x0

    .line 1009
    .local v14, "result":Landroid/graphics/Bitmap;
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gez v4, :cond_2

    .line 1010
    const/4 v4, 0x1

    move-object/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtworkFromUrl(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1015
    :cond_0
    :goto_0
    if-nez v14, :cond_1

    if-eqz p9, :cond_1

    .line 1016
    const/4 v5, 0x1

    move-object v4, p0

    move-wide/from16 v6, p1

    move/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move/from16 v13, p10

    invoke-static/range {v4 .. v13}, Lcom/google/android/music/utils/AlbumArtUtils;->getDefaultArtwork(Landroid/content/Context;ZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1019
    :cond_1
    return-object v14

    .line 1011
    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_0

    .line 1012
    const/4 v10, 0x1

    move-object v5, p0

    move-wide/from16 v6, p1

    move/from16 v8, p4

    move/from16 v9, p5

    invoke-static/range {v5 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->resolveArtwork(Landroid/content/Context;JIIZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto :goto_0
.end method

.method private static resolveArtworkRaw(Landroid/content/Context;JII[I)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "album_id"    # J
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "outOriginalBounds"    # [I

    .prologue
    .line 1318
    if-lez p3, :cond_0

    if-gtz p4, :cond_1

    .line 1319
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must specify target height and width"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1322
    :cond_1
    const/4 v6, 0x0

    .line 1324
    .local v6, "fd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/music/store/MusicContent$AlbumArt;->openFileDescriptor(Landroid/content/Context;JII)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    .line 1325
    if-eqz v6, :cond_3

    .line 1326
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p3

    move v3, p4

    move-object v4, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/AlbumArtUtils;->loadBitmap(Landroid/content/Context;Ljava/io/FileDescriptor;II[IZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1332
    if-eqz v6, :cond_2

    .line 1333
    :try_start_1
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1337
    :cond_2
    :goto_0
    return-object v0

    .line 1332
    :cond_3
    if-eqz v6, :cond_4

    .line 1333
    :try_start_2
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1337
    :cond_4
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1329
    :catch_0
    move-exception v0

    .line 1332
    if-eqz v6, :cond_4

    .line 1333
    :try_start_3
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 1334
    :catch_1
    move-exception v0

    goto :goto_1

    .line 1331
    :catchall_0
    move-exception v0

    .line 1332
    if-eqz v6, :cond_5

    .line 1333
    :try_start_4
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 1335
    :cond_5
    :goto_2
    throw v0

    .line 1334
    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method private static resolveArtworkRaw(Landroid/content/Context;Ljava/lang/String;II[IZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "outOriginalBounds"    # [I
    .param p5, "cropToSquare"    # Z

    .prologue
    .line 1350
    const/4 v6, 0x0

    .line 1352
    .local v6, "fd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/music/store/MusicContent$UrlArt;->openFileDescriptor(Landroid/content/Context;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    .line 1353
    if-eqz v6, :cond_2

    .line 1354
    if-lez p2, :cond_1

    if-lez p3, :cond_1

    .line 1355
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/AlbumArtUtils;->loadBitmap(Landroid/content/Context;Ljava/io/FileDescriptor;II[IZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1364
    if-eqz v6, :cond_0

    .line 1365
    :try_start_1
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1369
    :cond_0
    :goto_0
    return-object v0

    .line 1358
    :cond_1
    :try_start_2
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1364
    if-eqz v6, :cond_0

    .line 1365
    :try_start_3
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 1366
    :catch_0
    move-exception v1

    goto :goto_0

    .line 1364
    :cond_2
    if-eqz v6, :cond_3

    .line 1365
    :try_start_4
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 1369
    :cond_3
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1361
    :catch_1
    move-exception v0

    .line 1364
    if-eqz v6, :cond_3

    .line 1365
    :try_start_5
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 1366
    :catch_2
    move-exception v0

    goto :goto_1

    .line 1363
    :catchall_0
    move-exception v0

    .line 1364
    if-eqz v6, :cond_4

    .line 1365
    :try_start_6
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1367
    :cond_4
    :goto_2
    throw v0

    .line 1366
    :catch_3
    move-exception v1

    goto :goto_0

    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_2
.end method

.method private static scaleStep(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2792
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->getConfigOrDefault(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2793
    .local v0, "tmp":Landroid/graphics/Bitmap;
    invoke-static {p0, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleToFit(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 2794
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapPool:Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;

    invoke-virtual {v1, p0}, Lcom/google/android/music/utils/AlbumArtUtils$MutableBitmapPool;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 2795
    return-object v0
.end method

.method private static scaleToFit(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 7
    .param p0, "source"    # Landroid/graphics/Bitmap;
    .param p1, "dest"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1461
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1462
    .local v0, "c":Landroid/graphics/Canvas;
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 1463
    .local v1, "paint":Landroid/graphics/Paint;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1464
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1466
    return-void
.end method

.method public static scaleToSize(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 2776
    if-lez p1, :cond_3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, p2, :cond_3

    .line 2778
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    mul-int/lit8 v1, p1, 0x2

    if-le v0, v1, :cond_1

    .line 2779
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {p0, v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleStep(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0

    .line 2781
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p1, :cond_2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, p2, :cond_3

    .line 2782
    :cond_2
    invoke-static {p0, p1, p2}, Lcom/google/android/music/utils/AlbumArtUtils;->scaleStep(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 2785
    :cond_3
    return-object p0
.end method

.method public static setPreferredConfig(Landroid/graphics/Bitmap$Config;)V
    .locals 1
    .param p0, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 592
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-object p0, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 593
    sget-object v0, Lcom/google/android/music/utils/AlbumArtUtils;->sExternalBitmapOptionsCache:Landroid/graphics/BitmapFactory$Options;

    iput-object p0, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 594
    return-void
.end method

.method public static shouldUseLargeOverlay(Landroid/graphics/Bitmap;I)Z
    .locals 1
    .param p0, "largeBadge"    # Landroid/graphics/Bitmap;
    .param p1, "height"    # I

    .prologue
    .line 2347
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    if-lt p1, v0, :cond_0

    .line 2348
    const/4 v0, 0x1

    .line 2350
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "imageUrl"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 2972
    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2976
    .local v2, "inputUrl":Landroid/net/Uri;
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 2977
    .local v3, "path":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2991
    .end local v2    # "inputUrl":Landroid/net/Uri;
    .end local v3    # "path":Ljava/lang/String;
    .end local p0    # "imageUrl":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 2973
    .restart local p0    # "imageUrl":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2974
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 2981
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "inputUrl":Landroid/net/Uri;
    .restart local v3    # "path":Ljava/lang/String;
    :cond_1
    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 2982
    .local v1, "index":I
    if-ne v1, v5, :cond_2

    .line 2983
    const-string v4, "%3D"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 2985
    :cond_2
    if-eq v1, v5, :cond_0

    .line 2990
    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2991
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static throwIfRecycled(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1469
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1470
    const-string v0, "AlbumArtUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bitmap "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is recycled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1471
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isRecycled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1473
    :cond_0
    return-void
.end method

.method private static trackCacheUsage(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "sizeKey"    # Ljava/lang/String;
    .param p1, "cacheHit"    # Z

    .prologue
    .line 1178
    sget-object v2, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCache:Ljava/util/HashMap;

    monitor-enter v2

    .line 1179
    :try_start_0
    sget v1, Lcom/google/android/music/utils/AlbumArtUtils;->sTotalSizeCacheRequests:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/music/utils/AlbumArtUtils;->sTotalSizeCacheRequests:I

    .line 1180
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCacheRequests:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;

    .line 1182
    .local v0, "cacheRequest":Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;
    if-nez v0, :cond_0

    .line 1183
    new-instance v0, Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;

    .end local v0    # "cacheRequest":Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;-><init>(Lcom/google/android/music/utils/AlbumArtUtils$1;)V

    .line 1184
    .restart local v0    # "cacheRequest":Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;
    sget-object v1, Lcom/google/android/music/utils/AlbumArtUtils;->sSizeCacheRequests:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1186
    :cond_0
    # operator++ for: Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;->cacheRequests:I
    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;->access$608(Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;)I

    .line 1187
    if-eqz p1, :cond_1

    .line 1188
    # operator++ for: Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;->cacheHits:I
    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;->access$708(Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;)I

    .line 1190
    :cond_1
    monitor-exit v2

    .line 1191
    return-void

    .line 1190
    .end local v0    # "cacheRequest":Lcom/google/android/music/utils/AlbumArtUtils$CacheRequest;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
