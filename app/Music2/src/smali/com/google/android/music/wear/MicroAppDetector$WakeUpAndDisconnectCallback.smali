.class final Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;
.super Ljava/lang/Object;
.source "MicroAppDetector.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/wear/MicroAppDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WakeUpAndDisconnectCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mHowManyNodes:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 0
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput-object p1, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 156
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->maybeDisconnect()V

    return-void
.end method

.method private maybeDisconnect()V
    .locals 2

    .prologue
    .line 188
    iget v0, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mHowManyNodes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mHowManyNodes:I

    .line 189
    iget v0, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mHowManyNodes:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 192
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 148
    check-cast p1, Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->onResult(Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;)V

    return-void
.end method

.method public onResult(Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;)V
    .locals 8
    .param p1, "getConnectedNodesResult"    # Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;

    .prologue
    .line 160
    invoke-interface {p1}, Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v3

    if-nez v3, :cond_1

    .line 161
    const-string v3, "MicroAppDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get nodes: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v3, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v3}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 184
    :cond_0
    return-void

    .line 167
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;->getNodes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iput v3, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mHowManyNodes:I

    .line 169
    invoke-interface {p1}, Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;->getNodes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/wearable/Node;

    .line 171
    .local v2, "node":Lcom/google/android/gms/wearable/Node;
    sget-object v3, Lcom/google/android/gms/wearable/Wearable;->MessageApi:Lcom/google/android/gms/wearable/MessageApi;

    iget-object v4, p0, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v2}, Lcom/google/android/gms/wearable/Node;->getId()Ljava/lang/String;

    move-result-object v5

    const-string v6, "wake_up"

    const/4 v7, 0x0

    new-array v7, v7, [B

    invoke-interface {v3, v4, v5, v6, v7}, Lcom/google/android/gms/wearable/MessageApi;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    .line 176
    .local v1, "messageResultPendingResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;>;"
    new-instance v3, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback$1;-><init>(Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;)V

    const-wide/16 v4, 0x1e

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v3, v4, v5, v6}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;JLjava/util/concurrent/TimeUnit;)V

    goto :goto_0
.end method
