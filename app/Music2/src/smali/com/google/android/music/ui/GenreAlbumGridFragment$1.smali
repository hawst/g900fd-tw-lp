.class Lcom/google/android/music/ui/GenreAlbumGridFragment$1;
.super Ljava/lang/Object;
.source "GenreAlbumGridFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/GenreAlbumGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mSavedContext:Landroid/content/Context;

.field private mSavedGenreName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

.field final synthetic val$genreList:Lcom/google/android/music/medialist/GenreAlbumList;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/GenreAlbumGridFragment;Lcom/google/android/music/medialist/GenreAlbumList;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->val$genreList:Lcom/google/android/music/medialist/GenreAlbumList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->mSavedContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->val$genreList:Lcom/google/android/music/medialist/GenreAlbumList;

    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->mSavedContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/GenreAlbumList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->mSavedGenreName:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public taskCompleted()V
    .locals 5

    .prologue
    .line 68
    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->mSavedGenreName:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreName:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->access$002(Lcom/google/android/music/ui/GenreAlbumGridFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 69
    new-instance v0, Lcom/google/android/music/medialist/GenreSongList;

    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    # getter for: Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreId:J
    invoke-static {v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->access$100(Lcom/google/android/music/ui/GenreAlbumGridFragment;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    # getter for: Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->access$000(Lcom/google/android/music/ui/GenreAlbumGridFragment;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, -0x1

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/google/android/music/medialist/GenreSongList;-><init>(JLjava/lang/String;I)V

    .line 70
    .local v0, "songList":Lcom/google/android/music/medialist/GenreSongList;
    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    # getter for: Lcom/google/android/music/ui/GenreAlbumGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;
    invoke-static {v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->access$200(Lcom/google/android/music/ui/GenreAlbumGridFragment;)Lcom/google/android/music/ui/AllSongsView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/AllSongsView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 71
    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    # getter for: Lcom/google/android/music/ui/GenreAlbumGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;
    invoke-static {v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->access$200(Lcom/google/android/music/ui/GenreAlbumGridFragment;)Lcom/google/android/music/ui/AllSongsView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/AllSongsView;->invalidate()V

    .line 72
    return-void
.end method
