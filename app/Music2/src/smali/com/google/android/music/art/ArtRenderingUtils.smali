.class public Lcom/google/android/music/art/ArtRenderingUtils;
.super Ljava/lang/Object;
.source "ArtRenderingUtils.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/ArtRenderingUtils;->LOGV:Z

    return-void
.end method

.method public static calculateSourceSlice(Landroid/graphics/Rect;IIII)V
    .locals 15
    .param p0, "rect"    # Landroid/graphics/Rect;
    .param p1, "srcWidth"    # I
    .param p2, "srcHeight"    # I
    .param p3, "destWidth"    # I
    .param p4, "destHeight"    # I

    .prologue
    .line 172
    move/from16 v0, p1

    int-to-float v13, v0

    move/from16 v0, p2

    int-to-float v14, v0

    div-float v2, v13, v14

    .line 173
    .local v2, "aspectSrc":F
    move/from16 v0, p3

    int-to-float v13, v0

    move/from16 v0, p4

    int-to-float v14, v0

    div-float v1, v13, v14

    .line 176
    .local v1, "aspectDest":F
    sub-float v13, v2, v1

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    const v14, 0x3c23d70a    # 0.01f

    cmpg-float v13, v13, v14

    if-gez v13, :cond_0

    .line 177
    const/4 v9, 0x0

    .line 178
    .local v9, "srcTopLeftX":I
    const/4 v10, 0x0

    .line 179
    .local v10, "srcTopLeftY":I
    move/from16 v6, p1

    .line 180
    .local v6, "srcBottomRightX":I
    move/from16 v7, p2

    .line 222
    .local v7, "srcBottomRightY":I
    :goto_0
    invoke-virtual {p0, v9, v10, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 223
    return-void

    .line 181
    .end local v6    # "srcBottomRightX":I
    .end local v7    # "srcBottomRightY":I
    .end local v9    # "srcTopLeftX":I
    .end local v10    # "srcTopLeftY":I
    :cond_0
    cmpl-float v13, v2, v1

    if-lez v13, :cond_1

    .line 184
    move/from16 v0, p2

    int-to-float v13, v0

    move/from16 v0, p4

    int-to-float v14, v0

    div-float v3, v13, v14

    .line 188
    .local v3, "scaleFactor":F
    move/from16 v0, p3

    int-to-float v13, v0

    mul-float v5, v13, v3

    .line 189
    .local v5, "scaledDestWidth":F
    move/from16 v0, p1

    int-to-float v13, v0

    const/high16 v14, 0x40000000    # 2.0f

    div-float v12, v13, v14

    .line 191
    .local v12, "srcWidthMidpoint":F
    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v5, v13

    sub-float v13, v12, v13

    float-to-int v9, v13

    .line 192
    .restart local v9    # "srcTopLeftX":I
    const/4 v10, 0x0

    .line 193
    .restart local v10    # "srcTopLeftY":I
    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v5, v13

    add-float/2addr v13, v12

    float-to-int v6, v13

    .line 194
    .restart local v6    # "srcBottomRightX":I
    move/from16 v7, p2

    .line 195
    .restart local v7    # "srcBottomRightY":I
    goto :goto_0

    .line 197
    .end local v3    # "scaleFactor":F
    .end local v5    # "scaledDestWidth":F
    .end local v6    # "srcBottomRightX":I
    .end local v7    # "srcBottomRightY":I
    .end local v9    # "srcTopLeftX":I
    .end local v10    # "srcTopLeftY":I
    .end local v12    # "srcWidthMidpoint":F
    :cond_1
    move/from16 v0, p1

    int-to-float v13, v0

    move/from16 v0, p3

    int-to-float v14, v0

    div-float v3, v13, v14

    .line 205
    .restart local v3    # "scaleFactor":F
    move/from16 v0, p4

    int-to-float v13, v0

    mul-float v4, v13, v3

    .line 206
    .local v4, "scaledDestHeight":F
    move/from16 v0, p2

    int-to-float v13, v0

    const/high16 v14, 0x40400000    # 3.0f

    div-float v8, v13, v14

    .line 210
    .local v8, "srcCenterYPoint":F
    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v4, v13

    sub-float v11, v8, v13

    .line 211
    .local v11, "srcTopYPoint":F
    const/4 v13, 0x0

    cmpg-float v13, v11, v13

    if-gez v13, :cond_2

    .line 212
    sub-float/2addr v8, v11

    .line 213
    const/4 v11, 0x0

    .line 216
    :cond_2
    const/4 v9, 0x0

    .line 217
    .restart local v9    # "srcTopLeftX":I
    float-to-int v10, v11

    .line 218
    .restart local v10    # "srcTopLeftY":I
    move/from16 v6, p1

    .line 219
    .restart local v6    # "srcBottomRightX":I
    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v4, v13

    add-float/2addr v13, v8

    float-to-int v7, v13

    .restart local v7    # "srcBottomRightY":I
    goto :goto_0
.end method

.method public static createRadioOverlay(Landroid/content/Context;IZZ)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edge"    # I
    .param p2, "useRadioImages"    # Z
    .param p3, "circleCrop"    # Z

    .prologue
    .line 113
    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p1, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 114
    .local v8, "result":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 115
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 116
    .local v6, "p":Landroid/graphics/Paint;
    const/4 v11, 0x1

    invoke-virtual {v6, v11}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 118
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 119
    .local v7, "res":Landroid/content/res/Resources;
    const v11, 0x7f020047

    invoke-static {v7, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 120
    .local v10, "texture":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v3, v11, v12, p1, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 121
    .local v3, "destRect":Landroid/graphics/Rect;
    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 124
    if-eqz p2, :cond_2

    .line 125
    const v9, 0x7f02013b

    .line 126
    .local v9, "smallIcon":I
    const v5, 0x7f02004e

    .line 132
    .local v5, "largeIcon":I
    :goto_0
    invoke-static {v7, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 133
    .local v4, "iconOverlay":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-static {p1, v11}, Lcom/google/android/music/art/ArtRenderingUtils;->useSmallOverlay(II)Z

    move-result v11

    if-nez v11, :cond_0

    .line 134
    invoke-static {v7, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 136
    :cond_0
    new-instance v3, Landroid/graphics/Rect;

    .end local v3    # "destRect":Landroid/graphics/Rect;
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 137
    .restart local v3    # "destRect":Landroid/graphics/Rect;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    sub-int v2, p1, v11

    .line 138
    .local v2, "dWidth":I
    div-int/lit8 v11, v2, 0x2

    iput v11, v3, Landroid/graphics/Rect;->left:I

    .line 139
    div-int/lit8 v11, v2, 0x2

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    add-int/2addr v11, v12

    iput v11, v3, Landroid/graphics/Rect;->right:I

    .line 140
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sub-int v1, p1, v11

    .line 141
    .local v1, "dHeight":I
    div-int/lit8 v11, v1, 0x2

    iput v11, v3, Landroid/graphics/Rect;->top:I

    .line 142
    div-int/lit8 v11, v1, 0x2

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    add-int/2addr v11, v12

    iput v11, v3, Landroid/graphics/Rect;->bottom:I

    .line 143
    const/4 v11, 0x0

    invoke-virtual {v0, v4, v11, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 144
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 145
    if-eqz p3, :cond_1

    .line 146
    invoke-static {v8}, Lcom/google/android/music/art/ArtRenderingUtils;->cropToCircle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 148
    :cond_1
    return-object v8

    .line 128
    .end local v1    # "dHeight":I
    .end local v2    # "dWidth":I
    .end local v4    # "iconOverlay":Landroid/graphics/Bitmap;
    .end local v5    # "largeIcon":I
    .end local v9    # "smallIcon":I
    :cond_2
    const v9, 0x7f020107

    .line 129
    .restart local v9    # "smallIcon":I
    const v5, 0x7f02004d

    .restart local v5    # "largeIcon":I
    goto :goto_0
.end method

.method public static cropToCircle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "input"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v10, 0x0

    .line 53
    if-nez p0, :cond_0

    .line 54
    const/4 v4, 0x0

    .line 83
    :goto_0
    return-object v4

    .line 56
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/art/ArtRenderingUtils;->getMinimumDimension(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 57
    .local v6, "size":I
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 58
    .local v7, "srcRect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int v3, v8, v6

    .line 59
    .local v3, "dw":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int v2, v8, v6

    .line 60
    .local v2, "dh":I
    div-int/lit8 v8, v2, 0x2

    iput v8, v7, Landroid/graphics/Rect;->top:I

    .line 61
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    div-int/lit8 v9, v2, 0x2

    sub-int/2addr v8, v9

    iput v8, v7, Landroid/graphics/Rect;->bottom:I

    .line 62
    div-int/lit8 v8, v3, 0x2

    iput v8, v7, Landroid/graphics/Rect;->left:I

    .line 63
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    div-int/lit8 v9, v2, 0x2

    sub-int/2addr v8, v9

    iput v8, v7, Landroid/graphics/Rect;->right:I

    .line 65
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v10, v10, v6, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 69
    .local v1, "destRect":Landroid/graphics/Rect;
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v6, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 70
    .local v4, "output":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 73
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    const/4 v8, 0x1

    invoke-direct {v5, v8}, Landroid/graphics/Paint;-><init>(I)V

    .line 74
    .local v5, "paint":Landroid/graphics/Paint;
    const/high16 v8, -0x1000000

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 77
    div-int/lit8 v8, v6, 0x2

    int-to-float v8, v8

    div-int/lit8 v9, v6, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v6, 0x2

    int-to-float v10, v10

    invoke-virtual {v0, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 80
    new-instance v8, Landroid/graphics/PorterDuffXfermode;

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v8, v9}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 81
    invoke-virtual {v0, p0, v7, v1, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private static getMinimumDimension(Landroid/graphics/Bitmap;)I
    .locals 2
    .param p0, "in"    # Landroid/graphics/Bitmap;

    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 88
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 90
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public static sliceBitmapSectionAndScale(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "src"    # Landroid/graphics/Bitmap;
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/music/art/ArtRenderingUtils;->sliceBitmapSectionAndScale(Landroid/graphics/Bitmap;IILandroid/graphics/Paint;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static sliceBitmapSectionAndScale(Landroid/graphics/Bitmap;IILandroid/graphics/Paint;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "src"    # Landroid/graphics/Bitmap;
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v7, 0x0

    .line 41
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 42
    .local v2, "result":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 43
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v7, v7, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 44
    .local v1, "destRect":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 45
    .local v4, "srcRect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 46
    .local v5, "srcWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 47
    .local v3, "srcHeight":I
    invoke-static {v4, v5, v3, p1, p2}, Lcom/google/android/music/art/ArtRenderingUtils;->calculateSourceSlice(Landroid/graphics/Rect;IIII)V

    .line 48
    invoke-virtual {v0, p0, v4, v1, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 49
    return-object v2
.end method

.method private static useSmallOverlay(II)Z
    .locals 1
    .param p0, "edge1"    # I
    .param p1, "edge2"    # I

    .prologue
    .line 152
    mul-int/lit8 v0, p1, 0x3

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
