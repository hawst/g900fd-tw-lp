.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QueueEventInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;,
        Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;


# instance fields
.field public multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

.field public queueEventType:I

.field public singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

.field public stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

.field public tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1627
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1628
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1933
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    .line 1936
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    .line 1939
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    .line 1942
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    .line 1945
    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    .line 1628
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1969
    const/4 v0, 0x0

    .line 1970
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1971
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1974
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    if-eqz v1, :cond_1

    .line 1975
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1978
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    if-eqz v1, :cond_2

    .line 1979
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1982
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    if-eqz v1, :cond_3

    .line 1983
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1986
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    if-eqz v1, :cond_4

    .line 1987
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1990
    :cond_4
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->cachedSize:I

    .line 1991
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1999
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2000
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2004
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2005
    :sswitch_0
    return-object p0

    .line 2010
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2011
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 2016
    :cond_1
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    goto :goto_0

    .line 2018
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    goto :goto_0

    .line 2023
    .end local v1    # "temp":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    if-nez v2, :cond_3

    .line 2024
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    .line 2026
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2030
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    if-nez v2, :cond_4

    .line 2031
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    .line 2033
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2037
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    if-nez v2, :cond_5

    .line 2038
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    .line 2040
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2044
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    if-nez v2, :cond_6

    .line 2045
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    .line 2047
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2000
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1624
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1950
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1951
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1953
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    if-eqz v0, :cond_1

    .line 1954
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1956
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    if-eqz v0, :cond_2

    .line 1957
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->multipleTracksRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1959
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    if-eqz v0, :cond_3

    .line 1960
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->tracksAddedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$TracksAddedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1962
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    if-eqz v0, :cond_4

    .line 1963
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1965
    :cond_4
    return-void
.end method
