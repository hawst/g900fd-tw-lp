.class Lcom/google/android/music/ui/BaseActivity$4;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$4;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountStatusUpdate(Landroid/accounts/Account;Lcom/google/android/music/NautilusStatus;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$4;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/BaseActivity;->onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$4;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->updateMusicDrawer()V

    .line 275
    return-void
.end method
