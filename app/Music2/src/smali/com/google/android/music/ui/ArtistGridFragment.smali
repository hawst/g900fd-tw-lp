.class public Lcom/google/android/music/ui/ArtistGridFragment;
.super Lcom/google/android/music/ui/MediaListGridFragment;
.source "ArtistGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ArtistGridFragment$1;,
        Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mPlayTutorialHeader:Landroid/view/ViewGroup;

.field private mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/ArtistGridFragment;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;-><init>()V

    .line 115
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/ArtistGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistGridFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method private updateCardHeader()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/music/ui/TutorialCardsFactory;->setupMyLibraryTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ArtistGridFragment;->setIsCardHeaderShowing(Z)V

    .line 107
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getSupportsFastScroll()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initEmptyScreen()V

    .line 178
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const v0, 0x7f0b02a4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ArtistGridFragment;->setEmptyScreenText(I)V

    .line 180
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ArtistGridFragment;->setEmptyScreenLearnMoreVisible(Z)V

    .line 185
    :goto_0
    return-void

    .line 182
    :cond_0
    const v0, 0x7f0b02a5

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ArtistGridFragment;->setEmptyScreenText(I)V

    .line 183
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ArtistGridFragment;->setEmptyScreenLearnMoreVisible(Z)V

    goto :goto_0
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;-><init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/ArtistGridFragment$1;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/ArtistGridFragment$ArtistsAdapter;-><init>(Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment;Lcom/google/android/music/ui/ArtistGridFragment$1;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistGridFragment;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 52
    new-instance v0, Lcom/google/android/music/medialist/AllArtistsList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllArtistsList;-><init>()V

    sget-object v1, Lcom/google/android/music/ui/ArtistGridFragment;->PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/music/ui/ArtistGridFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 57
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 58
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->onDestroyView()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    .line 113
    return-void
.end method

.method protected onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 1
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 97
    invoke-direct {p0}, Lcom/google/android/music/ui/ArtistGridFragment;->updateCardHeader()V

    .line 99
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 77
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 79
    .local v0, "listView":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistGridFragment;->getSupportsFastScroll()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 81
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    .line 82
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistGridFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 83
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 84
    invoke-direct {p0}, Lcom/google/android/music/ui/ArtistGridFragment;->updateCardHeader()V

    .line 86
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 87
    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 88
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    .line 90
    :cond_0
    return-void
.end method
