.class Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackPlaylistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createPlaylistObjectAdapter(J)Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$playlistId:J


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;JLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    iput-wide p2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$playlistId:J

    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 27
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 58
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 59
    .local v7, "name":Ljava/lang/String;
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 60
    .local v25, "ownerName":Ljava/lang/String;
    const/16 v24, 0x0

    .line 61
    .local v24, "ownerLabel":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 62
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    const v4, 0x7f0b0102

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v25, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    .line 65
    :cond_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    const v4, 0x7f0b00c6

    invoke-virtual {v3, v4}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 69
    :cond_1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$playlistId:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$context:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v8

    invoke-static {v4, v5, v3, v8}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v23

    .line 73
    .local v23, "artUri":Landroid/net/Uri;
    new-instance v3, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v3, v7}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v2

    .line 75
    .local v2, "albumItem":Lcom/google/android/music/leanback/Item;
    new-instance v26, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;-><init>(Ljava/lang/Object;)V

    .line 76
    .local v26, "playlistDetails":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/music/xdi/XdiUtils;->getSongListForPlaylistCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/SongList;

    move-result-object v4

    # setter for: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v3, v4}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$002(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/medialist/SongList;

    .line 80
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$000(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;)Lcom/google/android/music/medialist/SongList;

    move-result-object v3

    if-nez v3, :cond_2

    .line 102
    :goto_0
    return-object v26

    .line 84
    :cond_2
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 86
    .local v6, "type":I
    if-gez v6, :cond_3

    .line 87
    # getter for: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not get playlist type for playlist: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$playlistId:J

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_3
    const/4 v12, 0x0

    .line 93
    .local v12, "actionId":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$playlistId:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v8}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$000(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;)Lcom/google/android/music/medialist/SongList;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    const v10, 0x7f0b00c8

    invoke-virtual {v9, v10}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    const v11, 0x7f0b02dc

    invoke-virtual {v10, v11}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    add-int/lit8 v22, v12, 0x1

    .end local v12    # "actionId":I
    .local v22, "actionId":I
    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createPlayAction(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    invoke-static/range {v3 .. v12}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$200(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 97
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->val$playlistId:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$000(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;)Lcom/google/android/music/medialist/SongList;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    const v4, 0x7f0b0056

    invoke-virtual {v3, v4}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    const v4, 0x7f0b02dc

    invoke-virtual {v3, v4}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    add-int/lit8 v12, v22, 0x1

    .end local v22    # "actionId":I
    .restart local v12    # "actionId":I
    move/from16 v16, v6

    move-object/from16 v17, v7

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createPlayAction(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    invoke-static/range {v13 .. v22}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$200(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    goto/16 :goto_0
.end method
