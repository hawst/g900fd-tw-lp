.class public Lcom/google/android/music/ui/GenreAlbumGridActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "GenreAlbumGridActivity.java"


# instance fields
.field private mGenreAlbumList:Lcom/google/android/music/medialist/GenreAlbumList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/GenreAlbumGridActivity;)Lcom/google/android/music/medialist/GenreAlbumList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreAlbumGridActivity;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity;->mGenreAlbumList:Lcom/google/android/music/medialist/GenreAlbumList;

    return-object v0
.end method

.method public static final buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/AlbumList;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumList"    # Lcom/google/android/music/medialist/AlbumList;

    .prologue
    .line 57
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/GenreAlbumGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "genrealbumlist"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 59
    return-object v0
.end method

.method public static final showAlbumsOfGenre(Landroid/content/Context;JZ)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # J
    .param p3, "shouldFilter"    # Z

    .prologue
    .line 52
    new-instance v1, Lcom/google/android/music/medialist/GenreAlbumList;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, v2}, Lcom/google/android/music/medialist/GenreAlbumList;-><init>(JZ)V

    invoke-static {p0, v1}, Lcom/google/android/music/ui/GenreAlbumGridActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/AlbumList;)Landroid/content/Intent;

    move-result-object v0

    .line 53
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "genrealbumlist"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/medialist/GenreAlbumList;

    iput-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity;->mGenreAlbumList:Lcom/google/android/music/medialist/GenreAlbumList;

    .line 30
    new-instance v1, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;-><init>(Lcom/google/android/music/ui/GenreAlbumGridActivity;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity;->mGenreAlbumList:Lcom/google/android/music/medialist/GenreAlbumList;

    invoke-static {v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->newInstance(Lcom/google/android/music/medialist/GenreAlbumList;)Lcom/google/android/music/ui/GenreAlbumGridFragment;

    move-result-object v0

    .line 46
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/GenreAlbumGridActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 48
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    return-void
.end method
