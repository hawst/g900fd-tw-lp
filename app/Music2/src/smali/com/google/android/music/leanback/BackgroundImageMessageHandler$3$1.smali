.class Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;
.super Ljava/lang/Object;
.source "BackgroundImageMessageHandler.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->onBitmapLoaded(Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field final synthetic this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iput-object p2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 5

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iget-object v0, v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$500(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iget-object v1, v1, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetWidth:I
    invoke-static {v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$600(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iget-object v2, v2, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mTargetHeight:I
    invoke-static {v2}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$700(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->val$bitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->resizeBitmap(Landroid/content/Context;IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->mBitmap:Landroid/graphics/Bitmap;

    .line 230
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iget-object v0, v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getDarkenBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iget-object v0, v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$500(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->darkenBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->mBitmap:Landroid/graphics/Bitmap;

    .line 233
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iget-object v0, v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    iget-object v1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->this$1:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;

    iget-wide v2, v1, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->val$startTime:J

    iget-object v1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postDisplayBitmapMessage(JLandroid/graphics/Bitmap;)V

    .line 238
    return-void
.end method
