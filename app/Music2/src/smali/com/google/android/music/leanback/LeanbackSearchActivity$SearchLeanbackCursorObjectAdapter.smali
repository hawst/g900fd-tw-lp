.class abstract Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "SearchLeanbackCursorObjectAdapter"
.end annotation


# instance fields
.field private final mRowIndex:I

.field private final mTitleResourceId:I

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;II)V
    .locals 1
    .param p2, "rowIndex"    # I
    .param p3, "titleResourceId"    # I

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    .line 152
    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mItemPresenter:Lcom/google/android/music/leanback/ItemPresenter;
    invoke-static {p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$000(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 153
    iput p2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->mRowIndex:I

    .line 154
    iput p3, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->mTitleResourceId:I

    .line 155
    return-void
.end method


# virtual methods
.method protected onCursorChanged()V
    .locals 4

    .prologue
    .line 159
    invoke-super {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->onCursorChanged()V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mListRows:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$100(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->mRowIndex:I

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget v3, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->mTitleResourceId:I

    invoke-virtual {v2, v3, p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->createListRow(ILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$200(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingAdapters:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$200(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mListRows:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$100(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->getBackgroundImageMessageHandler()Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessageImmediate(Ljava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackSearchActivity;->showNoResults()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$300(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V

    goto :goto_0

    .line 171
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SearchLeanbackCursorObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackSearchActivity;->hideNoResults()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$400(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V

    goto :goto_0
.end method
