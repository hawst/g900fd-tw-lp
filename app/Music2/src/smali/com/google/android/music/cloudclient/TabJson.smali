.class public Lcom/google/android/music/cloudclient/TabJson;
.super Lcom/google/api/client/json/GenericJson;
.source "TabJson.java"


# instance fields
.field public mDataStatus:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "data_status"
    .end annotation
.end field

.field public mGroups:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groups"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;",
            ">;"
        }
    .end annotation
.end field

.field public mTabType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "tab_type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
