.class public Lcom/google/android/music/ui/RemoveFromLibraryButton;
.super Lcom/google/android/music/ui/BaseActionButton;
.source "RemoveFromLibraryButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    const v0, 0x7f0b0232

    const v1, 0x7f0200c7

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/ui/BaseActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 25
    return-void
.end method

.method public static removeFromMyLibrary(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "container"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 28
    if-nez p1, :cond_0

    .line 29
    const-string v0, "ActionButton"

    const-string v1, "MediaList is null"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    .end local p1    # "container":Lcom/google/android/music/medialist/MediaList;
    :goto_0
    return-void

    .line 32
    .restart local p1    # "container":Lcom/google/android/music/medialist/MediaList;
    :cond_0
    instance-of v0, p1, Lcom/google/android/music/medialist/AlbumSongList;

    if-nez v0, :cond_1

    .line 33
    const-string v0, "ActionButton"

    const-string v1, "Can\'t remove from a non-album song list"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 36
    :cond_1
    check-cast p1, Lcom/google/android/music/medialist/AlbumSongList;

    .end local p1    # "container":Lcom/google/android/music/medialist/MediaList;
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/AlbumSongList;->removeFromMyLibrary(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method protected handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 41
    invoke-static {p1, p2}, Lcom/google/android/music/ui/RemoveFromLibraryButton;->removeFromMyLibrary(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V

    .line 42
    return-void
.end method
