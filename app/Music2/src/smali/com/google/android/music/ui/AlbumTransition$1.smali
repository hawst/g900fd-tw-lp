.class Lcom/google/android/music/ui/AlbumTransition$1;
.super Landroid/support/v4/app/SharedElementCallback;
.source "AlbumTransition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AlbumTransition;->setup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/AlbumTransition;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/AlbumTransition;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    invoke-direct {p0}, Landroid/support/v4/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedElementEnd(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 224
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_2

    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/google/android/music/ui/AlbumTransition;->access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 225
    invoke-interface {p2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 226
    .local v6, "sharedElement":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/google/android/music/ui/AlbumTransition;->access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v9

    invoke-virtual {v7, v10, v10, v8, v9}, Landroid/widget/ImageView;->layout(IIII)V

    .line 233
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    iget-object v8, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I
    invoke-static {v8}, Lcom/google/android/music/ui/AlbumTransition;->access$1000(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v8

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static {v7, v8}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 234
    .local v3, "parent":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v7

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    invoke-virtual {v3, v10, v10, v7, v8}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 236
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    iget-object v8, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mActionBarId:I
    invoke-static {v8}, Lcom/google/android/music/ui/AlbumTransition;->access$1100(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v8

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static {v7, v8}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v0

    .line 237
    .local v0, "actionBar":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 238
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V
    invoke-static {v7, v0, v11}, Lcom/google/android/music/ui/AlbumTransition;->access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V

    .line 240
    :cond_0
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mBackgroundContainerId:I
    invoke-static {v7}, Lcom/google/android/music/ui/AlbumTransition;->access$500(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 243
    .local v1, "backgroundContainer":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 245
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/view/View;->setTranslationY(F)V

    .line 248
    :cond_1
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    iget-object v8, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mShadowId:I
    invoke-static {v8}, Lcom/google/android/music/ui/AlbumTransition;->access$1200(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v8

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static {v7, v8}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 249
    .local v4, "shadow":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V
    invoke-static {v7, v4, v11}, Lcom/google/android/music/ui/AlbumTransition;->access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V

    .line 250
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    iget-object v8, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I
    invoke-static {v8}, Lcom/google/android/music/ui/AlbumTransition;->access$1300(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v8

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    # setter for: Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I
    invoke-static {v7, v8}, Lcom/google/android/music/ui/AlbumTransition;->access$1302(Lcom/google/android/music/ui/AlbumTransition;I)I

    .line 251
    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    .line 252
    .local v2, "height":I
    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/GradientDrawable;

    .line 261
    .local v5, "shadowDrawable":Landroid/graphics/drawable/GradientDrawable;
    iget-object v7, p0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I
    invoke-static {v7}, Lcom/google/android/music/ui/AlbumTransition;->access$1300(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v7

    invoke-virtual {v5, v10, v10, v7, v2}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 263
    .end local v0    # "actionBar":Landroid/view/View;
    .end local v1    # "backgroundContainer":Landroid/view/View;
    .end local v2    # "height":I
    .end local v3    # "parent":Landroid/view/ViewGroup;
    .end local v4    # "shadow":Landroid/widget/ImageView;
    .end local v5    # "shadowDrawable":Landroid/graphics/drawable/GradientDrawable;
    .end local v6    # "sharedElement":Landroid/view/View;
    :cond_2
    return-void
.end method

.method public onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v22

    if-lez v22, :cond_0

    .line 104
    const/16 v22, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 109
    .local v18, "sharedElement":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mStartingSharedElementColor:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$000(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mImageId:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$100(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 113
    .local v12, "imageView":Landroid/widget/ImageView;
    if-nez v12, :cond_1

    .line 217
    .end local v12    # "imageView":Landroid/widget/ImageView;
    .end local v18    # "sharedElement":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 120
    .restart local v12    # "imageView":Landroid/widget/ImageView;
    .restart local v18    # "sharedElement":Landroid/view/View;
    :cond_1
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v22

    if-lez v22, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;

    move-result-object v22

    if-nez v22, :cond_3

    .line 121
    const/16 v22, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageView;

    .line 123
    .local v19, "snapshot":Landroid/widget/ImageView;
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 124
    .local v6, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v6, :cond_0

    .line 128
    check-cast v6, Landroid/graphics/drawable/BitmapDrawable;

    .end local v6    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    .line 130
    .local v8, "fullBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v8, v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v23

    # setter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$302(Lcom/google/android/music/ui/AlbumTransition;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 134
    if-eqz v8, :cond_2

    .line 135
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 137
    :cond_2
    const/4 v8, 0x0

    .line 138
    const/4 v6, 0x0

    .line 140
    .restart local v6    # "drawable":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    new-instance v23, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;
    invoke-static/range {v24 .. v24}, Lcom/google/android/music/ui/AlbumTransition;->access$400(Lcom/google/android/music/ui/AlbumTransition;)Lcom/google/android/music/ui/BaseActivity;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$202(Lcom/google/android/music/ui/AlbumTransition;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$300(Lcom/google/android/music/ui/AlbumTransition;)Landroid/graphics/Bitmap;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mBackgroundContainerId:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$500(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 146
    .local v5, "backgroundContainer":Landroid/view/View;
    if-eqz v5, :cond_3

    instance-of v0, v5, Landroid/widget/FrameLayout;

    move/from16 v22, v0

    if-eqz v22, :cond_3

    .line 149
    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v24

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getHeight()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 153
    check-cast v5, Landroid/widget/FrameLayout;

    .end local v5    # "backgroundContainer":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 157
    .end local v6    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v8    # "fullBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "snapshot":Landroid/widget/ImageView;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AlbumTransition;->accountForSharedElementPadding(Landroid/view/View;)V

    .line 159
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 160
    .local v11, "imageHeight":I
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v22

    const/high16 v23, 0x40000000    # 2.0f

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    .line 162
    .local v21, "widthSpec":I
    const/high16 v22, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-static {v11, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 163
    .local v10, "heightSpec":I
    move/from16 v0, v21

    invoke-virtual {v12, v0, v10}, Landroid/widget/ImageView;->measure(II)V

    .line 164
    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v24

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v12, v0, v1, v2, v11}, Landroid/widget/ImageView;->layout(IIII)V

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;

    move-result-object v22

    if-eqz v22, :cond_4

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v25

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3, v11}, Landroid/widget/ImageView;->layout(IIII)V

    .line 169
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mFabId:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$600(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 171
    .local v7, "fab":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V
    invoke-static {v0, v7, v1}, Lcom/google/android/music/ui/AlbumTransition;->access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V

    .line 172
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Landroid/support/v4/view/ViewCompat;->setScaleX(Landroid/view/View;F)V

    .line 173
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Landroid/support/v4/view/ViewCompat;->setScaleY(Landroid/view/View;F)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSpacerId:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$800(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v20

    .line 179
    .local v20, "spacer":Landroid/view/View;
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getHeight()I

    move-result v22

    sub-int v14, v11, v22

    .line 180
    .local v14, "offset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$1000(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    .line 181
    .local v15, "parent":Landroid/view/ViewGroup;
    const/16 v22, 0x0

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getWidth()I

    move-result v23

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getHeight()I

    move-result v24

    add-int v24, v24, v14

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v15, v0, v14, v1, v2}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 182
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I
    invoke-static/range {v24 .. v24}, Lcom/google/android/music/ui/AlbumTransition;->access$1000(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v24

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static/range {v23 .. v24}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v23

    const/16 v24, 0x0

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V
    invoke-static/range {v22 .. v24}, Lcom/google/android/music/ui/AlbumTransition;->access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$1000(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/music/ui/BaseTrackListView;

    .line 195
    .local v13, "list":Lcom/google/android/music/ui/BaseTrackListView;
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Lcom/google/android/music/ui/BaseTrackListView;->setAbsoluteY(I)V

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mActionBarId:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$1100(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v4

    .line 198
    .local v4, "actionBar":Landroid/view/View;
    if-eqz v4, :cond_5

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V
    invoke-static {v0, v4, v1}, Lcom/google/android/music/ui/AlbumTransition;->access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V

    .line 201
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mBackgroundContainerId:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/music/ui/AlbumTransition;->access$500(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 204
    .restart local v5    # "backgroundContainer":Landroid/view/View;
    if-eqz v5, :cond_6

    .line 206
    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v24

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getHeight()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 207
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 210
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mShadowId:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$1200(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 211
    .local v16, "shadow":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    move/from16 v2, v23

    # invokes: Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/ui/AlbumTransition;->access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$1300(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v23

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v24

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(II)I

    move-result v23

    # setter for: Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I
    invoke-static/range {v22 .. v23}, Lcom/google/android/music/ui/AlbumTransition;->access$1302(Lcom/google/android/music/ui/AlbumTransition;I)I

    .line 213
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ImageView;->getHeight()I

    move-result v9

    .line 214
    .local v9, "height":I
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v17

    check-cast v17, Landroid/graphics/drawable/GradientDrawable;

    .line 215
    .local v17, "shadowDrawable":Landroid/graphics/drawable/GradientDrawable;
    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition$1;->this$0:Lcom/google/android/music/ui/AlbumTransition;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I
    invoke-static/range {v24 .. v24}, Lcom/google/android/music/ui/AlbumTransition;->access$1300(Lcom/google/android/music/ui/AlbumTransition;)I

    move-result v24

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v9}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    goto/16 :goto_0
.end method
