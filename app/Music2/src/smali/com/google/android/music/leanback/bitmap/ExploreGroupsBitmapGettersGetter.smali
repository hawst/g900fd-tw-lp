.class abstract Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;
.source "ExploreGroupsBitmapGettersGetter.java"


# instance fields
.field private final mBaseLoaderId:I

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J
    .param p6, "handler"    # Landroid/os/Handler;

    .prologue
    .line 23
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 24
    add-int/lit8 v0, p3, 0x1

    iput v0, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->mBaseLoaderId:I

    .line 25
    iput-object p6, p0, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->mHandler:Landroid/os/Handler;

    .line 26
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->startLoading()V

    .line 27
    return-void
.end method


# virtual methods
.method protected addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 16
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 39
    .local v12, "groupId":J
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 41
    .local v9, "groupType":I
    new-instance v2, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->mBaseLoaderId:I

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->mSeed:J

    const-wide/16 v14, 0x1

    add-long/2addr v6, v14

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    int-to-long v14, v8

    add-long/2addr v6, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->getGroupItemsUri(J)Landroid/net/Uri;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/net/Uri;I)V

    .line 45
    .local v2, "exploreGroupItemsBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter$1;-><init>(Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 51
    invoke-virtual {v2}, Lcom/google/android/music/leanback/bitmap/ExploreGroupItemsBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v10

    .line 52
    .local v10, "bitmapGetters":[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    array-length v3, v10

    if-ge v11, v3, :cond_1

    .line 53
    aget-object v3, v10, v11

    if-eqz v3, :cond_0

    .line 54
    aget-object v3, v10, v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 57
    :cond_1
    return-void
.end method

.method protected abstract getGroupItemsUri(J)Landroid/net/Uri;
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/ui/ExploreClusterListFragment;->GROUPS_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method
