.class final Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;
.super Ljava/lang/Object;
.source "PlaybackLogsConsumer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/wear/PlaybackLogsConsumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ConsumePlaybackLogsRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/wear/PlaybackLogsConsumer;


# direct methods
.method private constructor <init>(Lcom/google/android/music/wear/PlaybackLogsConsumer;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;->this$0:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/wear/PlaybackLogsConsumer;Lcom/google/android/music/wear/PlaybackLogsConsumer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer;
    .param p2, "x1"    # Lcom/google/android/music/wear/PlaybackLogsConsumer$1;

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;-><init>(Lcom/google/android/music/wear/PlaybackLogsConsumer;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 138
    const-string v5, "PlaybackLogsConsumer"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 139
    const-string v5, "PlaybackLogsConsumer"

    const-string v6, "Reporting play log events."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;->this$0:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;
    invoke-static {v5}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->access$600(Lcom/google/android/music/wear/PlaybackLogsConsumer;)Lcom/google/android/music/wear/util/DataApiWrapper;

    move-result-object v5

    const-wide/16 v6, 0xa

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v6, v7, v8}, Lcom/google/android/music/wear/util/DataApiWrapper;->getAllDataItems(JLjava/util/concurrent/TimeUnit;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/music/wear/util/GmsApiException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 160
    .local v0, "allDataItems":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    iget-object v5, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;->this$0:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;
    invoke-static {v5}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->access$700(Lcom/google/android/music/wear/PlaybackLogsConsumer;)Lcom/google/android/music/wear/WearMusicDataProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/wear/WearMusicDataProvider;->getSyncedMediaLists()Ljava/util/List;

    move-result-object v5

    # invokes: Lcom/google/android/music/wear/PlaybackLogsConsumer;->buildCollectionIdToMediaListMap(Ljava/util/List;)Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->access$800(Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    .line 162
    .local v1, "collectionIdToMediaListMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/music/wear/WearMediaList;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 163
    .local v2, "dataItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    # invokes: Lcom/google/android/music/wear/PlaybackLogsConsumer;->isPlaybackLogEvent(Landroid/net/Uri;)Z
    invoke-static {v5}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->access$900(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 165
    iget-object v6, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;->this$0:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/wearable/DataMap;

    # invokes: Lcom/google/android/music/wear/PlaybackLogsConsumer;->reportEvent(Ljava/util/Map;Lcom/google/android/gms/wearable/DataMap;)V
    invoke-static {v6, v1, v5}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->access$1000(Lcom/google/android/music/wear/PlaybackLogsConsumer;Ljava/util/Map;Lcom/google/android/gms/wearable/DataMap;)V

    .line 168
    iget-object v5, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$ConsumePlaybackLogsRunnable;->this$0:Lcom/google/android/music/wear/PlaybackLogsConsumer;

    # getter for: Lcom/google/android/music/wear/PlaybackLogsConsumer;->mDataApiWrapper:Lcom/google/android/music/wear/util/DataApiWrapper;
    invoke-static {v5}, Lcom/google/android/music/wear/PlaybackLogsConsumer;->access$600(Lcom/google/android/music/wear/PlaybackLogsConsumer;)Lcom/google/android/music/wear/util/DataApiWrapper;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    invoke-interface {v6, v5}, Lcom/google/android/music/wear/util/DataApiWrapper;->deleteDataItems(Landroid/net/Uri;)V

    goto :goto_0

    .line 146
    .end local v0    # "allDataItems":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    .end local v1    # "collectionIdToMediaListMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/music/wear/WearMediaList;>;"
    .end local v2    # "dataItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/net/Uri;Lcom/google/android/gms/wearable/DataMap;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 147
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v5, "PlaybackLogsConsumer"

    const-string v6, "Interrupted while getting all data items"

    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 148
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 171
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :goto_1
    return-void

    .line 150
    :catch_1
    move-exception v3

    .line 152
    .local v3, "e":Ljava/util/concurrent/TimeoutException;
    const-string v5, "PlaybackLogsConsumer"

    const-string v6, "Timeout while getting all data items"

    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 154
    .end local v3    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_2
    move-exception v3

    .line 156
    .local v3, "e":Lcom/google/android/music/wear/util/GmsApiException;
    const-string v5, "PlaybackLogsConsumer"

    const-string v6, "Exception while getting all data items"

    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
