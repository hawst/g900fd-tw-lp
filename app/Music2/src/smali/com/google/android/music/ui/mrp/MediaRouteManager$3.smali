.class Lcom/google/android/music/ui/mrp/MediaRouteManager$3;
.super Ljava/lang/Object;
.source "MediaRouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/mrp/MediaRouteManager;->restoreMediaRoute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 396
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$300(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 397
    :cond_0
    const-string v1, "MediaRouteManager"

    const-string v2, "Timeout reached when restoring selected route"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$600(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 399
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$400(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 400
    .local v0, "selectedRoute":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-eqz v0, :cond_2

    .line 401
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->setMediaRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    invoke-static {v1, v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$900(Lcom/google/android/music/ui/mrp/MediaRouteManager;Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 406
    .end local v0    # "selectedRoute":Landroid/support/v7/media/MediaRouter$RouteInfo;
    :cond_1
    :goto_0
    return-void

    .line 403
    .restart local v0    # "selectedRoute":Landroid/support/v7/media/MediaRouter$RouteInfo;
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$400(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$3;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$400(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter;->getDefaultRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouter;->selectRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    goto :goto_0
.end method
