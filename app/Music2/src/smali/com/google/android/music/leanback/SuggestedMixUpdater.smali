.class Lcom/google/android/music/leanback/SuggestedMixUpdater;
.super Ljava/lang/Object;
.source "SuggestedMixUpdater.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

.field private final mContext:Landroid/content/Context;

.field private final mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

.field private final mStore:Lcom/google/android/music/store/Store;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget-boolean v0, Lcom/google/android/music/leanback/LeanbackLog;->LOGV:Z

    sput-boolean v0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->LOGV:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mContext:Landroid/content/Context;

    .line 45
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    .line 46
    invoke-static {p1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mStore:Lcom/google/android/music/store/Store;

    .line 47
    iput-object p2, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 48
    return-void
.end method

.method static cleanupIfDisabled(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "disabled":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 78
    .local v4, "resolver":Landroid/content/ContentResolver;
    invoke-static {v4}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->getMaxNumberOfSeeds(Landroid/content/ContentResolver;)I

    move-result v2

    .line 79
    .local v2, "maxSeeds":I
    invoke-static {v4}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->getMaxMixSize(Landroid/content/ContentResolver;)I

    move-result v1

    .line 80
    .local v1, "maxMixSize":I
    if-lez v2, :cond_0

    if-gtz v1, :cond_4

    .line 81
    :cond_0
    sget-boolean v6, Lcom/google/android/music/leanback/SuggestedMixUpdater;->LOGV:Z

    if-eqz v6, :cond_1

    .line 82
    const-string v6, "MusicLeanback"

    const-string v7, "Suggested mixes turned off via GSerives. Seeds: %d, Mix size: %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_1
    const/4 v0, 0x1

    .line 104
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    .line 106
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    .line 107
    .local v5, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->removeAllSuggestedMixes()V

    .line 110
    .end local v5    # "store":Lcom/google/android/music/store/Store;
    :cond_3
    return v0

    .line 89
    :cond_4
    const-string v6, "music_suggested_mixes_min_locker_tracks"

    const/16 v7, 0xc8

    invoke-static {v4, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 92
    .local v3, "minNumberOfSongsRequired":I
    if-lez v3, :cond_2

    .line 93
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/android/music/store/Store;->hasRemoteSongs(I)Z

    move-result v6

    if-nez v6, :cond_2

    .line 94
    sget-boolean v6, Lcom/google/android/music/leanback/SuggestedMixUpdater;->LOGV:Z

    if-eqz v6, :cond_5

    .line 95
    const-string v6, "MusicLeanback"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Suggested mixes turned off. Locker has less than "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static getMaxMixSize(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 57
    const-string v0, "music_suggested_mix_size"

    const/16 v1, 0x19

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static getMaxNumberOfSeeds(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 51
    const-string v0, "music_max_suggested_mixes"

    const/4 v1, 0x4

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private storeMixes(ILcom/google/android/music/cloudclient/RadioFeedResponse;)Z
    .locals 13
    .param p1, "sourceAccount"    # I
    .param p2, "radioFeedJson"    # Lcom/google/android/music/cloudclient/RadioFeedResponse;

    .prologue
    .line 179
    iget-object v10, p2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v5, v10, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    .line 180
    .local v5, "radioStations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableRadioStation;>;"
    iget-object v10, p2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v10, v10, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    .line 182
    .local v1, "howMany":I
    new-array v7, v1, [Ljava/lang/String;

    .line 183
    .local v7, "seeds":[Ljava/lang/String;
    new-array v8, v1, [Ljava/lang/String;

    .line 184
    .local v8, "titles":[Ljava/lang/String;
    new-array v0, v1, [Ljava/util/List;

    .line 185
    .local v0, "contents":[Ljava/util/List;, "[Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    const/4 v2, 0x0

    .line 186
    .local v2, "i":I
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    .line 187
    .local v4, "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    iget-object v9, v4, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    .line 188
    .local v9, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 190
    const-string v10, "MusicLeanback"

    const-string v11, "Empty suggested mix"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const/4 v10, 0x0

    .line 207
    .end local v4    # "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .end local v9    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :goto_1
    return v10

    .line 194
    .restart local v4    # "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .restart local v9    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_0
    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/TrackJson;

    .line 195
    .local v6, "seedTrack":Lcom/google/android/music/cloudclient/TrackJson;
    invoke-virtual {v6}, Lcom/google/android/music/cloudclient/TrackJson;->getEffectiveRemoteId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v2

    .line 196
    aget-object v10, v7, v2

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 197
    const-string v10, "MusicLeanback"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Empty id for seed track: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Lcom/google/android/music/cloudclient/TrackJson;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/Exception;

    invoke-direct {v12}, Ljava/lang/Exception;-><init>()V

    invoke-static {v10, v11, v12}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 198
    const/4 v10, 0x0

    goto :goto_1

    .line 200
    :cond_1
    iget-object v10, v6, Lcom/google/android/music/cloudclient/TrackJson;->mTitle:Ljava/lang/String;

    aput-object v10, v8, v2

    .line 201
    aput-object v9, v0, v2

    .line 202
    add-int/lit8 v2, v2, 0x1

    .line 203
    goto :goto_0

    .line 206
    .end local v4    # "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .end local v6    # "seedTrack":Lcom/google/android/music/cloudclient/TrackJson;
    .end local v9    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_2
    iget-object v10, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v10, p1, v7, v8, v0}, Lcom/google/android/music/store/Store;->updateSuggestedMixes(I[Ljava/lang/String;[Ljava/lang/String;[Ljava/util/List;)V

    .line 207
    const/4 v10, 0x1

    goto :goto_1
.end method


# virtual methods
.method updateMixes()Z
    .locals 14

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 119
    const/4 v7, 0x0

    .line 121
    .local v7, "success":Z
    :try_start_0
    iget-object v9, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v9}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v6

    .line 122
    .local v6, "streamingAccount":Landroid/accounts/Account;
    if-nez v6, :cond_1

    .line 123
    const-string v9, "MusicLeanback"

    const-string v10, "No valid streaming account configured"

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .end local v6    # "streamingAccount":Landroid/accounts/Account;
    :cond_0
    :goto_0
    return v8

    .line 125
    .restart local v6    # "streamingAccount":Landroid/accounts/Account;
    :cond_1
    iget-object v9, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v9}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v9

    if-nez v9, :cond_3

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isAcceptedUser()Z

    move-result v9

    if-nez v9, :cond_3

    .line 126
    const-string v9, "MusicLeanback"

    const-string v10, "User has no level of service"

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 156
    .end local v6    # "streamingAccount":Landroid/accounts/Account;
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "MusicLeanback"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException while updating suggested mixes. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    sget-boolean v8, Lcom/google/android/music/leanback/SuggestedMixUpdater;->LOGV:Z

    if-eqz v8, :cond_2

    .line 159
    const-string v8, "MusicLeanback"

    const-string v9, "IOException"

    invoke-static {v8, v9, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    :goto_1
    move v8, v7

    .line 167
    goto :goto_0

    .line 130
    .restart local v6    # "streamingAccount":Landroid/accounts/Account;
    :cond_3
    :try_start_1
    iget-object v9, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->cleanupIfDisabled(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 134
    invoke-static {v6}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v5

    .line 136
    .local v5, "sourceAccount":I
    iget-object v9, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->getMaxNumberOfSeeds(Landroid/content/ContentResolver;)I

    move-result v3

    .line 137
    .local v3, "maxSeeds":I
    iget-object v9, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->getMaxMixSize(Landroid/content/ContentResolver;)I

    move-result v2

    .line 138
    .local v2, "maxMixSize":I
    if-lez v3, :cond_4

    if-gtz v2, :cond_5

    .line 140
    :cond_4
    const-string v9, "MusicLeanback"

    const-string v10, "Invalid  number of seeds (%d) or mix size (%d)"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 161
    .end local v2    # "maxMixSize":I
    .end local v3    # "maxSeeds":I
    .end local v5    # "sourceAccount":I
    .end local v6    # "streamingAccount":Landroid/accounts/Account;
    :catch_1
    move-exception v1

    .line 162
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v8, "MusicLeanback"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "InterruptedException while updating suggested mixes. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    sget-boolean v8, Lcom/google/android/music/leanback/SuggestedMixUpdater;->LOGV:Z

    if-eqz v8, :cond_2

    .line 164
    const-string v8, "MusicLeanback"

    const-string v9, "InterruptedException"

    invoke-static {v8, v9, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 145
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "maxMixSize":I
    .restart local v3    # "maxSeeds":I
    .restart local v5    # "sourceAccount":I
    .restart local v6    # "streamingAccount":Landroid/accounts/Account;
    :cond_5
    :try_start_2
    iget-object v9, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v9}, Lcom/google/android/music/preferences/MusicPreferences;->getContentFilter()I

    move-result v0

    .line 146
    .local v0, "contentFilter":I
    iget-object v9, p0, Lcom/google/android/music/leanback/SuggestedMixUpdater;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    invoke-interface {v9, v3, v2, v0}, Lcom/google/android/music/cloudclient/MusicCloud;->getMixesFeed(III)Lcom/google/android/music/cloudclient/RadioFeedResponse;

    move-result-object v4

    .line 148
    .local v4, "radioFeedJson":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    if-eqz v4, :cond_6

    iget-object v9, v4, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    if-eqz v9, :cond_6

    iget-object v9, v4, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v9, v9, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    if-eqz v9, :cond_6

    iget-object v9, v4, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v9, v9, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v9, v10, :cond_7

    .line 151
    :cond_6
    const-string v9, "MusicLeanback"

    const-string v10, "Failed to get suggested mixes content."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 154
    :cond_7
    invoke-direct {p0, v5, v4}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->storeMixes(ILcom/google/android/music/cloudclient/RadioFeedResponse;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v7

    goto/16 :goto_1
.end method
