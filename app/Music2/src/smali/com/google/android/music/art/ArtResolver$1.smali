.class Lcom/google/android/music/art/ArtResolver$1;
.super Ljava/lang/Object;
.source "ArtResolver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ArtRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtResolver;

.field final synthetic val$artUrl:Ljava/lang/String;

.field final synthetic val$artist:Lcom/google/android/music/cloudclient/QuizArtistJson;

.field final synthetic val$artistMetajamId:Ljava/lang/String;

.field final synthetic val$request:Lcom/google/android/music/art/ArtRequest;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;Ljava/lang/String;Lcom/google/android/music/cloudclient/QuizArtistJson;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver$1;->this$0:Lcom/google/android/music/art/ArtResolver;

    iput-object p2, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/art/ArtResolver$1;->val$request:Lcom/google/android/music/art/ArtRequest;

    iput-object p4, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artistMetajamId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artist:Lcom/google/android/music/cloudclient/QuizArtistJson;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$1;->this$0:Lcom/google/android/music/art/ArtResolver;

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$1;->val$request:Lcom/google/android/music/art/ArtRequest;

    # invokes: Lcom/google/android/music/art/ArtResolver;->getGenericArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z
    invoke-static {v0, v1, v2}, Lcom/google/android/music/art/ArtResolver;->access$000(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    .line 403
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artistMetajamId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$1;->this$0:Lcom/google/android/music/art/ArtResolver;

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$1;->val$request:Lcom/google/android/music/art/ArtRequest;

    const-wide/16 v2, -0x1

    iget-object v4, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artistMetajamId:Ljava/lang/String;

    # invokes: Lcom/google/android/music/art/ArtResolver;->processArtistArtCore(Lcom/google/android/music/art/ArtRequest;JLjava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/art/ArtResolver;->access$100(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ArtRequest;JLjava/lang/String;)V

    goto :goto_0

    .line 399
    :cond_1
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getArt(QuizArtistJson): Missing both artistMetajamId and artUrl. No art will be shown for this artist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$1;->val$artist:Lcom/google/android/music/cloudclient/QuizArtistJson;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$1;->val$request:Lcom/google/android/music/art/ArtRequest;

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$1;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    goto :goto_0
.end method
