.class public Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowRecommendationsJson.java"


# instance fields
.field public mListenNowItems:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "listennow_items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/ListenNowItemJson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
