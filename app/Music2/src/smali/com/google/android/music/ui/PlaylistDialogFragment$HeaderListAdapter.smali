.class public Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PlaylistDialogFragment.java"

# interfaces
.implements Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/PlaylistDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "HeaderListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;",
        ">;",
        "Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;"
    }
.end annotation


# instance fields
.field private final mHeader:Ljava/lang/String;

.field private final mLayoutId:I


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "header"    # Ljava/lang/String;
    .param p3, "layoutId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;>;"
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 224
    iput-object p2, p0, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;->mHeader:Ljava/lang/String;

    .line 225
    iput p3, p0, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;->mLayoutId:I

    .line 226
    return-void
.end method


# virtual methods
.method public addLayoutIds(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget v0, p0, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;->mLayoutId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 236
    return-void
.end method

.method public getHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;->mHeader:Ljava/lang/String;

    return-object v0
.end method

.method public getItemLayoutId(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 230
    iget v0, p0, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;->mLayoutId:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 245
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 246
    .local v0, "result":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    iget-object v1, v1, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    return-object v0
.end method
