.class Lcom/google/android/music/xdi/RootHeadersCursor;
.super Landroid/database/MatrixCursor;
.source "RootHeadersCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mItemHeight:I

.field private final mItemWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mContext:Landroid/content/Context;

    .line 23
    new-instance v1, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v1, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 24
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mItemWidth:I

    .line 25
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mItemHeight:I

    .line 27
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 29
    .local v0, "isNautilus":Z
    const/4 v1, 0x0

    const v2, 0x7f0b009d

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/xdi/RootHeadersCursor;->addBrowseHeader(II)V

    .line 31
    const/4 v1, 0x1

    const v2, 0x7f0b0096

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/xdi/RootHeadersCursor;->addBrowseHeader(II)V

    .line 33
    const/4 v1, 0x2

    const v2, 0x7f0b0097

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/xdi/RootHeadersCursor;->addBrowseHeader(II)V

    .line 35
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->hasPlaylists(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    const/4 v1, 0x3

    const v2, 0x7f0b0098

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/xdi/RootHeadersCursor;->addBrowseHeader(II)V

    .line 39
    :cond_0
    if-eqz v0, :cond_1

    .line 40
    const/4 v1, 0x4

    const v2, 0x7f0b0099

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/xdi/RootHeadersCursor;->addBrowseHeader(II)V

    .line 41
    const/4 v1, 0x5

    const v2, 0x7f0b009b

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/xdi/RootHeadersCursor;->addBrowseHeader(II)V

    .line 47
    :goto_0
    return-void

    .line 44
    :cond_1
    const/4 v1, 0x7

    const v2, 0x7f0b009a

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/xdi/RootHeadersCursor;->addBrowseHeader(II)V

    goto :goto_0
.end method

.method private addBrowseHeader(II)V
    .locals 7
    .param p1, "headerId"    # I
    .param p2, "nameResId"    # I

    .prologue
    const/4 v6, 0x0

    .line 54
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/Object;

    .line 56
    .local v1, "values":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 59
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "name"

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 60
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "background_image_uri"

    invoke-virtual {v2, v1, v3, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "bg_image_uri"

    invoke-virtual {v2, v1, v3, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "icon_uri"

    invoke-virtual {v2, v1, v3, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "default_item_width"

    iget v4, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mItemWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "default_item_height"

    iget v4, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mItemHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "color_hint"

    iget-object v4, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00a6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "badge_uri"

    iget-object v4, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    iget-object v2, p0, Lcom/google/android/music/xdi/RootHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "items_uri"

    invoke-virtual {v2, v1, v3, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/RootHeadersCursor;->addRow([Ljava/lang/Object;)V

    .line 73
    return-void
.end method
