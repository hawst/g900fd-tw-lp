.class public Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;
.super Lcom/google/android/music/download/cache/CacheStrategy;
.source "FillUpToLimitCacheStrategy.java"


# instance fields
.field private final mFileSys:Lcom/google/android/music/download/cache/FileSystem;

.field private final mMaxSizeToUse:J

.field private final mMinSizeToLeaveFree:J

.field private final mPercentToUse:F

.field private final mStore:Lcom/google/android/music/store/Store;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/cache/FileSystem;Lcom/google/android/music/store/Store;FJJ)V
    .locals 0
    .param p1, "fileSystem"    # Lcom/google/android/music/download/cache/FileSystem;
    .param p2, "store"    # Lcom/google/android/music/store/Store;
    .param p3, "percentToUse"    # F
    .param p4, "maxSizeToUse"    # J
    .param p6, "minSizeToLeaveFree"    # J

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/music/download/cache/CacheStrategy;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mStore:Lcom/google/android/music/store/Store;

    .line 50
    iput-object p1, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    .line 51
    iput-wide p4, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mMaxSizeToUse:J

    .line 52
    iput p3, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mPercentToUse:F

    .line 53
    iput-wide p6, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mMinSizeToLeaveFree:J

    .line 54
    return-void
.end method

.method private checkMaxSize(JLcom/google/android/music/download/cache/CacheLocation;J)J
    .locals 8
    .param p1, "fileSize"    # J
    .param p3, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p4, "cacheUsedSize"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 86
    iget-wide v4, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mMaxSizeToUse:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-wide v2

    .line 87
    :cond_1
    add-long v0, p4, p1

    .line 88
    .local v0, "newSpaceIfNoDelete":J
    iget-wide v4, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mMaxSizeToUse:J

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 89
    iget-wide v2, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mMaxSizeToUse:J

    sub-long v2, v0, v2

    goto :goto_0
.end method

.method private checkMinFree(JLcom/google/android/music/download/cache/CacheLocation;J)J
    .locals 11
    .param p1, "fileSize"    # J
    .param p3, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p4, "cacheUsedSize"    # J

    .prologue
    .line 108
    iget-wide v4, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mMinSizeToLeaveFree:J

    .line 109
    .local v4, "minFreeSpace":J
    iget-wide v6, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mMinSizeToLeaveFree:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    const-wide/16 v4, 0x0

    .line 110
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {p3}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/music/download/cache/FileSystem;->getFreeSpace(Ljava/io/File;)J

    move-result-wide v2

    .line 111
    .local v2, "freeSize":J
    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    .line 112
    sub-long v6, v4, v2

    add-long/2addr v6, p1

    .line 118
    :goto_0
    return-wide v6

    .line 114
    :cond_1
    sub-long v0, v2, v4

    .line 115
    .local v0, "availableSpace":J
    cmp-long v6, p1, v0

    if-lez v6, :cond_2

    .line 116
    sub-long v6, p1, v0

    goto :goto_0

    .line 118
    :cond_2
    const-wide/16 v6, 0x0

    goto :goto_0
.end method

.method private checkPercentageUsed(JLcom/google/android/music/download/cache/CacheLocation;J)J
    .locals 10
    .param p1, "fileSize"    # J
    .param p3, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p4, "cacheUsedSize"    # J

    .prologue
    const-wide/16 v6, 0x0

    .line 96
    iget v8, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mPercentToUse:F

    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-wide v6

    .line 97
    :cond_1
    iget-object v8, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mFileSys:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {p3}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/google/android/music/download/cache/FileSystem;->getTotalSpace(Ljava/io/File;)J

    move-result-wide v4

    .line 98
    .local v4, "totalSpace":J
    add-long v2, p4, p1

    .line 99
    .local v2, "newSpaceIfNoDelete":J
    long-to-float v8, v4

    iget v9, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mPercentToUse:F

    mul-float/2addr v8, v9

    float-to-long v0, v8

    .line 100
    .local v0, "allowedUsedSpace":J
    cmp-long v8, v2, v0

    if-lez v8, :cond_0

    .line 101
    sub-long v6, v2, v0

    goto :goto_0
.end method


# virtual methods
.method public checkRequiredSpace(JLcom/google/android/music/download/cache/CacheLocation;Z)J
    .locals 15
    .param p1, "fileSize"    # J
    .param p3, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p4, "auto"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->mStore:Lcom/google/android/music/store/Store;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/Store;->getTotalCachedSize(I)J

    move-result-wide v4

    .local v4, "cacheUsedSize":J
    move-object v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    .line 62
    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->checkMaxSize(JLcom/google/android/music/download/cache/CacheLocation;J)J

    move-result-wide v6

    .local v6, "neededByMaxSize":J
    move-object v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    .line 63
    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->checkPercentageUsed(JLcom/google/android/music/download/cache/CacheLocation;J)J

    move-result-wide v10

    .local v10, "neededByPercent":J
    move-object v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    .line 64
    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;->checkMinFree(JLcom/google/android/music/download/cache/CacheLocation;J)J

    move-result-wide v8

    .line 66
    .local v8, "neededByMinFree":J
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v12

    .line 72
    .local v12, "spaceNeeded":J
    if-eqz p4, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, v12, v0

    if-lez v0, :cond_1

    .line 73
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-lez v0, :cond_1

    .line 74
    :cond_0
    new-instance v0, Lcom/google/android/music/download/cache/OutOfSpaceException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot auto-cache because cache cannot reach its max size. Needed by max: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Needed by min-free: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Needed by percent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/download/cache/OutOfSpaceException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    return-wide v12
.end method
