.class public Lcom/google/android/music/ui/RecentActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "RecentActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method static newRecentActivityIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 25
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/google/android/music/ui/RecentActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 26
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/music/ui/RecentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b024b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/RecentActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/google/android/music/ui/MaterialRecentFragment;

    invoke-direct {v0}, Lcom/google/android/music/ui/MaterialRecentFragment;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/RecentActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 21
    return-void
.end method
