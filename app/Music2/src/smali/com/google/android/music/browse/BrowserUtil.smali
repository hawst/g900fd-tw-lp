.class public Lcom/google/android/music/browse/BrowserUtil;
.super Ljava/lang/Object;
.source "BrowserUtil.java"


# static fields
.field public static final AUTOPLAYLIST:Landroid/net/Uri;

.field public static final MAINSTAGE:Landroid/net/Uri;

.field public static final PLAYLISTS:Landroid/net/Uri;

.field private static final PROJECTION_MAINSTAGE:[Ljava/lang/String;

.field private static final PROJECTION_PLAYLISTS:[Ljava/lang/String;

.field private static final PROJECTION_QUEUE:[Ljava/lang/String;

.field private static final PROJECTION_RADIOSTATIONS:[Ljava/lang/String;

.field public static final RADIOSTATIONS:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    const-class v0, Lcom/google/android/music/browse/BrowserUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->TAG:Ljava/lang/String;

    .line 35
    sget-object v0, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->MAINSTAGE:Landroid/net/Uri;

    .line 36
    sget-object v0, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->AUTOPLAYLIST:Landroid/net/Uri;

    .line 37
    sget-object v0, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->PLAYLISTS:Landroid/net/Uri;

    .line 38
    sget-object v0, Lcom/google/android/music/store/MusicContent$RadioStations;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->RADIOSTATIONS:Landroid/net/Uri;

    .line 61
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "playlist_id"

    aput-object v1, v0, v4

    const-string v1, "playlist_name"

    aput-object v1, v0, v5

    const-string v1, "playlist_type"

    aput-object v1, v0, v6

    const-string v1, "playlist_share_token"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "playlist_art_url"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "playlist_owner_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "playlist_owner_profile_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "radio_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "radio_name"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "radio_art"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "radio_seed_source_type"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "radio_seed_source_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "radio_source_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_MAINSTAGE:[Ljava/lang/String;

    .line 109
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "radio_id"

    aput-object v1, v0, v3

    const-string v1, "radio_name"

    aput-object v1, v0, v4

    const-string v1, "radio_art"

    aput-object v1, v0, v5

    const-string v1, "radio_seed_source_type"

    aput-object v1, v0, v6

    const-string v1, "radio_seed_source_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "radio_source_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_RADIOSTATIONS:[Ljava/lang/String;

    .line 125
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "playlist_name"

    aput-object v1, v0, v4

    const-string v1, "playlist_art_url"

    aput-object v1, v0, v5

    const-string v1, "playlist_share_token"

    aput-object v1, v0, v6

    const-string v1, "playlist_type"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    .line 139
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "AlbumArtLocation"

    aput-object v1, v0, v6

    const-string v1, "SongId"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_QUEUE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    .line 59
    return-void
.end method

.method public static getAndroidResourceUri(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 4
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "resourceId"    # I

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    const-string v3, "/"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getComposedAlbumArt(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "artUri"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x60

    .line 540
    if-nez p1, :cond_1

    .line 541
    const/4 v0, 0x0

    .line 555
    :cond_0
    :goto_0
    return-object v0

    .line 543
    :cond_1
    const/4 v0, 0x0

    .line 544
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 547
    .local v1, "urls":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 548
    iget-object v2, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, v4, v4, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedMultiImageComposite(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 550
    if-nez v0, :cond_0

    .line 551
    iget-object v2, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, v4, v4, v5}, Lcom/google/android/music/utils/AlbumArtUtils;->getMultiImageComposite(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private getCursorInt(Landroid/database/Cursor;II)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "columnIndex"    # I
    .param p3, "defaultInt"    # I

    .prologue
    .line 566
    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 567
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result p3

    .line 569
    .end local p3    # "defaultInt":I
    :cond_0
    return p3
.end method

.method private getCursorLong(Landroid/database/Cursor;IJ)J
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "columnIndex"    # I
    .param p3, "defaultLong"    # J

    .prologue
    .line 573
    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p3

    .line 576
    .end local p3    # "defaultLong":J
    :cond_0
    return-wide p3
.end method

.method private getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "columnIndex"    # I
    .param p3, "defaultStr"    # Ljava/lang/String;

    .prologue
    .line 559
    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 560
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 562
    .end local p3    # "defaultStr":Ljava/lang/String;
    :cond_0
    return-object p3
.end method


# virtual methods
.method getAutoPlayListItems(IZ)Ljava/util/List;
    .locals 24
    .param p1, "max"    # I
    .param p2, "isDownloadedOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    sget-object v2, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v23

    .line 192
    .local v23, "playlistsUri":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 193
    const-string v2, "filter"

    sget-object v3, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 196
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 199
    .local v16, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    .local v19, "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    const/4 v10, 0x0

    .line 201
    .local v10, "name":Ljava/lang/String;
    if-nez v16, :cond_1

    .line 238
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return-object v19

    .line 204
    :cond_1
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    :try_start_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    move/from16 v0, v17

    move/from16 v1, p1

    if-ge v0, v1, :cond_6

    .line 205
    sget-object v21, Lcom/google/android/music/browse/PlayableMediaId$Type;->AUTOPLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 206
    .local v21, "playListType":Lcom/google/android/music/browse/PlayableMediaId$Type;
    const/4 v2, 0x0

    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v6

    .line 207
    .local v6, "id":J
    const-wide/16 v2, -0x4

    cmp-long v2, v6, v2

    if-nez v2, :cond_3

    .line 208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00c2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 216
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const/4 v4, 0x4

    const/4 v5, 0x1

    const/16 v8, 0x60

    const/16 v9, 0x60

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-static {v6, v7, v12}, Lcom/google/android/music/store/MusicContent$AutoPlaylists$Members;->getAutoPlaylistItemsUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-static {v2, v12}, Lcom/google/android/music/utils/AlbumArtUtils;->createAlbumIdIteratorFactoryForContentUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;

    move-result-object v12

    new-instance v13, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;

    invoke-direct {v13}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;-><init>()V

    const/4 v14, 0x0

    invoke-static/range {v3 .. v14}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 224
    .local v15, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, v21

    invoke-static {v0, v6, v7}, Lcom/google/android/music/browse/PlayableMediaId;->newAutoPlaylistMediaId(Lcom/google/android/music/browse/PlayableMediaId$Type;J)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v22

    .line 226
    .local v22, "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    new-instance v2, Landroid/media/MediaDescription$Builder;

    invoke-direct {v2}, Landroid/media/MediaDescription$Builder;-><init>()V

    invoke-virtual {v2, v10}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/media/MediaDescription$Builder;->setIconBitmap(Landroid/graphics/Bitmap;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/browse/PlayableMediaId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaDescription$Builder;->build()Landroid/media/MediaDescription;

    move-result-object v20

    .line 231
    .local v20, "mediaDescription":Landroid/media/MediaDescription;
    sget-object v2, Lcom/google/android/music/browse/BrowserUtil;->TAG:Ljava/lang/String;

    invoke-virtual/range {v20 .. v20}, Landroid/media/MediaDescription;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v18, Landroid/media/browse/MediaBrowser$MediaItem;

    const/4 v2, 0x2

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v2}, Landroid/media/browse/MediaBrowser$MediaItem;-><init>(Landroid/media/MediaDescription;I)V

    .line 234
    .local v18, "item":Landroid/media/browse/MediaBrowser$MediaItem;
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 209
    .end local v15    # "bitmap":Landroid/graphics/Bitmap;
    .end local v18    # "item":Landroid/media/browse/MediaBrowser$MediaItem;
    .end local v20    # "mediaDescription":Landroid/media/MediaDescription;
    .end local v22    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    :cond_3
    const-wide/16 v2, -0x3

    cmp-long v2, v6, v2

    if-nez v2, :cond_4

    .line 210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00be

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_2

    .line 211
    :cond_4
    const-wide/16 v2, -0x1

    cmp-long v2, v6, v2

    if-nez v2, :cond_5

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00b9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_2

    .line 213
    :cond_5
    const-wide/16 v2, -0x2

    cmp-long v2, v6, v2

    if-nez v2, :cond_2

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00bb

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    goto/16 :goto_2

    .line 238
    .end local v6    # "id":J
    .end local v21    # "playListType":Lcom/google/android/music/browse/PlayableMediaId$Type;
    :cond_6
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v10    # "name":Ljava/lang/String;
    .end local v17    # "i":I
    .end local v19    # "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    :catchall_0
    move-exception v2

    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method getIFLRadioItem()Landroid/media/browse/MediaBrowser$MediaItem;
    .locals 9

    .prologue
    .line 249
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 250
    .local v2, "prefsOwner":Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-static {v4, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 252
    .local v1, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v3, 0x0

    .line 253
    .local v3, "title":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 254
    iget-object v4, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0054

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 263
    :goto_0
    new-instance v4, Landroid/media/MediaDescription$Builder;

    invoke-direct {v4}, Landroid/media/MediaDescription$Builder;-><init>()V

    invoke-static {}, Lcom/google/android/music/browse/PlayableMediaId;->newLuckyRadioMediaId()Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/browse/PlayableMediaId;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020045

    invoke-static {v5, v6}, Lcom/google/android/music/browse/BrowserUtil;->getAndroidResourceUri(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaDescription$Builder;->setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaDescription$Builder;->build()Landroid/media/MediaDescription;

    move-result-object v0

    .line 269
    .local v0, "description":Landroid/media/MediaDescription;
    new-instance v4, Landroid/media/browse/MediaBrowser$MediaItem;

    const/4 v5, 0x2

    invoke-direct {v4, v0, v5}, Landroid/media/browse/MediaBrowser$MediaItem;-><init>(Landroid/media/MediaDescription;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v4

    .line 256
    .end local v0    # "description":Landroid/media/MediaDescription;
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02eb

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v8, 0x7f0b02ed

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02ec

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v8, 0x7f0b02ed

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    goto :goto_1

    .line 271
    :catchall_0
    move-exception v4

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4
.end method

.method getMainStageItems(IZ)Ljava/util/List;
    .locals 3
    .param p1, "max"    # I
    .param p2, "isDownloadedOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    sget-object v1, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 348
    .local v0, "mainstageUri":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 349
    const-string v1, "filter"

    sget-object v2, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 352
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/google/android/music/browse/BrowserUtil;->getMainStageOrRecentItems(Landroid/net/Uri;I)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method getMainStageOrRecentItems(Landroid/net/Uri;I)Ljava/util/List;
    .locals 37
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "max"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_MAINSTAGE:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 364
    .local v21, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    .local v25, "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    if-nez v21, :cond_0

    .line 454
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return-object v25

    .line 368
    :cond_0
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_1
    :try_start_1
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_b

    move/from16 v0, v23

    move/from16 v1, p2

    if-ge v0, v1, :cond_b

    .line 369
    const/16 v30, 0x0

    .line 370
    .local v30, "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    const/4 v12, 0x0

    .line 371
    .local v12, "name":Ljava/lang/String;
    const/16 v22, 0x0

    .line 372
    .local v22, "description":Ljava/lang/String;
    const/16 v20, 0x0

    .line 373
    .local v20, "artUri":Ljava/lang/String;
    const/16 v17, 0x0

    .line 374
    .local v17, "albumArtBitmap":Landroid/graphics/Bitmap;
    const/16 v4, 0x8

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 375
    const/16 v4, 0x8

    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v6, v7}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v18

    .line 376
    .local v18, "albumId":J
    const/16 v4, 0x9

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 377
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02d1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 378
    const/4 v4, 0x0

    const/16 v5, 0x60

    const/16 v6, 0x60

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v4, v5, v6}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v20

    .line 380
    move-wide/from16 v0, v18

    invoke-static {v0, v1, v12}, Lcom/google/android/music/browse/PlayableMediaId;->newAlbumMediaId(JLjava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v30

    .line 437
    .end local v18    # "albumId":J
    :goto_2
    if-nez v17, :cond_1

    if-eqz v20, :cond_1

    .line 438
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/music/browse/BrowserUtil;->getComposedAlbumArt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 440
    :cond_1
    new-instance v4, Landroid/media/MediaDescription$Builder;

    invoke-direct {v4}, Landroid/media/MediaDescription$Builder;-><init>()V

    invoke-virtual {v4, v12}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/media/MediaDescription$Builder;->setSubtitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v5

    if-nez v20, :cond_a

    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v5, v4}, Landroid/media/MediaDescription$Builder;->setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/media/MediaDescription$Builder;->setIconBitmap(Landroid/graphics/Bitmap;)Landroid/media/MediaDescription$Builder;

    move-result-object v4

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/browse/PlayableMediaId;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaDescription$Builder;->build()Landroid/media/MediaDescription;

    move-result-object v28

    .line 447
    .local v28, "mediaDescription":Landroid/media/MediaDescription;
    sget-object v4, Lcom/google/android/music/browse/BrowserUtil;->TAG:Ljava/lang/String;

    invoke-virtual/range {v28 .. v28}, Landroid/media/MediaDescription;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    new-instance v24, Landroid/media/browse/MediaBrowser$MediaItem;

    const/4 v4, 0x2

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Landroid/media/browse/MediaBrowser$MediaItem;-><init>(Landroid/media/MediaDescription;I)V

    .line 450
    .local v24, "item":Landroid/media/browse/MediaBrowser$MediaItem;
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    .end local v24    # "item":Landroid/media/browse/MediaBrowser$MediaItem;
    .end local v28    # "mediaDescription":Landroid/media/MediaDescription;
    :goto_4
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_1

    .line 381
    :cond_2
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 382
    const/4 v4, 0x1

    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v6, v7}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v8

    .line 383
    .local v8, "listId":J
    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 384
    .local v26, "listName":Ljava/lang/String;
    const/4 v4, 0x3

    const/4 v5, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorInt(Landroid/database/Cursor;II)I

    move-result v27

    .line 385
    .local v27, "listType":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-static {v4, v0, v1}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistPrimaryLabel(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    .line 386
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00a2

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 387
    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 388
    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 390
    .local v36, "shareToken":Ljava/lang/String;
    const/16 v4, 0x46

    move/from16 v0, v27

    if-ne v0, v4, :cond_3

    .line 391
    move-object/from16 v0, v36

    move-object/from16 v1, v20

    invoke-static {v0, v12, v1}, Lcom/google/android/music/browse/PlayableMediaId;->newSharedWithMePlaylistMediaId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v30

    .line 397
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v10, 0x60

    const/16 v11, 0x60

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-static/range {v5 .. v16}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 400
    goto/16 :goto_2

    .line 394
    :cond_3
    invoke-static/range {v27 .. v27}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-static {v8, v9, v0, v4}, Lcom/google/android/music/browse/PlayableMediaId;->newPlaylistMediaId(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v30

    goto :goto_5

    .line 400
    .end local v8    # "listId":J
    .end local v26    # "listName":Ljava/lang/String;
    .end local v27    # "listType":I
    .end local v36    # "shareToken":Ljava/lang/String;
    :cond_4
    const/16 v4, 0x10

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_8

    .line 401
    const/16 v4, 0x11

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 402
    if-nez v12, :cond_5

    .line 403
    const/16 v4, 0x9

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 405
    :cond_5
    const/16 v4, 0x12

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 406
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0099

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 409
    const/16 v4, 0x10

    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v6, v7}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v32

    .line 410
    .local v32, "radioId":J
    const/16 v4, 0x15

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v31, 0x0

    .line 412
    .local v31, "radioRemoteId":Ljava/lang/String;
    :goto_6
    const/16 v4, 0x14

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 414
    .local v34, "radioSeedId":Ljava/lang/String;
    const/16 v4, 0x13

    const/4 v5, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorInt(Landroid/database/Cursor;II)I

    move-result v35

    .line 417
    .local v35, "radioSeedType":I
    if-nez v31, :cond_6

    const-wide/16 v32, -0x1

    .end local v32    # "radioId":J
    :cond_6
    invoke-static/range {v34 .. v34}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    move-wide/from16 v0, v32

    invoke-static {v0, v1, v12, v4, v5}, Lcom/google/android/music/browse/PlayableMediaId;->newRadioMediaId(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v30

    .line 420
    goto/16 :goto_2

    .line 410
    .end local v31    # "radioRemoteId":Ljava/lang/String;
    .end local v34    # "radioSeedId":Ljava/lang/String;
    .end local v35    # "radioSeedType":I
    .restart local v32    # "radioId":J
    :cond_7
    const/16 v4, 0x15

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    goto :goto_6

    .line 420
    .end local v32    # "radioId":J
    :cond_8
    const/16 v4, 0xc

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 421
    const/16 v4, 0x8

    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v6, v7}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v18

    .line 424
    .restart local v18    # "albumId":J
    const/16 v4, 0xc

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 426
    .local v29, "metajamAlbumId":Ljava/lang/String;
    const/16 v4, 0x9

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 427
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02d1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 428
    const/16 v4, 0xd

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 429
    move-wide/from16 v0, v18

    move-object/from16 v2, v29

    invoke-static {v0, v1, v2, v12}, Lcom/google/android/music/browse/PlayableMediaId;->newNautuilusAlbumMediaId(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v30

    .line 431
    goto/16 :goto_2

    .line 432
    .end local v18    # "albumId":J
    .end local v29    # "metajamAlbumId":Ljava/lang/String;
    :cond_9
    sget-object v4, Lcom/google/android/music/browse/BrowserUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected mainstage item: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static/range {v21 .. v21}, Landroid/database/DatabaseUtils;->dumpCursorToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 454
    .end local v12    # "name":Ljava/lang/String;
    .end local v17    # "albumArtBitmap":Landroid/graphics/Bitmap;
    .end local v20    # "artUri":Ljava/lang/String;
    .end local v22    # "description":Ljava/lang/String;
    .end local v23    # "i":I
    .end local v25    # "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    .end local v30    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    :catchall_0
    move-exception v4

    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4

    .line 440
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v17    # "albumArtBitmap":Landroid/graphics/Bitmap;
    .restart local v20    # "artUri":Ljava/lang/String;
    .restart local v22    # "description":Ljava/lang/String;
    .restart local v23    # "i":I
    .restart local v25    # "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    .restart local v30    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    :cond_a
    :try_start_2
    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    goto/16 :goto_3

    .line 454
    .end local v12    # "name":Ljava/lang/String;
    .end local v17    # "albumArtBitmap":Landroid/graphics/Bitmap;
    .end local v20    # "artUri":Ljava/lang/String;
    .end local v22    # "description":Ljava/lang/String;
    .end local v30    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    :cond_b
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method getPlayListItems(IZ)Ljava/util/List;
    .locals 25
    .param p1, "max"    # I
    .param p2, "isDownloadedOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    sget-object v2, Lcom/google/android/music/store/MusicContent$Playlists;->RECENTS_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v24

    .line 281
    .local v24, "recentPlaylistUri":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 282
    const-string v2, "filter"

    sget-object v3, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 285
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 289
    .local v23, "recentPlaylistCursor":Landroid/database/Cursor;
    sget-object v2, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v22

    .line 290
    .local v22, "playlistsUri":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_1

    .line 291
    const-string v2, "filter"

    sget-object v3, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 294
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 297
    .local v8, "allPlaylistCursor":Landroid/database/Cursor;
    const/4 v2, 0x2

    new-array v11, v2, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object v23, v11, v2

    const/4 v2, 0x1

    aput-object v8, v11, v2

    .line 298
    .local v11, "cursors":[Landroid/database/Cursor;
    new-instance v10, Landroid/database/MergeCursor;

    invoke-direct {v10, v11}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 299
    .local v10, "cursor":Landroid/database/Cursor;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 301
    .local v12, "existingPlaylists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :try_start_0
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 302
    .local v17, "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    if-nez v10, :cond_2

    .line 333
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return-object v17

    .line 306
    :cond_2
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    move/from16 v0, p1

    if-ge v13, v0, :cond_5

    .line 307
    const/4 v2, 0x0

    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v14

    .line 309
    .local v14, "id":J
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 310
    add-int/lit8 v13, v13, -0x1

    .line 306
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 313
    :cond_3
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 315
    .local v20, "name":Ljava/lang/String;
    const/4 v2, 0x4

    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorInt(Landroid/database/Cursor;II)I

    move-result v18

    .line 316
    .local v18, "listType":I
    const/16 v2, 0x60

    const/16 v3, 0x60

    invoke-static {v14, v15, v2, v3}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 319
    .local v9, "artUri":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-static {v14, v15, v0, v2}, Lcom/google/android/music/browse/PlayableMediaId;->newPlaylistMediaId(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v21

    .line 321
    .local v21, "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    new-instance v2, Landroid/media/MediaDescription$Builder;

    invoke-direct {v2}, Landroid/media/MediaDescription$Builder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v3

    if-nez v9, :cond_4

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v3, v2}, Landroid/media/MediaDescription$Builder;->setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/browse/PlayableMediaId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaDescription$Builder;->build()Landroid/media/MediaDescription;

    move-result-object v19

    .line 326
    .local v19, "mediaDescription":Landroid/media/MediaDescription;
    sget-object v2, Lcom/google/android/music/browse/BrowserUtil;->TAG:Ljava/lang/String;

    invoke-virtual/range {v19 .. v19}, Landroid/media/MediaDescription;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    new-instance v16, Landroid/media/browse/MediaBrowser$MediaItem;

    const/4 v2, 0x2

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v2}, Landroid/media/browse/MediaBrowser$MediaItem;-><init>(Landroid/media/MediaDescription;I)V

    .line 329
    .local v16, "item":Landroid/media/browse/MediaBrowser$MediaItem;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 333
    .end local v9    # "artUri":Ljava/lang/String;
    .end local v13    # "i":I
    .end local v14    # "id":J
    .end local v16    # "item":Landroid/media/browse/MediaBrowser$MediaItem;
    .end local v17    # "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    .end local v18    # "listType":I
    .end local v19    # "mediaDescription":Landroid/media/MediaDescription;
    .end local v20    # "name":Ljava/lang/String;
    .end local v21    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    :catchall_0
    move-exception v2

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 321
    .restart local v9    # "artUri":Ljava/lang/String;
    .restart local v13    # "i":I
    .restart local v14    # "id":J
    .restart local v17    # "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    .restart local v18    # "listType":I
    .restart local v20    # "name":Ljava/lang/String;
    .restart local v21    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    :cond_4
    :try_start_2
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_3

    .line 333
    .end local v9    # "artUri":Ljava/lang/String;
    .end local v14    # "id":J
    .end local v18    # "listType":I
    .end local v20    # "name":Ljava/lang/String;
    .end local v21    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    :cond_5
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method getQueueItems(IZ)Ljava/util/List;
    .locals 20
    .param p1, "max"    # I
    .param p2, "isDownloadedOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501
    sget-object v2, Lcom/google/android/music/store/MusicContent$Queue;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v17

    .line 502
    .local v17, "queueUri":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 503
    const-string v2, "filter"

    sget-object v3, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 506
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_QUEUE:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 509
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    .local v13, "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    if-nez v10, :cond_1

    .line 535
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return-object v13

    .line 513
    :cond_1
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    move/from16 v0, p1

    if-ge v11, v0, :cond_3

    .line 514
    const/4 v2, 0x4

    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v18

    .line 515
    .local v18, "trackId":J
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 516
    .local v15, "name":Ljava/lang/String;
    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 517
    .local v8, "art":Ljava/lang/String;
    const/4 v9, 0x0

    .line 518
    .local v9, "artUri":Landroid/net/Uri;
    if-eqz v8, :cond_2

    .line 519
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 521
    :cond_2
    move-wide/from16 v0, v18

    invoke-static {v0, v1, v15}, Lcom/google/android/music/browse/PlayableMediaId;->newPlaybackQueueItemMediaId(JLjava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v16

    .line 524
    .local v16, "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    new-instance v2, Landroid/media/MediaDescription$Builder;

    invoke-direct {v2}, Landroid/media/MediaDescription$Builder;-><init>()V

    invoke-virtual {v2, v15}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/media/MediaDescription$Builder;->setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/music/browse/PlayableMediaId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaDescription$Builder;->build()Landroid/media/MediaDescription;

    move-result-object v14

    .line 529
    .local v14, "mediaDescription":Landroid/media/MediaDescription;
    new-instance v12, Landroid/media/browse/MediaBrowser$MediaItem;

    const/4 v2, 0x2

    invoke-direct {v12, v14, v2}, Landroid/media/browse/MediaBrowser$MediaItem;-><init>(Landroid/media/MediaDescription;I)V

    .line 531
    .local v12, "item":Landroid/media/browse/MediaBrowser$MediaItem;
    invoke-interface {v13, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 513
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 535
    .end local v8    # "art":Ljava/lang/String;
    .end local v9    # "artUri":Landroid/net/Uri;
    .end local v12    # "item":Landroid/media/browse/MediaBrowser$MediaItem;
    .end local v14    # "mediaDescription":Landroid/media/MediaDescription;
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    .end local v18    # "trackId":J
    :cond_3
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v11    # "i":I
    .end local v13    # "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    :catchall_0
    move-exception v2

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method getRadioItems(IZ)Ljava/util/List;
    .locals 22
    .param p1, "max"    # I
    .param p2, "isDownloadedOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 459
    sget-object v2, Lcom/google/android/music/store/MusicContent$RadioStations;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v21

    .line 460
    .local v21, "radioUri":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 461
    const-string v2, "filter"

    sget-object v3, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 464
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/browse/BrowserUtil;->PROJECTION_RADIOSTATIONS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 467
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    .local v15, "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    if-nez v10, :cond_1

    .line 496
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return-object v15

    .line 471
    :cond_1
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    move/from16 v0, p1

    if-ge v11, v0, :cond_4

    .line 472
    const/4 v2, 0x0

    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v4, v5}, Lcom/google/android/music/browse/BrowserUtil;->getCursorLong(Landroid/database/Cursor;IJ)J

    move-result-wide v12

    .line 473
    .local v12, "id":J
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 474
    .local v17, "name":Ljava/lang/String;
    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 475
    .local v9, "artUri":Ljava/lang/String;
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorString(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 476
    .local v19, "radioSeedId":Ljava/lang/String;
    const/4 v2, 0x3

    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/music/browse/BrowserUtil;->getCursorInt(Landroid/database/Cursor;II)I

    move-result v20

    .line 478
    .local v20, "radioSeedType":I
    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-static {v12, v13, v0, v2, v3}, Lcom/google/android/music/browse/PlayableMediaId;->newRadioMediaId(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId;

    move-result-object v18

    .line 480
    .local v18, "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    const/4 v8, 0x0

    .line 481
    .local v8, "albumArtBitmap":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_2

    .line 482
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/browse/BrowserUtil;->getComposedAlbumArt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 484
    :cond_2
    new-instance v2, Landroid/media/MediaDescription$Builder;

    invoke-direct {v2}, Landroid/media/MediaDescription$Builder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v3

    if-nez v9, :cond_3

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3, v2}, Landroid/media/MediaDescription$Builder;->setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/media/MediaDescription$Builder;->setIconBitmap(Landroid/graphics/Bitmap;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/browse/PlayableMediaId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaDescription$Builder;->build()Landroid/media/MediaDescription;

    move-result-object v16

    .line 490
    .local v16, "mediaDescription":Landroid/media/MediaDescription;
    new-instance v14, Landroid/media/browse/MediaBrowser$MediaItem;

    const/4 v2, 0x2

    move-object/from16 v0, v16

    invoke-direct {v14, v0, v2}, Landroid/media/browse/MediaBrowser$MediaItem;-><init>(Landroid/media/MediaDescription;I)V

    .line 492
    .local v14, "item":Landroid/media/browse/MediaBrowser$MediaItem;
    invoke-interface {v15, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 471
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 484
    .end local v14    # "item":Landroid/media/browse/MediaBrowser$MediaItem;
    .end local v16    # "mediaDescription":Landroid/media/MediaDescription;
    :cond_3
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_2

    .line 496
    .end local v8    # "albumArtBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "artUri":Ljava/lang/String;
    .end local v12    # "id":J
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "playableId":Lcom/google/android/music/browse/PlayableMediaId;
    .end local v19    # "radioSeedId":Ljava/lang/String;
    .end local v20    # "radioSeedType":I
    :cond_4
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v11    # "i":I
    .end local v15    # "items":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    :catchall_0
    move-exception v2

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method getRecentItems(IZ)Ljava/util/List;
    .locals 3
    .param p1, "max"    # I
    .param p2, "isDownloadedOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    sget-object v1, Lcom/google/android/music/store/MusicContent$Recent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 339
    .local v0, "recentUri":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 340
    const-string v1, "filter"

    sget-object v2, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 343
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/google/android/music/browse/BrowserUtil;->getMainStageOrRecentItems(Landroid/net/Uri;I)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public retrieveContent(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    .local p1, "contentList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 159
    .local v6, "results":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .line 160
    .local v5, "prefsOwner":Ljava/lang/Object;
    iget-object v7, p0, Lcom/google/android/music/browse/BrowserUtil;->mContext:Landroid/content/Context;

    invoke-static {v7, v5}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 161
    .local v4, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v2, 0x0

    .line 163
    .local v2, "isDownloadedOnly":Z
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 165
    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 167
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;

    .line 168
    .local v0, "child":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;
    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getMax()I

    move-result v3

    .line 169
    .local v3, "max":I
    const-string v7, "MAINSTAGE"

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 170
    invoke-virtual {p0, v3, v2}, Lcom/google/android/music/browse/BrowserUtil;->getMainStageItems(IZ)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 165
    .end local v0    # "child":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "max":I
    :catchall_0
    move-exception v7

    invoke-static {v5}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v7

    .line 171
    .restart local v0    # "child":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "max":I
    :cond_0
    const-string v7, "PLAYLISTS"

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 172
    invoke-virtual {p0, v3, v2}, Lcom/google/android/music/browse/BrowserUtil;->getPlayListItems(IZ)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 173
    :cond_1
    const-string v7, "AUTOPLAYLIST"

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 174
    invoke-virtual {p0, v3, v2}, Lcom/google/android/music/browse/BrowserUtil;->getAutoPlayListItems(IZ)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 175
    :cond_2
    const-string v7, "IFLRADIO"

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 176
    invoke-virtual {p0}, Lcom/google/android/music/browse/BrowserUtil;->getIFLRadioItem()Landroid/media/browse/MediaBrowser$MediaItem;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 177
    :cond_3
    const-string v7, "RECENT"

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 178
    invoke-virtual {p0, v3, v2}, Lcom/google/android/music/browse/BrowserUtil;->getRecentItems(IZ)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 179
    :cond_4
    const-string v7, "QUEUE"

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 180
    invoke-virtual {p0, v3, v2}, Lcom/google/android/music/browse/BrowserUtil;->getQueueItems(IZ)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    .line 181
    :cond_5
    const-string v7, "RADIOSTATIONS"

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 182
    invoke-virtual {p0, v3, v2}, Lcom/google/android/music/browse/BrowserUtil;->getRadioItems(IZ)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    .line 184
    :cond_6
    sget-object v7, Lcom/google/android/music/browse/BrowserUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unrecogonized content type "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 187
    .end local v0    # "child":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem$Content;
    .end local v3    # "max":I
    :cond_7
    return-object v6
.end method
