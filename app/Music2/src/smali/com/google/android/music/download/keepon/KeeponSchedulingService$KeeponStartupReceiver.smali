.class public final Lcom/google/android/music/download/keepon/KeeponSchedulingService$KeeponStartupReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KeeponSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/keepon/KeeponSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KeeponStartupReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 555
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 558
    # getter for: Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->access$000()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 559
    const-string v1, "KeeponService"

    const-string v2, "KeeponStartupReceiver.onReceive"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 562
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.download.keepon.KeeponSchedulingService.START_DOWNLOAD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 563
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 564
    return-void
.end method
