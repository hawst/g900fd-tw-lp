.class public Lcom/google/android/music/ui/SpinnerHeaderView;
.super Landroid/widget/RelativeLayout;
.source "SpinnerHeaderView.java"


# instance fields
.field private mSpinner:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/ui/SpinnerHeaderView;->mSpinner:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 35
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f0e01d2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/SpinnerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SpinnerHeaderView;->mSpinner:Landroid/view/View;

    .line 27
    return-void
.end method
