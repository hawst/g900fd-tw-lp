.class public final Lcom/google/android/music/leanback/AutoCacheSchedulingService;
.super Lcom/google/android/music/download/AbstractSchedulingService;
.source "AutoCacheSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/AutoCacheSchedulingService$ActionReceiver;,
        Lcom/google/android/music/leanback/AutoCacheSchedulingService$EnablingReceiver;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mAppUseTime:J

.field private mConnectivityRecoveryTime:J

.field private mCurrentUpdateRefreshedMixes:Z

.field private mCurrentUpdateTriggerTime:J

.field private mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

.field private mInitializedMixes:Z

.field private mLastAttemptTime:J

.field private mLastUpdateTriggerTime:J

.field private mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mScheduledTriggerTime:J

.field private mSourceAccount:I

.field private volatile mStartId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/google/android/music/leanback/LeanbackLog;->LOGV:Z

    sput-boolean v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 123
    const-string v0, "MusicLeanback"

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService;-><init>(Ljava/lang/String;Lcom/google/android/music/download/DownloadRequest$Owner;Ljava/lang/Class;)V

    .line 97
    iput-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    .line 98
    iput-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    .line 99
    iput-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    .line 100
    iput-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    .line 101
    iput-boolean v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    .line 102
    iput v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mSourceAccount:I

    .line 103
    iput-boolean v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    .line 104
    iput-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    .line 105
    iput-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    .line 855
    new-instance v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService$2;-><init>(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 124
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 49
    sget-boolean v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->loadState()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/AutoCacheSchedulingService;ZZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/AutoCacheSchedulingService;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->updateSuggestedMixes(ZZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Intent;

    .prologue
    .line 49
    invoke-static {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->releaseWakeLock(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/AutoCacheSchedulingService;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/AutoCacheSchedulingService;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->checkAutoCacheConditions(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->startAutoCaching()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->stopAutoCaching()V

    return-void
.end method

.method static synthetic access$700(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->startWakefulService(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    return-object v0
.end method

.method private changeAlarmEnabledState(Ljava/lang/Long;)V
    .locals 8
    .param p1, "when"    # Ljava/lang/Long;

    .prologue
    const/4 v5, 0x0

    .line 485
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    move-object v0, v2

    check-cast v0, Landroid/app/AlarmManager;

    .line 486
    .local v0, "alarmist":Landroid/app/AlarmManager;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.music.leanback.AUTO_CACHE_ALARM"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v5, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 488
    .local v1, "intent":Landroid/app/PendingIntent;
    if-eqz p1, :cond_1

    .line 489
    sget-boolean v2, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v2, :cond_0

    const-string v2, "MusicLeanback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set alarm for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v5, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 496
    :goto_0
    return-void

    .line 494
    :cond_1
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private checkAttemptFrequencyAndReschedule()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v4, 0x1

    .line 373
    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gtz v5, :cond_1

    .line 375
    sget-boolean v5, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v5, :cond_0

    const-string v5, "MusicLeanback"

    const-string v6, "Attempt allowed: no previous"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_0
    :goto_0
    return v4

    .line 380
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    sub-long v2, v6, v8

    .line 382
    .local v2, "timeSinceLastAttempt":J
    iget-object v5, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    invoke-virtual {v5}, Lcom/google/android/music/leanback/DailyWindow;->geFrequencyInMillisec()J

    move-result-wide v6

    cmp-long v5, v2, v6

    if-lez v5, :cond_2

    .line 383
    sget-boolean v5, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v5, :cond_0

    .line 384
    const-string v5, "MusicLeanback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Attempt allowed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exceeds window frequency"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 390
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_autocache_min_retry_delay_seconds"

    const-wide/16 v8, 0xe10

    invoke-static {v5, v6, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    mul-long v0, v6, v10

    .line 394
    .local v0, "retryDelayInMillisec":J
    cmp-long v5, v2, v0

    if-lez v5, :cond_3

    .line 395
    sget-boolean v5, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v5, :cond_0

    .line 396
    const-string v5, "MusicLeanback"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Attempt allowed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exceeds retry frequency of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 402
    :cond_3
    sget-boolean v4, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v4, :cond_4

    .line 403
    const-string v4, "MusicLeanback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Attempt disallowed. Previous: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v8, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Retry frequency: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_4
    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    add-long/2addr v4, v0

    add-long/2addr v4, v10

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->scheduleAlarm(J)V

    .line 409
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method private checkAutoCacheConditions(Z)Z
    .locals 11
    .param p1, "powerConnected"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 600
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isAutoCachingEnabled()Z

    move-result v1

    .line 601
    .local v1, "enabled":Z
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isBatteryCharging(Landroid/content/Context;)Z

    move-result v0

    .line 602
    .local v0, "charging":Z
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    .line 604
    sget-boolean v6, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v6, :cond_0

    .line 605
    const-string v6, "MusicLeanback"

    const-string v7, "Power connected but stale charging state detected. Overriding."

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    :cond_0
    const/4 v0, 0x1

    .line 609
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->hasHighSpeedConnection()Z

    move-result v3

    .line 610
    .local v3, "onWifi":Z
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->checkAutoCacheWindow()Z

    move-result v2

    .line 611
    .local v2, "inWindow":Z
    sget-boolean v6, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v6, :cond_2

    .line 612
    const-string v6, "MusicLeanback"

    const-string v7, "autoCacheConditions: enabled=%s, charging=%s, onWifi=%s, inWindow=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    :goto_0
    return v4

    :cond_3
    move v4, v5

    goto :goto_0
.end method

.method private checkAutoCacheWindow()Z
    .locals 6

    .prologue
    .line 699
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    if-nez v2, :cond_0

    .line 700
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->createDailyWindow(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/leanback/DailyWindow;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    .line 703
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 704
    .local v0, "now":J
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    invoke-virtual {v2, v4, v5}, Lcom/google/android/music/leanback/DailyWindow;->isInWindow(J)Z

    move-result v2

    if-nez v2, :cond_2

    .line 707
    sget-boolean v2, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v2, :cond_1

    .line 708
    const-string v2, "MusicLeanback"

    const-string v3, "Last update was outside the window"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/music/leanback/DailyWindow;->isInWindow(J)Z

    move-result v2

    .line 714
    :goto_0
    return v2

    :cond_2
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/google/android/music/leanback/DailyWindow;->isInSameWindow(JJ)Z

    move-result v2

    goto :goto_0
.end method

.method private checkIfAppIsUsedAndReschedule()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 421
    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_1

    .line 447
    :cond_0
    :goto_0
    return v2

    .line 426
    :cond_1
    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 432
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->getMostRecentPlayDate()J

    move-result-wide v0

    .line 433
    .local v0, "playtime":J
    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    .line 434
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    .line 435
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 438
    :cond_2
    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 443
    sget-boolean v2, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v2, :cond_3

    const-string v2, "MusicLeanback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not used since "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_3
    sget-boolean v2, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v2, :cond_4

    const-string v2, "MusicLeanback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not played since "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->scheduleDailyWindowAlarm(Lcom/google/android/music/leanback/DailyWindow;J)V

    .line 447
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private checkLastPlayedSuggestedMixTime()Z
    .locals 26

    .prologue
    .line 626
    sget-boolean v13, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v13, :cond_0

    .line 627
    const-string v13, "MusicLeanback"

    const-string v22, "Checking last time a suggested mix was played."

    move-object/from16 v0, v22

    invoke-static {v13, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    if-nez v13, :cond_2

    .line 631
    sget-boolean v13, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v13, :cond_1

    .line 632
    const-string v13, "MusicLeanback"

    const-string v22, "The mixes have never been initialized.  We should update regardless of last played suggested mix time.  Skip check."

    move-object/from16 v0, v22

    invoke-static {v13, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_1
    const/4 v13, 0x1

    .line 684
    :goto_0
    return v13

    .line 638
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 639
    .local v10, "now":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/music/preferences/MusicPreferences;->getLastTimeSuggestedMixPlayedMillis()J

    move-result-wide v4

    .line 640
    .local v4, "lastTimePlayed":J
    sub-long v14, v10, v4

    .line 641
    .local v14, "timeElapsedSinceLastSuggestedMixPlayed":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    move-wide/from16 v22, v0

    sub-long v16, v10, v22

    .line 643
    .local v16, "timeElapsedSinceLastUpdate":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    .line 645
    .local v12, "resolver":Landroid/content/ContentResolver;
    const-string v13, "music_time_since_suggested_mix_played_lower_update_threshold_hours"

    const-wide/16 v22, 0x18

    move-wide/from16 v0, v22

    invoke-static {v12, v13, v0, v1}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    .line 648
    .local v6, "lowerThresholdHours":J
    sget-object v13, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v22, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v22

    invoke-virtual {v13, v6, v7, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    .line 653
    .local v8, "lowerThresholdMillis":J
    cmp-long v13, v14, v8

    if-gez v13, :cond_4

    .line 654
    sget-boolean v13, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v13, :cond_3

    .line 655
    const-string v13, "MusicLeanback"

    const-string v22, "Time elapsed since the last update is recent enough to allow updating, ok to update.  timeElapsed=%dms lowerLimit=%dms"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v13, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    :cond_3
    const/4 v13, 0x1

    goto :goto_0

    .line 662
    :cond_4
    const-string v13, "music_time_since_suggested_mix_played_upper_threshold_hours"

    const-wide/16 v22, 0xa8

    move-wide/from16 v0, v22

    invoke-static {v12, v13, v0, v1}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v18

    .line 665
    .local v18, "upperThresholdHours":J
    sget-object v13, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v22, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v18

    move-object/from16 v2, v22

    invoke-virtual {v13, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v20

    .line 670
    .local v20, "upperThresholdMillis":J
    cmp-long v13, v16, v20

    if-ltz v13, :cond_6

    .line 671
    sget-boolean v13, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v13, :cond_5

    .line 672
    const-string v13, "MusicLeanback"

    const-string v22, "Time elapsed since the last update is more than the maximum time between updates.  Ok to update.  timeElapsed=%dms maxTime=%dms"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v13, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :cond_5
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 680
    :cond_6
    sget-boolean v13, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v13, :cond_7

    .line 681
    const-string v13, "MusicLeanback"

    const-string v22, "Too soon to update mixes - not played within %d hours and already updated within %d hours"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v13, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    :cond_7
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method private static createDailyWindow(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/leanback/DailyWindow;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 184
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 185
    .local v3, "resolver":Landroid/content/ContentResolver;
    const-string v7, "music_autocache_minutes_since_midnight"

    invoke-virtual {p1}, Lcom/google/android/music/preferences/MusicPreferences;->getAutoCacheMinutesSinceMidnight()I

    move-result v8

    invoke-static {v3, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 188
    .local v4, "start":I
    const-string v7, "music_autocache_duration_in_minutes"

    invoke-virtual {p1}, Lcom/google/android/music/preferences/MusicPreferences;->getAutoCacheDurationInMinutes()I

    move-result v8

    invoke-static {v3, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 191
    .local v1, "duration":I
    const-string v7, "music_autocache_frequency_in_minutes"

    const/16 v8, 0x5a0

    invoke-static {v3, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 194
    .local v2, "frequency":I
    const-string v7, "music_autocache_trigger_window_percentage"

    const/16 v8, 0x28

    invoke-static {v3, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 197
    .local v5, "triggerPercentage":I
    invoke-static {v4, v1}, Lcom/google/android/music/leanback/DailyWindow$Builder;->startBuildingInMinutes(II)Lcom/google/android/music/leanback/DailyWindow$Builder;

    move-result-object v0

    .line 198
    .local v0, "builder":Lcom/google/android/music/leanback/DailyWindow$Builder;
    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/DailyWindow$Builder;->setFrequencyInMinutes(I)Lcom/google/android/music/leanback/DailyWindow$Builder;

    .line 199
    invoke-virtual {v0, v5}, Lcom/google/android/music/leanback/DailyWindow$Builder;->setVariableTriggerPercentage(I)Lcom/google/android/music/leanback/DailyWindow$Builder;

    .line 201
    invoke-virtual {v0}, Lcom/google/android/music/leanback/DailyWindow$Builder;->build()Lcom/google/android/music/leanback/DailyWindow;

    move-result-object v6

    .line 203
    .local v6, "window":Lcom/google/android/music/leanback/DailyWindow;
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_0

    .line 204
    const-string v7, "MusicLeanback"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DailyWindow: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_0
    return-object v6
.end method

.method private disable()V
    .locals 2

    .prologue
    .line 479
    const-class v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$ActionReceiver;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/music/utils/SystemUtils;->setComponentEnabled(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 480
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->changeAlarmEnabledState(Ljava/lang/Long;)V

    .line 481
    return-void
.end method

.method private doUpdate()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 335
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 336
    .local v0, "now":J
    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 337
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    .line 338
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    .line 340
    :cond_0
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    .line 341
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 343
    iget-boolean v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    if-nez v3, :cond_2

    .line 344
    new-instance v2, Lcom/google/android/music/leanback/SuggestedMixUpdater;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/music/leanback/SuggestedMixUpdater;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 346
    .local v2, "suggestedMixUpdater":Lcom/google/android/music/leanback/SuggestedMixUpdater;
    invoke-virtual {v2}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->updateMixes()Z

    move-result v3

    if-nez v3, :cond_1

    .line 350
    const-string v3, "MusicLeanback"

    const-string v4, "Suggested mix update failed"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->scheduleDailyWindowAlarm(Lcom/google/android/music/leanback/DailyWindow;J)V

    .line 362
    .end local v2    # "suggestedMixUpdater":Lcom/google/android/music/leanback/SuggestedMixUpdater;
    :goto_0
    return-void

    .line 355
    .restart local v2    # "suggestedMixUpdater":Lcom/google/android/music/leanback/SuggestedMixUpdater;
    :cond_1
    iput-boolean v8, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    .line 356
    iput-boolean v8, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    .line 357
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 361
    .end local v2    # "suggestedMixUpdater":Lcom/google/android/music/leanback/SuggestedMixUpdater;
    :cond_2
    invoke-direct {p0, v8}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->finishUpdate(Z)V

    goto :goto_0
.end method

.method private enableActionReceiver()V
    .locals 2

    .prologue
    .line 462
    const-class v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$ActionReceiver;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/music/utils/SystemUtils;->setComponentEnabled(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 463
    return-void
.end method

.method private finishUpdate(Z)V
    .locals 8
    .param p1, "success"    # Z

    .prologue
    const/4 v5, 0x0

    .line 323
    sget-boolean v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "MusicLeanback"

    const-string v1, "Finished update. Success: %b Started: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    new-instance v4, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    .line 326
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    .line 327
    iput-boolean v5, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    .line 328
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 330
    iget-object v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    iget-wide v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->scheduleDailyWindowAlarm(Lcom/google/android/music/leanback/DailyWindow;J)V

    .line 331
    return-void
.end method

.method private getAutoCacheManager()Lcom/google/android/music/store/AutoCacheManager;
    .locals 1

    .prologue
    .line 831
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->getAutoCacheManager()Lcom/google/android/music/store/AutoCacheManager;

    move-result-object v0

    return-object v0
.end method

.method private hasHighSpeedConnection()Z
    .locals 3

    .prologue
    .line 688
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v1

    .line 689
    .local v1, "monitor":Lcom/google/android/music/net/INetworkMonitor;
    const/4 v0, 0x0

    .line 691
    .local v0, "hasHighSpeed":Z
    if-eqz v1, :cond_0

    .line 692
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/music/net/INetworkMonitor;->hasHighSpeedConnection()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 695
    :cond_0
    :goto_0
    return v0

    .line 694
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private loadState()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide/16 v4, 0x0

    .line 499
    const-string v1, "autocache.prefs"

    invoke-virtual {p0, v1, v8}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 500
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "last.update.time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    .line 501
    const-string v1, "current.update.time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    .line 502
    const-string v1, "scheduled.trigger.time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    .line 503
    const-string v1, "current.update.mixes.refreshed"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    .line 505
    const-string v1, "initialized"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    .line 506
    const-string v1, "account"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mSourceAccount:I

    .line 507
    const-string v1, "last.attempt.time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    .line 508
    const-string v1, "app.use.time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    .line 509
    const-string v1, "connectivity.recovery.time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    .line 511
    sget-boolean v1, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v1, :cond_0

    .line 512
    const-string v1, "MusicLeanback"

    const-string v2, "State: lastUpdateTrigger=%s, currentUpdateTrigger=%s, scheduledTrigger=%s, currentUpdateRefreshedMixes=%s, initializedMixes=%s, sourceAccount=%s, lastAttempt=%s, appUseTime=%s, connectivityRecoveryTime=%s"

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/Object;

    new-instance v4, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v4, v3, v8

    const/4 v4, 0x1

    new-instance v5, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v5, v3, v4

    const/4 v4, 0x2

    new-instance v5, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-boolean v5, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-boolean v5, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget v5, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mSourceAccount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    new-instance v5, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v5, v3, v4

    const/4 v4, 0x7

    new-instance v5, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v5, v3, v4

    const/16 v4, 0x8

    new-instance v5, Ljava/util/Date;

    iget-wide v6, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :cond_0
    return-void
.end method

.method private static releaseWakeLock(Landroid/content/Intent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 116
    invoke-static {p0}, Landroid/support/v4/content/WakefulBroadcastReceiver;->completeWakefulIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    const-string v0, "MusicLeanback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No wakelock held for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method

.method private saveState()V
    .locals 6

    .prologue
    .line 525
    const-string v2, "autocache.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 526
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 527
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "last.update.time"

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 528
    const-string v2, "current.update.time"

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 529
    const-string v2, "scheduled.trigger.time"

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 530
    const-string v2, "current.update.mixes.refreshed"

    iget-boolean v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 531
    const-string v2, "initialized"

    iget-boolean v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 532
    const-string v2, "account"

    iget v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mSourceAccount:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 533
    const-string v2, "last.attempt.time"

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 534
    const-string v2, "app.use.time"

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 535
    const-string v2, "connectivity.recovery.time"

    iget-wide v4, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 536
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 537
    return-void
.end method

.method private scheduleAlarm(J)V
    .locals 1
    .param p1, "date"    # J

    .prologue
    .line 472
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->changeAlarmEnabledState(Ljava/lang/Long;)V

    .line 473
    return-void
.end method

.method private scheduleDailyWindowAlarm(Lcom/google/android/music/leanback/DailyWindow;J)V
    .locals 2
    .param p1, "window"    # Lcom/google/android/music/leanback/DailyWindow;
    .param p2, "lastTriggerTime"    # J

    .prologue
    .line 466
    invoke-virtual {p1, p2, p3}, Lcom/google/android/music/leanback/DailyWindow;->getTriggerTime(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    .line 467
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 468
    iget-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->scheduleAlarm(J)V

    .line 469
    return-void
.end method

.method private selectConnectivityRecoveryTime()J
    .locals 10

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "music_max_connectivity_restore_delay_seconds"

    const/16 v5, 0xb4

    invoke-static {v1, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 454
    .local v0, "maxConnectivityRestoreDelay":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-long v8, v1

    mul-long/2addr v6, v8

    add-long v2, v4, v6

    .line 456
    .local v2, "selectedTime":J
    sget-boolean v1, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "MusicLeanback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delaying network activity until "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_0
    return-wide v2
.end method

.method private startAutoCaching()V
    .locals 2

    .prologue
    .line 587
    const-string v0, "MusicLeanback"

    const-string v1, "Start autocaching."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    iget v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mStartId:I

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->sendInitScheduleMessage(I)V

    .line 589
    return-void
.end method

.method private static startWakefulService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "broadcastIntent"    # Landroid/content/Intent;

    .prologue
    .line 553
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 554
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.google.android.music.leanback.ACTIVATE_AUTO_CACHE"

    .line 555
    .local v1, "newAction":Ljava/lang/String;
    const-string v3, "com.google.android.music.START_DOWNLOAD_SCHEDULING"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 556
    const-string v1, "com.google.android.music.leanback.APP_IN_USE"

    .line 576
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 577
    .local v2, "refreshIntent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    invoke-static {p0, v2}, Landroid/support/v4/content/WakefulBroadcastReceiver;->startWakefulService(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v3

    if-nez v3, :cond_1

    .line 579
    const-string v3, "MusicLeanback"

    const-string v4, "Failed to start AutoCaching service"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    .end local v2    # "refreshIntent":Landroid/content/Intent;
    :cond_1
    :goto_1
    return-void

    .line 557
    :cond_2
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 558
    const-string v3, "noConnectivity"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 560
    sget-boolean v3, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v3, :cond_1

    const-string v3, "MusicLeanback"

    const-string v4, "Ignoring connectivity loss"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 563
    :cond_3
    const-string v1, "com.google.android.music.leanback.CONNECTIVITY_CHANGE"

    goto :goto_0

    .line 564
    :cond_4
    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 573
    const-string v1, "com.google.android.music.leanback.POWER_CONNECTED"

    goto :goto_0
.end method

.method private stopAutoCaching()V
    .locals 2

    .prologue
    .line 595
    const-string v0, "MusicLeanback"

    const-string v1, "Stop autocaching."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    iget v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mStartId:I

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->sendCancelDownloadsMessage(I)V

    .line 597
    return-void
.end method

.method private updateSuggestedMixes(ZZ)Z
    .locals 14
    .param p1, "updateInUse"    # Z
    .param p2, "connectivityChange"    # Z

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 217
    .local v4, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v0, 0x0

    .line 218
    .local v0, "disable":Z
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v6

    .line 219
    .local v6, "streamingAccount":Landroid/accounts/Account;
    if-nez v6, :cond_2

    .line 220
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_0

    const-string v7, "MusicLeanback"

    const-string v10, "No valid streaming account configured"

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_0
    const/4 v0, 0x1

    .line 228
    :goto_0
    if-eqz v0, :cond_3

    .line 229
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->resetState()V

    .line 230
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->disable()V

    .line 231
    const/4 v1, 0x0

    .line 319
    :cond_1
    :goto_1
    return v1

    .line 225
    :cond_2
    invoke-static {p0}, Lcom/google/android/music/leanback/SuggestedMixUpdater;->cleanupIfDisabled(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 234
    :cond_3
    invoke-static {p0, v4}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->createDailyWindow(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/leanback/DailyWindow;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    .line 236
    invoke-static {v6}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v5

    .line 237
    .local v5, "sourceAccount":I
    iget v7, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mSourceAccount:I

    if-eq v5, v7, :cond_4

    .line 238
    const-string v7, "MusicLeanback"

    const-string v10, "New source account"

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->resetState()V

    .line 240
    iput v5, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mSourceAccount:I

    .line 245
    :cond_4
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->enableActionReceiver()V

    .line 247
    if-eqz p1, :cond_5

    .line 248
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    .line 249
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 252
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 253
    .local v2, "now":J
    iget-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    cmp-long v7, v2, v10

    if-gez v7, :cond_7

    iget-object v7, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    iget-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    invoke-virtual {v7, v10, v11, v2, v3}, Lcom/google/android/music/leanback/DailyWindow;->isInSameWindow(JJ)Z

    move-result v7

    if-nez v7, :cond_7

    .line 258
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_6

    const-string v7, "MusicLeanback"

    const-string v10, "Still before the scheduled trigger. Bailing"

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    .line 262
    :cond_7
    if-eqz p2, :cond_9

    .line 263
    iget-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-nez v7, :cond_8

    .line 264
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->selectConnectivityRecoveryTime()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    .line 265
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 268
    :cond_8
    iget-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    invoke-direct {p0, v10, v11}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->scheduleAlarm(J)V

    .line 269
    const/4 v1, 0x0

    goto :goto_1

    .line 270
    :cond_9
    iget-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-eqz v7, :cond_b

    .line 271
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_a

    const-string v7, "MusicLeanback"

    const-string v10, "Recovered from connectivity change"

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_a
    const-wide/16 v10, 0x0

    iput-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    .line 273
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 276
    :cond_b
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->checkIfAppIsUsedAndReschedule()Z

    move-result v7

    if-nez v7, :cond_c

    .line 279
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 282
    :cond_c
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->checkLastPlayedSuggestedMixTime()Z

    move-result v7

    if-nez v7, :cond_d

    .line 283
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 286
    :cond_d
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->checkAttemptFrequencyAndReschedule()Z

    move-result v7

    if-nez v7, :cond_e

    .line 287
    const/4 v1, 0x1

    goto/16 :goto_1

    .line 291
    :cond_e
    const/4 v1, 0x1

    .line 292
    .local v1, "okToAutoCache":Z
    iget-boolean v7, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    if-nez v7, :cond_10

    .line 294
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_f

    const-string v7, "MusicLeanback"

    const-string v10, "Initial update"

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_f
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->doUpdate()V

    goto/16 :goto_1

    .line 296
    :cond_10
    iget-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-nez v7, :cond_13

    .line 298
    iget-wide v8, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    .line 299
    .local v8, "timeToRun":J
    cmp-long v7, v8, v2

    if-gtz v7, :cond_12

    .line 301
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_11

    const-string v7, "MusicLeanback"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting new update. Last update was at: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    iget-wide v12, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_11
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->doUpdate()V

    goto/16 :goto_1

    .line 305
    :cond_12
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_1

    const-string v7, "MusicLeanback"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Too early to update. Last update: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    iget-wide v12, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ". Will update: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 309
    .end local v8    # "timeToRun":J
    :cond_13
    iget-object v7, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mDailyWindow:Lcom/google/android/music/leanback/DailyWindow;

    iget-wide v10, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    invoke-virtual {v7, v10, v11, v2, v3}, Lcom/google/android/music/leanback/DailyWindow;->isInSameWindow(JJ)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 311
    sget-boolean v7, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v7, :cond_14

    const-string v7, "MusicLeanback"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Continue update started at: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    iget-wide v12, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_14
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->doUpdate()V

    goto/16 :goto_1

    .line 316
    :cond_15
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->finishUpdate(Z)V

    .line 317
    const/4 v1, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method protected getNextDownloads(Lcom/google/android/music/download/cache/ICacheManager;Ljava/util/Collection;)Ljava/util/List;
    .locals 23
    .param p1, "cacheManager"    # Lcom/google/android/music/download/cache/ICacheManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/cache/ICacheManager;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/TrackDownloadRequest;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 768
    .local p2, "blacklistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 770
    .local v21, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->checkAutoCacheConditions(Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 771
    sget-boolean v3, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v3, :cond_0

    .line 772
    const-string v3, "MusicLeanback"

    const-string v5, "getNextDownloads: Conditions for autocache not met. Stopping."

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    :cond_0
    :goto_0
    return-object v21

    .line 777
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getAutoCacheManager()Lcom/google/android/music/store/AutoCacheManager;

    move-result-object v3

    const/4 v5, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v3, v5, v0}, Lcom/google/android/music/store/AutoCacheManager;->getNextAutoCacheDownloads(ILjava/util/Collection;)[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v22

    .line 779
    .local v22, "trackIds":[Lcom/google/android/music/download/ContentIdentifier;
    if-nez v22, :cond_2

    .line 780
    sget-boolean v3, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v3, :cond_0

    .line 781
    const-string v3, "MusicLeanback"

    const-string v5, "getNextDownloads: trackIds=null"

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 786
    :cond_2
    sget-boolean v3, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v3, :cond_3

    .line 787
    const-string v3, "MusicLeanback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getNextDownloads: trackIds.length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v22

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    :cond_3
    move-object/from16 v2, v22

    .local v2, "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    array-length v0, v2

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    aget-object v4, v2, v17

    .line 791
    .local v4, "id":Lcom/google/android/music/download/ContentIdentifier;
    const/16 v20, 0x0

    .line 793
    .local v20, "musicFile":Lcom/google/android/music/store/MusicFile;
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v4}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v6

    invoke-static {v3, v5, v6, v7}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    .line 798
    :goto_2
    if-nez v20, :cond_4

    .line 790
    :goto_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 795
    :catch_0
    move-exception v16

    .line 796
    .local v16, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v3, "MusicLeanback"

    const-string v5, "Failed to load track data: "

    move-object/from16 v0, v16

    invoke-static {v3, v5, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 801
    .end local v16    # "e":Lcom/google/android/music/store/DataNotFoundException;
    :cond_4
    const/4 v13, 0x0

    .line 805
    .local v13, "fileLocation":Lcom/google/android/music/download/cache/FileLocation;
    :try_start_1
    sget-object v3, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v5

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getSize()J

    move-result-wide v6

    const/4 v8, 0x2

    move-object/from16 v3, p1

    invoke-interface/range {v3 .. v8}, Lcom/google/android/music/download/cache/ICacheManager;->getTempFileLocation(Lcom/google/android/music/download/ContentIdentifier;IJI)Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v13

    .line 808
    if-nez v13, :cond_5

    .line 809
    const-string v19, "Failed to get file location."

    .line 810
    .local v19, "msg":Ljava/lang/String;
    const-string v3, "MusicLeanback"

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    new-instance v3, Lcom/google/android/music/download/cache/OutOfSpaceException;

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/google/android/music/download/cache/OutOfSpaceException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 813
    .end local v19    # "msg":Ljava/lang/String;
    :catch_1
    move-exception v16

    .line 814
    .local v16, "e":Landroid/os/RemoteException;
    const-string v3, "MusicLeanback"

    const-string v5, "Failed to get temp file location"

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 817
    .end local v16    # "e":Landroid/os/RemoteException;
    :cond_5
    new-instance v3, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getSourceAccount()I

    move-result v7

    sget v8, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_AUTOCACHE:I

    sget-object v9, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v3 .. v15}, Lcom/google/android/music/download/TrackDownloadRequest;-><init>(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/music/download/TrackOwner;JZLcom/google/android/music/download/cache/FileLocation;Ljava/lang/String;Z)V

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method protected getTotalDownloadSize()J
    .locals 2

    .prologue
    .line 827
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getAutoCacheManager()Lcom/google/android/music/store/AutoCacheManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/AutoCacheManager;->getTotalSizeToAutoCache()J

    move-result-wide v0

    return-wide v0
.end method

.method protected isDownloadingPaused()Z
    .locals 1

    .prologue
    .line 872
    const/4 v0, 0x0

    return v0
.end method

.method protected notifyAllWorkFinished()V
    .locals 2

    .prologue
    .line 743
    const-string v0, "MusicLeanback"

    const-string v1, "All work finished."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    return-void
.end method

.method protected notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V
    .locals 3
    .param p1, "reason"    # Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .prologue
    .line 761
    const-string v0, "MusicLeanback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Disabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    return-void
.end method

.method protected notifyDownloadCompleted(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 721
    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->isSavable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getHttpContentType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getDownloadByteLength()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getStreamFidelity()I

    move-result v6

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->storeInCache(Lcom/google/android/music/download/TrackDownloadRequest;Ljava/lang/String;JI)V

    .line 731
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 733
    return-void

    .line 725
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->deleteFromStorage(Lcom/google/android/music/download/TrackDownloadRequest;)V

    goto :goto_0
.end method

.method protected notifyDownloadFailed(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 738
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->deleteFromStorage(Lcom/google/android/music/download/TrackDownloadRequest;)V

    .line 739
    return-void
.end method

.method protected notifyDownloadProgress(FLcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 5
    .param p1, "progressRatio"    # F
    .param p2, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 753
    sget-boolean v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 754
    const-string v0, "MusicLeanback"

    const-string v1, "id=%s, title=%s, progressRatio=%f"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getTrackTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    :cond_0
    return-void
.end method

.method protected notifyDownloadStarting()V
    .locals 2

    .prologue
    .line 748
    const-string v0, "MusicLeanback"

    const-string v1, "Download starting."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->onCreate()V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 174
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 179
    invoke-super {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->onDestroy()V

    .line 180
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/download/AbstractSchedulingService;->onStartCommand(Landroid/content/Intent;II)I

    .line 131
    iput p3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mStartId:I

    .line 133
    new-instance v0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;-><init>(Lcom/google/android/music/leanback/AutoCacheSchedulingService;Landroid/content/Intent;)V

    .line 161
    .local v0, "runnable":Ljava/lang/Runnable;
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->postRunnable(Ljava/lang/Runnable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    const-string v1, "MusicLeanback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "postRunnable failed when processing intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-static {p1}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->releaseWakeLock(Landroid/content/Intent;)V

    .line 167
    :cond_0
    const/4 v1, 0x3

    return v1
.end method

.method resetState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 540
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastUpdateTriggerTime:J

    .line 541
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateTriggerTime:J

    .line 542
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mScheduledTriggerTime:J

    .line 543
    iput-boolean v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mCurrentUpdateRefreshedMixes:Z

    .line 544
    iput-boolean v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mInitializedMixes:Z

    .line 545
    iput v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mSourceAccount:I

    .line 546
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mAppUseTime:J

    .line 547
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mLastAttemptTime:J

    .line 548
    iput-wide v0, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->mConnectivityRecoveryTime:J

    .line 549
    invoke-direct {p0}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->saveState()V

    .line 550
    return-void
.end method
