.class public Lcom/google/android/music/ui/PlayTabContainer;
.super Landroid/widget/HorizontalScrollView;
.source "PlayTabContainer.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private mLastScrollTo:I

.field private mScrollState:I

.field private mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

.field private final mTitleOffset:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/PlayTabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0}, Lcom/google/android/music/ui/PlayTabContainer;->setupBackground()V

    .line 42
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/PlayTabContainer;->setHorizontalScrollBarEnabled(Z)V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTitleOffset:I

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/PlayTabContainer;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/PlayTabContainer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/music/ui/PlayTabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/PlayTabContainer;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/PlayTabContainer;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/PlayTabContainer;->scrollToChild(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/PlayTabContainer;)Lcom/google/android/music/ui/PlayTabStrip;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/PlayTabContainer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    return-object v0
.end method

.method private scrollToChild(II)V
    .locals 3
    .param p1, "childIndex"    # I
    .param p2, "extraOffset"    # I

    .prologue
    .line 139
    iget-object v2, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v2}, Lcom/google/android/music/ui/PlayTabStrip;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v2, p1}, Lcom/google/android/music/ui/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 144
    .local v0, "selectedLeft":I
    add-int v1, v0, p2

    .line 145
    .local v1, "targetScrollX":I
    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    .line 146
    :cond_2
    iget v2, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTitleOffset:I

    sub-int/2addr v1, v2

    .line 149
    :cond_3
    iget v2, p0, Lcom/google/android/music/ui/PlayTabContainer;->mLastScrollTo:I

    if-eq v1, v2, :cond_0

    .line 153
    iput v1, p0, Lcom/google/android/music/ui/PlayTabContainer;->mLastScrollTo:I

    .line 154
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/ui/PlayTabContainer;->scrollTo(II)V

    goto :goto_0
.end method

.method private setupBackground()V
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlayTabContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 54
    .local v0, "backgroundTile":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 55
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/PlayTabContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    .line 62
    const v0, 0x7f0e01e9

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/PlayTabContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/PlayTabStrip;

    iput-object v0, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    .line 63
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/google/android/music/ui/PlayTabContainer;->mScrollState:I

    .line 128
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 114
    iget-object v2, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v2}, Lcom/google/android/music/ui/PlayTabStrip;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 123
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/music/ui/PlayTabStrip;->onPageScrolled(IFI)V

    .line 120
    iget-object v2, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v2, p1}, Lcom/google/android/music/ui/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 121
    .local v1, "selectedTitle":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    float-to-int v0, v2

    .line 122
    .local v0, "extraOffset":I
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/PlayTabContainer;->scrollToChild(II)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/music/ui/PlayTabContainer;->mScrollState:I

    if-nez v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/PlayTabStrip;->onPageSelected(I)V

    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/PlayTabContainer;->scrollToChild(II)V

    .line 136
    :cond_0
    return-void
.end method

.method public setSelectedIndicatorColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/PlayTabStrip;->setSelectedIndicatorColor(I)V

    .line 70
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 8
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/music/ui/PlayTabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 81
    iget-object v5, p0, Lcom/google/android/music/ui/PlayTabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    .line 82
    .local v0, "adapter":Landroid/support/v4/view/PagerAdapter;
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlayTabContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 83
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 84
    const v5, 0x7f0400c2

    iget-object v6, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 86
    .local v4, "title":Landroid/widget/TextView;
    invoke-virtual {v0, v1}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    move v2, v1

    .line 88
    .local v2, "indexToSelect":I
    new-instance v5, Lcom/google/android/music/ui/PlayTabContainer$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/music/ui/PlayTabContainer$1;-><init>(Lcom/google/android/music/ui/PlayTabContainer;I)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v5, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v5, v4}, Lcom/google/android/music/ui/PlayTabStrip;->addView(Landroid/view/View;)V

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    .end local v2    # "indexToSelect":I
    .end local v4    # "title":Landroid/widget/TextView;
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;

    invoke-virtual {v5}, Lcom/google/android/music/ui/PlayTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    new-instance v6, Lcom/google/android/music/ui/PlayTabContainer$2;

    invoke-direct {v6, p0}, Lcom/google/android/music/ui/PlayTabContainer$2;-><init>(Lcom/google/android/music/ui/PlayTabContainer;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 110
    return-void
.end method
