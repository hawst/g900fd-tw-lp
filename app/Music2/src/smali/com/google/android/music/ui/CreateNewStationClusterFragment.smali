.class public Lcom/google/android/music/ui/CreateNewStationClusterFragment;
.super Lcom/google/android/music/ui/SearchClustersFragment;
.source "CreateNewStationClusterFragment.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mRadioStationTypes:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchClustersFragment;-><init>()V

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v1, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "bestmatch"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 37
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    const-string v2, "genre"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_0
    const-string v2, "artist"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    const-string v2, "album"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    const-string v2, "track"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/SearchClustersFragment;-><init>(Ljava/lang/String;)V

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v1, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "bestmatch"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 37
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    const-string v2, "genre"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_0
    const-string v2, "artist"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    const-string v2, "album"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    const-string v2, "track"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method protected createCluster(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
    .locals 18
    .param p1, "clusterIndex"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 68
    const-string v5, ""

    .line 69
    .local v5, "title":Ljava/lang/String;
    sget-object v4, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 70
    .local v4, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 71
    .local v7, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 72
    .local v8, "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getScreenColumns()I

    move-result v9

    .line 73
    .local v9, "nbColumns":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    .line 75
    .local v10, "nbRows":I
    const-string v2, "bestmatch"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 76
    const-string v2, "searchType"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 77
    .local v17, "itemTypeIdx":I
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 78
    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 79
    .local v16, "itemType":I
    const/4 v2, 0x3

    move/from16 v0, v16

    if-eq v0, v2, :cond_0

    const/4 v2, 0x7

    move/from16 v0, v16

    if-ne v0, v2, :cond_2

    .line 81
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 82
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 99
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 135
    .end local v16    # "itemType":I
    .end local v17    # "itemTypeIdx":I
    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->LOGV:Z

    if-eqz v2, :cond_1

    .line 136
    const-string v2, "CreateNewStationCluster"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Empty doclist for clusterIndex: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 141
    .local v14, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    const/4 v2, 0x1

    invoke-virtual {v14, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsEmulatedRadio(Z)V

    .line 142
    const/4 v2, 0x1

    invoke-virtual {v14, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsFromSearch(Z)V

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v14, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setSearchString(Ljava/lang/String;)V

    goto :goto_2

    .line 83
    .end local v14    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v15    # "i$":Ljava/util/Iterator;
    .restart local v16    # "itemType":I
    .restart local v17    # "itemTypeIdx":I
    :cond_2
    const/4 v2, 0x2

    move/from16 v0, v16

    if-eq v0, v2, :cond_3

    const/4 v2, 0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_3

    const/4 v2, 0x6

    move/from16 v0, v16

    if-ne v0, v2, :cond_4

    .line 86
    :cond_3
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 87
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto :goto_0

    .line 88
    :cond_4
    const/4 v2, 0x5

    move/from16 v0, v16

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8

    move/from16 v0, v16

    if-ne v0, v2, :cond_6

    .line 90
    :cond_5
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 91
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto :goto_0

    .line 92
    :cond_6
    const/16 v2, 0x9

    move/from16 v0, v16

    if-ne v0, v2, :cond_7

    .line 93
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildGenreDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;

    move-result-object v7

    .line 94
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_0

    .line 96
    :cond_7
    const-string v2, "CreateNewStationCluster"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unhandled itemType when populating BEST_MATCH cluster: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 101
    .end local v16    # "itemType":I
    :cond_8
    const-string v2, "CreateNewStationCluster"

    const-string v3, "Failed to move the cursor"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 103
    .end local v17    # "itemTypeIdx":I
    :cond_9
    const-string v2, "artist"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 104
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 105
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b025c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 109
    :goto_3
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 110
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 107
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0261

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 111
    :cond_b
    const-string v2, "album"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 112
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 113
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b025d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 117
    :goto_4
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 118
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 115
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0262

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 119
    :cond_d
    const-string v2, "genre"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 122
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b025f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 123
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildGenreDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;

    move-result-object v7

    .line 124
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 126
    :cond_e
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b025e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 131
    :goto_5
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 132
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 129
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0263

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 146
    .restart local v15    # "i$":Ljava/util/Iterator;
    :cond_10
    new-instance v2, Lcom/google/android/music/ui/Cluster;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mSearchString:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/google/android/music/ui/Cluster;-><init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;ZLcom/google/android/music/store/ContainerDescriptor;)V

    return-object v2
.end method

.method protected getClusterContentUri(I)Landroid/net/Uri;
    .locals 2
    .param p1, "clusterIndex"    # I

    .prologue
    .line 62
    iget-object v1, p0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 63
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mSearchString:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/music/store/MusicContent$Search;->getSearchUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method protected getNumberOfClusters()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mRadioStationTypes:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method protected setupSearchEmptyScreen()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 153
    iget-object v2, p0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mSearchString:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 155
    .local v0, "isEmptyQuery":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 156
    const v2, 0x7f0200f5

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyImageView(I)V

    .line 157
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 158
    const v2, 0x7f0b00f1

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyScreenText(I)V

    .line 159
    const v2, 0x7f02013e

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyImageView(I)V

    .line 168
    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyScreenPadding(Z)V

    .line 169
    return-void

    .line 153
    .end local v0    # "isEmptyQuery":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 161
    .restart local v0    # "isEmptyQuery":Z
    :cond_2
    const v2, 0x7f0b00f2

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyScreenText(I)V

    .line 162
    const v2, 0x7f020105

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyImageView(I)V

    goto :goto_1

    .line 165
    :cond_3
    const v2, 0x7f0200f4

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyImageView(I)V

    .line 166
    const v2, 0x7f0b02aa

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;->setEmptyScreenText(I)V

    goto :goto_1
.end method
