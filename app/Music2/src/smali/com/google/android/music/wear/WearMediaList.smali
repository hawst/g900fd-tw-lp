.class public final Lcom/google/android/music/wear/WearMediaList;
.super Ljava/lang/Object;
.source "WearMediaList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/WearMediaList$MediaListType;
    }
.end annotation


# instance fields
.field public final artistName:Ljava/lang/String;

.field public final complete:Z

.field public final id:J

.field public final title:Ljava/lang/String;

.field public final type:Lcom/google/android/music/wear/WearMediaList$MediaListType;


# direct methods
.method public constructor <init>(JLjava/lang/String;Lcom/google/android/music/wear/WearMediaList$MediaListType;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "type"    # Lcom/google/android/music/wear/WearMediaList$MediaListType;
    .param p5, "artistName"    # Ljava/lang/String;
    .param p6, "complete"    # Z

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide p1, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    .line 35
    const-string v0, "title cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    .line 36
    const-string v0, "type cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/wear/WearMediaList$MediaListType;

    iput-object v0, p0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    .line 37
    sget-object v0, Lcom/google/android/music/wear/WearMediaList$MediaListType;->ALBUM:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    if-ne p4, v0, :cond_0

    if-eqz p5, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "artistName must be set for an album"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 39
    iput-object p5, p0, Lcom/google/android/music/wear/WearMediaList;->artistName:Ljava/lang/String;

    .line 40
    iput-boolean p6, p0, Lcom/google/android/music/wear/WearMediaList;->complete:Z

    .line 41
    return-void

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    instance-of v3, p1, Lcom/google/android/music/wear/WearMediaList;

    if-nez v3, :cond_1

    move v1, v2

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 59
    :cond_1
    if-eq p1, p0, :cond_0

    move-object v0, p1

    .line 62
    check-cast v0, Lcom/google/android/music/wear/WearMediaList;

    .line 63
    .local v0, "other":Lcom/google/android/music/wear/WearMediaList;
    iget-wide v4, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    iget-wide v6, v0, Lcom/google/android/music/wear/WearMediaList;->id:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    iget-object v4, v0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/wear/WearMediaList;->artistName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/wear/WearMediaList;->artistName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/music/wear/WearMediaList;->complete:Z

    iget-boolean v4, v0, Lcom/google/android/music/wear/WearMediaList;->complete:Z

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "id"

    iget-wide v2, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "artistName"

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->artistName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "complete"

    iget-boolean v2, p0, Lcom/google/android/music/wear/WearMediaList;->complete:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
