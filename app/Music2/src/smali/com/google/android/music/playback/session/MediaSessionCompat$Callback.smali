.class public abstract Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Callback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;,
        Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;
    }
.end annotation


# instance fields
.field final mCallbackObj:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 286
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;

    invoke-direct {v0, p0, v2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApi21;-><init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Lcom/google/android/music/playback/session/MediaSessionCompat$1;)V

    invoke-static {v0}, Lcom/google/android/music/playback/session/MediaSessionCompatApi21;->createCallback(Lcom/google/android/music/playback/session/MediaSessionCompatApi21$Callback;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->mCallbackObj:Ljava/lang/Object;

    .line 290
    :goto_0
    return-void

    .line 288
    :cond_0
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;

    invoke-direct {v0, p0, v2}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback$StubApiPreL;-><init>(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Lcom/google/android/music/playback/session/MediaSessionCompat$1;)V

    invoke-static {v0}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL;->createCallback(Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->mCallbackObj:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .locals 0
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "cb"    # Landroid/os/ResultReceiver;

    .prologue
    .line 302
    return-void
.end method

.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 359
    return-void
.end method

.method public onFastForward()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public onGetPlaybackPosition()J
    .locals 2

    .prologue
    .line 409
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "mediaButtonEvent"    # Landroid/content/Intent;

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 340
    return-void
.end method

.method public onPlay()V
    .locals 0

    .prologue
    .line 318
    return-void
.end method

.method public onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "mediaId"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 325
    return-void
.end method

.method public onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 334
    return-void
.end method

.method public onRewind()V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method public onSeekTo(J)V
    .locals 0
    .param p1, "pos"    # J

    .prologue
    .line 397
    return-void
.end method

.method public onSetRating(Lcom/google/android/music/playback/session/RatingCompat;)V
    .locals 0
    .param p1, "rating"    # Lcom/google/android/music/playback/session/RatingCompat;

    .prologue
    .line 403
    return-void
.end method

.method public onSkipToNext()V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public onSkipToPrevious()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public onSkipToQueueItem(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 347
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 389
    return-void
.end method
