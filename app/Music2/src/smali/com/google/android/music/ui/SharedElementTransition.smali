.class public Lcom/google/android/music/ui/SharedElementTransition;
.super Ljava/lang/Object;
.source "SharedElementTransition.java"


# instance fields
.field private mBaseActivity:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/TransitionInfo;Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0
    .param p1, "info"    # Lcom/google/android/music/ui/TransitionInfo;
    .param p2, "activity"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-object p2, p0, Lcom/google/android/music/ui/SharedElementTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    .line 166
    return-void
.end method

.method public static createFromInfo(Lcom/google/android/music/ui/TransitionInfo;Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/SharedElementTransition;
    .locals 3
    .param p0, "info"    # Lcom/google/android/music/ui/TransitionInfo;
    .param p1, "a"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/android/music/ui/TransitionInfo;->getTransitionType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 158
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No valid transition type specified."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 154
    :pswitch_0
    new-instance v0, Lcom/google/android/music/ui/AlbumTransition;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/AlbumTransition;-><init>(Lcom/google/android/music/ui/TransitionInfo;Lcom/google/android/music/ui/BaseActivity;)V

    .line 161
    .local v0, "transition":Lcom/google/android/music/ui/SharedElementTransition;
    return-object v0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static isTransitionEnabled(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "music_enable_activity_transitions"

    invoke-static {v4, v5, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 83
    .local v0, "gServicesEnabled":Z
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 86
    .local v2, "overlayEnabled":Z
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v1

    .line 88
    .local v1, "isLmp":Z
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static final startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "i"    # Landroid/content/Intent;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 46
    instance-of v1, p2, Lcom/google/android/music/ui/ShareableElement;

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/google/android/music/ui/SharedElementTransition;->isTransitionEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 48
    .local v0, "transitionEnabled":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 49
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/SharedElementTransition;->startActivityWithTransition(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)V

    .line 53
    :goto_1
    return-void

    .line 46
    .end local v0    # "transitionEnabled":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    .restart local v0    # "transitionEnabled":Z
    :cond_1
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private static startActivityWithTransition(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "i"    # Landroid/content/Intent;
    .param p2, "view"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 57
    invoke-static {p0}, Lcom/google/android/music/ui/SharedElementTransition;->updateSnapshot(Landroid/content/Context;)V

    move-object v3, p2

    .line 59
    check-cast v3, Lcom/google/android/music/ui/ShareableElement;

    invoke-interface {v3}, Lcom/google/android/music/ui/ShareableElement;->getTransitionInfo()Lcom/google/android/music/ui/TransitionInfo;

    move-result-object v0

    .line 60
    .local v0, "info":Lcom/google/android/music/ui/TransitionInfo;
    invoke-virtual {v0}, Lcom/google/android/music/ui/TransitionInfo;->getSharedElementName()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "sharedElementName":Ljava/lang/String;
    const-string v3, "transitionInfo"

    invoke-virtual {p1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v3, p0

    .line 65
    check-cast v3, Landroid/app/Activity;

    invoke-static {v3, p2, v2}, Landroid/support/v4/app/ActivityOptionsCompat;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v1

    .line 67
    .local v1, "options":Landroid/support/v4/app/ActivityOptionsCompat;
    invoke-virtual {v1}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 68
    return-void
.end method

.method private static updateSnapshot(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    instance-of v0, p0, Lcom/google/android/music/ui/BaseActivity;

    if-eqz v0, :cond_0

    .line 99
    check-cast p0, Lcom/google/android/music/ui/BaseActivity;

    .end local p0    # "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/music/ui/SharedElementTransition$1;

    invoke-direct {v0}, Lcom/google/android/music/ui/SharedElementTransition$1;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->setExitSharedElementCallback(Landroid/support/v4/app/SharedElementCallback;)V

    .line 113
    :cond_0
    return-void
.end method


# virtual methods
.method protected accountForSharedElementPadding(Landroid/view/View;)V
    .locals 13
    .param p1, "sharedElement"    # Landroid/view/View;

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 123
    iget-object v10, p0, Lcom/google/android/music/ui/SharedElementTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v10}, Lcom/google/android/music/ui/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 125
    iget-object v10, p0, Lcom/google/android/music/ui/SharedElementTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v10}, Lcom/google/android/music/ui/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "transitionInfo"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/ui/AlbumTransitionInfo;

    .line 128
    .local v6, "tInfo":Lcom/google/android/music/ui/AlbumTransitionInfo;
    if-eqz v6, :cond_0

    .line 129
    invoke-virtual {v6}, Lcom/google/android/music/ui/AlbumTransitionInfo;->getSharedElementPadding()Landroid/graphics/RectF;

    move-result-object v1

    .line 131
    .local v1, "cardPadding":Landroid/graphics/RectF;
    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v10

    int-to-float v10, v10

    iget v11, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v11

    float-to-int v4, v10

    .line 133
    .local v4, "left":I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    iget v11, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v10, v11

    float-to-int v7, v10

    .line 134
    .local v7, "top":I
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v10

    int-to-float v10, v10

    iget v11, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v11

    float-to-int v5, v10

    .line 135
    .local v5, "right":I
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v10

    int-to-float v10, v10

    iget v11, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v10, v11

    float-to-int v0, v10

    .line 136
    .local v0, "bottom":I
    sub-int v8, v5, v4

    .line 137
    .local v8, "width":I
    invoke-static {v8, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 138
    .local v9, "widthSpec":I
    sub-int v2, v0, v7

    .line 139
    .local v2, "height":I
    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 142
    .local v3, "heightSpec":I
    invoke-virtual {p1, v9, v3}, Landroid/view/View;->measure(II)V

    .line 143
    invoke-virtual {p1, v4, v7, v5, v0}, Landroid/view/View;->layout(IIII)V

    .line 147
    .end local v0    # "bottom":I
    .end local v1    # "cardPadding":Landroid/graphics/RectF;
    .end local v2    # "height":I
    .end local v3    # "heightSpec":I
    .end local v4    # "left":I
    .end local v5    # "right":I
    .end local v6    # "tInfo":Lcom/google/android/music/ui/AlbumTransitionInfo;
    .end local v7    # "top":I
    .end local v8    # "width":I
    .end local v9    # "widthSpec":I
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public setup()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method
