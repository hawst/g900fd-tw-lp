.class public Lcom/google/android/music/cast/CastUtils;
.super Ljava/lang/Object;
.source "CastUtils.java"


# static fields
.field public static final CAST_V2_COMPONENT_NAME:Landroid/content/ComponentName;

.field public static final DIAL_MRP_COMPONENT_NAME:Landroid/content/ComponentName;

.field private static sIsCallbackRegistred:Z

.field private static final sMediaRouterCallback:Landroid/support/v7/media/MediaRouter$Callback;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms"

    const-string v2, "com.google.android.gms.cast.media.CastMediaRouteProviderService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/cast/CastUtils;->CAST_V2_COMPONENT_NAME:Landroid/content/ComponentName;

    .line 70
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.music"

    const-string v2, "com.google.android.music.dial.DialMediaRouteProviderService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/cast/CastUtils;->DIAL_MRP_COMPONENT_NAME:Landroid/content/ComponentName;

    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/music/cast/CastUtils;->sIsCallbackRegistred:Z

    .line 75
    new-instance v0, Lcom/google/android/music/cast/CastUtils$1;

    invoke-direct {v0}, Lcom/google/android/music/cast/CastUtils$1;-><init>()V

    sput-object v0, Lcom/google/android/music/cast/CastUtils;->sMediaRouterCallback:Landroid/support/v7/media/MediaRouter$Callback;

    return-void
.end method

.method static synthetic access$000(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$ProviderInfo;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v7/media/MediaRouter;
    .param p1, "x1"    # Landroid/support/v7/media/MediaRouter$ProviderInfo;

    .prologue
    .line 37
    invoke-static {p0, p1}, Lcom/google/android/music/cast/CastUtils;->removeIfCastV2Provider(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$ProviderInfo;)Z

    move-result v0

    return v0
.end method

.method public static cleaup(Landroid/support/v7/media/MediaRouter;)V
    .locals 1
    .param p0, "router"    # Landroid/support/v7/media/MediaRouter;

    .prologue
    .line 109
    sget-boolean v0, Lcom/google/android/music/cast/CastUtils;->sIsCallbackRegistred:Z

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lcom/google/android/music/cast/CastUtils;->sMediaRouterCallback:Landroid/support/v7/media/MediaRouter$Callback;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/MediaRouter;->removeCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 111
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/music/cast/CastUtils;->sIsCallbackRegistred:Z

    .line 113
    :cond_0
    return-void
.end method

.method public static disableCastV2Provider(Landroid/content/Context;Landroid/support/v7/media/MediaRouter;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mediaRouter"    # Landroid/support/v7/media/MediaRouter;

    .prologue
    .line 92
    new-instance v4, Landroid/support/v7/media/MediaRouteSelector$Builder;

    invoke-direct {v4}, Landroid/support/v7/media/MediaRouteSelector$Builder;-><init>()V

    const-string v5, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v4, v5}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouteSelector$Builder;->build()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v3

    .line 95
    .local v3, "selector":Landroid/support/v7/media/MediaRouteSelector;
    sget-object v4, Lcom/google/android/music/cast/CastUtils;->sMediaRouterCallback:Landroid/support/v7/media/MediaRouter$Callback;

    invoke-virtual {p1, v3, v4}, Landroid/support/v7/media/MediaRouter;->addCallback(Landroid/support/v7/media/MediaRouteSelector;Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 96
    const/4 v4, 0x1

    sput-boolean v4, Lcom/google/android/music/cast/CastUtils;->sIsCallbackRegistred:Z

    .line 97
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter;->getProviders()Ljava/util/List;

    move-result-object v2

    .line 98
    .local v2, "providers":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/media/MediaRouter$ProviderInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/MediaRouter$ProviderInfo;

    .line 99
    .local v1, "provider":Landroid/support/v7/media/MediaRouter$ProviderInfo;
    invoke-static {p1, v1}, Lcom/google/android/music/cast/CastUtils;->removeIfCastV2Provider(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$ProviderInfo;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103
    .end local v1    # "provider":Landroid/support/v7/media/MediaRouter$ProviderInfo;
    :cond_1
    return-void
.end method

.method public static generateMplayUrl(Landroid/content/Context;ZLcom/google/android/music/store/MusicFile;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fromUserAction"    # Z
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    const/4 v4, 0x0

    .line 236
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 237
    .local v2, "urlBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getSourceType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move-object v0, v4

    .line 266
    :cond_0
    :goto_0
    return-object v0

    .line 239
    :pswitch_0
    const-string v3, "https://mclients.googleapis.com/music/mplay?songid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "&pt="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz p1, :cond_1

    const-string v3, "e"

    :goto_2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/music/download/RequestSigningUtil;->appendMplayUrlSignatureParams(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 255
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "url":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/music/cloudclient/MusicRequest;->rewriteURI(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "rewrittenUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    move-object v0, v4

    .line 263
    goto :goto_0

    .line 242
    .end local v0    # "rewrittenUrl":Ljava/lang/String;
    .end local v1    # "url":Ljava/lang/String;
    :pswitch_1
    const-string v3, "https://mclients.googleapis.com/music/mplay?mjck="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 249
    :cond_1
    const-string v3, "a"

    goto :goto_2

    .line 237
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getCastV2AppId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 136
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "music_cast_v2_app_id"

    const-string v2, "F05C50C9"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getCastV2Selector(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/music/cast/CastUtils;->getCastV2AppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCloudQueueUrlForReceiver(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 208
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 209
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string v3, "music_cloud_queue_url_authority"

    const-string v4, "www.googleapis.com"

    invoke-static {v1, v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "apiaryAuthority":Ljava/lang/String;
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "https"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "musicqueue"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "v1.0"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 222
    .local v2, "url":Ljava/lang/String;
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .end local v2    # "url":Ljava/lang/String;
    :goto_0
    return-object v2

    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static isCastV2Disabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 143
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "music_disable_cast_v2"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isCastV2Installed(Landroid/content/Context;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 167
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 168
    .local v5, "pm":Landroid/content/pm/PackageManager;
    new-instance v4, Landroid/content/Intent;

    const-string v10, "android.media.MediaRouteProviderService"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 169
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {v5, v4, v9}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 170
    .local v6, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v7, v6, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 172
    .local v7, "serviceInfo":Landroid/content/pm/ServiceInfo;
    if-eqz v7, :cond_0

    .line 173
    new-instance v0, Landroid/content/ComponentName;

    iget-object v10, v7, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v11, v7, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    .local v0, "cn":Landroid/content/ComponentName;
    sget-object v10, Lcom/google/android/music/cast/CastUtils;->CAST_V2_COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v10, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 175
    const/4 v8, 0x0

    .line 177
    .local v8, "versionCode":I
    :try_start_0
    iget-object v10, v7, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual {v5, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 178
    .local v3, "info":Landroid/content/pm/PackageInfo;
    if-nez v3, :cond_2

    move v8, v9

    .line 182
    .end local v3    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "music_min_required_gms_version_code"

    const v12, 0x43eebe

    invoke-static {v10, v11, v12}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    if-lt v8, v10, :cond_1

    .line 185
    const/4 v9, 0x1

    .line 192
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v6    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v7    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    .end local v8    # "versionCode":I
    :cond_1
    return v9

    .line 178
    .restart local v0    # "cn":Landroid/content/ComponentName;
    .restart local v3    # "info":Landroid/content/pm/PackageInfo;
    .restart local v6    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .restart local v7    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    .restart local v8    # "versionCode":I
    :cond_2
    :try_start_1
    iget v8, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 179
    .end local v3    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v10, "CastUtils"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Couldn\'t get package info for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v7, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isCastV2Route(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 3
    .param p0, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 127
    invoke-virtual {p0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getProvider()Landroid/support/v7/media/MediaRouter$ProviderInfo;

    move-result-object v0

    .line 128
    .local v0, "providerInfo":Landroid/support/v7/media/MediaRouter$ProviderInfo;
    if-nez v0, :cond_0

    .line 129
    const/4 v1, 0x0

    .line 131
    :goto_0
    return v1

    :cond_0
    sget-object v1, Lcom/google/android/music/cast/CastUtils;->CAST_V2_COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isDefaultRouteSelected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 296
    invoke-static {p0}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    .line 297
    .local v0, "mediaRouter":Landroid/support/v7/media/MediaRouter;
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 298
    .local v1, "routeInfo":Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v2

    return v2
.end method

.method public static isDialRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 3
    .param p0, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 273
    invoke-virtual {p0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getProvider()Landroid/support/v7/media/MediaRouter$ProviderInfo;

    move-result-object v0

    .line 274
    .local v0, "providerInfo":Landroid/support/v7/media/MediaRouter$ProviderInfo;
    if-nez v0, :cond_0

    .line 275
    const/4 v1, 0x0

    .line 277
    :goto_0
    return v1

    :cond_0
    sget-object v1, Lcom/google/android/music/cast/CastUtils;->DIAL_MRP_COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 120
    invoke-static {p0}, Lcom/google/android/music/cast/CastUtils;->getCastV2Selector(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "castV2Selector":Ljava/lang/String;
    const-string v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {p1, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->supportsControlCategory(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.cast.CATEGORY_CAST"

    invoke-virtual {p1, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->supportsControlCategory(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->supportsControlCategory(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static removeIfCastV2Provider(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$ProviderInfo;)Z
    .locals 3
    .param p0, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p1, "provider"    # Landroid/support/v7/media/MediaRouter$ProviderInfo;

    .prologue
    .line 158
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/cast/CastUtils;->CAST_V2_COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "CastUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removing provider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getProviderInstance()Landroid/support/v7/media/MediaRouteProvider;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/media/MediaRouter;->removeProvider(Landroid/support/v7/media/MediaRouteProvider;)V

    .line 161
    const/4 v0, 0x1

    .line 163
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static shouldUseCastV1(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    invoke-static {p0}, Lcom/google/android/music/cast/CastUtils;->isCastV2Disabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/music/cast/CastUtils;->isCastV2Installed(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static showMrpVolumeDialog(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 286
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanMR1OrGreater()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_show_mrp_volume_dialog"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static useCastV2Api(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 202
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "music_enable_chromecast_cloud_queue"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method
