.class public Lcom/google/android/music/ui/NowPlayingLaunchDelegateActivity;
.super Landroid/app/Activity;
.source "NowPlayingLaunchDelegateActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static getLaunchIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/NowPlayingLaunchDelegateActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 22
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 23
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 24
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 25
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 26
    return-object v0
.end method


# virtual methods
.method protected onResume()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingLaunchDelegateActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingLaunchDelegateActivity;->finish()V

    .line 40
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 45
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->openNowPlayingDrawer(Landroid/content/Context;)V

    .line 46
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 47
    return-void
.end method
