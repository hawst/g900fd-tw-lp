.class public Lcom/google/android/music/VoiceActionsActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "VoiceActionsActivity.java"

# interfaces
.implements Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;


# static fields
.field private static final LOGV:Z


# instance fields
.field private final EXTRA_METAJAM_ID:Ljava/lang/String;

.field private mActivityDestroyed:Z

.field private mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/VoiceActionsActivity;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    .line 41
    const-string v0, "android.intent.extra.inventory_identifier"

    iput-object v0, p0, Lcom/google/android/music/VoiceActionsActivity;->EXTRA_METAJAM_ID:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/VoiceActionsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/VoiceActionsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/music/VoiceActionsActivity;->launchSearchResults(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/VoiceActionsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/VoiceActionsActivity;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/VoiceActionsActivity;->playIFLRadio()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/VoiceActionsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/VoiceActionsActivity;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/music/VoiceActionsActivity;->mActivityDestroyed:Z

    return v0
.end method

.method private getDeeplinkMetajam(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 223
    const-string v1, "android.intent.extra.inventory_identifier"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "metajamIds":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 225
    :cond_0
    const/4 v1, 0x0

    .line 227
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    aget-object v1, v0, v1

    goto :goto_0
.end method

.method private launchSearchResults(Ljava/lang/String;)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 235
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/SearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    const-string v1, "MusicVoice"

    const-string v2, "Search acitivity query string not set."

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :goto_0
    const-string v1, "surpressKeyboard"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 243
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 244
    invoke-virtual {p0, v0}, Lcom/google/android/music/VoiceActionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 245
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->finish()V

    .line 246
    return-void

    .line 240
    :cond_0
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private playIFLRadio()V
    .locals 4

    .prologue
    .line 148
    const-string v2, "MusicVoice"

    const-string v3, "No query provided. Playing IFL radio or shuffling music."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    .line 152
    .local v1, "hasNautilus":Z
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 154
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/google/android/music/VoiceActionsActivity$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/music/VoiceActionsActivity$2;-><init>(Lcom/google/android/music/VoiceActionsActivity;ZLandroid/content/Context;)V

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 189
    return-void
.end method

.method private processMetajamDeeplink(Ljava/lang/String;)V
    .locals 2
    .param p1, "metajamId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 89
    new-instance v0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-direct {v0}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;-><init>()V

    .line 90
    .local v0, "openMetajamItemInfo":Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setMetajamId(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setCanSendToStore(Z)V

    .line 92
    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setSignUpForced(Z)V

    .line 93
    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setSignUpIfNeeded(Z)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setIsNautilusEnabled(Z)V

    .line 95
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->setAutoPlay(Z)V

    .line 96
    invoke-static {p0, v0, p0}, Lcom/google/android/music/ui/AppNavigation;->openMetajamItem(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    .line 97
    return-void
.end method

.method private processVoiceQuery()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 104
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    .line 106
    .local v1, "hasNautilus":Z
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    .line 107
    .local v2, "hasNetworkConnection":Z
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    const/4 v6, 0x1

    .line 108
    .local v6, "useNautilusSearch":Z
    :goto_0
    const-string v8, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 109
    const-string v8, "query"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 110
    .local v5, "searchItem":Ljava/lang/String;
    if-nez v5, :cond_0

    const-string v5, ""

    .line 111
    :cond_0
    move-object v4, v5

    .line 112
    .local v4, "query":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/music/VoiceActionsActivity;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    new-instance v10, Lcom/google/android/music/VoiceActionsActivity$1;

    invoke-direct {v10, p0, v4}, Lcom/google/android/music/VoiceActionsActivity$1;-><init>(Lcom/google/android/music/VoiceActionsActivity;Ljava/lang/String;)V

    invoke-virtual {v8, v5, v9, v10, v7}, Lcom/google/android/music/utils/VoiceActionHelper;->processVoiceQuery(Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V

    .line 135
    .end local v4    # "query":Ljava/lang/String;
    .end local v5    # "searchItem":Ljava/lang/String;
    :goto_1
    return-void

    .end local v6    # "useNautilusSearch":Z
    :cond_1
    move v6, v7

    .line 107
    goto :goto_0

    .line 128
    .restart local v6    # "useNautilusSearch":Z
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 129
    const-string v7, "MusicVoice"

    const-string v8, "No action provided."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->finish()V

    goto :goto_1

    .line 132
    :cond_3
    const-string v7, "MusicVoice"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unsupported action: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->finish()V

    goto :goto_1
.end method


# virtual methods
.method protected doesSupportDownloadOnlyBanner()Z
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method public onAlbumError()V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/google/android/music/VoiceActionsActivity;->processVoiceQuery()V

    .line 200
    return-void
.end method

.method public onArtistError()V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/music/VoiceActionsActivity;->processVoiceQuery()V

    .line 205
    return-void
.end method

.method public onConnectionError()V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/android/music/VoiceActionsActivity;->processVoiceQuery()V

    .line 210
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->turnOffDownloadedOnlyMode()V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0359

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 51
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->isLeanbackEnvironment(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 52
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 53
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 55
    const-class v3, Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 59
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/music/VoiceActionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->finish()V

    .line 71
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 57
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newSearchIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    .line 63
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    new-instance v3, Lcom/google/android/music/utils/VoiceActionHelper;

    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/google/android/music/utils/VoiceActionHelper;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    iput-object v3, p0, Lcom/google/android/music/VoiceActionsActivity;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/music/VoiceActionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/music/VoiceActionsActivity;->getDeeplinkMetajam(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 65
    .local v2, "metajamId":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    .line 66
    .local v0, "hasNetworkConnection":Z
    if-eqz v0, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 67
    invoke-direct {p0, v2}, Lcom/google/android/music/VoiceActionsActivity;->processMetajamDeeplink(Ljava/lang/String;)V

    goto :goto_1

    .line 69
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/VoiceActionsActivity;->processVoiceQuery()V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/music/VoiceActionsActivity;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/music/VoiceActionsActivity;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    invoke-virtual {v0}, Lcom/google/android/music/utils/VoiceActionHelper;->cancelSearch()V

    .line 143
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/VoiceActionsActivity;->mActivityDestroyed:Z

    .line 144
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onDestroy()V

    .line 145
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/music/VoiceActionsActivity;->getDeeplinkMetajam(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "metajamId":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    .line 78
    .local v0, "hasNetworkConnection":Z
    if-eqz v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 79
    invoke-direct {p0, v1}, Lcom/google/android/music/VoiceActionsActivity;->processMetajamDeeplink(Ljava/lang/String;)V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/VoiceActionsActivity;->processVoiceQuery()V

    goto :goto_0
.end method

.method public onTrackError()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/google/android/music/VoiceActionsActivity;->processVoiceQuery()V

    .line 195
    return-void
.end method
