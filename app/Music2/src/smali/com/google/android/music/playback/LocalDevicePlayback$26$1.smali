.class Lcom/google/android/music/playback/LocalDevicePlayback$26$1;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$26;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

.field final synthetic val$currentPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

.field final synthetic val$nextAudioId:Lcom/google/android/music/download/ContentIdentifier;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$26;Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/playback/AsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 3344
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->val$nextAudioId:Lcom/google/android/music/download/ContentIdentifier;

    iput-object p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->val$currentPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Z)V
    .locals 3
    .param p1, "skipToNext"    # Z

    .prologue
    .line 3348
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to open MusicId ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->val$nextAudioId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") for playback"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3350
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # operator++ for: Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7808(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    .line 3351
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 3353
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->tryNext()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9500(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 3357
    :goto_0
    return-void

    .line 3355
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onPlaybackFailure()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9600(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    goto :goto_0
.end method

.method public onSuccess()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3364
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->val$currentPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v2, v2, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 3365
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I
    invoke-static {v1, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 3367
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$26;

    iget-object v2, v2, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setNextPlayer(Lcom/google/android/music/playback/AsyncMediaPlayer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3375
    :cond_0
    :goto_0
    return-void

    .line 3368
    :catch_0
    move-exception v0

    .line 3369
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "LocalDevicePlayback"

    const-string v2, "failed to set next: nextAudioId=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;->val$nextAudioId:Lcom/google/android/music/download/ContentIdentifier;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
