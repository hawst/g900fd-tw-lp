.class public Lcom/google/android/music/art/ArtResolver2$ArtUrl;
.super Ljava/lang/Object;
.source "ArtResolver2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtResolver2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArtUrl"
.end annotation


# instance fields
.field private final mUrl:Ljava/lang/String;

.field private final mUseAlbumPipeline:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "useAlbumPipeline"    # Z

    .prologue
    .line 604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 605
    const-string v0, "url cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 606
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;

    .line 607
    iput-boolean p2, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUseAlbumPipeline:Z

    .line 608
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 626
    if-ne p0, p1, :cond_1

    .line 634
    :cond_0
    :goto_0
    return v1

    .line 627
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 629
    check-cast v0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .line 631
    .local v0, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    iget-boolean v3, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUseAlbumPipeline:Z

    iget-boolean v4, v0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUseAlbumPipeline:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 632
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 639
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 640
    .local v0, "result":I
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUseAlbumPipeline:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 641
    return v0

    .line 640
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public useAlbumPipeline()Z
    .locals 1

    .prologue
    .line 621
    iget-boolean v0, p0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUseAlbumPipeline:Z

    return v0
.end method
