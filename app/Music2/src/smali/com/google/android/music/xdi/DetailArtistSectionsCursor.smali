.class Lcom/google/android/music/xdi/DetailArtistSectionsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistSectionsCursor.java"


# static fields
.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mContext:Landroid/content/Context;

    .line 41
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 43
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->addSectionsForArtist(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method private addSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 5
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;
    .param p3, "sectionName"    # Ljava/lang/String;
    .param p4, "sectionPath"    # Ljava/lang/String;
    .param p5, "sectionType"    # I
    .param p6, "sectionId"    # I

    .prologue
    .line 98
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/Object;

    .line 99
    .local v1, "values":[Ljava/lang/Object;
    sget-object v2, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "details/artists"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "section"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 106
    .local v0, "contentUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "name"

    invoke-virtual {v2, v1, v3, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_header"

    invoke-virtual {v2, v1, v3, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 109
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    invoke-virtual {v2, v1, v3, p2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 110
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_subname"

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "section_type"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "content_uri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->addRow([Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method private addSectionsForArtist(Ljava/lang/String;)V
    .locals 22
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 47
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v2, v0, v5}, Lcom/google/android/music/xdi/XdiUtils;->getArtistCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 49
    .local v18, "cursor":Landroid/database/Cursor;
    if-nez v18, :cond_0

    .line 94
    :goto_0
    return-void

    .line 54
    :cond_0
    :try_start_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 55
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 56
    .local v4, "artistName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v20

    .line 58
    .local v20, "isNautilusEnabled":Z
    const/4 v8, 0x0

    .line 59
    .local v8, "sectionId":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02d2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "artist"

    const/4 v7, 0x2

    add-int/lit8 v15, v8, 0x1

    .end local v8    # "sectionId":I
    .local v15, "sectionId":I
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->addSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 62
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b02d3

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "albums"

    const/4 v14, 0x1

    add-int/lit8 v8, v15, 0x1

    .end local v15    # "sectionId":I
    .restart local v8    # "sectionId":I
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v4

    invoke-direct/range {v9 .. v15}, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->addSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 67
    if-eqz v20, :cond_1

    .line 68
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v21

    .line 70
    .local v21, "isNautilusId":Z
    if-eqz v21, :cond_2

    .line 71
    move-object/from16 v3, p1

    .line 83
    .local v3, "artistMetajamId":Ljava/lang/String;
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00b1

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "related"

    const/4 v7, 0x1

    add-int/lit8 v15, v8, 0x1

    .end local v8    # "sectionId":I
    .restart local v15    # "sectionId":I
    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->addSection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    .end local v3    # "artistMetajamId":Ljava/lang/String;
    .end local v4    # "artistName":Ljava/lang/String;
    .end local v15    # "sectionId":I
    .end local v20    # "isNautilusEnabled":Z
    .end local v21    # "isNautilusId":Z
    :cond_1
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 73
    .restart local v4    # "artistName":Ljava/lang/String;
    .restart local v8    # "sectionId":I
    .restart local v20    # "isNautilusEnabled":Z
    .restart local v21    # "isNautilusId":Z
    :cond_2
    const-wide/16 v16, 0x0

    .line 75
    .local v16, "artistIdLong":J
    :try_start_1
    invoke-static/range {p1 .. p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v16

    .line 81
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Lcom/google/android/music/xdi/XdiUtils;->getArtistMetajamId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "artistMetajamId":Ljava/lang/String;
    goto :goto_1

    .line 76
    .end local v3    # "artistMetajamId":Ljava/lang/String;
    :catch_0
    move-exception v19

    .line 77
    .local v19, "e":Ljava/lang/NumberFormatException;
    const-string v2, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error converting artist id to long: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 92
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v4    # "artistName":Ljava/lang/String;
    .end local v8    # "sectionId":I
    .end local v16    # "artistIdLong":J
    .end local v19    # "e":Ljava/lang/NumberFormatException;
    .end local v20    # "isNautilusEnabled":Z
    .end local v21    # "isNautilusId":Z
    :catchall_0
    move-exception v2

    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method
