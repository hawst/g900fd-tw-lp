.class public Lcom/google/android/music/store/NautilusContentProviderHelper;
.super Ljava/lang/Object;
.source "NautilusContentProviderHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;,
        Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/NautilusContentProviderHelper;->LOGV:Z

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getTrack(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/AlbumJson;)J
    .locals 11
    .param p0, "normalizer"    # Lcom/google/android/music/store/TagNormalizer;
    .param p1, "album"    # Lcom/google/android/music/cloudclient/AlbumJson;

    .prologue
    .line 513
    iget-object v6, p1, Lcom/google/android/music/cloudclient/AlbumJson;->mAlbumArtist:Ljava/lang/String;

    invoke-static {p0, v6}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 514
    .local v1, "canonicalArtist":Ljava/lang/String;
    iget-object v6, p1, Lcom/google/android/music/cloudclient/AlbumJson;->mName:Ljava/lang/String;

    invoke-static {p0, v6}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 516
    .local v2, "canonicalName":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-nez v2, :cond_2

    .line 517
    :cond_0
    const-wide/16 v4, -0x1

    .line 530
    :cond_1
    :goto_0
    return-wide v4

    .line 520
    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v6, 0x100

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 521
    .local v0, "buffer":Ljava/lang/StringBuffer;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/16 v7, 0x1f

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 525
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 526
    .local v3, "key":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v4

    .line 527
    .local v4, "id":J
    sget-boolean v6, Lcom/google/android/music/store/NautilusContentProviderHelper;->LOGV:Z

    if-eqz v6, :cond_1

    .line 528
    const-string v6, "NautilusContentProvider"

    const-string v7, "Album: key=%s, id=%d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/ArtistJson;)J
    .locals 8
    .param p0, "normalizer"    # Lcom/google/android/music/store/TagNormalizer;
    .param p1, "artist"    # Lcom/google/android/music/cloudclient/ArtistJson;

    .prologue
    .line 500
    iget-object v3, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mName:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 501
    .local v2, "key":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 502
    const-wide/16 v0, -0x1

    .line 509
    :cond_0
    :goto_0
    return-wide v0

    .line 505
    :cond_1
    invoke-static {v2}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v0

    .line 506
    .local v0, "id":J
    sget-boolean v3, Lcom/google/android/music/store/NautilusContentProviderHelper;->LOGV:Z

    if-eqz v3, :cond_0

    .line 507
    const-string v3, "NautilusContentProvider"

    const-string v4, "Artist: key=%s, id=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/sync/google/model/Track;)J
    .locals 13
    .param p0, "normalizer"    # Lcom/google/android/music/store/TagNormalizer;
    .param p1, "track"    # Lcom/google/android/music/sync/google/model/Track;

    .prologue
    const/16 v10, 0x1f

    .line 535
    iget-object v8, p1, Lcom/google/android/music/sync/google/model/Track;->mAlbumArtist:Ljava/lang/String;

    invoke-static {p0, v8}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 536
    .local v2, "canonicalAlbumArtist":Ljava/lang/String;
    iget-object v8, p1, Lcom/google/android/music/sync/google/model/Track;->mArtist:Ljava/lang/String;

    invoke-static {p0, v8}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 537
    .local v4, "canonicalTrackArtist":Ljava/lang/String;
    iget-object v8, p1, Lcom/google/android/music/sync/google/model/Track;->mAlbum:Ljava/lang/String;

    invoke-static {p0, v8}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 538
    .local v1, "canonicalAlbum":Ljava/lang/String;
    iget-object v8, p1, Lcom/google/android/music/sync/google/model/Track;->mTitle:Ljava/lang/String;

    invoke-static {p0, v8}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 540
    .local v3, "canonicalTitle":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    if-nez v3, :cond_2

    .line 542
    :cond_0
    const-wide/16 v6, -0x1

    .line 566
    :cond_1
    :goto_0
    return-wide v6

    .line 545
    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v8, 0x100

    invoke-direct {v0, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 546
    .local v0, "buffer":Ljava/lang/StringBuffer;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 554
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v8

    iget v9, p1, Lcom/google/android/music/sync/google/model/Track;->mDiscNumber:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v8

    iget v9, p1, Lcom/google/android/music/sync/google/model/Track;->mTrackNumber:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 557
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 558
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 561
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 562
    .local v5, "key":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v6

    .line 563
    .local v6, "id":J
    sget-boolean v8, Lcom/google/android/music/store/NautilusContentProviderHelper;->LOGV:Z

    if-eqz v8, :cond_1

    .line 564
    const-string v8, "NautilusContentProvider"

    const-string v9, "Track: key=%s, id=%d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v11, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 627
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v1

    .line 628
    .local v1, "cache":Lcom/google/android/music/store/NautilusContentCache;
    invoke-virtual {v1, p1}, Lcom/google/android/music/store/NautilusContentCache;->getAlbum(Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v0

    .line 629
    .local v0, "album":Lcom/google/android/music/cloudclient/AlbumJson;
    if-nez v0, :cond_0

    .line 631
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCloudClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicCloud;

    move-result-object v2

    .line 633
    .local v2, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    invoke-interface {v2, p1}, Lcom/google/android/music/cloudclient/MusicCloud;->getNautilusAlbum(Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v0

    .line 636
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/NautilusContentCache;->putAlbum(Lcom/google/android/music/cloudclient/AlbumJson;)V
    :try_end_0
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 652
    .end local v2    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :cond_0
    :goto_0
    return-object v0

    .line 637
    .restart local v2    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :catch_0
    move-exception v3

    .line 638
    .local v3, "e":Lorg/apache/http/client/HttpResponseException;
    invoke-virtual {v3}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v4

    const/16 v5, 0x194

    if-ne v4, v5, :cond_1

    .line 641
    const-string v4, "NautilusContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Album not found by id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Clearing the Recent table."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    invoke-static {p0, p1}, Lcom/google/android/music/store/RecentItemsManager;->deleteNautilusAlbum(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 644
    :cond_1
    const-string v4, "NautilusContentProvider"

    invoke-virtual {v3}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 646
    .end local v3    # "e":Lorg/apache/http/client/HttpResponseException;
    :catch_1
    move-exception v3

    .line 647
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v4, "NautilusContentProvider"

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 648
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v3

    .line 649
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "NautilusContentProvider"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 605
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v1

    .line 606
    .local v1, "cache":Lcom/google/android/music/store/NautilusContentCache;
    invoke-virtual {v1, p1}, Lcom/google/android/music/store/NautilusContentCache;->getArtist(Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v0

    .line 607
    .local v0, "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    if-nez v0, :cond_0

    .line 609
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCloudClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicCloud;

    move-result-object v2

    .line 611
    .local v2, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    const/16 v4, 0x14

    const/16 v5, 0x14

    const/4 v6, 0x1

    :try_start_0
    invoke-interface {v2, p1, v4, v5, v6}, Lcom/google/android/music/cloudclient/MusicCloud;->getNautilusArtist(Ljava/lang/String;IIZ)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v0

    .line 615
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/NautilusContentCache;->putArtist(Lcom/google/android/music/cloudclient/ArtistJson;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 622
    .end local v2    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :cond_0
    :goto_0
    return-object v0

    .line 616
    .restart local v2    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :catch_0
    move-exception v3

    .line 617
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v4, "NautilusContentProvider"

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 618
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v3

    .line 619
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "NautilusContentProvider"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static getArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 570
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v1

    .line 573
    .local v1, "cache":Lcom/google/android/music/store/NautilusContentCache;
    invoke-virtual {v1, p1}, Lcom/google/android/music/store/NautilusContentCache;->getArtistArtUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 574
    .local v4, "url":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 600
    .end local v4    # "url":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 579
    .restart local v4    # "url":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1, p1}, Lcom/google/android/music/store/NautilusContentCache;->getArtist(Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v0

    .line 580
    .local v0, "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    if-eqz v0, :cond_1

    .line 581
    iget-object v4, v0, Lcom/google/android/music/cloudclient/ArtistJson;->mArtistArtRef:Ljava/lang/String;

    goto :goto_0

    .line 586
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCloudClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicCloud;

    move-result-object v2

    .line 588
    .local v2, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_0
    invoke-interface {v2, p1, v5, v6, v7}, Lcom/google/android/music/cloudclient/MusicCloud;->getNautilusArtist(Ljava/lang/String;IIZ)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_2

    .line 591
    iget-object v5, v0, Lcom/google/android/music/cloudclient/ArtistJson;->mArtistArtRef:Ljava/lang/String;

    invoke-virtual {v1, p1, v5}, Lcom/google/android/music/store/NautilusContentCache;->putArtistArtUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v4, v0, Lcom/google/android/music/cloudclient/ArtistJson;->mArtistArtRef:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 594
    :catch_0
    move-exception v3

    .line 595
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v5, "NautilusContentProvider"

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 600
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :goto_1
    const/4 v4, 0x0

    goto :goto_0

    .line 596
    :catch_1
    move-exception v3

    .line 597
    .local v3, "e":Ljava/io/IOException;
    const-string v5, "NautilusContentProvider"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static getCanonicalString(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "normalizer"    # Lcom/google/android/music/store/TagNormalizer;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 710
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 711
    .local v0, "canonicalString":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 712
    move-object v0, p1

    .line 714
    :cond_0
    return-object v0

    .line 710
    .end local v0    # "canonicalString":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getCloudClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicCloud;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 676
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static getTrack(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 656
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentCache;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;

    move-result-object v0

    .line 657
    .local v0, "cache":Lcom/google/android/music/store/NautilusContentCache;
    invoke-virtual {v0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v3

    .line 658
    .local v3, "track":Lcom/google/android/music/sync/google/model/Track;
    if-nez v3, :cond_0

    .line 660
    invoke-static {p0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getCloudClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicCloud;

    move-result-object v1

    .line 662
    .local v1, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    invoke-interface {v1, p1}, Lcom/google/android/music/cloudclient/MusicCloud;->getNautilusTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v3

    .line 665
    invoke-virtual {v0, v3}, Lcom/google/android/music/store/NautilusContentCache;->putTrack(Lcom/google/android/music/sync/google/model/Track;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 672
    .end local v1    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :cond_0
    :goto_0
    return-object v3

    .line 666
    .restart local v1    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :catch_0
    move-exception v2

    .line 667
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v4, "NautilusContentProvider"

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 668
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v2

    .line 669
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "NautilusContentProvider"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static insert(Landroid/content/Context;Lcom/google/android/music/store/Store;ILandroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "type"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "nid"    # Ljava/lang/String;

    .prologue
    .line 331
    const-string v14, "addToLibrary"

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 333
    .local v2, "addToLibrary":Z
    const/4 v11, 0x0

    .line 334
    .local v11, "newUri":Landroid/net/Uri;
    packed-switch p2, :pswitch_data_0

    .line 403
    :goto_0
    :pswitch_0
    if-eqz v11, :cond_0

    if-eqz v2, :cond_0

    .line 405
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 406
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/RecentItemsManager;->updateRecentItemsAsync(Landroid/content/Context;)V

    :cond_0
    move-object v14, v11

    .line 408
    :goto_1
    return-object v14

    .line 336
    :pswitch_1
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getTrack(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v12

    .line 337
    .local v12, "track":Lcom/google/android/music/sync/google/model/Track;
    if-nez v12, :cond_1

    .line 338
    const-string v14, "NautilusContentProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Couldn\'t get the track for id: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    const/4 v14, 0x0

    goto :goto_1

    .line 342
    :cond_1
    const/4 v14, 0x1

    new-array v14, v14, [Lcom/google/android/music/sync/google/model/Track;

    const/4 v15, 0x0

    aput-object v12, v14, v15

    invoke-static {v14}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insertTracks(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/List;Z)Landroid/net/Uri;

    move-result-object v11

    .line 343
    goto :goto_0

    .line 346
    .end local v12    # "track":Lcom/google/android/music/sync/google/model/Track;
    :pswitch_2
    new-instance v13, Ljava/util/LinkedList;

    invoke-direct {v13}, Ljava/util/LinkedList;-><init>()V

    .line 347
    .local v13, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    const-string v14, ","

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 348
    .local v9, "ids":[Ljava/lang/String;
    move-object v4, v9

    .local v4, "arr$":[Ljava/lang/String;
    array-length v10, v4

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_2
    if-ge v7, v10, :cond_3

    aget-object v8, v4, v7

    .line 349
    .local v8, "id":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getTrack(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v12

    .line 350
    .restart local v12    # "track":Lcom/google/android/music/sync/google/model/Track;
    if-nez v12, :cond_2

    .line 351
    const-string v14, "NautilusContentProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Couldn\'t get the track for id: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 353
    :cond_2
    invoke-interface {v13, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 356
    .end local v8    # "id":Ljava/lang/String;
    .end local v12    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_3
    invoke-static {v13}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insertTracks(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/List;Z)Landroid/net/Uri;

    move-result-object v11

    .line 357
    goto/16 :goto_0

    .line 360
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v9    # "ids":[Ljava/lang/String;
    .end local v10    # "len$":I
    .end local v13    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :pswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v3

    .line 361
    .local v3, "album":Lcom/google/android/music/cloudclient/AlbumJson;
    if-nez v3, :cond_4

    .line 362
    const-string v14, "NautilusContentProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Couldn\'t get the album for id: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 364
    :cond_4
    iget-object v14, v3, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    if-eqz v14, :cond_5

    iget-object v14, v3, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    if-nez v14, :cond_6

    .line 365
    :cond_5
    const-string v14, "NautilusContentProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "The album doesn\'t contain any tracks: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 369
    :cond_6
    iget-object v14, v3, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insertTracks(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/List;Z)Landroid/net/Uri;

    move-result-object v11

    .line 370
    goto/16 :goto_0

    .line 373
    .end local v3    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    :pswitch_4
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v5

    .line 374
    .local v5, "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    if-nez v5, :cond_7

    .line 375
    const-string v14, "NautilusContentProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Couldn\'t get the artist for id: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 377
    :cond_7
    iget-object v14, v5, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    if-eqz v14, :cond_8

    iget-object v14, v5, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    if-nez v14, :cond_9

    .line 378
    :cond_8
    const-string v14, "NautilusContentProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "The artist doesn\'t contain any albums: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 382
    :cond_9
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 383
    .local v6, "artistTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    iget-object v14, v5, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/AlbumJson;

    .line 384
    .restart local v3    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    iget-object v13, v3, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    .line 385
    .restart local v13    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    if-nez v13, :cond_b

    .line 388
    iget-object v14, v3, Lcom/google/android/music/cloudclient/AlbumJson;->mAlbumId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/google/android/music/store/NautilusContentProviderHelper;->getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v3

    .line 389
    if-eqz v3, :cond_b

    .line 390
    iget-object v13, v3, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    .line 393
    :cond_b
    if-eqz v13, :cond_a

    .line 394
    invoke-interface {v6, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 398
    .end local v3    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    .end local v13    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v6, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insertTracks(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/List;Z)Landroid/net/Uri;

    move-result-object v11

    .line 399
    goto/16 :goto_0

    .line 334
    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static insertTracks(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/List;Z)Landroid/net/Uri;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p3, "addToLibrary"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/music/store/Store;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;Z)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .local p2, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    const/4 v4, 0x0

    .line 685
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 687
    .local v2, "owner":Ljava/lang/Object;
    invoke-static {p0, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 690
    .local v1, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 692
    .local v3, "streamingAccount":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 695
    if-nez v3, :cond_1

    .line 705
    :cond_0
    :goto_0
    return-object v4

    .line 692
    .end local v3    # "streamingAccount":Landroid/accounts/Account;
    :catchall_0
    move-exception v4

    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4

    .line 699
    .restart local v3    # "streamingAccount":Landroid/accounts/Account;
    :cond_1
    invoke-static {v3}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v5

    invoke-virtual {p1, v5, p2, p3}, Lcom/google/android/music/store/Store;->tryToInsertOrUpdateExternalSongs(ILjava/util/List;Z)Ljava/util/List;

    move-result-object v0

    .line 701
    .local v0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 702
    const-string v4, "NautilusContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Successfully inserted "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " tracks."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$XAudio;->getSelectedAudioUri(Ljava/util/List;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_0
.end method

.method public static merge(Landroid/database/Cursor;Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "to"    # Landroid/database/Cursor;
    .param p1, "from"    # Landroid/database/Cursor;
    .param p2, "mergeOnColumn"    # Ljava/lang/String;

    .prologue
    .line 422
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 423
    .local v3, "srcMergeColValuesMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 426
    .local v0, "mergeColumnIdx":I
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 427
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 428
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 432
    :cond_1
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 433
    .local v2, "result":Landroid/database/MatrixCursor;
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 434
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 437
    .local v1, "mergeColumnVal":Ljava/lang/String;
    :goto_2
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 438
    .local v4, "srcMergeRowPos":Ljava/lang/Integer;
    if-eqz v4, :cond_4

    .line 439
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 440
    invoke-static {v2, p1}, Lcom/google/android/music/utils/DbUtils;->addRowToMatrixCursor(Landroid/database/MatrixCursor;Landroid/database/Cursor;)V

    goto :goto_1

    .line 434
    .end local v1    # "mergeColumnVal":Ljava/lang/String;
    .end local v4    # "srcMergeRowPos":Ljava/lang/Integer;
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 442
    .restart local v1    # "mergeColumnVal":Ljava/lang/String;
    .restart local v4    # "srcMergeRowPos":Ljava/lang/Integer;
    :cond_3
    const-string v5, "NautilusContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to move the source cursor to position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 445
    :cond_4
    invoke-static {v2, p0}, Lcom/google/android/music/utils/DbUtils;->addRowToMatrixCursor(Landroid/database/MatrixCursor;Landroid/database/Cursor;)V

    goto :goto_1

    .line 452
    .end local v1    # "mergeColumnVal":Ljava/lang/String;
    .end local v4    # "srcMergeRowPos":Ljava/lang/Integer;
    :cond_5
    return-object v2
.end method

.method public static varargs merge([Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 14
    .param p0, "cursors"    # [Landroid/database/Cursor;

    .prologue
    .line 464
    if-eqz p0, :cond_0

    array-length v9, p0

    if-nez v9, :cond_1

    .line 465
    :cond_0
    const/4 v3, 0x0

    .line 495
    :goto_0
    return-object v3

    .line 466
    :cond_1
    array-length v9, p0

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    .line 467
    const/4 v9, 0x0

    aget-object v3, p0, v9

    goto :goto_0

    .line 470
    :cond_2
    const/4 v9, 0x0

    aget-object v5, p0, v9

    .line 471
    .local v5, "firstCursor":Landroid/database/Cursor;
    invoke-interface {v5}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    .line 474
    .local v1, "cols":[Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 475
    const/4 v2, 0x0

    .line 476
    .local v2, "count":I
    move-object v0, p0

    .local v0, "arr$":[Landroid/database/Cursor;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v8, :cond_4

    aget-object v4, v0, v7

    .line 477
    .local v4, "cursor":Landroid/database/Cursor;
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 478
    int-to-long v10, v2

    const/4 v9, 0x0

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    add-long/2addr v10, v12

    long-to-int v2, v10

    .line 476
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 481
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_4
    new-instance v3, Landroid/database/MatrixCursor;

    invoke-direct {v3, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 482
    .local v3, "countCursor":Landroid/database/MatrixCursor;
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v3, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 484
    .end local v0    # "arr$":[Landroid/database/Cursor;
    .end local v2    # "count":I
    .end local v3    # "countCursor":Landroid/database/MatrixCursor;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :cond_5
    invoke-static {v1}, Lcom/google/android/music/store/ProjectionUtils;->isHasDifferentArtistProjection([Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 485
    const/4 v6, 0x0

    .line 486
    .local v6, "hasDifferent":I
    move-object v0, p0

    .restart local v0    # "arr$":[Landroid/database/Cursor;
    array-length v8, v0

    .restart local v8    # "len$":I
    const/4 v7, 0x0

    .restart local v7    # "i$":I
    :goto_2
    if-ge v7, v8, :cond_7

    aget-object v4, v0, v7

    .line 487
    .restart local v4    # "cursor":Landroid/database/Cursor;
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 488
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    add-int/2addr v6, v9

    .line 486
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 491
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_7
    new-instance v4, Landroid/database/MatrixCursor;

    invoke-direct {v4, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 492
    .local v4, "cursor":Landroid/database/MatrixCursor;
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/Object;

    const/4 v11, 0x0

    if-nez v6, :cond_8

    const/4 v9, 0x0

    :goto_3
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v10, v11

    invoke-virtual {v4, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object v3, v4

    .line 493
    goto :goto_0

    .line 492
    :cond_8
    const/4 v9, 0x1

    goto :goto_3

    .line 495
    .end local v0    # "arr$":[Landroid/database/Cursor;
    .end local v4    # "cursor":Landroid/database/MatrixCursor;
    .end local v6    # "hasDifferent":I
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :cond_9
    new-instance v3, Lcom/google/android/music/store/CustomMergeCursor;

    invoke-direct {v3, p0}, Lcom/google/android/music/store/CustomMergeCursor;-><init>([Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public static query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    .locals 11
    .param p0, "executor"    # Ljava/util/concurrent/ThreadPoolExecutor;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uriType"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "nautilusId"    # Ljava/lang/String;
    .param p5, "projection"    # [Ljava/lang/String;

    .prologue
    .line 309
    new-instance v2, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;-><init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v9

    .line 312
    .local v9, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;>;"
    const/4 v10, 0x0

    .line 314
    .local v10, "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    const-wide/16 v2, 0x1e

    :try_start_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v9, v2, v3, v4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-object v10, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_0
    if-nez v10, :cond_0

    .line 321
    new-instance v10, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    .end local v10    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    new-instance v2, Landroid/database/MatrixCursor;

    move-object/from16 v0, p5

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    const-wide/16 v4, -0x1

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-direct {v10, v2, v4, v5, v3}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;-><init>(Landroid/database/Cursor;JLjava/util/List;)V

    .line 324
    .restart local v10    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    :cond_0
    return-object v10

    .line 315
    :catch_0
    move-exception v8

    .line 317
    .local v8, "e":Ljava/lang/Exception;
    const-string v2, "NautilusContentProvider"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
