.class public Lcom/google/android/music/leanback/bitmap/ExploreRecommendedBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;
.source "ExploreRecommendedBitmapGettersGetter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J
    .param p6, "handler"    # Landroid/os/Handler;

    .prologue
    .line 15
    invoke-direct/range {p0 .. p6}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/os/Handler;)V

    .line 16
    return-void
.end method


# virtual methods
.method public bridge synthetic getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    return-object v0
.end method

.method protected getGroupItemsUri(J)Landroid/net/Uri;
    .locals 1
    .param p1, "groupId"    # J

    .prologue
    .line 25
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedGroupsUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 12
    invoke-super {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    return-void
.end method

.method public bridge synthetic startLoading()V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/ExploreGroupsBitmapGettersGetter;->startLoading()V

    return-void
.end method
