.class public Lcom/google/android/music/ui/MusicPlayHeaderListLayout;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.source "MusicPlayHeaderListLayout.java"


# instance fields
.field protected mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

.field private mLayoutListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

.field private mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field protected mPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mPrevFloatingState:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout$1;-><init>(Lcom/google/android/music/ui/MusicPlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mLayoutListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

    .line 54
    new-instance v0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout$2;-><init>(Lcom/google/android/music/ui/MusicPlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/MusicPlayHeaderListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->updateActionBar()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/MusicPlayHeaderListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->configureHeaderForDownloadOnlyMode()V

    return-void
.end method

.method private configureHeaderForDownloadOnlyMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->lockHeader(Z)V

    .line 128
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->setHeaderShadowMode(I)V

    .line 134
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->unlockHeader()V

    .line 132
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->setHeaderShadowMode(I)V

    goto :goto_0
.end method

.method private updateActionBar()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141
    invoke-virtual {p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->isHeaderFloating()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v1, v3

    .line 145
    .local v1, "curFloating":Z
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPrevFloatingState:Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPrevFloatingState:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-ne v1, v4, :cond_2

    .line 154
    :goto_1
    return-void

    .end local v1    # "curFloating":Z
    :cond_1
    move v1, v2

    .line 141
    goto :goto_0

    .line 150
    .restart local v1    # "curFloating":Z
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPrevFloatingState:Ljava/lang/Boolean;

    if-eqz v4, :cond_3

    move v0, v3

    .line 151
    .local v0, "animate":Z
    :goto_2
    iget-object v2, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/music/ui/BaseActivity;->setActionBarFloating(ZZ)V

    .line 153
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPrevFloatingState:Ljava/lang/Boolean;

    goto :goto_1

    .end local v0    # "animate":Z
    :cond_3
    move v0, v2

    .line 150
    goto :goto_2
.end method


# virtual methods
.method public configureForMusic(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 2
    .param p1, "configurator"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
    .param p2, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p3, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 99
    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {p3}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iput-object p2, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    .line 102
    iput-object p3, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 104
    iget-object v0, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v1, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 106
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 107
    invoke-direct {p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->updateActionBar()V

    .line 108
    invoke-direct {p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->configureHeaderForDownloadOnlyMode()V

    .line 109
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->onFinishInflate()V

    .line 80
    iget-object v0, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mLayoutListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->setOnLayoutChangedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;)V

    .line 81
    return-void
.end method

.method public unregisterSharedPreferenceChangeListener()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    iget-object v1, p0, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 116
    return-void
.end method
