.class Lcom/google/android/music/ui/SDCardSelectorFragment$2;
.super Ljava/lang/Object;
.source "SDCardSelectorFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SDCardSelectorFragment;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 5

    .prologue
    .line 143
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->mLocations:Ljava/util/List;

    .line 145
    iget-object v3, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-virtual {v3}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownUsableLocations()Ljava/util/Collection;

    move-result-object v2

    .line 148
    .local v2, "resultSet":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/cache/CacheLocation;

    .line 149
    .local v1, "location":Lcom/google/android/music/download/cache/CacheLocation;
    iget-object v3, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # invokes: Lcom/google/android/music/ui/SDCardSelectorFragment;->checkLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z
    invoke-static {v3, v1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$100(Lcom/google/android/music/ui/SDCardSelectorFragment;Lcom/google/android/music/download/cache/CacheLocation;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    iget-object v3, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->mLocations:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    .end local v1    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->mLocations:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    iget-object v4, v4, Lcom/google/android/music/ui/SDCardSelectorFragment;->SIZE_SORT_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 156
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->mLocations:Ljava/util/List;

    # setter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$202(Lcom/google/android/music/ui/SDCardSelectorFragment;Ljava/util/List;)Ljava/util/List;

    .line 161
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$2;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # invokes: Lcom/google/android/music/ui/SDCardSelectorFragment;->updateList()V
    invoke-static {v0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$300(Lcom/google/android/music/ui/SDCardSelectorFragment;)V

    .line 162
    return-void
.end method
