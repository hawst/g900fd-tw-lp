.class public Lcom/google/android/music/cloudclient/TrackJson;
.super Lcom/google/api/client/json/GenericJson;
.source "TrackJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/TrackJson$TrackOrigin;,
        Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;,
        Lcom/google/android/music/cloudclient/TrackJson$ImageRef;
    }
.end annotation


# static fields
.field private static final RATINGS_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final TRACK_ORIGIN_NOT_USER_SELECTED:I = 0x4

.field public static final TRACK_TYPE_NAUTILUS_EPHEMERAL:I = 0x7

.field public static final TRACK_TYPE_NAUTILUS_PERSISTENT:I = 0x8

.field public static final TRACK_TYPE_PROMO:I = 0x6

.field public static final TRACK_TYPE_PURCHASED:I = 0x4


# instance fields
.field public mAlbum:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "album"
    .end annotation
.end field

.field public mAlbumArtRef:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtRef"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/TrackJson$ImageRef;",
            ">;"
        }
    .end annotation
.end field

.field public mAlbumArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtist"
    .end annotation
.end field

.field public mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumId"
    .end annotation
.end field

.field public mArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist"
    .end annotation
.end field

.field public mArtistArtRef:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistArtRef"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/TrackJson$ImageRef;",
            ">;"
        }
    .end annotation
.end field

.field public mArtistId:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistId"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mBeatsPerMinute:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "beatsPerMinute"
    .end annotation
.end field

.field public mClientId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "clientId"
    .end annotation
.end field

.field public mComment:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "comment"
    .end annotation
.end field

.field public mComposer:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "composer"
    .end annotation
.end field

.field public mCreationTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "creationTimestamp"
    .end annotation
.end field

.field public mDiscNumber:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "discNumber"
    .end annotation
.end field

.field public mDurationMillis:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "durationMillis"
    .end annotation
.end field

.field public mEstimatedSize:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "estimatedSize"
    .end annotation
.end field

.field public mGenre:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "genre"
    .end annotation
.end field

.field public mIsDeleted:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "deleted"
    .end annotation
.end field

.field public mLastModifiedTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lastModifiedTimestamp"
    .end annotation
.end field

.field public mLastRatingChangeTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lastRatingChangeTimestamp"
    .end annotation
.end field

.field public mNautilusId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "nid"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public mPlayCount:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playCount"
    .end annotation
.end field

.field public mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "primaryVideo"
    .end annotation
.end field

.field public mRating:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "rating"
    .end annotation
.end field

.field public mRemoteId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mStoreId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "storeId"
    .end annotation
.end field

.field public mTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field

.field public mTotalDiscCount:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "totalDiscCount"
    .end annotation
.end field

.field public mTrackAvailableForPurchase:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackAvailableForPurchase"
    .end annotation
.end field

.field public mTrackAvailableForSubscription:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackAvailableForSubscription"
    .end annotation
.end field

.field public mTrackNumber:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackNumber"
    .end annotation
.end field

.field public mTrackOrigin:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackOrigin"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/TrackJson$TrackOrigin;",
            ">;"
        }
    .end annotation
.end field

.field public mTrackType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackType"
    .end annotation
.end field

.field public mYear:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "year"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    .line 53
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "NOT_RATED"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "0"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "ONE_STAR"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "1"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "TWO_STARS"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "2"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "THREE_STARS"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "3"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "FOUR_STARS"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "4"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "FIVE_STARS"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    const-string v1, "5"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 16
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 108
    iput-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mCreationTimestamp:J

    .line 111
    iput-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mLastModifiedTimestamp:J

    .line 132
    iput v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mYear:I

    .line 138
    iput v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackNumber:I

    .line 144
    iput-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDurationMillis:J

    .line 147
    iput v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mBeatsPerMinute:I

    .line 159
    iput-wide v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mEstimatedSize:J

    .line 162
    iput v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTotalDiscCount:I

    .line 165
    iput v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDiscNumber:I

    .line 168
    iput v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackType:I

    return-void
.end method


# virtual methods
.method public getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/TrackJson;->hasLockerId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 314
    :goto_0
    return-object v0

    .line 311
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/TrackJson;->hasNautilusId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    goto :goto_0

    .line 314
    :cond_1
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    goto :goto_0
.end method

.method public getEffectiveRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/TrackJson;->hasLockerId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRemoteId:Ljava/lang/String;

    .line 304
    :goto_0
    return-object v0

    .line 301
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/TrackJson;->hasNautilusId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/TrackJson;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 304
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNormalizedNautilusId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x54

    if-ne v0, v1, :cond_1

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    .line 328
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRatingAsInt()I
    .locals 2

    .prologue
    .line 283
    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRating:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/music/cloudclient/TrackJson;->RATINGS_MAP:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRating:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getTrackType()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackType:I

    return v0
.end method

.method public hasLockerId()Z
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRemoteId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNautilusId()Z
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mNautilusId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRating(I)V
    .locals 1
    .param p1, "rating"    # I

    .prologue
    .line 287
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRating:Ljava/lang/String;

    .line 288
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 238
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 239
    .local v0, "s":Ljava/lang/StringBuffer;
    const-string v1, "; remoteid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; ctime:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mCreationTimestamp:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; mtime:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mLastModifiedTimestamp:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; isDeleted: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mIsDeleted:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; title: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; artist: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtist:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; composer: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mComposer:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; album: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbum:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; albumartist: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; year: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mYear:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; comment: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; track num: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackNumber:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; genre: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mGenre:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; duration: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDurationMillis:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; albumartref: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtRef:Ljava/util/List;

    if-nez v1, :cond_1

    move-object v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; artistartref: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistArtRef:Ljava/util/List;

    if-nez v1, :cond_2

    move-object v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; bpm: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mBeatsPerMinute:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; playCount: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPlayCount:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; estimatedSize: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mEstimatedSize:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; discNumber:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDiscNumber:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; totalDiscCount:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTotalDiscCount:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; trackType:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackType:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; rating:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRating:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; storeId:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mStoreId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; albumId:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; trackOrigin:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackOrigin:Ljava/util/List;

    if-nez v1, :cond_3

    move-object v1, v2

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; clientId:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mClientId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; artistId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistId:Ljava/util/List;

    if-nez v1, :cond_4

    move-object v1, v2

    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; trackAvailableForPurchase: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForPurchase:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; trackAvailableForSubscription: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForSubscription:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; ratingsTimestamp: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mLastRatingChangeTimestamp:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; videoId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    if-nez v1, :cond_5

    move-object v1, v2

    :goto_4
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, "; videoThumbnailUrl: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_0
    :goto_5
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 278
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 239
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtRef:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistArtRef:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackOrigin:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_4
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistId:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mId:Ljava/lang/String;

    goto :goto_4

    :cond_6
    iget-object v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v2, v1, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    goto :goto_5
.end method

.method public wipeAllFields()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 203
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRemoteId:Ljava/lang/String;

    .line 204
    iput-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mCreationTimestamp:J

    .line 205
    iput-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mLastModifiedTimestamp:J

    .line 206
    iput-boolean v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mIsDeleted:Z

    .line 207
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTitle:Ljava/lang/String;

    .line 208
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtist:Ljava/lang/String;

    .line 209
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mComposer:Ljava/lang/String;

    .line 210
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbum:Ljava/lang/String;

    .line 211
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtist:Ljava/lang/String;

    .line 212
    iput v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mYear:I

    .line 213
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mComment:Ljava/lang/String;

    .line 214
    iput v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackNumber:I

    .line 215
    iput-wide v6, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDurationMillis:J

    .line 216
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtRef:Ljava/util/List;

    .line 217
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistArtRef:Ljava/util/List;

    .line 218
    iput v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mBeatsPerMinute:I

    .line 219
    iput v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPlayCount:I

    .line 220
    iput-wide v6, p0, Lcom/google/android/music/cloudclient/TrackJson;->mEstimatedSize:J

    .line 221
    iput v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTotalDiscCount:I

    .line 222
    iput v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mDiscNumber:I

    .line 223
    iput v1, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackType:I

    .line 224
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mRating:Ljava/lang/String;

    .line 225
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mStoreId:Ljava/lang/String;

    .line 226
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumId:Ljava/lang/String;

    .line 227
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackOrigin:Ljava/util/List;

    .line 228
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mClientId:Ljava/lang/String;

    .line 229
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistId:Ljava/util/List;

    .line 230
    iput-boolean v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForSubscription:Z

    .line 231
    iput-boolean v2, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForPurchase:Z

    .line 232
    iput-wide v4, p0, Lcom/google/android/music/cloudclient/TrackJson;->mLastRatingChangeTimestamp:J

    .line 233
    iput-object v0, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    .line 234
    return-void
.end method
