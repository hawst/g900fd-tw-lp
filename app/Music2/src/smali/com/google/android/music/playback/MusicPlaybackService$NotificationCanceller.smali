.class Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;
.super Landroid/os/Handler;
.source "MusicPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotificationCanceller"
.end annotation


# instance fields
.field private final mServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/playback/MusicPlaybackService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 2278
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2279
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;->mServiceRef:Ljava/lang/ref/WeakReference;

    .line 2280
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 18
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2284
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v14}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/music/playback/MusicPlaybackService;

    .line 2285
    .local v11, "service":Lcom/google/android/music/playback/MusicPlaybackService;
    if-nez v11, :cond_0

    .line 2305
    :goto_0
    return-void

    .line 2289
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "cancel_notification"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 2290
    .local v12, "sent":J
    const-wide/32 v8, 0x36ee80

    .line 2291
    .local v8, "hour":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2293
    .local v2, "current":J
    const-wide/32 v14, 0x5265c00

    div-long v6, v12, v14

    .line 2294
    .local v6, "dayStart":J
    const-wide/32 v14, 0x5265c00

    div-long v4, v2, v14

    .line 2298
    .local v4, "dayEnd":J
    cmp-long v14, v6, v4

    if-nez v14, :cond_1

    const-wide/32 v14, 0x5265c00

    rem-long v14, v12, v14

    cmp-long v14, v14, v8

    if-lez v14, :cond_1

    .line 2299
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;->obtainMessage()Landroid/os/Message;

    move-result-object v10

    .line 2300
    .local v10, "newMsg":Landroid/os/Message;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 2301
    const-wide/16 v14, 0x1

    add-long/2addr v14, v4

    const-wide/32 v16, 0x5265c00

    mul-long v14, v14, v16

    sub-long/2addr v14, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v14, v15}, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 2303
    .end local v10    # "newMsg":Landroid/os/Message;
    :cond_1
    const/4 v14, 0x1

    invoke-virtual {v11, v14}, Lcom/google/android/music/playback/MusicPlaybackService;->stopForegroundService(Z)V

    goto :goto_0
.end method
