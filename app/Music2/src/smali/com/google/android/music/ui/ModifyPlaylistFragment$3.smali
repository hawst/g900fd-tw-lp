.class Lcom/google/android/music/ui/ModifyPlaylistFragment$3;
.super Ljava/lang/Object;
.source "ModifyPlaylistFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ModifyPlaylistFragment;->handleCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private volatile mPlaylistId:J

.field private volatile mSongsAddedCount:I

.field final synthetic this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$description:Ljava/lang/String;

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$shareStateArg:I


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 289
    iput-object p1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$name:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$description:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$shareStateArg:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mSongsAddedCount:I

    .line 292
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 296
    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$name:Ljava/lang/String;

    # invokes: Lcom/google/android/music/ui/ModifyPlaylistFragment;->idForplaylist(Landroid/content/Context;Ljava/lang/String;)I
    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$700(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    .line 297
    iget-wide v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_2

    .line 298
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    .line 307
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$800(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_1

    .line 308
    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$800(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    iget-wide v4, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/music/medialist/SongList;->appendToPlaylist(Landroid/content/Context;J)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mSongsAddedCount:I

    .line 312
    :cond_1
    return-void

    .line 300
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$description:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$shareStateArg:I

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContent$Playlists;->createPlaylist(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    .line 302
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 316
    iget-wide v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mPlaylistId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mShareSwitch:Landroid/widget/CompoundButton;
    invoke-static {v2}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$900(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Landroid/widget/CompoundButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 317
    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    const v3, 0x7f0b034f

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 328
    :goto_0
    return-void

    .line 319
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$800(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    if-nez v2, :cond_1

    .line 320
    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    const v3, 0x7f0b00b8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 322
    .local v1, "toastformat":Ljava/lang/String;
    new-array v2, v6, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$name:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 323
    .local v0, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    invoke-static {v2, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 325
    .end local v0    # "message":Ljava/lang/String;
    .end local v1    # "toastformat":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$context:Landroid/content/Context;

    iget v3, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->mSongsAddedCount:I

    iget-object v4, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$3;->val$name:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/music/utils/MusicUtils;->showSongsAddedToPlaylistToast(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method
