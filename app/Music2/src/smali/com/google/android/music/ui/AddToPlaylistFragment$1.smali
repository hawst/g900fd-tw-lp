.class Lcom/google/android/music/ui/AddToPlaylistFragment$1;
.super Ljava/lang/Object;
.source "AddToPlaylistFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/AddToPlaylistFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/AddToPlaylistFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/AddToPlaylistFragment;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/AddToPlaylistFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "itemId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v8, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/AddToPlaylistFragment;

    invoke-virtual {v8}, Lcom/google/android/music/ui/AddToPlaylistFragment;->dismiss()V

    .line 62
    iget-object v8, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/AddToPlaylistFragment;

    invoke-virtual {v8, p3}, Lcom/google/android/music/ui/AddToPlaylistFragment;->getAdapterItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    .line 64
    .local v5, "item":Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;
    iget-wide v8, v5, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;->id:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 65
    iget-object v8, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/AddToPlaylistFragment;

    invoke-virtual {v8}, Lcom/google/android/music/ui/AddToPlaylistFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 66
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    new-instance v4, Lcom/google/android/music/ui/ModifyPlaylistFragment;

    invoke-direct {v4}, Lcom/google/android/music/ui/ModifyPlaylistFragment;-><init>()V

    .line 67
    .local v4, "fragment":Landroid/support/v4/app/DialogFragment;
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    .line 68
    .local v6, "manager":Lcom/google/android/music/ui/UIStateManager;
    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    .line 69
    .local v7, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    iget-object v8, p0, Lcom/google/android/music/ui/AddToPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/AddToPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/AddToPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;
    invoke-static {v8}, Lcom/google/android/music/ui/AddToPlaylistFragment;->access$000(Lcom/google/android/music/ui/AddToPlaylistFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    invoke-static {v9, v8}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->makeCreateArgs(Lcom/google/android/music/medialist/SongList;Z)Landroid/os/Bundle;

    move-result-object v1

    .line 71
    .local v1, "args":Landroid/os/Bundle;
    invoke-virtual {v4, v1}, Landroid/support/v4/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 73
    .local v3, "fm":Landroid/support/v4/app/FragmentManager;
    const-string v8, "ModifyPlaylistFragment"

    invoke-virtual {v4, v3, v8}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 93
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v3    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v4    # "fragment":Landroid/support/v4/app/DialogFragment;
    .end local v6    # "manager":Lcom/google/android/music/ui/UIStateManager;
    .end local v7    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :goto_1
    return-void

    .line 69
    .restart local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .restart local v4    # "fragment":Landroid/support/v4/app/DialogFragment;
    .restart local v6    # "manager":Lcom/google/android/music/ui/UIStateManager;
    .restart local v7    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 76
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v4    # "fragment":Landroid/support/v4/app/DialogFragment;
    .end local v6    # "manager":Lcom/google/android/music/ui/UIStateManager;
    .end local v7    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 77
    .local v2, "context":Landroid/content/Context;
    new-instance v8, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;

    invoke-direct {v8, p0, v2, v5}, Lcom/google/android/music/ui/AddToPlaylistFragment$1$1;-><init>(Lcom/google/android/music/ui/AddToPlaylistFragment$1;Landroid/content/Context;Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;)V

    invoke-static {v8}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_1
.end method
