.class public Lcom/google/android/music/ui/NowPlayingArtPageFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "NowPlayingArtPageFragment.java"


# static fields
.field private static VIDEO_WIDGET_STATE_END:I

.field private static VIDEO_WIDGET_STATE_START:I


# instance fields
.field private mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mAlbumId:J

.field private mAlbumName:Ljava/lang/String;

.field private mArtUrl:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mCurrentAnimation:Landroid/animation/ObjectAnimator;

.field private mIsCurrentlyActive:Z

.field private mIsViewCreated:Z

.field private mMusicId:J

.field private mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

.field private mPanLeftTopListener:Landroid/animation/Animator$AnimatorListener;

.field private mPanRightBottomListener:Landroid/animation/Animator$AnimatorListener;

.field private mPanningLeftOrTop:Z

.field private mQueuePosition:I

.field private mRootView:Landroid/view/View;

.field private mShortAnimationDuration:I

.field private mStartPanningRunnable:Ljava/lang/Runnable;

.field private mStarted:Z

.field private mStatusListener:Landroid/content/BroadcastReceiver;

.field private mTitle:Ljava/lang/String;

.field private mVideoId:Ljava/lang/String;

.field private mVideoThumbnail:Ljava/lang/String;

.field private mVideoWidgetState:I

.field private mYouTubeClickListener:Landroid/view/View$OnClickListener;

.field private mYouTubeOverlay:Landroid/widget/ImageView;

.field private mYouTubeWidgetIcon:Landroid/widget/ImageView;

.field private mYouTubeWidgetIconGray:Landroid/widget/ImageView;

.field private mYouTubeWidgetText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    sput v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_START:I

    .line 63
    const/4 v0, 0x2

    sput v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_END:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mQueuePosition:I

    .line 89
    sget v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_START:I

    iput v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoWidgetState:I

    .line 94
    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsViewCreated:Z

    .line 99
    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mPanningLeftOrTop:Z

    .line 103
    new-instance v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$1;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mStartPanningRunnable:Ljava/lang/Runnable;

    .line 121
    new-instance v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$2;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeClickListener:Landroid/view/View$OnClickListener;

    .line 136
    new-instance v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    .line 158
    new-instance v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$4;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->stopPanning()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->startPanningIfNeeded()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/NowPlayingArtPageFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/android/music/ui/NowPlayingArtPageFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoWidgetState:I

    return p1
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_START:I

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeWidgets()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->scheduleYouTubeAnimation()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/ui/NowPlayingArtPageFragment;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->startYouTubeAnimation(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->isDrawerExpanded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mPanningLeftOrTop:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->panLeftOrTop()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->panRightOrBottom()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mQueuePosition:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateArtVisibility()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeVisibility()V

    return-void
.end method

.method private clearArtwork()V
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->clearArtwork()V

    .line 475
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->stopPanning()V

    .line 476
    return-void
.end method

.method private doPan(FFFLjava/lang/String;Landroid/animation/Animator$AnimatorListener;)V
    .locals 5
    .param p1, "currentPos"    # F
    .param p2, "targetPos"    # F
    .param p3, "fullTravel"    # F
    .param p4, "propToAnimate"    # Ljava/lang/String;
    .param p5, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 598
    const v2, 0x46ea6000    # 30000.0f

    sub-float v3, p2, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v3, p3

    mul-float/2addr v2, v3

    float-to-long v0, v2

    .line 607
    .local v0, "duration":J
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput p1, v3, v4

    const/4 v4, 0x1

    aput p2, v3, v4

    invoke-static {v2, p4, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    .line 608
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 609
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 610
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, p5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 611
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 612
    return-void
.end method

.method private findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private initializeView()V
    .locals 5

    .prologue
    .line 307
    const v0, 0x7f0e01c4

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 308
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0e01cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetText:Landroid/widget/TextView;

    .line 309
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0e01cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIcon:Landroid/widget/ImageView;

    .line 310
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0e01ce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIconGray:Landroid/widget/ImageView;

    .line 311
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0e01cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeOverlay:Landroid/widget/ImageView;

    .line 313
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    new-instance v1, Lcom/google/android/music/ui/NowPlayingArtPageFragment$5;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$5;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    new-instance v1, Lcom/google/android/music/ui/NowPlayingArtPageFragment$6;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$6;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    const v1, 0x7f0b0352

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mTitle:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mArtistName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIconGray:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mShortAnimationDuration:I

    .line 340
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeWidgets()V

    .line 341
    return-void
.end method

.method private isDrawerExpanded()Z
    .locals 3

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 480
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExpandingState()Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/NowPlayingArtPageFragment;
    .locals 3
    .param p0, "position"    # I
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "albumName"    # Ljava/lang/String;
    .param p3, "artistName"    # Ljava/lang/String;
    .param p4, "albumId"    # J
    .param p6, "artUrl"    # Ljava/lang/String;
    .param p7, "musicId"    # J
    .param p9, "videoId"    # Ljava/lang/String;
    .param p10, "videoThumbnail"    # Ljava/lang/String;

    .prologue
    .line 187
    new-instance v1, Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;-><init>()V

    .line 188
    .local v1, "fragment":Lcom/google/android/music/ui/NowPlayingArtPageFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 189
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "position"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 190
    const-string v2, "title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v2, "albumName"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v2, "artistName"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v2, "albumId"

    invoke-virtual {v0, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 194
    const-string v2, "artUrl"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v2, "musicId"

    invoke-virtual {v0, v2, p7, p8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 196
    const-string v2, "videoId"

    invoke-virtual {v0, v2, p9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string v2, "videoThumbnail"

    invoke-virtual {v0, v2, p10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 199
    return-object v1
.end method

.method private panLeftOrTop()V
    .locals 7

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    .line 550
    .local v6, "context":Landroid/content/Context;
    if-nez v6, :cond_0

    .line 569
    :goto_0
    return-void

    .line 556
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getTop()I

    move-result v0

    neg-int v0, v0

    int-to-float v2, v0

    .line 558
    .local v2, "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getY()F

    move-result v0

    add-float v1, v0, v2

    .line 559
    .local v1, "currentPos":F
    const-string v4, "translationY"

    .line 560
    .local v4, "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getTop()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .line 568
    .local v3, "fullTravel":F
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mPanLeftTopListener:Landroid/animation/Animator$AnimatorListener;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->doPan(FFFLjava/lang/String;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 562
    .end local v1    # "currentPos":F
    .end local v2    # "targetPos":F
    .end local v3    # "fullTravel":F
    .end local v4    # "propToAnimate":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getLeft()I

    move-result v0

    neg-int v0, v0

    int-to-float v2, v0

    .line 563
    .restart local v2    # "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getX()F

    move-result v0

    add-float v1, v0, v2

    .line 564
    .restart local v1    # "currentPos":F
    const-string v4, "translationX"

    .line 565
    .restart local v4    # "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getLeft()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .restart local v3    # "fullTravel":F
    goto :goto_1
.end method

.method private panRightOrBottom()V
    .locals 7

    .prologue
    .line 572
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    .line 573
    .local v6, "context":Landroid/content/Context;
    if-nez v6, :cond_0

    .line 592
    :goto_0
    return-void

    .line 579
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 580
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getTop()I

    move-result v0

    int-to-float v2, v0

    .line 581
    .local v2, "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getY()F

    move-result v0

    sub-float v1, v0, v2

    .line 582
    .local v1, "currentPos":F
    const-string v4, "translationY"

    .line 583
    .local v4, "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getTop()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .line 591
    .local v3, "fullTravel":F
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mPanRightBottomListener:Landroid/animation/Animator$AnimatorListener;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->doPan(FFFLjava/lang/String;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 585
    .end local v1    # "currentPos":F
    .end local v2    # "targetPos":F
    .end local v3    # "fullTravel":F
    .end local v4    # "propToAnimate":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getLeft()I

    move-result v0

    int-to-float v2, v0

    .line 586
    .restart local v2    # "targetPos":F
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getX()F

    move-result v0

    sub-float v1, v0, v2

    .line 587
    .restart local v1    # "currentPos":F
    const-string v4, "translationX"

    .line 588
    .restart local v4    # "propToAnimate":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getLeft()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .restart local v3    # "fullTravel":F
    goto :goto_1
.end method

.method private scheduleYouTubeAnimation()V
    .locals 5

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->shouldDoYouTubeAnimation()Z

    move-result v2

    if-nez v2, :cond_0

    .line 399
    :goto_0
    return-void

    .line 384
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 385
    .local v1, "showViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIconGray:Landroid/widget/ImageView;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 388
    .local v0, "hideViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeOverlay:Landroid/widget/ImageView;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIcon:Landroid/widget/ImageView;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetText:Landroid/widget/TextView;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    new-instance v2, Lcom/google/android/music/ui/NowPlayingArtPageFragment$7;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$7;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/16 v4, 0x7d0

    invoke-static {v2, v3, v4}, Lcom/google/android/music/utils/MusicUtils;->runDelayedOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;I)Landroid/os/Handler;

    goto :goto_0
.end method

.method private shouldDoYouTubeAnimation()Z
    .locals 2

    .prologue
    .line 369
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombMr1OrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoWidgetState:I

    sget v1, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_START:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsViewCreated:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startPanningIfNeeded()V
    .locals 2

    .prologue
    .line 488
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->startPanningIfNeeded(J)V

    .line 489
    return-void
.end method

.method private startPanningIfNeeded(J)V
    .locals 0
    .param p1, "delay"    # J
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 536
    return-void
.end method

.method private startYouTubeAnimation(Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "showViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p2, "hideViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v6, 0x0

    .line 408
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->shouldDoYouTubeAnimation()Z

    move-result v2

    if-nez v2, :cond_1

    .line 437
    :cond_0
    return-void

    .line 412
    :cond_1
    sget v2, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_END:I

    iput v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoWidgetState:I

    .line 414
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 415
    .local v1, "v":Landroid/view/View;
    invoke-static {v1, v6}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 416
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 419
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 420
    .restart local v1    # "v":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mShortAnimationDuration:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 426
    .end local v1    # "v":Landroid/view/View;
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 427
    .restart local v1    # "v":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mShortAnimationDuration:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lcom/google/android/music/ui/NowPlayingArtPageFragment$8;

    invoke-direct {v3, p0, v1}, Lcom/google/android/music/ui/NowPlayingArtPageFragment$8;-><init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_2
.end method

.method private stopPanning()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 543
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 544
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    .line 546
    :cond_0
    return-void
.end method

.method private updateArtVisibility()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 445
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mStarted:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->isDrawerExpanded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mArtUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mArtUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    .line 463
    :goto_0
    return-void

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbum:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-wide v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbumId:J

    invoke-virtual {v0, v2, v3, v1, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 461
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->clearArtwork()V

    goto :goto_0
.end method

.method private updateYouTubeVisibility()V
    .locals 1

    .prologue
    .line 466
    sget v0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_START:I

    iput v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoWidgetState:I

    .line 467
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeWidgets()V

    .line 468
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->isDrawerExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->scheduleYouTubeAnimation()V

    .line 471
    :cond_0
    return-void
.end method

.method private updateYouTubeWidgets()V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 344
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetText:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 345
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIcon:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 346
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIconGray:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 347
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeOverlay:Landroid/widget/ImageView;

    const v1, 0x3e99999a    # 0.3f

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 348
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIconGray:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeOverlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 366
    :goto_0
    return-void

    .line 354
    :cond_0
    iget v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoWidgetState:I

    sget v1, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_START:I

    if-ne v0, v1, :cond_1

    .line 355
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeOverlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIconGray:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 360
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeOverlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mYouTubeWidgetIconGray:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 205
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/BaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 206
    const v3, 0x7f04010e

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mRootView:Landroid/view/View;

    .line 208
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 209
    .local v2, "playbackStatusFilter":Landroid/content/IntentFilter;
    const-string v3, "com.android.music.playstatechanged"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 210
    const-string v3, "com.android.music.metachanged"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 211
    const-string v3, "com.android.music.asyncopencomplete"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 212
    const-string v3, "com.android.music.asyncopenstart"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 213
    const-string v3, "com.android.music.playbackfailed"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 216
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 217
    .local v1, "drawerStateFilter":Landroid/content/IntentFilter;
    const-string v3, "com.google.android.music.nowplaying.DRAWER_STATE_CHANGED_ACTION"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 222
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "position"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mQueuePosition:I

    .line 223
    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mTitle:Ljava/lang/String;

    .line 224
    const-string v3, "albumName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbumName:Ljava/lang/String;

    .line 225
    const-string v3, "artistName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mArtistName:Ljava/lang/String;

    .line 226
    const-string v3, "albumId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mAlbumId:J

    .line 227
    const-string v3, "artUrl"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mArtUrl:Ljava/lang/String;

    .line 228
    const-string v3, "musicId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mMusicId:J

    .line 229
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/preferences/MusicPreferences;->isYouTubeAvailable(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 230
    const-string v3, "videoId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoId:Ljava/lang/String;

    .line 231
    const-string v3, "videoThumbnail"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoThumbnail:Ljava/lang/String;

    .line 233
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->initializeView()V

    .line 235
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsViewCreated:Z

    .line 236
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mRootView:Landroid/view/View;

    return-object v3
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsViewCreated:Z

    .line 300
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 301
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 303
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 304
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 272
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onPause()V

    .line 276
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z

    if-nez v0, :cond_0

    .line 279
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->clearArtwork()V

    .line 281
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->stopPanning()V

    .line 282
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onResume()V

    .line 258
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getQueuePosition()I

    move-result v1

    iget v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mQueuePosition:I

    if-ne v1, v2, :cond_0

    .line 260
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z

    .line 261
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateArtVisibility()V

    .line 262
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeVisibility()V

    .line 263
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->startPanningIfNeeded()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 265
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "NowPlayingArtPageFragment"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 241
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStart()V

    .line 245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mStarted:Z

    .line 246
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateArtVisibility()V

    .line 247
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeVisibility()V

    .line 248
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 286
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStop()V

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mStarted:Z

    .line 291
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateArtVisibility()V

    .line 292
    return-void
.end method
