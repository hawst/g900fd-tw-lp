.class Lcom/google/android/music/ui/ManageDownloadsFragment$2;
.super Ljava/lang/Object;
.source "ManageDownloadsFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ManageDownloadsFragment;->updateProgress()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private volatile mFreeSize:J

.field private volatile mNotDownloaded:J

.field private volatile mProgress:I

.field private volatile mSecondaryProgress:I

.field final synthetic this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ManageDownloadsFragment;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 278
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    iput v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mProgress:I

    .line 280
    iput v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mSecondaryProgress:I

    .line 282
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mNotDownloaded:J

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 18

    .prologue
    .line 287
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/music/ui/UIStateManager;->getCacheManager()Lcom/google/android/music/download/cache/ICacheManager;

    move-result-object v2

    .line 288
    .local v2, "cacheManager":Lcom/google/android/music/download/cache/ICacheManager;
    if-eqz v2, :cond_1

    .line 290
    :try_start_0
    invoke-interface {v2}, Lcom/google/android/music/download/cache/ICacheManager;->getStorageSizes()Lcom/google/android/music/download/cache/StorageSizes;

    move-result-object v5

    .line 291
    .local v5, "storageSizes":Lcom/google/android/music/download/cache/StorageSizes;
    invoke-virtual {v5}, Lcom/google/android/music/download/cache/StorageSizes;->getTotalBytes()J

    move-result-wide v12

    long-to-double v10, v12

    .line 293
    .local v10, "totalSize":D
    invoke-virtual {v5}, Lcom/google/android/music/download/cache/StorageSizes;->getFreeBytes()J

    move-result-wide v12

    double-to-long v14, v10

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mFreeSize:J

    .line 294
    invoke-virtual {v5}, Lcom/google/android/music/download/cache/StorageSizes;->getMusicBytes()J

    move-result-wide v12

    long-to-double v12, v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mFreeSize:J

    long-to-double v14, v14

    sub-double v14, v10, v14

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    .line 297
    .local v6, "musicSize":D
    sub-double v12, v10, v6

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mFreeSize:J

    long-to-double v14, v14

    sub-double v8, v12, v14

    .line 298
    .local v8, "otherSize":D
    div-double v12, v8, v10

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    mul-double/2addr v12, v14

    double-to-int v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mProgress:I

    .line 299
    const/4 v4, 0x0

    .line 300
    .local v4, "musicProgress":I
    const-wide/16 v12, 0x0

    cmpl-double v12, v6, v12

    if-lez v12, :cond_0

    .line 301
    const/4 v12, 0x1

    div-double v14, v6, v10

    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    mul-double v14, v14, v16

    double-to-int v13, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 303
    :cond_0
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mProgress:I

    add-int/2addr v12, v4

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mSecondaryProgress:I

    .line 304
    invoke-virtual {v5}, Lcom/google/android/music/download/cache/StorageSizes;->getNotDownloadedBytes()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mNotDownloaded:J

    .line 306
    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$300()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 307
    const-string v12, "ManageDownloads"

    const-string v13, "total=%.2f free=%d music=%.2f other=%.2f progress=%d secondaryProgress=%d"

    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mFreeSize:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mProgress:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mSecondaryProgress:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    .end local v4    # "musicProgress":I
    .end local v5    # "storageSizes":Lcom/google/android/music/download/cache/StorageSizes;
    .end local v6    # "musicSize":D
    .end local v8    # "otherSize":D
    .end local v10    # "totalSize":D
    :cond_1
    :goto_0
    return-void

    .line 312
    :catch_0
    move-exception v3

    .line 313
    .local v3, "e":Landroid/os/RemoteException;
    const-string v12, "ManageDownloads"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Failed to get cache sizes: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v3}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 321
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 323
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # invokes: Lcom/google/android/music/ui/ManageDownloadsFragment;->showMenuInActionBar()Z
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$400(Lcom/google/android/music/ui/ManageDownloadsFragment;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 328
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$500(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 340
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$700(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 341
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$700(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;

    move-result-object v4

    iget v5, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mProgress:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 342
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$700(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;

    move-result-object v4

    iget v5, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mSecondaryProgress:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 345
    :cond_3
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBarEmptyScreen:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$800(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 346
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBarEmptyScreen:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$800(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;

    move-result-object v4

    iget v5, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mProgress:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 347
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBarEmptyScreen:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$800(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;

    move-result-object v4

    iget v5, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mSecondaryProgress:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 349
    :cond_4
    invoke-static {v0}, Lcom/google/android/music/utils/IOUtils;->getSizeUnitStrings(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v3

    .line 350
    .local v3, "units":[Ljava/lang/String;
    iget-wide v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mFreeSize:J

    invoke-static {v4, v5, v3}, Lcom/google/android/music/utils/IOUtils;->readableFileSize(J[Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 351
    .local v1, "free":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    const v5, 0x7f0b0369

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v7, v6, v9

    iget-object v7, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 353
    .local v2, "freeSpaceString":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextView:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$900(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 355
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextView:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$900(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    :cond_5
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextViewEmptyScreen:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1000(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 358
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextViewEmptyScreen:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1000(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    :cond_6
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    invoke-virtual {v4, v8}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setListShown(Z)V

    goto/16 :goto_0

    .line 329
    .end local v1    # "free":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "freeSpaceString":Ljava/lang/String;
    .end local v3    # "units":[Ljava/lang/String;
    :cond_7
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsDownloadButtonUpdated:Z
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$600(Lcom/google/android/music/ui/ManageDownloadsFragment;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 330
    iget-wide v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->mNotDownloaded:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_8

    .line 331
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$500(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 337
    :goto_2
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # setter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsDownloadButtonUpdated:Z
    invoke-static {v4, v8}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$602(Lcom/google/android/music/ui/ManageDownloadsFragment;Z)Z

    goto/16 :goto_1

    .line 334
    :cond_8
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;
    invoke-static {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$500(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2
.end method
