.class final Lcom/google/android/music/utils/BugReporter$1;
.super Ljava/lang/Object;
.source "BugReporter.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/BugReporter;->launchGoogleFeedback(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/music/utils/BugReporter$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 64
    :try_start_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 65
    .local v1, "parcel":Landroid/os/Parcel;
    iget-object v3, p0, Lcom/google/android/music/utils/BugReporter$1;->val$activity:Landroid/app/Activity;

    # invokes: Lcom/google/android/music/utils/BugReporter;->getCurrentScreenshot(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/google/android/music/utils/BugReporter;->access$000(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 66
    .local v2, "screenshot":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 67
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    .line 69
    :cond_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {p2, v3, v1, v4, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    iget-object v3, p0, Lcom/google/android/music/utils/BugReporter$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v3, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 75
    .end local v1    # "parcel":Landroid/os/Parcel;
    .end local v2    # "screenshot":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "BugReporter"

    const-string v4, "Error connecting to bug report service"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    iget-object v3, p0, Lcom/google/android/music/utils/BugReporter$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v3, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/music/utils/BugReporter$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v4, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    throw v3
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 79
    return-void
.end method
