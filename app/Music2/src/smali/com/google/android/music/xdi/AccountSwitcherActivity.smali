.class public Lcom/google/android/music/xdi/AccountSwitcherActivity;
.super Landroid/app/Activity;
.source "AccountSwitcherActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 47
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 48
    const/4 v4, -0x1

    if-ne p2, v4, :cond_2

    const/4 v4, 0x1

    if-ne p1, v4, :cond_2

    .line 49
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 51
    .local v1, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 52
    .local v0, "currentAccount":Landroid/accounts/Account;
    const-string v4, "authAccount"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, "selectedAccountString":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v0, :cond_0

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 56
    :cond_0
    invoke-virtual {v1, v3}, Lcom/google/android/music/preferences/MusicPreferences;->resolveAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 57
    .local v2, "selectedAccount":Landroid/accounts/Account;
    if-eqz v2, :cond_3

    .line 58
    invoke-virtual {v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamingAccount(Landroid/accounts/Account;)V

    .line 59
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->notifyChangeToPano(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    .end local v2    # "selectedAccount":Landroid/accounts/Account;
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 69
    .end local v0    # "currentAccount":Landroid/accounts/Account;
    .end local v1    # "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v3    # "selectedAccountString":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/xdi/AccountSwitcherActivity;->finish()V

    .line 70
    return-void

    .line 61
    .restart local v0    # "currentAccount":Landroid/accounts/Account;
    .restart local v1    # "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v2    # "selectedAccount":Landroid/accounts/Account;
    .restart local v3    # "selectedAccountString":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v4, "MusicXdi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not resolve account for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 66
    .end local v0    # "currentAccount":Landroid/accounts/Account;
    .end local v2    # "selectedAccount":Landroid/accounts/Account;
    .end local v3    # "selectedAccountString":Ljava/lang/String;
    :catchall_0
    move-exception v4

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 34
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 37
    .local v1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getAllAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 38
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->getSelectedAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    new-array v2, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "com.google"

    aput-object v6, v2, v5

    const-string v5, "mail"

    move-object v6, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    .line 42
    .local v8, "picker":Landroid/content/Intent;
    invoke-virtual {p0, v8, v3}, Lcom/google/android/music/xdi/AccountSwitcherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 43
    return-void
.end method
