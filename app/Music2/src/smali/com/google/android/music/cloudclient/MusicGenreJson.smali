.class public Lcom/google/android/music/cloudclient/MusicGenreJson;
.super Lcom/google/api/client/json/GenericJson;
.source "MusicGenreJson.java"


# instance fields
.field public mChildren:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "children"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mImages:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "images"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/ImageRefJson;",
            ">;"
        }
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field

.field public mParentId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "parentId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
