.class public Lcom/google/android/music/art/StaticArt;
.super Ljava/lang/Object;
.source "StaticArt.java"


# static fields
.field private static final LOGV:Z

.field private static final sLoadedImages:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ALBUM_ART:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/StaticArt;->LOGV:Z

    .line 47
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/music/art/StaticArt;->sLoadedImages:Landroid/util/SparseArray;

    return-void
.end method

.method public static getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "result":Landroid/graphics/Bitmap;
    sparse-switch p1, :sswitch_data_0

    .line 96
    sget-boolean v1, Lcom/google/android/music/art/StaticArt;->LOGV:Z

    if-eqz v1, :cond_0

    .line 97
    const-string v1, "StaticArt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "StaticArt.getArt: Unknown key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    move-object v1, v0

    .line 100
    :goto_1
    return-object v1

    .line 58
    :sswitch_0
    const/4 v1, 0x0

    goto :goto_1

    .line 60
    :sswitch_1
    const/16 v1, 0x65

    const v2, 0x7f02003c

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 62
    goto :goto_0

    .line 64
    :sswitch_2
    const/16 v1, 0x66

    const v2, 0x7f02003f

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 66
    goto :goto_0

    .line 68
    :sswitch_3
    const/16 v1, 0x67

    const v2, 0x7f02013a

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 70
    goto :goto_0

    .line 72
    :sswitch_4
    const/16 v1, 0x68

    const v2, 0x7f020146

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    goto :goto_0

    .line 76
    :sswitch_5
    const/16 v1, 0xc9

    const v2, 0x7f02003a

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 78
    goto :goto_0

    .line 80
    :sswitch_6
    const/16 v1, 0xca

    const v2, 0x7f02017a

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 82
    goto :goto_0

    .line 84
    :sswitch_7
    const/16 v1, 0x12e

    const v2, 0x7f0200d6

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 86
    goto :goto_0

    .line 88
    :sswitch_8
    const/16 v1, 0x12d

    const v2, 0x7f0200d5

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 90
    goto :goto_0

    .line 92
    :sswitch_9
    const/16 v1, 0x12f

    const v2, 0x7f020100

    invoke-static {p0, v1, v2}, Lcom/google/android/music/art/StaticArt;->loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 94
    goto :goto_0

    .line 56
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0x67 -> :sswitch_3
        0x68 -> :sswitch_4
        0xc9 -> :sswitch_5
        0xca -> :sswitch_6
        0x12d -> :sswitch_8
        0x12e -> :sswitch_7
        0x12f -> :sswitch_9
    .end sparse-switch
.end method

.method private static getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceId"    # I

    .prologue
    .line 124
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 125
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 126
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private static loadImageIfNeeded(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # I
    .param p2, "drawableId"    # I

    .prologue
    .line 105
    sget-object v2, Lcom/google/android/music/art/StaticArt;->sLoadedImages:Landroid/util/SparseArray;

    monitor-enter v2

    .line 106
    :try_start_0
    sget-object v1, Lcom/google/android/music/art/StaticArt;->sLoadedImages:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 107
    .local v0, "result":Landroid/graphics/Bitmap;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    :cond_0
    invoke-static {p0, p2}, Lcom/google/android/music/art/StaticArt;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 112
    sget-object v2, Lcom/google/android/music/art/StaticArt;->sLoadedImages:Landroid/util/SparseArray;

    monitor-enter v2

    .line 113
    :try_start_1
    sget-object v1, Lcom/google/android/music/art/StaticArt;->sLoadedImages:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_3

    if-eqz v0, :cond_3

    .line 114
    sget-object v1, Lcom/google/android/music/art/StaticArt;->sLoadedImages:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 118
    :cond_1
    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 120
    :cond_2
    return-object v0

    .line 107
    .end local v0    # "result":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 115
    .restart local v0    # "result":Landroid/graphics/Bitmap;
    :cond_3
    if-eqz v0, :cond_1

    .line 116
    :try_start_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 118
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method
