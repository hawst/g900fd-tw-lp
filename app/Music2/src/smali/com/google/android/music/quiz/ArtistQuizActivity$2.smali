.class Lcom/google/android/music/quiz/ArtistQuizActivity$2;
.super Ljava/lang/Object;
.source "ArtistQuizActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/quiz/ArtistQuizActivity;->getUserQuizResults()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mInsertedRecs:Z

.field mListenNowResponse:Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

.field final synthetic this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

.field final synthetic val$maxRetries:I

.field final synthetic val$retryDelay:J


# direct methods
.method constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;IJ)V
    .locals 1

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    iput p2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->val$maxRetries:I

    iput-wide p3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->val$retryDelay:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mInsertedRecs:Z

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 6

    .prologue
    .line 217
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->val$maxRetries:I

    if-ge v1, v2, :cond_3

    .line 219
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mClientImpl:Lcom/google/android/music/cloudclient/MusicCloudImpl;
    invoke-static {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$400(Lcom/google/android/music/quiz/ArtistQuizActivity;)Lcom/google/android/music/cloudclient/MusicCloudImpl;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedGenreIds:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$100(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mSelectedArtistIds:Ljava/util/HashSet;
    invoke-static {v4}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$200(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # getter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mUnselectedArtistIds:Ljava/util/HashSet;
    invoke-static {v5}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$300(Lcom/google/android/music/quiz/ArtistQuizActivity;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/music/cloudclient/MusicCloudImpl;->getUserQuizResults(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mListenNowResponse:Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    .line 221
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mListenNowResponse:Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mListenNowResponse:Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;->mListenNowItems:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mListenNowResponse:Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;->mListenNowItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 224
    :cond_0
    const-string v2, "MusicQuiz"

    const-string v3, "Empty ListenNow recommendations for New User Quiz"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    .line 242
    :cond_1
    :goto_1
    :try_start_1
    iget-wide v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->val$retryDelay:J

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3

    .line 217
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 225
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getStoreService()Lcom/google/android/music/store/IStoreService;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 226
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getStoreService()Lcom/google/android/music/store/IStoreService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mListenNowResponse:Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    invoke-static {v3}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonString(Lcom/google/api/client/json/GenericJson;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/music/store/IStoreService;->insertLockerRecommendations(Ljava/lang/String;)V

    .line 229
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mInsertedRecs:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 247
    :cond_3
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "MusicQuiz"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 234
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 235
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "MusicQuiz"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 236
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 237
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MusicQuiz"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 243
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_3
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "MusicQuiz"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public taskCompleted()V
    .locals 5

    .prologue
    .line 251
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->mInsertedRecs:Z

    if-nez v2, :cond_2

    .line 255
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    iget-object v3, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v3}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b033f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 260
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "metajamIdDestination"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "metajamIdDestination":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v2}, Lcom/google/android/music/quiz/ArtistQuizActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "playlistShareTokenDestination"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "playlistShareTokenDestination":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 269
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-static {v2, v0, v1}, Lcom/google/android/music/ui/AppNavigation;->goListenNow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$2;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-static {v2}, Lcom/google/android/music/ui/AppNavigation;->goListenNow(Landroid/content/Context;)V

    goto :goto_0
.end method
