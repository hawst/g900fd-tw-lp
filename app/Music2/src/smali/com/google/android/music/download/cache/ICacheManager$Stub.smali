.class public abstract Lcom/google/android/music/download/cache/ICacheManager$Stub;
.super Landroid/os/Binder;
.source "ICacheManager.java"

# interfaces
.implements Lcom/google/android/music/download/cache/ICacheManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/ICacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/cache/ICacheManager$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/cache/ICacheManager;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/music/download/cache/ICacheManager;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/google/android/music/download/cache/ICacheManager;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/google/android/music/download/cache/ICacheManager$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/google/android/music/download/cache/ICacheManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 160
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 46
    :sswitch_0
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    sget-object v1, Lcom/google/android/music/download/ContentIdentifier;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/ContentIdentifier;

    .line 60
    .local v2, "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 62
    .local v3, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 64
    .local v4, "_arg2":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg3":I
    move-object v1, p0

    .line 65
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->getTempFileLocation(Lcom/google/android/music/download/ContentIdentifier;IJI)Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v8

    .line 66
    .local v8, "_result":Lcom/google/android/music/download/cache/FileLocation;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 67
    if-eqz v8, :cond_1

    .line 68
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 69
    invoke-virtual {v8, p3, v0}, Lcom/google/android/music/download/cache/FileLocation;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v2    # "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":J
    .end local v6    # "_arg3":I
    .end local v8    # "_result":Lcom/google/android/music/download/cache/FileLocation;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    goto :goto_1

    .line 72
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":J
    .restart local v6    # "_arg3":I
    .restart local v8    # "_result":Lcom/google/android/music/download/cache/FileLocation;
    :cond_1
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 78
    .end local v2    # "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":J
    .end local v6    # "_arg3":I
    .end local v8    # "_result":Lcom/google/android/music/download/cache/FileLocation;
    :sswitch_2
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    sget-object v1, Lcom/google/android/music/download/DownloadRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/DownloadRequest;

    .line 87
    .local v2, "_arg0":Lcom/google/android/music/download/DownloadRequest;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 89
    .local v3, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 91
    .restart local v4    # "_arg2":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .restart local v6    # "_arg3":I
    move-object v1, p0

    .line 92
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->storeInCache(Lcom/google/android/music/download/DownloadRequest;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v8

    .line 93
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 94
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    .end local v2    # "_arg0":Lcom/google/android/music/download/DownloadRequest;
    .end local v3    # "_arg1":Ljava/lang/String;
    .end local v4    # "_arg2":J
    .end local v6    # "_arg3":I
    .end local v8    # "_result":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/google/android/music/download/DownloadRequest;
    goto :goto_2

    .line 99
    .end local v2    # "_arg0":Lcom/google/android/music/download/DownloadRequest;
    :sswitch_3
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_3

    .line 102
    sget-object v1, Lcom/google/android/music/download/DownloadRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/DownloadRequest;

    .line 107
    .restart local v2    # "_arg0":Lcom/google/android/music/download/DownloadRequest;
    :goto_3
    invoke-virtual {p0, v2}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->requestDelete(Lcom/google/android/music/download/DownloadRequest;)V

    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 105
    .end local v2    # "_arg0":Lcom/google/android/music/download/DownloadRequest;
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/google/android/music/download/DownloadRequest;
    goto :goto_3

    .line 113
    .end local v2    # "_arg0":Lcom/google/android/music/download/DownloadRequest;
    :sswitch_4
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->getFreePersistentStorageSpaceInBytes()J

    move-result-wide v8

    .line 115
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 116
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 121
    .end local v8    # "_result":J
    :sswitch_5
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->getTotalPersistentStorageSpaceInBytes()J

    move-result-wide v8

    .line 123
    .restart local v8    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 124
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 129
    .end local v8    # "_result":J
    :sswitch_6
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->getStorageSizes()Lcom/google/android/music/download/cache/StorageSizes;

    move-result-object v8

    .line 131
    .local v8, "_result":Lcom/google/android/music/download/cache/StorageSizes;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 132
    if-eqz v8, :cond_4

    .line 133
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    invoke-virtual {v8, p3, v0}, Lcom/google/android/music/download/cache/StorageSizes;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 137
    :cond_4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 143
    .end local v8    # "_result":Lcom/google/android/music/download/cache/StorageSizes;
    :sswitch_7
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/download/cache/IDeleteFilter$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/cache/IDeleteFilter;

    move-result-object v2

    .line 146
    .local v2, "_arg0":Lcom/google/android/music/download/cache/IDeleteFilter;
    invoke-virtual {p0, v2}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->registerDeleteFilter(Lcom/google/android/music/download/cache/IDeleteFilter;)V

    .line 147
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 152
    .end local v2    # "_arg0":Lcom/google/android/music/download/cache/IDeleteFilter;
    :sswitch_8
    const-string v1, "com.google.android.music.download.cache.ICacheManager"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/download/cache/IDeleteFilter$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/cache/IDeleteFilter;

    move-result-object v2

    .line 155
    .restart local v2    # "_arg0":Lcom/google/android/music/download/cache/IDeleteFilter;
    invoke-virtual {p0, v2}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->unregisterDeleteFilter(Lcom/google/android/music/download/cache/IDeleteFilter;)V

    .line 156
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
