.class Lcom/google/android/music/ui/GridFragment$3;
.super Ljava/lang/Object;
.source "GridFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/GridFragment;->initEmptyScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/GridFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/GridFragment;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/music/ui/GridFragment$3;->this$0:Lcom/google/android/music/ui/GridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 234
    iget-object v1, p0, Lcom/google/android/music/ui/GridFragment$3;->this$0:Lcom/google/android/music/ui/GridFragment;

    const v2, 0x7f0b008c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/ui/GridFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "url":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/GridFragment$3;->this$0:Lcom/google/android/music/ui/GridFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/GridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/music/ui/AppNavigation;->openHelpLink(Landroid/content/Context;Ljava/lang/String;)V

    .line 237
    return-void
.end method
