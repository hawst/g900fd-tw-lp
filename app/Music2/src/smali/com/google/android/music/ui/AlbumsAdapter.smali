.class public Lcom/google/android/music/ui/AlbumsAdapter;
.super Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;
.source "AlbumsAdapter.java"


# static fields
.field public static final PROJECTION:[Ljava/lang/String;

.field private static TAG:Ljava/lang/String;

.field static final sTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# instance fields
.field protected mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-string v0, "AlbumsAdapter"

    sput-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->TAG:Ljava/lang/String;

    .line 28
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->PROJECTION:[Ljava/lang/String;

    .line 48
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    sput-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->sTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 6
    .param p1, "fragment"    # Lcom/google/android/music/ui/AlbumGridFragment;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->sTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v2

    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->sTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;IIILandroid/database/Cursor;)V

    .line 62
    iput-object p3, p0, Lcom/google/android/music/ui/AlbumsAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 63
    invoke-virtual {p0}, Lcom/google/android/music/ui/AlbumsAdapter;->init()V

    .line 64
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 3
    .param p1, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->sTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    sget-object v1, Lcom/google/android/music/ui/AlbumsAdapter;->sTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;III)V

    .line 55
    iput-object p2, p0, Lcom/google/android/music/ui/AlbumsAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/music/ui/AlbumsAdapter;->init()V

    .line 57
    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    instance-of v1, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 131
    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 132
    .local v0, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindLoading()V

    .line 134
    .end local v0    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 15
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    .line 94
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v9

    .line 96
    .local v9, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 97
    .local v12, "id":J
    invoke-virtual {v9, v12, v13}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 98
    invoke-virtual {v9, v12, v13}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 100
    const/4 v14, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 101
    .local v3, "albumName":Ljava/lang/String;
    const/4 v14, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 102
    .local v8, "artistName":Ljava/lang/String;
    const/4 v14, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 103
    .local v6, "artistId":J
    const/4 v14, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_1

    const/4 v4, 0x0

    .line 105
    .local v4, "artUrl":Ljava/lang/String;
    :goto_0
    const/4 v14, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_2

    const/4 v2, 0x0

    .line 107
    .local v2, "albumMetajamId":Ljava/lang/String;
    :goto_1
    const/4 v14, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_3

    const/4 v5, 0x0

    .line 109
    .local v5, "artistMetajamId":Ljava/lang/String;
    :goto_2
    const/4 v14, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    if-eqz v14, :cond_4

    const/4 v10, 0x1

    .line 110
    .local v10, "hasLocal":Z
    :goto_3
    invoke-virtual {v9, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v9, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v9, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v9, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 114
    invoke-virtual {v9, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v9, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 116
    sget-object v14, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v9, v14}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 117
    invoke-virtual {v9, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v9, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v9, v10}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 121
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v14, :cond_0

    move-object/from16 v11, p1

    .line 122
    check-cast v11, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 123
    .local v11, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v14, p0, Lcom/google/android/music/ui/AlbumsAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    invoke-virtual {v11, v9, v14}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 126
    .end local v11    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void

    .line 103
    .end local v2    # "albumMetajamId":Ljava/lang/String;
    .end local v4    # "artUrl":Ljava/lang/String;
    .end local v5    # "artistMetajamId":Ljava/lang/String;
    .end local v10    # "hasLocal":Z
    :cond_1
    const/4 v14, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 105
    .restart local v4    # "artUrl":Ljava/lang/String;
    :cond_2
    const/4 v14, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 107
    .restart local v2    # "albumMetajamId":Ljava/lang/String;
    :cond_3
    const/4 v14, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 109
    .restart local v5    # "artistMetajamId":Ljava/lang/String;
    :cond_4
    const/4 v10, 0x0

    goto :goto_3
.end method

.method public getFakeView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 87
    sget-object v1, Lcom/google/android/music/ui/AlbumsAdapter;->TAG:Ljava/lang/String;

    const-string v2, "Subclass must override this method to support fake view"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-object v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 74
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 75
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    sget-object v2, Lcom/google/android/music/ui/AlbumsAdapter;->sTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 78
    :cond_0
    return-object v0
.end method

.method protected init()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AlbumsAdapter;->setDisableFake(Z)V

    .line 69
    return-void
.end method
