.class public abstract Lcom/google/android/music/ui/MediaListCardAdapter;
.super Lcom/google/android/music/ui/MediaListCursorAdapter;
.source "MediaListCardAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/MusicFragment;I)V
    .locals 0
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 23
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V
    .locals 0
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 27
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 32
    instance-of v0, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/music/ui/cardlib/model/Document;

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListCardAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v1, v0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 45
    :cond_0
    return-void
.end method
