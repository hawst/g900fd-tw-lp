.class public Lcom/google/android/music/ui/AppNavigation;
.super Ljava/lang/Object;
.source "AppNavigation.java"


# direct methods
.method public static getHelpLinkResId()I
    .locals 1

    .prologue
    .line 116
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const v0, 0x7f0b0092

    .line 119
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0b0091

    goto :goto_0
.end method

.method public static getHomeScreenPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 203
    invoke-static {p0}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 204
    .local v0, "i":Landroid/content/Intent;
    invoke-static {p0, v1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getIntentToOpenApp(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 229
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/music/ui/HomeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .local v1, "launchComponent":Landroid/content/ComponentName;
    invoke-static {v1}, Landroid/support/v4/content/IntentCompat;->makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 231
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 232
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    return-object v2
.end method

.method public static getIntentToOpenAppWithPlaybackScreen(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 243
    invoke-static {p0}, Lcom/google/android/music/ui/NowPlayingLaunchDelegateActivity;->getLaunchIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 244
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, v1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getIntentToOpenDownloadContainerActivity(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 281
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/DownloadContainerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 282
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 284
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getShowSonglistIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 211
    invoke-static {p0, p1}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v0

    .line 212
    .local v0, "intent":Landroid/content/Intent;
    return-object v0
.end method

.method public static goHome(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;Z)V

    .line 152
    return-void
.end method

.method public static goHome(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "allowTutorial"    # Z

    .prologue
    .line 162
    invoke-static {p0}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 163
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 164
    const-string v1, "allowTutorial"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 165
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 166
    return-void
.end method

.method public static goListenNow(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 173
    invoke-static {p0, v0, v0}, Lcom/google/android/music/ui/AppNavigation;->goListenNow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public static goListenNow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "metajamIdDestination"    # Ljava/lang/String;
    .param p2, "playlistShareTokenDestination"    # Ljava/lang/String;

    .prologue
    .line 190
    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-static {p0, v1}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;Lcom/google/android/music/ui/HomeActivity$Screen;)Landroid/content/Intent;

    move-result-object v0

    .line 192
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 193
    const-string v1, "metajamIdDestination"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v1, "playlistShareTokenDestination"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 197
    return-void
.end method

.method public static openHelpLink(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 260
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 261
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 262
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 263
    const v2, 0x8000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 264
    const/high16 v2, 0x80000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 267
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 268
    .local v1, "resolvedActivities":Landroid/content/pm/ResolveInfo;
    if-nez v1, :cond_0

    .line 269
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02b0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static openMetajamItem(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "metajamIdDestination"    # Ljava/lang/String;

    .prologue
    .line 296
    const-string v2, "https://play.google.com/music/m/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 298
    .local v0, "builder":Landroid/net/Uri$Builder;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/MusicUrlHandler;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 299
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 300
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 301
    return-void
.end method

.method public static openMetajamItem(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 0
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "openMetajamItemInfo"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;
    .param p2, "openMetajamItemCallback"    # Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    .prologue
    .line 314
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->openMetajamItem(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V

    .line 316
    return-void
.end method

.method public static openNowPlayingDrawer(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 251
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.music.OPEN_DRAWER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 253
    return-void
.end method

.method public static openPlaylist(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "playlistShareTokenDestination"    # Ljava/lang/String;

    .prologue
    .line 325
    const-string v2, "https://play.google.com/music/playlist/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 327
    .local v0, "builder":Landroid/net/Uri$Builder;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/activities/SharedSongsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 329
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 330
    return-void
.end method

.method public static showHomeScreen(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V
    .locals 1
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "screenToShow"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/music/ui/AppNavigation;->showHomeScreen(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeActivity$Screen;Z)V

    .line 61
    return-void
.end method

.method public static showHomeScreen(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeActivity$Screen;Z)V
    .locals 9
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "screenToShow"    # Lcom/google/android/music/ui/HomeActivity$Screen;
    .param p2, "openMenu"    # Z

    .prologue
    const/4 v8, 0x1

    .line 73
    sget-object v6, Lcom/google/android/music/ui/HomeActivity$Screen;->SHOP:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-ne p1, v6, :cond_1

    .line 74
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 75
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v1}, Lcom/google/android/music/download/IntentConstants;->getMusicStoreIntent(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Intent;

    move-result-object v4

    .line 76
    .local v4, "shop":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 77
    invoke-virtual {p0, v4}, Lcom/google/android/music/ui/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 110
    .end local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v4    # "shop":Landroid/content/Intent;
    :goto_0
    return-void

    .line 79
    .restart local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v4    # "shop":Landroid/content/Intent;
    :cond_0
    const v6, 0x7f0b0351

    invoke-static {p0, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 81
    .end local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v4    # "shop":Landroid/content/Intent;
    :cond_1
    sget-object v6, Lcom/google/android/music/ui/HomeActivity$Screen;->SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-ne p1, v6, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->closeSideDrawer()V

    .line 83
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/google/android/music/activities/MusicSettingsActivity;

    invoke-direct {v3, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    .local v3, "settingsIntent":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 85
    .end local v3    # "settingsIntent":Landroid/content/Intent;
    :cond_2
    sget-object v6, Lcom/google/android/music/ui/HomeActivity$Screen;->HELP:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-ne p1, v6, :cond_3

    .line 87
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 88
    .local v2, "res":Landroid/content/res/Resources;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/music/ui/AppNavigation;->getHelpLinkResId()I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&hl="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 90
    .local v5, "url":Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/google/android/music/ui/AppNavigation;->openHelpLink(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    .end local v2    # "res":Landroid/content/res/Resources;
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    sget-object v6, Lcom/google/android/music/ui/HomeActivity$Screen;->FEEDBACK:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-ne p1, v6, :cond_4

    .line 92
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->closeSideDrawer()V

    .line 96
    new-instance v6, Lcom/google/android/music/ui/AppNavigation$1;

    invoke-direct {v6, p0}, Lcom/google/android/music/ui/AppNavigation$1;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    const/16 v7, 0xfa

    invoke-static {v6, p0, v7}, Lcom/google/android/music/utils/MusicUtils;->runDelayedOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;I)Landroid/os/Handler;

    goto :goto_0

    .line 104
    :cond_4
    invoke-static {p0, p1}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;Lcom/google/android/music/ui/HomeActivity$Screen;)Landroid/content/Intent;

    move-result-object v0

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v0, p2, v8}, Lcom/google/android/music/ui/BaseActivity;->putExtraDrawerState(Landroid/content/Intent;ZZ)V

    .line 108
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static startQuiz(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 127
    invoke-static {p0, v0, v0}, Lcom/google/android/music/ui/AppNavigation;->startQuiz(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public static startQuiz(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "metajamIdDestination"    # Ljava/lang/String;
    .param p2, "playlistShareTokenDestination"    # Ljava/lang/String;

    .prologue
    .line 139
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 141
    const-string v1, "metajamIdDestination"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string v1, "playlistShareTokenDestination"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 145
    return-void
.end method
