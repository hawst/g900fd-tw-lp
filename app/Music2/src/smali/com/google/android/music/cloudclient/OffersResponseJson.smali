.class public Lcom/google/android/music/cloudclient/OffersResponseJson;
.super Lcom/google/api/client/json/GenericJson;
.source "OffersResponseJson.java"


# static fields
.field public static final ACCOUNT_STATUS_LOCKER:I = 0x2

.field public static final ACCOUNT_STATUS_NONE:I = 0x1

.field public static final COUPON_STATUS_ALREADY_REDEEMED:I = 0x3

.field public static final COUPON_STATUS_EXPIRED:I = 0x4

.field public static final COUPON_STATUS_INVALID:I = 0x2

.field public static final COUPON_STATUS_OK:I = 0x1

.field public static final NAUTILUS_IN_TRIAL:I = 0x3

.field public static final NAUTILUS_PAID:I = 0x4


# instance fields
.field public mAccountStatus:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "accountStatus"
    .end annotation
.end field

.field public mCouponStatus:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "couponStatus"
    .end annotation
.end field

.field public mIsLockerAvailable:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lockerAvail"
    .end annotation
.end field

.field public mIsVideoServiceAvailable:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "isVsAvail"
    .end annotation
.end field

.field public mNautilusExpirationTimeInSeconds:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "eTimeSecs"
    .end annotation
.end field

.field public mOffers:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "offers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/OfferJson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mAccountStatus:I

    return-void
.end method


# virtual methods
.method public canSignupForNautilus()Z
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/OffersResponseJson;->isNautilusAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/OffersResponseJson;->isSignedUpForNautilus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCouponStatus()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mCouponStatus:I

    return v0
.end method

.method public getDefaultOffer()Lcom/google/android/music/cloudclient/OfferJson;
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/OffersResponseJson;->isNautilusAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mOffers:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/OfferJson;

    .line 82
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLockerAvailable()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mIsLockerAvailable:Z

    return v0
.end method

.method public isNautilusAvailable()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mOffers:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mOffers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSignedUpForNautilus()Z
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mAccountStatus:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mAccountStatus:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidMusicAccount()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 47
    iget-boolean v1, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mIsLockerAvailable:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mAccountStatus:I

    if-le v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
