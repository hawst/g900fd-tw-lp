.class public Lcom/google/android/music/ui/TvHomeMenu;
.super Ljava/lang/Object;
.source "TvHomeMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/TvHomeMenu;->LOGV:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/HomeActivity$Screen;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/music/ui/TvHomeMenu;->getScreenIndex(Lcom/google/android/music/ui/HomeActivity$Screen;)I

    move-result v0

    return v0
.end method

.method public static configureListView(Landroid/widget/ListView;Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;
    .locals 4
    .param p0, "listView"    # Landroid/widget/ListView;
    .param p1, "activity"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 63
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    .line 64
    .local v1, "oldAdapter":Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getSelectedIndex()I

    move-result v2

    .line 67
    .local v2, "oldSelectedIndex":I
    :goto_0
    invoke-static {p1}, Lcom/google/android/music/ui/TvHomeMenu;->createListAdapter(Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    move-result-object v0

    .line 68
    .local v0, "adapter":Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;
    invoke-virtual {v0}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 69
    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->onItemSelected(I)Z

    .line 71
    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 72
    invoke-static {p1, v0}, Lcom/google/android/music/ui/TvHomeMenu;->createClickListener(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    return-object v0

    .line 64
    .end local v0    # "adapter":Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;
    .end local v2    # "oldSelectedIndex":I
    :cond_1
    const/high16 v2, -0x80000000

    goto :goto_0
.end method

.method private static createClickListener(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "adapter"    # Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/music/ui/TvHomeMenu$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/TvHomeMenu$1;-><init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;)V

    return-object v0
.end method

.method private static createListAdapter(Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;
    .locals 8
    .param p0, "context"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 79
    .local v4, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/google/android/music/ui/HomeMenuScreens;->getMenuScreens()[Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v2

    .line 82
    .local v2, "itemScreens":[Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-static {p0}, Lcom/google/android/music/utils/BugReporter;->isGoogleFeedbackInstalled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 83
    array-length v1, v2

    .line 88
    .local v1, "itemCount":I
    :goto_0
    new-array v3, v1, [Ljava/lang/String;

    .line 89
    .local v3, "items":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 90
    aget-object v5, v2, v0

    invoke-virtual {v5, v4}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v0

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 85
    .end local v0    # "i":I
    .end local v1    # "itemCount":I
    .end local v3    # "items":[Ljava/lang/String;
    :cond_0
    array-length v5, v2

    add-int/lit8 v1, v5, -0x1

    .restart local v1    # "itemCount":I
    goto :goto_0

    .line 92
    .restart local v0    # "i":I
    .restart local v3    # "items":[Ljava/lang/String;
    :cond_1
    new-instance v5, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    const v6, 0x7f0400d9

    const v7, 0x7f0e007d

    invoke-direct {v5, p0, v6, v7, v3}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;-><init>(Lcom/google/android/music/ui/BaseActivity;II[Ljava/lang/String;)V

    return-object v5
.end method

.method private static getScreenIndex(Lcom/google/android/music/ui/HomeActivity$Screen;)I
    .locals 4
    .param p0, "screen"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 43
    invoke-static {}, Lcom/google/android/music/ui/HomeMenuScreens;->getMenuScreens()[Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v1

    .line 45
    .local v1, "screens":[Lcom/google/android/music/ui/HomeActivity$Screen;
    array-length v2, v1

    .line 46
    .local v2, "screensCount":I
    const/4 v0, 0x0

    .local v0, "ii":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 47
    aget-object v3, v1, v0

    invoke-virtual {v3, p0}, Lcom/google/android/music/ui/HomeActivity$Screen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 51
    .end local v0    # "ii":I
    :goto_1
    return v0

    .line 46
    .restart local v0    # "ii":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static selectScreen(Lcom/google/android/music/ui/HomeActivity$Screen;Landroid/widget/ListView;)V
    .locals 2
    .param p0, "screen"    # Lcom/google/android/music/ui/HomeActivity$Screen;
    .param p1, "homeMenuList"    # Landroid/widget/ListView;

    .prologue
    .line 38
    invoke-static {p0}, Lcom/google/android/music/ui/TvHomeMenu;->getScreenIndex(Lcom/google/android/music/ui/HomeActivity$Screen;)I

    move-result v0

    .line 39
    .local v0, "position":I
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->onItemSelected(I)Z

    .line 40
    return-void
.end method
