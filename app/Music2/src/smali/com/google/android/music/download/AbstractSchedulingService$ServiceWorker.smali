.class final Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "AbstractSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/AbstractSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceWorker"
.end annotation


# instance fields
.field private mCompletedDownloadsSize:J

.field private mHighSpeedAvailable:Z

.field private mOutOfSpace:Z

.field private mSuccessiveFailures:I

.field private mTotalDownloadSize:J

.field final synthetic this$0:Lcom/google/android/music/download/AbstractSchedulingService;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/AbstractSchedulingService;Ljava/lang/String;)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 232
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    .line 233
    invoke-direct {p0, p2}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 228
    iput v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mSuccessiveFailures:I

    .line 229
    iput-boolean v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mHighSpeedAvailable:Z

    .line 230
    iput-boolean v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mOutOfSpace:Z

    .line 234
    return-void
.end method

.method private blackListToString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 662
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 663
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 664
    .local v2, "key":J
    const-string v4, "("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 666
    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 667
    iget-object v4, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 668
    const-string v4, "),"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 670
    .end local v2    # "key":J
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 671
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 673
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private handleCancelDownloads(I)V
    .locals 8
    .param p1, "startId"    # I

    .prologue
    const/4 v7, 0x1

    .line 621
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v3, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v3

    .line 622
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v2, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 623
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v2, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 624
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 626
    if-lez v1, :cond_1

    .line 628
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 629
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "%d pending requests. Clearing."

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;
    invoke-static {v2}, Lcom/google/android/music/download/AbstractSchedulingService;->access$000(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/IDownloadQueueManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;
    invoke-static {v3}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1900(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/music/download/DownloadRequest$Owner;->toInt()I

    move-result v3

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/google/android/music/download/IDownloadQueueManager;->cancelAndPurge(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 639
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v2, v7}, Lcom/google/android/music/download/AbstractSchedulingService;->stopForegroundService(Z)V

    .line 640
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v2, p1}, Lcom/google/android/music/download/AbstractSchedulingService;->stopSelf(I)V

    .line 641
    return-void

    .line 624
    .end local v1    # "size":I
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 634
    .restart local v1    # "size":I
    :catch_0
    move-exception v0

    .line 635
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to cancel downloads"

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private handleCheckInitCompletedMessage()V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->isAllServicesConnected()Z
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1500(Lcom/google/android/music/download/AbstractSchedulingService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->checkDependentServices()V
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$100(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 520
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 525
    :goto_0
    return-void

    .line 523
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I
    invoke-static {v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$800(Lcom/google/android/music/download/AbstractSchedulingService;)I

    move-result v1

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendFinalCheckInitCompletedMessage(I)V
    invoke-static {v0, v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1600(Lcom/google/android/music/download/AbstractSchedulingService;I)V

    goto :goto_0
.end method

.method private handleFinalCheckInitCompletedMessage(I)V
    .locals 1
    .param p1, "startId"    # I

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->isAllServicesConnected()Z
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1500(Lcom/google/android/music/download/AbstractSchedulingService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/AbstractSchedulingService;->stopSelf(I)V

    .line 507
    :goto_0
    return-void

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->checkDependentServices()V
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$100(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 505
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V

    goto :goto_0
.end method

.method private handleInitSchedule()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 292
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleInitSchedule"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_0
    iput-wide v4, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mCompletedDownloadsSize:J

    .line 296
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->getTotalDownloadSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    .line 297
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTotalDownloadSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_1
    iget-wide v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    cmp-long v0, v0, v4

    if-gtz v0, :cond_2

    .line 302
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I
    invoke-static {v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$800(Lcom/google/android/music/download/AbstractSchedulingService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/AbstractSchedulingService;->sendCancelDownloadsMessage(I)V

    .line 304
    :cond_2
    return-void
.end method

.method private handleProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 18
    .param p1, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 372
    const/4 v8, 0x0

    .line 373
    .local v8, "numberOfRequests":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v12, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v12

    .line 374
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v11, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v8

    .line 375
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 377
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "requests: %d progress: %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aput-object p1, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_0
    const-wide/16 v2, 0x0

    .line 381
    .local v2, "currentDownloadProgress":J
    const/4 v10, 0x0

    .line 383
    .local v10, "request":Lcom/google/android/music/download/TrackDownloadRequest;
    sget-object v11, Lcom/google/android/music/download/AbstractSchedulingService$7;->$SwitchMap$com$google$android$music$download$DownloadState$State:[I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/music/download/DownloadState$State;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 465
    new-instance v11, Ljava/lang/RuntimeException;

    const-string v12, "Unhandled request state"

    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 375
    .end local v2    # "currentDownloadProgress":J
    .end local v10    # "request":Lcom/google/android/music/download/TrackDownloadRequest;
    :catchall_0
    move-exception v11

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v11

    .line 388
    .restart local v2    # "currentDownloadProgress":J
    .restart local v10    # "request":Lcom/google/android/music/download/TrackDownloadRequest;
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getCompletedBytes()J

    move-result-wide v2

    .line 468
    :cond_1
    :goto_0
    :pswitch_1
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 469
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "mCompletedDownloadsSize=%d currentDownloadProgress=%d mTotalDownloadSize=%d"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mCompletedDownloadsSize:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_2
    const/4 v9, 0x0

    .line 475
    .local v9, "progressRatio":F
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-eqz v11, :cond_3

    .line 476
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mCompletedDownloadsSize:J

    long-to-float v11, v12

    long-to-float v12, v2

    add-float/2addr v11, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    long-to-float v12, v12

    div-float v9, v11, v12

    .line 480
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v11

    sget-object v12, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v11, v12, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v11, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-eqz v11, :cond_4

    .line 481
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    move-object/from16 v0, p1

    invoke-virtual {v11, v9, v0}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDownloadProgress(FLcom/google/android/music/download/TrackDownloadProgress;)V

    .line 484
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v12, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v12

    .line 485
    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v11, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v8

    .line 486
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 487
    if-nez v8, :cond_5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->isDownloadingPaused()Z

    move-result v11

    if-nez v11, :cond_5

    .line 488
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I
    invoke-static {v12}, Lcom/google/android/music/download/AbstractSchedulingService;->access$800(Lcom/google/android/music/download/AbstractSchedulingService;)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/google/android/music/download/AbstractSchedulingService;->sendScheduleDownloadsMessage(I)V

    .line 490
    :cond_5
    return-void

    .line 392
    .end local v9    # "progressRatio":F
    :pswitch_2
    const-wide/16 v2, 0x0

    .line 394
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v12, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v12

    .line 395
    :try_start_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v11, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 396
    .local v6, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 397
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lcom/google/android/music/download/TrackDownloadRequest;

    move-object v10, v0

    .line 398
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/google/android/music/download/TrackDownloadRequest;->isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 399
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 400
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mCompletedDownloadsSize:J

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getCompletedBytes()J

    move-result-wide v16

    add-long v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mCompletedDownloadsSize:J

    .line 404
    :cond_7
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 406
    if-eqz v10, :cond_8

    .line 407
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    move-object/from16 v0, p1

    invoke-virtual {v11, v10, v0}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDownloadCompleted(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V

    .line 410
    :cond_8
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 411
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mCompletedDownloadsSize="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mCompletedDownloadsSize:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 404
    .end local v6    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    :catchall_1
    move-exception v11

    :try_start_4
    monitor-exit v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v11

    .line 416
    :pswitch_3
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mSuccessiveFailures:I

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mSuccessiveFailures:I

    .line 417
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getError()I

    move-result v11

    const/4 v12, 0x5

    if-ne v11, v12, :cond_b

    .line 418
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v12, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->DEVICE_NOT_AUTHORIZED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V
    invoke-static {v11, v12}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1400(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    .line 448
    :goto_1
    :pswitch_4
    const-wide/16 v2, 0x0

    .line 449
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v12, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v12

    .line 450
    :try_start_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v11, v11, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 451
    .restart local v6    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    :cond_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 452
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lcom/google/android/music/download/TrackDownloadRequest;

    move-object v10, v0

    .line 453
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/google/android/music/download/TrackDownloadRequest;->isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 454
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 458
    :cond_a
    monitor-exit v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 459
    if-eqz v10, :cond_1

    .line 460
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    move-object/from16 v0, p1

    invoke-virtual {v11, v10, v0}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDownloadFailed(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V

    goto/16 :goto_0

    .line 419
    .end local v6    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getError()I

    move-result v11

    const/16 v12, 0xc

    if-eq v11, v12, :cond_c

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getError()I

    move-result v11

    const/16 v12, 0xd

    if-ne v11, v12, :cond_f

    .line 421
    :cond_c
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v12, "music_download_max_blacklist_size"

    const/16 v13, 0x3e8

    invoke-static {v11, v12, v13}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    .line 424
    .local v7, "maxBlacklistSize":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v12

    monitor-enter v12

    .line 425
    :try_start_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v4

    .line 428
    .local v4, "id":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v11

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-interface {v11, v13}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_d

    .line 429
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v11

    if-ge v11, v7, :cond_e

    .line 430
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v11

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-interface {v11, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v11

    if-eqz v11, :cond_d

    .line 432
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v11

    const-string v13, "Blacklisting id=%s blacklist=[%s]"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->blackListToString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_d
    :goto_2
    monitor-exit v12

    goto/16 :goto_1

    .end local v4    # "id":J
    :catchall_2
    move-exception v11

    monitor-exit v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v11

    .line 437
    .restart local v4    # "id":J
    :cond_e
    :try_start_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v11

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Max blacklist size reached: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_2

    .line 444
    .end local v4    # "id":J
    .end local v7    # "maxBlacklistSize":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V
    invoke-static {v11}, Lcom/google/android/music/download/AbstractSchedulingService;->access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V

    goto/16 :goto_1

    .line 458
    :catchall_3
    move-exception v11

    :try_start_8
    monitor-exit v12
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v11

    .line 486
    .restart local v9    # "progressRatio":F
    :catchall_4
    move-exception v11

    :try_start_9
    monitor-exit v12
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v11

    .line 383
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private handleScheduleDownloads()Z
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 307
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 308
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "handleScheduleDownloads"

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_0
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v7, v8, :cond_2

    .line 312
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 313
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "The service is disabled"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_1
    :goto_0
    return v5

    .line 318
    :cond_2
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v7, v7, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v7

    .line 319
    :try_start_0
    iget-object v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v8, v8, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    .line 320
    .local v3, "size":I
    if-eqz v3, :cond_4

    .line 321
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 322
    iget-object v5, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "We have %d pending requests. Not scheduling new."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_3
    monitor-exit v7

    move v5, v6

    goto :goto_0

    .line 327
    :cond_4
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 329
    const/4 v2, 0x0

    .line 331
    .local v2, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    :try_start_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 332
    .local v0, "blacklistedIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v7

    monitor-enter v7
    :try_end_1
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_1 .. :try_end_1} :catch_0

    .line 333
    :try_start_2
    iget-object v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v8}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 334
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 335
    :try_start_3
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;
    invoke-static {v8}, Lcom/google/android/music/download/AbstractSchedulingService;->access$200(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/cache/ICacheManager;

    move-result-object v8

    invoke-virtual {v7, v8, v0}, Lcom/google/android/music/download/AbstractSchedulingService;->getNextDownloads(Lcom/google/android/music/download/cache/ICacheManager;Ljava/util/Collection;)Ljava/util/List;
    :try_end_3
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v2

    .line 343
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_5

    .line 345
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->disableStartupReceiver()V
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1200(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 350
    :goto_1
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v7, v7, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v7

    .line 351
    :try_start_4
    iget-object v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v8, v8, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 352
    iget-object v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v8, v8, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_6

    .line 353
    monitor-exit v7

    goto :goto_0

    .line 355
    :catchall_0
    move-exception v5

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v5

    .line 327
    .end local v0    # "blacklistedIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v2    # "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    .end local v3    # "size":I
    :catchall_1
    move-exception v5

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v5

    .line 334
    .restart local v0    # "blacklistedIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v2    # "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    .restart local v3    # "size":I
    :catchall_2
    move-exception v5

    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v5
    :try_end_7
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_7 .. :try_end_7} :catch_0

    .line 336
    .end local v0    # "blacklistedIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catch_0
    move-exception v1

    .line 337
    .local v1, "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    iget-object v5, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to get next downloads: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/music/download/cache/OutOfSpaceException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iput-boolean v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mOutOfSpace:Z

    .line 339
    iget-object v5, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V
    invoke-static {v5}, Lcom/google/android/music/download/AbstractSchedulingService;->access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 340
    iget-object v5, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->enableStartupReceiver()V
    invoke-static {v5}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1100(Lcom/google/android/music/download/AbstractSchedulingService;)V

    move v5, v6

    .line 341
    goto/16 :goto_0

    .line 348
    .end local v1    # "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    .restart local v0    # "blacklistedIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->enableStartupReceiver()V
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1100(Lcom/google/android/music/download/AbstractSchedulingService;)V

    goto :goto_1

    .line 355
    :cond_6
    :try_start_8
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 360
    :try_start_9
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 361
    .local v4, "tmp":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$000(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/IDownloadQueueManager;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;
    invoke-static {v8}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1300(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/IDownloadProgressListener;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v7, v4, v8, v9}, Lcom/google/android/music/download/IDownloadQueueManager;->download(Ljava/util/List;Lcom/google/android/music/download/IDownloadProgressListener;I)V

    .line 363
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDownloadStarting()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_1

    move v5, v6

    .line 364
    goto/16 :goto_0

    .line 365
    .end local v4    # "tmp":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/DownloadRequest;>;"
    :catch_1
    move-exception v1

    .line 366
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Failed to schedule downloads"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private handleUpdateEnabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V
    .locals 14
    .param p1, "reason"    # Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .prologue
    const/4 v1, 0x1

    const-wide/16 v12, 0x0

    const/4 v6, 0x0

    .line 528
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 529
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleUpdateEnabled: mState="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v9}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    :cond_0
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->isDownloadingPaused()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v7, v8, :cond_2

    .line 533
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 534
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Downloading is paused by user"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    :cond_1
    :goto_0
    return-void

    .line 539
    :cond_2
    if-eqz p1, :cond_4

    .line 541
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 542
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleUpdateEnabled: DisableReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_3
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v7, v8, :cond_1

    .line 546
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7, v8}, Lcom/google/android/music/download/AbstractSchedulingService;->access$502(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 547
    iput v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mSuccessiveFailures:I

    .line 548
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v6, p1}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    goto :goto_0

    .line 553
    :cond_4
    iget-boolean v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mOutOfSpace:Z

    if-eqz v7, :cond_6

    .line 554
    iget-wide v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    cmp-long v7, v8, v12

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v7, v8, :cond_5

    .line 555
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->OUT_OF_SPACE:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-virtual {v7, v8}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    .line 557
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7, v8}, Lcom/google/android/music/download/AbstractSchedulingService;->access$502(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 558
    iput-boolean v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mOutOfSpace:Z

    goto :goto_0

    .line 562
    :cond_6
    const/4 v3, 0x0

    .line 563
    .local v3, "mobileSpeed":Z
    const/4 v2, 0x0

    .line 565
    .local v2, "highSpeed":Z
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1700(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v4

    .line 567
    .local v4, "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    if-nez v4, :cond_7

    .line 568
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing binding to the network monitor"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 572
    :cond_7
    :try_start_0
    invoke-interface {v4}, Lcom/google/android/music/net/INetworkMonitor;->hasMobileOrMeteredConnection()Z

    move-result v3

    .line 573
    invoke-interface {v4}, Lcom/google/android/music/net/INetworkMonitor;->hasHighSpeedConnection()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 579
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 580
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "handleUpdateEnabled: mobileSpeed=%b highSpeed=%b"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_8
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1800(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineDLOnlyOnWifi()Z

    move-result v5

    .line 587
    .local v5, "wifiOnly":Z
    if-nez v2, :cond_9

    if-eqz v3, :cond_a

    if-nez v5, :cond_a

    .line 589
    .local v1, "enable":Z
    :cond_9
    :goto_1
    if-eqz v1, :cond_b

    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WORKING:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v7, v8, :cond_b

    .line 590
    iput-boolean v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mHighSpeedAvailable:Z

    .line 591
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WORKING:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7, v8}, Lcom/google/android/music/download/AbstractSchedulingService;->access$502(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 592
    iput v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mSuccessiveFailures:I

    .line 593
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->updateBlacklistedIds()V

    .line 594
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$800(Lcom/google/android/music/download/AbstractSchedulingService;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/music/download/AbstractSchedulingService;->sendScheduleDownloadsMessage(I)V

    goto/16 :goto_0

    .line 574
    .end local v1    # "enable":Z
    .end local v5    # "wifiOnly":Z
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v5    # "wifiOnly":Z
    :cond_a
    move v1, v6

    .line 587
    goto :goto_1

    .line 595
    .restart local v1    # "enable":Z
    :cond_b
    if-nez v1, :cond_e

    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v7, v8, :cond_e

    .line 596
    iget-wide v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    cmp-long v6, v6, v12

    if-eqz v6, :cond_c

    .line 597
    iget-boolean v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mHighSpeedAvailable:Z

    if-eqz v6, :cond_d

    if-nez v2, :cond_d

    if-eqz v5, :cond_d

    .line 598
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v7, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->HIGH_SPEED_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-virtual {v6, v7}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    .line 603
    :cond_c
    :goto_2
    iput-boolean v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mHighSpeedAvailable:Z

    .line 604
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v7, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v6, v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$502(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    goto/16 :goto_0

    .line 600
    :cond_d
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v7, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->CONNECTIVITY_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-virtual {v6, v7}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    goto :goto_2

    .line 605
    :cond_e
    iget v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mSuccessiveFailures:I

    const/4 v8, 0x3

    if-lt v7, v8, :cond_1

    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v7, v8, :cond_1

    .line 611
    iget-wide v8, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mTotalDownloadSize:J

    cmp-long v7, v8, v12

    if-eqz v7, :cond_f

    .line 612
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->SUCCESSIVE_FAILURES:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-virtual {v7, v8}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    .line 614
    :cond_f
    iget-object v7, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    sget-object v8, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v7, v8}, Lcom/google/android/music/download/AbstractSchedulingService;->access$502(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    .line 615
    iput v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mSuccessiveFailures:I

    goto/16 :goto_0
.end method

.method private updateBlacklistedIds()V
    .locals 12

    .prologue
    .line 644
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "music_download_blacklist_ttl_sec"

    const-wide/16 v8, 0xe10

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long v4, v6, v8

    .line 648
    .local v4, "ttlMs":J
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v7

    monitor-enter v7

    .line 649
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mBlacklistedIds:Ljava/util/Map;
    invoke-static {v6}, Lcom/google/android/music/download/AbstractSchedulingService;->access$1000(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 650
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 651
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 652
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Long;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 653
    .local v2, "timestampMs":J
    add-long v8, v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v6, v8, v10

    if-gtz v6, :cond_0

    .line 654
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 657
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    .end local v2    # "timestampMs":J
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    :cond_1
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 658
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 238
    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/AbstractSchedulingService;->access$700()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage: msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 289
    :cond_1
    :goto_0
    return-void

    .line 244
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I
    invoke-static {v0, v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$802(Lcom/google/android/music/download/AbstractSchedulingService;I)I

    .line 245
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WAITING_FOR_SERVICES:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v0, v1, :cond_3

    .line 247
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I
    invoke-static {v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$800(Lcom/google/android/music/download/AbstractSchedulingService;)I

    move-result v1

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendCheckInitCompletedMessage(I)V
    invoke-static {v0, v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$900(Lcom/google/android/music/download/AbstractSchedulingService;I)V

    .line 253
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->handleInitSchedule()V

    goto :goto_0

    .line 248
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->INITIALIZED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v0, v1, :cond_4

    .line 249
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V

    goto :goto_1

    .line 250
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WORKING:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v0, v1, :cond_2

    .line 251
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mStartId:I
    invoke-static {v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$800(Lcom/google/android/music/download/AbstractSchedulingService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/AbstractSchedulingService;->sendScheduleDownloadsMessage(I)V

    goto :goto_1

    .line 257
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->handleScheduleDownloads()Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WORKING:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v0, v1, :cond_5

    .line 259
    iget-wide v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->mCompletedDownloadsSize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 260
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-virtual {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->notifyAllWorkFinished()V

    .line 263
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v0, v1, :cond_1

    .line 264
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/AbstractSchedulingService;->stopSelf(I)V

    goto :goto_0

    .line 270
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-direct {p0, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->handleProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V

    goto :goto_0

    .line 274
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->handleFinalCheckInitCompletedMessage(I)V

    goto/16 :goto_0

    .line 278
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->handleCheckInitCompletedMessage()V

    goto/16 :goto_0

    .line 282
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-direct {p0, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->handleUpdateEnabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V

    goto/16 :goto_0

    .line 286
    :pswitch_6
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/google/android/music/download/AbstractSchedulingService$ServiceWorker;->handleCancelDownloads(I)V

    goto/16 :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
