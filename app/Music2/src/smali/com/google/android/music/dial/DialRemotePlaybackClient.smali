.class public Lcom/google/android/music/dial/DialRemotePlaybackClient;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "DialRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
.implements Lcom/google/android/music/dial/DialDevice$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/DialRemotePlaybackClient$4;,
        Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;,
        Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;,
        Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mAppContext:Ljava/lang/String;

.field private final mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

.field private final mContext:Landroid/content/Context;

.field private final mDevice:Lcom/google/android/music/dial/DialDevice;

.field private mGroupId:Ljava/lang/String;

.field private mIsPreparing:Z

.field private mIsQueueLoaded:Z

.field private mIsSessionInitialized:Z

.field private mIsStreaming:Z

.field private mItemId:Ljava/lang/String;

.field private mKnownPositionMillis:J

.field private mLastRefreshTimestampMillis:J

.field private mMusicFile:Lcom/google/android/music/store/MusicFile;

.field private mMuted:Z

.field private mPlaybackState:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

.field private final mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

.field private mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

.field private final mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private final mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

.field private mSessionId:Ljava/lang/String;

.field private mVolume:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_PLAYBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/cast/CastTokenClient;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;
    .param p3, "device"    # Lcom/google/android/music/dial/DialDevice;
    .param p4, "castTokenClient"    # Lcom/google/android/music/cast/CastTokenClient;
    .param p5, "serviceHooks"    # Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    .prologue
    const/4 v4, 0x0

    .line 156
    const-string v1, "DialRemotePlaybackClient"

    invoke-direct {p0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 76
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mKnownPositionMillis:J

    .line 78
    new-instance v1, Lcom/google/android/music/playback/StopWatch;

    invoke-direct {v1}, Lcom/google/android/music/playback/StopWatch;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    .line 157
    iput-boolean v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 158
    iput-boolean v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsSessionInitialized:Z

    .line 159
    iput-object p1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    .line 160
    iput-object p2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 161
    iput-object p3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mDevice:Lcom/google/android/music/dial/DialDevice;

    .line 162
    iput-object p4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    .line 163
    iput-object p5, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    .line 166
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 167
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 168
    const-string v1, "com.google.android.music.dial.RemoteDevice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/dial/RemoteDeviceInfo;

    iput-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    .line 169
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    invoke-virtual {v1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getGroupId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mDevice:Lcom/google/android/music/dial/DialDevice;

    invoke-virtual {v1, p0}, Lcom/google/android/music/dial/DialDevice;->addListener(Lcom/google/android/music/dial/DialDevice$Listener;)V

    .line 173
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/dial/DialRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialRemotePlaybackClient;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->handleEvent(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialRemotePlaybackClient;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialRemotePlaybackClient;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/DialRemotePlaybackClient;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/dial/DialRemotePlaybackClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/DialRemotePlaybackClient;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->handleNewSession()V

    return-void
.end method

.method private declared-synchronized forceCreateSession()V
    .locals 7

    .prologue
    .line 581
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 582
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "forceCreateSession %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 586
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid groupID.  Cannot forceCreateSession."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 608
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 589
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mAppContext:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 590
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid appContext.  Cannot forceCreateSession."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 581
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 594
    :cond_3
    :try_start_2
    new-instance v0, Lcom/google/android/music/dial/model/CreateSessionCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/CreateSessionCommandJson;-><init>()V

    .line 595
    .local v0, "cmd":Lcom/google/android/music/dial/model/CreateSessionCommandJson;
    iget-object v2, v0, Lcom/google/android/music/dial/model/CreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->mGroupId:Ljava/lang/String;

    .line 596
    iget-object v2, v0, Lcom/google/android/music/dial/model/CreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    const-string v3, "com.google.RemoteSonosReceiver"

    iput-object v3, v2, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->mAppId:Ljava/lang/String;

    .line 597
    iget-object v2, v0, Lcom/google/android/music/dial/model/CreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mAppContext:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->mAppContext:Ljava/lang/String;

    .line 598
    iget-object v2, v0, Lcom/google/android/music/dial/model/CreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    const-string v3, ""

    iput-object v3, v2, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->mCustomData:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 601
    :try_start_3
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 602
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 603
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v3, "Force create session"

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 605
    :catch_0
    move-exception v1

    .line 606
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_4
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Malformed command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method private getFreshCastToken()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 985
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    invoke-virtual {v2}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getHouseholdId()Ljava/lang/String;

    move-result-object v0

    .line 988
    .local v0, "householdId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 989
    const-string v2, "%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 997
    .local v1, "remoteEndpointId":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receiver:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    const-string v4, "_"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1000
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    invoke-interface {v2, v1}, Lcom/google/android/music/cast/CastTokenClient;->clearCachedCastToken(Ljava/lang/String;)V

    .line 1001
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getProvider()Landroid/support/v7/media/MediaRouter$ProviderInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1, v5}, Lcom/google/android/music/cast/CastTokenClient;->getCastToken(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 991
    .end local v1    # "remoteEndpointId":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    .restart local v1    # "remoteEndpointId":Ljava/lang/String;
    goto :goto_0
.end method

.method private declared-synchronized handleEvent(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 5
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "DialRemotePlaybackClient"

    const-string v1, "Got event %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/music/dial/model/GenericDialEvent;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_0
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mGroupCoordinatorChangedEventJson:Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;

    if-eqz v0, :cond_2

    .line 220
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onGroupCoordinatorChangedEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 221
    :cond_2
    :try_start_1
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mGroupVolumeEventJson:Lcom/google/android/music/dial/model/GroupVolumeEventJson;

    if-eqz v0, :cond_3

    .line 222
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onGroupVolumeEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 223
    :cond_3
    :try_start_2
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackStatusEventJson:Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    if-eqz v0, :cond_4

    .line 224
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onPlaybackStatusEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V

    goto :goto_0

    .line 225
    :cond_4
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionStatusEventJson:Lcom/google/android/music/dial/model/SessionStatusEventJson;

    if-eqz v0, :cond_5

    .line 226
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onSessionStatusEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V

    goto :goto_0

    .line 227
    :cond_5
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mSubscribeResponseJson:Lcom/google/android/music/dial/model/SubscribeResponseJson;

    if-eqz v0, :cond_6

    .line 228
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onSubscribeResponse(Lcom/google/android/music/dial/model/GenericDialEvent;)V

    goto :goto_0

    .line 229
    :cond_6
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mSuccessResponseJson:Lcom/google/android/music/dial/model/SuccessResponseJson;

    if-eqz v0, :cond_7

    .line 230
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onSuccessResponse(Lcom/google/android/music/dial/model/GenericDialEvent;)V

    goto :goto_0

    .line 231
    :cond_7
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackErrorEventJson:Lcom/google/android/music/dial/model/PlaybackErrorEventJson;

    if-eqz v0, :cond_8

    .line 232
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onPlaybackErrorEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V

    goto :goto_0

    .line 233
    :cond_8
    iget-object v0, p2, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionErrorEventJson:Lcom/google/android/music/dial/model/SessionErrorEventJson;

    if-eqz v0, :cond_1

    .line 234
    invoke-direct {p0, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->onSessionErrorEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private handleExistingSession()V
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 392
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient$3;-><init>(Lcom/google/android/music/dial/DialRemotePlaybackClient;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 418
    return-void
.end method

.method private handleNewSession()V
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 370
    new-instance v0, Lcom/google/android/music/dial/DialRemotePlaybackClient$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient$2;-><init>(Lcom/google/android/music/dial/DialRemotePlaybackClient;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 386
    return-void
.end method

.method private onGroupCoordinatorChangedEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 10
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 239
    iget-object v1, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mGroupCoordinatorChangedEventJson:Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;

    .line 240
    .local v1, "gcEvent":Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;
    iget-object v2, v1, Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;->mGroupId:Ljava/lang/String;

    .line 242
    .local v2, "groupId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 243
    const/4 v3, 0x0

    .line 245
    .local v3, "groupStatus":Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    :try_start_0
    iget-object v4, v1, Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;->mGroupStatus:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 252
    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 253
    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v5, "%s for %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    aput-object v2, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 255
    :cond_0
    sget-object v4, Lcom/google/android/music/dial/DialRemotePlaybackClient$4;->$SwitchMap$com$google$android$music$dial$DialRemotePlaybackClient$GroupStatus:[I

    invoke-virtual {v3}, Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 274
    .end local v3    # "groupStatus":Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    :cond_1
    :goto_0
    return-void

    .line 246
    .restart local v3    # "groupStatus":Lcom/google/android/music/dial/DialRemotePlaybackClient$GroupStatus;
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "DialRemotePlaybackClient"

    const-string v5, "Got invalid group status in GroupCoordinatorChangedEvent - %s"

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, v1, Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;->mGroupStatus:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 257
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :pswitch_0
    sget-boolean v4, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v4, :cond_2

    .line 258
    const-string v4, "DialRemotePlaybackClient"

    const-string v5, "Group coordinator moved for %s"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mDevice:Lcom/google/android/music/dial/DialDevice;

    iget-object v5, v1, Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;->mGroupName:Ljava/lang/String;

    iget-object v6, v1, Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;->mWebsocketUrl:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/music/dial/DialDevice;->update(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 263
    :pswitch_1
    sget-boolean v4, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v4, :cond_3

    .line 264
    const-string v4, "DialRemotePlaybackClient"

    const-string v5, "Group coordinator gone for %s.  Tearing down session"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->tearDownSession()V

    goto :goto_0

    .line 255
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onGroupVolumeEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 277
    iget-object v0, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mGroupVolumeEventJson:Lcom/google/android/music/dial/model/GroupVolumeEventJson;

    iget v0, v0, Lcom/google/android/music/dial/model/GroupVolumeEventJson;->mVolume:I

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mVolume:F

    .line 278
    iget-object v0, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mGroupVolumeEventJson:Lcom/google/android/music/dial/model/GroupVolumeEventJson;

    iget-boolean v0, v0, Lcom/google/android/music/dial/model/GroupVolumeEventJson;->mMuted:Z

    iput-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMuted:Z

    .line 280
    const-string v0, "setVolume"

    iget-object v1, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v1, v1, Lcom/google/android/music/dial/model/EventHeaderJson;->mResponse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "setRelativeVolume"

    iget-object v1, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v1, v1, Lcom/google/android/music/dial/model/EventHeaderJson;->mResponse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    iget-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMuted:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->onVolumeChanged(I)V

    .line 284
    :cond_0
    return-void

    .line 282
    :cond_1
    iget v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mVolume:F

    float-to-int v0, v0

    goto :goto_0
.end method

.method private onPlaybackErrorEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 7
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    const/4 v6, 0x0

    .line 433
    iget-object v3, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackErrorEventJson:Lcom/google/android/music/dial/model/PlaybackErrorEventJson;

    iget-object v1, v3, Lcom/google/android/music/dial/model/PlaybackErrorEventJson;->mErrorCode:Ljava/lang/String;

    .line 434
    .local v1, "errorCode":Ljava/lang/String;
    const/4 v2, 0x0

    .line 436
    .local v2, "playbackState":Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    :try_start_0
    invoke-static {v1}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 445
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 446
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Playback error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 448
    :cond_0
    sget-object v3, Lcom/google/android/music/dial/DialRemotePlaybackClient$4;->$SwitchMap$com$google$android$music$dial$DialRemotePlaybackClient$PlaybackState:[I

    invoke-virtual {v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 462
    :cond_1
    :goto_0
    return-void

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "DialRemotePlaybackClient"

    const-string v4, "Got invalid error code in PlaybackErrorEvent - %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 441
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PlaybackError bad error code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 450
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :pswitch_0
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsStreaming:Z

    .line 451
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsPreparing:Z

    .line 452
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 453
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v3}, Lcom/google/android/music/playback/StopWatch;->pause()V

    goto :goto_0

    .line 456
    :pswitch_1
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 457
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v3}, Lcom/google/android/music/playback/StopWatch;->pause()V

    goto :goto_0

    .line 448
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onPlaybackStatusEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 288
    :try_start_0
    iget-object v2, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackStatusEventJson:Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    iget-object v2, v2, Lcom/google/android/music/dial/model/PlaybackStatusEventJson;->mPlaybackState:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPlaybackState:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    sget-boolean v2, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 296
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Playback state: %s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPlaybackState:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    invoke-virtual {v5}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_0
    iget-object v2, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackStatusEventJson:Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    iget v2, v2, Lcom/google/android/music/dial/model/PlaybackStatusEventJson;->mPositionMillis:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mKnownPositionMillis:J

    .line 299
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 300
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;

    .line 301
    .local v1, "previousItemId":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackStatusEventJson:Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    iget-object v2, v2, Lcom/google/android/music/dial/model/PlaybackStatusEventJson;->mItemId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;

    .line 303
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 304
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v3, "Playback state: %s item:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPlaybackState:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    invoke-virtual {v5}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 308
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    if-eqz v2, :cond_2

    .line 309
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mItemId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->onCloudQueueTrackChanged(Ljava/lang/String;)V

    .line 312
    :cond_2
    sget-object v2, Lcom/google/android/music/dial/DialRemotePlaybackClient$4;->$SwitchMap$com$google$android$music$dial$DialRemotePlaybackClient$PlaybackState:[I

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPlaybackState:Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;

    invoke-virtual {v3}, Lcom/google/android/music/dial/DialRemotePlaybackClient$PlaybackState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 343
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 344
    .end local v1    # "previousItemId":Ljava/lang/String;
    :goto_1
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got invalid playback state "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackStatusEventJson:Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    iget-object v4, v4, Lcom/google/android/music/dial/model/PlaybackStatusEventJson;->mPlaybackState:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 314
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "previousItemId":Ljava/lang/String;
    :pswitch_0
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsStreaming:Z

    .line 315
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsPreparing:Z

    .line 316
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->pause()V

    goto :goto_0

    .line 320
    :pswitch_1
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsStreaming:Z

    .line 321
    iput-boolean v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsPreparing:Z

    .line 322
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->pause()V

    goto :goto_0

    .line 326
    :pswitch_2
    iput-boolean v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsStreaming:Z

    .line 327
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsPreparing:Z

    .line 328
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->start()V

    .line 329
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenComplete()V

    .line 330
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackPlaying()V

    goto :goto_0

    .line 334
    :pswitch_3
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsStreaming:Z

    .line 335
    iput-boolean v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsPreparing:Z

    .line 336
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 337
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackPaused()V

    goto :goto_0

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private onSessionErrorEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 465
    iget-object v3, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionErrorEventJson:Lcom/google/android/music/dial/model/SessionErrorEventJson;

    iget-object v1, v3, Lcom/google/android/music/dial/model/SessionErrorEventJson;->mErrorCode:Ljava/lang/String;

    .line 466
    .local v1, "errorCode":Ljava/lang/String;
    const/4 v2, 0x0

    .line 468
    .local v2, "sessionState":Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    :try_start_0
    invoke-static {v1}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 478
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 479
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Session error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 482
    :cond_0
    sget-object v3, Lcom/google/android/music/dial/DialRemotePlaybackClient$4;->$SwitchMap$com$google$android$music$dial$DialRemotePlaybackClient$SessionState:[I

    invoke-virtual {v2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 503
    :cond_1
    :goto_0
    return-void

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "DialRemotePlaybackClient"

    const-string v4, "Got invalid error code in SessionErrorEvent - %s"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v1, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 473
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Session inv error code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 486
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->forceCreateSession()V

    goto :goto_0

    .line 489
    :pswitch_1
    sget-boolean v3, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v3, :cond_2

    .line 490
    const-string v3, "DialRemotePlaybackClient"

    const-string v4, "Leaving session %s because of eviction."

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->tearDownSession()V

    goto :goto_0

    .line 498
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->tearDownSession()V

    goto :goto_0

    .line 482
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onSessionStatusEvent(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 347
    iget-object v0, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionStatusEventJson:Lcom/google/android/music/dial/model/SessionStatusEventJson;

    iget-object v0, v0, Lcom/google/android/music/dial/model/SessionStatusEventJson;->mSessionId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    const-string v0, "DialRemotePlaybackClient"

    const-string v1, "Invalid sessionId.  Cannot assign mSessionId in onSessionStatusEvent."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_0
    iget-object v0, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionStatusEventJson:Lcom/google/android/music/dial/model/SessionStatusEventJson;

    iget-object v0, v0, Lcom/google/android/music/dial/model/SessionStatusEventJson;->mSessionState:Ljava/lang/String;

    sget-object v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->CONNECTED:Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->value:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;->access$100(Lcom/google/android/music/dial/DialRemotePlaybackClient$SessionState;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsSessionInitialized:Z

    .line 353
    iget-object v0, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionStatusEventJson:Lcom/google/android/music/dial/model/SessionStatusEventJson;

    iget-boolean v0, v0, Lcom/google/android/music/dial/model/SessionStatusEventJson;->mSessionCreated:Z

    if-eqz v0, :cond_3

    .line 355
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->handleNewSession()V

    .line 362
    :cond_1
    :goto_0
    iget-object v0, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionStatusEventJson:Lcom/google/android/music/dial/model/SessionStatusEventJson;

    iget-object v0, v0, Lcom/google/android/music/dial/model/SessionStatusEventJson;->mSessionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    .line 363
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 364
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Session start: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 366
    :cond_2
    return-void

    .line 358
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->handleExistingSession()V

    goto :goto_0
.end method

.method private onSubscribeResponse(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 422
    return-void
.end method

.method private onSuccessResponse(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 425
    const-string v0, "leaveSession"

    iget-object v1, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v1, v1, Lcom/google/android/music/dial/model/EventHeaderJson;->mResponse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->tearDownSession()V

    .line 430
    :cond_0
    :goto_0
    return-void

    .line 427
    :cond_1
    const-string v0, "loadCloudQueue"

    iget-object v1, p1, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v1, v1, Lcom/google/android/music/dial/model/EventHeaderJson;->mResponse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    goto :goto_0
.end method

.method private sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V
    .locals 3
    .param p1, "command"    # Lcom/google/android/music/dial/model/GenericDialCommand;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/dial/MalformedDialCommandException;
        }
    .end annotation

    .prologue
    .line 1018
    sget-boolean v0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v0, :cond_0

    .line 1019
    const-string v0, "DialRemotePlaybackClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sending command "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/dial/model/GenericDialCommand;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mDevice:Lcom/google/android/music/dial/DialDevice;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/music/dial/DialDevice;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;Lcom/google/android/music/dial/DialDevice$Callback;)V

    .line 1022
    return-void
.end method

.method private declared-synchronized subscribe(Ljava/lang/String;)Z
    .locals 8
    .param p1, "groupId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 530
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/music/dial/model/SubscribeCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/SubscribeCommandJson;-><init>()V

    .line 531
    .local v0, "cmd":Lcom/google/android/music/dial/model/SubscribeCommandJson;
    iget-object v3, v0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mBody:Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;

    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;->mNamespaces:Ljava/util/List;

    .line 532
    iget-object v3, v0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mBody:Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;

    iget-object v3, v3, Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;->mNamespaces:Ljava/util/List;

    new-instance v4, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;

    const-string v5, "session"

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct {v4, v5, v6, v7, p1}, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 533
    iget-object v3, v0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mBody:Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;

    iget-object v3, v3, Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;->mNamespaces:Ljava/util/List;

    new-instance v4, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;

    const-string v5, "playback"

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct {v4, v5, v6, v7, p1}, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    iget-object v3, v0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mBody:Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;

    iget-object v3, v3, Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;->mNamespaces:Ljava/util/List;

    new-instance v4, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;

    const-string v5, "groupvolume"

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct {v4, v5, v6, v7, p1}, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 538
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 539
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v4, "Subscribe"

    invoke-static {v3, v4}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 545
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 541
    :catch_0
    move-exception v1

    .line 542
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_2
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Malformed command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 543
    const/4 v2, 0x0

    goto :goto_0

    .line 530
    .end local v0    # "cmd":Lcom/google/android/music/dial/model/SubscribeCommandJson;
    .end local v1    # "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private tearDownSession()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 506
    sget-boolean v0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v0, :cond_0

    .line 507
    const-string v0, "DialRemotePlaybackClient"

    const-string v1, "Tearing down socket for session %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mDevice:Lcom/google/android/music/dial/DialDevice;

    invoke-virtual {v0}, Lcom/google/android/music/dial/DialDevice;->teardown()V

    .line 510
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    .line 511
    iput-boolean v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 512
    iput-boolean v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsSessionInitialized:Z

    .line 513
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    .line 514
    return-void
.end method


# virtual methods
.method public declared-synchronized getPosition()J
    .locals 6

    .prologue
    .line 910
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mKnownPositionMillis:J

    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v4}, Lcom/google/android/music/playback/StopWatch;->getTime()J

    move-result-wide v4

    add-long v0, v2, v4

    .line 911
    .local v0, "position":J
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    if-eqz v2, :cond_0

    .line 912
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 914
    .end local v0    # "position":J
    :cond_0
    monitor-exit p0

    return-wide v0

    .line 910
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized init()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 182
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;

    if-nez v1, :cond_0

    .line 183
    const-string v1, "DialRemotePlaybackClient"

    const-string v2, "Invalid RemoteDeviceInfo"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :goto_0
    monitor-exit p0

    return v0

    .line 186
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 187
    const-string v1, "DialRemotePlaybackClient"

    const-string v2, "Invalid group ID"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 191
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mDevice:Lcom/google/android/music/dial/DialDevice;

    invoke-virtual {v1}, Lcom/google/android/music/dial/DialDevice;->setup()V

    .line 194
    iget-object v1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->subscribe(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 195
    const-string v1, "DialRemotePlaybackClient"

    const-string v2, "Error subscribing to namespaces"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 199
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized isPreparing()Z
    .locals 1

    .prologue
    .line 981
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsPreparing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isQueueLoaded()Z
    .locals 1

    .prologue
    .line 966
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isSessionInitialized()Z
    .locals 1

    .prologue
    .line 971
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsSessionInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isStreaming()Z
    .locals 1

    .prologue
    .line 976
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsStreaming:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized joinSession(Ljava/lang/String;)V
    .locals 7
    .param p1, "appContext"    # Ljava/lang/String;

    .prologue
    .line 550
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 551
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "joinSession %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 555
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid groupID.  Cannot joinSession."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 558
    :cond_2
    :try_start_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 559
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid appContext.  Cannot joinSession."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 550
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 563
    :cond_3
    :try_start_2
    new-instance v0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;-><init>()V

    .line 564
    .local v0, "cmd":Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;
    iget-object v2, v0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->mGroupId:Ljava/lang/String;

    .line 565
    iget-object v2, v0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    const-string v3, "com.google.RemoteSonosReceiver"

    iput-object v3, v2, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->mAppId:Ljava/lang/String;

    .line 566
    iget-object v2, v0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    iput-object p1, v2, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->mAppContext:Ljava/lang/String;

    .line 568
    iput-object p1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mAppContext:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 571
    :try_start_3
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 572
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 573
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v3, "Join session"

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 575
    :catch_0
    move-exception v1

    .line 576
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_4
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Malformed command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized leaveSession()V
    .locals 7

    .prologue
    .line 612
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 613
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "leaveSession %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 617
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid sessionID.  Cannot leaveSession."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 647
    :goto_0
    monitor-exit p0

    return-void

    .line 621
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/music/dial/model/LeaveSessionCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/LeaveSessionCommandJson;-><init>()V

    .line 622
    .local v0, "cmd":Lcom/google/android/music/dial/model/LeaveSessionCommandJson;
    iget-object v2, v0, Lcom/google/android/music/dial/model/LeaveSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/LeaveSessionCommandJson$LeaveSessionCommandBody;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/dial/model/LeaveSessionCommandJson$LeaveSessionCommandBody;->mSessionId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 625
    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 626
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 627
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v3, "Leave session"

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 646
    :cond_2
    :goto_1
    :try_start_3
    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->tearDownSession()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 612
    .end local v0    # "cmd":Lcom/google/android/music/dial/model/LeaveSessionCommandJson;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 629
    .restart local v0    # "cmd":Lcom/google/android/music/dial/model/LeaveSessionCommandJson;
    :catch_0
    move-exception v1

    .line 630
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_4
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Malformed command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized loadCloudQueue(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
    .locals 14
    .param p1, "queueBaseUrl"    # Ljava/lang/String;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J
    .param p6, "playOnCompletion"    # Z

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    sget-boolean v8, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v8, :cond_0

    .line 653
    const-string v8, "DialRemotePlaybackClient"

    const-string v9, "loadCloudQueue %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v12}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    :cond_0
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 658
    const-string v8, "DialRemotePlaybackClient"

    const-string v9, "Invalid sessionId.  Cannot loadCloudQueue."

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 708
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 661
    :cond_2
    :try_start_1
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 662
    const-string v8, "DialRemotePlaybackClient"

    const-string v9, "Invalid itemId.  Cannot loadCloudQueue."

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 652
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 665
    :cond_3
    const-wide/16 v8, 0x0

    cmp-long v8, p4, v8

    if-gez v8, :cond_4

    .line 666
    :try_start_2
    const-string v8, "DialRemotePlaybackClient"

    const-string v9, "Invalid positionMillis.  Cannot loadCloudQueue."

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 670
    :cond_4
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 672
    new-instance v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;

    invoke-direct {v3}, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;-><init>()V

    .line 673
    .local v3, "cmd":Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    iput-object p1, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mQueueBaseUrl:Ljava/lang/String;

    .line 674
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    move/from16 v0, p6

    iput-boolean v0, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mPlayOnCompletion:Z

    .line 675
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    iget-object v9, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    iput-object v9, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mSessionId:Ljava/lang/String;

    .line 676
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    move-object/from16 v0, p3

    iput-object v0, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mItemId:Ljava/lang/String;

    .line 677
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    const-string v9, "application/x-cloud-queue"

    iput-object v9, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mContentType:Ljava/lang/String;

    .line 678
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    move-wide/from16 v0, p4

    iput-wide v0, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mPositionMillis:J

    .line 679
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    .line 680
    .local v5, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v8, "Authorization"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "playon="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-direct {p0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->getFreshCastToken()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    iput-object v5, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mHttpHeaders:Ljava/util/Map;

    .line 684
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-static {v8, v9, v10}, Lcom/google/android/music/cast/CastUtils;->generateMplayUrl(Landroid/content/Context;ZLcom/google/android/music/store/MusicFile;)Ljava/lang/String;

    move-result-object v6

    .line 686
    .local v6, "mplayUrl":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/store/MusicFile;->getAlbumArtLocation()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/utils/AlbumArtUtils;->stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 688
    .local v2, "albumArtLocation":Ljava/lang/String;
    new-instance v7, Lcom/google/android/music/dial/model/TrackMetadataJson;

    invoke-direct {v7}, Lcom/google/android/music/dial/model/TrackMetadataJson;-><init>()V

    .line 689
    .local v7, "trackMetadata":Lcom/google/android/music/dial/model/TrackMetadataJson;
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v8}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mTrackTitle:Ljava/lang/String;

    .line 690
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v8}, Lcom/google/android/music/store/MusicFile;->getTrackArtist()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mTrackArtist:Ljava/lang/String;

    .line 691
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v8}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mAlbumArtist:Ljava/lang/String;

    .line 692
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v8}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mAlbumTitle:Ljava/lang/String;

    .line 693
    iput-object v2, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mAlbumArtUrl:Ljava/lang/String;

    .line 694
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v8}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v8

    iput-wide v8, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mDurationMillis:J

    .line 695
    iput-object v6, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mTrackUrl:Ljava/lang/String;

    .line 696
    const-string v8, "audio/mpeg"

    iput-object v8, v7, Lcom/google/android/music/dial/model/TrackMetadataJson;->mContentType:Ljava/lang/String;

    .line 697
    iget-object v8, v3, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;

    iput-object v7, v8, Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;->mFirstTrackMetadata:Lcom/google/android/music/dial/model/TrackMetadataJson;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 700
    :try_start_3
    invoke-direct {p0, v3}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 701
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 702
    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v9, "Load Queue item:%s\n%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p3, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v12}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 705
    :catch_0
    move-exception v4

    .line 706
    .local v4, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_4
    const-string v8, "DialRemotePlaybackClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Malformed command "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public onError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V
    .locals 5
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "error"    # Lcom/google/android/music/dial/DialDevice$Listener$Error;

    .prologue
    .line 518
    sget-boolean v0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v0, :cond_0

    .line 519
    const-string v0, "DialRemotePlaybackClient"

    const-string v1, "Got error event %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/music/dial/DialDevice$Listener$Error;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :cond_0
    return-void
.end method

.method public onMessageReceived(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
    .locals 2
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 206
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/dial/DialRemotePlaybackClient$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/music/dial/DialRemotePlaybackClient$1;-><init>(Lcom/google/android/music/dial/DialRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 212
    return-void
.end method

.method public declared-synchronized pause()V
    .locals 7

    .prologue
    .line 839
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 840
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "pause %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 845
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid groupID.  Cannot joinSession."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 860
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 849
    :cond_2
    :try_start_1
    new-instance v0, Lcom/google/android/music/dial/model/PauseCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/PauseCommandJson;-><init>()V

    .line 850
    .local v0, "cmd":Lcom/google/android/music/dial/model/PauseCommandJson;
    iget-object v2, v0, Lcom/google/android/music/dial/model/PauseCommandJson;->mBody:Lcom/google/android/music/dial/model/PauseCommandJson$PauseCommandBody;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/dial/model/PauseCommandJson$PauseCommandBody;->mGroupId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 853
    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 854
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 855
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v3, "Pause"

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 857
    :catch_0
    move-exception v1

    .line 858
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_3
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Malformed command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 839
    .end local v0    # "cmd":Lcom/google/android/music/dial/model/PauseCommandJson;
    .end local v1    # "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized play()V
    .locals 7

    .prologue
    .line 814
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 815
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "play %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 820
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid groupID.  Cannot joinSession."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 835
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 824
    :cond_2
    :try_start_1
    new-instance v0, Lcom/google/android/music/dial/model/PlayCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/PlayCommandJson;-><init>()V

    .line 825
    .local v0, "cmd":Lcom/google/android/music/dial/model/PlayCommandJson;
    iget-object v2, v0, Lcom/google/android/music/dial/model/PlayCommandJson;->mBody:Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/dial/model/PlayCommandJson$PlayCommandBody;->mGroupId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 828
    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 829
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 830
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v3, "Play"

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 832
    :catch_0
    move-exception v1

    .line 833
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_3
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Malformed command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 814
    .end local v0    # "cmd":Lcom/google/android/music/dial/model/PlayCommandJson;
    .end local v1    # "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized refreshCloudQueue()V
    .locals 8

    .prologue
    .line 938
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 939
    .local v2, "now":J
    iget-wide v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mLastRefreshTimestampMillis:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x3e8

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 942
    iput-wide v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mLastRefreshTimestampMillis:J

    .line 944
    new-instance v0, Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;-><init>()V

    .line 945
    .local v0, "cmd":Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;
    iget-object v4, v0, Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;->mBody:Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson$RefreshCloudQueueCommandBody;

    iget-object v5, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson$RefreshCloudQueueCommandBody;->mSessionId:Ljava/lang/String;

    .line 946
    sget-boolean v4, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v4, :cond_0

    .line 947
    const-string v4, "DialRemotePlaybackClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Issuing refresh command: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 950
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 951
    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 952
    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v5, "Refresh cloud queue"

    invoke-static {v4, v5}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 962
    .end local v0    # "cmd":Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 954
    .restart local v0    # "cmd":Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;
    :catch_0
    move-exception v1

    .line 955
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_2
    const-string v4, "DialRemotePlaybackClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Malformed command "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 938
    .end local v0    # "cmd":Lcom/google/android/music/dial/model/RefreshCloudQueueCommandJson;
    .end local v1    # "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    .end local v2    # "now":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 958
    .restart local v2    # "now":J
    :cond_2
    :try_start_3
    sget-boolean v4, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v4, :cond_1

    .line 959
    const-string v4, "DialRemotePlaybackClient"

    const-string v5, "Rate-limiting refreshCloudQueue"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized seek(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7
    .param p1, "queueVersion"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "positionMillis"    # J

    .prologue
    .line 712
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 713
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "seek %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v6}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 718
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid sessionId.  Cannot seek."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 743
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 721
    :cond_2
    :try_start_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 722
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid itemId.  Cannot seek."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 712
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 725
    :cond_3
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-gez v2, :cond_4

    .line 726
    :try_start_2
    const-string v2, "DialRemotePlaybackClient"

    const-string v3, "Invalid positionMillis.  Cannot seek."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 730
    :cond_4
    new-instance v0, Lcom/google/android/music/dial/model/SeekCommandJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/SeekCommandJson;-><init>()V

    .line 731
    .local v0, "cmd":Lcom/google/android/music/dial/model/SeekCommandJson;
    iget-object v2, v0, Lcom/google/android/music/dial/model/SeekCommandJson;->mBody:Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;

    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;->mSessionId:Ljava/lang/String;

    .line 732
    iget-object v2, v0, Lcom/google/android/music/dial/model/SeekCommandJson;->mBody:Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;

    iput-object p2, v2, Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;->mItemId:Ljava/lang/String;

    .line 733
    iget-object v2, v0, Lcom/google/android/music/dial/model/SeekCommandJson;->mBody:Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;

    iput-wide p3, v2, Lcom/google/android/music/dial/model/SeekCommandJson$SeekCommandBody;->mPositionMillis:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 736
    :try_start_3
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 737
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 738
    iget-object v2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v3, "Seek"

    invoke-static {v2, v3}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 740
    :catch_0
    move-exception v1

    .line 741
    .local v1, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_4
    const-string v2, "DialRemotePlaybackClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Malformed command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized setVolume(D)V
    .locals 7
    .param p1, "volume"    # D

    .prologue
    .line 869
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 871
    .local v0, "actualVolume":Ljava/lang/Double;
    new-instance v1, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;

    invoke-direct {v1}, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;-><init>()V

    .line 872
    .local v1, "cmd":Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;
    iget-object v3, v1, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;->mBody:Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;

    iget-object v4, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mGroupId:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;->mGroupId:Ljava/lang/String;

    .line 873
    iget-object v3, v1, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;->mBody:Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v4

    iput v4, v3, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;->mVolume:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 876
    :try_start_1
    invoke-direct {p0, v1}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 877
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 878
    iget-object v3, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Set volume to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 883
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 880
    :catch_0
    move-exception v2

    .line 881
    .local v2, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_2
    const-string v3, "DialRemotePlaybackClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Malformed command "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 869
    .end local v0    # "actualVolume":Ljava/lang/Double;
    .end local v1    # "cmd":Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;
    .end local v2    # "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized skipToItem(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
    .locals 12
    .param p1, "queueVersion"    # Ljava/lang/String;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J
    .param p6, "playOnCompletion"    # Z

    .prologue
    .line 748
    monitor-enter p0

    :try_start_0
    sget-boolean v7, Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z

    if-eqz v7, :cond_0

    .line 749
    const-string v7, "DialRemotePlaybackClient"

    const-string v8, "skipToItem route:%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v11}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 753
    const-string v7, "DialRemotePlaybackClient"

    const-string v8, "Invalid queueVersion.  Cannot skipToItem."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 756
    :cond_2
    :try_start_1
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 757
    const-string v7, "DialRemotePlaybackClient"

    const-string v8, "Invalid sessionId.  Cannot skipToItem."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 748
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 760
    :cond_3
    :try_start_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 761
    const-string v7, "DialRemotePlaybackClient"

    const-string v8, "Invalid itemId.  Cannot skipToItem."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 764
    :cond_4
    const-wide/16 v8, 0x0

    cmp-long v7, p4, v8

    if-gez v7, :cond_5

    .line 765
    const-string v7, "DialRemotePlaybackClient"

    const-string v8, "Invalid positionMillis.  Cannot skipToItem."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 768
    :cond_5
    if-nez p2, :cond_6

    .line 769
    const-string v7, "DialRemotePlaybackClient"

    const-string v8, "Invalid musicFile.  Cannot skipToItem."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 772
    :cond_6
    iget-boolean v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mIsQueueLoaded:Z

    if-nez v7, :cond_7

    .line 773
    const-string v7, "DialRemotePlaybackClient"

    const-string v8, "The queue has not been loaded yet.  Cannot skipToItem."

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 777
    :cond_7
    iput-object p2, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 779
    new-instance v3, Lcom/google/android/music/dial/model/SkipToItemCommandJson;

    invoke-direct {v3}, Lcom/google/android/music/dial/model/SkipToItemCommandJson;-><init>()V

    .line 780
    .local v3, "cmd":Lcom/google/android/music/dial/model/SkipToItemCommandJson;
    iget-object v7, v3, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    iput-object p1, v7, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;->mQueueVersion:Ljava/lang/String;

    .line 781
    iget-object v7, v3, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    iget-object v8, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    iput-object v8, v7, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;->mSessionId:Ljava/lang/String;

    .line 782
    iget-object v7, v3, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    iput-object p3, v7, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;->mItemId:Ljava/lang/String;

    .line 783
    iget-object v7, v3, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    move-wide/from16 v0, p4

    iput-wide v0, v7, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;->mPositionMillis:J

    .line 784
    iget-object v7, v3, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    move/from16 v0, p6

    iput-boolean v0, v7, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;->mPlayOnCompletion:Z

    .line 786
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-static {v7, v8, v9}, Lcom/google/android/music/cast/CastUtils;->generateMplayUrl(Landroid/content/Context;ZLcom/google/android/music/store/MusicFile;)Ljava/lang/String;

    move-result-object v5

    .line 788
    .local v5, "mplayUrl":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getAlbumArtLocation()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/music/utils/AlbumArtUtils;->stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 790
    .local v2, "albumArtLocation":Ljava/lang/String;
    new-instance v6, Lcom/google/android/music/dial/model/TrackMetadataJson;

    invoke-direct {v6}, Lcom/google/android/music/dial/model/TrackMetadataJson;-><init>()V

    .line 791
    .local v6, "trackMetadata":Lcom/google/android/music/dial/model/TrackMetadataJson;
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mTrackTitle:Ljava/lang/String;

    .line 792
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->getTrackArtist()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mTrackArtist:Ljava/lang/String;

    .line 793
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mAlbumArtist:Ljava/lang/String;

    .line 794
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mAlbumTitle:Ljava/lang/String;

    .line 795
    iput-object v2, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mAlbumArtUrl:Ljava/lang/String;

    .line 796
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v8

    iput-wide v8, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mDurationMillis:J

    .line 797
    iput-object v5, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mTrackUrl:Ljava/lang/String;

    .line 798
    const-string v7, "audio/mpeg"

    iput-object v7, v6, Lcom/google/android/music/dial/model/TrackMetadataJson;->mContentType:Ljava/lang/String;

    .line 799
    iget-object v7, v3, Lcom/google/android/music/dial/model/SkipToItemCommandJson;->mBody:Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;

    iput-object v6, v7, Lcom/google/android/music/dial/model/SkipToItemCommandJson$SkipToItemCommandBody;->mTrackMetadata:Lcom/google/android/music/dial/model/TrackMetadataJson;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 802
    :try_start_3
    invoke-direct {p0, v3}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->sendCommand(Lcom/google/android/music/dial/model/GenericDialCommand;)V

    .line 803
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/music/dial/Utils;->isDebugToastEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 804
    iget-object v7, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;

    const-string v8, "Skip to item:%s\n%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p3, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v11}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/dial/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/music/dial/MalformedDialCommandException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 807
    :catch_0
    move-exception v4

    .line 808
    .local v4, "e":Lcom/google/android/music/dial/MalformedDialCommandException;
    :try_start_4
    const-string v7, "DialRemotePlaybackClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Malformed command "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method
