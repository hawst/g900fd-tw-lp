.class Lcom/google/android/music/ui/BaseActivity$5;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/BaseActivity;->setupNowPlayingBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$5;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDragEnded(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 0
    .param p1, "source"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "endState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 480
    return-void
.end method

.method public onDragStarted(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 0
    .param p1, "source"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "currentState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 476
    return-void
.end method

.method public onExpandingStateChanged(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 9
    .param p1, "drawer"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "oldState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .param p3, "newState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 428
    # getter for: Lcom/google/android/music/ui/BaseActivity;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/BaseActivity;->access$800()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 429
    const-string v6, "MusicBaseActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bottom state changed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity$5;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v6}, Lcom/google/android/music/ui/BaseActivity;->getContentContainer()Landroid/view/ViewGroup;

    move-result-object v1

    .line 435
    .local v1, "contentContainer":Landroid/view/ViewGroup;
    const/high16 v2, 0x40000

    .line 436
    .local v2, "contentFocusability":I
    const/high16 v3, 0x40000

    .line 438
    .local v3, "drawerFocusability":I
    sget-object v6, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne p3, v6, :cond_1

    .line 439
    const/high16 v3, 0x60000

    .line 442
    :cond_1
    sget-object v6, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne p3, v6, :cond_2

    .line 443
    const/high16 v2, 0x60000

    .line 447
    :cond_2
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity$5;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # getter for: Lcom/google/android/music/ui/BaseActivity;->mContentViews:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/google/android/music/ui/BaseActivity;->access$900(Lcom/google/android/music/ui/BaseActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 448
    .local v5, "vg":Landroid/view/ViewGroup;
    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    goto :goto_0

    .line 450
    .end local v5    # "vg":Landroid/view/ViewGroup;
    :cond_3
    invoke-virtual {p1, v3}, Lcom/google/android/music/widgets/ExpandingScrollView;->setDescendantFocusability(I)V

    .line 454
    sget-object v6, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne p3, v6, :cond_5

    .line 455
    const/4 v0, 0x0

    .line 460
    .local v0, "bottomPadding":I
    :goto_1
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v6

    if-eq v0, v6, :cond_4

    .line 462
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v6

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v7

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v8

    invoke-virtual {v1, v6, v7, v8, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 465
    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    .line 467
    :cond_4
    return-void

    .line 457
    .end local v0    # "bottomPadding":I
    :cond_5
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity$5;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v6}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f00d6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .restart local v0    # "bottomPadding":I
    goto :goto_1
.end method

.method public onMoving(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;F)V
    .locals 0
    .param p1, "source"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p2, "baseState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .param p3, "ratio"    # F

    .prologue
    .line 472
    return-void
.end method
