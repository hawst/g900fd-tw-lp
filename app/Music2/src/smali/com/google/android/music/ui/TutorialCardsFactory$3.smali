.class final Lcom/google/android/music/ui/TutorialCardsFactory$3;
.super Ljava/lang/Object;
.source "TutorialCardsFactory.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainstageWearCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$card:Landroid/widget/LinearLayout;

.field final synthetic val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

.field final synthetic val$fragment:Lcom/google/android/music/ui/MusicFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$3;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/TutorialCardsFactory$3;->val$card:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$3;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/music/ui/TutorialCardsFactory$3;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$3;->val$card:Landroid/widget/LinearLayout;

    const-string v2, "MainstageWear"

    iget-object v3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$3;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    # invokes: Lcom/google/android/music/ui/TutorialCardsFactory;->dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/ui/TutorialCardsFactory;->access$100(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    .line 265
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setWearSyncEnabled(Z)V

    .line 266
    return-void
.end method
