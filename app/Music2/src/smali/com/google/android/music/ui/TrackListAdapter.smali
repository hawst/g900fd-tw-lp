.class public Lcom/google/android/music/ui/TrackListAdapter;
.super Lcom/google/android/music/ui/MediaListCursorAdapter;
.source "TrackListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    }
.end annotation


# static fields
.field static PROJECTION:[Ljava/lang/String;


# instance fields
.field mAlbumArtistIdIdx:I

.field mAlbumIdIdx:I

.field mAlbumIdx:I

.field mAlbumMetajamIdx:I

.field mAlbumStoreIdIdx:I

.field mArtistIdx:I

.field mArtistMetajamIdx:I

.field mArtworkUrlIdx:I

.field mAudioIdIdx:I

.field private mCallback:Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;

.field private mCanShowPlayIndicator:Ljava/lang/Boolean;

.field private mContextClickListener:Landroid/view/View$OnClickListener;

.field private mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;

.field private mCurrentPlayPosition:I

.field private mCurrentTrackMetajamId:Ljava/lang/String;

.field mDomainIdx:I

.field private mDragHandleBG:Landroid/graphics/drawable/Drawable;

.field mDurationIdx:I

.field private mEditMode:Z

.field mHasRemoteIdx:I

.field private mHighlightTrackMetajamId:Ljava/lang/String;

.field private mHighlightTrackSongId:J

.field private mIsContainerCurrentlyPlaying:Z

.field private mIsInInfiniteMix:Z

.field mIsLocalIdx:I

.field private mIsMaterialEnabled:Z

.field mPlaylistMemberIdIdx:I

.field mQueueContainerNameIdx:I

.field mQueueContainerTypeIdx:I

.field mQueueItemContainerIdIdx:I

.field mQueueItemStateIdx:I

.field private mRemovedItemsCount:I

.field private mShowAlbumArt:Z

.field private mShowTrackArtist:Z

.field mSongIdIdx:I

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field mStoreSongIdx:I

.field private mTempRowMapping:Landroid/util/SparseIntArray;

.field mTitleIdx:I

.field mTrackArtistIdIdx:I

.field mTrackMetajamIdx:I

.field mTrackNumberIdx:I

.field private mUnknownAlbum:Ljava/lang/String;

.field private mUnknownArtist:Ljava/lang/String;

.field mVideoIdIdx:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "audio_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "AlbumArtistId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "artistSort"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "hasRemote"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Genre"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "StoreId"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SongId"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Domain"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ArtistArtLocation"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "track"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "itemState"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "containerId"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "containerType"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "containerName"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "Vid"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "VThumbnailUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/TrackListAdapter;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/music/ui/MusicFragment;ILcom/google/android/music/medialist/MediaList;)V
    .locals 3
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I
    .param p3, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 202
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 59
    iput-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    .line 122
    iput-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    .line 126
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentPlayPosition:I

    .line 134
    iput-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowTrackArtist:Z

    .line 137
    iput-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mEditMode:Z

    .line 140
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    .line 141
    iput v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mRemovedItemsCount:I

    .line 142
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackSongId:J

    .line 143
    iput-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackMetajamId:Ljava/lang/String;

    .line 161
    new-instance v0, Lcom/google/android/music/ui/TrackListAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/TrackListAdapter$1;-><init>(Lcom/google/android/music/ui/TrackListAdapter;)V

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mContextClickListener:Landroid/view/View$OnClickListener;

    .line 203
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/TrackListAdapter;->init(Lcom/google/android/music/medialist/MediaList;)V

    .line 204
    return-void
.end method

.method constructor <init>(Lcom/google/android/music/ui/MusicFragment;ILcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I
    .param p3, "medialist"    # Lcom/google/android/music/medialist/MediaList;
    .param p4, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 196
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/music/ui/MediaListCursorAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 59
    iput-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    .line 122
    iput-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    .line 126
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentPlayPosition:I

    .line 134
    iput-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowTrackArtist:Z

    .line 137
    iput-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mEditMode:Z

    .line 140
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    .line 141
    iput v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mRemovedItemsCount:I

    .line 142
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackSongId:J

    .line 143
    iput-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackMetajamId:Ljava/lang/String;

    .line 161
    new-instance v0, Lcom/google/android/music/ui/TrackListAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/TrackListAdapter$1;-><init>(Lcom/google/android/music/ui/TrackListAdapter;)V

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mContextClickListener:Landroid/view/View$OnClickListener;

    .line 197
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/TrackListAdapter;->init(Lcom/google/android/music/medialist/MediaList;)V

    .line 198
    invoke-direct {p0, p4}, Lcom/google/android/music/ui/TrackListAdapter;->getColumnIndices(Landroid/database/Cursor;)V

    .line 199
    return-void
.end method

.method constructor <init>(Lcom/google/android/music/ui/MusicFragment;ZLcom/google/android/music/medialist/MediaList;)V
    .locals 1
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "leftPadding"    # Z
    .param p3, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 190
    if-eqz p2, :cond_0

    const v0, 0x7f0400f6

    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/music/ui/TrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILcom/google/android/music/medialist/MediaList;)V

    .line 192
    return-void

    .line 190
    :cond_0
    const v0, 0x7f0400f8

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/music/ui/MusicFragment;ZLcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "leftPadding"    # Z
    .param p3, "medialist"    # Lcom/google/android/music/medialist/MediaList;
    .param p4, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 179
    if-eqz p2, :cond_0

    const v0, 0x7f0400f6

    :goto_0
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/music/ui/TrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    .line 182
    return-void

    .line 179
    :cond_0
    const v0, 0x7f0400f8

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/TrackListAdapter;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackListAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/TrackListAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackListAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsInInfiniteMix:Z

    return p1
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/TrackListAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackListAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsContainerCurrentlyPlaying:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/TrackListAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackListAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentPlayPosition:I

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/music/ui/TrackListAdapter;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackListAdapter;
    .param p1, "x1"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/music/ui/TrackListAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackListAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentTrackMetajamId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/TrackListAdapter;)Landroid/util/SparseIntArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/TrackListAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method private getAlbum(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 608
    iget v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumIdx:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 609
    .local v0, "album":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 610
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mUnknownAlbum:Ljava/lang/String;

    .line 612
    :cond_0
    return-object v0
.end method

.method private getAlbumMetajamId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 629
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumMetajamIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getArtistMetajamId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 634
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtistMetajamIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtistMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtistMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getColumnIndices(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 243
    if-eqz p1, :cond_0

    .line 245
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTitleIdx:I

    .line 246
    const-string v0, "artist"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtistIdx:I

    .line 247
    const-string v0, "AlbumArtistId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumArtistIdIdx:I

    .line 249
    const-string v0, "artist_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackArtistIdIdx:I

    .line 251
    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mDurationIdx:I

    .line 253
    const-string v0, "audio_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAudioIdIdx:I

    .line 255
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v0, :cond_1

    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mPlaylistMemberIdIdx:I

    .line 258
    const-string v0, "album"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumIdx:I

    .line 259
    const-string v0, "album_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumIdIdx:I

    .line 260
    const-string v0, "StoreAlbumId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumStoreIdIdx:I

    .line 263
    const-string v0, "hasLocal"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsLocalIdx:I

    .line 264
    const-string v0, "hasRemote"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHasRemoteIdx:I

    .line 266
    const-string v0, "Domain"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mDomainIdx:I

    .line 267
    const-string v0, "SongId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mSongIdIdx:I

    .line 268
    const-string v0, "StoreId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mStoreSongIdx:I

    .line 269
    const-string v0, "Nid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    .line 270
    const-string v0, "StoreAlbumId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumMetajamIdx:I

    .line 271
    const-string v0, "ArtistMetajamId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtistMetajamIdx:I

    .line 272
    const-string v0, "artworkUrl"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtworkUrlIdx:I

    .line 273
    const-string v0, "track"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackNumberIdx:I

    .line 274
    const-string v0, "itemState"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueItemStateIdx:I

    .line 275
    const-string v0, "containerId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueItemContainerIdIdx:I

    .line 277
    const-string v0, "containerType"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueContainerTypeIdx:I

    .line 278
    const-string v0, "containerName"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueContainerNameIdx:I

    .line 279
    const-string v0, "Vid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mVideoIdIdx:I

    .line 281
    :cond_0
    return-void

    .line 255
    :cond_1
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method private getPlaylistMemberId(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 639
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mPlaylistMemberIdIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mPlaylistMemberIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private getTrackArtist(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 616
    iget v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtistIdx:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 617
    .local v0, "artist":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 618
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mUnknownArtist:Ljava/lang/String;

    .line 620
    :cond_0
    return-object v0
.end method

.method private getTrackMetajamId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 624
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    if-ltz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Lcom/google/android/music/medialist/MediaList;)V
    .locals 2
    .param p1, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 208
    instance-of v1, p1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/google/android/music/medialist/ArtistSongList;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/google/android/music/medialist/GenreSongList;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowAlbumArt:Z

    .line 214
    instance-of v1, p1, Lcom/google/android/music/medialist/SongList;

    if-eqz v1, :cond_2

    check-cast p1, Lcom/google/android/music/medialist/SongList;

    .end local p1    # "medialist":Lcom/google/android/music/medialist/MediaList;
    :goto_1
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 215
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 216
    .local v0, "context":Landroid/content/Context;
    const v1, 0x7f0b00c4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mUnknownArtist:Ljava/lang/String;

    .line 217
    const v1, 0x7f0b00c5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mUnknownAlbum:Ljava/lang/String;

    .line 218
    return-void

    .line 208
    .end local v0    # "context":Landroid/content/Context;
    .restart local p1    # "medialist":Lcom/google/android/music/medialist/MediaList;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 214
    :cond_2
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private isInInfiniteMixMode()Z
    .locals 1

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsInInfiniteMix:Z

    return v0
.end method

.method private populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x0

    .line 284
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 285
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 286
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 287
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAudioIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 291
    :cond_0
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTitleIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 292
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/TrackListAdapter;->getPlaylistMemberId(Landroid/database/Cursor;)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setIdInParent(J)V

    .line 294
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 295
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumStoreIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 296
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAlbumStoreIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 298
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/TrackListAdapter;->getAlbum(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 300
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/TrackListAdapter;->getTrackArtist(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 301
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackArtistIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 303
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/TrackListAdapter;->getTrackMetajamId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTrackMetajamId(Ljava/lang/String;)V

    .line 304
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/TrackListAdapter;->getAlbumMetajamId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 305
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/TrackListAdapter;->getArtistMetajamId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 306
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mStoreSongIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 307
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mStoreSongIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSongStoreId(Ljava/lang/String;)V

    .line 309
    :cond_2
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtworkUrlIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 310
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mArtworkUrlIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 314
    :cond_3
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mDomainIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_9

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsNautilus(Z)V

    .line 316
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueItemStateIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 318
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setQueueItemId(J)V

    .line 319
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueItemStateIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setQueueItemState(I)V

    .line 321
    :cond_4
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueItemContainerIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 322
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueItemContainerIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setQueueItemContainerId(J)V

    .line 324
    :cond_5
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueContainerTypeIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 325
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueContainerTypeIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setQueueItemContainerType(I)V

    .line 327
    :cond_6
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueContainerNameIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 328
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mQueueContainerNameIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setQueueItemContainerName(Ljava/lang/String;)V

    .line 330
    :cond_7
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mVideoIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 331
    iget v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mVideoIdIdx:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setVideoId(Ljava/lang/String;)V

    .line 333
    :cond_8
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPosition(I)V

    .line 334
    return-void

    :cond_9
    move v0, v1

    .line 314
    goto :goto_0
.end method

.method private updatePlayIndicator(Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;Landroid/database/Cursor;)V
    .locals 9
    .param p1, "vh"    # Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 531
    iget-object v4, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->play_indicator:Lcom/google/android/music/PlayingIndicator;

    if-eqz v4, :cond_3

    .line 536
    const/4 v0, 0x0

    .line 538
    .local v0, "showPlayIndicator":Z
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->canShowPlaybackIndicator()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 539
    iget-boolean v4, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsContainerCurrentlyPlaying:Z

    if-eqz v4, :cond_1

    .line 543
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    iget v5, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentPlayPosition:I

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/google/android/music/ui/TrackListAdapter;->mAudioIdIdx:I

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v6}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentTrackMetajamId:Ljava/lang/String;

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    invoke-interface {p2, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCurrentTrackMetajamId:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_0
    move v0, v2

    .line 553
    :cond_1
    :goto_0
    if-eqz v0, :cond_5

    .line 554
    iget-object v4, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->play_indicator:Lcom/google/android/music/PlayingIndicator;

    invoke-virtual {v4, v3}, Lcom/google/android/music/PlayingIndicator;->setVisibility(I)V

    .line 555
    iget-object v4, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->title:Lcom/google/android/music/StatefulShadowTextView;

    invoke-virtual {v4, v8, v2}, Lcom/google/android/music/StatefulShadowTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 563
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    if-eqz v2, :cond_3

    .line 564
    if-eqz v0, :cond_6

    .line 565
    .local v1, "visibility":I
    :goto_2
    iget-object v2, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->trackNumber:Lcom/google/android/music/StatefulShadowTextView;

    if-eqz v2, :cond_2

    .line 566
    iget-object v2, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->trackNumber:Lcom/google/android/music/StatefulShadowTextView;

    invoke-virtual {v2, v1}, Lcom/google/android/music/StatefulShadowTextView;->setVisibility(I)V

    .line 570
    :cond_2
    iget-object v2, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    if-eqz v2, :cond_3

    .line 571
    iget-object v2, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v2, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setVisibility(I)V

    .line 575
    .end local v0    # "showPlayIndicator":Z
    .end local v1    # "visibility":I
    :cond_3
    return-void

    .restart local v0    # "showPlayIndicator":Z
    :cond_4
    move v0, v3

    .line 543
    goto :goto_0

    .line 557
    :cond_5
    iget-object v2, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->play_indicator:Lcom/google/android/music/PlayingIndicator;

    invoke-virtual {v2, v1}, Lcom/google/android/music/PlayingIndicator;->setVisibility(I)V

    .line 558
    iget-object v2, p1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->title:Lcom/google/android/music/StatefulShadowTextView;

    invoke-virtual {v2, v8, v3}, Lcom/google/android/music/StatefulShadowTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_1

    :cond_6
    move v1, v3

    .line 564
    goto :goto_2
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 747
    return-void
.end method

.method public bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 29
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    .line 368
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;

    .line 370
    .local v22, "vh":Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/TrackListAdapter;->populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V

    .line 372
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mTitleIdx:I

    move/from16 v24, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->titleBuffer:Landroid/database/CharArrayBuffer;

    move-object/from16 v25, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 373
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->title:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->titleBuffer:Landroid/database/CharArrayBuffer;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/database/CharArrayBuffer;->data:[C

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->titleBuffer:Landroid/database/CharArrayBuffer;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/database/CharArrayBuffer;->sizeCopied:I

    move/from16 v27, v0

    invoke-virtual/range {v24 .. v27}, Lcom/google/android/music/StatefulShadowTextView;->setText([CII)V

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    instance-of v14, v0, Lcom/google/android/music/medialist/SharedSongList;

    .line 377
    .local v14, "isSharedMode":Z
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->contextMenu:Landroid/view/View;

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    .line 378
    if-eqz v14, :cond_10

    .line 379
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->contextMenu:Landroid/view/View;

    move-object/from16 v24, v0

    const/16 v25, 0x4

    invoke-virtual/range {v24 .. v25}, Landroid/view/View;->setVisibility(I)V

    .line 385
    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v10

    .line 386
    .local v10, "isDownloadedOnlyMode":Z
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/ui/UIStateManager;->isStreamingEnabled()Z

    move-result v15

    .line 387
    .local v15, "isStreamingEnabled":Z
    if-eqz v15, :cond_1

    if-eqz v10, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mIsLocalIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    if-eqz v24, :cond_11

    :cond_2
    const/4 v9, 0x1

    .line 389
    .local v9, "isAvailable":Z
    :goto_1
    move-object/from16 v0, v22

    iput-boolean v9, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->isAvailable:Z

    .line 390
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mHasRemoteIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    if-eqz v24, :cond_12

    const/16 v24, 0x1

    :goto_2
    move/from16 v0, v24

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->hasRemote:Z

    .line 392
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->title:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Lcom/google/android/music/StatefulShadowTextView;->setPrimaryAndOnline(ZZ)V

    .line 393
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->duration:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    if-eqz v24, :cond_4

    .line 395
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    move/from16 v24, v0

    if-eqz v24, :cond_3

    .line 396
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->duration:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/StatefulShadowTextView;->setVisibility(I)V

    .line 399
    :cond_3
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->duration:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    move/from16 v24, v0

    if-nez v24, :cond_13

    const/16 v24, 0x1

    :goto_3
    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-virtual {v0, v1, v9}, Lcom/google/android/music/StatefulShadowTextView;->setPrimaryAndOnline(ZZ)V

    .line 402
    :cond_4
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->album:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    if-eqz v24, :cond_5

    .line 403
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->album:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Lcom/google/android/music/StatefulShadowTextView;->setPrimaryAndOnline(ZZ)V

    .line 404
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->album:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/StatefulShadowTextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    :cond_5
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    if-eqz v24, :cond_6

    .line 408
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mShowTrackArtist:Z

    move/from16 v24, v0

    if-eqz v24, :cond_14

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_14

    .line 409
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Lcom/google/android/music/StatefulShadowTextView;->setPrimaryAndOnline(ZZ)V

    .line 410
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/StatefulShadowTextView;->setText(Ljava/lang/CharSequence;)V

    .line 411
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/StatefulShadowTextView;->setVisibility(I)V

    .line 417
    :cond_6
    :goto_4
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->duration:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    if-eqz v24, :cond_7

    .line 418
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mDurationIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    move/from16 v0, v24

    div-int/lit16 v7, v0, 0x3e8

    .line 419
    .local v7, "duration":I
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->duration:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/TrackListAdapter;->getContext()Landroid/content/Context;

    move-result-object v25

    int-to-long v0, v7

    move-wide/from16 v26, v0

    invoke-static/range {v25 .. v27}, Lcom/google/android/music/utils/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/StatefulShadowTextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    .end local v7    # "duration":I
    :cond_7
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v24, v0

    if-eqz v24, :cond_9

    .line 424
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mShowAlbumArt:Z

    move/from16 v24, v0

    if-eqz v24, :cond_8

    if-eqz v14, :cond_15

    .line 425
    :cond_8
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v24, v0

    const/16 v25, 0x8

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/AsyncAlbumArtImageView;->setVisibility(I)V

    .line 439
    :cond_9
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    move/from16 v24, v0

    if-eqz v24, :cond_b

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->trackNumber:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    if-eqz v24, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/medialist/SongList;->shouldShowTrackNumbers()Z

    move-result v24

    if-eqz v24, :cond_b

    .line 442
    const-string v18, "-"

    .line 444
    .local v18, "number":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackNumberIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v24

    if-nez v24, :cond_a

    .line 445
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackNumberIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 446
    .local v17, "n":I
    if-eqz v17, :cond_a

    .line 447
    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    .line 451
    .end local v17    # "n":I
    :cond_a
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->trackNumber:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/StatefulShadowTextView;->setVisibility(I)V

    .line 452
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->trackNumber:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Lcom/google/android/music/StatefulShadowTextView;->setPrimaryAndOnline(ZZ)V

    .line 453
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->trackNumber:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/music/StatefulShadowTextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    .end local v18    # "number":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mIsContainerCurrentlyPlaying:Z

    move/from16 v24, v0

    if-eqz v24, :cond_c

    .line 465
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/ui/TrackListAdapter;->isInInfiniteMixMode()Z

    move-result v24

    if-eqz v24, :cond_17

    .line 466
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v20

    .line 467
    .local v20, "size":I
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v23

    .line 468
    .local v23, "viewPos":I
    const/16 v24, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/ViewUtils;->fadeViewForPosition(Landroid/view/View;III)V

    .line 475
    .end local v20    # "size":I
    .end local v23    # "viewPos":I
    :cond_c
    :goto_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackSongId:J

    move-wide/from16 v24, v0

    const-wide/16 v26, -0x1

    cmp-long v24, v24, v26

    if-nez v24, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackMetajamId:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_18

    :cond_d
    const/4 v11, 0x1

    .line 477
    .local v11, "isHighlightingTrack":Z
    :goto_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackSongId:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x1

    cmp-long v24, v24, v26

    if-eqz v24, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mSongIdIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v24

    if-nez v24, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mSongIdIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackSongId:J

    move-wide/from16 v26, v0

    cmp-long v24, v24, v26

    if-nez v24, :cond_19

    const/4 v13, 0x1

    .line 480
    .local v13, "isMatchingSongId":Z
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackMetajamId:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_1a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v24

    if-nez v24, :cond_1a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mTrackMetajamIdx:I

    move/from16 v24, v0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackMetajamId:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1a

    const/4 v12, 0x1

    .line 484
    .local v12, "isMatchingMetajamId":Z
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1d

    .line 486
    const v24, 0x7f0e019f

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 487
    .local v8, "grabber":Landroid/view/View;
    if-eqz v8, :cond_e

    .line 488
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mEditMode:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1b

    .line 489
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 495
    :cond_e
    :goto_a
    if-eqz v11, :cond_1c

    if-nez v13, :cond_f

    if-eqz v12, :cond_1c

    .line 496
    :cond_f
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f0c00ca

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 524
    .end local v8    # "grabber":Landroid/view/View;
    :goto_b
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/TrackListAdapter;->updatePlayIndicator(Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;Landroid/database/Cursor;)V

    .line 525
    return-void

    .line 381
    .end local v9    # "isAvailable":Z
    .end local v10    # "isDownloadedOnlyMode":Z
    .end local v11    # "isHighlightingTrack":Z
    .end local v12    # "isMatchingMetajamId":Z
    .end local v13    # "isMatchingSongId":Z
    .end local v15    # "isStreamingEnabled":Z
    :cond_10
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->contextMenu:Landroid/view/View;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 387
    .restart local v10    # "isDownloadedOnlyMode":Z
    .restart local v15    # "isStreamingEnabled":Z
    :cond_11
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 390
    .restart local v9    # "isAvailable":Z
    :cond_12
    const/16 v24, 0x0

    goto/16 :goto_2

    .line 399
    :cond_13
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 413
    :cond_14
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    move-object/from16 v24, v0

    const/16 v25, 0x8

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/StatefulShadowTextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 427
    :cond_15
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/AsyncAlbumArtImageView;->setVisibility(I)V

    .line 428
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAvailable(Z)V

    .line 429
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_16

    .line 431
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 433
    :cond_16
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v26

    const/16 v25, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    move-object/from16 v3, v25

    move-object/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 471
    :cond_17
    const/high16 v24, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    goto/16 :goto_6

    .line 475
    :cond_18
    const/4 v11, 0x0

    goto/16 :goto_7

    .line 477
    .restart local v11    # "isHighlightingTrack":Z
    :cond_19
    const/4 v13, 0x0

    goto/16 :goto_8

    .line 480
    .restart local v13    # "isMatchingSongId":Z
    :cond_1a
    const/4 v12, 0x0

    goto/16 :goto_9

    .line 491
    .restart local v8    # "grabber":Landroid/view/View;
    .restart local v12    # "isMatchingMetajamId":Z
    :cond_1b
    const/16 v24, 0x8

    move/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_a

    .line 500
    :cond_1c
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f0c00c9

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_b

    .line 504
    .end local v8    # "grabber":Landroid/view/View;
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mEditMode:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1e

    .line 507
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v16

    .line 508
    .local v16, "left":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getPaddingTop()I

    move-result v21

    .line 509
    .local v21, "top":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getPaddingRight()I

    move-result v19

    .line 510
    .local v19, "right":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 511
    .local v6, "bottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/TrackListAdapter;->mDragHandleBG:Landroid/graphics/drawable/Drawable;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 512
    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v21

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_b

    .line 513
    .end local v6    # "bottom":I
    .end local v16    # "left":I
    .end local v19    # "right":I
    .end local v21    # "top":I
    :cond_1e
    if-eqz v11, :cond_20

    if-nez v13, :cond_1f

    if-eqz v12, :cond_20

    .line 514
    :cond_1f
    const v24, 0x7f0c0080

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_b

    .line 518
    :cond_20
    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_b
.end method

.method canShowPlaybackIndicator()Z
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 721
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 722
    return-void
.end method

.method public clearHighlight()V
    .locals 2

    .prologue
    .line 883
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackSongId:J

    .line 884
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackMetajamId:Ljava/lang/String;

    .line 885
    return-void
.end method

.method disablePlaybackIndicator()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    .line 234
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->notifyDataSetChanged()V

    .line 236
    :cond_1
    return-void
.end method

.method enablePlaybackIndicator()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 226
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCanShowPlayIndicator:Ljava/lang/Boolean;

    .line 227
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->notifyDataSetChanged()V

    .line 229
    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 579
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getCount()I

    move-result v0

    .line 580
    .local v0, "count":I
    iget v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mRemovedItemsCount:I

    sub-int v1, v0, v1

    return v1
.end method

.method public getDocument(I)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 8
    .param p1, "position"    # I

    .prologue
    .line 844
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 845
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v1}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 846
    .local v1, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getCount()I

    move-result v3

    if-le p1, v3, :cond_2

    .line 847
    :cond_0
    const-string v3, "TrackListAdapter"

    const-string v4, "Position out of range. pos=%d, count=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    :cond_1
    :goto_0
    return-object v1

    .line 852
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 853
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->hasCount(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 854
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 855
    .local v2, "origPos":I
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 856
    invoke-direct {p0, v1, v0}, Lcom/google/android/music/ui/TrackListAdapter;->populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V

    .line 859
    :cond_3
    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 860
    const-string v3, "TrackListAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to restore the cursor to the original position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    invoke-super {p0, v0, p2, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method protected getItemTempPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    invoke-super {p0, v0, p2, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public moveItemTemp(II)V
    .locals 6
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 790
    if-eq p1, p2, :cond_2

    .line 791
    iget-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 793
    .local v0, "cursorFrom":I
    if-le p1, p2, :cond_0

    .line 794
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-le v1, p2, :cond_1

    .line 795
    iget-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    iget-object v3, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    add-int/lit8 v4, v1, -0x1

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 794
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 798
    .end local v1    # "i":I
    :cond_0
    move v1, p1

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, p2, :cond_1

    .line 799
    iget-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    iget-object v3, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    add-int/lit8 v4, v1, 0x1

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 798
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 802
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 804
    .end local v0    # "cursorFrom":I
    .end local v1    # "i":I
    :cond_2
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 338
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 340
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;-><init>(Lcom/google/android/music/ui/TrackListAdapter;)V

    .line 341
    .local v1, "vh":Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    new-instance v2, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v2}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 342
    const v2, 0x7f0e00b8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/StatefulShadowTextView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->title:Lcom/google/android/music/StatefulShadowTextView;

    .line 343
    iget-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->title:Lcom/google/android/music/StatefulShadowTextView;

    if-nez v2, :cond_0

    .line 344
    const v2, 0x7f0e00d3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/StatefulShadowTextView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->title:Lcom/google/android/music/StatefulShadowTextView;

    .line 346
    :cond_0
    const v2, 0x7f0e0173

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->comboColumn:Landroid/view/View;

    .line 347
    const v2, 0x7f0e0276

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->contextMenu:Landroid/view/View;

    .line 348
    iget-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->contextMenu:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/music/ui/TrackListAdapter;->mContextClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    const v2, 0x7f0e007c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->icon:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 351
    const v2, 0x7f0e0174

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/PlayingIndicator;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->play_indicator:Lcom/google/android/music/PlayingIndicator;

    .line 352
    new-instance v2, Landroid/database/CharArrayBuffer;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->titleBuffer:Landroid/database/CharArrayBuffer;

    .line 353
    const v2, 0x7f0e016c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/StatefulShadowTextView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->trackNumber:Lcom/google/android/music/StatefulShadowTextView;

    .line 354
    const v2, 0x7f0e019e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/StatefulShadowTextView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->duration:Lcom/google/android/music/StatefulShadowTextView;

    .line 355
    const v2, 0x7f0e01c4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/StatefulShadowTextView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->album:Lcom/google/android/music/StatefulShadowTextView;

    .line 356
    const v2, 0x7f0e01c3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/StatefulShadowTextView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    .line 357
    iget-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    if-nez v2, :cond_1

    .line 358
    const v2, 0x7f0e00d4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/StatefulShadowTextView;

    iput-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->artist:Lcom/google/android/music/StatefulShadowTextView;

    .line 360
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 361
    iget-object v2, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->contextMenu:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 362
    return-object v0
.end method

.method protected onContentChanged()V
    .locals 1

    .prologue
    .line 734
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->resetTempState()V

    .line 735
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->updatePlaybackState()V

    .line 736
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/TrackListAdapter;->getColumnIndices(Landroid/database/Cursor;)V

    .line 737
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListCursorAdapter;->onContentChanged()V

    .line 739
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCallback:Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;

    if-eqz v0, :cond_0

    .line 740
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCallback:Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;

    invoke-interface {v0}, Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;->onContentChanged()V

    .line 742
    :cond_0
    return-void
.end method

.method public removeItemTemp(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 814
    iget v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mRemovedItemsCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mRemovedItemsCount:I

    .line 815
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getCount()I

    move-result v0

    .line 816
    .local v0, "count":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 817
    iget-object v2, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    iget-object v3, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    add-int/lit8 v4, v1, 0x1

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 816
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 819
    :cond_0
    return-void
.end method

.method protected resetTempState()V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 645
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mRemovedItemsCount:I

    .line 646
    return-void
.end method

.method public setEditMode(Z)V
    .locals 2
    .param p1, "editmode"    # Z

    .prologue
    .line 831
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mEditMode:Z

    .line 832
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mDragHandleBG:Landroid/graphics/drawable/Drawable;

    .line 833
    return-void
.end method

.method public setHighlightTrack(J)V
    .locals 1
    .param p1, "highlightTrackSongId"    # J

    .prologue
    .line 871
    iput-wide p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackSongId:J

    .line 872
    return-void
.end method

.method public setHighlightTrack(Ljava/lang/String;)V
    .locals 0
    .param p1, "highlightTrackMetajamId"    # Ljava/lang/String;

    .prologue
    .line 879
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mHighlightTrackMetajamId:Ljava/lang/String;

    .line 880
    return-void
.end method

.method setMaterialEnabled(Z)V
    .locals 0
    .param p1, "isMaterialEnabled"    # Z

    .prologue
    .line 221
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsMaterialEnabled:Z

    .line 222
    return-void
.end method

.method public setOnContentChangedCallback(Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;

    .prologue
    .line 836
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCallback:Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;

    .line 837
    return-void
.end method

.method public setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 1
    .param p1, "songlist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 750
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 751
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mIsContainerCurrentlyPlaying:Z

    .line 752
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->notifyDataSetChanged()V

    .line 753
    return-void
.end method

.method public showAlbumArt(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowAlbumArt:Z

    if-eq p1, v0, :cond_0

    .line 774
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowAlbumArt:Z

    .line 775
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->notifyDataSetChanged()V

    .line 779
    :goto_0
    return-void

    .line 777
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowAlbumArt:Z

    goto :goto_0
.end method

.method public showTrackArtist(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 760
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowTrackArtist:Z

    if-eq v0, p1, :cond_0

    .line 761
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowTrackArtist:Z

    .line 762
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->notifyDataSetChanged()V

    .line 766
    :goto_0
    return-void

    .line 764
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mShowTrackArtist:Z

    goto :goto_0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 726
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->resetTempState()V

    .line 727
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->updatePlaybackState()V

    .line 728
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/TrackListAdapter;->getColumnIndices(Landroid/database/Cursor;)V

    .line 729
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method updatePlaybackState()V
    .locals 4

    .prologue
    .line 653
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->isActivityValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 717
    :cond_0
    :goto_0
    return-void

    .line 656
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/TrackListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 657
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 658
    .local v1, "currentSongList":Lcom/google/android/music/medialist/SongList;
    if-eqz v1, :cond_0

    .line 661
    sget-object v2, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Lcom/google/android/music/ui/TrackListAdapter$2;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/music/ui/TrackListAdapter$2;-><init>(Lcom/google/android/music/ui/TrackListAdapter;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;)V

    invoke-static {v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsyncWithCallback(Lcom/google/android/music/utils/LoggableHandler;Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0
.end method
