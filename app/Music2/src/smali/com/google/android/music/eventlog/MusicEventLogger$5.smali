.class Lcom/google/android/music/eventlog/MusicEventLogger$5;
.super Ljava/lang/Object;
.source "MusicEventLogger.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/eventlog/MusicEventLogger;->logSeekEventAsync(ZJJJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

.field final synthetic val$curPosition:J

.field final synthetic val$isRemote:Z

.field final synthetic val$musicId:J

.field final synthetic val$seekPosition:J


# direct methods
.method constructor <init>(Lcom/google/android/music/eventlog/MusicEventLogger;ZJJJ)V
    .locals 1

    .prologue
    .line 572
    iput-object p1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iput-boolean p2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$isRemote:Z

    iput-wide p3, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$musicId:J

    iput-wide p5, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$curPosition:J

    iput-wide p7, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$seekPosition:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-boolean v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$isRemote:Z

    iget-wide v2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$musicId:J

    iget-wide v4, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$curPosition:J

    iget-wide v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger$5;->val$seekPosition:J

    # invokes: Lcom/google/android/music/eventlog/MusicEventLogger;->logSeekEvent(ZJJJ)V
    invoke-static/range {v0 .. v7}, Lcom/google/android/music/eventlog/MusicEventLogger;->access$300(Lcom/google/android/music/eventlog/MusicEventLogger;ZJJJ)V

    .line 576
    return-void
.end method
