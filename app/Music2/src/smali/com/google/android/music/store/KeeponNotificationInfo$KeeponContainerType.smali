.class public final enum Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;
.super Ljava/lang/Enum;
.source "KeeponNotificationInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/KeeponNotificationInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KeeponContainerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

.field public static final enum ALBUM:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

.field public static final enum AUTOLIST:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

.field public static final enum PLAYLIST:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

.field public static final enum RADIO:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->ALBUM:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    new-instance v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    const-string v1, "AUTOLIST"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->AUTOLIST:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    new-instance v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    const-string v1, "PLAYLIST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->PLAYLIST:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    new-instance v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    const-string v1, "RADIO"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->RADIO:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    .line 50
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    sget-object v1, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->ALBUM:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->AUTOLIST:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->PLAYLIST:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->RADIO:Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->$VALUES:[Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->$VALUES:[Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    invoke-virtual {v0}, [Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/store/KeeponNotificationInfo$KeeponContainerType;

    return-object v0
.end method
