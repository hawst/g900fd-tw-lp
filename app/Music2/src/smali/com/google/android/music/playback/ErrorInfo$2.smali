.class Lcom/google/android/music/playback/ErrorInfo$2;
.super Ljava/lang/Object;
.source "ErrorInfo.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/ErrorInfo;->createAlert(Landroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/ErrorInfo;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

.field final synthetic val$neutralActionUriId:I


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/ErrorInfo;ILandroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/google/android/music/playback/ErrorInfo$2;->this$0:Lcom/google/android/music/playback/ErrorInfo;

    iput p2, p0, Lcom/google/android/music/playback/ErrorInfo$2;->val$neutralActionUriId:I

    iput-object p3, p0, Lcom/google/android/music/playback/ErrorInfo$2;->val$activity:Landroid/app/Activity;

    iput-object p4, p0, Lcom/google/android/music/playback/ErrorInfo$2;->val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 491
    iget v0, p0, Lcom/google/android/music/playback/ErrorInfo$2;->val$neutralActionUriId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 492
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo$2;->this$0:Lcom/google/android/music/playback/ErrorInfo;

    iget-object v1, p0, Lcom/google/android/music/playback/ErrorInfo$2;->val$activity:Landroid/app/Activity;

    # invokes: Lcom/google/android/music/playback/ErrorInfo;->startManageDevicesActivity(Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/google/android/music/playback/ErrorInfo;->access$000(Lcom/google/android/music/playback/ErrorInfo;Landroid/app/Activity;)V

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo$2;->val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/google/android/music/playback/ErrorInfo$2;->val$dismissedListener:Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;

    invoke-interface {v0}, Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;->onErrorAlertDismissed()V

    .line 497
    :cond_1
    return-void
.end method
