.class Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArtistMode"
.end annotation


# instance fields
.field public id:J

.field public parentName:Ljava/lang/String;

.field public uri:Landroid/net/Uri;

.field public useFauxArt:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 711
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;)V
    .locals 2
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    .prologue
    .line 713
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 714
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->parentName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->parentName:Ljava/lang/String;

    .line 715
    iget-wide v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->id:J

    iput-wide v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->id:J

    .line 716
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    .line 717
    iget-boolean v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->useFauxArt:Z

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->useFauxArt:Z

    .line 718
    return-void
.end method

.method private makeFauxArt(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "albumsUri"    # Landroid/net/Uri;

    .prologue
    .line 722
    const/4 v12, 0x0

    .line 723
    .local v12, "childAlbums":Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
    if-eqz p5, :cond_0

    .line 724
    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-static {v0, v1}, Lcom/google/android/music/utils/AlbumArtUtils;->createAlbumIdIteratorFactoryForContentUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;

    move-result-object v12

    .line 727
    :cond_0
    const/4 v4, 0x7

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->id:J

    iget-object v10, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->parentName:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v14, 0x0

    move-object/from16 v3, p1

    move/from16 v8, p2

    move/from16 v9, p3

    move-object/from16 v13, p4

    invoke-static/range {v3 .. v14}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 771
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    const/4 v4, 0x0

    .line 735
    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->useFauxArt:Z

    if-eqz v0, :cond_1

    .line 736
    iget-object v5, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->makeFauxArt(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 749
    :cond_0
    :goto_0
    return-object v6

    .line 738
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 739
    .local v7, "remoteUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->cropToSquare()Z

    move-result v0

    invoke-static {p1, v7, p2, p3, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 741
    .local v6, "artistArt":Landroid/graphics/Bitmap;
    if-nez v6, :cond_0

    .line 744
    invoke-interface {p5, v7}, Lcom/google/android/music/download/artwork/RemoteUrlSink;->report(Ljava/lang/String;)V

    .line 745
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v6, v4

    .line 747
    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, v4

    .line 749
    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->makeFauxArt(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 762
    instance-of v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    if-nez v0, :cond_0

    .line 763
    const/4 v0, 0x0

    .line 766
    :goto_0
    return v0

    :cond_0
    move-object v6, p1

    .line 765
    check-cast v6, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    .line 766
    .local v6, "o":Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
    iget-object v1, v6, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->parentName:Ljava/lang/String;

    iget-wide v2, v6, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->id:J

    iget-object v4, v6, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    iget-boolean v5, v6, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->useFauxArt:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->equals(Ljava/lang/String;JLandroid/net/Uri;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/String;JLandroid/net/Uri;Z)Z
    .locals 2
    .param p1, "parentName"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "uri"    # Landroid/net/Uri;
    .param p5, "useFauxArt"    # Z

    .prologue
    .line 775
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->parentName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->id:J

    cmp-long v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    invoke-static {v0, p4}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->isEqual(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->useFauxArt:Z

    if-ne v0, p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 757
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ArtistMode<parentName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->parentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
