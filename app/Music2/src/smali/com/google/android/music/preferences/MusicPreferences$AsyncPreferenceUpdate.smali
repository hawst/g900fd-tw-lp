.class Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;
.super Ljava/lang/Object;
.source "MusicPreferences.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/preferences/MusicPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncPreferenceUpdate"
.end annotation


# instance fields
.field private final mExtraTask:Ljava/lang/Runnable;

.field private final mKey:Ljava/lang/String;

.field private final mValue:Ljava/lang/Object;

.field final synthetic this$0:Lcom/google/android/music/preferences/MusicPreferences;


# direct methods
.method public constructor <init>(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 1660
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;-><init>(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Runnable;)V

    .line 1661
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;
    .param p4, "extraTask"    # Ljava/lang/Runnable;

    .prologue
    .line 1663
    iput-object p1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1664
    iput-object p2, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mKey:Ljava/lang/String;

    .line 1665
    iput-object p3, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    .line 1666
    iput-object p4, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mExtraTask:Ljava/lang/Runnable;

    .line 1667
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1670
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # invokes: Lcom/google/android/music/preferences/MusicPreferences;->hasPreferenceService()Z
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$400(Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1673
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->runWithPreferenceService(Ljava/lang/Runnable;)V

    .line 1696
    :cond_0
    :goto_0
    return-void

    .line 1677
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    if-nez v1, :cond_2

    .line 1678
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$500(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/preferences/IPreferenceService;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/music/preferences/IPreferenceService;->remove(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1693
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mExtraTask:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 1694
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mExtraTask:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 1679
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1680
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$500(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/preferences/IPreferenceService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v3, v1}, Lcom/google/android/music/preferences/IPreferenceService;->setIntPreference(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1690
    :catch_0
    move-exception v0

    .line 1691
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MusicPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error communicating to preference service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1681
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 1682
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$500(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/preferences/IPreferenceService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/music/preferences/IPreferenceService;->setLongPreference(Ljava/lang/String;J)V

    goto :goto_1

    .line 1683
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1684
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$500(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/preferences/IPreferenceService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/music/preferences/IPreferenceService;->setStringPreference(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1685
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1686
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$500(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/preferences/IPreferenceService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v2, v3, v1}, Lcom/google/android/music/preferences/IPreferenceService;->setBooleanPreference(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1688
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown value type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;->mValue:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
.end method
