.class Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackAlbumDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createAlbumDetails(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

.field final synthetic val$albumId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->val$albumId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 36
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 129
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->val$albumId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v31

    .line 131
    .local v31, "isNautilusAlbum":Z
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 132
    .local v32, "name":Ljava/lang/String;
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 133
    .local v24, "artist":Ljava/lang/String;
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 134
    .local v33, "songCount":I
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 135
    .local v22, "albumYear":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-virtual {v4}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f120001

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    move/from16 v0, v33

    invoke-virtual {v4, v5, v0, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    .line 138
    .local v34, "songCountString":Ljava/lang/String;
    const/16 v30, 0x0

    .line 139
    .local v30, "decodedExternalAlbumArtUriBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    const/16 v17, 0x0

    .line 140
    .local v17, "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    if-eqz v31, :cond_5

    .line 141
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 142
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 143
    .local v23, "artUriString":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 144
    new-instance v30, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;

    .end local v30    # "decodedExternalAlbumArtUriBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;-><init>(Ljava/lang/String;)V

    .line 158
    .end local v23    # "artUriString":Ljava/lang/String;
    .restart local v30    # "decodedExternalAlbumArtUriBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    :cond_0
    :goto_0
    new-instance v29, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    const/4 v5, 0x0

    aput-object v17, v4, v5

    const/4 v5, 0x1

    aput-object v30, v4, v5

    move-object/from16 v0, v29

    invoke-direct {v0, v4}, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;-><init>([Lcom/google/android/music/leanback/bitmap/BitmapGetter;)V

    .line 162
    .local v29, "bitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    const/16 v28, 0x0

    .line 163
    .local v28, "artistMetajamId":Ljava/lang/String;
    const-wide/16 v26, 0x0

    .line 164
    .local v26, "artistIdLong":J
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 165
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 167
    :cond_1
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 168
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 171
    :cond_2
    const/16 v25, 0x0

    .line 172
    .local v25, "artistId":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 173
    move-object/from16 v25, v28

    .line 181
    :goto_1
    if-lez v22, :cond_8

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v35

    .line 182
    .local v35, "subtitle":Ljava/lang/String;
    :goto_2
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 183
    invoke-static/range {v35 .. v35}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 184
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "    "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 189
    :cond_3
    :goto_3
    invoke-static/range {v35 .. v35}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 190
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "    "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 194
    :goto_4
    new-instance v4, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v4}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v4, v0}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->val$albumId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v19

    .line 196
    .local v19, "albumItem":Lcom/google/android/music/leanback/Item;
    new-instance v16, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;-><init>(Ljava/lang/Object;)V

    .line 197
    .local v16, "albumDetails":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-virtual {v4}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "card_bitmap"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 198
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-virtual {v4}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-virtual {v4}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v7, "card_bitmap"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-direct {v5, v6, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 204
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-virtual {v4}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 206
    const/4 v9, 0x0

    .line 207
    .local v9, "id":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->val$albumId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    const v7, 0x7f0b00c8

    invoke-virtual {v6, v7}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    const v8, 0x7f0b02db

    invoke-virtual {v7, v8}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    add-int/lit8 v15, v9, 0x1

    .end local v9    # "id":I
    .local v15, "id":I
    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createPlayAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    invoke-static/range {v4 .. v9}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->access$100(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 210
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->val$albumId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    const v5, 0x7f0b0056

    invoke-virtual {v4, v5}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    const v5, 0x7f0b02db

    invoke-virtual {v4, v5}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    add-int/lit8 v9, v15, 0x1

    .end local v15    # "id":I
    .restart local v9    # "id":I
    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createPlayAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;
    invoke-static/range {v10 .. v15}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->access$100(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/support/v17/leanback/widget/Action;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 214
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 215
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    const v6, 0x7f0b0244

    invoke-virtual {v5, v6}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v15, v9, 0x1

    .end local v9    # "id":I
    .restart local v15    # "id":I
    move-object/from16 v0, v25

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createGoToArtistAction(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;
    invoke-static {v4, v0, v5, v9}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->access$200(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v17/leanback/widget/Action;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    move v9, v15

    .line 218
    .end local v15    # "id":I
    .restart local v9    # "id":I
    :cond_4
    return-object v16

    .line 150
    .end local v9    # "id":I
    .end local v16    # "albumDetails":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    .end local v19    # "albumItem":Lcom/google/android/music/leanback/Item;
    .end local v25    # "artistId":Ljava/lang/String;
    .end local v26    # "artistIdLong":J
    .end local v28    # "artistMetajamId":Ljava/lang/String;
    .end local v29    # "bitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .end local v35    # "subtitle":Ljava/lang/String;
    :cond_5
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->val$albumId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 151
    .local v20, "albumIdLong":J
    const-wide/16 v4, 0x0

    cmp-long v4, v20, v4

    if-ltz v4, :cond_0

    .line 152
    new-instance v18, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v17    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .local v18, "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    move-object/from16 v17, v18

    .end local v18    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .restart local v17    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    goto/16 :goto_0

    .line 174
    .end local v20    # "albumIdLong":J
    .restart local v25    # "artistId":Ljava/lang/String;
    .restart local v26    # "artistIdLong":J
    .restart local v28    # "artistMetajamId":Ljava/lang/String;
    .restart local v29    # "bitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    :cond_6
    const-wide/16 v4, 0x0

    cmp-long v4, v26, v4

    if-lez v4, :cond_7

    .line 175
    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_1

    .line 177
    :cond_7
    # getter for: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid artist ID in album "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$2;->val$albumId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 181
    :cond_8
    const/16 v35, 0x0

    goto/16 :goto_2

    .line 186
    .restart local v35    # "subtitle":Ljava/lang/String;
    :cond_9
    move-object/from16 v35, v24

    goto/16 :goto_3

    .line 192
    :cond_a
    move-object/from16 v35, v34

    goto/16 :goto_4

    .line 202
    .restart local v16    # "albumDetails":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    .restart local v19    # "albumItem":Lcom/google/android/music/leanback/Item;
    :cond_b
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 154
    .end local v16    # "albumDetails":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    .end local v19    # "albumItem":Lcom/google/android/music/leanback/Item;
    .end local v25    # "artistId":Ljava/lang/String;
    .end local v26    # "artistIdLong":J
    .end local v28    # "artistMetajamId":Ljava/lang/String;
    .end local v29    # "bitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .end local v35    # "subtitle":Ljava/lang/String;
    :catch_0
    move-exception v4

    goto/16 :goto_0
.end method
