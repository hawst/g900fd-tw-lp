.class Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;
.super Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;
.source "LeanbackAlbumDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Landroid/content/Context;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

.field final synthetic val$this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->val$this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$300(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V

    .line 309
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyChanged()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$400(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V

    .line 310
    return-void
.end method

.method public onItemRangeChanged(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 314
    if-nez p1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$500(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V

    .line 317
    :cond_0
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 321
    if-nez p1, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$300(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeInserted(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$600(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V

    .line 325
    :cond_0
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 329
    if-nez p1, :cond_0

    .line 330
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->recalculateSize()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$300(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;->this$1:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeRemoved(II)V
    invoke-static {v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->access$700(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V

    .line 333
    :cond_0
    return-void
.end method
