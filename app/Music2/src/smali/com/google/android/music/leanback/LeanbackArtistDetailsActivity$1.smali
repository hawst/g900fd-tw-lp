.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createArtistDetails(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

.field final synthetic val$artistId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;->val$artistId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 14
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v13, 0x1

    .line 116
    const/4 v10, 0x0

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 117
    .local v8, "name":Ljava/lang/String;
    invoke-interface {p1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "artUri":Ljava/lang/String;
    const/4 v10, 0x2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 119
    .local v7, "metajamId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 120
    const/4 v0, 0x0

    .line 127
    :goto_0
    const-wide/16 v2, 0x0

    .line 128
    .local v2, "artistIdLong":J
    iget-object v10, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;->val$artistId:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 129
    new-instance v9, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v9}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 130
    .local v9, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    invoke-static {v9, v8}, Lcom/google/android/music/store/Store;->canonicalizeAndGenerateId(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v6

    .line 132
    .local v6, "idAndCanonical":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    iget-object v10, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 141
    .end local v6    # "idAndCanonical":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v9    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    :goto_1
    new-instance v10, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v10}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v10, v8}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v10

    invoke-virtual {v10, v7}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v10

    invoke-virtual {v10, v13}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->secondaryId(J)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v4

    .line 143
    .local v4, "artistItem":Lcom/google/android/music/leanback/Item;
    new-instance v1, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    invoke-direct {v1, v4}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;-><init>(Ljava/lang/Object;)V

    .line 144
    .local v1, "artistDetails":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    iget-object v10, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    iget-object v11, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;->val$artistId:Ljava/lang/String;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createShuffleAllSongsAction(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;
    invoke-static {v10, v11, v8}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$100(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/Action;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 145
    return-object v1

    .line 122
    .end local v1    # "artistDetails":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    .end local v2    # "artistIdLong":J
    .end local v4    # "artistItem":Lcom/google/android/music/leanback/Item;
    :cond_0
    # getter for: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$000()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Art uri before strip:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils;->stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    # getter for: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$000()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Art uri after strip:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 135
    .restart local v2    # "artistIdLong":J
    :cond_1
    :try_start_0
    iget-object v10, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;->val$artistId:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_1

    .line 136
    :catch_0
    move-exception v5

    .line 137
    .local v5, "e":Ljava/lang/NumberFormatException;
    # getter for: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$000()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unexpected artist id! "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$1;->val$artistId:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
