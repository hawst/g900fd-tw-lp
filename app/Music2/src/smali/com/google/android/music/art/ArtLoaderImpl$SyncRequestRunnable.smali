.class Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;
.super Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
.source "ArtLoaderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtLoaderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncRequestRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtLoaderImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V
    .locals 0
    .param p2, "remoteArtUrl"    # Ljava/lang/String;
    .param p3, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .param p4, "listener"    # Lcom/google/android/music/art/ArtLoader$Listener;
    .param p5, "listenerHandler"    # Landroid/os/Handler;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    .line 244
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V

    .line 245
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 249
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 250
    .local v1, "latch":Ljava/util/concurrent/CountDownLatch;
    new-instance v3, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 252
    .local v3, "pfdHolder":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Landroid/os/ParcelFileDescriptor;>;"
    iget-object v4, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    # getter for: Lcom/google/android/music/art/ArtLoaderImpl;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;
    invoke-static {v4}, Lcom/google/android/music/art/ArtLoaderImpl;->access$400(Lcom/google/android/music/art/ArtLoaderImpl;)Lcom/google/android/music/download/artwork/ArtMonitor;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->mRemoteArtUrl:Ljava/lang/String;

    new-instance v6, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;

    invoke-direct {v6, p0, v3, v1}, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;-><init>(Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;)V

    invoke-interface {v4, v5, v6}, Lcom/google/android/music/download/artwork/ArtMonitor;->registerArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V

    .line 267
    invoke-virtual {p0}, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->getFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 268
    .local v2, "pfd":Landroid/os/ParcelFileDescriptor;
    if-eqz v2, :cond_0

    .line 269
    invoke-virtual {p0, v2}, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->notifyListener(Landroid/os/ParcelFileDescriptor;)V

    .line 280
    :goto_0
    return-void

    .line 274
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_1
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/ParcelFileDescriptor;

    invoke-virtual {p0, v4}, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->notifyListener(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v4, "ArtLoaderImpl"

    const-string v5, "Interrupted while synchronously getting art"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
