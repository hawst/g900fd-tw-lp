.class Lcom/google/android/music/dial/DialDevice$5;
.super Ljava/lang/Object;
.source "DialDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialDevice;->onMessageReceived(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialDevice;

.field final synthetic val$isFinal:Z

.field final synthetic val$message:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 396
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    iput-object p2, p0, Lcom/google/android/music/dial/DialDevice$5;->val$message:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/music/dial/DialDevice$5;->val$isFinal:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 399
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$800(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice$5;->val$message:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    iget-boolean v5, p0, Lcom/google/android/music/dial/DialDevice$5;->val$isFinal:Z

    if-eqz v5, :cond_1

    .line 402
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$800(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 403
    .local v4, "msgString":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/dial/GenericDialEventFactory;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/model/GenericDialEvent;

    move-result-object v3

    .line 404
    .local v3, "gMsg":Lcom/google/android/music/dial/model/GenericDialEvent;
    iget-object v5, v3, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v1, v5, Lcom/google/android/music/dial/model/EventHeaderJson;->mCmdId:Ljava/lang/String;

    .line 405
    .local v1, "commandId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 406
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mCallbackMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$900(Lcom/google/android/music/dial/DialDevice;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/DialDevice$Callback;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/dial/MalformedDialEventException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 407
    .local v0, "cbk":Lcom/google/android/music/dial/DialDevice$Callback;
    if-eqz v0, :cond_0

    .line 409
    :try_start_1
    invoke-interface {v0, v3}, Lcom/google/android/music/dial/DialDevice$Callback;->onResultReceived(Lcom/google/android/music/dial/model/GenericDialEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 411
    :try_start_2
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mCallbackMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$900(Lcom/google/android/music/dial/DialDevice;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/music/dial/MalformedDialEventException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 428
    .end local v0    # "cbk":Lcom/google/android/music/dial/DialDevice$Callback;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$800(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 431
    .end local v1    # "commandId":Ljava/lang/String;
    .end local v3    # "gMsg":Lcom/google/android/music/dial/model/GenericDialEvent;
    .end local v4    # "msgString":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 411
    .restart local v0    # "cbk":Lcom/google/android/music/dial/DialDevice$Callback;
    .restart local v1    # "commandId":Ljava/lang/String;
    .restart local v3    # "gMsg":Lcom/google/android/music/dial/model/GenericDialEvent;
    .restart local v4    # "msgString":Ljava/lang/String;
    :catchall_0
    move-exception v5

    :try_start_3
    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mCallbackMap:Ljava/util/Map;
    invoke-static {v6}, Lcom/google/android/music/dial/DialDevice;->access$900(Lcom/google/android/music/dial/DialDevice;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v5
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/android/music/dial/MalformedDialEventException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 421
    .end local v0    # "cbk":Lcom/google/android/music/dial/DialDevice$Callback;
    .end local v1    # "commandId":Ljava/lang/String;
    .end local v3    # "gMsg":Lcom/google/android/music/dial/model/GenericDialEvent;
    .end local v4    # "msgString":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 422
    .local v2, "e":Ljava/io/IOException;
    :try_start_4
    const-string v5, "DialDevice"

    const-string v6, "Error converting message to JSON"

    invoke-static {v5, v6, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 428
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$800(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_1

    .line 417
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "commandId":Ljava/lang/String;
    .restart local v3    # "gMsg":Lcom/google/android/music/dial/model/GenericDialEvent;
    .restart local v4    # "msgString":Ljava/lang/String;
    :cond_2
    :try_start_5
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/dial/DialDevice;->access$200(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/google/android/music/dial/DialDevice;->handleMessage(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 418
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/dial/DialDevice;->access$200(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/google/android/music/dial/DialDevice;->callListenersOnMessageReceived(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
    invoke-static {v5, v6, v3}, Lcom/google/android/music/dial/DialDevice;->access$1000(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/google/android/music/dial/MalformedDialEventException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 424
    .end local v1    # "commandId":Ljava/lang/String;
    .end local v3    # "gMsg":Lcom/google/android/music/dial/model/GenericDialEvent;
    .end local v4    # "msgString":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 425
    .local v2, "e":Lcom/google/android/music/dial/MalformedDialEventException;
    :try_start_6
    const-string v5, "DialDevice"

    const-string v6, "Event is missing required fields"

    invoke-static {v5, v6, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 428
    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$800(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_1

    .end local v2    # "e":Lcom/google/android/music/dial/MalformedDialEventException;
    :catchall_1
    move-exception v5

    iget-object v6, p0, Lcom/google/android/music/dial/DialDevice$5;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mMessageBuffer:Ljava/lang/StringBuilder;
    invoke-static {v6}, Lcom/google/android/music/dial/DialDevice;->access$800(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    throw v5
.end method
