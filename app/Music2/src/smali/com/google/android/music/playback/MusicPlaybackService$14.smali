.class Lcom/google/android/music/playback/MusicPlaybackService$14;
.super Ljava/lang/Object;
.source "MusicPlaybackService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;

.field final synthetic val$errorMsg:Ljava/lang/String;

.field final synthetic val$inError:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1902
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iput-boolean p2, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->val$inError:Z

    iput-object p3, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->val$errorMsg:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 1905
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1906
    const/4 v5, 0x3

    .line 1915
    .local v5, "playstate":I
    :goto_0
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v8}, Lcom/google/android/music/playback/DevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v3

    .line 1916
    .local v3, "mediaList":Lcom/google/android/music/medialist/SongList;
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->isInfiniteMixMode()Z

    move-result v8

    if-nez v8, :cond_7

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/android/music/medialist/SongList;->getStoreUrl()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_7

    :cond_0
    const/4 v1, 0x1

    .line 1918
    .local v1, "canShuffle":Z
    :goto_1
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->access$900(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v2

    .line 1920
    .local v2, "isDownloadedOnly":Z
    const-wide/16 v6, 0x0

    .line 1921
    .local v6, "position":J
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mRemotePlaybackControlEnabled:Z
    invoke-static {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->access$600(Lcom/google/android/music/playback/MusicPlaybackService;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1922
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->position()J

    move-result-wide v6

    .line 1925
    :cond_1
    new-instance v8, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;

    invoke-direct {v8}, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;-><init>()V

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v8, v5, v6, v7, v9}, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->setState(IJF)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->getMediaSessionTransportControlFlags()J
    invoke-static {v9}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1800(Lcom/google/android/music/playback/MusicPlaybackService;)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->setActions(J)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v10, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v10}, Lcom/google/android/music/playback/MusicPlaybackService;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v11}, Lcom/google/android/music/playback/MusicPlaybackService;->getRating()I

    move-result v11

    invoke-static {v9, v10, v11, v2, v1}, Lcom/google/android/music/playback/MediaSessionUtil;->getCurrentCustomAction(Landroid/content/Context;Lcom/google/android/music/playback/TrackInfo;IZZ)Ljava/util/List;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->setCustomActions(Ljava/util/List;)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v10, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v10}, Lcom/google/android/music/playback/MusicPlaybackService;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/playback/PlaybackState;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v11}, Lcom/google/android/music/playback/MusicPlaybackService;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/google/android/music/playback/MediaSessionUtil;->getQueueActiveId(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/download/ContentIdentifier;)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->setActiveItemId(J)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;

    move-result-object v0

    .line 1934
    .local v0, "builder":Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    iget-boolean v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->val$inError:Z

    if-eqz v8, :cond_2

    .line 1935
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->val$errorMsg:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->setErrorMessage(Ljava/lang/CharSequence;)Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;

    .line 1937
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;->build()Lcom/google/android/music/playback/session/PlaybackStateCompat;

    move-result-object v4

    .line 1938
    .local v4, "playbackState":Lcom/google/android/music/playback/session/PlaybackStateCompat;
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$100()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1939
    const-string v8, "MusicPlaybackService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "new playback state: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1941
    :cond_3
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;
    invoke-static {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1600(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/playback/session/MediaSessionCompat;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setPlaybackState(Lcom/google/android/music/playback/session/PlaybackStateCompat;)V

    return-void

    .line 1907
    .end local v0    # "builder":Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    .end local v1    # "canShuffle":Z
    .end local v2    # "isDownloadedOnly":Z
    .end local v3    # "mediaList":Lcom/google/android/music/medialist/SongList;
    .end local v4    # "playbackState":Lcom/google/android/music/playback/session/PlaybackStateCompat;
    .end local v5    # "playstate":I
    .end local v6    # "position":J
    :cond_4
    iget-boolean v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->val$inError:Z

    if-eqz v8, :cond_5

    .line 1908
    const/4 v5, 0x7

    .restart local v5    # "playstate":I
    goto/16 :goto_0

    .line 1909
    .end local v5    # "playstate":I
    :cond_5
    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v8}, Lcom/google/android/music/playback/DevicePlayback;->isPreparing()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService$14;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v8}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v8}, Lcom/google/android/music/playback/DevicePlayback;->isStreaming()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1911
    const/4 v5, 0x6

    .restart local v5    # "playstate":I
    goto/16 :goto_0

    .line 1913
    .end local v5    # "playstate":I
    :cond_6
    const/4 v5, 0x2

    .restart local v5    # "playstate":I
    goto/16 :goto_0

    .line 1916
    .restart local v3    # "mediaList":Lcom/google/android/music/medialist/SongList;
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1
.end method
