.class public Lcom/google/android/music/leanback/SongRow;
.super Landroid/support/v17/leanback/widget/Row;
.source "SongRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/SongRow$RadioIntentListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

.field private final mIntent:Landroid/content/Intent;

.field private final mIsLast:Z

.field private final mIsPlaying:Z

.field private final mItem:Lcom/google/android/music/leanback/Item;

.field private final mPosition:I

.field private mRadioClicked:Z

.field private mRadioIntent:Landroid/content/Intent;

.field private final mSendToast:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;Z)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "position"    # I
    .param p4, "intent"    # Landroid/content/Intent;
    .param p5, "item"    # Lcom/google/android/music/leanback/Item;
    .param p6, "isLast"    # Z

    .prologue
    .line 43
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/leanback/SongRow;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;ZZ)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;ZZ)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "position"    # I
    .param p4, "intent"    # Landroid/content/Intent;
    .param p5, "item"    # Lcom/google/android/music/leanback/Item;
    .param p6, "isPlaying"    # Z
    .param p7, "isLast"    # Z

    .prologue
    .line 48
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/leanback/SongRow;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;ZZZ)V

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;ZZZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "position"    # I
    .param p4, "intent"    # Landroid/content/Intent;
    .param p5, "item"    # Lcom/google/android/music/leanback/Item;
    .param p6, "isPlaying"    # Z
    .param p7, "sendToast"    # Z
    .param p8, "isLast"    # Z

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Row;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/SongRow;->mRadioClicked:Z

    .line 53
    iput-object p1, p0, Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/google/android/music/leanback/SongRow;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 55
    iput p3, p0, Lcom/google/android/music/leanback/SongRow;->mPosition:I

    .line 56
    iput-object p4, p0, Lcom/google/android/music/leanback/SongRow;->mIntent:Landroid/content/Intent;

    .line 57
    iput-object p5, p0, Lcom/google/android/music/leanback/SongRow;->mItem:Lcom/google/android/music/leanback/Item;

    .line 58
    iput-boolean p6, p0, Lcom/google/android/music/leanback/SongRow;->mIsPlaying:Z

    .line 59
    iput-boolean p7, p0, Lcom/google/android/music/leanback/SongRow;->mSendToast:Z

    .line 60
    iput-boolean p8, p0, Lcom/google/android/music/leanback/SongRow;->mIsLast:Z

    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/SongRow;->loadRadioIntent(Lcom/google/android/music/leanback/SongRow$RadioIntentListener;)V

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/SongRow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/SongRow;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/SongRow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/SongRow;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow;->mRadioIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/leanback/SongRow;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/SongRow;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/music/leanback/SongRow;->mRadioIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/music/leanback/SongRow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/SongRow;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/music/leanback/SongRow;->mRadioClicked:Z

    return p1
.end method

.method private loadRadioIntent(Lcom/google/android/music/leanback/SongRow$RadioIntentListener;)V
    .locals 7
    .param p1, "radioIntentListener"    # Lcom/google/android/music/leanback/SongRow$RadioIntentListener;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/music/leanback/SongRow;->getDocument()Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v1

    .line 124
    .local v1, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v2

    .line 125
    .local v2, "isNautilus":Z
    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 128
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :goto_0
    if-eqz v2, :cond_1

    new-instance v3, Lcom/google/android/music/medialist/NautilusSingleSongList;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    :goto_1
    new-instance v4, Lcom/google/android/music/leanback/SongRow$2;

    invoke-direct {v4, p0, v3, p1}, Lcom/google/android/music/leanback/SongRow$2;-><init>(Lcom/google/android/music/leanback/SongRow;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/leanback/SongRow$RadioIntentListener;)V

    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 150
    return-void

    .line 125
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    goto :goto_0

    .line 128
    .restart local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_1
    new-instance v3, Lcom/google/android/music/medialist/SingleSongList;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public getDocument()Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    return-object v0
.end method

.method public getItem()Lcom/google/android/music/leanback/Item;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow;->mItem:Lcom/google/android/music/leanback/Item;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/google/android/music/leanback/SongRow;->mPosition:I

    return v0
.end method

.method getRadioIntentListener()Lcom/google/android/music/leanback/SongRow$RadioIntentListener;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/music/leanback/SongRow$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/SongRow$1;-><init>(Lcom/google/android/music/leanback/SongRow;)V

    return-object v0
.end method

.method public isLast()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/music/leanback/SongRow;->mIsLast:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/music/leanback/SongRow;->mIsPlaying:Z

    return v0
.end method

.method onClicked()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 88
    :cond_0
    return-void
.end method

.method onRadioClicked()V
    .locals 7

    .prologue
    const v6, 0x7f0b0372

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/music/leanback/SongRow;->getRadioIntentListener()Lcom/google/android/music/leanback/SongRow$RadioIntentListener;

    move-result-object v0

    .line 101
    .local v0, "radioIntentListener":Lcom/google/android/music/leanback/SongRow$RadioIntentListener;
    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow;->mRadioIntent:Landroid/content/Intent;

    if-eqz v1, :cond_2

    .line 102
    iget-boolean v1, p0, Lcom/google/android/music/leanback/SongRow;->mSendToast:Z

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/SongRow;->getDocument()Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow;->mRadioIntent:Landroid/content/Intent;

    invoke-interface {v0, v1}, Lcom/google/android/music/leanback/SongRow$RadioIntentListener;->onRadioIntentLoaded(Landroid/content/Intent;)V

    .line 120
    :cond_1
    :goto_0
    return-void

    .line 109
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/music/leanback/SongRow;->mRadioClicked:Z

    if-nez v1, :cond_1

    .line 112
    iput-boolean v3, p0, Lcom/google/android/music/leanback/SongRow;->mRadioClicked:Z

    .line 113
    iget-boolean v1, p0, Lcom/google/android/music/leanback/SongRow;->mSendToast:Z

    if-eqz v1, :cond_3

    .line 114
    iget-object v1, p0, Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/leanback/SongRow;->mContext:Landroid/content/Context;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/SongRow;->getDocument()Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 118
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/SongRow;->loadRadioIntent(Lcom/google/android/music/leanback/SongRow$RadioIntentListener;)V

    goto :goto_0
.end method
