.class Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;
.super Landroid/database/MatrixCursor;
.source "ExploreGenreHeadersCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mGenreId:Ljava/lang/String;

.field private final mGenreName:Ljava/lang/String;

.field private final mItemHeight:I

.field private final mItemWidth:I

.field private final mParentGenreId:Ljava/lang/String;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "genreId"    # Ljava/lang/String;
    .param p4, "genreName"    # Ljava/lang/String;
    .param p5, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    .line 43
    iput-object p3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreId:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreName:Ljava/lang/String;

    .line 45
    iput-object p5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mParentGenreId:Ljava/lang/String;

    .line 46
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 47
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemWidth:I

    .line 48
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemHeight:I

    .line 52
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->extractRadio()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->extractSubgenres()V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->extractFeatured()V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->extractNewReleases()V

    .line 56
    return-void
.end method


# virtual methods
.method extractFeatured()V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 120
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 125
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 169
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v0

    new-array v13, v0, [Ljava/lang/Object;

    .line 132
    .local v13, "values":[Ljava/lang/Object;
    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 134
    .local v8, "groupId":J
    const/4 v0, 0x4

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 135
    .local v11, "groupType":I
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 136
    .local v10, "groupTitle":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 139
    .local v7, "groupDescription":Ljava/lang/String;
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "explore/genre"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "featured"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v12

    .line 148
    .local v12, "itemsUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "_id"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "name"

    invoke-virtual {v0, v13, v1, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "display_name"

    invoke-virtual {v0, v13, v1, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "background_image_uri"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "bg_image_uri"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "icon_uri"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "default_item_width"

    iget v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemWidth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "default_item_height"

    iget v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemHeight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "color_hint"

    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "badge_uri"

    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "items_uri"

    invoke-virtual {v0, v13, v1, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    invoke-virtual {p0, v13}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 167
    .end local v7    # "groupDescription":Ljava/lang/String;
    .end local v8    # "groupId":J
    .end local v10    # "groupTitle":Ljava/lang/String;
    .end local v11    # "groupType":I
    .end local v12    # "itemsUri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method extractNewReleases()V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 172
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$Explore;->getNewReleaseGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 177
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 221
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v0

    new-array v13, v0, [Ljava/lang/Object;

    .line 184
    .local v13, "values":[Ljava/lang/Object;
    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 186
    .local v8, "groupId":J
    const/4 v0, 0x4

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 187
    .local v11, "groupType":I
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 188
    .local v10, "groupTitle":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 191
    .local v7, "groupDescription":Ljava/lang/String;
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "explore/genre"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "newreleases"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v12

    .line 200
    .local v12, "itemsUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "_id"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "name"

    invoke-virtual {v0, v13, v1, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "display_name"

    invoke-virtual {v0, v13, v1, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "background_image_uri"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "bg_image_uri"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "icon_uri"

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "default_item_width"

    iget v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemWidth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "default_item_height"

    iget v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemHeight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "color_hint"

    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "badge_uri"

    iget-object v2, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v1, "items_uri"

    invoke-virtual {v0, v13, v1, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 216
    invoke-virtual {p0, v13}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 219
    .end local v7    # "groupDescription":Ljava/lang/String;
    .end local v8    # "groupId":J
    .end local v10    # "groupTitle":Ljava/lang/String;
    .end local v11    # "groupType":I
    .end local v12    # "itemsUri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method extractRadio()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 59
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v3}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v3

    new-array v2, v3, [Ljava/lang/Object;

    .line 60
    .local v2, "values":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0099

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "name":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "explore/genre"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "radio"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "items"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mParentGenreId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/music/xdi/XdiUtils;->getGenreIdForIntentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 70
    .local v0, "itemsUri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "_id"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "name"

    invoke-virtual {v3, v2, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "display_name"

    invoke-virtual {v3, v2, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "background_image_uri"

    invoke-virtual {v3, v2, v4, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 74
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "bg_image_uri"

    invoke-virtual {v3, v2, v4, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "icon_uri"

    invoke-virtual {v3, v2, v4, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 76
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "default_item_width"

    iget v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 77
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "default_item_height"

    iget v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "color_hint"

    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c00a6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 80
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "badge_uri"

    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 82
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "items_uri"

    invoke-virtual {v3, v2, v4, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    invoke-virtual {p0, v2}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->addRow([Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method extractSubgenres()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 89
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mParentGenreId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 117
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v3}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v3

    new-array v2, v3, [Ljava/lang/Object;

    .line 94
    .local v2, "values":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00a8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "name":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "explore/genre"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "items"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mParentGenreId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/music/xdi/XdiUtils;->getGenreIdForIntentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 102
    .local v0, "itemsUri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "_id"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "name"

    invoke-virtual {v3, v2, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 104
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "display_name"

    invoke-virtual {v3, v2, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "background_image_uri"

    invoke-virtual {v3, v2, v4, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "bg_image_uri"

    invoke-virtual {v3, v2, v4, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "icon_uri"

    invoke-virtual {v3, v2, v4, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "default_item_width"

    iget v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 109
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "default_item_height"

    iget v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mItemHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 110
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "color_hint"

    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c00a6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "badge_uri"

    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "items_uri"

    invoke-virtual {v3, v2, v4, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 116
    invoke-virtual {p0, v2}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
