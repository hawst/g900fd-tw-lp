.class public Lcom/google/android/music/store/SoundSearchPlaylistHelper;
.super Ljava/lang/Object;
.source "SoundSearchPlaylistHelper.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "SoundSearchHelper"

    sput-object v0, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method private static getSoundSearchEntries(Landroid/content/Context;Ljava/lang/String;I)Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "continuationToken"    # Ljava/lang/String;
    .param p2, "numResults"    # I

    .prologue
    const/4 v3, 0x0

    .line 27
    const/4 v2, 0x0

    .line 28
    .local v2, "response":Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 30
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/google/android/music/cloudclient/MusicCloud;->getSoundSearchEntries(Ljava/lang/String;I)Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    move-object v3, v2

    .line 38
    :goto_0
    return-object v3

    .line 31
    :catch_0
    move-exception v1

    .line 32
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v4, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 34
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 35
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getSoundSearchPlaylistEntries(Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "continuationToken":Ljava/lang/String;
    const/4 v1, 0x0

    .line 54
    .local v1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_downstream_page_size"

    const/16 v7, 0xfa

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 56
    .local v2, "maxNum":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_max_sound_search_playlist_size"

    const/16 v7, 0x3e8

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 60
    .local v3, "maxSize":I
    invoke-static {p0, v0, v2}, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->getSoundSearchEntries(Landroid/content/Context;Ljava/lang/String;I)Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;

    move-result-object v4

    .line 63
    .local v4, "response":Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;
    if-nez v4, :cond_1

    .line 64
    sget-object v5, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->TAG:Ljava/lang/String;

    const-string v6, "Failed to get playlist entries response"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v1

    .line 96
    :goto_0
    return-object v5

    .line 68
    :cond_1
    iget-object v5, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;

    if-eqz v5, :cond_2

    iget-object v5, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;

    invoke-virtual {v5}, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;->mPlaylistEntry:Ljava/util/List;

    if-eqz v5, :cond_2

    iget-object v5, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;->mPlaylistEntry:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 72
    :cond_2
    sget-object v5, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->TAG:Ljava/lang/String;

    const-string v6, "Got empty playlist entries response from server"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    if-nez v1, :cond_3

    .line 74
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_3
    move-object v5, v1

    .line 76
    goto :goto_0

    .line 80
    :cond_4
    if-nez v1, :cond_5

    .line 81
    iget-object v5, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;

    iget-object v1, v5, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;->mPlaylistEntry:Ljava/util/List;

    .line 92
    :goto_1
    iget-object v0, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mNextPageToken:Ljava/lang/String;

    .line 94
    if-nez v0, :cond_0

    :goto_2
    move-object v5, v1

    .line 96
    goto :goto_0

    .line 83
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    iget-object v6, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;->mPlaylistEntry:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/2addr v5, v6

    if-le v5, v3, :cond_6

    .line 85
    sget-object v5, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->TAG:Ljava/lang/String;

    const-string v6, "sound search playlist is larger than the max allowed: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 89
    :cond_6
    iget-object v5, v4, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;->mPlaylistEntry:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public static updateSoundSearchPlaylist(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/music/store/Store;)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    const-wide/16 v4, 0x0

    .line 109
    invoke-virtual {p2}, Lcom/google/android/music/store/Store;->getSoundSearchPlaylistId()J

    move-result-wide v2

    .line 110
    .local v2, "soundSearchPlaylistId":J
    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    move-wide v2, v4

    .line 122
    .end local v2    # "soundSearchPlaylistId":J
    :goto_0
    return-wide v2

    .line 113
    .restart local v2    # "soundSearchPlaylistId":J
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->getSoundSearchPlaylistEntries(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 114
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    if-nez v0, :cond_1

    move-wide v2, v4

    .line 115
    goto :goto_0

    .line 116
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    invoke-virtual {p2}, Lcom/google/android/music/store/Store;->clearSoundSearchPlaylist()V

    goto :goto_0

    .line 119
    :cond_2
    invoke-virtual {p2, p1, v2, v3, v0}, Lcom/google/android/music/store/Store;->replacePlaylistItems(Landroid/accounts/Account;JLjava/util/List;)V

    goto :goto_0
.end method
