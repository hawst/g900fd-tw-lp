.class public Lcom/google/android/music/cloudclient/ProfileRequest;
.super Ljava/lang/Object;
.source "ProfileRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/ProfileRequest$ProfileInfo;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/cloudclient/ProfileRequest;->LOGV:Z

    return-void
.end method
