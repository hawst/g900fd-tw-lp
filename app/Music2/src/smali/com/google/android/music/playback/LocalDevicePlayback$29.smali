.class Lcom/google/android/music/playback/LocalDevicePlayback$29;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->play()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 3667
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 3672
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3673
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2102(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 3674
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4402(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 3675
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 3678
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 3682
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/medialist/AllSongsList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/music/medialist/AllSongsList;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 3683
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3700(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3728
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 3730
    :goto_0
    return-void

    .line 3687
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3688
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->isQueueLoaded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3689
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->play()V

    .line 3726
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3728
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 3691
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->position()J

    move-result-wide v6

    const/4 v8, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3728
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 3701
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isRenderingAudioLocally()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3702
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3703
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3706
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3709
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->position()J

    move-result-wide v6

    const/4 v8, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V

    .line 3711
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7900(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 3712
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7900(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/music/playback/AsyncMediaPlayer;->seek(J)J

    .line 3713
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-wide/16 v2, -0x1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7902(Lcom/google/android/music/playback/LocalDevicePlayback;J)J

    .line 3718
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->duration()J

    move-result-wide v10

    .line 3719
    .local v10, "duration":J
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4100(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const-wide/16 v0, 0x7d0

    cmp-long v0, v10, v0

    if-lez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->position()J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    sub-long v2, v10, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_6

    .line 3721
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3700(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 3723
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$29;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method
