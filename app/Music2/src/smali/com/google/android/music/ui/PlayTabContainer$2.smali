.class Lcom/google/android/music/ui/PlayTabContainer$2;
.super Ljava/lang/Object;
.source "PlayTabContainer.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/PlayTabContainer;->setViewPager(Landroid/support/v4/view/ViewPager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/PlayTabContainer;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/PlayTabContainer;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/music/ui/PlayTabContainer$2;->this$0:Lcom/google/android/music/ui/PlayTabContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/music/ui/PlayTabContainer$2;->this$0:Lcom/google/android/music/ui/PlayTabContainer;

    iget-object v1, p0, Lcom/google/android/music/ui/PlayTabContainer$2;->this$0:Lcom/google/android/music/ui/PlayTabContainer;

    # getter for: Lcom/google/android/music/ui/PlayTabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/google/android/music/ui/PlayTabContainer;->access$000(Lcom/google/android/music/ui/PlayTabContainer;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/music/ui/PlayTabContainer;->scrollToChild(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/ui/PlayTabContainer;->access$100(Lcom/google/android/music/ui/PlayTabContainer;II)V

    .line 107
    iget-object v0, p0, Lcom/google/android/music/ui/PlayTabContainer$2;->this$0:Lcom/google/android/music/ui/PlayTabContainer;

    # getter for: Lcom/google/android/music/ui/PlayTabContainer;->mTabStrip:Lcom/google/android/music/ui/PlayTabStrip;
    invoke-static {v0}, Lcom/google/android/music/ui/PlayTabContainer;->access$200(Lcom/google/android/music/ui/PlayTabContainer;)Lcom/google/android/music/ui/PlayTabStrip;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/PlayTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 108
    return-void
.end method
