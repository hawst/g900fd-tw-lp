.class Lcom/google/android/music/leanback/LeanbackSearchActivity$1;
.super Ljava/lang/Object;
.source "LeanbackSearchActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/SpeechRecognitionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackSearchActivity;->setupFragment(Landroid/app/Fragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public recognizeSpeech()V
    .locals 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$500(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mSearchFragment:Lcom/google/android/music/leanback/LeanbackSearchFragment;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$600(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Lcom/google/android/music/leanback/LeanbackSearchFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$500(Lcom/google/android/music/leanback/LeanbackSearchActivity;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackSearchFragment;->setSearchQuery(Ljava/lang/String;Z)V

    .line 250
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/leanback/LeanbackSearchActivity;->mPendingSearchQuery:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$502(Lcom/google/android/music/leanback/LeanbackSearchActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackSearchActivity;->startSpeechRecognizerActivity()V
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$700(Lcom/google/android/music/leanback/LeanbackSearchActivity;)V

    goto :goto_0
.end method
