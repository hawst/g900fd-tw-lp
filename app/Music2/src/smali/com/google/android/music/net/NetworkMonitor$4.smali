.class Lcom/google/android/music/net/NetworkMonitor$4;
.super Ljava/lang/Object;
.source "NetworkMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/net/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/net/NetworkMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/music/net/NetworkMonitor;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitor$4;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 460
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$4;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v4

    monitor-enter v4

    .line 462
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$4;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 464
    .local v1, "i":I
    :goto_0
    if-lez v1, :cond_0

    .line 465
    add-int/lit8 v1, v1, -0x1

    .line 466
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$4;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/net/INetworkChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    .local v2, "listener":Lcom/google/android/music/net/INetworkChangeListener;
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$4;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$600(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v3

    iget-object v5, p0, Lcom/google/android/music/net/NetworkMonitor$4;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z
    invoke-static {v5}, Lcom/google/android/music/net/NetworkMonitor;->access$700(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v5

    invoke-interface {v2, v3, v5}, Lcom/google/android/music/net/INetworkChangeListener;->onNetworkChanged(ZZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 470
    :catch_0
    move-exception v0

    .line 471
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v3, "NetworkMonitor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error trying to notify NetworkChangeListener: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 476
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "i":I
    .end local v2    # "listener":Lcom/google/android/music/net/INetworkChangeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 475
    .restart local v1    # "i":I
    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$4;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 476
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 477
    return-void
.end method
