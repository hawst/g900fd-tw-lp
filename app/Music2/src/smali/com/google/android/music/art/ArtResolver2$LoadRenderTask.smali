.class Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;
.super Ljava/lang/Object;
.source "ArtResolver2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtResolver2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadRenderTask"
.end annotation


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mRequest:Lcom/google/android/music/art/ArtRequest2;

.field private final mResolver:Lcom/google/android/music/art/ArtResolver2;

.field private final mShortEdge:I


# direct methods
.method private constructor <init>(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;)V
    .locals 1
    .param p1, "resolver"    # Lcom/google/android/music/art/ArtResolver2;
    .param p2, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493
    const-string v0, "resolver must not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    const-string v0, "request must not be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mResolver:Lcom/google/android/music/art/ArtResolver2;

    .line 496
    iput-object p2, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    .line 497
    # getter for: Lcom/google/android/music/art/ArtResolver2;->mAppContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/music/art/ArtResolver2;->access$400(Lcom/google/android/music/art/ArtResolver2;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mAppContext:Landroid/content/Context;

    .line 498
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/utils/ViewUtils;->getShortestEdge(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mShortEdge:I

    .line 499
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtResolver2$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/art/ArtResolver2;
    .param p2, "x1"    # Lcom/google/android/music/art/ArtRequest2;
    .param p3, "x2"    # Lcom/google/android/music/art/ArtResolver2$1;

    .prologue
    .line 486
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;-><init>(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;)V

    return-void
.end method

.method private loadCachedArt(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "originInfo"    # Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;

    .prologue
    .line 576
    iget-object v0, p1, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .line 577
    .local v0, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    iget-object v5, p1, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artType:Lcom/google/android/music/art/ArtType;

    iget v6, p1, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->sizeBucket:I

    invoke-virtual {v5, v6}, Lcom/google/android/music/art/ArtType;->getBucketSize(I)I

    move-result v4

    .line 579
    .local v4, "width":I
    iget-object v5, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mResolver:Lcom/google/android/music/art/ArtResolver2;

    # getter for: Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->access$200(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    # invokes: Lcom/google/android/music/art/ArtResolver2;->loadArtUrl(Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    invoke-static {v5, v6, v7}, Lcom/google/android/music/art/ArtResolver2;->access$800(Lcom/google/android/music/art/ArtResolver2;Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 580
    .local v1, "fd":Landroid/os/ParcelFileDescriptor;
    if-eqz v1, :cond_0

    .line 581
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 582
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    iput v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 583
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6, v2}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 592
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    .local v3, "result":Landroid/graphics/Bitmap;
    :goto_0
    return-object v3

    .line 585
    .end local v3    # "result":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->useAlbumPipeline()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 586
    iget-object v5, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->getUrl()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mShortEdge:I

    iget v8, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mShortEdge:I

    const/4 v9, 0x1

    invoke-static {v5, v6, v7, v8, v9}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .restart local v3    # "result":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 589
    .end local v3    # "result":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromDisk(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .restart local v3    # "result":Landroid/graphics/Bitmap;
    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 520
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v10}, Lcom/google/android/music/art/ArtRequest2;->isInFinalState()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 573
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    const/16 v11, 0x15

    invoke-virtual {v10, v11}, Lcom/google/android/music/art/ArtRequest2;->externalChangeState(I)V

    .line 522
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v10}, Lcom/google/android/music/art/ArtRequest2;->getAvailableItems()Ljava/util/List;

    move-result-object v4

    .line 526
    .local v4, "itemsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/art/ArtResolver2$ArtUrl;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 527
    .local v7, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .line 528
    .local v9, "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v10}, Lcom/google/android/music/art/ArtRequest2;->isInFinalState()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 551
    .end local v9    # "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    new-array v8, v10, [Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .line 552
    .local v8, "results":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    invoke-interface {v7, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 553
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v10}, Lcom/google/android/music/art/ArtRequest2;->isInFinalState()Z

    move-result v10

    if-nez v10, :cond_3

    .line 556
    array-length v10, v8

    iget-object v11, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v11}, Lcom/google/android/music/art/ArtRequest2;->getMinimumNeededItemCount()I

    move-result v11

    if-ge v10, v11, :cond_9

    .line 557
    const-string v10, "ArtResolver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Not enough items to render request "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    const/16 v11, 0x29

    invoke-virtual {v10, v11}, Lcom/google/android/music/art/ArtRequest2;->externalChangeState(I)V

    .line 567
    :cond_3
    :goto_2
    move-object v0, v8

    .local v0, "arr$":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_3
    if-ge v2, v5, :cond_a

    aget-object v1, v0, v2

    .line 568
    .local v1, "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->release()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 569
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mResolver:Lcom/google/android/music/art/ArtResolver2;

    invoke-virtual {v10, v1}, Lcom/google/android/music/art/ArtResolver2;->removeManagedBitmap(Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    .line 567
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 530
    .end local v0    # "arr$":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v1    # "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v5    # "len$":I
    .end local v8    # "results":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .local v2, "i$":Ljava/util/Iterator;
    .restart local v9    # "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :cond_5
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    const/4 v11, 0x1

    invoke-virtual {v10, v9, v11}, Lcom/google/android/music/art/ArtRequest2;->generateOriginInfo(Lcom/google/android/music/art/ArtResolver2$ArtUrl;I)Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;

    move-result-object v3

    .line 532
    .local v3, "info":Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mResolver:Lcom/google/android/music/art/ArtResolver2;

    # invokes: Lcom/google/android/music/art/ArtResolver2;->checkManagedBitmapMap(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    invoke-static {v10, v3}, Lcom/google/android/music/art/ArtResolver2;->access$500(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v6

    .line 533
    .local v6, "result":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    if-eqz v6, :cond_7

    .line 534
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 547
    :cond_6
    :goto_4
    if-eqz v6, :cond_1

    .line 548
    invoke-virtual {v6}, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->retain()V

    goto :goto_1

    .line 536
    :cond_7
    invoke-direct {p0, v3}, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->loadCachedArt(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 537
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_6

    .line 538
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mResolver:Lcom/google/android/music/art/ArtResolver2;

    # invokes: Lcom/google/android/music/art/ArtResolver2;->createManagedBitmap(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;Landroid/graphics/Bitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    invoke-static {v10, v3, v1}, Lcom/google/android/music/art/ArtResolver2;->access$600(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;Landroid/graphics/Bitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v6

    .line 539
    if-nez v6, :cond_8

    .line 540
    const-string v10, "ArtResolver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error loading bitmap that ArtResolver2 thinks we should have. Origin info: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 543
    :cond_8
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 561
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "info":Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;
    .end local v6    # "result":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v9    # "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .restart local v8    # "results":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    :cond_9
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v10, v8}, Lcom/google/android/music/art/ArtRequest2;->renderImage([Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V

    goto :goto_2

    .line 572
    .restart local v0    # "arr$":[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .local v2, "i$":I
    .restart local v5    # "len$":I
    :cond_a
    iget-object v10, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mResolver:Lcom/google/android/music/art/ArtResolver2;

    iget-object v11, p0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->mRequest:Lcom/google/android/music/art/ArtRequest2;

    # invokes: Lcom/google/android/music/art/ArtResolver2;->notifyRequestListeners(Lcom/google/android/music/art/ArtRequest2;)V
    invoke-static {v10, v11}, Lcom/google/android/music/art/ArtResolver2;->access$700(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;)V

    goto/16 :goto_0
.end method
