.class public Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
.super Ljava/lang/Object;
.source "RemoteDeviceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/RemoteDeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mApplicationUrl:Landroid/net/Uri;

.field private mDeviceId:Ljava/lang/String;

.field private mDeviceType:Ljava/lang/String;

.field private mFriendlyName:Ljava/lang/String;

.field private mGroupId:Ljava/lang/String;

.field private mGroupName:Ljava/lang/String;

.field private mHouseholdId:Ljava/lang/String;

.field private mIsGroupCoordinator:Z

.field private mManufacturer:Ljava/lang/String;

.field private mManufacturerUrl:Landroid/net/Uri;

.field private mModelDescription:Ljava/lang/String;

.field private mModelName:Ljava/lang/String;

.field private mModelNumber:Ljava/lang/String;

.field private mRemoteServices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/dial/RemoteService;",
            ">;"
        }
    .end annotation
.end field

.field private mSerialNumber:Ljava/lang/String;

.field private mUniqueDeviceName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mRemoteServices:Ljava/util/List;

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/dial/RemoteDeviceInfo;)V
    .locals 1
    .param p1, "rdi"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mRemoteServices:Ljava/util/List;

    .line 50
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceId:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getUniqueDeviceName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mUniqueDeviceName:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceType:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mFriendlyName:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getManufacturer()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturer:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getManufacturerUrl()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturerUrl:Landroid/net/Uri;

    .line 56
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getModelDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelDescription:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getModelName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelName:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getModelNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelNumber:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mSerialNumber:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getApplicationUrl()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mApplicationUrl:Landroid/net/Uri;

    .line 61
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getRemoteServices()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mRemoteServices:Ljava/util/List;

    .line 62
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getGroupId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupId:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getGroupName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupName:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getIsGroupCoordinator()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mIsGroupCoordinator:Z

    .line 65
    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getHouseholdId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mHouseholdId:Ljava/lang/String;

    .line 66
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mApplicationUrl:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mRemoteServices:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mIsGroupCoordinator:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mHouseholdId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mUniqueDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mFriendlyName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturer:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturerUrl:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelNumber:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public addRemoteService(Lcom/google/android/music/dial/RemoteService;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 1
    .param p1, "remoteService"    # Lcom/google/android/music/dial/RemoteService;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mRemoteServices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    return-object p0
.end method

.method public build()Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/music/dial/RemoteDeviceInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo;-><init>(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;Lcom/google/android/music/dial/RemoteDeviceInfo$1;)V

    return-object v0
.end method

.method public setApplicationUrl(Landroid/net/Uri;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "applicationUrl"    # Landroid/net/Uri;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mApplicationUrl:Landroid/net/Uri;

    .line 110
    return-object p0
.end method

.method public setDeviceId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceId:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public setDeviceType(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceType:Ljava/lang/String;

    .line 78
    return-object p0
.end method

.method public setFriendlyName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "friendlyName"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mFriendlyName:Ljava/lang/String;

    .line 82
    return-object p0
.end method

.method public setGroupId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "groupId"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupId:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public setGroupName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "groupName"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupName:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public setHouseholdId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mHouseholdId:Ljava/lang/String;

    .line 134
    return-object p0
.end method

.method public setIsGroupCoordinator(Z)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mIsGroupCoordinator:Z

    .line 130
    return-object p0
.end method

.method public setManufacturer(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "manufacturer"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturer:Ljava/lang/String;

    .line 86
    return-object p0
.end method

.method public setManufacturerUrl(Landroid/net/Uri;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "manufacturerUrl"    # Landroid/net/Uri;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturerUrl:Landroid/net/Uri;

    .line 90
    return-object p0
.end method

.method public setModelDescription(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "modelDescription"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelDescription:Ljava/lang/String;

    .line 94
    return-object p0
.end method

.method public setModelName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelName:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method public setModelNumber(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "modelNumber"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelNumber:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method public setRemoteServices(Ljava/util/List;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/dial/RemoteService;",
            ">;)",
            "Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "remoteServices":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/dial/RemoteService;>;"
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mRemoteServices:Ljava/util/List;

    .line 114
    return-object p0
.end method

.method public setSerialNumber(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "serialNumber"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mSerialNumber:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method public setUniqueDeviceName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .locals 0
    .param p1, "uniqueDeviceName"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mUniqueDeviceName:Ljava/lang/String;

    .line 74
    return-object p0
.end method
