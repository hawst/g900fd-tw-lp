.class Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultiUrlCompositeMode"
.end annotation


# instance fields
.field protected mDrawRadioOverlay:Z

.field protected mUrls:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 515
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 513
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mDrawRadioOverlay:Z

    .line 516
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    .prologue
    .line 518
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 513
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mDrawRadioOverlay:Z

    .line 519
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    .line 520
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 584
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 533
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v13, 0x0

    .line 556
    :cond_0
    return-object v13

    .line 535
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    invoke-static {v13}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 536
    .local v12, "urls":[Ljava/lang/String;
    array-length v5, v12

    .line 537
    .local v5, "artCount":I
    new-array v8, v5, [Landroid/os/ParcelFileDescriptor;

    .line 538
    .local v8, "fds":[Landroid/os/ParcelFileDescriptor;
    const/4 v3, 0x1

    .line 539
    .local v3, "allArtsLoaded":Z
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v5, :cond_2

    .line 541
    :try_start_0
    aget-object v13, v12, v9

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14, v15}, Lcom/google/android/music/store/MusicContent$UrlArt;->openFileDescriptor(Landroid/content/Context;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v13

    aput-object v13, v8, v9
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 539
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 542
    :catch_0
    move-exception v6

    .line 543
    .local v6, "e":Ljava/io/FileNotFoundException;
    aget-object v13, v12, v9

    move-object/from16 v0, p5

    invoke-interface {v0, v13}, Lcom/google/android/music/download/artwork/RemoteUrlSink;->report(Ljava/lang/String;)V

    .line 544
    const/4 v3, 0x0

    goto :goto_1

    .line 549
    .end local v6    # "e":Ljava/io/FileNotFoundException;
    :cond_2
    if-eqz v3, :cond_4

    .line 550
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mDrawRadioOverlay:Z

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v13, v1, v2, v14}, Lcom/google/android/music/utils/AlbumArtUtils;->getMultiImageComposite(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 556
    move-object v4, v8

    .local v4, "arr$":[Landroid/os/ParcelFileDescriptor;
    array-length v11, v4

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_2
    if-ge v10, v11, :cond_0

    aget-object v7, v4, v10

    .line 558
    .local v7, "fd":Landroid/os/ParcelFileDescriptor;
    if-eqz v7, :cond_3

    :try_start_2
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 556
    :cond_3
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 553
    .end local v4    # "arr$":[Landroid/os/ParcelFileDescriptor;
    .end local v7    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    :cond_4
    const/4 v13, 0x0

    .line 556
    move-object v4, v8

    .restart local v4    # "arr$":[Landroid/os/ParcelFileDescriptor;
    array-length v11, v4

    .restart local v11    # "len$":I
    const/4 v10, 0x0

    .restart local v10    # "i$":I
    :goto_4
    if-ge v10, v11, :cond_0

    aget-object v7, v4, v10

    .line 558
    .restart local v7    # "fd":Landroid/os/ParcelFileDescriptor;
    if-eqz v7, :cond_5

    :try_start_3
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 556
    :cond_5
    :goto_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .end local v4    # "arr$":[Landroid/os/ParcelFileDescriptor;
    .end local v7    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    :catchall_0
    move-exception v13

    move-object v4, v8

    .restart local v4    # "arr$":[Landroid/os/ParcelFileDescriptor;
    array-length v11, v4

    .restart local v11    # "len$":I
    const/4 v10, 0x0

    .restart local v10    # "i$":I
    :goto_6
    if-ge v10, v11, :cond_7

    aget-object v7, v4, v10

    .line 558
    .restart local v7    # "fd":Landroid/os/ParcelFileDescriptor;
    if-eqz v7, :cond_6

    :try_start_4
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 556
    :cond_6
    :goto_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .end local v7    # "fd":Landroid/os/ParcelFileDescriptor;
    :cond_7
    throw v13

    .line 559
    .restart local v7    # "fd":Landroid/os/ParcelFileDescriptor;
    :catch_1
    move-exception v14

    goto :goto_3

    :catch_2
    move-exception v14

    goto :goto_5

    :catch_3
    move-exception v14

    goto :goto_7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 571
    instance-of v1, p1, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    if-nez v1, :cond_0

    .line 572
    const/4 v1, 0x0

    .line 575
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 574
    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    .line 575
    .local v0, "o":Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 527
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 566
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MultiUrlCompositeMode<url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
