.class public Lcom/google/android/music/leanback/LeanbackLauncherActivity;
.super Lcom/google/android/music/leanback/LeanbackBrowseActivity;
.source "LeanbackLauncherActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/NowPlayingItem$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;,
        Lcom/google/android/music/leanback/LeanbackLauncherActivity$ShadowlessListRow;,
        Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

.field private mNautilusEnabled:Ljava/lang/Boolean;

.field private mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

.field private mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

.field private final mPlaylistObserver:Landroid/database/ContentObserver;

.field private mRecentActivityAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

.field private mRecommendationsObjectAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

.field private final mUiStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

.field private final mUpdateNautilusRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;-><init>()V

    .line 130
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mHandler:Landroid/os/Handler;

    .line 131
    new-instance v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$1;

    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getAsyncHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistObserver:Landroid/database/ContentObserver;

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNautilusEnabled:Ljava/lang/Boolean;

    .line 151
    new-instance v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUiStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    .line 166
    new-instance v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$3;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUpdateNautilusRunnable:Ljava/lang/Runnable;

    .line 871
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Lcom/google/android/music/AsyncCursor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->showHidePlaylistRow()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->extractDataForConcierge(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUpdateNautilusRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNautilusEnabled:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->updateNautilusEnabled(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->extractDataForRecents(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->extractDataForMainstage(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->extractDataForPlaylists(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getConciergeListRow(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method private addNautilusDependentRows()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x7

    const/4 v2, 0x6

    .line 272
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->clear(I)V

    .line 273
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->clear(I)V

    .line 274
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-virtual {v0, v4}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->clear(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNautilusEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->showSituations(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->buildConciergeListRow()V

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecommendationsObjectAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->showFeelingLuckyVisibile(Z)V

    .line 283
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getRadioListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getExploreListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 289
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecommendationsObjectAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;->showFeelingLuckyVisibile(Z)V

    .line 287
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getInstantMixesListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private buildConciergeListRow()V
    .locals 5

    .prologue
    .line 414
    move-object v0, p0

    .line 415
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/16 v2, 0x12d

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;

    invoke-direct {v4, p0, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 436
    return-void
.end method

.method private createFeelingLuckyRadioItem(Z)Lcom/google/android/music/leanback/Item;
    .locals 6
    .param p1, "forceTwoLines"    # Z

    .prologue
    const v4, 0x7f0b02ed

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 333
    invoke-static {p0}, Lcom/google/android/music/mix/MixDescriptor;->getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v0

    .line 336
    .local v0, "feelingLuckyIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 337
    const v2, 0x7f0b02eb

    invoke-virtual {p0, v2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 344
    .local v1, "feelingLuckyName":Ljava/lang/String;
    :goto_0
    new-instance v2, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v2}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    if-eqz p1, :cond_1

    const-string v2, " "

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    const v3, 0x7f020045

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->iconResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    const v3, 0x7f0200cf

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v2

    return-object v2

    .line 340
    .end local v1    # "feelingLuckyName":Ljava/lang/String;
    :cond_0
    const v2, 0x7f0b02ec

    invoke-virtual {p0, v2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "feelingLuckyName":Ljava/lang/String;
    goto :goto_0

    .line 344
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private extractDataForConcierge(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x0

    .line 544
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 545
    .local v2, "id":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 546
    .local v4, "name":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 547
    .local v0, "artUri":Ljava/lang/String;
    const/4 v5, 0x2

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 549
    .local v1, "description":Ljava/lang/String;
    invoke-static {p0, v2, v4}, Lcom/google/android/music/leanback/LeanbackUtils;->newSituationIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 550
    .local v3, "intent":Landroid/content/Intent;
    new-instance v5, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v5}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v5, v4}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v5

    return-object v5
.end method

.method private extractDataForMainstage(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 26
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 658
    move-object/from16 v12, p0

    .line 660
    .local v12, "context":Landroid/content/Context;
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 661
    .local v16, "id":J
    const/4 v9, 0x0

    .line 662
    .local v9, "artUri":Ljava/lang/String;
    const/16 v19, 0x0

    .line 663
    .local v19, "name":Ljava/lang/String;
    const/4 v13, 0x0

    .line 664
    .local v13, "description":Ljava/lang/String;
    const/16 v22, 0x0

    .line 665
    .local v22, "stringAlbumId":Ljava/lang/String;
    const/16 v20, 0x0

    .line 666
    .local v20, "overlayResourceId":I
    const/16 v23, 0x0

    .line 667
    .local v23, "wide":Z
    const/4 v2, 0x0

    .line 669
    .local v2, "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    const/4 v15, 0x0

    .line 671
    .local v15, "intent":Landroid/content/Intent;
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 672
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 673
    .local v10, "albumId":J
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 674
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 675
    const v3, 0x7f0b00c5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 677
    :cond_0
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 678
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 679
    const v3, 0x7f0b00c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 681
    :cond_1
    const/4 v3, 0x1

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v4

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v5

    invoke-static {v10, v11, v3, v4, v5}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 684
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    .line 685
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    .line 746
    .end local v10    # "albumId":J
    :goto_0
    if-eqz v2, :cond_2

    .line 747
    invoke-virtual {v2}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;->startLoading()V

    .line 749
    :cond_2
    new-instance v3, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v15}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v3

    :goto_1
    return-object v3

    .line 686
    :cond_3
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 687
    new-instance v14, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v14}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 688
    .local v14, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v14, v1}, Lcom/google/android/music/ui/MainstageDocumentHelper;->buildMainstageDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 689
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 690
    .local v18, "listType":I
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v19

    .line 691
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getSubTitle()Ljava/lang/String;

    move-result-object v13

    .line 692
    const v20, 0x7f0200ce

    .line 693
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v8

    .line 694
    .local v8, "songList":Lcom/google/android/music/medialist/SongList;
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v9

    .line 695
    if-nez v9, :cond_4

    .line 696
    new-instance v2, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;

    .end local v2    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    add-int/lit16 v5, v3, 0x1388

    const-wide/32 v6, -0x21524111

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v24, v0

    add-long v6, v6, v24

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLcom/google/android/music/medialist/SongList;)V

    .line 702
    .restart local v2    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    :cond_4
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v15

    .line 704
    sparse-switch v18, :sswitch_data_0

    goto/16 :goto_0

    .line 707
    :sswitch_0
    const v20, 0x7f0200cf

    .line 708
    goto/16 :goto_0

    .line 713
    .end local v8    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v14    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v18    # "listType":I
    :cond_5
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 714
    new-instance v14, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v14}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 715
    .restart local v14    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v14, v1}, Lcom/google/android/music/ui/MainstageDocumentHelper;->buildMainstageDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 716
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v19

    .line 717
    const v3, 0x7f0b0264

    invoke-virtual {v12, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 718
    const v20, 0x7f0200cf

    .line 719
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v9

    .line 721
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v21, -0x1

    .line 724
    .local v21, "radioSeedType":I
    :goto_2
    const/4 v3, 0x4

    move/from16 v0, v21

    if-ne v0, v3, :cond_7

    const/16 v23, 0x1

    .line 726
    :goto_3
    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v15

    .line 728
    goto/16 :goto_0

    .line 721
    .end local v21    # "radioSeedType":I
    :cond_6
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    goto :goto_2

    .line 724
    .restart local v21    # "radioSeedType":I
    :cond_7
    const/16 v23, 0x0

    goto :goto_3

    .line 728
    .end local v14    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v21    # "radioSeedType":I
    :cond_8
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_a

    .line 732
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 733
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 734
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 735
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_9

    .line 736
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 739
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    goto/16 :goto_0

    .line 741
    :cond_a
    sget-object v3, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected mainstage item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Landroid/database/DatabaseUtils;->dumpCursorToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 704
    nop

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0x3c -> :sswitch_0
    .end sparse-switch
.end method

.method private extractDataForPlaylists(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 20
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 755
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 756
    .local v4, "id":J
    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 757
    .local v6, "name":Ljava/lang/String;
    const/4 v9, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 758
    .local v15, "description":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    const v9, 0x7f0b00fd

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 760
    :cond_0
    const/4 v9, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 761
    .local v2, "artUri":Ljava/lang/String;
    const/4 v9, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 762
    .local v7, "listType":I
    const/16 v17, 0x0

    .line 763
    .local v17, "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    new-instance v3, Lcom/google/android/music/medialist/PlaylistSongList;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v13}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 767
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    if-nez v2, :cond_1

    .line 768
    new-instance v8, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v10

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    add-int/lit16 v11, v9, 0x1770

    const-wide/32 v12, -0x21524111

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    int-to-long v0, v9

    move-wide/from16 v18, v0

    add-long v12, v12, v18

    move-object/from16 v9, p0

    move-object v14, v3

    invoke-direct/range {v8 .. v14}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLcom/google/android/music/medialist/SongList;)V

    .line 773
    .end local v17    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    .local v8, "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    invoke-virtual {v8}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;->startLoading()V

    .line 775
    :goto_0
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v16

    .line 776
    .local v16, "intent":Landroid/content/Intent;
    new-instance v9, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v9}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v9, v6}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v15}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v2}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    const v10, 0x7f0200ce

    invoke-virtual {v9, v10}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v9

    return-object v9

    .end local v8    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    .end local v16    # "intent":Landroid/content/Intent;
    .restart local v17    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    :cond_1
    move-object/from16 v8, v17

    .end local v17    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    .restart local v8    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    goto :goto_0
.end method

.method private extractDataForRecents(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    .locals 26
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 561
    move-object/from16 v12, p0

    .line 563
    .local v12, "context":Landroid/content/Context;
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 564
    .local v16, "id":J
    const/4 v9, 0x0

    .line 565
    .local v9, "artUri":Ljava/lang/String;
    const/16 v19, 0x0

    .line 566
    .local v19, "name":Ljava/lang/String;
    const/4 v13, 0x0

    .line 567
    .local v13, "description":Ljava/lang/String;
    const/16 v22, 0x0

    .line 568
    .local v22, "stringAlbumId":Ljava/lang/String;
    const/16 v20, 0x0

    .line 569
    .local v20, "overlayResourceId":I
    const/16 v23, 0x0

    .line 570
    .local v23, "wide":Z
    const/4 v2, 0x0

    .line 571
    .local v2, "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    const/4 v15, 0x0

    .line 573
    .local v15, "intent":Landroid/content/Intent;
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 574
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 575
    .local v10, "albumId":J
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 576
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 577
    const v3, 0x7f0b00c5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 579
    :cond_0
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 580
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 581
    const v3, 0x7f0b00c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 583
    :cond_1
    const/4 v3, 0x1

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v4

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v5

    invoke-static {v10, v11, v3, v4, v5}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 586
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    .line 587
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    .line 649
    .end local v10    # "albumId":J
    :goto_0
    if-eqz v2, :cond_2

    .line 650
    invoke-virtual {v2}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;->startLoading()V

    .line 652
    :cond_2
    new-instance v3, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v15}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v3

    :goto_1
    return-object v3

    .line 588
    :cond_3
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 589
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3}, Lcom/google/android/music/ui/RecentCardDocumentHelper;->buildSingleDocument(Landroid/content/Context;Landroid/database/Cursor;I)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v14

    .line 591
    .local v14, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 592
    .local v18, "listType":I
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v19

    .line 593
    const v20, 0x7f0200ce

    .line 594
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getSubTitle()Ljava/lang/String;

    move-result-object v13

    .line 596
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v8

    .line 597
    .local v8, "songList":Lcom/google/android/music/medialist/SongList;
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v9

    .line 598
    if-nez v9, :cond_4

    .line 599
    new-instance v2, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;

    .end local v2    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    add-int/lit16 v5, v3, 0xfa0

    const-wide/32 v6, -0x21524111

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v24, v0

    add-long v6, v6, v24

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLcom/google/android/music/medialist/SongList;)V

    .line 607
    .restart local v2    # "songListBitmapGettersGetter":Lcom/google/android/music/leanback/bitmap/SongListBitmapGettersGetter;
    :cond_4
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v15

    .line 608
    sparse-switch v18, :sswitch_data_0

    goto/16 :goto_0

    .line 611
    :sswitch_0
    const v20, 0x7f0200cf

    .line 612
    goto/16 :goto_0

    .line 617
    .end local v8    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v14    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v18    # "listType":I
    :cond_5
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 618
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3}, Lcom/google/android/music/ui/RecentCardDocumentHelper;->buildSingleDocument(Landroid/content/Context;Landroid/database/Cursor;I)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v14

    .line 620
    .restart local v14    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v19

    .line 621
    const v3, 0x7f0b0264

    invoke-virtual {v12, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 622
    const v20, 0x7f0200cf

    .line 623
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v9

    .line 625
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v21, -0x1

    .line 628
    .local v21, "radioSeedType":I
    :goto_2
    const/4 v3, 0x4

    move/from16 v0, v21

    if-ne v0, v3, :cond_7

    const/16 v23, 0x1

    .line 630
    :goto_3
    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Landroid/content/Intent;

    move-result-object v15

    .line 631
    goto/16 :goto_0

    .line 625
    .end local v21    # "radioSeedType":I
    :cond_6
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    goto :goto_2

    .line 628
    .restart local v21    # "radioSeedType":I
    :cond_7
    const/16 v23, 0x0

    goto :goto_3

    .line 631
    .end local v14    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v21    # "radioSeedType":I
    :cond_8
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_a

    .line 635
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 636
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 637
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 638
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_9

    .line 639
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 642
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    goto/16 :goto_0

    .line 644
    :cond_a
    sget-object v3, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected mainstage item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Landroid/database/DatabaseUtils;->dumpCursorToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 608
    nop

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0x3c -> :sswitch_0
    .end sparse-switch
.end method

.method private getConciergeListRow(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 439
    new-instance v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$8;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$8;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 445
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/16 v1, 0x12c

    invoke-static {}, Lcom/google/android/music/store/MusicContent$Situations;->getTopSituationsUri()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/songza/SituationCardHelper;->SITUATION_PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 448
    const/4 v1, 0x7

    invoke-virtual {p0, v1, p1, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(ILjava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    return-object v1
.end method

.method private getExploreListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 14

    .prologue
    const v13, 0x7f0b00a7

    const v12, 0x7f0b00a6

    const v11, 0x7f0b00a5

    const v10, 0x7f0b00a4

    const/4 v7, 0x0

    .line 476
    new-instance v8, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 478
    .local v8, "exploreAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {p0, v12}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    new-instance v0, Lcom/google/android/music/leanback/bitmap/ExploreTopChartsBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/16 v3, 0x3e8

    const-wide/32 v4, 0x186a0

    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mHandler:Landroid/os/Handler;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/leanback/bitmap/ExploreTopChartsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/os/Handler;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {p0, v12}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v7}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreTopChartsIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 485
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {p0, v10}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v9

    new-instance v0, Lcom/google/android/music/leanback/bitmap/ExploreRecommendedBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/16 v3, 0x7d0

    const-wide/32 v4, 0x30d40

    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mHandler:Landroid/os/Handler;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/bitmap/ExploreRecommendedBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/os/Handler;)V

    invoke-virtual {v9, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {p0, v10}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreRecommendedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 493
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {p0, v11}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/leanback/bitmap/ExploreNewReleasesBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/16 v4, 0x9

    const-wide/32 v5, 0x783883

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/leanback/bitmap/ExploreNewReleasesBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {p0, v11}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v7}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreNewReleasesIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 502
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {p0, v13}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v6

    new-instance v0, Lcom/google/android/music/leanback/bitmap/ExploreGenresBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/16 v3, 0xa

    const-wide/16 v4, -0x2fbe

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/ExploreGenresBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    invoke-virtual {v6, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {p0, v13}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v7}, Lcom/google/android/music/leanback/LeanbackUtils;->newExploreGenresIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 511
    const/4 v0, 0x5

    const v1, 0x7f0b009b

    invoke-virtual {p0, v0, v1, v8}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method private getInstantMixesListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 5

    .prologue
    .line 515
    new-instance v0, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 518
    .local v0, "instantMixesAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    const v3, 0x7f0b00ae

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 519
    .local v2, "name":Ljava/lang/String;
    invoke-static {p0, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newMyMixesIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 520
    .local v1, "intent":Landroid/content/Intent;
    new-instance v3, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 523
    const v3, 0x7f0b00ad

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 524
    invoke-static {p0, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newSuggestedMixesIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 525
    new-instance v3, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 528
    const/4 v3, 0x6

    const v4, 0x7f0b009a

    invoke-virtual {p0, v3, v4, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v3

    return-object v3
.end method

.method private getMyMusicListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 12

    .prologue
    .line 371
    new-instance v9, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v0

    invoke-direct {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 373
    .local v9, "myMusicAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newShuffleAllPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v10

    .line 375
    .local v10, "shuffleAllIntent":Landroid/content/Intent;
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newMyArtistsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v7

    .line 377
    .local v7, "myArtistsIntent":Landroid/content/Intent;
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newMyGenresIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v8

    .line 379
    .local v8, "myGenresIntent":Landroid/content/Intent;
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newMyAlbumsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v6

    .line 381
    .local v6, "myAlbumsIntent":Landroid/content/Intent;
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    const v1, 0x7f0b0054

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    new-instance v0, Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x6

    const-wide/32 v4, -0x5ed14e0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    invoke-virtual {v11, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    const v1, 0x7f0200d0

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 385
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    const v1, 0x7f0b009e

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    new-instance v0, Lcom/google/android/music/leanback/bitmap/MyArtistsBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x7

    const-wide/16 v4, -0x1d2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/MyArtistsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    invoke-virtual {v11, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 389
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    const v1, 0x7f0b00a0

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    new-instance v0, Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x5

    const-wide/32 v4, 0x1eebe20

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    invoke-virtual {v11, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 393
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    const v1, 0x7f0b009f

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v11

    new-instance v0, Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x4

    const-wide/16 v4, 0x7b

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    invoke-virtual {v11, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 397
    const/4 v0, 0x2

    const v1, 0x7f0b0097

    invoke-virtual {p0, v0, v1, v9}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method private getPlaylistsListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 4

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$6;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$6;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 407
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/music/medialist/AllPlaylists;

    invoke-direct {v2}, Lcom/google/android/music/medialist/AllPlaylists;-><init>()V

    sget-object v3, Lcom/google/android/music/ui/PlaylistClustersFragment;->CURSOR_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 409
    const/4 v1, 0x3

    const v2, 0x7f0b0098

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    return-object v1
.end method

.method private getRadioListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 11

    .prologue
    .line 452
    new-instance v9, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v0

    invoke-direct {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 454
    .local v9, "radioAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createFeelingLuckyRadioItem(Z)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 456
    const v0, 0x7f0b00ab

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 457
    .local v8, "name":Ljava/lang/String;
    invoke-static {p0, v8}, Lcom/google/android/music/leanback/LeanbackUtils;->newMyMixesIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 458
    .local v7, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v6

    new-instance v0, Lcom/google/android/music/leanback/bitmap/MyMixesBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/16 v3, 0x8

    const-wide v4, 0xd8ea451ad5L

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/MyMixesBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    invoke-virtual {v6, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 464
    const v0, 0x7f0b00aa

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 465
    invoke-static {p0, v8}, Lcom/google/android/music/leanback/LeanbackUtils;->newSuggestedMixesIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 466
    new-instance v0, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v10

    new-instance v0, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/16 v3, 0xbb8

    const-wide/32 v4, 0xbcbc12

    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/bitmap/SuggestedMixesBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJLandroid/os/Handler;)V

    invoke-virtual {v10, v0}, Lcom/google/android/music/leanback/Item$Builder;->bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 472
    const/4 v0, 0x4

    const v1, 0x7f0b0099

    invoke-virtual {p0, v0, v1, v9}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method private getRecentActivityListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 311
    new-instance v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$4;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$4;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 317
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    new-instance v2, Lcom/google/android/music/medialist/RecentItemsList;

    invoke-direct {v2}, Lcom/google/android/music/medialist/RecentItemsList;-><init>()V

    sget-object v3, Lcom/google/android/music/ui/RecentCardDocumentHelper;->RECENT_PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v4, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 320
    new-instance v1, Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v1}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;-><init>()V

    .line 321
    .local v1, "classPresenterSelector":Landroid/support/v17/leanback/widget/ClassPresenterSelector;
    const-class v2, Lcom/google/android/music/leanback/Item;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 322
    const-class v2, Lcom/google/android/music/leanback/NowPlayingItem;

    new-instance v3, Lcom/google/android/music/leanback/NowPlayingItemPresenter;

    invoke-direct {v3, p0}, Lcom/google/android/music/leanback/NowPlayingItemPresenter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 324
    new-instance v2, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-direct {v2, v1, v3, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecentActivityAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    .line 327
    const v2, 0x7f0b024b

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecentActivityAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    invoke-virtual {p0, v4, v2, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    return-object v2
.end method

.method private getRecommendationsListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 351
    invoke-direct {p0, v5}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createFeelingLuckyRadioItem(Z)Lcom/google/android/music/leanback/Item;

    move-result-object v2

    .line 352
    .local v2, "feelingLuckyItem":Lcom/google/android/music/leanback/Item;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$5;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$5;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 358
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    new-instance v3, Lcom/google/android/music/medialist/MainstageList;

    invoke-direct {v3}, Lcom/google/android/music/medialist/MainstageList;-><init>()V

    sget-object v4, Lcom/google/android/music/ui/MainstageDocumentHelper;->MAINSTAGE_PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v5, v0, v3, v4}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 361
    new-instance v1, Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v1}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;-><init>()V

    .line 362
    .local v1, "classPresenterSelector":Landroid/support/v17/leanback/widget/ClassPresenterSelector;
    const-class v3, Lcom/google/android/music/leanback/Item;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 363
    new-instance v3, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecommendationsObjectAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    .line 366
    const v3, 0x7f0b024a

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecommendationsObjectAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecommendationsObjectAdapter;

    invoke-virtual {p0, v5, v3, v4}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v3

    return-object v3
.end method

.method private getSettingsListRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 9

    .prologue
    .line 532
    new-instance v0, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    new-instance v3, Lcom/google/android/music/leanback/IconPresenter;

    invoke-direct {v3}, Lcom/google/android/music/leanback/IconPresenter;-><init>()V

    invoke-direct {v0, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 535
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    const v3, 0x7f0b0209

    invoke-virtual {p0, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 536
    .local v2, "name":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newLicensesIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 537
    .local v1, "intent":Landroid/content/Intent;
    new-instance v3, Lcom/google/android/music/leanback/Icon$Builder;

    invoke-direct {v3}, Lcom/google/android/music/leanback/Icon$Builder;-><init>()V

    const v4, 0x7f020109

    invoke-virtual {v3, v4}, Lcom/google/android/music/leanback/Icon$Builder;->iconResourceId(I)Lcom/google/android/music/leanback/Icon$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/music/leanback/Icon$Builder;->label(Ljava/lang/String;)Lcom/google/android/music/leanback/Icon$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/music/leanback/Icon$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Icon$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/leanback/Icon$Builder;->build()Lcom/google/android/music/leanback/Icon;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 539
    new-instance v3, Lcom/google/android/music/leanback/LeanbackLauncherActivity$ShadowlessListRow;

    const-wide/16 v4, 0x8

    new-instance v6, Landroid/support/v17/leanback/widget/HeaderItem;

    const v7, 0x7f0b0058

    invoke-virtual {p0, v7}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4, v5, v6, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$ShadowlessListRow;-><init>(JLandroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    return-object v3
.end method

.method private showHidePlaylistRow()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 292
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncCursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getPlaylistsListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->clear(I)V

    goto :goto_0
.end method

.method private startPlaylistMonitor()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncCursor;->requery()Z

    .line 301
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->showHidePlaylistRow()V

    .line 302
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncCursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 303
    return-void
.end method

.method private stopPlaylistMonitor()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncCursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 307
    return-void
.end method

.method private updateNautilusEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNautilusEnabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNautilusEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq p1, v0, :cond_1

    .line 266
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNautilusEnabled:Ljava/lang/Boolean;

    .line 267
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->addNautilusDependentRows()V

    .line 269
    :cond_1
    return-void
.end method


# virtual methods
.method protected getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getRecentActivityListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getRecommendationsListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getMyMusicListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    const/16 v1, 0x63

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getSettingsListRow()Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 242
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getDatabaseVersion()I

    move-result v0

    invoke-static {}, Lcom/google/android/music/store/Store;->getDatabaseVersion()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 244
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->updateNautilusEnabled(Z)V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 175
    new-instance v11, Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v11}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;-><init>()V

    .line 176
    .local v11, "selector":Landroid/support/v17/leanback/widget/ClassPresenterSelector;
    const-class v0, Landroid/support/v17/leanback/widget/ListRow;

    new-instance v1, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-virtual {v11, v0, v1}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 178
    new-instance v10, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v10}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    .line 179
    .local v10, "presenter":Landroid/support/v17/leanback/widget/ListRowPresenter;
    invoke-virtual {v10, v7}, Landroid/support/v17/leanback/widget/ListRowPresenter;->setShadowEnabled(Z)V

    .line 180
    invoke-virtual {v10, v7}, Landroid/support/v17/leanback/widget/ListRowPresenter;->setSelectEffectEnabled(Z)V

    .line 181
    const-class v0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$ShadowlessListRow;

    invoke-virtual {v11, v0, v10}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 183
    new-instance v0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    invoke-direct {v0, v11}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    .line 184
    new-instance v0, Lcom/google/android/music/leanback/NowPlayingItem;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1, p0}, Lcom/google/android/music/leanback/NowPlayingItem;-><init>(Lcom/google/android/music/leanback/LeanbackItemActivity;ILcom/google/android/music/leanback/NowPlayingItem$Listener;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    .line 185
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getBackgroundImageMessageHandler()Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/NowPlayingItem;->init(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)V

    .line 188
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v3, v7

    const-string v0, "playlist_name"

    aput-object v0, v3, v9

    .line 190
    .local v3, "cols":[Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/AsyncCursor;

    sget-object v2, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/music/AsyncCursor;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZZ)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

    .line 194
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUiStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->registerUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 195
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/NowPlayingItem;->cleanup()V

    .line 229
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mPlaylistCursor:Lcom/google/android/music/AsyncCursor;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncCursor;->close()V

    .line 230
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mUiStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->unregisterUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 231
    invoke-super {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onDestroy()V

    .line 232
    return-void
.end method

.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 1
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-virtual {v0, p2}, Lcom/google/android/music/leanback/NowPlayingItem;->onItemSelected(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 255
    :cond_0
    return-void
.end method

.method public onNowPlayingChanged()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecentActivityAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mRecentActivityAdapter:Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->onNowPlayingChanged()V

    .line 262
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->stopPlaylistMonitor()V

    .line 223
    invoke-super {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onPause()V

    .line 224
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 199
    invoke-super {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;->onResume()V

    .line 201
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->startPlaylistMonitor()V

    .line 204
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 205
    .local v1, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getAllAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 206
    .local v0, "accounts":[Landroid/accounts/Account;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v2

    if-nez v2, :cond_2

    .line 208
    const/4 v2, 0x1

    array-length v3, v0

    if-ne v2, v3, :cond_1

    .line 209
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->launchVerificationCheck(Landroid/content/Context;)V

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->launchAccountSwitcherIntent(Landroid/app/Activity;)V

    goto :goto_0

    .line 214
    :cond_2
    array-length v2, v0

    if-nez v2, :cond_0

    .line 216
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->launchAccountSwitcherIntent(Landroid/app/Activity;)V

    goto :goto_0
.end method
