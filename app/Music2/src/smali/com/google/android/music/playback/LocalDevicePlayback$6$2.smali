.class Lcom/google/android/music/playback/LocalDevicePlayback$6$2;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$6;->onSuccess(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$6;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$6;)V
    .locals 0

    .prologue
    .line 492
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 496
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$6;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-string v1, "com.google.android.music.refreshcomplete"

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$6;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 500
    return-void

    .line 498
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$6;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
