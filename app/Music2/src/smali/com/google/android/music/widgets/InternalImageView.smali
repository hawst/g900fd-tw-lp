.class Lcom/google/android/music/widgets/InternalImageView;
.super Landroid/widget/ImageView;
.source "InternalImageView.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final LOGV:Z


# instance fields
.field private mBroadcasting:Z

.field private final mCheckable:Z

.field private mExpectedAspectRatio:F

.field private mIsChecked:Z

.field private mListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

.field private final mParent:Lcom/google/android/music/widgets/ArtImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/widgets/InternalImageView;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/widgets/ArtImageView;Z)V
    .locals 2
    .param p1, "parent"    # Lcom/google/android/music/widgets/ArtImageView;
    .param p2, "checkable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-virtual {p1}, Lcom/google/android/music/widgets/ArtImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 25
    iput-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    .line 26
    iput-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mBroadcasting:Z

    .line 29
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mExpectedAspectRatio:F

    .line 33
    iput-boolean p2, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    .line 34
    iput-object p1, p0, Lcom/google/android/music/widgets/InternalImageView;->mParent:Lcom/google/android/music/widgets/ArtImageView;

    .line 35
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/music/widgets/InternalImageView;->invalidate()V

    .line 105
    return-void
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    goto :goto_0
.end method

.method public onCreateDrawableState(I)[I
    .locals 4
    .param p1, "extraSpace"    # I

    .prologue
    .line 110
    iget-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    if-eqz v1, :cond_1

    .line 111
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/ImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 112
    .local v0, "state":[I
    iget-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    if-eqz v1, :cond_0

    .line 113
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x10100a0

    aput v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/music/widgets/InternalImageView;->mergeDrawableStates([I[I)[I

    .line 118
    :cond_0
    :goto_0
    return-object v0

    .line 116
    .end local v0    # "state":[I
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    .restart local v0    # "state":[I
    goto :goto_0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 49
    const-class v0, Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 50
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    if-eqz v0, :cond_0

    .line 51
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 53
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 58
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-class v0, Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 60
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    if-eqz v0, :cond_0

    .line 61
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 62
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 65
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v9, -0x80000000

    const/high16 v8, 0x40000000    # 2.0f

    .line 146
    iget v6, p0, Lcom/google/android/music/widgets/InternalImageView;->mExpectedAspectRatio:F

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v6, v7

    const v7, 0x3c23d70a    # 0.01f

    cmpg-float v6, v6, v7

    if-gez v6, :cond_0

    .line 149
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 150
    .local v4, "widthMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 151
    .local v1, "heightMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 152
    .local v5, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 154
    .local v2, "heightSize":I
    if-nez v4, :cond_2

    if-nez v1, :cond_2

    .line 192
    .end local v1    # "heightMode":I
    .end local v2    # "heightSize":I
    .end local v4    # "widthMode":I
    .end local v5    # "widthSize":I
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/music/widgets/InternalImageView;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/music/widgets/InternalImageView;->mExpectedAspectRatio:F

    div-float v0, v6, v7

    .line 195
    .local v0, "calculatedHeight":F
    invoke-virtual {p0}, Lcom/google/android/music/widgets/InternalImageView;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v6, v0

    if-gez v6, :cond_1

    .line 196
    invoke-virtual {p0}, Lcom/google/android/music/widgets/InternalImageView;->getMeasuredWidth()I

    move-result v6

    float-to-int v7, v0

    invoke-virtual {p0, v6, v7}, Lcom/google/android/music/widgets/InternalImageView;->setMeasuredDimension(II)V

    .line 198
    :cond_1
    return-void

    .line 156
    .end local v0    # "calculatedHeight":F
    .restart local v1    # "heightMode":I
    .restart local v2    # "heightSize":I
    .restart local v4    # "widthMode":I
    .restart local v5    # "widthSize":I
    :cond_2
    if-ne v4, v8, :cond_3

    if-ne v1, v8, :cond_3

    .line 157
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 158
    .local v3, "size":I
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    move p1, p2

    .line 160
    goto :goto_0

    .end local v3    # "size":I
    :cond_3
    if-eq v4, v8, :cond_4

    if-ne v1, v8, :cond_8

    .line 161
    :cond_4
    if-ne v4, v8, :cond_6

    .line 163
    if-ne v1, v9, :cond_5

    if-lt v2, v5, :cond_0

    .line 168
    :cond_5
    move p2, p1

    goto :goto_0

    .line 172
    :cond_6
    if-ne v4, v9, :cond_7

    if-lt v5, v2, :cond_0

    .line 177
    :cond_7
    move p1, p2

    goto :goto_0

    .line 180
    :cond_8
    if-ne v4, v9, :cond_9

    if-ne v1, v9, :cond_9

    .line 181
    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 182
    .restart local v3    # "size":I
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    move p1, p2

    .line 184
    goto :goto_0

    .end local v3    # "size":I
    :cond_9
    if-eq v4, v9, :cond_a

    if-ne v1, v9, :cond_c

    .line 185
    :cond_a
    if-ne v4, v9, :cond_b

    move v3, v5

    .line 186
    .restart local v3    # "size":I
    :goto_1
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    move p1, p2

    .line 188
    goto :goto_0

    .end local v3    # "size":I
    :cond_b
    move v3, v2

    .line 185
    goto :goto_1

    .line 189
    :cond_c
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown modes: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/widgets/InternalImageView;->toggle()V

    .line 124
    :cond_0
    invoke-super {p0}, Landroid/widget/ImageView;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 4
    .param p1, "checked"    # Z

    .prologue
    .line 69
    iget-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    if-nez v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    sget-boolean v1, Lcom/google/android/music/widgets/InternalImageView;->LOGV:Z

    if-eqz v1, :cond_2

    .line 71
    const-string v1, "InternalImageView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setChecked: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mParent:Lcom/google/android/music/widgets/ArtImageView;

    instance-of v1, v1, Lcom/google/android/music/widgets/ArtImageCheckbox;

    if-nez v1, :cond_3

    .line 74
    const-string v1, "InternalImageView"

    const-string v2, "setChecked called on a non-checkbox view."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mParent:Lcom/google/android/music/widgets/ArtImageView;

    check-cast v0, Lcom/google/android/music/widgets/ArtImageCheckbox;

    .line 78
    .local v0, "checkbox":Lcom/google/android/music/widgets/ArtImageCheckbox;
    iget-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    if-eq v1, p1, :cond_0

    .line 79
    iput-boolean p1, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    .line 80
    invoke-virtual {p0}, Lcom/google/android/music/widgets/InternalImageView;->refreshDrawableState()V

    .line 81
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 87
    const/16 v1, 0x800

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/InternalImageView;->sendAccessibilityEvent(I)V

    .line 89
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mBroadcasting:Z

    if-nez v1, :cond_0

    .line 93
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mBroadcasting:Z

    .line 94
    iget-object v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    if-eqz v1, :cond_5

    .line 95
    iget-object v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    iget-boolean v2, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    invoke-interface {v1, v0, v2}, Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;->onCheckedChanged(Lcom/google/android/music/widgets/ArtImageCheckbox;Z)V

    .line 97
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/widgets/InternalImageView;->mBroadcasting:Z

    goto :goto_0
.end method

.method public setExpectedAspectRatio(F)V
    .locals 0
    .param p1, "expectedAspectRatio"    # F

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/music/widgets/InternalImageView;->mExpectedAspectRatio:F

    .line 43
    invoke-virtual {p0}, Lcom/google/android/music/widgets/InternalImageView;->invalidate()V

    .line 44
    return-void
.end method

.method public setOnCheckedChangeListener(Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/music/widgets/InternalImageView;->mListener:Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    .line 141
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mCheckable:Z

    if-nez v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/widgets/InternalImageView;->mIsChecked:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/InternalImageView;->setChecked(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
