.class Lcom/google/android/music/leanback/SongRowPresenter$5;
.super Ljava/lang/Object;
.source "SongRowPresenter.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/SongRowPresenter;->updateSelector(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/SongRowPresenter;

.field final synthetic val$deltaWidth:F

.field final synthetic val$lp:Landroid/view/ViewGroup$LayoutParams;

.field final synthetic val$selectorView:Landroid/view/View;

.field final synthetic val$targetWidth:I


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/SongRowPresenter;Landroid/view/ViewGroup$LayoutParams;IFLandroid/view/View;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->this$0:Lcom/google/android/music/leanback/SongRowPresenter;

    iput-object p2, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    iput p3, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$targetWidth:I

    iput p4, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$deltaWidth:F

    iput-object p5, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$selectorView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    iget v1, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$targetWidth:I

    iget v2, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$deltaWidth:F

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v4

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 175
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRowPresenter$5;->val$selectorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 176
    return-void
.end method
