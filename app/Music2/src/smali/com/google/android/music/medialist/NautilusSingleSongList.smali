.class public Lcom/google/android/music/medialist/NautilusSingleSongList;
.super Lcom/google/android/music/medialist/NautilusSongList;
.source "NautilusSingleSongList.java"


# instance fields
.field private final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private final mNautilusTrackId:Ljava/lang/String;

.field private final mTrackTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "nautilusTrackId"    # Ljava/lang/String;
    .param p3, "trackTitle"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/music/medialist/NautilusSongList;-><init>()V

    .line 26
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid nautilus track id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p2, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mNautilusTrackId:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mTrackTitle:Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 32
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mNautilusTrackId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mTrackTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mNautilusTrackId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$XAudio;->getNautilusAudioUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mTrackTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getNautilusId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mNautilusTrackId:Ljava/lang/String;

    return-object v0
.end method

.method public isAllInLibrary(Landroid/content/Context;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 37
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    .line 43
    .local v2, "cols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusSingleSongList;->mNautilusTrackId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$XAudio;->getLockerTrackUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 47
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 51
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 53
    :goto_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 56
    :cond_0
    return v6

    :cond_1
    move v6, v9

    .line 51
    goto :goto_0

    .line 53
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method
