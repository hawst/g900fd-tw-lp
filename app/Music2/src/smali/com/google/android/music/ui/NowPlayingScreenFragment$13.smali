.class Lcom/google/android/music/ui/NowPlayingScreenFragment$13;
.super Landroid/content/BroadcastReceiver;
.source "NowPlayingScreenFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 1251
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1254
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1255
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->DEBUG_QUEUE:Z
    invoke-static {}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1800()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1256
    const-string v1, "NowPlayingFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive: action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    :cond_0
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1261
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfo(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2600(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/content/Intent;)V

    .line 1262
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshPlaybackControls()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1263
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefreshNow()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1295
    :cond_1
    :goto_0
    return-void

    .line 1264
    :cond_2
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1265
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshPlaybackControls()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1267
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefreshNow()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto :goto_0

    .line 1268
    :cond_3
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1272
    :cond_4
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1273
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$600(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1281
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    const-wide/16 v2, -0x1

    # setter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mPosOverride:J
    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1502(Lcom/google/android/music/ui/NowPlayingScreenFragment;J)J

    .line 1282
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshPlaybackControls()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1283
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfo(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2600(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/content/Intent;)V

    goto :goto_0

    .line 1274
    :cond_5
    const-string v1, "playing"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1275
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefreshNow()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto :goto_1

    .line 1278
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshNow()J
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J

    goto :goto_1

    .line 1284
    :cond_7
    const-string v1, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1285
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateButtonsVisibility()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2800(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto :goto_0

    .line 1286
    :cond_8
    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1287
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupPlayQueue()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto/16 :goto_0

    .line 1288
    :cond_9
    const-string v1, "com.google.android.music.refreshcomplete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1289
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->unlockNowPlayingScreen()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 1290
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->setupPlayQueue()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto/16 :goto_0

    .line 1291
    :cond_a
    const-string v1, "com.google.android.music.refreshfailed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1292
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$13;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->unlockNowPlayingScreen()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto/16 :goto_0
.end method
