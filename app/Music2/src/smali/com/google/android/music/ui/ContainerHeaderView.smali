.class public Lcom/google/android/music/ui/ContainerHeaderView;
.super Landroid/widget/RelativeLayout;
.source "ContainerHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final sBackgroundHandler:Landroid/os/Handler;

.field private static final sMainHandler:Landroid/os/Handler;


# instance fields
.field mActionButtons:[Lcom/google/android/music/ui/BaseActionButton;

.field private mActionsContainer:Landroid/view/ViewGroup;

.field private mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mAlbumYear:Landroid/widget/TextView;

.field private mAlbumYearHyphen:Landroid/widget/TextView;

.field private mAlbumYearValue:I

.field private mArtClearedOnStop:Z

.field private mArtistArt1:Landroid/widget/ImageView;

.field private mArtistArt1ShowingNonDefaultArt:Z

.field private mArtistArt2:Landroid/widget/ImageView;

.field private final mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

.field private final mArtistArtHelper2:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

.field private mArtistArtUrl:Ljava/lang/String;

.field private mArtistArtUrls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mArtistId:J

.field private mArtistMetajamId:Ljava/lang/String;

.field private mBuyButton:Landroid/view/View;

.field private mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

.field private mCurrentArtistArtUrlIndex:I

.field private mFragment:Lcom/google/android/music/ui/BaseListFragment;

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mOverflow:Landroid/view/View;

.field private mOwnerPhoto:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mPinButton:Lcom/google/android/music/KeepOnView;

.field private mPlayRadio:Lcom/google/android/music/ui/BaseActionButton;

.field private mPrimaryTitle:Ljava/lang/String;

.field private mSecondaryTitle:Ljava/lang/String;

.field private mSharePlaylist:Lcom/google/android/music/ui/BaseActionButton;

.field private mShowingFirstSlide:Z

.field private mShuffle:Lcom/google/android/music/ui/BaseActionButton;

.field private mSlideRefreshRunnable:Ljava/lang/Runnable;

.field private mSlideShowHandler:Landroid/os/Handler;

.field private mSlideShowInitialized:Z

.field private mSlideSwitchRunnable:Ljava/lang/Runnable;

.field private mSongCount:Landroid/widget/TextView;

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mSubtitle:Landroid/widget/TextView;

.field private mTitle:Lcom/google/android/music/widgets/MarqueeTextView;

.field private mTopShadow:Landroid/widget/ImageView;

.field private mViewFlipper:Landroid/widget/ViewFlipper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v1

    sput-boolean v1, Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z

    .line 158
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/music/ui/ContainerHeaderView;->sMainHandler:Landroid/os/Handler;

    .line 159
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ArtHelperThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 160
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 161
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/music/ui/ContainerHeaderView;->sBackgroundHandler:Landroid/os/Handler;

    .line 162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 165
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistId:J

    .line 108
    iput v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearValue:I

    .line 121
    iput-boolean v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z

    .line 122
    iput-boolean v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowInitialized:Z

    .line 124
    iput-boolean v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtClearedOnStop:Z

    .line 154
    new-instance v0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    const v1, 0x7f0e00e8

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;-><init>(Lcom/google/android/music/ui/ContainerHeaderView;I)V

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    .line 155
    new-instance v0, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    const v1, 0x7f0e00e9

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;-><init>(Lcom/google/android/music/ui/ContainerHeaderView;I)V

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper2:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    .line 166
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/BaseListFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/AsyncAlbumArtImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/widgets/MarqueeTextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mTitle:Lcom/google/android/music/widgets/MarqueeTextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSubtitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYear:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearHyphen:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/android/music/ui/ContainerHeaderView;Lcom/google/android/music/ui/cardlib/model/Document;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPrimaryTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/ui/ContainerHeaderView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/google/android/music/ui/ContainerHeaderView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z

    return p1
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPrimaryTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper2:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    return-object v0
.end method

.method static synthetic access$2200()Z
    .locals 1

    .prologue
    .line 66
    sget-boolean v0, Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ViewFlipper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/music/ui/ContainerHeaderView;Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ContainerHeaderView;->clearArtistArt(Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getNextArtistUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/music/ui/ContainerHeaderView;->sMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2900()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/music/ui/ContainerHeaderView;->sBackgroundHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSecondaryTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/music/ui/ContainerHeaderView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtClearedOnStop:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSecondaryTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3102(Lcom/google/android/music/ui/ContainerHeaderView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1ShowingNonDefaultArt:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/ContainerHeaderView;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistId:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/google/android/music/ui/ContainerHeaderView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistId:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistMetajamId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/ContainerHeaderView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearValue:I

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/music/ui/ContainerHeaderView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearValue:I

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/ContainerHeaderView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ContainerHeaderView;->showOwnerProfilePicture(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ContainerHeaderView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    return-object v0
.end method

.method private clearArtistArt(Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "iv"    # Landroid/widget/ImageView;

    .prologue
    .line 672
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 673
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 674
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 676
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 677
    return-void
.end method

.method private disablePinning()V
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setVisibility(I)V

    .line 580
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setOnClick(Z)V

    .line 581
    return-void
.end method

.method private enablePinning()V
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 575
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setOnClick(Z)V

    .line 576
    return-void
.end method

.method private extractArtistArtUrls(Landroid/database/Cursor;I)V
    .locals 16
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "howMany"    # I

    .prologue
    .line 758
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 760
    .local v6, "start":J
    const-string v9, "ArtistArtLocation"

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 763
    .local v2, "artistArtIdx":I
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    .line 768
    .local v5, "origPos":I
    const/4 v9, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 770
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 771
    .local v3, "artistsArtUrlsSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 772
    .local v4, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v9

    move/from16 v0, p2

    if-ge v9, v0, :cond_2

    const/16 v9, 0x3e8

    if-ge v4, v9, :cond_2

    .line 775
    if-ltz v2, :cond_1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 778
    .local v8, "url":Ljava/lang/String;
    :goto_1
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 779
    invoke-virtual {v3, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 781
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 782
    goto :goto_0

    .line 775
    .end local v8    # "url":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 786
    :cond_2
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v9, -0x1

    if-eq v5, v9, :cond_3

    .line 787
    const-string v9, "ContainerHeader"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Failed to restore cursor position. Current pos="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    :cond_3
    new-instance v9, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    .line 792
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 794
    sget-boolean v9, Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z

    if-eqz v9, :cond_4

    .line 795
    const-string v9, "ContainerHeader"

    const-string v10, "Gathering %d artist urls took: %d ms. Looked at %d rows."

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v6

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    :cond_4
    return-void
.end method

.method private getFirstArtistUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 842
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mCurrentArtistArtUrlIndex:I

    .line 843
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mCurrentArtistArtUrlIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private getNextArtistUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 851
    iget v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mCurrentArtistArtUrlIndex:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mCurrentArtistArtUrlIndex:I

    .line 852
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mCurrentArtistArtUrlIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private isDownloadedOnlyMode()Z
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setupArtistArtSlideShow(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 687
    iget-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowInitialized:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->hasCount(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 689
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 690
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    .line 692
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 693
    new-instance v0, Lcom/google/android/music/ui/ContainerHeaderView$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/ContainerHeaderView$4;-><init>(Lcom/google/android/music/ui/ContainerHeaderView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    .line 723
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_2

    .line 724
    new-instance v0, Lcom/google/android/music/ui/ContainerHeaderView$5;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/ContainerHeaderView$5;-><init>(Lcom/google/android/music/ui/ContainerHeaderView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    .line 751
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowInitialized:Z

    .line 753
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->startArtistArtSlideShow()V

    .line 755
    :cond_3
    return-void
.end method

.method private shouldDoArtistSlideShow()Z
    .locals 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0}, Lcom/google/android/music/medialist/SongList;->shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowSongCount()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 589
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v0, :cond_0

    .line 599
    :goto_0
    return v2

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v0, :cond_2

    .line 595
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v0, Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v0

    const/16 v3, 0x32

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v2, v1

    .line 599
    goto :goto_0
.end method

.method private showButtonIfXLarge(Landroid/view/View;)V
    .locals 1
    .param p1, "button"    # Landroid/view/View;

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseListFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 570
    :cond_0
    return-void
.end method

.method private showOwnerProfilePicture(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 603
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mOwnerPhoto:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setVisibility(I)V

    .line 605
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mOwnerPhoto:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    .line 607
    :cond_0
    return-void
.end method

.method private startArtistArtSlideShow()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x5dc

    .line 805
    iget-boolean v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowInitialized:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 838
    :cond_0
    :goto_0
    return-void

    .line 807
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 808
    sget-boolean v2, Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z

    if-eqz v2, :cond_0

    .line 809
    const-string v2, "ContainerHeader"

    const-string v3, "Not enough artists to do a slide show"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 815
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v2

    if-eqz v2, :cond_3

    .line 816
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->showNext()V

    .line 820
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getFirstArtistUrl()Ljava/lang/String;

    move-result-object v0

    .line 821
    .local v0, "firstUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    invoke-virtual {v2, v0}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->requestBitmap(Ljava/lang/String;)V

    .line 824
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getNextArtistUrl()Ljava/lang/String;

    move-result-object v1

    .line 825
    .local v1, "secondUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper2:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    invoke-virtual {v2, v1}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->requestBitmap(Ljava/lang/String;)V

    .line 828
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050007

    invoke-virtual {v2, v3, v4}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 829
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050008

    invoke-virtual {v2, v3, v4}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 830
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 831
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getOutAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 834
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z

    .line 835
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 836
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 837
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private updateActionButtonsVisibility()V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v0, :cond_2

    .line 540
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPlayRadio:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->showButtonIfXLarge(Landroid/view/View;)V

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->showButtonIfXLarge(Landroid/view/View;)V

    .line 564
    :cond_1
    :goto_0
    return-void

    .line 548
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v0, :cond_3

    .line 549
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->showButtonIfXLarge(Landroid/view/View;)V

    goto :goto_0

    .line 550
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/SharedSongList;

    if-nez v0, :cond_1

    .line 552
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-eqz v0, :cond_4

    .line 553
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->showButtonIfXLarge(Landroid/view/View;)V

    goto :goto_0

    .line 554
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/AlbumSongList;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/ArtistSongList;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/NautilusSongList;

    if-eqz v0, :cond_7

    .line 556
    :cond_5
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 558
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPlayRadio:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->showButtonIfXLarge(Landroid/view/View;)V

    .line 560
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->showButtonIfXLarge(Landroid/view/View;)V

    goto :goto_0

    .line 561
    :cond_7
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/GenreSongList;

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->showButtonIfXLarge(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public getAlbumArtHeight()I
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getHeight()I

    move-result v0

    return v0
.end method

.method public isArtistArtShown()Z
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 612
    .local v0, "context":Landroid/content/Context;
    iget-object v4, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    if-ne p1, v4, :cond_1

    .line 613
    iget-object v4, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 615
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mBuyButton:Landroid/view/View;

    if-ne p1, v4, :cond_2

    .line 616
    iget-object v4, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v4}, Lcom/google/android/music/medialist/SongList;->getStoreUrl()Ljava/lang/String;

    move-result-object v3

    .line 617
    .local v3, "url":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 618
    invoke-static {v0, v3}, Lcom/google/android/music/download/IntentConstants;->getStoreBuyIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 619
    .local v2, "shopIntent":Landroid/content/Intent;
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 621
    .end local v2    # "shopIntent":Landroid/content/Intent;
    .end local v3    # "url":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mOverflow:Landroid/view/View;

    if-ne p1, v4, :cond_0

    .line 622
    iget-object v4, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    if-eqz v4, :cond_3

    .line 623
    new-instance v1, Lcom/google/android/music/ui/ScreenMenuHandler;

    iget-object v4, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;

    iget-object v5, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    sget-object v6, Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;->TRACK_CONTAINER:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    invoke-direct {v1, v4, v5, v6}, Lcom/google/android/music/ui/ScreenMenuHandler;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;)V

    .line 625
    .local v1, "menuHandler":Lcom/google/android/music/ui/ScreenMenuHandler;
    invoke-virtual {v1, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->showPopupMenu(Landroid/view/View;)V

    goto :goto_0

    .line 628
    .end local v1    # "menuHandler":Lcom/google/android/music/ui/ScreenMenuHandler;
    :cond_3
    new-instance v4, Lcom/google/android/music/ui/ContainerHeaderView$3;

    invoke-direct {v4, p0, v0, p1}, Lcom/google/android/music/ui/ContainerHeaderView$3;-><init>(Lcom/google/android/music/ui/ContainerHeaderView;Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 170
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 173
    const v0, 0x7f0e00e8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;

    .line 174
    const v0, 0x7f0e00e9

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt2:Landroid/widget/ImageView;

    .line 175
    const v0, 0x7f0e00d1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 176
    const v0, 0x7f0e00ea

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mTopShadow:Landroid/widget/ImageView;

    .line 178
    const v0, 0x7f0e00e7

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    .line 181
    const v0, 0x7f0e00f0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseActionButton;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSharePlaylist:Lcom/google/android/music/ui/BaseActionButton;

    .line 182
    const v0, 0x7f0e00d9

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseActionButton;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPlayRadio:Lcom/google/android/music/ui/BaseActionButton;

    .line 183
    const v0, 0x7f0e00f1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseActionButton;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    .line 185
    const v0, 0x7f0e00f2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mBuyButton:Landroid/view/View;

    .line 186
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mBuyButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    const v0, 0x7f0e00f3

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mOverflow:Landroid/view/View;

    .line 188
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mOverflow:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    const v0, 0x7f0e00ef

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/KeepOnView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    .line 192
    const v0, 0x7f0e00ed

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mActionsContainer:Landroid/view/ViewGroup;

    .line 193
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/ui/BaseActionButton;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPlayRadio:Lcom/google/android/music/ui/BaseActionButton;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSharePlaylist:Lcom/google/android/music/ui/BaseActionButton;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mActionButtons:[Lcom/google/android/music/ui/BaseActionButton;

    .line 198
    const v0, 0x7f0e00b8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/MarqueeTextView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mTitle:Lcom/google/android/music/widgets/MarqueeTextView;

    .line 199
    const v0, 0x7f0e00f5

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSubtitle:Landroid/widget/TextView;

    .line 200
    const v0, 0x7f0e00f8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    .line 201
    const v0, 0x7f0e00f6

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYear:Landroid/widget/TextView;

    .line 202
    const v0, 0x7f0e00f7

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumYearHyphen:Landroid/widget/TextView;

    .line 204
    const v0, 0x7f0e00f4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mOwnerPhoto:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 205
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 657
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 658
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 659
    .local v1, "v":Landroid/view/View;
    if-ne v1, p0, :cond_0

    .line 660
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    neg-int v2, v2

    div-int/lit8 v0, v2, 0x2

    .line 661
    .local v0, "offset":I
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ViewFlipper;->setTranslationY(F)V

    .line 664
    .end local v0    # "offset":I
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 669
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 212
    iget-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtClearedOnStop:Z

    if-eqz v0, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->shouldDoArtistSlideShow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->startArtistArtSlideShow()V

    .line 225
    :cond_0
    :goto_0
    iput-boolean v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtClearedOnStop:Z

    .line 226
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_2

    .line 219
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1ShowingNonDefaultArt:Z

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->clearArtistArt(Landroid/widget/ImageView;)V

    .line 236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1ShowingNonDefaultArt:Z

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt2:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView;->clearArtistArt(Landroid/widget/ImageView;)V

    .line 240
    iget-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowInitialized:Z

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 245
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtClearedOnStop:Z

    .line 246
    return-void
.end method

.method public setContainerDocument(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 0
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 261
    return-void
.end method

.method public setCursor(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 457
    if-nez p1, :cond_0

    .line 502
    :goto_0
    return-void

    .line 462
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    .line 463
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->shouldShowSongCount()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->hasCount(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 464
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 465
    .local v2, "songCount":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v5, 0x7f120000

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v3, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 467
    .local v0, "displayCount":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v3}, Lcom/google/android/music/medialist/SongList;->getShouldFilter()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->isDownloadedOnlyMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 468
    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b0156

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0155

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 473
    .local v1, "displayText":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 487
    .end local v0    # "displayCount":Ljava/lang/CharSequence;
    .end local v1    # "displayText":Ljava/lang/CharSequence;
    .end local v2    # "songCount":I
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v3}, Lcom/google/android/music/medialist/SongList;->shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    if-nez v3, :cond_2

    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->hasCount(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 489
    const/16 v3, 0xa

    invoke-direct {p0, p1, v3}, Lcom/google/android/music/ui/ContainerHeaderView;->extractArtistArtUrls(Landroid/database/Cursor;I)V

    .line 491
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->shouldDoArtistSlideShow()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 492
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ContainerHeaderView;->setupArtistArtSlideShow(Landroid/database/Cursor;)V

    .line 501
    :cond_2
    :goto_2
    iget-object v5, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mTopShadow:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->isDownloadedOnlyMode()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x8

    :goto_3
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 475
    .restart local v0    # "displayCount":Ljava/lang/CharSequence;
    .restart local v2    # "songCount":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 478
    .end local v0    # "displayCount":Ljava/lang/CharSequence;
    .end local v2    # "songCount":I
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 493
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 494
    iget-object v5, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->requestBitmap(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move v3, v4

    .line 501
    goto :goto_3
.end method

.method public setFragment(Lcom/google/android/music/ui/BaseListFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/android/music/ui/BaseListFragment;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;

    .line 253
    return-void
.end method

.method public setMusicPreferences(Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 0
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 509
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 510
    return-void
.end method

.method public setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 4
    .param p1, "list"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    const/16 v3, 0x8

    .line 267
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 271
    invoke-virtual {p1}, Lcom/google/android/music/medialist/SongList;->hasArtistArt()Z

    move-result v1

    if-nez v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, v3}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 275
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->updateActionButtonsVisibility()V

    .line 277
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSharePlaylist:Lcom/google/android/music/ui/BaseActionButton;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/BaseActionButton;->setMediaList(Lcom/google/android/music/medialist/MediaList;)V

    .line 278
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPlayRadio:Lcom/google/android/music/ui/BaseActionButton;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/BaseActionButton;->setMediaList(Lcom/google/android/music/medialist/MediaList;)V

    .line 279
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mShuffle:Lcom/google/android/music/ui/BaseActionButton;

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/BaseActionButton;->setMediaList(Lcom/google/android/music/medialist/MediaList;)V

    .line 281
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mAlbumArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    new-instance v2, Lcom/google/android/music/ui/ContainerHeaderView$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/ui/ContainerHeaderView$1;-><init>(Lcom/google/android/music/ui/ContainerHeaderView;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    if-eqz v1, :cond_1

    .line 289
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->supportsOfflineCaching(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 290
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->enablePinning()V

    .line 296
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/SharedSongList;

    if-eqz v1, :cond_2

    .line 297
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mOverflow:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 298
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView;->mBuyButton:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 302
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 303
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/android/music/ui/ContainerHeaderView$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/ui/ContainerHeaderView$2;-><init>(Lcom/google/android/music/ui/ContainerHeaderView;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 450
    return-void

    .line 292
    .end local v0    # "context":Landroid/content/Context;
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerHeaderView;->disablePinning()V

    goto :goto_0
.end method
