.class final Lcom/google/android/music/leanback/LeanbackPlayActivity$11;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;->setBackground(Landroid/content/Context;JLjava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private mArtUri:Ljava/lang/String;

.field final synthetic val$artistId:J

.field final synthetic val$artistMetajamId:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$itemGetterListener:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;J)V
    .locals 0

    .prologue
    .line 974
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$artistMetajamId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$itemGetterListener:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;

    iput-wide p4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$artistId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 979
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$artistMetajamId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->getNautilusArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->mArtUri:Ljava/lang/String;

    .line 981
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->mArtUri:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils;->stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->mArtUri:Ljava/lang/String;

    .line 982
    return-void
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 986
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$itemGetterListener:Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;

    new-instance v1, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v1}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->mArtUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$artistMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;->val$artistId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/leanback/Item$Builder;->secondaryId(J)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/Item$Builder;->darkenBackground(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;->onItemReceived(Lcom/google/android/music/leanback/Item;)V

    .line 989
    return-void
.end method
