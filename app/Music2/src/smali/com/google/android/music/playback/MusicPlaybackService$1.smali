.class Lcom/google/android/music/playback/MusicPlaybackService$1;
.super Landroid/content/BroadcastReceiver;
.source "MusicPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 162
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "action":Ljava/lang/String;
    const-string v3, "command"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "cmd":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIntentReceiver.onReceive "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 166
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->shouldIgnoreCommand(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v3, p2, v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$000(Lcom/google/android/music/playback/MusicPlaybackService;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    const-string v3, "next"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "com.android.music.musicservicecommand.next"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 170
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->next()V

    goto :goto_0

    .line 171
    :cond_3
    const-string v3, "previous"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "com.android.music.musicservicecommand.previous"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 172
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->prev()V

    goto :goto_0

    .line 173
    :cond_5
    const-string v3, "togglepause"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "com.android.music.musicservicecommand.togglepause"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 174
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 175
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    goto :goto_0

    .line 177
    :cond_7
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    goto :goto_0

    .line 179
    :cond_8
    const-string v3, "pause"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "com.android.music.musicservicecommand.pause"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 180
    :cond_9
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    goto :goto_0

    .line 181
    :cond_a
    const-string v3, "play"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 182
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    goto :goto_0

    .line 183
    :cond_b
    const-string v3, "stop"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 184
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    .line 185
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/playback/MusicPlaybackService;->seek(J)J

    goto/16 :goto_0

    .line 186
    :cond_c
    const-string v3, "musicwidgetupdate"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 188
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$100()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 189
    const-string v3, "MusicPlaybackService"

    const-string v4, "onReceive() with musicwidgetupdate"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_d
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.music.metachanged"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    .local v2, "musicState":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-static {v3, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtrasFromSharedPreferences(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    .line 194
    const-string v3, "playing"

    iget-object v4, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 195
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const-string v4, "com.android.music.metachanged"

    invoke-static {v3, v2, v4}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 197
    .end local v2    # "musicState":Landroid/content/Intent;
    :cond_e
    const-string v3, "com.android.music.playstatusrequest"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 198
    iget-object v4, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const-string v5, "com.android.music.playstatusresponse"

    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$1;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v4, v5, v3}, Lcom/google/android/music/playback/MusicPlaybackService;->notifyChange(Ljava/lang/String;Lcom/google/android/music/playback/DevicePlayback;)V

    goto/16 :goto_0
.end method
