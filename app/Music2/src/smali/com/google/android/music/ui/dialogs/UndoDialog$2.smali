.class Lcom/google/android/music/ui/dialogs/UndoDialog$2;
.super Ljava/lang/Object;
.source "UndoDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/dialogs/UndoDialog;-><init>(Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/dialogs/UndoDialog;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$2;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$2;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$200(Lcom/google/android/music/ui/dialogs/UndoDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$2;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mRemoveHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$000(Lcom/google/android/music/ui/dialogs/UndoDialog;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$2;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mRemoveHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$000(Lcom/google/android/music/ui/dialogs/UndoDialog;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$2;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    check-cast p1, Landroid/app/Dialog;

    .end local p1    # "dialog":Landroid/content/DialogInterface;
    # invokes: Lcom/google/android/music/ui/dialogs/UndoDialog;->getRemoveRunnable(Landroid/app/Dialog;)Ljava/lang/Runnable;
    invoke-static {v1, p1}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$300(Lcom/google/android/music/ui/dialogs/UndoDialog;Landroid/app/Dialog;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 85
    :cond_0
    return-void
.end method
