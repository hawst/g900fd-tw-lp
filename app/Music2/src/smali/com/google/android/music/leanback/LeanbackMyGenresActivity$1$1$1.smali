.class Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;
.super Ljava/lang/Object;
.source "LeanbackMyGenresActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/Item$StringGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;->bind(Landroid/database/Cursor;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mArtUri:Ljava/lang/String;

.field final synthetic this$2:Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;

.field final synthetic val$albumId:J

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;JLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->this$2:Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;

    iput-wide p2, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->val$albumId:J

    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->mArtUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->mArtUri:Ljava/lang/String;

    .line 66
    :goto_0
    return-object v0

    .line 61
    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->val$albumId:J

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->val$context:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->mArtUri:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;->mArtUri:Ljava/lang/String;

    goto :goto_0
.end method
