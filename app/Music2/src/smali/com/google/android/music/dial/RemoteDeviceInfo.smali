.class public Lcom/google/android/music/dial/RemoteDeviceInfo;
.super Ljava/lang/Object;
.source "RemoteDeviceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/dial/RemoteDeviceInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mApplicationUrl:Landroid/net/Uri;

.field private final mDeviceId:Ljava/lang/String;

.field private final mDeviceType:Ljava/lang/String;

.field private final mFriendlyName:Ljava/lang/String;

.field private final mGroupId:Ljava/lang/String;

.field private final mGroupName:Ljava/lang/String;

.field private final mHouseholdId:Ljava/lang/String;

.field private final mIsGroupCoordinator:Z

.field private final mManufacturer:Ljava/lang/String;

.field private final mManufacturerUrl:Landroid/net/Uri;

.field private final mModelDescription:Ljava/lang/String;

.field private final mModelName:Ljava/lang/String;

.field private final mModelNumber:Ljava/lang/String;

.field private final mRemoteServices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/dial/RemoteService;",
            ">;"
        }
    .end annotation
.end field

.field private final mSerialNumber:Ljava/lang/String;

.field private final mUniqueDeviceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 205
    new-instance v0, Lcom/google/android/music/dial/RemoteDeviceInfo$1;

    invoke-direct {v0}, Lcom/google/android/music/dial/RemoteDeviceInfo$1;-><init>()V

    sput-object v0, Lcom/google/android/music/dial/RemoteDeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$100(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceId:Ljava/lang/String;

    .line 163
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mUniqueDeviceName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$200(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mUniqueDeviceName:Ljava/lang/String;

    .line 164
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mDeviceType:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$300(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceType:Ljava/lang/String;

    .line 165
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mFriendlyName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$400(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mFriendlyName:Ljava/lang/String;

    .line 166
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturer:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$500(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mManufacturer:Ljava/lang/String;

    .line 167
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mManufacturerUrl:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$600(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mManufacturerUrl:Landroid/net/Uri;

    .line 168
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelDescription:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$700(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelDescription:Ljava/lang/String;

    .line 169
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$800(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelName:Ljava/lang/String;

    .line 170
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mModelNumber:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$900(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelNumber:Ljava/lang/String;

    .line 171
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mSerialNumber:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$1000(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mSerialNumber:Ljava/lang/String;

    .line 172
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mApplicationUrl:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$1100(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mApplicationUrl:Landroid/net/Uri;

    .line 173
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mRemoteServices:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$1200(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mRemoteServices:Ljava/util/List;

    .line 174
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$1300(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mGroupId:Ljava/lang/String;

    .line 175
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mGroupName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$1400(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mGroupName:Ljava/lang/String;

    .line 176
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mIsGroupCoordinator:Z
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$1500(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mIsGroupCoordinator:Z

    .line 177
    # getter for: Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->mHouseholdId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->access$1600(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mHouseholdId:Ljava/lang/String;

    .line 178
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;Lcom/google/android/music/dial/RemoteDeviceInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .param p2, "x1"    # Lcom/google/android/music/dial/RemoteDeviceInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/RemoteDeviceInfo;-><init>(Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 318
    if-ne p1, p0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v1

    .line 321
    :cond_1
    instance-of v3, p1, Lcom/google/android/music/dial/RemoteDeviceInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 322
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 324
    check-cast v0, Lcom/google/android/music/dial/RemoteDeviceInfo;

    .line 325
    .local v0, "RemoteDevice":Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-virtual {p0}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 326
    invoke-virtual {v0}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 329
    goto :goto_0

    .line 331
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getApplicationUrl()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mApplicationUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mFriendlyName:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mGroupId:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mGroupName:Ljava/lang/String;

    return-object v0
.end method

.method public getHouseholdId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mHouseholdId:Ljava/lang/String;

    return-object v0
.end method

.method public getIsGroupCoordinator()Z
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mIsGroupCoordinator:Z

    return v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mManufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getManufacturerUrl()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mManufacturerUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public getModelDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelName:Ljava/lang/String;

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteServices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/dial/RemoteService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mRemoteServices:Ljava/util/List;

    return-object v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mUniqueDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 340
    const/4 v0, 0x0

    .line 342
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mUniqueDeviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mDeviceType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mManufacturer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mManufacturerUrl:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mModelNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mSerialNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mApplicationUrl:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mRemoteServices:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mGroupId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mGroupName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 201
    iget-boolean v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mIsGroupCoordinator:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 202
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteDeviceInfo;->mHouseholdId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 203
    return-void

    .line 201
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
