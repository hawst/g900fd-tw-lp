.class public Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;
.super Ljava/lang/Object;
.source "DocumentClickHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static mContext:Landroid/content/Context;


# instance fields
.field private mDoc:Lcom/google/android/music/ui/cardlib/model/Document;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    sput-object p1, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->mContext:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 46
    return-void
.end method

.method public static onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getIsEmulateRadio()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 55
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v5, v6, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v5, v6, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_2

    .line 58
    :cond_0
    new-instance v5, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$1;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 206
    :cond_1
    :goto_0
    return-void

    .line 65
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v5, v6, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_4

    .line 67
    :cond_3
    new-instance v5, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 76
    :cond_4
    new-instance v5, Ljava/security/InvalidParameterException;

    const-string v6, "New radio stations can only be created from artist, albums or tracks"

    invoke-direct {v5, v6}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 81
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_7

    .line 82
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 83
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v5, v1, v2}, Lcom/google/android/music/ui/TrackContainerActivity;->showNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 198
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isOnMainstage()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 199
    new-instance v5, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$3;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 85
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v6

    const/4 v9, 0x1

    move-object/from16 v5, p0

    move-object/from16 v8, p1

    move-object/from16 v10, p2

    invoke-static/range {v5 .. v10}, Lcom/google/android/music/ui/TrackContainerActivity;->showAlbum(Landroid/content/Context;JLcom/google/android/music/ui/cardlib/model/Document;ZLandroid/view/View;)V

    goto :goto_1

    .line 87
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_9

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/google/android/music/ui/ArtistPageActivity;->showNautilusArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 92
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7, v5, v8}, Lcom/google/android/music/ui/ArtistPageActivity;->showArtist(Landroid/content/Context;JLjava/lang/String;Z)V

    goto :goto_1

    .line 95
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_e

    .line 96
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    const/16 v6, 0x32

    if-eq v5, v6, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    const/16 v6, 0x3c

    if-ne v5, v6, :cond_b

    .line 99
    :cond_a
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v16

    .line 100
    .local v16, "list":Lcom/google/android/music/medialist/SongList;
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 103
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/AppNavigation;->openNowPlayingDrawer(Landroid/content/Context;)V

    goto :goto_1

    .line 104
    .end local v16    # "list":Lcom/google/android/music/medialist/SongList;
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    const/16 v6, 0x50

    if-ne v5, v6, :cond_d

    .line 109
    new-instance v12, Landroid/content/Intent;

    const-string v5, "com.google.android.googlequicksearchbox.MUSIC_SEARCH"

    invoke-direct {v12, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    .local v12, "i":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v12, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v14

    .line 111
    .local v14, "info":Landroid/content/pm/ResolveInfo;
    if-nez v14, :cond_c

    .line 112
    invoke-static/range {p0 .. p1}, Lcom/google/android/music/ui/TrackContainerActivity;->showPlaylist(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto/16 :goto_1

    .line 114
    :cond_c
    invoke-static/range {p0 .. p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->openSoundSearch(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto/16 :goto_1

    .line 118
    .end local v12    # "i":Landroid/content/Intent;
    .end local v14    # "info":Landroid/content/pm/ResolveInfo;
    :cond_d
    invoke-static/range {p0 .. p1}, Lcom/google/android/music/ui/TrackContainerActivity;->showPlaylist(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto/16 :goto_1

    .line 121
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_14

    .line 122
    const/4 v11, 0x0

    .line 123
    .local v11, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isFromSearch()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 124
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 125
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSearchString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    .line 132
    :cond_f
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 133
    if-nez v11, :cond_10

    .line 134
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/store/ContainerDescriptor;->newNautilusSingleSongDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    .line 139
    :cond_10
    new-instance v5, Lcom/google/android/music/medialist/NautilusSingleSongList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v11, v6, v7}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, -0x1

    invoke-static {v5, v6}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    goto/16 :goto_1

    .line 128
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSearchString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    goto :goto_2

    .line 143
    :cond_12
    if-nez v11, :cond_13

    .line 144
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    .line 147
    :cond_13
    new-instance v5, Lcom/google/android/music/medialist/SingleSongList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v11, v6, v7, v8}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    const/4 v6, -0x1

    invoke-static {v5, v6}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    goto/16 :goto_1

    .line 151
    .end local v11    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_15

    .line 152
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7, v5}, Lcom/google/android/music/ui/GenreAlbumGridActivity;->showAlbumsOfGenre(Landroid/content/Context;JZ)V

    goto/16 :goto_1

    .line 153
    :cond_15
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_17

    .line 154
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioRemoteId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_16

    .line 155
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7, v5}, Lcom/google/android/music/utils/MusicUtils;->playRadio(Landroid/content/Context;JLjava/lang/String;)V

    goto/16 :goto_1

    .line 157
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedType()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v7, v8}, Lcom/google/android/music/utils/MusicUtils;->playRecommendedRadio(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 161
    :cond_17
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_19

    .line 163
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isTopLevelGenre()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 164
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getGenreId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSubgenreCount()I

    move-result v7

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v7, v8}, Lcom/google/android/music/ui/GenresExploreActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v12

    .line 170
    .restart local v12    # "i":Landroid/content/Intent;
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 167
    .end local v12    # "i":Landroid/content/Intent;
    :cond_18
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getGenreId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getParentGenreId()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v7}, Lcom/google/android/music/ui/SubGenresExploreActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v12

    .restart local v12    # "i":Landroid/content/Intent;
    goto :goto_3

    .line 171
    .end local v12    # "i":Landroid/content/Intent;
    :cond_19
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->IM_FEELING_LUCKY:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_1b

    .line 172
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/MusicUtils;->canStartImFeelingLucky(Landroid/content/Context;)Z

    move-result v13

    .line 173
    .local v13, "iflRadioEnabled":Z
    if-eqz v13, :cond_1a

    .line 174
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/MusicUtils;->playImFeelingLuckyRadio(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 176
    :cond_1a
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->shuffleOnDevice()V

    goto/16 :goto_1

    .line 178
    .end local v13    # "iflRadioEnabled":Z
    :cond_1b
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v5, v6, :cond_1c

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_1d

    .line 180
    :cond_1c
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v18

    .line 181
    .local v18, "songlist":Lcom/google/android/music/medialist/SongList;
    if-eqz v18, :cond_1

    .line 184
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackContainerActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v15

    .line 185
    .local v15, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 186
    .end local v15    # "intent":Landroid/content/Intent;
    .end local v18    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_1d
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_1e

    .line 187
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getVideoId()Ljava/lang/String;

    move-result-object v19

    .line 188
    .local v19, "videoId":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v17

    .line 189
    .local v17, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v4

    .line 190
    .local v4, "account":Landroid/accounts/Account;
    if-eqz v4, :cond_1

    move-object/from16 v5, p0

    .line 193
    check-cast v5, Landroid/app/Activity;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, v19

    invoke-static {v5, v0, v6, v7}, Lcom/google/android/music/youtube/YouTubeUtils;->startVideo(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 195
    .end local v4    # "account":Landroid/accounts/Account;
    .end local v17    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v19    # "videoId":Ljava/lang/String;
    :cond_1e
    new-instance v5, Ljava/security/InvalidParameterException;

    invoke-direct {v5}, Ljava/security/InvalidParameterException;-><init>()V

    throw v5
.end method

.method private static openSoundSearch(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 209
    invoke-virtual {p1, p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 211
    .local v0, "songList":Lcom/google/android/music/medialist/PlaylistSongList;
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;

    invoke-direct {v1, v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;-><init>(Lcom/google/android/music/medialist/PlaylistSongList;Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 230
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v0, v1, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 51
    return-void
.end method
