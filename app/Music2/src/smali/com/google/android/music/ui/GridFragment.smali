.class public Lcom/google/android/music/ui/GridFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "GridFragment.java"


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mEmptyImageView:Landroid/widget/ImageView;

.field private mEmptyScreen:Landroid/view/View;

.field private mEmptyTextView:Landroid/widget/TextView;

.field private mGrid:Landroid/widget/ListView;

.field private mGridContainer:Landroid/widget/FrameLayout;

.field private mGridShown:Z

.field private final mHandler:Landroid/os/Handler;

.field private mIsCardHeaderShowing:Z

.field private mLearnMore:Landroid/widget/TextView;

.field private mProgressContainer:Landroid/view/View;

.field private final mRequestFocus:Ljava/lang/Runnable;

.field private mShouldShowEmptyScreen:Z

.field private final mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 32
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mHandler:Landroid/os/Handler;

    .line 34
    new-instance v0, Lcom/google/android/music/ui/GridFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/GridFragment$1;-><init>(Lcom/google/android/music/ui/GridFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mRequestFocus:Ljava/lang/Runnable;

    .line 50
    iput-boolean v1, p0, Lcom/google/android/music/ui/GridFragment;->mShouldShowEmptyScreen:Z

    .line 51
    iput-boolean v1, p0, Lcom/google/android/music/ui/GridFragment;->mIsCardHeaderShowing:Z

    .line 53
    new-instance v0, Lcom/google/android/music/ui/GridFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/GridFragment$2;-><init>(Lcom/google/android/music/ui/GridFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/GridFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GridFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    return-object v0
.end method

.method private ensureGrid()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 338
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    if-eqz v3, :cond_0

    .line 378
    :goto_0
    return-void

    .line 341
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getView()Landroid/view/View;

    move-result-object v2

    .line 342
    .local v2, "root":Landroid/view/View;
    if-nez v2, :cond_1

    .line 343
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Content view not yet created"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 345
    :cond_1
    instance-of v3, v2, Landroid/widget/ListView;

    if-eqz v3, :cond_4

    .line 346
    check-cast v2, Landroid/widget/ListView;

    .end local v2    # "root":Landroid/view/View;
    iput-object v2, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    .line 363
    :cond_2
    iput-boolean v5, p0, Lcom/google/android/music/ui/GridFragment;->mGridShown:Z

    .line 364
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 366
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_6

    .line 367
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 368
    .local v0, "adapter":Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 369
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GridFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 377
    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/music/ui/GridFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 348
    .restart local v2    # "root":Landroid/view/View;
    :cond_4
    const v3, 0x7f0e011d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    .line 349
    const v3, 0x7f0e011c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    .line 350
    const v3, 0x102000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 351
    .local v1, "rawGridView":Landroid/view/View;
    instance-of v3, v1, Landroid/widget/ListView;

    if-nez v3, :cond_5

    .line 352
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 356
    :cond_5
    check-cast v1, Landroid/widget/ListView;

    .end local v1    # "rawGridView":Landroid/view/View;
    iput-object v1, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    .line 357
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    if-nez v3, :cond_2

    .line 358
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 373
    .end local v2    # "root":Landroid/view/View;
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    if-eqz v3, :cond_3

    .line 374
    invoke-direct {p0, v4, v4}, Lcom/google/android/music/ui/GridFragment;->setGridShown(ZZ)V

    goto :goto_1
.end method

.method private refreshEmptyScreen()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 215
    iget-object v1, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/music/ui/GridFragment;->mShouldShowEmptyScreen:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/ui/GridFragment;->mIsCardHeaderShowing:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 218
    :cond_0
    return-void

    .line 215
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setGridShown(ZZ)V
    .locals 6
    .param p1, "shown"    # Z
    .param p2, "animate"    # Z

    .prologue
    const v5, 0x10a0001

    const/high16 v4, 0x10a0000

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 295
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;->ensureGrid()V

    .line 296
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    if-nez v0, :cond_0

    .line 297
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridShown:Z

    if-ne v0, p1, :cond_1

    .line 328
    :goto_0
    return-void

    .line 302
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/music/ui/GridFragment;->mGridShown:Z

    .line 303
    if-eqz p1, :cond_3

    .line 304
    if-eqz p2, :cond_2

    .line 305
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 313
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 311
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    goto :goto_1

    .line 316
    :cond_3
    if-eqz p2, :cond_4

    .line 317
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 319
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 325
    :goto_2
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 322
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 323
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    goto :goto_2
.end method


# virtual methods
.method public getListAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;->ensureGrid()V

    .line 178
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    return-object v0
.end method

.method protected getScreenColumns()I
    .locals 2

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->getScreenColumnCount(Landroid/content/res/Resources;Lcom/google/android/music/preferences/MusicPreferences;)I

    move-result v0

    return v0
.end method

.method protected initEmptyScreen()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 221
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    if-nez v0, :cond_0

    .line 222
    const v0, 0x7f040031

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GridFragment;->setEmptyScreen(I)V

    .line 224
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    const v1, 0x7f0e0112

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyImageView:Landroid/widget/ImageView;

    .line 225
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    const v1, 0x7f0e0114

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyTextView:Landroid/widget/TextView;

    .line 226
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    const v1, 0x7f0e0115

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mLearnMore:Landroid/widget/TextView;

    .line 227
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mLearnMore:Landroid/widget/TextView;

    const v1, 0x7f0b02a3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 228
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyTextView:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mLearnMore:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mLearnMore:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/music/ui/GridFragment$3;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/GridFragment$3;-><init>(Lcom/google/android/music/ui/GridFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->initEmptyScreen()V

    .line 113
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    const v1, 0x7f040035

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 96
    .local v0, "result":Landroid/view/View;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/GridFragment;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/UIStateManager;->registerUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 97
    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/GridFragment;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->unregisterUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/ui/GridFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 122
    iput-object v2, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridShown:Z

    .line 124
    iput-object v2, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/google/android/music/ui/GridFragment;->mProgressContainer:Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    .line 125
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 126
    return-void
.end method

.method protected onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 0
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 255
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStart()V

    .line 71
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStop()V

    .line 76
    return-void
.end method

.method public onTutorialCardClosed()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GridFragment;->setIsCardHeaderShowing(Z)V

    .line 66
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 106
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;->ensureGrid()V

    .line 107
    return-void
.end method

.method protected setEmptyImageView(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 248
    return-void
.end method

.method protected setEmptyScreen(I)V
    .locals 2
    .param p1, "resourceId"    # I

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;->ensureGrid()V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    .line 189
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mGridContainer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 190
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GridFragment;->setEmptyScreenVisible(Z)V

    .line 191
    return-void
.end method

.method protected setEmptyScreenLearnMoreVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 251
    iget-object v1, p0, Lcom/google/android/music/ui/GridFragment;->mLearnMore:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    return-void

    .line 251
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setEmptyScreenText(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 244
    return-void
.end method

.method protected setEmptyScreenVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/music/ui/GridFragment;->mEmptyScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;->ensureGrid()V

    .line 208
    iput-boolean p1, p0, Lcom/google/android/music/ui/GridFragment;->mShouldShowEmptyScreen:Z

    .line 209
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;->refreshEmptyScreen()V

    .line 211
    :cond_0
    return-void
.end method

.method protected setIsCardHeaderShowing(Z)V
    .locals 0
    .param p1, "showing"    # Z

    .prologue
    .line 387
    iput-boolean p1, p0, Lcom/google/android/music/ui/GridFragment;->mIsCardHeaderShowing:Z

    .line 388
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;->refreshEmptyScreen()V

    .line 389
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 5
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 132
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_2

    move v0, v1

    .line 134
    .local v0, "hadAdapter":Z
    :goto_0
    iput-object p1, p0, Lcom/google/android/music/ui/GridFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 136
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    if-eqz v3, :cond_1

    .line 137
    iget-object v3, p0, Lcom/google/android/music/ui/GridFragment;->mGrid:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/google/android/music/ui/GridFragment;->mAdapter:Landroid/widget/ListAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    iget-boolean v3, p0, Lcom/google/android/music/ui/GridFragment;->mGridShown:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/google/android/music/ui/GridFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/google/android/music/ui/GridFragment;->setGridShown(ZZ)V

    .line 144
    :cond_1
    return-void

    .end local v0    # "hadAdapter":Z
    :cond_2
    move v0, v2

    .line 132
    goto :goto_0
.end method
