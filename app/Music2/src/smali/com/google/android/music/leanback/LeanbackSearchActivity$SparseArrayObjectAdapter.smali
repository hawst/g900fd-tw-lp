.class public Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SparseArrayObjectAdapter"
.end annotation


# instance fields
.field private mItems:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 1
    .param p1, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 94
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V
    .locals 1
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 94
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    .line 98
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 130
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 131
    .local v0, "itemCount":I
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 132
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->notifyItemRangeRemoved(II)V

    .line 133
    return-void
.end method

.method public clear(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 105
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 106
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 107
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 108
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->notifyItemRangeRemoved(II)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->notifyChanged()V

    .line 113
    :cond_0
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public set(ILjava/lang/Object;)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 116
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 117
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 118
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, v0, p2}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 119
    invoke-virtual {p0, v0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->notifyItemRangeChanged(II)V

    .line 126
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->notifyChanged()V

    .line 127
    return-void

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 122
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 123
    invoke-virtual {p0, v0, v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->notifyItemRangeInserted(II)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method
