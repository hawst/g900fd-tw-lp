.class public Lcom/google/android/music/download/cache/StorageProbeUtils;
.super Ljava/lang/Object;
.source "StorageProbeUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/cache/StorageProbeUtils$1;,
        Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;,
        Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
    }
.end annotation


# static fields
.field private static final DEFAULT_KITKAT_PROBE_METHODS:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_PROBE_METHODS:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/download/cache/StorageProbeUtils;->LOGV:Z

    .line 82
    sget-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->KITKAT_API:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    sget-object v1, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils;->DEFAULT_KITKAT_PROBE_METHODS:Ljava/util/EnumSet;

    .line 91
    sget-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils;->DEFAULT_PROBE_METHODS:Ljava/util/EnumSet;

    return-void

    .line 33
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Landroid/content/Context;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/android/music/download/cache/StorageProbeUtils;->getLocationsWithMountService(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static findAllLocations(Landroid/content/Context;)Ljava/util/Collection;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 109
    .local v16, "results":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInternal(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v2

    .line 111
    .local v2, "internal":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v2, :cond_0

    .line 112
    new-instance v1, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;

    sget-object v3, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->CONTEXT_APIS:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    const/4 v4, 0x0

    const v5, 0x7f0b02b3

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;-><init>(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Ljava/io/File;Ljava/lang/String;ZZ)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v11

    .line 123
    .local v11, "external":Ljava/io/File;
    if-eqz v11, :cond_1

    .line 124
    new-instance v4, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v1, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {v4, v11, v1}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    .line 126
    .local v4, "externalLocation":Lcom/google/android/music/download/cache/CacheLocation;
    new-instance v3, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;

    sget-object v5, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->CONTEXT_APIS:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    const/4 v6, 0x0

    const v1, 0x7f0b02bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/google/android/music/utils/PostFroyoUtils$EnvironmentCompat;->isExternalStorageEmulated()Z

    move-result v8

    const/4 v9, 0x1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;-><init>(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Ljava/io/File;Ljava/lang/String;ZZ)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 131
    .end local v4    # "externalLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_1
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v15, Lcom/google/android/music/download/cache/StorageProbeUtils;->DEFAULT_KITKAT_PROBE_METHODS:Ljava/util/EnumSet;

    .line 134
    .local v15, "probeMethods":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;>;"
    :goto_0
    invoke-virtual {v15}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    .line 136
    .local v13, "method":Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->getMountPoints(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v14

    .line 137
    .local v14, "points":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    sget-boolean v1, Lcom/google/android/music/download/cache/StorageProbeUtils;->LOGV:Z

    if-eqz v1, :cond_2

    .line 138
    const-string v1, "StorageProbeUtils"

    const-string v3, "Method %s: %d locations found"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v13, v5, v6

    const/4 v6, 0x1

    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 142
    .end local v14    # "points":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    :catch_0
    move-exception v10

    .line 143
    .local v10, "e":Ljava/lang/RuntimeException;
    const-string v1, "StorageProbeUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error getting mount points using method: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v10}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 131
    .end local v10    # "e":Ljava/lang/RuntimeException;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "method":Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
    .end local v15    # "probeMethods":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;>;"
    :cond_3
    sget-object v15, Lcom/google/android/music/download/cache/StorageProbeUtils;->DEFAULT_PROBE_METHODS:Ljava/util/EnumSet;

    goto :goto_0

    .line 147
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v15    # "probeMethods":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;>;"
    :cond_4
    return-object v16
.end method

.method private static getCacheDirectoryForMountPoint(Ljava/io/File;Landroid/content/Context;)Ljava/io/File;
    .locals 4
    .param p0, "mountPoint"    # Ljava/io/File;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 394
    new-instance v1, Ljava/io/File;

    const-string v2, "Android/data/"

    invoke-direct {v1, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 395
    .local v1, "rootCacheDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 396
    .local v0, "ourFilesDir":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v3, "files"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v2
.end method

.method public static getLocationsWithKitKatApis(Landroid/content/Context;)Ljava/util/Set;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 343
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 346
    .local v14, "results":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v1

    if-nez v1, :cond_1

    .line 383
    :cond_0
    return-object v14

    .line 349
    :cond_1
    invoke-virtual {p0, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    .line 350
    .local v12, "maybeEmulatedPath":Ljava/io/File;
    invoke-static {}, Lcom/google/android/music/utils/PostFroyoUtils$EnvironmentCompat;->isExternalStorageEmulated()Z

    move-result v13

    .line 352
    .local v13, "oldApiExternalEmulated":Z
    invoke-virtual {p0, v3}, Landroid/content/Context;->getExternalFilesDirs(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v11

    .line 353
    .local v11, "locations":[Ljava/io/File;
    if-eqz v11, :cond_0

    .line 354
    move-object v7, v11

    .local v7, "arr$":[Ljava/io/File;
    array-length v10, v7

    .local v10, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v10, :cond_0

    aget-object v9, v7, v8

    .line 355
    .local v9, "l":Ljava/io/File;
    if-nez v9, :cond_2

    .line 354
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 356
    :cond_2
    const/4 v5, 0x0

    .line 357
    .local v5, "emulated":Z
    invoke-virtual {v9, v12}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v13, :cond_5

    .line 358
    sget-boolean v1, Lcom/google/android/music/download/cache/StorageProbeUtils;->LOGV:Z

    if-eqz v1, :cond_3

    .line 359
    const-string v1, "StorageProbeUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found emulated path: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_3
    const/4 v5, 0x1

    .line 370
    :cond_4
    :goto_2
    new-instance v0, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;

    new-instance v1, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v2, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {v1, v9, v2}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    sget-object v2, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->KITKAT_API:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    const/4 v6, 0x1

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;-><init>(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Ljava/io/File;Ljava/lang/String;ZZ)V

    .line 379
    .local v0, "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    invoke-virtual {v14, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 363
    .end local v0    # "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    :cond_5
    sget-boolean v1, Lcom/google/android/music/download/cache/StorageProbeUtils;->LOGV:Z

    if-eqz v1, :cond_4

    .line 364
    const-string v1, "StorageProbeUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found path: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static getLocationsWithMountService(Landroid/content/Context;)Ljava/util/Set;
    .locals 36
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    new-instance v28, Ljava/util/HashSet;

    invoke-direct/range {v28 .. v28}, Ljava/util/HashSet;-><init>()V

    .line 219
    .local v28, "mountPoints":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    new-instance v21, Ljava/lang/Object;

    invoke-direct/range {v21 .. v21}, Ljava/lang/Object;-><init>()V

    .line 220
    .local v21, "handle":Ljava/lang/Object;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v32

    .line 221
    .local v32, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/16 v25, 0x0

    .line 223
    .local v25, "isTablet":Z
    :try_start_0
    invoke-virtual/range {v32 .. v32}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v25

    .line 225
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 226
    const/16 v32, 0x0

    .line 228
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v25, :cond_1

    .line 229
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageProbeUtils;->LOGV:Z

    if-eqz v3, :cond_0

    .line 230
    const-string v3, "StorageProbeUtils"

    const-string v4, "Disabling mount service lookups for device"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    :cond_0
    :goto_0
    return-object v28

    .line 225
    :catchall_0
    move-exception v3

    invoke-static/range {v21 .. v21}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 226
    const/16 v32, 0x0

    throw v3

    .line 235
    :cond_1
    :try_start_1
    const-string v3, "android.os.storage.IMountService"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v30

    .line 236
    .local v30, "mountServiceClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "getVolumeList"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v27

    .line 237
    .local v27, "method":Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/google/android/music/download/cache/StorageProbeUtils;->getMountService()Ljava/lang/Object;

    move-result-object v29

    .line 238
    .local v29, "mountService":Ljava/lang/Object;
    const-string v3, "android.os.storage.StorageVolume"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v34

    .line 239
    .local v34, "storageVolumeClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "getPath"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    move-object/from16 v0, v34

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v20

    .line 240
    .local v20, "getPathMethod":Ljava/lang/reflect/Method;
    const-string v3, "isEmulated"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    move-object/from16 v0, v34

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v23

    .line 243
    .local v23, "isEmulatedMethod":Ljava/lang/reflect/Method;
    const/16 v19, 0x0

    .line 244
    .local v19, "getDescriptionMethod":Ljava/lang/reflect/Method;
    const/16 v24, 0x0

    .line 247
    .local v24, "isJBDescriptionMethod":Z
    :try_start_2
    const-string v3, "getDescription"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v4, v9

    move-object/from16 v0, v34

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v19

    .line 249
    const/16 v24, 0x1

    .line 253
    :goto_1
    if-nez v19, :cond_2

    .line 256
    :try_start_3
    const-string v3, "getDescription"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    move-object/from16 v0, v34

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v19

    .line 262
    :cond_2
    const/4 v3, 0x0

    :try_start_4
    new-array v3, v3, [Ljava/lang/Object;

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-static {v0, v1, v3}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v33

    .line 263
    .local v33, "res":Ljava/lang/Object;
    if-eqz v33, :cond_0

    .line 266
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 267
    invoke-static/range {v33 .. v33}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v26

    .line 268
    .local v26, "length":I
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_2
    move/from16 v0, v22

    move/from16 v1, v26

    if-ge v0, v1, :cond_0

    .line 269
    move-object/from16 v0, v33

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v16

    .line 270
    .local v16, "arrayElement":Ljava/lang/Object;
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-static {v0, v1, v3}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 272
    .local v31, "path":Ljava/lang/String;
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1, v3}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 273
    .local v7, "isEmulated":Z
    const-string v3, "mounted"

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/google/android/music/download/cache/StorageProbeUtils;->getVolumeState(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 275
    .local v8, "isMounted":Z
    if-eqz v24, :cond_6

    .line 276
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1, v3}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 281
    .local v6, "description":Ljava/lang/String;
    :goto_3
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageProbeUtils;->LOGV:Z

    if-eqz v3, :cond_3

    .line 282
    const-string v3, "StorageProbeUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "arrayElement="

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v3, "StorageProbeUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "state: "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/google/android/music/download/cache/StorageProbeUtils;->getVolumeState(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_3
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    .local v5, "mountPoint":Ljava/io/File;
    move-object/from16 v0, p0

    invoke-static {v5, v0}, Lcom/google/android/music/download/cache/StorageProbeUtils;->getCacheDirectoryForMountPoint(Ljava/io/File;Landroid/content/Context;)Ljava/io/File;

    move-result-object v17

    .line 289
    .local v17, "cachePath":Ljava/io/File;
    new-instance v2, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;

    new-instance v3, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v4, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-object/from16 v0, v17

    invoke-direct {v3, v0, v4}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    sget-object v4, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;-><init>(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Ljava/io/File;Ljava/lang/String;ZZ)V

    .line 298
    .local v2, "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    if-eqz v8, :cond_5

    .line 302
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_4
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/utils/IOUtils;->isWritableDirectory(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v35, 0x1

    .line 304
    .local v35, "success":Z
    :goto_4
    if-nez v35, :cond_5

    .line 307
    new-instance v2, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;

    .end local v2    # "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    new-instance v10, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v3, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-object/from16 v0, v17

    invoke-direct {v10, v0, v3}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    sget-object v11, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    const/4 v15, 0x0

    move-object v9, v2

    move-object v12, v5

    move-object v13, v6

    move v14, v7

    invoke-direct/range {v9 .. v15}, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;-><init>(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Ljava/io/File;Ljava/lang/String;ZZ)V

    .line 320
    .end local v35    # "success":Z
    .restart local v2    # "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    :cond_5
    move-object/from16 v0, v28

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 268
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_2

    .line 257
    .end local v2    # "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    .end local v5    # "mountPoint":Ljava/io/File;
    .end local v6    # "description":Ljava/lang/String;
    .end local v7    # "isEmulated":Z
    .end local v8    # "isMounted":Z
    .end local v16    # "arrayElement":Ljava/lang/Object;
    .end local v17    # "cachePath":Ljava/io/File;
    .end local v22    # "i":I
    .end local v26    # "length":I
    .end local v31    # "path":Ljava/lang/String;
    .end local v33    # "res":Ljava/lang/Object;
    :catch_0
    move-exception v18

    .line 259
    .local v18, "e":Ljava/lang/NoSuchMethodException;
    goto/16 :goto_0

    .line 279
    .end local v18    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v7    # "isEmulated":Z
    .restart local v8    # "isMounted":Z
    .restart local v16    # "arrayElement":Ljava/lang/Object;
    .restart local v22    # "i":I
    .restart local v26    # "length":I
    .restart local v31    # "path":Ljava/lang/String;
    .restart local v33    # "res":Ljava/lang/Object;
    :cond_6
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1, v3}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    .restart local v6    # "description":Ljava/lang/String;
    goto/16 :goto_3

    .line 302
    .restart local v2    # "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    .restart local v5    # "mountPoint":Ljava/io/File;
    .restart local v17    # "cachePath":Ljava/io/File;
    :cond_7
    const/16 v35, 0x0

    goto :goto_4

    .line 324
    .end local v2    # "foundLocation":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    .end local v5    # "mountPoint":Ljava/io/File;
    .end local v6    # "description":Ljava/lang/String;
    .end local v7    # "isEmulated":Z
    .end local v8    # "isMounted":Z
    .end local v16    # "arrayElement":Ljava/lang/Object;
    .end local v17    # "cachePath":Ljava/io/File;
    .end local v19    # "getDescriptionMethod":Ljava/lang/reflect/Method;
    .end local v20    # "getPathMethod":Ljava/lang/reflect/Method;
    .end local v22    # "i":I
    .end local v23    # "isEmulatedMethod":Ljava/lang/reflect/Method;
    .end local v24    # "isJBDescriptionMethod":Z
    .end local v26    # "length":I
    .end local v27    # "method":Ljava/lang/reflect/Method;
    .end local v29    # "mountService":Ljava/lang/Object;
    .end local v30    # "mountServiceClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v31    # "path":Ljava/lang/String;
    .end local v33    # "res":Ljava/lang/Object;
    .end local v34    # "storageVolumeClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_1
    move-exception v18

    .line 325
    .restart local v18    # "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "StorageProbeUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    invoke-virtual {v9}, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " failed to run because the mount "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "service does not contain required methods."

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    sget-boolean v3, Lcom/google/android/music/download/cache/StorageProbeUtils;->LOGV:Z

    if-eqz v3, :cond_0

    .line 328
    const-string v3, "StorageProbeUtils"

    const-string v4, "(NOT A CRASH) Exception: "

    move-object/from16 v0, v18

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 330
    .end local v18    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v18

    .line 331
    .local v18, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "StorageProbeUtils"

    const-string v4, "(NOT A CRASH) Exception: "

    move-object/from16 v0, v18

    invoke-static {v3, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 250
    .end local v18    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v19    # "getDescriptionMethod":Ljava/lang/reflect/Method;
    .restart local v20    # "getPathMethod":Ljava/lang/reflect/Method;
    .restart local v23    # "isEmulatedMethod":Ljava/lang/reflect/Method;
    .restart local v24    # "isJBDescriptionMethod":Z
    .restart local v27    # "method":Ljava/lang/reflect/Method;
    .restart local v29    # "mountService":Ljava/lang/Object;
    .restart local v30    # "mountServiceClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v34    # "storageVolumeClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_3
    move-exception v3

    goto/16 :goto_1
.end method

.method private static getMountService()Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 171
    :try_start_0
    const-string v6, "android.os.ServiceManager"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 172
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v6, "getService"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 173
    .local v3, "getServiceMethod":Ljava/lang/reflect/Method;
    const-string v6, "android.os.storage.IMountService$Stub"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 174
    const-string v6, "asInterface"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/os/IBinder;

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 179
    .local v0, "asInterfaceMethod":Ljava/lang/reflect/Method;
    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "mount"

    aput-object v7, v6, v10

    invoke-static {v3, v5, v6}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 180
    .local v4, "service":Ljava/lang/Object;
    if-eqz v4, :cond_0

    .line 181
    new-array v6, v11, [Ljava/lang/Object;

    aput-object v4, v6, v10

    invoke-static {v0, v5, v6}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 185
    .end local v0    # "asInterfaceMethod":Ljava/lang/reflect/Method;
    .end local v3    # "getServiceMethod":Ljava/lang/reflect/Method;
    .end local v4    # "service":Ljava/lang/Object;
    :goto_0
    return-object v5

    .line 175
    :catch_0
    move-exception v2

    .line 176
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "StorageProbeUtils"

    const-string v7, "Exception: "

    invoke-static {v6, v7, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 183
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "asInterfaceMethod":Ljava/lang/reflect/Method;
    .restart local v3    # "getServiceMethod":Ljava/lang/reflect/Method;
    .restart local v4    # "service":Ljava/lang/Object;
    :cond_0
    const-string v6, "StorageProbeUtils"

    const-string v7, "Failed to get mount service"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getVolumeState(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "mountService"    # Ljava/lang/Object;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 189
    const/4 v3, 0x0

    .line 190
    .local v3, "getVolumeStateMethod":Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 193
    :try_start_0
    const-class v4, Landroid/os/Environment;

    const-string v6, "getStorageState"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/io/File;

    aput-object v9, v7, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 194
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 195
    .local v2, "f":Ljava/io/File;
    const/4 v4, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v3, v4, v6}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    .end local v2    # "f":Ljava/io/File;
    :goto_0
    return-object v4

    .line 196
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/NoSuchMethodException;
    move-object v4, v5

    .line 197
    goto :goto_0

    .line 201
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :cond_0
    :try_start_1
    const-string v4, "android.os.storage.IMountService"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 202
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "getVolumeState"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 206
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    new-array v4, v9, [Ljava/lang/Object;

    aput-object p1, v4, v8

    invoke-static {v3, p0, v4}, Lcom/google/android/music/download/cache/StorageProbeUtils;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_0

    .line 203
    :catch_1
    move-exception v1

    .line 204
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "StorageProbeUtils"

    const-string v5, "Exception: "

    invoke-static {v4, v5, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static varargs invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p0, "method"    # Ljava/lang/reflect/Method;
    .param p1, "receiver"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 160
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 164
    :goto_0
    return-object v1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "StorageProbeUtils"

    const-string v2, "Exception"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 164
    const/4 v1, 0x0

    goto :goto_0
.end method
