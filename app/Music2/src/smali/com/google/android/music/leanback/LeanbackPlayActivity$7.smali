.class Lcom/google/android/music/leanback/LeanbackPlayActivity$7;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;->changeRating(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

.field final synthetic val$ratingToSet:I


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;I)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$7;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iput p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$7;->val$ratingToSet:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 523
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$7;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 525
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$7;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$7;->val$ratingToSet:I

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->setRating(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 526
    :catch_0
    move-exception v0

    .line 527
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
