.class Lcom/google/android/music/utils/RouteChecker$1;
.super Landroid/database/ContentObserver;
.source "RouteChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/RouteChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/utils/RouteChecker;


# direct methods
.method constructor <init>(Lcom/google/android/music/utils/RouteChecker;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/music/utils/RouteChecker$1;->this$0:Lcom/google/android/music/utils/RouteChecker;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker$1;->this$0:Lcom/google/android/music/utils/RouteChecker;

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getShouldValidateMediaRoutes()Z

    move-result v1

    # setter for: Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z
    invoke-static {v0, v1}, Lcom/google/android/music/utils/RouteChecker;->access$002(Lcom/google/android/music/utils/RouteChecker;Z)Z

    .line 188
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker$1;->this$0:Lcom/google/android/music/utils/RouteChecker;

    # getter for: Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z
    invoke-static {v0}, Lcom/google/android/music/utils/RouteChecker;->access$000(Lcom/google/android/music/utils/RouteChecker;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker$1;->this$0:Lcom/google/android/music/utils/RouteChecker;

    # invokes: Lcom/google/android/music/utils/RouteChecker;->registerPackages()V
    invoke-static {v0}, Lcom/google/android/music/utils/RouteChecker;->access$100(Lcom/google/android/music/utils/RouteChecker;)V

    .line 191
    :cond_0
    return-void
.end method
