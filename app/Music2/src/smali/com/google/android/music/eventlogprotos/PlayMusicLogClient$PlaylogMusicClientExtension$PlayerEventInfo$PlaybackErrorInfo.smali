.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaybackErrorInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;


# instance fields
.field public audioErrorCode:I

.field public failingUrl:Ljava/lang/String;

.field public playbackErrorClass:I

.field public playbackErrorCode:Ljava/lang/Integer;

.field public playbackPositionMillis:Ljava/lang/Integer;

.field public xhrErrorCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1064
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 1065
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1100
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorClass:I

    .line 1105
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->xhrErrorCode:I

    .line 1108
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->audioErrorCode:I

    .line 1065
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1135
    const/4 v0, 0x0

    .line 1136
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->failingUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1137
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->failingUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1140
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackPositionMillis:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1141
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackPositionMillis:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1144
    :cond_1
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorClass:I

    if-eq v1, v3, :cond_2

    .line 1145
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorClass:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1148
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorCode:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1149
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorCode:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1152
    :cond_3
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->xhrErrorCode:I

    if-eq v1, v3, :cond_4

    .line 1153
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->xhrErrorCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1156
    :cond_4
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->audioErrorCode:I

    if-eq v1, v3, :cond_5

    .line 1157
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->audioErrorCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1160
    :cond_5
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->cachedSize:I

    .line 1161
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1169
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1170
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1174
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1175
    :sswitch_0
    return-object p0

    .line 1180
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->failingUrl:Ljava/lang/String;

    goto :goto_0

    .line 1184
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackPositionMillis:Ljava/lang/Integer;

    goto :goto_0

    .line 1188
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1189
    .local v1, "temp":I
    if-eqz v1, :cond_1

    if-eq v1, v4, :cond_1

    if-eq v1, v5, :cond_1

    if-ne v1, v6, :cond_2

    .line 1193
    :cond_1
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorClass:I

    goto :goto_0

    .line 1195
    :cond_2
    iput v3, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorClass:I

    goto :goto_0

    .line 1200
    .end local v1    # "temp":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorCode:Ljava/lang/Integer;

    goto :goto_0

    .line 1204
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1205
    .restart local v1    # "temp":I
    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_3

    if-eq v1, v5, :cond_3

    if-eq v1, v6, :cond_3

    if-eq v1, v7, :cond_3

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    const/4 v2, 0x6

    if-eq v1, v2, :cond_3

    const/4 v2, 0x7

    if-eq v1, v2, :cond_3

    const/16 v2, 0x8

    if-eq v1, v2, :cond_3

    const/16 v2, 0x9

    if-ne v1, v2, :cond_4

    .line 1215
    :cond_3
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->xhrErrorCode:I

    goto :goto_0

    .line 1217
    :cond_4
    iput v3, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->xhrErrorCode:I

    goto :goto_0

    .line 1222
    .end local v1    # "temp":I
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1223
    .restart local v1    # "temp":I
    if-eqz v1, :cond_5

    if-eq v1, v4, :cond_5

    if-eq v1, v5, :cond_5

    if-eq v1, v6, :cond_5

    if-ne v1, v7, :cond_6

    .line 1228
    :cond_5
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->audioErrorCode:I

    goto :goto_0

    .line 1230
    :cond_6
    iput v3, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->audioErrorCode:I

    goto :goto_0

    .line 1170
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1061
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v2, -0x80000000

    .line 1113
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->failingUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1114
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->failingUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1116
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackPositionMillis:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1117
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackPositionMillis:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1119
    :cond_1
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorClass:I

    if-eq v0, v2, :cond_2

    .line 1120
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorClass:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1122
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorCode:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1123
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->playbackErrorCode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1125
    :cond_3
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->xhrErrorCode:I

    if-eq v0, v2, :cond_4

    .line 1126
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->xhrErrorCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1128
    :cond_4
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->audioErrorCode:I

    if-eq v0, v2, :cond_5

    .line 1129
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlaybackErrorInfo;->audioErrorCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1131
    :cond_5
    return-void
.end method
