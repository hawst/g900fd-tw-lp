.class public Lcom/google/android/music/store/MusicFile;
.super Lcom/google/android/music/store/Syncable;
.source "MusicFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/MusicFile$1;
    }
.end annotation


# static fields
.field private static final DELETE_PROJECTION:[Ljava/lang/String;

.field static final FULL_PROJECTION:[Ljava/lang/String;

.field public static final MEDIA_STORE_SOURCE_ACCOUNT_AS_INTEGER:Ljava/lang/Integer;

.field public static final MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

.field public static final PLAYCOUNT_SYNC_PROJECTION:[Ljava/lang/String;

.field private static final REFERENCES_PROJECTION:[Ljava/lang/String;

.field private static SUMMARY_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAddedTime:J

.field private mAlbumArtLocation:Ljava/lang/String;

.field private mAlbumArtist:Ljava/lang/String;

.field private mAlbumArtistId:J

.field private mAlbumArtistOrigin:I

.field private mAlbumId:J

.field private mAlbumIdSourceText:Ljava/lang/String;

.field private mAlbumMetajamId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mArtistArtLocation:Ljava/lang/String;

.field private mArtistMetajamId:Ljava/lang/String;

.field private mBitrate:I

.field private mCanonicalAlbum:Ljava/lang/String;

.field private mCanonicalAlbumArtist:Ljava/lang/String;

.field private mCanonicalGenre:Ljava/lang/String;

.field private mCanonicalTitle:Ljava/lang/String;

.field private mCanonicalTrackArtist:Ljava/lang/String;

.field private mClientId:Ljava/lang/String;

.field private mCompilation:Z

.field private mComposer:Ljava/lang/String;

.field private mDiscCount:S

.field private mDiscPosition:S

.field private mDomain:I

.field private mDurationInMilliSec:J

.field private mFileType:I

.field private mGenre:Ljava/lang/String;

.field private mGenreId:J

.field private mLastPlayDate:J

.field private mLocalCopyBitrate:I

.field private mLocalCopyPath:Ljava/lang/String;

.field private mLocalCopySize:J

.field private mLocalCopyStorageType:I

.field private mLocalCopyStorageVolumeId:Ljava/lang/String;

.field private mLocalCopyStreamFidelity:I

.field private mLocalCopyType:I

.field private mLocalId:J

.field private mNormalizer:Lcom/google/android/music/store/TagNormalizer;

.field private mPlayCount:I

.field private mRating:I

.field private mRatingTimestampMicrosec:J

.field private mSize:J

.field private mSongId:J

.field private mSourceType:I

.field private mStoreSongId:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mTrackArtist:Ljava/lang/String;

.field private mTrackArtistId:J

.field private mTrackArtistOrigin:I

.field private mTrackCountInAlbum:S

.field private mTrackMetajamId:Ljava/lang/String;

.field private mTrackPositionInAlbum:S

.field private mTrackType:I

.field private mVThumbnailUrl:Ljava/lang/String;

.field private mVid:Ljava/lang/String;

.field private mYear:S


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 33
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    .line 35
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_INTEGER:Ljava/lang/Integer;

    .line 39
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MUSIC.Id"

    aput-object v1, v0, v3

    const-string v1, "SourceAccount"

    aput-object v1, v0, v4

    const-string v1, "SourceId"

    aput-object v1, v0, v5

    const-string v1, "Size"

    aput-object v1, v0, v6

    const-string v1, "LocalCopyPath"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "LocalCopyType"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Album"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "AlbumArtist"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "AlbumArtistOrigin"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Artist"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Title"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "LocalCopyStorageType"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "LocalCopySize"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Domain"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SourceType"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "TrackType"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "LocalCopyStorageVolumeId"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/MusicFile;->SUMMARY_PROJECTION:[Ljava/lang/String;

    .line 87
    const/16 v0, 0x3a

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MUSIC.Id"

    aput-object v1, v0, v3

    const-string v1, "SourceAccount"

    aput-object v1, v0, v4

    const-string v1, "SourceId"

    aput-object v1, v0, v5

    const-string v1, "_sync_version"

    aput-object v1, v0, v6

    const-string v1, "Size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "FileType"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "FileDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "LocalCopyPath"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "LocalCopyType"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Title"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Album"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Artist"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "AlbumArtist"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Composer"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Genre"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Year"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Duration"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "TrackCount"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "TrackNumber"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "DiscCount"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "DiscNumber"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Compilation"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "BitRate"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "AlbumArtLocation"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SongId"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "AlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "AlbumArtistId"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "GenreId"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CanonicalName"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CanonicalAlbum"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CanonicalAlbumArtist"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CanonicalGenre"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "PlayCount"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "LastPlayDate"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "AlbumArtistOrigin"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "LocalCopySize"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "LocalCopyBitrate"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "TrackType"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "ArtistOrigin"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "ArtistId"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "CanonicalArtist"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "Rating"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "_sync_dirty"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "StoreId"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "LocalCopyStorageType"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "Domain"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "ArtistArtLocation"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "SourceType"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "ClientId"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "AlbumIdSourceText"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "LocalCopyStorageVolumeId"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "RatingTimestampMicrosec"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "Vid"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "LocalCopyStreamFidelity"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "VThumbnailUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/MusicFile;->FULL_PROJECTION:[Ljava/lang/String;

    .line 393
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "LocalCopyType"

    aput-object v1, v0, v4

    const-string v1, "LocalCopyPath"

    aput-object v1, v0, v5

    const-string v1, "LocalCopyStorageType"

    aput-object v1, v0, v6

    const-string v1, "LocalCopyStorageVolumeId"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/music/store/MusicFile;->DELETE_PROJECTION:[Ljava/lang/String;

    .line 406
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "SourceAccount"

    aput-object v1, v0, v4

    const-string v1, "SourceId"

    aput-object v1, v0, v5

    const-string v1, "PlayCount"

    aput-object v1, v0, v6

    const-string v1, "LastPlayDate"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SourceType"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/MusicFile;->PLAYCOUNT_SYNC_PROJECTION:[Ljava/lang/String;

    .line 425
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "ClientId"

    aput-object v1, v0, v4

    const-string v1, "SourceAccount"

    aput-object v1, v0, v5

    const-string v1, "SourceId"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/store/MusicFile;->REFERENCES_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/android/music/store/Syncable;-><init>()V

    .line 448
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    .line 450
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mFileType:I

    .line 453
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    .line 456
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    .line 460
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    .line 462
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    .line 486
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mDomain:I

    .line 489
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mRating:I

    .line 490
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    return-void
.end method

.method public static calculateAlbumId(Ljava/lang/String;Ljava/lang/String;Z)J
    .locals 4
    .param p0, "albumTitle"    # Ljava/lang/String;
    .param p1, "albumArtist"    # Ljava/lang/String;
    .param p2, "isArtistDerived"    # Z

    .prologue
    .line 2354
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2355
    const-wide/16 v2, 0x0

    .line 2371
    :goto_0
    return-wide v2

    .line 2358
    :cond_0
    new-instance v1, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v1}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 2359
    .local v1, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2360
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p0}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2362
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p2, :cond_2

    .line 2368
    :goto_1
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/MusicFile;->useArtistForAlbumId(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2369
    invoke-virtual {v1, p1}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2371
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0

    .line 2365
    :cond_2
    const/16 v2, 0x1f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static compileDeleteByLocalIdStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1688
    const-string v0, "delete from MUSIC where MUSIC.Id=?1"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compileFullUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1232
    const-string v0, "update MUSIC set SourceAccount=?, SourceId=?, _sync_version=?, Size=?, FileType=?, FileDate=?, LocalCopyPath=?, LocalCopyType=?, Title=?, Album=?, Artist=?, AlbumArtist=?, AlbumArtistOrigin=?, Composer=?, Genre=?, Year=?, Duration=?, TrackCount=?, TrackNumber=?, DiscCount=?, DiscNumber=?, Compilation=?, BitRate=?, AlbumArtLocation=?, SongId=?, AlbumId=?, AlbumArtistId=?, GenreId=?, CanonicalName=?, CanonicalAlbum=?, CanonicalAlbumArtist=?, CanonicalGenre=?, PlayCount=?, LastPlayDate=?, LocalCopySize=?, LocalCopyBitrate=?, TrackType=?, ArtistOrigin=?, ArtistId=?, CanonicalArtist=?, Rating=?, _sync_dirty=?, StoreId=?,StoreAlbumId=?,LocalCopyStorageType=?,Domain=?,ArtistArtLocation=?,SourceType=?,Nid=?,ClientId=?,ArtistMetajamId=?,AlbumIdSourceText=?,LocalCopyStorageVolumeId=?,RatingTimestampMicrosec=?,Vid=?,LocalCopyStreamFidelity=?,VThumbnailUrl=? where Id=?"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compileMusicInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1228
    const-string v0, "insert into MUSIC ( SourceAccount, SourceId, _sync_version, Size, FileType, FileDate, LocalCopyPath, LocalCopyType, Title, Album, Artist, AlbumArtist, AlbumArtistOrigin, Composer, Genre, Year, Duration, TrackCount, TrackNumber, DiscCount, DiscNumber, Compilation, BitRate, AlbumArtLocation, SongId, AlbumId, AlbumArtistId, GenreId, CanonicalName, CanonicalAlbum, CanonicalAlbumArtist, CanonicalGenre, PlayCount, LastPlayDate, LocalCopySize, LocalCopyBitrate, TrackType, ArtistOrigin, ArtistId, CanonicalArtist, Rating, _sync_dirty, StoreId, StoreAlbumId, LocalCopyStorageType, Domain, ArtistArtLocation, SourceType, Nid,ClientId,ArtistMetajamId,AlbumIdSourceText,LocalCopyStorageVolumeId,RatingTimestampMicrosec,Vid,LocalCopyStreamFidelity,VThumbnailUrl) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method private static convertDomain(Lcom/google/android/music/download/ContentIdentifier$Domain;)I
    .locals 3
    .param p0, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;

    .prologue
    .line 997
    sget-object v0, Lcom/google/android/music/store/MusicFile$1;->$SwitchMap$com$google$android$music$download$ContentIdentifier$Domain:[I

    invoke-virtual {p0}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1005
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown domain: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 999
    :pswitch_0
    const/4 v0, 0x0

    .line 1003
    :goto_0
    return v0

    .line 1001
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1003
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 997
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static convertDomain(I)Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 3
    .param p0, "domain"    # I

    .prologue
    .line 984
    packed-switch p0, :pswitch_data_0

    .line 992
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown domain: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 986
    :pswitch_1
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 990
    :goto_0
    return-object v0

    .line 988
    :pswitch_2
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    goto :goto_0

    .line 990
    :pswitch_3
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    goto :goto_0

    .line 984
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static delete(Landroid/database/sqlite/SQLiteStatement;J)V
    .locals 1
    .param p0, "deleteByLocalIdStatement"    # Landroid/database/sqlite/SQLiteStatement;
    .param p1, "localId"    # J

    .prologue
    .line 1751
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1752
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1753
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 1754
    return-void
.end method

.method public static final deleteAndGetLocalCacheFilepath(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "sourceAccount"    # I
    .param p3, "sourceId"    # Ljava/lang/String;

    .prologue
    .line 1699
    const/16 v16, 0x0

    .line 1700
    .local v16, "id":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1701
    .local v14, "file":Ljava/io/File;
    const/4 v3, 0x2

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x1

    aput-object p3, v7, v3

    .line 1702
    .local v7, "accountAndIdArgs":[Ljava/lang/String;
    const-string v4, "MUSIC"

    sget-object v5, Lcom/google/android/music/store/MusicFile;->DELETE_PROJECTION:[Ljava/lang/String;

    const-string v6, "SourceAccount=?1 AND SourceId=?2"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "1"

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1707
    .local v12, "c":Landroid/database/Cursor;
    if-eqz v12, :cond_0

    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1708
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1709
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 1711
    .local v13, "cacheType":I
    const/16 v3, 0x12c

    if-eq v13, v3, :cond_0

    .line 1712
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1713
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1715
    .local v15, "filepath":Ljava/lang/String;
    const/4 v3, 0x4

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->getUUIDFromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v18

    .line 1717
    .local v18, "storageId":Ljava/util/UUID;
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 1718
    const/4 v3, 0x3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 1720
    .local v19, "storageType":I
    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v15, v1, v2}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Ljava/lang/String;ILjava/util/UUID;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 1727
    .end local v13    # "cacheType":I
    .end local v15    # "filepath":Ljava/lang/String;
    .end local v18    # "storageId":Ljava/util/UUID;
    .end local v19    # "storageType":I
    :cond_0
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1730
    if-eqz v16, :cond_1

    .line 1731
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v3, 0x0

    aput-object v16, v17, v3

    .line 1732
    .local v17, "idArgs":[Ljava/lang/String;
    const-string v3, "MUSIC"

    const-string v4, "MUSIC.Id=?1"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1733
    const-string v3, "SHOULDKEEPON"

    const-string v4, "MusicId=?"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1736
    .end local v17    # "idArgs":[Ljava/lang/String;
    :cond_1
    if-nez v14, :cond_2

    const/4 v3, 0x0

    :goto_0
    return-object v3

    .line 1727
    :catchall_0
    move-exception v3

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v3

    .line 1736
    :cond_2
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static deleteByLocalId(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "localId"    # J

    .prologue
    .line 1768
    const/4 v1, 0x0

    .line 1769
    .local v1, "deleteStatement":Landroid/database/sqlite/SQLiteStatement;
    const/4 v3, 0x0

    .line 1770
    .local v3, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 1771
    .local v2, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1773
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/MusicFile;->compileDeleteByLocalIdStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 1774
    invoke-static {v1, p1, p2}, Lcom/google/android/music/store/MusicFile;->delete(Landroid/database/sqlite/SQLiteStatement;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1775
    const/4 v3, 0x1

    .line 1777
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1778
    invoke-virtual {v2, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1781
    if-eqz v3, :cond_0

    .line 1782
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1785
    :cond_0
    return-void

    .line 1777
    :catchall_0
    move-exception v4

    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1778
    invoke-virtual {v2, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v4
.end method

.method public static getMusicFilesToSync(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/database/Cursor;
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # J

    .prologue
    const/4 v5, 0x0

    .line 1882
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/MusicFile;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "SourceAccount=? AND _sync_dirty=1"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getPlaycountsToSync(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/database/Cursor;
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # J

    .prologue
    const/4 v5, 0x0

    .line 1905
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/MusicFile;->PLAYCOUNT_SYNC_PROJECTION:[Ljava/lang/String;

    const-string v3, "SourceAccount=? AND PlayCount>0"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    .locals 10
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p2, "musicId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/DataNotFoundException;
        }
    .end annotation

    .prologue
    .line 2312
    invoke-static {}, Lcom/google/android/music/store/MusicFile;->getSummaryProjection()[Ljava/lang/String;

    move-result-object v2

    .line 2314
    .local v2, "musicProjection":[Ljava/lang/String;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2315
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v3, "MUSIC"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2317
    const/4 v8, 0x0

    .line 2318
    .local v8, "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2320
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "MUSIC.Id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2323
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2324
    :cond_0
    new-instance v3, Lcom/google/android/music/store/DataNotFoundException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/music/store/DataNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2338
    :catchall_0
    move-exception v3

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 2339
    invoke-virtual {p0, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v3

    .line 2327
    :cond_1
    if-nez p1, :cond_2

    .line 2328
    :try_start_1
    new-instance v9, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v9}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .end local p1    # "musicFile":Lcom/google/android/music/store/MusicFile;
    .local v9, "musicFile":Lcom/google/android/music/store/MusicFile;
    move-object p1, v9

    .line 2333
    .end local v9    # "musicFile":Lcom/google/android/music/store/MusicFile;
    .restart local p1    # "musicFile":Lcom/google/android/music/store/MusicFile;
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p1, v8, v3}, Lcom/google/android/music/store/MusicFile;->populateFromSummary(Landroid/database/Cursor;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2338
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 2339
    invoke-virtual {p0, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object p1

    .line 2330
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/android/music/store/MusicFile;->reset()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static getSummaryProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1969
    sget-object v0, Lcom/google/android/music/store/MusicFile;->SUMMARY_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 9
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const/16 v8, 0x18

    const/4 v7, 0x7

    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1245
    iget-object v5, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 1246
    :cond_0
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    const-string v4, "Source id must be set before saving to DB"

    invoke-direct {v3, v4}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1249
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v1, v3

    .line 1250
    .local v1, "hasStoreSongId":Z
    :goto_0
    iget-object v5, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v0, v3

    .line 1252
    .local v0, "hasAlbumMetajamId":Z
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 1253
    .local v2, "isEmptyPath":Z
    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    packed-switch v4, :pswitch_data_0

    .line 1278
    :cond_2
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid storage type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .end local v0    # "hasAlbumMetajamId":Z
    .end local v1    # "hasStoreSongId":Z
    .end local v2    # "isEmptyPath":Z
    :cond_3
    move v1, v4

    .line 1249
    goto :goto_0

    .restart local v1    # "hasStoreSongId":Z
    :cond_4
    move v0, v4

    .line 1250
    goto :goto_1

    .line 1255
    .restart local v0    # "hasAlbumMetajamId":Z
    .restart local v2    # "isEmptyPath":Z
    :pswitch_0
    if-nez v2, :cond_5

    .line 1256
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Local path is set for storage type NONE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1261
    :pswitch_1
    if-eqz v2, :cond_5

    .line 1262
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    const-string v4, "Local path is not set for storage type INTERNAL"

    invoke-direct {v3, v4}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1267
    :pswitch_2
    if-eqz v2, :cond_5

    .line 1268
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    const-string v4, "Local path is not set for storage type EXTERNAL"

    invoke-direct {v3, v4}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1272
    :pswitch_3
    if-eqz v2, :cond_2

    .line 1273
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    const-string v4, "Local path is not set for storage type SECONDARY EXTERNAL"

    invoke-direct {v3, v4}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1281
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->setDerivedFields()V

    .line 1283
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1285
    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mSourceAccount:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1286
    const/16 v3, 0x25

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1287
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1288
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mSourceVersion:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 1289
    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1294
    :goto_2
    const/4 v3, 0x4

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mSize:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1295
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mFileType:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1297
    const/4 v3, 0x6

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mAddedTime:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1299
    if-eqz v2, :cond_9

    .line 1300
    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1304
    :goto_3
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1305
    const/16 v3, 0x2d

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1306
    const/16 v3, 0x24

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyBitrate:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1308
    const/16 v4, 0x9

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    if-nez v3, :cond_a

    const-string v3, ""

    :goto_4
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1309
    const/16 v4, 0xa

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    if-nez v3, :cond_b

    const-string v3, ""

    :goto_5
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1310
    const/16 v4, 0xb

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    if-nez v3, :cond_c

    const-string v3, ""

    :goto_6
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1312
    const/16 v3, 0x26

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1314
    const/16 v4, 0xc

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    if-nez v3, :cond_d

    const-string v3, ""

    :goto_7
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1316
    const/16 v3, 0xd

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1317
    const/16 v4, 0xe

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mComposer:Ljava/lang/String;

    if-nez v3, :cond_e

    const-string v3, ""

    :goto_8
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1318
    const/16 v4, 0xf

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    if-nez v3, :cond_f

    const-string v3, ""

    :goto_9
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1319
    const/16 v3, 0x10

    iget-short v4, p0, Lcom/google/android/music/store/MusicFile;->mYear:S

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1320
    const/16 v3, 0x11

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mDurationInMilliSec:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1321
    const/16 v3, 0x12

    iget-short v4, p0, Lcom/google/android/music/store/MusicFile;->mTrackCountInAlbum:S

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1322
    const/16 v3, 0x13

    iget-short v4, p0, Lcom/google/android/music/store/MusicFile;->mTrackPositionInAlbum:S

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1323
    const/16 v3, 0x14

    iget-short v4, p0, Lcom/google/android/music/store/MusicFile;->mDiscCount:S

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1324
    const/16 v3, 0x15

    iget-short v4, p0, Lcom/google/android/music/store/MusicFile;->mDiscPosition:S

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1325
    const/16 v3, 0x16

    iget-boolean v4, p0, Lcom/google/android/music/store/MusicFile;->mCompilation:Z

    if-eqz v4, :cond_10

    const-wide/16 v4, 0x1

    :goto_a
    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1326
    const/16 v3, 0x17

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mBitrate:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1327
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_11

    .line 1328
    :cond_6
    invoke-virtual {p1, v8}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1332
    :goto_b
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_12

    .line 1333
    :cond_7
    const/16 v3, 0x2f

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1337
    :goto_c
    const/16 v3, 0x19

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1338
    const/16 v3, 0x1a

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1339
    const/16 v3, 0x1b

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistId:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1340
    const/16 v3, 0x27

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistId:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1341
    const/16 v3, 0x1c

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mGenreId:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1342
    const/16 v4, 0x1d

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    if-nez v3, :cond_13

    const-string v3, ""

    :goto_d
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1344
    const/16 v4, 0x1e

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    if-nez v3, :cond_14

    const-string v3, ""

    :goto_e
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1346
    const/16 v4, 0x1f

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    if-nez v3, :cond_15

    const-string v3, ""

    :goto_f
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1348
    const/16 v4, 0x28

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    if-nez v3, :cond_16

    const-string v3, ""

    :goto_10
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1350
    const/16 v4, 0x20

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    if-nez v3, :cond_17

    const-string v3, ""

    :goto_11
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1352
    const/16 v3, 0x21

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mPlayCount:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1353
    const/16 v3, 0x22

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mLastPlayDate:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1354
    const/16 v3, 0x23

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopySize:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1355
    const/16 v3, 0x29

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mRating:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1356
    const/16 v3, 0x2a

    iget-boolean v4, p0, Lcom/google/android/music/store/MusicFile;->mNeedsSync:Z

    if-eqz v4, :cond_18

    const-wide/16 v4, 0x1

    :goto_12
    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1358
    if-eqz v1, :cond_19

    .line 1359
    const/16 v3, 0x2b

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1363
    :goto_13
    if-eqz v0, :cond_1a

    .line 1364
    const/16 v3, 0x2c

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1368
    :goto_14
    const/16 v3, 0x2e

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mDomain:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1370
    const/16 v3, 0x30

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1372
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    if-eqz v3, :cond_1b

    .line 1373
    const/16 v3, 0x31

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1377
    :goto_15
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    if-eqz v3, :cond_1c

    .line 1378
    const/16 v3, 0x32

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1382
    :goto_16
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    if-eqz v3, :cond_1d

    .line 1383
    const/16 v3, 0x33

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1387
    :goto_17
    const/16 v4, 0x34

    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumIdSourceText:Ljava/lang/String;

    if-nez v3, :cond_1e

    const-string v3, ""

    :goto_18
    invoke-virtual {p1, v4, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1389
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    if-eqz v3, :cond_1f

    .line 1390
    const/16 v3, 0x35

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1395
    :goto_19
    const/16 v3, 0x36

    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mRatingTimestampMicrosec:J

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1397
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mVid:Ljava/lang/String;

    if-eqz v3, :cond_20

    .line 1398
    const/16 v3, 0x37

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mVid:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1402
    :goto_1a
    const/16 v3, 0x38

    iget v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStreamFidelity:I

    int-to-long v4, v4

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1405
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mVThumbnailUrl:Ljava/lang/String;

    if-eqz v3, :cond_21

    .line 1406
    const/16 v3, 0x39

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mVThumbnailUrl:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1410
    :goto_1b
    return-void

    .line 1291
    :cond_8
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {p1, v6, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 1302
    :cond_9
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    invoke-virtual {p1, v7, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 1308
    :cond_a
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    goto/16 :goto_4

    .line 1309
    :cond_b
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    goto/16 :goto_5

    .line 1310
    :cond_c
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    goto/16 :goto_6

    .line 1314
    :cond_d
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    goto/16 :goto_7

    .line 1317
    :cond_e
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mComposer:Ljava/lang/String;

    goto/16 :goto_8

    .line 1318
    :cond_f
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    goto/16 :goto_9

    .line 1325
    :cond_10
    const-wide/16 v4, 0x0

    goto/16 :goto_a

    .line 1330
    :cond_11
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    invoke-virtual {p1, v8, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_b

    .line 1335
    :cond_12
    const/16 v3, 0x2f

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_c

    .line 1342
    :cond_13
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    goto/16 :goto_d

    .line 1344
    :cond_14
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    goto/16 :goto_e

    .line 1346
    :cond_15
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    goto/16 :goto_f

    .line 1348
    :cond_16
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    goto/16 :goto_10

    .line 1350
    :cond_17
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    goto/16 :goto_11

    .line 1356
    :cond_18
    const-wide/16 v4, 0x0

    goto/16 :goto_12

    .line 1361
    :cond_19
    const/16 v3, 0x2b

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_13

    .line 1366
    :cond_1a
    const/16 v3, 0x2c

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_14

    .line 1375
    :cond_1b
    const/16 v3, 0x31

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_15

    .line 1380
    :cond_1c
    const/16 v3, 0x32

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_16

    .line 1385
    :cond_1d
    const/16 v3, 0x33

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_17

    .line 1387
    :cond_1e
    iget-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumIdSourceText:Ljava/lang/String;

    goto/16 :goto_18

    .line 1393
    :cond_1f
    const/16 v3, 0x35

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_19

    .line 1400
    :cond_20
    const/16 v3, 0x37

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_1a

    .line 1408
    :cond_21
    const/16 v3, 0x39

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_1b

    .line 1253
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static readAndResetPreservingReferences(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # Ljava/lang/String;
    .param p2, "sourceId"    # Ljava/lang/String;
    .param p3, "file"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 1847
    if-nez p3, :cond_2

    .line 1848
    new-instance p3, Lcom/google/android/music/store/MusicFile;

    .end local p3    # "file":Lcom/google/android/music/store/MusicFile;
    invoke-direct {p3}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 1853
    .restart local p3    # "file":Lcom/google/android/music/store/MusicFile;
    :goto_0
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/MusicFile;->REFERENCES_PROJECTION:[Ljava/lang/String;

    const-string v3, "SourceAccount=? AND SourceId=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1859
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1860
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p3, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    .line 1861
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1862
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    .line 1864
    :cond_0
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p3, Lcom/google/android/music/store/MusicFile;->mSourceAccount:I

    .line 1865
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1868
    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1871
    return-object p3

    .line 1850
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_2
    invoke-virtual {p3}, Lcom/google/android/music/store/MusicFile;->reset()V

    goto :goto_0

    .line 1868
    .restart local v8    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static readMusicFile(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # Ljava/lang/String;
    .param p2, "sourceId"    # Ljava/lang/String;
    .param p3, "file"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    const/4 v5, 0x0

    .line 1797
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/MusicFile;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "SourceAccount=? AND SourceId=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1803
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1805
    if-nez p3, :cond_0

    .line 1806
    new-instance v9, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v9}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .end local p3    # "file":Lcom/google/android/music/store/MusicFile;
    .local v9, "file":Lcom/google/android/music/store/MusicFile;
    move-object p3, v9

    .line 1808
    .end local v9    # "file":Lcom/google/android/music/store/MusicFile;
    .restart local p3    # "file":Lcom/google/android/music/store/MusicFile;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/MusicFile;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1814
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static resetPlayCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1929
    .local p1, "musicIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1941
    :cond_0
    :goto_0
    return-void

    .line 1933
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 1934
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "PlayCount"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1936
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1937
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v2, "Id"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1938
    invoke-static {v0, p1}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    .line 1940
    const-string v2, "MUSIC"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private final setAlbumArtist(Ljava/lang/String;I)V
    .locals 4
    .param p1, "albumArtist"    # Ljava/lang/String;
    .param p2, "albumArtistOrigin"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 736
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/store/MusicFile;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 737
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    .line 738
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistId:J

    .line 739
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 740
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    .line 741
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    .line 742
    iput p2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    .line 744
    :cond_1
    return-void
.end method

.method private final setTrackArtist(Ljava/lang/String;I)V
    .locals 3
    .param p1, "trackArtist"    # Ljava/lang/String;
    .param p2, "trackArtistOrigin"    # I

    .prologue
    const/4 v2, 0x1

    .line 692
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/store/MusicFile;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 693
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    .line 694
    iput p2, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    .line 695
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 696
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistId:J

    .line 697
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    if-ne v0, v2, :cond_2

    .line 699
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    if-ne v0, v2, :cond_1

    .line 700
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Both track artist and album artist can\'t be derived"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;I)V

    .line 706
    :cond_2
    return-void
.end method

.method private static stringChanged(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "oldValue"    # Ljava/lang/String;
    .param p1, "newValue"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1098
    if-nez p0, :cond_2

    .line 1099
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1103
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1099
    goto :goto_0

    .line 1100
    :cond_2
    if-nez p1, :cond_3

    .line 1101
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1103
    :cond_3
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static throwIfInvalidRating(I)V
    .locals 3
    .param p0, "rating"    # I

    .prologue
    .line 1025
    if-ltz p0, :cond_0

    const/4 v0, 0x5

    if-le p0, v0, :cond_1

    .line 1027
    :cond_0
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rating value of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of expected range"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1031
    :cond_1
    return-void
.end method

.method public static useArtistForAlbumId(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "albumTitle"    # Ljava/lang/String;
    .param p1, "albumArtist"    # Ljava/lang/String;
    .param p2, "isArtistDerived"    # Z

    .prologue
    .line 2387
    if-eqz p2, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final forceAlbumArtist(Ljava/lang/String;)V
    .locals 1
    .param p1, "albumArtist"    # Ljava/lang/String;

    .prologue
    .line 725
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;I)V

    .line 726
    return-void
.end method

.method public final getAddedTime()J
    .locals 2

    .prologue
    .line 584
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mAddedTime:J

    return-wide v0
.end method

.method public getAlbumArtLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    return-object v0
.end method

.method public final getAlbumArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 765
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    return-wide v0
.end method

.method public final getAlbumMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 968
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public final getAlbumName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 751
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method public final getArtistMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public final getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getComposer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 769
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mComposer:Ljava/lang/String;

    return-object v0
.end method

.method public final getDiscCount()S
    .locals 1

    .prologue
    .line 842
    iget-short v0, p0, Lcom/google/android/music/store/MusicFile;->mDiscCount:S

    return v0
.end method

.method public final getDiscPosition()S
    .locals 1

    .prologue
    .line 850
    iget-short v0, p0, Lcom/google/android/music/store/MusicFile;->mDiscPosition:S

    return v0
.end method

.method public getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 1

    .prologue
    .line 980
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mDomain:I

    invoke-static {v0}, Lcom/google/android/music/store/MusicFile;->convertDomain(I)Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v0

    return-object v0
.end method

.method public final getDurationInMilliSec()J
    .locals 2

    .prologue
    .line 810
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mDurationInMilliSec:J

    return-wide v0
.end method

.method public final getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    return-object v0
.end method

.method public final getIsCloudFile()Z
    .locals 1

    .prologue
    .line 580
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceAccount:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getLastPlayDate()J
    .locals 2

    .prologue
    .line 906
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mLastPlayDate:J

    return-wide v0
.end method

.method public final getLocalCopyPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocalCopySize()J
    .locals 2

    .prologue
    .line 656
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopySize:J

    return-wide v0
.end method

.method public final getLocalCopyStorageType()I
    .locals 1

    .prologue
    .line 640
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    return v0
.end method

.method public final getLocalCopyStorageVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocalCopyType()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    return v0
.end method

.method public final getLocalId()J
    .locals 2

    .prologue
    .line 558
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    return-wide v0
.end method

.method public final getPlayCount()I
    .locals 1

    .prologue
    .line 924
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mPlayCount:I

    return v0
.end method

.method public final getRating()I
    .locals 1

    .prologue
    .line 936
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mRating:I

    return v0
.end method

.method public getRatingTimestampMicrosec()J
    .locals 2

    .prologue
    .line 972
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mRatingTimestampMicrosec:J

    return-wide v0
.end method

.method public final getSize()J
    .locals 2

    .prologue
    .line 603
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mSize:J

    return-wide v0
.end method

.method public final getSourceType()I
    .locals 1

    .prologue
    .line 1042
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    return v0
.end method

.method public final getStoreSongId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getTrackArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    return-object v0
.end method

.method public final getTrackMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTrackPositionInAlbum()S
    .locals 1

    .prologue
    .line 794
    iget-short v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackPositionInAlbum:S

    return v0
.end method

.method public final getTrackType()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    return v0
.end method

.method public final getYear()S
    .locals 1

    .prologue
    .line 802
    iget-short v0, p0, Lcom/google/android/music/store/MusicFile;->mYear:S

    return v0
.end method

.method public final insertMusicFile(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 6
    .param p1, "musicInsert"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 1426
    iget-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1427
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "MusicFile already created. Forgot to call reset()?"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1430
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 1431
    invoke-static {}, Lcom/google/android/music/store/Store;->generateClientId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    .line 1434
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/store/MusicFile;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 1436
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 1437
    .local v0, "insertedId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 1438
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to insert into MUSIC"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1440
    :cond_2
    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    .line 1443
    iget-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    return-wide v2
.end method

.method public final isNautilus()Z
    .locals 2

    .prologue
    .line 566
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPurchasedTrack()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 588
    iget v1, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSourceTypeLocker()Z
    .locals 2

    .prologue
    .line 1054
    iget v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "localId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/DataNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2294
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/MusicFile;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "Id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2299
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300
    invoke-virtual {p0, v8}, Lcom/google/android/music/store/MusicFile;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2305
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 2307
    return-void

    .line 2302
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/music/store/DataNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MusicFile with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/store/DataNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2305
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/16 v7, 0x17

    const/4 v6, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2010
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    .line 2011
    const/16 v0, 0x25

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    .line 2012
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceAccount:I

    .line 2013
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    .line 2014
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2015
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mSourceVersion:Ljava/lang/String;

    .line 2020
    :goto_0
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mSize:J

    .line 2021
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mFileType:I

    .line 2022
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mAddedTime:J

    .line 2023
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    .line 2024
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    .line 2025
    const/16 v0, 0x2d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    .line 2026
    const/16 v0, 0x24

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyBitrate:I

    .line 2027
    const/16 v0, 0x35

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2028
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    .line 2033
    :goto_1
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    .line 2034
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    .line 2035
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    .line 2036
    const/16 v0, 0x26

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    .line 2037
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    .line 2038
    const/16 v0, 0x22

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    .line 2039
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mComposer:Ljava/lang/String;

    .line 2040
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    .line 2041
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mYear:S

    .line 2042
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mDurationInMilliSec:J

    .line 2043
    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackCountInAlbum:S

    .line 2044
    const/16 v0, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackPositionInAlbum:S

    .line 2045
    const/16 v0, 0x13

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mDiscCount:S

    .line 2046
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mDiscPosition:S

    .line 2047
    const/16 v0, 0x15

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/music/store/MusicFile;->mCompilation:Z

    .line 2048
    const/16 v0, 0x16

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mBitrate:I

    .line 2049
    invoke-interface {p1, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2050
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    .line 2054
    :goto_3
    const/16 v0, 0x2f

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2055
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    .line 2059
    :goto_4
    const/16 v0, 0x18

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    .line 2060
    const/16 v0, 0x19

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    .line 2061
    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistId:J

    .line 2062
    const/16 v0, 0x27

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistId:J

    .line 2063
    const/16 v0, 0x1b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mGenreId:J

    .line 2064
    const/16 v0, 0x1c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    .line 2065
    const/16 v0, 0x1d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    .line 2066
    const/16 v0, 0x1e

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 2067
    const/16 v0, 0x28

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 2068
    const/16 v0, 0x1f

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    .line 2069
    const/16 v0, 0x20

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mPlayCount:I

    .line 2070
    const/16 v0, 0x21

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mLastPlayDate:J

    .line 2071
    const/16 v0, 0x23

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopySize:J

    .line 2072
    const/16 v0, 0x29

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mRating:I

    .line 2073
    const/16 v0, 0x2a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/google/android/music/store/MusicFile;->mNeedsSync:Z

    .line 2074
    const/16 v0, 0x2b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2075
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    .line 2079
    :goto_6
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2080
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    .line 2084
    :goto_7
    const/16 v0, 0x2e

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mDomain:I

    .line 2085
    const/16 v0, 0x30

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    .line 2086
    const/16 v0, 0x31

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2087
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    .line 2091
    :goto_8
    const/16 v0, 0x32

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2092
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    .line 2096
    :goto_9
    const/16 v0, 0x33

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2097
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    .line 2101
    :goto_a
    const/16 v0, 0x34

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumIdSourceText:Ljava/lang/String;

    .line 2102
    const/16 v0, 0x36

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mRatingTimestampMicrosec:J

    .line 2103
    const/16 v0, 0x37

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2104
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mVid:Ljava/lang/String;

    .line 2108
    :goto_b
    const/16 v0, 0x38

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStreamFidelity:I

    .line 2110
    const/16 v0, 0x39

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2111
    iput-object v3, p0, Lcom/google/android/music/store/MusicFile;->mVThumbnailUrl:Ljava/lang/String;

    .line 2115
    :goto_c
    return-void

    .line 2017
    :cond_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceVersion:Ljava/lang/String;

    goto/16 :goto_0

    .line 2030
    :cond_1
    const/16 v0, 0x35

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 2047
    goto/16 :goto_2

    .line 2052
    :cond_3
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    goto/16 :goto_3

    .line 2057
    :cond_4
    const/16 v0, 0x2f

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    goto/16 :goto_4

    :cond_5
    move v1, v2

    .line 2073
    goto/16 :goto_5

    .line 2077
    :cond_6
    const/16 v0, 0x2b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    goto/16 :goto_6

    .line 2082
    :cond_7
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    goto/16 :goto_7

    .line 2089
    :cond_8
    const/16 v0, 0x31

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    goto/16 :goto_8

    .line 2094
    :cond_9
    const/16 v0, 0x32

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    goto/16 :goto_9

    .line 2099
    :cond_a
    const/16 v0, 0x33

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    goto/16 :goto_a

    .line 2106
    :cond_b
    const/16 v0, 0x37

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mVid:Ljava/lang/String;

    goto :goto_b

    .line 2113
    :cond_c
    const/16 v0, 0x39

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mVThumbnailUrl:Ljava/lang/String;

    goto :goto_c
.end method

.method populateFromSummary(Landroid/database/Cursor;I)V
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "columnOffset"    # I

    .prologue
    .line 1978
    add-int/lit8 v0, p2, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    .line 1979
    add-int/lit8 v0, p2, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceAccount:I

    .line 1980
    add-int/lit8 v0, p2, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    .line 1981
    add-int/lit8 v0, p2, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mSize:J

    .line 1982
    add-int/lit8 v0, p2, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    .line 1983
    add-int/lit8 v0, p2, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    .line 1984
    add-int/lit8 v0, p2, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mDurationInMilliSec:J

    .line 1985
    add-int/lit8 v0, p2, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    .line 1986
    add-int/lit8 v0, p2, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    .line 1987
    add-int/lit8 v0, p2, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    .line 1988
    add-int/lit8 v0, p2, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    .line 1989
    add-int/lit8 v0, p2, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    .line 1990
    add-int/lit8 v0, p2, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    .line 1992
    add-int/lit8 v0, p2, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopySize:J

    .line 1993
    add-int/lit8 v0, p2, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mDomain:I

    .line 1994
    add-int/lit8 v0, p2, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    .line 1995
    add-int/lit8 v0, p2, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    .line 1996
    add-int/lit8 v0, p2, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    .line 1997
    add-int/lit8 v0, p2, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1998
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    .line 2003
    :goto_0
    add-int/lit8 v0, p2, 0x13

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    .line 2004
    add-int/lit8 v0, p2, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    .line 2005
    return-void

    .line 2000
    :cond_0
    add-int/lit8 v0, p2, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1159
    invoke-super {p0}, Lcom/google/android/music/store/Syncable;->reset()V

    .line 1160
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    .line 1161
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    .line 1162
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAddedTime:J

    .line 1163
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mSize:J

    .line 1164
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mFileType:I

    .line 1165
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAddedTime:J

    .line 1166
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    .line 1167
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    .line 1168
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopySize:J

    .line 1169
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyBitrate:I

    .line 1170
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    .line 1171
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    .line 1172
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mPlayCount:I

    .line 1173
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLastPlayDate:J

    .line 1174
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    .line 1175
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    .line 1176
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    .line 1177
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    .line 1178
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    .line 1179
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    .line 1180
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mComposer:Ljava/lang/String;

    .line 1181
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    .line 1182
    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mYear:S

    .line 1183
    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackCountInAlbum:S

    .line 1184
    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mTrackPositionInAlbum:S

    .line 1185
    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mDiscCount:S

    .line 1186
    iput-short v0, p0, Lcom/google/android/music/store/MusicFile;->mDiscPosition:S

    .line 1187
    iput-boolean v0, p0, Lcom/google/android/music/store/MusicFile;->mCompilation:Z

    .line 1188
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mBitrate:I

    .line 1189
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mDurationInMilliSec:J

    .line 1190
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    .line 1191
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    .line 1192
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mRating:I

    .line 1193
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    .line 1194
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    .line 1195
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mDomain:I

    .line 1196
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->resetDerivedFields()V

    .line 1197
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    .line 1198
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    .line 1199
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    .line 1200
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    .line 1201
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumIdSourceText:Ljava/lang/String;

    .line 1202
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mRatingTimestampMicrosec:J

    .line 1203
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mVid:Ljava/lang/String;

    .line 1204
    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStreamFidelity:I

    .line 1205
    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mVThumbnailUrl:Ljava/lang/String;

    .line 1206
    return-void
.end method

.method public resetDerivedFields()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1214
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    .line 1215
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistId:J

    .line 1216
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistId:J

    .line 1217
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    .line 1218
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mGenreId:J

    .line 1219
    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    .line 1220
    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    .line 1221
    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1222
    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1223
    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    .line 1224
    return-void
.end method

.method public final setAddedTime(J)V
    .locals 3
    .param p1, "addedTime"    # J

    .prologue
    .line 596
    const-wide v0, 0x4977387000L

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x3bb2cc3d800L

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 597
    :cond_0
    const-string v0, "MusicStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected added time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    :cond_1
    iput-wide p1, p0, Lcom/google/android/music/store/MusicFile;->mAddedTime:J

    .line 600
    return-void
.end method

.method public setAlbumArtLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumArtLocation"    # Ljava/lang/String;

    .prologue
    .line 822
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    .line 823
    return-void
.end method

.method public final setAlbumArtist(Ljava/lang/String;)V
    .locals 1
    .param p1, "albumArtist"    # Ljava/lang/String;

    .prologue
    .line 721
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;I)V

    .line 722
    return-void
.end method

.method public final setAlbumMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumMetajamId"    # Ljava/lang/String;

    .prologue
    .line 1021
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    .line 1022
    return-void
.end method

.method public final setAlbumName(Ljava/lang/String;)V
    .locals 4
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    .line 756
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/store/MusicFile;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 757
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    .line 758
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    .line 759
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    .line 760
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    .line 762
    :cond_0
    return-void
.end method

.method public setArtistArtLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "artistArtLocation"    # Ljava/lang/String;

    .prologue
    .line 830
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    .line 831
    return-void
.end method

.method public final setArtistMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "artistMetajamId"    # Ljava/lang/String;

    .prologue
    .line 1082
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    .line 1083
    return-void
.end method

.method public final setClientId(Ljava/lang/String;)V
    .locals 0
    .param p1, "clientId"    # Ljava/lang/String;

    .prologue
    .line 1074
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    .line 1075
    return-void
.end method

.method public final setComposer(Ljava/lang/String;)V
    .locals 0
    .param p1, "composer"    # Ljava/lang/String;

    .prologue
    .line 773
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mComposer:Ljava/lang/String;

    .line 774
    return-void
.end method

.method setDerivedFields()V
    .locals 15

    .prologue
    const/4 v8, 0x0

    const/4 v14, 0x0

    const/4 v7, 0x1

    const-wide/16 v12, 0x0

    const/16 v11, 0x1f

    .line 1536
    sget-object v9, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1537
    iput-object v14, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    .line 1539
    :cond_0
    sget-object v9, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1540
    iput-object v14, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1542
    :cond_1
    sget-object v9, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1543
    iput-object v14, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1546
    :cond_2
    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    if-nez v9, :cond_3

    .line 1547
    new-instance v9, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v9}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    iput-object v9, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    .line 1549
    :cond_3
    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_8

    move v2, v7

    .line 1550
    .local v2, "hasAlbumArtist":Z
    :goto_0
    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_9

    move v3, v7

    .line 1551
    .local v3, "hasTrackArtist":Z
    :goto_1
    if-nez v2, :cond_a

    if-eqz v3, :cond_a

    .line 1552
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    invoke-direct {p0, v8, v7}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;I)V

    .line 1557
    :cond_4
    :goto_2
    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mGenreId:J

    cmp-long v8, v8, v12

    if-nez v8, :cond_6

    .line 1558
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    .line 1559
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    if-eqz v8, :cond_5

    .line 1560
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    .line 1562
    :cond_5
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mGenreId:J

    .line 1565
    :cond_6
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    if-nez v8, :cond_c

    .line 1566
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_b

    .line 1567
    :cond_7
    new-instance v7, Lcom/google/android/music/store/InvalidDataException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Song title must not be empty. mTrackMetajamId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSourceId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v7

    .end local v2    # "hasAlbumArtist":Z
    .end local v3    # "hasTrackArtist":Z
    :cond_8
    move v2, v8

    .line 1549
    goto :goto_0

    .restart local v2    # "hasAlbumArtist":Z
    :cond_9
    move v3, v8

    .line 1550
    goto :goto_1

    .line 1553
    .restart local v3    # "hasTrackArtist":Z
    :cond_a
    if-nez v3, :cond_4

    if-eqz v2, :cond_4

    .line 1554
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    invoke-direct {p0, v8, v7}, Lcom/google/android/music/store/MusicFile;->setTrackArtist(Ljava/lang/String;I)V

    goto :goto_2

    .line 1571
    :cond_b
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    .line 1572
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    if-eqz v8, :cond_c

    .line 1573
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    .line 1577
    :cond_c
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    if-nez v8, :cond_d

    .line 1578
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    .line 1579
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    if-eqz v8, :cond_d

    .line 1580
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    .line 1584
    :cond_d
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    if-eqz v8, :cond_e

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    if-eqz v8, :cond_e

    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistId:J

    cmp-long v8, v8, v12

    if-eqz v8, :cond_e

    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistId:J

    cmp-long v8, v8, v12

    if-nez v8, :cond_11

    .line 1587
    :cond_e
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1588
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    if-eqz v8, :cond_f

    .line 1589
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1591
    :cond_f
    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistId:J

    .line 1593
    iget v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    if-eq v8, v7, :cond_10

    iget v8, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    if-eq v8, v7, :cond_10

    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/google/android/music/store/MusicFile;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1e

    .line 1596
    :cond_10
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1597
    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistId:J

    iput-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistId:J

    .line 1607
    :cond_11
    :goto_3
    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    cmp-long v7, v8, v12

    if-eqz v7, :cond_12

    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    cmp-long v7, v8, v12

    if-nez v7, :cond_1a

    .line 1608
    :cond_12
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 1609
    .local v4, "isUnknownAlbum":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1610
    .local v1, "builder":Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1611
    .local v6, "sourceTextBuilder":Ljava/lang/StringBuilder;
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1612
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_20

    .line 1613
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1617
    :goto_4
    const/4 v0, 0x0

    .line 1618
    .local v0, "albumIdHasArtist":Z
    iget v7, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    if-eqz v7, :cond_13

    iget v7, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_22

    :cond_13
    if-eqz v4, :cond_14

    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_22

    .line 1624
    :cond_14
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1626
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1627
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_21

    .line 1628
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1633
    :goto_5
    const/4 v0, 0x1

    .line 1643
    :cond_15
    :goto_6
    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    cmp-long v7, v8, v12

    if-nez v7, :cond_17

    .line 1644
    if-nez v0, :cond_16

    .line 1645
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1648
    :cond_16
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/store/MusicFile;->mAlbumIdSourceText:Ljava/lang/String;

    .line 1649
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mAlbumId:J

    .line 1652
    :cond_17
    iget-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    cmp-long v7, v8, v12

    if-nez v7, :cond_1a

    .line 1653
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1655
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-short v8, p0, Lcom/google/android/music/store/MusicFile;->mDiscPosition:S

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-short v8, p0, Lcom/google/android/music/store/MusicFile;->mTrackPositionInAlbum:S

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1657
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_19

    if-eqz v0, :cond_18

    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_19

    .line 1660
    :cond_18
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1662
    :cond_19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1663
    .local v5, "songKey":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    .line 1671
    .end local v0    # "albumIdHasArtist":Z
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "isUnknownAlbum":Z
    .end local v5    # "songKey":Ljava/lang/String;
    .end local v6    # "sourceTextBuilder":Ljava/lang/StringBuilder;
    :cond_1a
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 1672
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbum:Ljava/lang/String;

    .line 1674
    :cond_1b
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1c

    .line 1675
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1677
    :cond_1c
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1d

    .line 1678
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1680
    :cond_1d
    return-void

    .line 1599
    :cond_1e
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1600
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1f

    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    if-eqz v7, :cond_1f

    .line 1601
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1603
    :cond_1f
    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistId:J

    goto/16 :goto_3

    .line 1615
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    .restart local v4    # "isUnknownAlbum":Z
    .restart local v6    # "sourceTextBuilder":Ljava/lang/StringBuilder;
    :cond_20
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1630
    .restart local v0    # "albumIdHasArtist":Z
    :cond_21
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1634
    :cond_22
    if-eqz v4, :cond_15

    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_15

    .line 1637
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1638
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1640
    const/4 v0, 0x1

    goto/16 :goto_6
.end method

.method public final setDiscCount(S)V
    .locals 0
    .param p1, "discCount"    # S

    .prologue
    .line 846
    iput-short p1, p0, Lcom/google/android/music/store/MusicFile;->mDiscCount:S

    .line 847
    return-void
.end method

.method public final setDiscPosition(S)V
    .locals 0
    .param p1, "discPosition"    # S

    .prologue
    .line 854
    iput-short p1, p0, Lcom/google/android/music/store/MusicFile;->mDiscPosition:S

    .line 855
    return-void
.end method

.method public setDomain(Lcom/google/android/music/download/ContentIdentifier$Domain;)V
    .locals 1
    .param p1, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;

    .prologue
    .line 1010
    invoke-static {p1}, Lcom/google/android/music/store/MusicFile;->convertDomain(Lcom/google/android/music/download/ContentIdentifier$Domain;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mDomain:I

    .line 1011
    return-void
.end method

.method public final setDurationInMilliSec(J)V
    .locals 1
    .param p1, "durationInMilliSec"    # J

    .prologue
    .line 814
    iput-wide p1, p0, Lcom/google/android/music/store/MusicFile;->mDurationInMilliSec:J

    .line 815
    return-void
.end method

.method public final setFileType(I)V
    .locals 0
    .param p1, "fileType"    # I

    .prologue
    .line 615
    iput p1, p0, Lcom/google/android/music/store/MusicFile;->mFileType:I

    .line 616
    return-void
.end method

.method public final setGenre(Ljava/lang/String;)V
    .locals 2
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/store/MusicFile;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    .line 783
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mGenreId:J

    .line 784
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalGenre:Ljava/lang/String;

    .line 786
    :cond_0
    return-void
.end method

.method public final setLocalCopyType(I)V
    .locals 0
    .param p1, "localCopyType"    # I

    .prologue
    .line 636
    iput p1, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    .line 637
    return-void
.end method

.method public final setMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 620
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/music/store/MusicFile;->mFileType:I

    .line 621
    return-void
.end method

.method public final setRating(I)V
    .locals 0
    .param p1, "rating"    # I

    .prologue
    .line 944
    invoke-static {p1}, Lcom/google/android/music/store/MusicFile;->throwIfInvalidRating(I)V

    .line 945
    iput p1, p0, Lcom/google/android/music/store/MusicFile;->mRating:I

    .line 946
    return-void
.end method

.method public setRatingTimestampMicrosec(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 976
    iput-wide p1, p0, Lcom/google/android/music/store/MusicFile;->mRatingTimestampMicrosec:J

    .line 977
    return-void
.end method

.method public final setSize(J)V
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 607
    iput-wide p1, p0, Lcom/google/android/music/store/MusicFile;->mSize:J

    .line 608
    return-void
.end method

.method public final setSourceType(I)V
    .locals 0
    .param p1, "sourceType"    # I

    .prologue
    .line 1046
    iput p1, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    .line 1047
    return-void
.end method

.method public final setStoreSongId(Ljava/lang/String;)V
    .locals 0
    .param p1, "storeSongId"    # Ljava/lang/String;

    .prologue
    .line 960
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    .line 961
    return-void
.end method

.method public final setTagNormalizer(Lcom/google/android/music/store/TagNormalizer;)V
    .locals 0
    .param p1, "normalizer"    # Lcom/google/android/music/store/TagNormalizer;

    .prologue
    .line 1038
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    .line 1039
    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/store/MusicFile;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 677
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    .line 678
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/MusicFile;->mCanonicalTitle:Ljava/lang/String;

    .line 679
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mSongId:J

    .line 681
    :cond_0
    return-void
.end method

.method public final setTrackArtist(Ljava/lang/String;)V
    .locals 1
    .param p1, "trackArtist"    # Ljava/lang/String;

    .prologue
    .line 688
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/store/MusicFile;->setTrackArtist(Ljava/lang/String;I)V

    .line 689
    return-void
.end method

.method public final setTrackMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackMetajamId"    # Ljava/lang/String;

    .prologue
    .line 1066
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    .line 1067
    return-void
.end method

.method public final setTrackPositionInAlbum(S)V
    .locals 0
    .param p1, "trackPositionInAlbum"    # S

    .prologue
    .line 798
    iput-short p1, p0, Lcom/google/android/music/store/MusicFile;->mTrackPositionInAlbum:S

    .line 799
    return-void
.end method

.method public final setTrackType(I)V
    .locals 0
    .param p1, "trackType"    # I

    .prologue
    .line 571
    iput p1, p0, Lcom/google/android/music/store/MusicFile;->mTrackType:I

    .line 572
    return-void
.end method

.method public final setVThumbnailUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "vThumbnailUrl"    # Ljava/lang/String;

    .prologue
    .line 1125
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mVThumbnailUrl:Ljava/lang/String;

    .line 1126
    return-void
.end method

.method public final setVid(Ljava/lang/String;)V
    .locals 0
    .param p1, "vid"    # Ljava/lang/String;

    .prologue
    .line 1112
    iput-object p1, p0, Lcom/google/android/music/store/MusicFile;->mVid:Ljava/lang/String;

    .line 1113
    return-void
.end method

.method public final setYear(S)V
    .locals 0
    .param p1, "year"    # S

    .prologue
    .line 806
    iput-short p1, p0, Lcom/google/android/music/store/MusicFile;->mYear:S

    .line 807
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 509
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 510
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    const-string v1, "account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mSourceAccount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    const-string v1, "sourceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    const-string v1, "sourceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mSourceType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    const-string v1, "addedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mAddedTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    const-string v1, "size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    const-string v1, "fileType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mFileType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    const-string v1, "localPath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    const-string v1, "localCopyType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    const-string v1, "localCopyStorageType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    const-string v1, "localCopyStorageVolumeId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStorageVolumeId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521
    const-string v1, "title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    const-string v1, "artist="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtist:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    const-string v1, "artistOrigin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mTrackArtistOrigin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    const-string v1, "albumArtist="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 525
    const-string v1, "albumArtistOrigin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtistOrigin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    const-string v1, "album="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527
    const-string v1, "composer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mComposer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    const-string v1, "genre="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mGenre:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    const-string v1, "position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p0, Lcom/google/android/music/store/MusicFile;->mTrackPositionInAlbum:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    const-string v1, "year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p0, Lcom/google/android/music/store/MusicFile;->mYear:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    const-string v1, "albumArtLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumArtLocation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 532
    const-string v1, "artistArtLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mArtistArtLocation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 533
    const-string v1, "rating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mRating:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 534
    iget-object v1, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 535
    const-string v1, "storeSongId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mStoreSongId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 537
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 538
    const-string v1, "albumMetajamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 541
    const-string v1, "trackMetajamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 543
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 544
    const-string v1, "clientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 547
    const-string v1, "artistMetajamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mArtistMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    :cond_4
    const-string v1, "ratingTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mRatingTimestampMicrosec:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    const-string v1, "vid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mVid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    const-string v1, "localCopyStreamFidelity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalCopyStreamFidelity:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    const-string v1, "vUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/MusicFile;->mVThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final tryToInsertMusicFile(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 10
    .param p1, "musicInsert"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const-wide/16 v4, -0x1

    .line 1459
    iget-wide v6, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_0

    .line 1460
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v4, "MusicFile already created. Forgot to call reset()?"

    invoke-direct {v1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1463
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1464
    invoke-static {}, Lcom/google/android/music/store/Store;->generateClientId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/store/MusicFile;->mClientId:Ljava/lang/String;

    .line 1467
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/MusicFile;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1474
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    .line 1475
    .local v2, "insertedId":J
    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 1476
    const-string v1, "MusicStore"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert music file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1482
    .end local v2    # "insertedId":J
    :goto_0
    return-wide v4

    .line 1468
    :catch_0
    move-exception v0

    .line 1469
    .local v0, "e":Lcom/google/android/music/store/InvalidDataException;
    const-string v1, "MusicStore"

    const-string v6, "Failed to insert music file %s. Invalid Data"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1479
    .end local v0    # "e":Lcom/google/android/music/store/InvalidDataException;
    .restart local v2    # "insertedId":J
    :cond_2
    iput-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    .line 1482
    iget-wide v4, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    goto :goto_0
.end method

.method public final tryToUpdateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 5
    .param p1, "fullUpdateStatement"    # Landroid/database/sqlite/SQLiteStatement;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1514
    const/4 v1, 0x0

    .line 1516
    .local v1, "success":Z
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/store/MusicFile;->updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1517
    const/4 v1, 0x1

    .line 1522
    :goto_0
    return v1

    .line 1518
    :catch_0
    move-exception v0

    .line 1519
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "MusicStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to update file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/store/MusicFile;->mSourceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "fullUpdateStatement"    # Landroid/database/sqlite/SQLiteStatement;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1495
    iget-wide v0, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1496
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Music file ID must be known in order to update db record"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1500
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/MusicFile;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 1502
    const/16 v0, 0x3a

    iget-wide v2, p0, Lcom/google/android/music/store/MusicFile;->mLocalId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1504
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 1505
    return-void
.end method
