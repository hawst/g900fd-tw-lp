.class public Lcom/google/android/music/ui/MultiRowClusterView;
.super Landroid/widget/LinearLayout;
.source "MultiRowClusterView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/MultiRowClusterView$Row;
    }
.end annotation


# instance fields
.field private mCardLayoutId:I

.field private final mCards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardView;",
            ">;"
        }
    .end annotation
.end field

.field private mColumnCount:I

.field private mHeaderView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mRowCount:I

.field private final mRows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/MultiRowClusterView$Row;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/MultiRowClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    .line 30
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    .line 42
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mInflater:Landroid/view/LayoutInflater;

    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/ui/MultiRowClusterView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    .line 30
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    .line 48
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mInflater:Landroid/view/LayoutInflater;

    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/MultiRowClusterView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method private buildViews()V
    .locals 9

    .prologue
    .line 136
    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    if-nez v6, :cond_3

    .line 137
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 138
    .local v5, "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->clearThumbnail()V

    goto :goto_0

    .line 140
    .end local v5    # "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 166
    :cond_2
    return-void

    .line 143
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/MultiRowClusterView;->removeAllViews()V

    .line 144
    iget-object v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mHeaderView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/MultiRowClusterView;->addView(Landroid/view/View;)V

    .line 145
    iget-object v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/ui/MultiRowClusterView$Row;

    .line 146
    .local v4, "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    invoke-virtual {v4}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->removeAllViews()V

    goto :goto_1

    .line 149
    .end local v4    # "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    :cond_4
    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    iget-object v7, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v6, v7, :cond_5

    .line 150
    iget-object v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    iget v7, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    iget-object v8, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 152
    :cond_5
    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    iget v7, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    mul-int v0, v6, v7

    .line 153
    .local v0, "cardCount":I
    iget-object v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_6

    .line 154
    iget-object v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v0, v7}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 157
    :cond_6
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    if-ge v1, v6, :cond_2

    .line 158
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/MultiRowClusterView;->getRowView(I)Lcom/google/android/music/ui/MultiRowClusterView$Row;

    move-result-object v4

    .line 159
    .restart local v4    # "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    invoke-virtual {v4, v6}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->reset(I)V

    .line 160
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    if-ge v3, v6, :cond_7

    .line 161
    iget v6, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    mul-int/2addr v6, v1

    add-int/2addr v6, v3

    invoke-direct {p0, v6}, Lcom/google/android/music/ui/MultiRowClusterView;->getCardView(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-result-object v5

    .line 162
    .restart local v5    # "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v4, v5, v3}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->putCard(Lcom/google/android/music/ui/cardlib/layout/PlayCardView;I)V

    .line 160
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 164
    .end local v5    # "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_7
    invoke-virtual {p0, v4}, Lcom/google/android/music/ui/MultiRowClusterView;->addView(Landroid/view/View;)V

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private getCardView(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 169
    iget-object v2, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt p1, v2, :cond_0

    .line 170
    iget-object v2, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mInflater:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCardLayoutId:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 171
    .local v1, "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 173
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    iget-object v2, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    .end local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object v1, v2

    goto :goto_0
.end method

.method private getRowView(I)Lcom/google/android/music/ui/MultiRowClusterView$Row;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 182
    iget-object v1, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 183
    new-instance v0, Lcom/google/android/music/ui/MultiRowClusterView$Row;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MultiRowClusterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/MultiRowClusterView$Row;-><init>(Landroid/content/Context;)V

    .line 184
    .local v0, "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    iget-object v1, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    .end local v0    # "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/MultiRowClusterView$Row;

    move-object v0, v1

    goto :goto_0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    .line 53
    sget-object v1, Lcom/google/android/music/R$styleable;->MultiRowClusterView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 55
    .local v0, "array":Landroid/content/res/TypedArray;
    const v1, 0x7f04009b

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCardLayoutId:I

    .line 61
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    .line 62
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    .line 63
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 64
    return-void
.end method


# virtual methods
.method public getCard(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/common/base/Preconditions;->checkPositionIndex(II)I

    .line 85
    iget-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    return-object v0
.end method

.method public getCardCount()I
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    iget v1, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    mul-int/2addr v0, v1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 69
    const v0, 0x7f0e01ed

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MultiRowClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mHeaderView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    .line 70
    invoke-direct {p0}, Lcom/google/android/music/ui/MultiRowClusterView;->buildViews()V

    .line 71
    return-void
.end method

.method public setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "titleMain"    # Ljava/lang/String;
    .param p2, "titleSecondary"    # Ljava/lang/String;
    .param p3, "more"    # Ljava/lang/String;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mHeaderView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mHeaderView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V

    .line 130
    return-void
.end method

.method public setShownCardCount(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    const/4 v5, 0x0

    .line 96
    iget v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    div-int v3, p1, v4

    .line 97
    .local v3, "rows":I
    iget v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mColumnCount:I

    rem-int v4, p1, v4

    if-lez v4, :cond_2

    const/4 v0, 0x1

    .line 98
    .local v0, "hasNextRow":Z
    :goto_0
    if-eqz v0, :cond_0

    add-int/lit8 v3, v3, 0x1

    .line 99
    :cond_0
    iget v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    if-le v3, v4, :cond_1

    iget v3, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    .line 100
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_3

    .line 101
    iget-object v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/MultiRowClusterView$Row;

    .line 102
    .local v2, "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    invoke-virtual {v2, v5}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->setVisibility(I)V

    .line 100
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "hasNextRow":Z
    .end local v1    # "i":I
    .end local v2    # "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    :cond_2
    move v0, v5

    .line 97
    goto :goto_0

    .line 104
    .restart local v0    # "hasNextRow":Z
    .restart local v1    # "i":I
    :cond_3
    move v1, v3

    :goto_2
    iget v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRowCount:I

    if-ge v1, v4, :cond_4

    .line 105
    iget-object v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mRows:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/MultiRowClusterView$Row;

    .line 106
    .restart local v2    # "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->setVisibility(I)V

    .line 104
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 108
    .end local v2    # "row":Lcom/google/android/music/ui/MultiRowClusterView$Row;
    :cond_4
    const/4 v1, 0x0

    :goto_3
    if-ge v1, p1, :cond_5

    iget-object v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 109
    iget-object v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    invoke-virtual {v4, v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 111
    :cond_5
    move v1, p1

    :goto_4
    iget-object v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    .line 112
    iget-object v4, p0, Lcom/google/android/music/ui/MultiRowClusterView;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 114
    :cond_6
    return-void
.end method
