.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;
.super Landroid/support/v17/leanback/widget/ListRow;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ExploreGenresListRow"
.end annotation


# instance fields
.field private final mFeaturedAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field private final mFeaturedLoaderId:I

.field private final mGenreId:Ljava/lang/String;

.field private final mNewReleasesAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

.field private final mNewReleasesLoaderId:I


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/support/v17/leanback/widget/CursorObjectAdapter;IILjava/lang/String;)V
    .locals 0
    .param p1, "header"    # Landroid/support/v17/leanback/widget/HeaderItem;
    .param p2, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p3, "featuredAdapter"    # Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .param p4, "newReleasesAdapter"    # Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .param p5, "featuredLoaderId"    # I
    .param p6, "newReleasesLoaderId"    # I
    .param p7, "genreId"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 48
    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mFeaturedAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    .line 49
    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mNewReleasesAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    .line 50
    iput p5, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mFeaturedLoaderId:I

    .line 51
    iput p6, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mNewReleasesLoaderId:I

    .line 52
    iput-object p7, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mGenreId:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method getFeaturedAdapter()Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mFeaturedAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    return-object v0
.end method

.method getFeaturedLoaderId()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mFeaturedLoaderId:I

    return v0
.end method

.method getGenreId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mGenreId:Ljava/lang/String;

    return-object v0
.end method

.method getNewReleasesAdapter()Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mNewReleasesAdapter:Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    return-object v0
.end method

.method getNewReleasesLoaderId()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->mNewReleasesLoaderId:I

    return v0
.end method
