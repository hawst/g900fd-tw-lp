.class Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackExploreGroupsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0
    .param p2, "x0"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;

    invoke-direct {p0, p2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 37
    const/4 v6, 0x0

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 39
    .local v0, "groupId":J
    const/4 v6, 0x1

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, "name":Ljava/lang/String;
    const/4 v6, 0x4

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 42
    .local v2, "groupType":I
    new-instance v5, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1$1;

    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;

    invoke-virtual {v6}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v6

    invoke-direct {v5, p0, v6, v2}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1$1;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;Landroid/support/v17/leanback/widget/Presenter;I)V

    .line 51
    .local v5, "topChartsAdapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getClusterProjection(I)[Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "projection":[Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 53
    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;

    invoke-virtual {v8, v0, v1}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->getGroupItemsUri(J)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v7, v5, v8, v4}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 56
    :cond_0
    invoke-static {v3, v5}, Lcom/google/android/music/leanback/LeanbackItemActivity;->createListRow(Ljava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v6

    return-object v6
.end method
