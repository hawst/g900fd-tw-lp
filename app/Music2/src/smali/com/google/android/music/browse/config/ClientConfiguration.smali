.class public Lcom/google/android/music/browse/config/ClientConfiguration;
.super Ljava/lang/Object;
.source "ClientConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mPackage:Ljava/lang/String;

.field private mRoot:Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/music/browse/config/ClientConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/browse/config/ClientConfiguration;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method


# virtual methods
.method public getBrowserRoot()Landroid/service/media/MediaBrowserService$BrowserRoot;
    .locals 3

    .prologue
    .line 26
    new-instance v0, Landroid/service/media/MediaBrowserService$BrowserRoot;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mPackage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mRoot:Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    invoke-virtual {v2}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/service/media/MediaBrowserService$BrowserRoot;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public getItemById(Lcom/google/android/music/browse/BrowsableMediaId;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    .locals 5
    .param p1, "id"    # Lcom/google/android/music/browse/BrowsableMediaId;

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-virtual {p1}, Lcom/google/android/music/browse/BrowsableMediaId;->getPackage()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mPackage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 47
    sget-object v3, Lcom/google/android/music/browse/config/ClientConfiguration;->TAG:Ljava/lang/String;

    const-string v4, "Loading item from wrong package!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    :goto_0
    return-object v2

    .line 51
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/browse/BrowsableMediaId;->getTokens()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 52
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 53
    iget-object v2, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mRoot:Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    goto :goto_0

    .line 56
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 57
    .local v0, "child":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mRoot:Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    invoke-virtual {v3}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 58
    iget-object v2, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mRoot:Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    invoke-virtual {v2, v1}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getItemById(Ljava/util/Iterator;)Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    move-result-object v2

    goto :goto_0
.end method

.method public getPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mPackage:Ljava/lang/String;

    return-object v0
.end method

.method public setPackage(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 35
    if-nez p1, :cond_0

    .line 36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pkg cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mPackage:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setRoot(Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;)V
    .locals 0
    .param p1, "item"    # Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/music/browse/config/ClientConfiguration;->mRoot:Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    .line 43
    return-void
.end method
