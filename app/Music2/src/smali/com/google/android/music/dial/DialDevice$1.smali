.class Lcom/google/android/music/dial/DialDevice$1;
.super Ljava/lang/Object;
.source "DialDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialDevice;->setup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialDevice;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialDevice;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice$1;->this$0:Lcom/google/android/music/dial/DialDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 166
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$1;->this$0:Lcom/google/android/music/dial/DialDevice;

    # invokes: Lcom/google/android/music/dial/DialDevice;->getSocketInternal()Lcom/google/android/music/dial/WebSocket;
    invoke-static {v1}, Lcom/google/android/music/dial/DialDevice;->access$000(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;

    move-result-object v0

    .line 167
    .local v0, "socket":Lcom/google/android/music/dial/WebSocket;
    if-nez v0, :cond_0

    .line 168
    const-string v1, "DialDevice"

    const-string v2, "Socket connection failed for %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/dial/DialDevice$1;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-static {v5}, Lcom/google/android/music/dial/DialDevice;->access$100(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$1;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v2, p0, Lcom/google/android/music/dial/DialDevice$1;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/dial/DialDevice;->access$200(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/dial/DialDevice$Listener$Error;->SOCKET_CONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

    # invokes: Lcom/google/android/music/dial/DialDevice;->callListenersOnError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/music/dial/DialDevice;->access$300(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V

    .line 172
    :cond_0
    return-void
.end method
