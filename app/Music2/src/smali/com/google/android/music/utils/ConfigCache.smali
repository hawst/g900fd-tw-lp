.class public Lcom/google/android/music/utils/ConfigCache;
.super Ljava/lang/Object;
.source "ConfigCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;
    }
.end annotation


# instance fields
.field private final mCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCacheVersion:J

.field private volatile mContentResolver:Landroid/content/ContentResolver;

.field private final mLock:Ljava/lang/Object;

.field private final mObserver:Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;

.field private final mType:I


# direct methods
.method constructor <init>(ILandroid/os/Handler;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCacheVersion:J

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;

    .line 51
    iput p1, p0, Lcom/google/android/music/utils/ConfigCache;->mType:I

    .line 52
    new-instance v0, Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;

    invoke-direct {v0, p0, p2}, Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;-><init>(Lcom/google/android/music/utils/ConfigCache;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mObserver:Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/utils/ConfigCache;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/ConfigCache;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/utils/ConfigCache;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/ConfigCache;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$208(Lcom/google/android/music/utils/ConfigCache;)J
    .locals 4
    .param p0, "x0"    # Lcom/google/android/music/utils/ConfigCache;

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCacheVersion:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/music/utils/ConfigCache;->mCacheVersion:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/music/utils/ConfigCache;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/ConfigCache;

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/music/utils/ConfigCache;->mType:I

    return v0
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 127
    iget-object v2, p0, Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 128
    :try_start_0
    iget-wide v6, p0, Lcom/google/android/music/utils/ConfigCache;->mCacheVersion:J

    .line 129
    .local v6, "cacheVersion":J
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 131
    .local v9, "value":Ljava/lang/String;
    monitor-exit v2

    .line 161
    .end local v9    # "value":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 133
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    iget v0, p0, Lcom/google/android/music/utils/ConfigCache;->mType:I

    invoke-static {v0, p1}, Lcom/google/android/music/store/ConfigContent;->getConfigSettingUri(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 136
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "Value"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 138
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 139
    iget-object v2, p0, Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 140
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;

    const/4 v4, 0x0

    invoke-virtual {v0, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v9, v3

    .line 142
    goto :goto_0

    .line 133
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "cacheVersion":J
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 141
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "cacheVersion":J
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 146
    :cond_1
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 148
    :try_start_5
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;

    const/4 v4, 0x0

    invoke-virtual {v0, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 161
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v9, v3

    goto :goto_0

    .line 149
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 161
    :catchall_3
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 152
    :cond_3
    const/4 v0, 0x0

    :try_start_8
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 153
    .restart local v9    # "value":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 155
    :try_start_9
    iget-wide v4, p0, Lcom/google/android/music/utils/ConfigCache;->mCacheVersion:J

    cmp-long v0, v6, v4

    if-nez v0, :cond_4

    .line 156
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    :cond_4
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 161
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 158
    :catchall_4
    move-exception v0

    :try_start_a
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3
.end method

.method public init(Landroid/content/ContentResolver;)V
    .locals 5
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 60
    if-nez p1, :cond_0

    .line 61
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Missing ContentResolver"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 63
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 64
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/utils/ConfigCache;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v1, :cond_1

    .line 65
    iput-object p1, p0, Lcom/google/android/music/utils/ConfigCache;->mContentResolver:Landroid/content/ContentResolver;

    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "uri":Landroid/net/Uri;
    iget v1, p0, Lcom/google/android/music/utils/ConfigCache;->mType:I

    packed-switch v1, :pswitch_data_0

    .line 75
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unhandled type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/music/utils/ConfigCache;->mType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 80
    .end local v0    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 69
    .restart local v0    # "uri":Landroid/net/Uri;
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    .line 78
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/utils/ConfigCache;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/utils/ConfigCache;->mObserver:Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;

    invoke-virtual {v1, v0, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 80
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_1
    monitor-exit v2

    .line 81
    return-void

    .line 72
    .restart local v0    # "uri":Landroid/net/Uri;
    :pswitch_1
    sget-object v0, Lcom/google/android/music/store/ConfigContent;->APP_SETTINGS_URI:Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
