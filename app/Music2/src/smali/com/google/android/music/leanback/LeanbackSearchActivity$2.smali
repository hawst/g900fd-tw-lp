.class Lcom/google/android/music/leanback/LeanbackSearchActivity$2;
.super Ljava/lang/Object;
.source "LeanbackSearchActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/MusicUtils$QueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackSearchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryComplete(Landroid/database/Cursor;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 276
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 277
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 278
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackSearchActivity;->extractDataForBestMatch(Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;
    invoke-static {v2, p1}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->access$800(Lcom/google/android/music/leanback/LeanbackSearchActivity;Landroid/database/Cursor;)Lcom/google/android/music/leanback/Item;

    move-result-object v1

    .line 279
    .local v1, "item":Lcom/google/android/music/leanback/Item;
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getIntentGetter()Lcom/google/android/music/leanback/Item$IntentGetter;

    move-result-object v0

    .line 280
    .local v0, "intentGetter":Lcom/google/android/music/leanback/Item$IntentGetter;
    if-eqz v0, :cond_0

    .line 281
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-interface {v0}, Lcom/google/android/music/leanback/Item$IntentGetter;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 290
    .end local v0    # "intentGetter":Lcom/google/android/music/leanback/Item$IntentGetter;
    .end local v1    # "item":Lcom/google/android/music/leanback/Item;
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->finish()V

    .line 291
    return-void

    .line 283
    .restart local v0    # "intentGetter":Lcom/google/android/music/leanback/Item$IntentGetter;
    .restart local v1    # "item":Lcom/google/android/music/leanback/Item;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->val$intent:Landroid/content/Intent;

    const-string v3, "android.intent.action.SEARCH"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->val$intent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 287
    .end local v0    # "intentGetter":Lcom/google/android/music/leanback/Item$IntentGetter;
    .end local v1    # "item":Lcom/google/android/music/leanback/Item;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->val$intent:Landroid/content/Intent;

    const-string v3, "android.intent.action.SEARCH"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackSearchActivity;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackSearchActivity$2;->val$intent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/LeanbackSearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
