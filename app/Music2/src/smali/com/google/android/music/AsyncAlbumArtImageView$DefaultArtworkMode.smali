.class Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultArtworkMode"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 832
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;

    .prologue
    .line 834
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 835
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 859
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 841
    const/4 v0, 0x0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 851
    instance-of v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;

    if-nez v0, :cond_0

    .line 852
    const/4 v0, 0x0

    .line 854
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 846
    const-string v0, "DefaultArtworkMode<>"

    return-object v0
.end method
