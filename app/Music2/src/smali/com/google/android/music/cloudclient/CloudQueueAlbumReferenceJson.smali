.class public Lcom/google/android/music/cloudclient/CloudQueueAlbumReferenceJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueueAlbumReferenceJson.java"


# instance fields
.field public mArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist"
    .end annotation
.end field

.field public mMetajamCompactKey:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "metajamCompactKey"
    .end annotation
.end field

.field public mTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
