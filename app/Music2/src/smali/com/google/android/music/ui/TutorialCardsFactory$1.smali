.class final Lcom/google/android/music/ui/TutorialCardsFactory$1;
.super Ljava/lang/Object;
.source "TutorialCardsFactory.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainStageWelcomeCardWithImage(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$card:Landroid/widget/LinearLayout;

.field final synthetic val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

.field final synthetic val$fragment:Lcom/google/android/music/ui/MusicFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$card:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 223
    # invokes: Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->access$000()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$card:Landroid/widget/LinearLayout;

    const-string v2, "Mainstage"

    iget-object v3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    # invokes: Lcom/google/android/music/ui/TutorialCardsFactory;->dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/ui/TutorialCardsFactory;->access$100(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    .line 225
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isConciergeListenNowEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$card:Landroid/widget/LinearLayout;

    const-string v2, "MainstageSituations"

    iget-object v3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    # invokes: Lcom/google/android/music/ui/TutorialCardsFactory;->dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/ui/TutorialCardsFactory;->access$100(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$fragment:Lcom/google/android/music/ui/MusicFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$card:Landroid/widget/LinearLayout;

    const-string v2, "MainstageBasic"

    iget-object v3, p0, Lcom/google/android/music/ui/TutorialCardsFactory$1;->val$dismissHandler:Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    # invokes: Lcom/google/android/music/ui/TutorialCardsFactory;->dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/ui/TutorialCardsFactory;->access$100(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    goto :goto_0
.end method
