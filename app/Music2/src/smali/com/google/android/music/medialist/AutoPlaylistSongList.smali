.class public abstract Lcom/google/android/music/medialist/AutoPlaylistSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "AutoPlaylistSongList.java"


# static fields
.field private static final LOGV:Z

.field private static final TAG:Ljava/lang/String; = "AutoPlaylist"


# instance fields
.field private final mId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->LOGV:Z

    return-void
.end method

.method protected constructor <init>(IJ)V
    .locals 2
    .param p1, "sortOrder"    # I
    .param p2, "id"    # J

    .prologue
    .line 78
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 79
    iput-wide p2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    .line 80
    return-void
.end method

.method protected constructor <init>(IJZ)V
    .locals 2
    .param p1, "sortOrder"    # I
    .param p2, "id"    # J
    .param p4, "includeExternal"    # Z

    .prologue
    .line 83
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p4}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 84
    iput-wide p2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    .line 85
    return-void
.end method

.method protected constructor <init>(IJZZ)V
    .locals 0
    .param p1, "sortOrder"    # I
    .param p2, "id"    # J
    .param p4, "includeExternal"    # Z
    .param p5, "shouldFilter"    # Z

    .prologue
    .line 102
    invoke-direct {p0, p1, p5, p4}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 103
    iput-wide p2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    .line 104
    return-void
.end method

.method protected constructor <init>(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 73
    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 74
    iput-wide p1, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    .line 75
    return-void
.end method

.method private containsItems(Landroid/content/Context;[Ljava/lang/String;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cols"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 216
    iget-wide v0, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->getAutoPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 220
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    :cond_0
    const-string v0, "AutoPlaylist"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown playlist id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    :goto_0
    return v0

    .line 224
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ne v0, v7, :cond_2

    move v0, v7

    .line 227
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v0, v8

    .line 224
    goto :goto_1

    .line 227
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static final getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;
    .locals 4
    .param p0, "autoPlaylistId"    # J
    .param p2, "preferenceSort"    # Z
    .param p3, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    const/4 v0, -0x1

    .line 58
    const-wide/16 v2, -0x1

    cmp-long v1, p0, v2

    if-nez v1, :cond_1

    .line 59
    new-instance v1, Lcom/google/android/music/medialist/RecentlyAddedSongList;

    if-eqz p2, :cond_0

    invoke-virtual {p3}, Lcom/google/android/music/preferences/MusicPreferences;->getRecentlyAddedSongsSortOrder()I

    move-result v0

    :cond_0
    invoke-direct {v1, v0}, Lcom/google/android/music/medialist/RecentlyAddedSongList;-><init>(I)V

    move-object v0, v1

    .line 66
    :goto_0
    return-object v0

    .line 61
    :cond_1
    const-wide/16 v2, -0x2

    cmp-long v1, p0, v2

    if-nez v1, :cond_3

    .line 62
    new-instance v1, Lcom/google/android/music/medialist/AllSongsList;

    if-eqz p2, :cond_2

    invoke-virtual {p3}, Lcom/google/android/music/preferences/MusicPreferences;->getAllSongsSortOrder()I

    move-result v0

    :cond_2
    invoke-direct {v1, v0}, Lcom/google/android/music/medialist/AllSongsList;-><init>(I)V

    move-object v0, v1

    goto :goto_0

    .line 63
    :cond_3
    const-wide/16 v2, -0x3

    cmp-long v1, p0, v2

    if-nez v1, :cond_5

    .line 64
    new-instance v1, Lcom/google/android/music/medialist/StoreSongList;

    if-eqz p2, :cond_4

    invoke-virtual {p3}, Lcom/google/android/music/preferences/MusicPreferences;->getStoreSongsSortOrder()I

    move-result v0

    :cond_4
    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/google/android/music/medialist/StoreSongList;-><init>(IZ)V

    move-object v0, v1

    goto :goto_0

    .line 65
    :cond_5
    const-wide/16 v2, -0x4

    cmp-long v1, p0, v2

    if-nez v1, :cond_7

    .line 66
    new-instance v1, Lcom/google/android/music/medialist/ThumbsUpSongList;

    if-eqz p2, :cond_6

    invoke-virtual {p3}, Lcom/google/android/music/preferences/MusicPreferences;->getThumbsUpSongsSortOrder()I

    move-result v0

    :cond_6
    invoke-direct {v1, v0}, Lcom/google/android/music/medialist/ThumbsUpSongList;-><init>(I)V

    move-object v0, v1

    goto :goto_0

    .line 68
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected auto-playlist id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 206
    sget-boolean v1, Lcom/google/android/music/medialist/AutoPlaylistSongList;->LOGV:Z

    if-eqz v1, :cond_0

    .line 207
    const-string v1, "AutoPlaylist"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "containsLocalItems: mId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    .line 212
    .local v0, "cols":[Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->containsItems(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 195
    sget-boolean v1, Lcom/google/android/music/medialist/AutoPlaylistSongList;->LOGV:Z

    if-eqz v1, :cond_0

    .line 196
    const-string v1, "AutoPlaylist"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "containsRemoteItems: mId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "hasRemote"

    aput-object v2, v0, v1

    .line 201
    .local v0, "cols":[Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->containsItems(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 167
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getSortOrder()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "name":Ljava/lang/String;
    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    const-wide/16 v4, -0x3

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 131
    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->STORE_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 141
    .local v1, "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/music/store/ContainerDescriptor;->newAutoPlaylistDescriptor(Lcom/google/android/music/store/ContainerDescriptor$Type;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    return-object v2

    .line 132
    .end local v1    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    const-wide/16 v4, -0x4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 133
    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->THUMBS_UP_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .restart local v1    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    goto :goto_0

    .line 134
    .end local v1    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    :cond_1
    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 135
    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->RECENTLY_ADDED_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .restart local v1    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    goto :goto_0

    .line 136
    .end local v1    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    :cond_2
    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    const-wide/16 v4, -0x2

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 137
    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALL_SONGS_MY_LIBRARY:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .restart local v1    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    goto :goto_0

    .line 139
    .end local v1    # "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    :cond_3
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported auto playlist: id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 182
    iget-wide v0, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {p0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getSortParam()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContent$AutoPlaylists$Members;->getAutoPlaylistItemsUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadedSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 233
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponDownloadedSongCount"

    aput-object v0, v2, v1

    .line 236
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->getAutoPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 239
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 240
    :cond_0
    const-string v0, "AutoPlaylist"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown auto playlist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    const/4 v0, -0x1

    .line 246
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 243
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 246
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    return-wide v0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 173
    const/4 v2, 0x4

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getListingName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->createAlbumIdIteratorFactoryForContentUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;

    move-result-object v10

    move-object v1, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v11, p4

    move/from16 v12, p5

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getKeepOnSongCount(Landroid/content/Context;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 252
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "keeponSongCount"

    aput-object v0, v2, v1

    .line 255
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->getAutoPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 258
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    :cond_0
    const-string v0, "AutoPlaylist"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown auto playlist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    const/4 v0, -0x1

    .line 265
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 262
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 265
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getListingName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getListingNameResourceId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getListingNameResourceId()I
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getTitleResourceId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x64

    invoke-static {p1, v0, v1}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistPrimaryLabel(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 147
    .local v1, "prefsHolder":Ljava/lang/Object;
    invoke-static {p1, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 149
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/16 v2, 0x64

    :try_start_0
    invoke-static {p1, v0, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 152
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method protected abstract getTitleResourceId()I
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasArtistArt()Z
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x1

    return v0
.end method

.method public hasMetaData()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public isAllLocal(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 271
    sget-boolean v0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->LOGV:Z

    if-eqz v0, :cond_0

    .line 272
    const-string v0, "AutoPlaylist"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "containsLocalItems: mId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/AvailableSpaceTracker;Lcom/google/android/music/store/IStoreService;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "offlineMusicManager"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p3, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 310
    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {p2, v2, v3}, Lcom/google/android/music/AvailableSpaceTracker;->isAutoPlaylistSelected(J)Ljava/lang/Boolean;

    move-result-object v0

    .line 311
    .local v0, "result":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 314
    invoke-virtual {p0, p1, p3}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z

    move-result v1

    .line 319
    :goto_0
    return v1

    .line 316
    :cond_0
    sget-boolean v1, Lcom/google/android/music/medialist/AutoPlaylistSongList;->LOGV:Z

    if-eqz v1, :cond_1

    .line 317
    const-string v1, "AutoPlaylist"

    const-string v2, "isSelectedForOfflineCaching: id=%d result=%b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 326
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-interface {p2, v2, v3}, Lcom/google/android/music/store/IStoreService;->isAutoPlaylistSelectedAsKeepOn(J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 329
    :goto_0
    return v1

    .line 327
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AutoPlaylist"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error trying to get offline status for playlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 329
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public refreshMetaData(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 299
    return-void
.end method

.method public registerMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 303
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 305
    return-void
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for AutoPlayLists"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected supportsOfflineCaching()Z
    .locals 4

    .prologue
    .line 187
    sget-boolean v0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->LOGV:Z

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "AutoPlaylist"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportsOfflineCaching: mId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keepOnManager"    # Lcom/google/android/music/activitymanagement/KeepOnManager;
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 284
    iget-wide v2, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mId:J

    move-object v1, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleAutoPlaylistKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V

    .line 285
    return-void
.end method
