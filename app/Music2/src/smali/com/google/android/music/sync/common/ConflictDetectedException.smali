.class public Lcom/google/android/music/sync/common/ConflictDetectedException;
.super Ljava/lang/Exception;
.source "ConflictDetectedException.java"


# instance fields
.field private mCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 12
    return-void
.end method


# virtual methods
.method public getConflictCount()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/music/sync/common/ConflictDetectedException;->mCount:I

    return v0
.end method

.method public setConflictCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/music/sync/common/ConflictDetectedException;->mCount:I

    .line 28
    return-void
.end method
