.class public Lcom/google/android/music/ui/songza/SubSituationFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "SubSituationFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/songza/SubSituationFragment$SubSituationTitleViewHolder;,
        Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;,
        Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;,
        Lcom/google/android/music/ui/songza/SubSituationFragment$ListConfigurator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/BaseFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field public static final RADIO_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;

.field private final mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

.field private mExpandedRadioIndices:[Z

.field private mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private mRootView:Landroid/view/View;

.field private mSubSituationArtUrl:Ljava/lang/String;

.field private mSubSituationArtView:Lcom/google/android/music/art/SimpleArtView;

.field private mSubSituationId:Ljava/lang/String;

.field private mSubSituationTitle:Ljava/lang/String;

.field private mSubSituationTitleView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "radio_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "radio_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "radio_description"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "radio_art"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "radio_seed_source_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "radio_seed_source_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "radio_source_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/songza/SubSituationFragment;->RADIO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 67
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    .line 70
    iput-object v1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationId:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitle:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationArtUrl:Ljava/lang/String;

    .line 380
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/songza/SubSituationFragment;)Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/songza/SubSituationFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/songza/SubSituationFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/songza/SubSituationFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitleView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/songza/SubSituationFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/songza/SubSituationFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitleView:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/songza/SubSituationFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/songza/SubSituationFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/songza/SubSituationFragment;)[Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/songza/SubSituationFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mExpandedRadioIndices:[Z

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/songza/SubSituationFragment;
    .locals 3
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v1, Lcom/google/android/music/ui/songza/SubSituationFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/songza/SubSituationFragment;-><init>()V

    .line 94
    .local v1, "fragment":Lcom/google/android/music/ui/songza/SubSituationFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 95
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "subSituationId"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v2, "subSituationTitle"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v2, "subSituationArtUrl"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 99
    return-object v1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    .line 143
    .local v0, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 144
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "i"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 156
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$SituationRadios;->getRadioStationsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/songza/SubSituationFragment;->RADIO_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    .line 104
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 105
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 106
    const-string v3, "subSituationId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationId:Ljava/lang/String;

    .line 107
    const-string v3, "subSituationTitle"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitle:Ljava/lang/String;

    .line 108
    const-string v3, "subSituationArtUrl"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationArtUrl:Ljava/lang/String;

    .line 110
    :cond_0
    const v3, 0x7f0400dd

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRootView:Landroid/view/View;

    .line 111
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRootView:Landroid/view/View;

    const v4, 0x7f0e019d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    .line 113
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    new-instance v4, Lcom/google/android/music/ui/songza/SubSituationFragment$ListConfigurator;

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/ui/songza/SubSituationFragment$ListConfigurator;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->configureForMusic(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 116
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRootView:Landroid/view/View;

    const v4, 0x7f0e010d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 119
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v7}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 120
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/RecyclerView$ItemAnimator;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/support/v7/widget/RecyclerView$ItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 122
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRootView:Landroid/view/View;

    const v4, 0x7f0e0265

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/art/SimpleArtView;

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationArtView:Lcom/google/android/music/art/SimpleArtView;

    .line 123
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationArtUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 124
    new-instance v2, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationArtUrl:Ljava/lang/String;

    invoke-direct {v2, v3, v7}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 125
    .local v2, "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    new-instance v1, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    sget-object v3, Lcom/google/android/music/art/ArtType;->SONGZA_SITUATION:Lcom/google/android/music/art/ArtType;

    const/4 v4, 0x3

    const/high16 v5, 0x3f100000    # 0.5625f

    invoke-direct {v1, v3, v4, v5, v2}, Lcom/google/android/music/art/SingleUrlArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;IFLcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 127
    .local v1, "artDescriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationArtView:Lcom/google/android/music/art/SimpleArtView;

    invoke-virtual {v3, v1, v7}, Lcom/google/android/music/art/SimpleArtView;->bind(Lcom/google/android/music/art/ArtDescriptor;Z)V

    .line 132
    .end local v1    # "artDescriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    .end local v2    # "url":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :goto_0
    new-instance v3, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;

    invoke-direct {v3, p0, p0}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;-><init>(Lcom/google/android/music/ui/songza/SubSituationFragment;Lcom/google/android/music/ui/songza/SubSituationFragment;)V

    iput-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;

    .line 133
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 135
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mRootView:Landroid/view/View;

    return-object v3

    .line 129
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationArtView:Lcom/google/android/music/art/SimpleArtView;

    const v4, 0x7f02017c

    invoke-virtual {v3, v4}, Lcom/google/android/music/art/SimpleArtView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mPlayHeaderListLayout:Lcom/google/android/music/ui/MusicPlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->unregisterSharedPreferenceChangeListener()V

    .line 151
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 152
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    if-eqz p2, :cond_0

    .line 164
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mExpandedRadioIndices:[Z

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->swapCursor(Landroid/database/Cursor;)V

    .line 167
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 45
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/songza/SubSituationFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment;->mAdapter:Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->swapCursor(Landroid/database/Cursor;)V

    .line 172
    return-void
.end method
