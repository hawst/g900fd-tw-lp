.class public Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
.super Ljava/lang/Object;
.source "NautilusContentProviderHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/NautilusContentProviderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NautilusQueryResult"
.end annotation


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private final mExcludeIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocalId:J


# direct methods
.method public constructor <init>(Landroid/database/Cursor;JLjava/util/List;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "localId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p4, "excludeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->mCursor:Landroid/database/Cursor;

    .line 61
    iput-wide p2, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->mLocalId:J

    .line 62
    iput-object p4, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->mExcludeIds:Ljava/util/List;

    .line 63
    return-void
.end method


# virtual methods
.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getExcludeClause(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->mExcludeIds:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/android/music/utils/DbUtils;->getNotInClause(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->mLocalId:J

    return-wide v0
.end method
