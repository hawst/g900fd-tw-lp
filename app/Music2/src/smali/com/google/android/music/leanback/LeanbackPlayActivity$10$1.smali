.class Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->getItem(Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity$10;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$10;)V
    .locals 0

    .prologue
    .line 937
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity$10;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 947
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 948
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;->mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/music/xdi/TrackAdapter;->populateDocumentFromCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)V

    .line 949
    return-object v0
.end method

.method protected bindColumns(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 942
    new-instance v0, Lcom/google/android/music/xdi/TrackAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity$10;

    iget-object v1, v1, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$leanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity$10;

    iget-object v2, v2, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;->val$songList:Lcom/google/android/music/medialist/SongList;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/music/xdi/TrackAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10$1;->mTrackAdapter:Lcom/google/android/music/xdi/TrackAdapter;

    .line 943
    return-void
.end method
