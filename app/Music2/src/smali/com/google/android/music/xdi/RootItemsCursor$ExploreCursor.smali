.class Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;
.super Landroid/database/MatrixCursor;
.source "RootItemsCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/xdi/RootItemsCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExploreCursor"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 261
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 263
    new-instance v2, Lcom/google/android/music/xdi/ProjectionMap;

    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    .line 266
    .local v2, "projectionMap":Lcom/google/android/music/xdi/ProjectionMap;
    const/4 v3, 0x0

    .line 267
    .local v3, "id":I
    add-int/lit8 v12, v3, 0x1

    .end local v3    # "id":I
    .local v12, "id":I
    const v4, 0x7f0b00a6

    const-string v5, "explore/featured"

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;->addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;IILjava/lang/String;ZZ)V

    .line 270
    add-int/lit8 v3, v12, 0x1

    .end local v12    # "id":I
    .restart local v3    # "id":I
    const v8, 0x7f0b00a4

    const-string v9, "explore/recommendations"

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object v4, p0

    move-object v5, p1

    move-object v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v11}, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;->addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;IILjava/lang/String;ZZ)V

    .line 273
    add-int/lit8 v12, v3, 0x1

    .end local v3    # "id":I
    .restart local v12    # "id":I
    const v4, 0x7f0b00a5

    const-string v5, "explore/newreleases/headers"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;->addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;IILjava/lang/String;ZZ)V

    .line 276
    add-int/lit8 v3, v12, 0x1

    .end local v12    # "id":I
    .restart local v3    # "id":I
    const v8, 0x7f0b00a7

    const-string v9, "explore/genres"

    const/4 v10, 0x1

    const/4 v11, 0x1

    move-object v4, p0

    move-object v5, p1

    move-object v6, v2

    move v7, v12

    invoke-direct/range {v4 .. v11}, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;->addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;IILjava/lang/String;ZZ)V

    .line 279
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/music/xdi/RootItemsCursor$1;

    .prologue
    .line 259
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;IILjava/lang/String;ZZ)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "id"    # I
    .param p4, "nameResId"    # I
    .param p5, "path"    # Ljava/lang/String;
    .param p6, "requiresNameSuffix"    # Z
    .param p7, "isBrowseIntent"    # Z

    .prologue
    .line 283
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 286
    .local v3, "values":[Ljava/lang/Object;
    invoke-virtual {p1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "name":Ljava/lang/String;
    if-eqz p6, :cond_0

    .line 290
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, p5}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 298
    .local v2, "uri":Landroid/net/Uri;
    :goto_0
    if-eqz p7, :cond_1

    .line 299
    invoke-static {v2, v1}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntentByName(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 304
    .local v0, "intent":Landroid/content/Intent;
    :goto_1
    const-string v4, "_id"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 305
    const-string v4, "display_name"

    invoke-virtual {p2, v3, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 306
    const-string v4, "image_uri"

    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 308
    const-string v4, "intent_uri"

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 311
    invoke-virtual {p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;->addRow([Ljava/lang/Object;)V

    .line 312
    return-void

    .line 294
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, p5}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .restart local v2    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 301
    :cond_1
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/google/android/music/xdi/XdiContract;->getGridIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v0

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_1
.end method
