.class public abstract Lcom/google/android/music/store/BaseStore;
.super Ljava/lang/Object;
.source "BaseStore.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private lock()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method private unlock()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method


# virtual methods
.method public beginRead()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 25
    const/4 v0, 0x0

    .line 26
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->lock()V

    .line 28
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/music/store/BaseStore;->getDatabase(Z)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 30
    if-nez v0, :cond_0

    .line 31
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->unlock()V

    .line 34
    :cond_0
    return-object v0

    .line 30
    :catchall_0
    move-exception v1

    if-nez v0, :cond_1

    .line 31
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->unlock()V

    :cond_1
    throw v1
.end method

.method public beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 55
    .local v1, "success":Z
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->lock()V

    .line 57
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/google/android/music/store/BaseStore;->getDatabase(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    const/4 v1, 0x1

    .line 61
    if-nez v1, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->unlock()V

    .line 65
    :cond_0
    return-object v0

    .line 61
    :catchall_0
    move-exception v2

    if-nez v1, :cond_1

    .line 62
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->unlock()V

    :cond_1
    throw v2
.end method

.method public endRead(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->unlock()V

    .line 44
    return-void
.end method

.method public endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "success"    # Z

    .prologue
    .line 77
    if-eqz p2, :cond_0

    .line 78
    :try_start_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 80
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->unlock()V

    .line 84
    return-void

    .line 82
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;->unlock()V

    throw v0
.end method

.method protected abstract getDatabase(Z)Landroid/database/sqlite/SQLiteDatabase;
.end method
