.class final Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;
.super Ljava/lang/Object;
.source "PlaybackLogsConsumer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/wear/PlaybackLogsConsumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    }
.end annotation


# instance fields
.field private final collectionId:Ljava/lang/String;

.field private final eventTimeMs:J

.field private final isExplicit:Z

.field private final trackId:J

.field private final type:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;


# direct methods
.method private constructor <init>(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;JJLjava/lang/String;Z)V
    .locals 0
    .param p1, "type"    # Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    .param p2, "trackId"    # J
    .param p4, "eventTimeMs"    # J
    .param p6, "collectionId"    # Ljava/lang/String;
    .param p7, "isExplicit"    # Z

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput-object p1, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->type:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    .line 219
    iput-wide p2, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->trackId:J

    .line 220
    iput-wide p4, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J

    .line 221
    iput-object p6, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->collectionId:Ljava/lang/String;

    .line 222
    iput-boolean p7, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->isExplicit:Z

    .line 223
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->type:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->trackId:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->eventTimeMs:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->collectionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;->isExplicit:Z

    return v0
.end method

.method public static fromDataMap(Lcom/google/android/gms/wearable/DataMap;)Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;
    .locals 11
    .param p0, "dataMap"    # Lcom/google/android/gms/wearable/DataMap;

    .prologue
    .line 191
    const-string v0, "eventType"

    const/4 v9, 0x0

    invoke-virtual {p0, v0, v9}, Lcom/google/android/gms/wearable/DataMap;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 195
    .local v8, "eventTypeInt":I
    packed-switch v8, :pswitch_data_0

    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unsupported event type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :pswitch_0
    sget-object v1, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->COUNT_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    .line 205
    .local v1, "type":Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    :goto_0
    const-string v0, "trackId"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 207
    .local v2, "trackId":J
    const-string v0, "eventTimeMs"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/DataMap;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 209
    .local v4, "eventTimeMs":J
    const-string v0, "collectionId"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 211
    .local v6, "collectionId":Ljava/lang/String;
    const-string v0, "isExplicit"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/DataMap;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 213
    .local v7, "isExplicit":Z
    new-instance v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;-><init>(Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;JJLjava/lang/String;Z)V

    return-object v0

    .line 200
    .end local v1    # "type":Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    .end local v2    # "trackId":J
    .end local v4    # "eventTimeMs":J
    .end local v6    # "collectionId":Ljava/lang/String;
    .end local v7    # "isExplicit":Z
    :pswitch_1
    sget-object v1, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->ANALYTICS_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    .line 201
    .restart local v1    # "type":Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    goto :goto_0

    .line 195
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
