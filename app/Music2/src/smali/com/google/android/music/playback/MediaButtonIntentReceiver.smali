.class public Lcom/google/android/music/playback/MediaButtonIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaButtonIntentReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/MediaButtonIntentReceiver$MediaButtonReceiverHelper;
    }
.end annotation


# static fields
.field public static RELEASE_RECEIVER_LOCK:Ljava/lang/String;

.field private static mDown:Z

.field private static mHandler:Landroid/os/Handler;

.field private static mLastClickTime:J

.field private static mLaunched:Z

.field public static sWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J

    .line 30
    sput-boolean v2, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mDown:Z

    .line 31
    sput-boolean v2, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLaunched:Z

    .line 33
    const-string v0, "releasereceiverlock"

    sput-object v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->RELEASE_RECEIVER_LOCK:Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver$1;

    invoke-direct {v0}, Lcom/google/android/music/playback/MediaButtonIntentReceiver$1;-><init>()V

    sput-object v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 61
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 23
    sget-boolean v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLaunched:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 23
    sput-boolean p0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLaunched:Z

    return p0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 23
    sget-boolean v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mDown:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 23
    sput-boolean p0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mDown:Z

    return p0
.end method

.method static synthetic access$200()J
    .locals 2

    .prologue
    .line 23
    sget-wide v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J

    return-wide v0
.end method

.method static synthetic access$202(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 23
    sput-wide p0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mLastClickTime:J

    return-wide p0
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 53
    const-string v0, "MediaButton"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received: intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-static {p1, p2}, Lcom/google/android/music/playback/MediaButtonIntentReceiver$MediaButtonReceiverHelper;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->isOrderedBroadcast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->abortBroadcast()V

    .line 59
    :cond_0
    return-void
.end method
