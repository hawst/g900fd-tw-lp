.class public Lcom/google/android/music/sync/api/MusicUrl;
.super Lcom/google/api/client/googleapis/GoogleUrl;
.source "MusicUrl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    }
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "encodedPath"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/api/client/googleapis/GoogleUrl;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 51
    .local v2, "locale":Ljava/util/Locale;
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "language":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "country":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 54
    .local v3, "localeString":Ljava/lang/String;
    :goto_0
    const-string v4, "hl"

    invoke-virtual {p0, v4, v3}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    return-void

    .line 53
    .end local v3    # "localeString":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static forAddBlacklistItems()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 377
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 378
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 379
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "listennow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 380
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "add_dismissed_items"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    return-object v0
.end method

.method public static forCloudQueueRequest(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 409
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    invoke-static {p0}, Lcom/google/android/music/sync/api/MusicUrl;->getCloudQueueSyncUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 410
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 411
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "queue"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    const-string v1, "key"

    invoke-static {p0}, Lcom/google/android/music/sync/api/MusicUrl;->getCloudQueueSyncApiKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    return-object v0
.end method

.method public static forCloudQueueSyncRequest(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 417
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    invoke-static {p0}, Lcom/google/android/music/sync/api/MusicUrl;->getCloudQueueSyncUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 418
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 419
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "queue"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "sync"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    const-string v1, "key"

    invoke-static {p0}, Lcom/google/android/music/sync/api/MusicUrl;->getCloudQueueSyncApiKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    return-object v0
.end method

.method public static forConfigEntriesFeed()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 4

    .prologue
    .line 228
    new-instance v1, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v2, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 229
    .local v1, "url":Lcom/google/android/music/sync/api/MusicUrl;
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getCookie()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "cookie":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 231
    const-string v2, "cookie"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :cond_0
    const-string v2, "json"

    iput-object v2, v1, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 234
    iget-object v2, v1, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v3, "config"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    return-object v1
.end method

.method public static forEnableNautilusRequest()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 402
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 403
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "signup"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "applyfoplessoffer"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    return-object v0
.end method

.method public static forEphemeralTop(I)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "updateMin"    # I

    .prologue
    .line 311
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 312
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 313
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "ephemeral"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "top"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    const-string v1, "updated-min"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    return-object v0
.end method

.method public static forGenres(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 192
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 193
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 194
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "explore"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "genres"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    if-eqz p0, :cond_0

    .line 197
    const-string v1, "parent-genre"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    :cond_0
    return-object v0
.end method

.method public static forGetBlacklistItems()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 369
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 370
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 371
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "listennow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "get_dismissed_items"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    return-object v0
.end method

.method public static forGetSharedPlaylistEntries(ILjava/lang/String;J)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "maxEntries"    # I
    .param p1, "continuationToken"    # Ljava/lang/String;
    .param p2, "updateMin"    # J

    .prologue
    .line 256
    const/4 v1, 0x1

    if-ge p0, v1, :cond_0

    .line 257
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid number of max entities requested."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 260
    :cond_0
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 261
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 262
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "plentries"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "shared"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    return-object v0
.end method

.method public static forIsPlaylistShared(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 385
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 386
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 387
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "isplaylistshared"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    const-string v1, "id"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    return-object v0
.end method

.method public static forListenNowRecommendations()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 393
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 394
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 395
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "listennow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "getlistennowitems"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    return-object v0
.end method

.method public static forListenNowSituations()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 426
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 427
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 428
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "listennow"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "situations"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    return-object v0
.end method

.method public static forNautilusAlbum(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "nid"    # Ljava/lang/String;

    .prologue
    .line 277
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 278
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 279
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "fetchalbum"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    const-string v1, "nid"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    const-string v1, "include-tracks"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    const-string v1, "include-description"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    return-object v0
.end method

.method public static forNautilusArtist(Ljava/lang/String;IIZ)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "nid"    # Ljava/lang/String;
    .param p1, "numTopTracks"    # I
    .param p2, "numRelatedArtists"    # I
    .param p3, "includeAlbums"    # Z

    .prologue
    .line 288
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 289
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 290
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "fetchartist"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    const-string v1, "nid"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    const-string v1, "include-albums"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    if-lez p1, :cond_0

    .line 294
    const-string v1, "num-top-tracks"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    :cond_0
    if-lez p2, :cond_1

    .line 297
    const-string v1, "num-related-artists"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    :cond_1
    return-object v0
.end method

.method public static forNautilusTrack(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "nid"    # Ljava/lang/String;

    .prologue
    .line 269
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 270
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 271
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "fetchtrack"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    const-string v1, "nid"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    return-object v0
.end method

.method public static forOffers()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 303
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 304
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 305
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "signup"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "offers"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    return-object v0
.end method

.method public static forPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistsFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 102
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    return-object v0
.end method

.method public static forPlaylistEntriesBatchMutation()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 142
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 143
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 144
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "plentriesbatch"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    return-object v0
.end method

.method public static forPlaylistEntriesFeed()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 107
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 109
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "plentries"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    return-object v0
.end method

.method public static forPlaylistEntriesFeedAsPost()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 115
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 116
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "plentryfeed"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    return-object v0
.end method

.method public static forPlaylistEntry(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistEntriesFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 122
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    return-object v0
.end method

.method public static forPlaylistsBatchMutation()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 135
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 137
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "playlistbatch"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    return-object v0
.end method

.method public static forPlaylistsFeed()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 88
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 89
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "playlists"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    return-object v0
.end method

.method public static forPlaylistsFeedAsPost()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 95
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 96
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "playlistfeed"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    return-object v0
.end method

.method public static forQuizArtistList()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 353
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 354
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 355
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "newuserquiz"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "getrecommendedartists"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    return-object v0
.end method

.method public static forQuizGenreList()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 345
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 346
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 347
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "newuserquiz"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "listgenres"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    return-object v0
.end method

.method public static forQuizResultsList()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 361
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 362
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 363
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "newuserquiz"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "processquizresults"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    return-object v0
.end method

.method public static forRadioEditStations()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 212
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 213
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 214
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "radio"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "editstation"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    return-object v0
.end method

.method public static forRadioGetStations()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 204
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 205
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 206
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "radio"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "station"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    return-object v0
.end method

.method public static forRadioStationFeed()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 220
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 221
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 222
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "radio"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "stationfeed"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    return-object v0
.end method

.method public static forSoundSearchAutoPlaylist(Ljava/lang/String;I)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "continuationToken"    # Ljava/lang/String;
    .param p1, "numResults"    # I

    .prologue
    .line 325
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 326
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 327
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "plentries"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "soundsearch"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 331
    const-string v1, "start-token"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    :cond_0
    const-string v1, "num-results"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    return-object v0
.end method

.method public static forTab(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;ILjava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "tab"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p1, "numItems"    # I
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 174
    const/4 v1, 0x1

    if-ge p1, v1, :cond_0

    .line 175
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid number of max entities requested."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 178
    :cond_0
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 179
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 180
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "explore"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "tabs"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    const-string v1, "tabs"

    invoke-virtual {p0}, Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const-string v1, "num-items"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    if-eqz p2, :cond_1

    .line 185
    const-string v1, "genre"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    :cond_1
    return-object v0
.end method

.method public static forTrack(Ljava/lang/String;I)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "dimension"    # I

    .prologue
    .line 74
    invoke-static {p1}, Lcom/google/android/music/sync/api/MusicUrl;->forTracksFeed(I)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    .line 75
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    return-object v0
.end method

.method public static forTrackStatsBatchReport()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 82
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "trackstats"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-object v0
.end method

.method public static forTracksBatchMutation()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 149
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 150
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 151
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "trackbatch"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    return-object v0
.end method

.method public static forTracksFeed(I)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "dimension"    # I

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 60
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "tracks"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    const-string v1, "art-dimension"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-object v0
.end method

.method public static forTracksFeedAsPost(I)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3
    .param p0, "dimension"    # I

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 68
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "trackfeed"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    const-string v1, "art-dimension"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    return-object v0
.end method

.method public static forUserDevicesList()Lcom/google/android/music/sync/api/MusicUrl;
    .locals 3

    .prologue
    .line 338
    new-instance v0, Lcom/google/android/music/sync/api/MusicUrl;

    const-string v1, "https://mclients.googleapis.com/sj/v1.10"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/MusicUrl;-><init>(Ljava/lang/String;)V

    .line 339
    .local v0, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const-string v1, "json"

    iput-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->alt:Ljava/lang/String;

    .line 340
    iget-object v1, v0, Lcom/google/android/music/sync/api/MusicUrl;->pathParts:Ljava/util/List;

    const-string v2, "devicemanagementinfo"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    return-object v0
.end method

.method private static getCloudQueueSyncApiKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 449
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 450
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "music_cloud_queue_private_api_key"

    const-string v2, "AIzaSyDtEV5rwG_F1jvyj6WVlOOzD2vZa8DEpLE"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getCloudQueueSyncUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 434
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 435
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string v2, "music_cloud_queue_url_authority"

    const-string v3, "www.googleapis.com"

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 439
    .local v0, "apiaryAuthority":Ljava/lang/String;
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "https"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "musicqueuesync"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "v1.0"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
