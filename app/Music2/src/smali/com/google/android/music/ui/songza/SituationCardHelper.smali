.class public Lcom/google/android/music/ui/songza/SituationCardHelper;
.super Ljava/lang/Object;
.source "SituationCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/songza/SituationCardHelper$SituationItemViewHolder;,
        Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;
    }
.end annotation


# static fields
.field public static final HEADER_PROJECTION:[Ljava/lang/String;

.field public static final SITUATION_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "situation_header"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/music/ui/songza/SituationCardHelper;->HEADER_PROJECTION:[Ljava/lang/String;

    .line 40
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "situation_id"

    aput-object v1, v0, v2

    const-string v1, "situation_title"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "situation_description"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "situation_image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "situation_wide_image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "situation_has_subsituations"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/songza/SituationCardHelper;->SITUATION_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static initHeaderLoader(Landroid/content/Context;)Landroid/support/v4/content/CursorLoader;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 57
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-static {}, Lcom/google/android/music/store/MusicContent$SituationsHeader;->getHeaderDescriptionUri()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/songza/SituationCardHelper;->HEADER_PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static initTopSituationsLoader(Landroid/content/Context;)Landroid/support/v4/content/CursorLoader;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 62
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-static {}, Lcom/google/android/music/store/MusicContent$Situations;->getTopSituationsUri()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/songza/SituationCardHelper;->SITUATION_PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
