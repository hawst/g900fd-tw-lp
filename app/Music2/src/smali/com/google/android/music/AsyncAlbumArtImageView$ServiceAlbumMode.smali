.class Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ServiceAlbumMode"
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public albumId:J

.field public artist:Ljava/lang/String;

.field public service:Lcom/google/android/music/playback/IMusicPlaybackService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 594
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;)V
    .locals 2
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    .prologue
    .line 596
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 597
    iget-wide v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    iput-wide v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    .line 598
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->service:Lcom/google/android/music/playback/IMusicPlaybackService;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->service:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 599
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->album:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->album:Ljava/lang/String;

    .line 600
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->artist:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->artist:Ljava/lang/String;

    .line 601
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 645
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 606
    const/4 v14, 0x0

    .line 608
    .local v14, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->service:Lcom/google/android/music/playback/IMusicPlaybackService;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    invoke-interface {v2, v4, v5}, Lcom/google/android/music/playback/IMusicPlaybackService;->getAlbumArtUrl(J)Ljava/lang/String;

    move-result-object v3

    .line 609
    .local v3, "url":Ljava/lang/String;
    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v2, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-static/range {v2 .. v8}, Lcom/google/android/music/utils/AlbumArtUtils;->getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 611
    if-nez v14, :cond_0

    .line 612
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->album:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->artist:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v4, p1

    move/from16 v8, p2

    move/from16 v9, p3

    move-object/from16 v12, p4

    invoke-static/range {v4 .. v13}, Lcom/google/android/music/utils/AlbumArtUtils;->getDefaultArtwork(Landroid/content/Context;ZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 614
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lcom/google/android/music/download/artwork/AlbumIdSink;->report(Ljava/lang/Long;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 623
    .end local v3    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v14

    .line 616
    :catch_0
    move-exception v15

    .line 617
    .local v15, "e":Landroid/os/RemoteException;
    const-string v2, "AsyncAlbumArtImageView"

    invoke-virtual {v15}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 618
    .end local v15    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v15

    .line 621
    .local v15, "e":Ljava/lang/Exception;
    const-string v2, "AsyncAlbumArtImageView"

    invoke-virtual {v15}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 633
    instance-of v2, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    if-nez v2, :cond_1

    .line 637
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 636
    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    .line 637
    .local v0, "o":Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;
    iget-wide v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    iget-wide v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->service:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->service:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->album:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->album:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->artist:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->artist:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 628
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ServiceAlbumMode<albumId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
