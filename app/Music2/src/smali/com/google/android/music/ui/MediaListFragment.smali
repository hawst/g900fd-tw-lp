.class public abstract Lcom/google/android/music/ui/MediaListFragment;
.super Lcom/google/android/music/ui/BaseListFragment;
.source "MediaListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/BaseListFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivityCreated:Z

.field private mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

.field private mAsync:Z

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field private mIsDataSetObserverRegistered:Z

.field private mMediaList:Lcom/google/android/music/medialist/MediaList;

.field private mProjection:[Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;-><init>()V

    .line 33
    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mActivityCreated:Z

    .line 35
    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mIsDataSetObserverRegistered:Z

    .line 36
    new-instance v0, Lcom/google/android/music/ui/MediaListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MediaListFragment$1;-><init>(Lcom/google/android/music/ui/MediaListFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/MediaListFragment;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MediaListFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/MediaListFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MediaListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MediaListFragment;->updateEmptyScreenVisibility(Z)V

    return-void
.end method

.method private initFromArgs()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 130
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v4

    .line 133
    :cond_1
    const-string v5, "list"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "embrio":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 137
    invoke-static {v1}, Lcom/google/android/music/medialist/MediaList;->thaw(Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList;

    move-result-object v2

    .line 138
    .local v2, "list":Lcom/google/android/music/medialist/MediaList;
    if-eqz v2, :cond_0

    .line 141
    const-string v5, "proj"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "proj":[Ljava/lang/String;
    if-eqz v3, :cond_0

    array-length v5, v3

    if-eqz v5, :cond_0

    .line 145
    iput-object v2, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 146
    iput-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mProjection:[Ljava/lang/String;

    .line 147
    const-string v4, "async"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    .line 149
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private initializeListView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 182
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 183
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    iget-boolean v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    if-eqz v3, :cond_1

    .line 186
    iget-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v4, p0, Lcom/google/android/music/ui/MediaListFragment;->mProjection:[Ljava/lang/String;

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/android/music/medialist/MediaList;->getCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v1

    .line 187
    .local v1, "asyncCursor":Landroid/database/Cursor;
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/MediaListFragment;->newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    .line 188
    iget-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/MediaListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 196
    .end local v1    # "asyncCursor":Landroid/database/Cursor;
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mIsDataSetObserverRegistered:Z

    if-nez v3, :cond_0

    .line 197
    iget-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    iget-object v4, p0, Lcom/google/android/music/ui/MediaListFragment;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v3, v4}, Lcom/google/android/music/ui/MediaListCursorAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 198
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mIsDataSetObserverRegistered:Z

    .line 200
    :cond_0
    return-void

    .line 190
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    .line 191
    iget-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/MediaListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    .line 193
    .local v2, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method private updateEmptyScreenVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 278
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/MediaListFragment;->setEmptyScreenVisible(Z)V

    .line 279
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 280
    .local v0, "lv":Landroid/widget/ListView;
    if-eqz p1, :cond_0

    .line 281
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 285
    :goto_0
    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 286
    return-void

    .line 283
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getSupportsFastScroll()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    return-object v0
.end method

.method protected getSupportsFastScroll()Z
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method

.method protected init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V
    .locals 2
    .param p1, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "async"    # Z

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    if-nez p1, :cond_1

    .line 92
    const-string v0, "MediaListFragment"

    const-string v1, "Tried to initialize with null MediaList."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_1
    iput-object p1, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 96
    iput-object p2, p0, Lcom/google/android/music/ui/MediaListFragment;->mProjection:[Ljava/lang/String;

    .line 97
    iput-boolean p3, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    .line 99
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mActivityCreated:Z

    if-eqz v0, :cond_2

    .line 101
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListFragment;->initializeListView()V

    .line 103
    :cond_2
    return-void
.end method

.method protected isInitialized()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
.end method

.method abstract newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 204
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListFragment;->initializeListView()V

    .line 210
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mActivityCreated:Z

    .line 211
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListFragment;->initFromArgs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    const-string v0, "MediaListFragment"

    const-string v1, "Failed to init from args, expecting init() to be called by subclass."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    iget-boolean v1, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    if-eqz v1, :cond_0

    .line 232
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Attempt to use loader with async cursor"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/MediaList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 235
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_1

    .line 236
    new-instance v1, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v4, p0, Lcom/google/android/music/ui/MediaListFragment;->mProjection:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 238
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/google/android/music/ui/MediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v4, p0, Lcom/google/android/music/ui/MediaListFragment;->mProjection:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mIsDataSetObserverRegistered:Z

    if-eqz v0, :cond_0

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mIsDataSetObserverRegistered:Z

    .line 217
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/MediaListFragment;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MediaListCursorAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 222
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MediaListCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 225
    :cond_1
    invoke-super {p0}, Lcom/google/android/music/ui/BaseListFragment;->onDestroy()V

    .line 226
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    if-eqz v0, :cond_0

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to use loader with async cursor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MediaListCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 251
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/MediaListFragment;->updateEmptyScreenVisibility(Z)V

    .line 256
    return-void

    .line 251
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 22
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/MediaListFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 260
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to use loader with async cursor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MediaListCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 267
    return-void
.end method

.method protected saveMediaListAsArguments()V
    .locals 3

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    if-nez v1, :cond_0

    .line 126
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 118
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 119
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 122
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_1
    const-string v1, "list"

    iget-object v2, p0, Lcom/google/android/music/ui/MediaListFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    invoke-virtual {v2}, Lcom/google/android/music/medialist/MediaList;->freeze()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v1, "proj"

    iget-object v2, p0, Lcom/google/android/music/ui/MediaListFragment;->mProjection:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 124
    const-string v1, "async"

    iget-boolean v2, p0, Lcom/google/android/music/ui/MediaListFragment;->mAsync:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 125
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MediaListFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method
