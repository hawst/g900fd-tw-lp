.class public Lcom/google/android/music/store/ConfigContent;
.super Ljava/lang/Object;
.source "ConfigContent.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final APP_SETTINGS_URI:Landroid/net/Uri;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final SERVER_SETTINGS_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-string v0, "content://com.google.android.music.ConfigContent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/ConfigContent;->CONTENT_URI:Landroid/net/Uri;

    .line 25
    sget-object v0, Lcom/google/android/music/store/ConfigContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "server"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    .line 28
    sget-object v0, Lcom/google/android/music/store/ConfigContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "app"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/ConfigContent;->APP_SETTINGS_URI:Landroid/net/Uri;

    return-void
.end method

.method public static getConfigSettingUri(ILjava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0, "type"    # I
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-static {p1}, Lcom/google/android/music/ui/cardlib/utils/Utils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "encodedName":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 87
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unhandled type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :pswitch_0
    sget-object v1, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 85
    :goto_0
    return-object v1

    :pswitch_1
    sget-object v1, Lcom/google/android/music/store/ConfigContent;->APP_SETTINGS_URI:Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
