.class public Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioFeedResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/RadioFeedResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RadioData"
.end annotation


# instance fields
.field public mRadioStations:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "stations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncableRadioStation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    return-void
.end method
