.class Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RemoteAsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemStatusReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;


# direct methods
.method private constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p2, "x1"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;

    .prologue
    .line 297
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 304
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v2, "onReceive intent=%s extras=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 310
    :cond_0
    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 311
    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/media/MediaItemStatus;->fromBundle(Landroid/os/Bundle;)Landroid/support/v7/media/MediaItemStatus;

    move-result-object v0

    .line 313
    .local v0, "status":Landroid/support/v7/media/MediaItemStatus;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processMediaItemStatus(Landroid/support/v7/media/MediaItemStatus;)V
    invoke-static {v1, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Landroid/support/v7/media/MediaItemStatus;)V

    .line 317
    .end local v0    # "status":Landroid/support/v7/media/MediaItemStatus;
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v2, "Received update with no status!"

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    goto :goto_0
.end method
