.class public Lcom/google/android/music/medialist/NautilusSelectedSongList;
.super Lcom/google/android/music/medialist/NautilusSongList;
.source "NautilusSelectedSongList.java"


# instance fields
.field private mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field protected final mMetajamIds:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/music/store/ContainerDescriptor;[Ljava/lang/String;)V
    .locals 0
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "metajamIds"    # [Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/music/medialist/NautilusSongList;-><init>()V

    .line 20
    iput-object p2, p0, Lcom/google/android/music/medialist/NautilusSelectedSongList;->mMetajamIds:[Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/google/android/music/medialist/NautilusSelectedSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 22
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusSelectedSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/NautilusSelectedSongList;->mMetajamIds:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/medialist/NautilusSelectedSongList;->encodeArg([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusSelectedSongList;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/music/medialist/NautilusSelectedSongList;->mMetajamIds:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$XAudio;->getNautilusSelectedAudioUri([Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getNautilusId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return-object v0
.end method
