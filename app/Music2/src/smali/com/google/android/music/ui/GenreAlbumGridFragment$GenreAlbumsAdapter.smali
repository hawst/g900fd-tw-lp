.class final Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;
.super Lcom/google/android/music/ui/AlbumsAdapter;
.source "GenreAlbumGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/GenreAlbumGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "GenreAlbumsAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/GenreAlbumGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 0
    .param p2, "fragment"    # Lcom/google/android/music/ui/AlbumGridFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    .line 105
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/music/ui/AlbumsAdapter;-><init>(Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->init()V

    .line 107
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/GenreAlbumGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 0
    .param p2, "fragment"    # Lcom/google/android/music/ui/AlbumGridFragment;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    .line 99
    invoke-direct {p0, p2, p3}, Lcom/google/android/music/ui/AlbumsAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->init()V

    .line 101
    return-void
.end method


# virtual methods
.method public getFakeView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 120
    invoke-static {p2}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 121
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 122
    iget-object v2, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    # getter for: Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreId:J
    invoke-static {v2}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->access$100(Lcom/google/android/music/ui/GenreAlbumGridFragment;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 123
    iget-object v2, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    # getter for: Lcom/google/android/music/ui/GenreAlbumGridFragment;->mGenreName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->access$000(Lcom/google/android/music/ui/GenreAlbumGridFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0055

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    move-object v1, p2

    .line 126
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 127
    .local v1, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v2, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 128
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTag(Ljava/lang/Object;)V

    .line 129
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    return-object v1
.end method

.method protected init()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/android/music/ui/AlbumsAdapter;->init()V

    .line 112
    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->this$0:Lcom/google/android/music/ui/GenreAlbumGridFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 113
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/GenreAlbumGridFragment$GenreAlbumsAdapter;->setDisableFake(Z)V

    .line 116
    :cond_0
    return-void
.end method
