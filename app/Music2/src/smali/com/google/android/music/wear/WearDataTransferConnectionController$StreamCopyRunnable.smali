.class final Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;
.super Ljava/lang/Object;
.source "WearDataTransferConnectionController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/wear/WearDataTransferConnectionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StreamCopyRunnable"
.end annotation


# instance fields
.field private final mInputStream:Ljava/io/InputStream;

.field private final mOutputStream:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "outputStream"    # Ljava/io/OutputStream;

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const-string v0, "inputStream cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mInputStream:Ljava/io/InputStream;

    .line 140
    const-string v0, "outputStream cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    iput-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mOutputStream:Ljava/io/OutputStream;

    .line 141
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 146
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mInputStream:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mOutputStream:Ljava/io/OutputStream;

    invoke-static {v1, v2}, Lcom/google/common/io/ByteStreams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    iget-object v1, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mInputStream:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 152
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 157
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "WearTransferController"

    const-string v2, "IOException while closing output stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 147
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 148
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v1, "WearTransferController"

    const-string v2, "Exception when copying data into output stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 150
    iget-object v1, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mInputStream:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 152
    :try_start_3
    iget-object v1, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 153
    :catch_2
    move-exception v0

    .line 154
    const-string v1, "WearTransferController"

    const-string v2, "IOException while closing output stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 150
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mInputStream:Ljava/io/InputStream;

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 152
    :try_start_4
    iget-object v2, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 155
    :goto_1
    throw v1

    .line 153
    :catch_3
    move-exception v0

    .line 154
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v2, "WearTransferController"

    const-string v3, "IOException while closing output stream"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
