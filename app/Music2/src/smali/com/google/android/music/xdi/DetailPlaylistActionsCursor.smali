.class Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailPlaylistActionsCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "playlistId"    # J

    .prologue
    .line 33
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 34
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    .line 35
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 37
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->addPlaylistActions(J)V

    .line 38
    return-void
.end method

.method private addPlayAction(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "playlistId"    # J
    .param p3, "playlistType"    # I
    .param p4, "playlistName"    # Ljava/lang/String;
    .param p5, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p6, "actionName"    # Ljava/lang/String;
    .param p7, "actionSubName"    # Ljava/lang/String;
    .param p8, "playMode"    # Ljava/lang/String;
    .param p9, "actionId"    # I

    .prologue
    .line 98
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "container"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "playlist_type"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "offset"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "play_mode"

    invoke-virtual {v2, v3, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 106
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v2, p5, v0}, Lcom/google/android/music/xdi/XdiUtils;->setSonglistInfoInIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Landroid/content/Intent;)V

    .line 108
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/Object;

    .line 109
    .local v1, "values":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 110
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    invoke-virtual {v2, v1, v3, p6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_subname"

    invoke-virtual {v2, v1, v3, p7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "intent_uri"

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->addRow([Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method private addPlaylistActions(J)V
    .locals 23
    .param p1, "playlistId"    # J

    .prologue
    .line 41
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/google/android/music/xdi/MusicProjections;->PLAYLIST_COLUMNS:[Ljava/lang/String;

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1, v4}, Lcom/google/android/music/xdi/XdiUtils;->getPlaylistCursor(Landroid/content/Context;J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 44
    .local v2, "cursor":Landroid/database/Cursor;
    if-nez v2, :cond_0

    .line 81
    :goto_0
    return-void

    .line 49
    :cond_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 50
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/music/xdi/XdiUtils;->getSongListForPlaylistCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/SongList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 52
    .local v8, "songList":Lcom/google/android/music/medialist/SongList;
    if-nez v8, :cond_1

    .line 79
    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 56
    :cond_1
    const/4 v3, 0x1

    :try_start_1
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 57
    .local v7, "name":Ljava/lang/String;
    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 59
    .local v6, "type":I
    if-gez v6, :cond_2

    .line 60
    const-string v3, "MusicXdi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not get playlist type for playlist: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 64
    :cond_2
    :try_start_2
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 65
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00c6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 68
    :cond_3
    const/4 v12, 0x0

    .line 69
    .local v12, "actionId":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00c8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b02dc

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "play"

    add-int/lit8 v22, v12, 0x1

    .end local v12    # "actionId":I
    .local v22, "actionId":I
    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    invoke-direct/range {v3 .. v12}, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->addPlayAction(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 73
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0056

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b02dc

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "shuffle"

    add-int/lit8 v12, v22, 0x1

    .end local v22    # "actionId":I
    .restart local v12    # "actionId":I
    move-object/from16 v13, p0

    move-wide/from16 v14, p1

    move/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v8

    invoke-direct/range {v13 .. v22}, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;->addPlayAction(JILjava/lang/String;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 79
    .end local v6    # "type":I
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v12    # "actionId":I
    :cond_4
    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v3
.end method
