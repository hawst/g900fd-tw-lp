.class final Lcom/google/android/music/ui/SharedElementTransition$1;
.super Landroid/support/v4/app/SharedElementCallback;
.source "SharedElementTransition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SharedElementTransition;->updateSnapshot(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Landroid/support/v4/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCaptureSharedElementSnapshot(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;
    .locals 3
    .param p1, "sharedElement"    # Landroid/view/View;
    .param p2, "viewToGlobalMatrix"    # Landroid/graphics/Matrix;
    .param p3, "screenBounds"    # Landroid/graphics/RectF;

    .prologue
    .line 104
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/ui/ShareableElement;

    .line 105
    .local v0, "shared":Lcom/google/android/music/ui/ShareableElement;
    invoke-interface {v0, p2, p3}, Lcom/google/android/music/ui/ShareableElement;->setSharedElementSnapshotInfo(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 106
    invoke-interface {v0}, Lcom/google/android/music/ui/ShareableElement;->getSnapshotElement()Landroid/view/View;

    move-result-object v1

    .line 108
    .local v1, "snapshotElement":Landroid/view/View;
    invoke-super {p0, v1, p2, p3}, Landroid/support/v4/app/SharedElementCallback;->onCaptureSharedElementSnapshot(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;

    move-result-object v2

    return-object v2
.end method
