.class public abstract Lcom/google/android/music/medialist/SongList;
.super Lcom/google/android/music/medialist/MediaList;
.source "SongList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/SongList;",
            ">;"
        }
    .end annotation
.end field

.field protected static final DEFAULT_POSITION_SEARCH_RADIUS:I = 0xfa

.field public static final SORT_BY_ALBUM:I = 0x2

.field public static final SORT_BY_ARTIST:I = 0x3

.field public static final SORT_BY_DATE:I = 0x4

.field public static final SORT_BY_NATURAL_ORDER:I = 0x0

.field public static final SORT_BY_RATING_TIMESTAMP:I = 0x5

.field public static final SORT_BY_TITLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SongList"


# instance fields
.field protected mSortOrder:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 296
    new-instance v0, Lcom/google/android/music/medialist/SongList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/SongList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/SongList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V
    .locals 0
    .param p1, "sortOrder"    # I
    .param p2, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;
    .param p3, "shouldFilter"    # Z
    .param p4, "includeExternal"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/music/medialist/MediaList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V

    .line 49
    iput p1, p0, Lcom/google/android/music/medialist/SongList;->mSortOrder:I

    .line 50
    return-void
.end method

.method public constructor <init>(IZZ)V
    .locals 1
    .param p1, "sortOrder"    # I
    .param p2, "shouldFilter"    # Z
    .param p3, "includeExternal"    # Z

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/music/medialist/MediaList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V

    .line 54
    iput p1, p0, Lcom/google/android/music/medialist/SongList;->mSortOrder:I

    .line 55
    return-void
.end method

.method public static shouldTryArtistSlideShow(Lcom/google/android/music/medialist/SongList;)Z
    .locals 1
    .param p0, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 345
    instance-of v0, p0, Lcom/google/android/music/medialist/PlaylistSongList;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/music/medialist/GenreSongList;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    .line 150
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "appending to playlist not supported by base SongList"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 254
    const/4 v0, 0x0

    return v0
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 245
    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
.end method

.method public getDownloadedSongCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 263
    const/4 v0, -0x1

    return v0
.end method

.method public getKeepOnSongCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 272
    const/4 v0, -0x1

    return v0
.end method

.method public getSortOrder()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/music/medialist/SongList;->mSortOrder:I

    return v0
.end method

.method protected getSortParam()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    iget v1, p0, Lcom/google/android/music/medialist/SongList;->mSortOrder:I

    packed-switch v1, :pswitch_data_0

    .line 116
    const/4 v0, 0x0

    .line 119
    .local v0, "sortParam":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 101
    .end local v0    # "sortParam":Ljava/lang/String;
    :pswitch_0
    const-string v0, "name"

    .line 102
    .restart local v0    # "sortParam":Ljava/lang/String;
    goto :goto_0

    .line 104
    .end local v0    # "sortParam":Ljava/lang/String;
    :pswitch_1
    const-string v0, "album"

    .line 105
    .restart local v0    # "sortParam":Ljava/lang/String;
    goto :goto_0

    .line 107
    .end local v0    # "sortParam":Ljava/lang/String;
    :pswitch_2
    const-string v0, "artist"

    .line 108
    .restart local v0    # "sortParam":Ljava/lang/String;
    goto :goto_0

    .line 110
    .end local v0    # "sortParam":Ljava/lang/String;
    :pswitch_3
    const-string v0, "date"

    .line 111
    .restart local v0    # "sortParam":Ljava/lang/String;
    goto :goto_0

    .line 113
    .end local v0    # "sortParam":Ljava/lang/String;
    :pswitch_4
    const-string v0, "ratingTimestamp"

    .line 114
    .restart local v0    # "sortParam":Ljava/lang/String;
    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getSuggestedPositionSearchRadius()I
    .locals 1

    .prologue
    .line 320
    const/16 v0, 0xfa

    return v0
.end method

.method public abstract getValidSortOrders()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public hasArtistArt()Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public hasMetaData()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method public hasStablePrimaryIds()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method public hasUniqueAudioId()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public isAllLocal(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/AvailableSpaceTracker;Lcom/google/android/music/store/IStoreService;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "offlineMusicManager"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p3, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 236
    const/4 v0, 0x0

    return v0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 227
    const/4 v0, 0x0

    return v0
.end method

.method public populateExternalSearchExtras(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 294
    return-void
.end method

.method public refreshMetaData(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no metadata"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public registerMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no metadata"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSortOrder(I)V
    .locals 0
    .param p1, "order"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/google/android/music/medialist/SongList;->mSortOrder:I

    .line 91
    return-void
.end method

.method public shouldShowTrackNumbers()Z
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    return v0
.end method

.method public abstract storeDefaultSortOrder(Landroid/content/Context;)V
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method protected supportsOfflineCaching()Z
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    return v0
.end method

.method public final supportsOfflineCaching(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 181
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 182
    .local v1, "prefsHolder":Ljava/lang/Object;
    invoke-static {p1, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 184
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineFeatureAvailable()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 185
    const/4 v2, 0x0

    .line 188
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 190
    :goto_0
    return v2

    .line 188
    :cond_0
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/music/medialist/SongList;->supportsOfflineCaching()Z

    move-result v2

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public supportsVideoCluster()Z
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x0

    return v0
.end method

.method public toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keepOnManager"    # Lcom/google/android/music/activitymanagement/KeepOnManager;
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 287
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toggleOfflineCaching is called on class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " that does not implement it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public unregisterMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 171
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 172
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/google/android/music/medialist/SongList;->freeze()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 313
    return-void
.end method
