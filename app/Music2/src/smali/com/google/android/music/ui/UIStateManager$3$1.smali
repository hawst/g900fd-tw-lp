.class Lcom/google/android/music/ui/UIStateManager$3$1;
.super Ljava/lang/Object;
.source "UIStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/UIStateManager$3;->onDownloadabilityChanged(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/UIStateManager$3;

.field final synthetic val$isDownloadable:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/UIStateManager$3;Z)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager$3$1;->this$1:Lcom/google/android/music/ui/UIStateManager$3;

    iput-boolean p2, p0, Lcom/google/android/music/ui/UIStateManager$3$1;->val$isDownloadable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$3$1;->this$1:Lcom/google/android/music/ui/UIStateManager$3;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$3;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mIsDownloadingEnabled:Z
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->access$500(Lcom/google/android/music/ui/UIStateManager;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$3$1;->val$isDownloadable:Z

    if-eq v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$3$1;->this$1:Lcom/google/android/music/ui/UIStateManager$3;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$3;->this$0:Lcom/google/android/music/ui/UIStateManager;

    iget-boolean v1, p0, Lcom/google/android/music/ui/UIStateManager$3$1;->val$isDownloadable:Z

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mIsDownloadingEnabled:Z
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$502(Lcom/google/android/music/ui/UIStateManager;Z)Z

    .line 164
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$3$1;->this$1:Lcom/google/android/music/ui/UIStateManager$3;

    iget-object v0, v0, Lcom/google/android/music/ui/UIStateManager$3;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # invokes: Lcom/google/android/music/ui/UIStateManager;->notifyDownloadabilityChanged()V
    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->access$600(Lcom/google/android/music/ui/UIStateManager;)V

    .line 166
    :cond_0
    return-void
.end method
