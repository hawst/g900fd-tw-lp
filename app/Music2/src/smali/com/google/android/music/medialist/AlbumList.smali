.class public abstract Lcom/google/android/music/medialist/AlbumList;
.super Lcom/google/android/music/medialist/MediaList;
.source "AlbumList.java"


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V
    .locals 0
    .param p1, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;
    .param p2, "shouldFilter"    # Z
    .param p3, "includeExternal"    # Z

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/medialist/MediaList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V

    .line 18
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "shouldFilter"    # Z

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/music/medialist/MediaList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V

    .line 13
    return-void
.end method
