.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;
.super Landroid/support/v17/leanback/widget/ListRowPresenter;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExploreGenresListRowPresenter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 6
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/ListRowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V

    move-object v0, p2

    .line 80
    check-cast v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;

    .line 81
    .local v0, "exploreGenresListRow":Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;
    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    const v2, 0x7f0e015f

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 82
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getFeaturedLoaderId()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getFeaturedAdapter()Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getGenreId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/ExploreClusterListFragment;->GROUPS_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 86
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getNewReleasesLoaderId()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getNewReleasesAdapter()Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getGenreId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/MusicContent$Explore;->getNewReleaseGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/ExploreClusterListFragment;->GROUPS_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method protected onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/ListRowPresenter;->onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 95
    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    const v2, 0x7f0e015f

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;

    .line 97
    .local v0, "exploreGenresListRow":Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getFeaturedLoaderId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 98
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRowPresenter;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;->getNewReleasesLoaderId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 99
    return-void
.end method
