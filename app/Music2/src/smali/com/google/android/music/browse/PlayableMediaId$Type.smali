.class public final enum Lcom/google/android/music/browse/PlayableMediaId$Type;
.super Ljava/lang/Enum;
.source "PlayableMediaId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/browse/PlayableMediaId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/browse/PlayableMediaId$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum AUTOPLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum LUCKY_RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum NAUTILUS_ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum PLAYBACK_QUEUE_ITEM:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum PUBLIC_PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

.field public static final enum UNKNOWN:Lcom/google/android/music/browse/PlayableMediaId$Type;


# instance fields
.field private mId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "PLAYLIST"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 36
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "AUTOPLAYLIST"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->AUTOPLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 37
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "PUBLIC_PLAYLIST"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->PUBLIC_PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 38
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 39
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "NAUTILUS_ALBUM"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 40
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "RADIO"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 41
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "LUCKY_RADIO"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->LUCKY_RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 42
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "PLAYBACK_QUEUE_ITEM"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->PLAYBACK_QUEUE_ITEM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 43
    new-instance v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x8

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/browse/PlayableMediaId$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->UNKNOWN:Lcom/google/android/music/browse/PlayableMediaId$Type;

    .line 34
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/music/browse/PlayableMediaId$Type;

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->AUTOPLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->PUBLIC_PLAYLIST:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/music/browse/PlayableMediaId$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/browse/PlayableMediaId$Type;->RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/browse/PlayableMediaId$Type;->LUCKY_RADIO:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/browse/PlayableMediaId$Type;->PLAYBACK_QUEUE_ITEM:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/browse/PlayableMediaId$Type;->UNKNOWN:Lcom/google/android/music/browse/PlayableMediaId$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->$VALUES:[Lcom/google/android/music/browse/PlayableMediaId$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lcom/google/android/music/browse/PlayableMediaId$Type;->mId:I

    .line 49
    return-void
.end method

.method public static fromId(I)Lcom/google/android/music/browse/PlayableMediaId$Type;
    .locals 7
    .param p0, "id"    # I

    .prologue
    .line 56
    # getter for: Lcom/google/android/music/browse/PlayableMediaId;->TYPE_VALUES:[Lcom/google/android/music/browse/PlayableMediaId$Type;
    invoke-static {}, Lcom/google/android/music/browse/PlayableMediaId;->access$000()[Lcom/google/android/music/browse/PlayableMediaId$Type;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/browse/PlayableMediaId$Type;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 57
    .local v3, "type":Lcom/google/android/music/browse/PlayableMediaId$Type;
    iget v4, v3, Lcom/google/android/music/browse/PlayableMediaId$Type;->mId:I

    if-ne v4, p0, :cond_0

    .line 58
    return-object v3

    .line 56
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    .end local v3    # "type":Lcom/google/android/music/browse/PlayableMediaId$Type;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/browse/PlayableMediaId$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/browse/PlayableMediaId$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/browse/PlayableMediaId$Type;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/music/browse/PlayableMediaId$Type;->$VALUES:[Lcom/google/android/music/browse/PlayableMediaId$Type;

    invoke-virtual {v0}, [Lcom/google/android/music/browse/PlayableMediaId$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/browse/PlayableMediaId$Type;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/music/browse/PlayableMediaId$Type;->mId:I

    return v0
.end method
