.class abstract Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;
.source "MediaListBitmapGettersGetter.java"


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J

    .prologue
    .line 19
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 20
    return-void
.end method


# virtual methods
.method protected getCursorLoader()Landroid/support/v4/content/Loader;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v0

    .line 29
    .local v0, "mediaList":Lcom/google/android/music/medialist/MediaList;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->getProjection()[Ljava/lang/String;

    move-result-object v1

    .line 30
    .local v1, "projection":[Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcom/google/android/music/medialist/MediaList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    .line 31
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_0

    .line 32
    new-instance v3, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;

    iget-object v4, p0, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/music/ui/NonUriMediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 34
    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Lcom/google/android/music/ui/MediaListCursorLoader;

    iget-object v4, p0, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract getMediaList()Lcom/google/android/music/medialist/MediaList;
.end method

.method protected abstract getProjection()[Ljava/lang/String;
.end method
