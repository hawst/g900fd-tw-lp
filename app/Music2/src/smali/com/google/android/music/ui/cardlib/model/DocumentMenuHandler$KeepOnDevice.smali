.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KeepOnDevice"
.end annotation


# instance fields
.field final mKeeponView:Lcom/google/android/music/KeepOnView;

.field final mSongList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1089
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1090
    const v1, 0x7f0e0026

    iget-boolean v0, p1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsSelectedForOfflineCaching:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0b0241

    :goto_0
    invoke-direct {p0, p2, v1, v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1093
    new-instance v0, Lcom/google/android/music/KeepOnViewMedium;

    iget-object v1, p1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/music/KeepOnViewMedium;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->mKeeponView:Lcom/google/android/music/KeepOnView;

    .line 1094
    iget-object v0, p1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v1, p1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 1096
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->mKeeponView:Lcom/google/android/music/KeepOnView;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 1097
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->mKeeponView:Lcom/google/android/music/KeepOnView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setVisibility(I)V

    .line 1098
    return-void

    .line 1090
    :cond_0
    const v0, 0x7f0b0240

    goto :goto_0
.end method


# virtual methods
.method public onActionSelected()V
    .locals 1

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->mKeeponView:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v0}, Lcom/google/android/music/KeepOnView;->onKeepOnViewClicked()V

    .line 1103
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1108
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->onActionSelected()V

    .line 1109
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$200(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1110
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$200(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->dismiss()V

    .line 1112
    :cond_0
    return-void
.end method
