.class public interface abstract Lcom/google/android/music/download/keepon/IKeeponCallbackManager;
.super Ljava/lang/Object;
.source "IKeeponCallbackManager.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/keepon/IKeeponCallbackManager$Stub;
    }
.end annotation


# virtual methods
.method public abstract addDownloadProgressListener(Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/download/IDownloadProgressListener;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract removeDownloadProgressListener(Lcom/google/android/music/download/IDownloadProgressListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
