.class public Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;
.super Lcom/google/android/music/ui/MusicPlayHeaderListLayout;
.source "ContainerPlayHeaderListLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$ListConfigurator;
    }
.end annotation


# instance fields
.field private mSpacerHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->mSpacerHeight:I

    .line 37
    return-void
.end method

.method private addOnScrollListener()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;-><init>(Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 135
    return-void
.end method


# virtual methods
.method getConfigurator()Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$ListConfigurator;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$ListConfigurator;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->onFinishInflate()V

    .line 43
    invoke-direct {p0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->addOnScrollListener()V

    .line 44
    return-void
.end method

.method public onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 52
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MusicPlayHeaderListLayout;->onMeasure(II)V

    .line 54
    const v4, 0x7f0e0195

    invoke-virtual {p0, v4}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    .line 56
    .local v1, "headerBg":Lcom/google/android/music/ui/MaterialContainerHeaderBg;
    invoke-virtual {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getMeasuredHeight()I

    move-result v0

    .line 61
    .local v0, "artHeight":I
    invoke-virtual {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->usingAlbumArtForBackground()Z

    move-result v4

    if-eqz v4, :cond_0

    .end local v0    # "artHeight":I
    :goto_0
    iput v0, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->mSpacerHeight:I

    .line 65
    const v4, 0x7f0e0014

    invoke-virtual {p0, v4}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 66
    .local v3, "spacer":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 67
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v6, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->mSpacerHeight:I

    invoke-direct {v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->mSpacerHeight:I

    invoke-virtual {p0, v4, v5}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->setTabMode(II)V

    .line 71
    return-void

    .line 61
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "spacer":Landroid/view/View;
    .restart local v0    # "artHeight":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->getActionBarHeight()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v0, v4

    goto :goto_0
.end method
