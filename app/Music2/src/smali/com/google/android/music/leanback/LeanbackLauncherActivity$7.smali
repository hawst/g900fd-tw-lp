.class Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;
.super Ljava/lang/Object;
.source "LeanbackLauncherActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackLauncherActivity;->buildConciergeListRow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 419
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;->val$context:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/music/store/MusicContent$SituationsHeader;->getHeaderDescriptionUri()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/songza/SituationCardHelper;->HEADER_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 426
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 428
    .local v0, "rowName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->mListRowsAdapter:Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackLauncherActivity;)Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;->this$0:Lcom/google/android/music/leanback/LeanbackLauncherActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackLauncherActivity;->getConciergeListRow(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    invoke-static {v3, v0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity;->access$900(Lcom/google/android/music/leanback/LeanbackLauncherActivity;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackSearchActivity$SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 430
    .end local v0    # "rowName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 416
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$7;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 434
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method
