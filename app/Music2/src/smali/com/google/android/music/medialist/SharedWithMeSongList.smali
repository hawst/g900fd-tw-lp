.class public Lcom/google/android/music/medialist/SharedWithMeSongList;
.super Lcom/google/android/music/medialist/ExternalSongList;
.source "SharedWithMeSongList.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "SharedWithMeSongList"


# instance fields
.field private final mArtUrl:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private final mId:J

.field private mLocalPlaylistId:J

.field private mLocalPlaylistType:I

.field private final mName:Ljava/lang/String;

.field private final mOwnerName:Ljava/lang/String;

.field private final mOwnerProfilePhotoUrl:Ljava/lang/String;

.field private final mShareToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "shareToken"    # Ljava/lang/String;
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "ownerName"    # Ljava/lang/String;
    .param p7, "artLocation"    # Ljava/lang/String;
    .param p8, "ownerProfilePhotoUrl"    # Ljava/lang/String;

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/medialist/ExternalSongList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;Z)V

    .line 61
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->setFlag(I)V

    .line 62
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->setFlag(I)V

    .line 64
    iput-wide p1, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mId:J

    .line 65
    iput-object p3, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mShareToken:Ljava/lang/String;

    .line 66
    iput-object p4, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mName:Ljava/lang/String;

    .line 67
    iput-object p5, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mDescription:Ljava/lang/String;

    .line 68
    iput-object p6, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerName:Ljava/lang/String;

    .line 69
    iput-object p7, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mArtUrl:Ljava/lang/String;

    .line 70
    iput-object p8, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    .line 72
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistType:I

    .line 74
    return-void
.end method

.method private findPlaylistIdAndType(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 342
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "playlist_id"

    aput-object v0, v2, v3

    const-string v0, "playlist_type"

    aput-object v0, v2, v4

    .line 346
    .local v2, "listCols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mShareToken:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUriByShareToken(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 347
    .local v1, "listUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 349
    .local v6, "c":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    :try_start_0
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 350
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    .line 352
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 357
    return-void

    .line 355
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getSongCount(Landroid/content/Context;[Ljava/lang/String;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cols"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 284
    iget-wide v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 288
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 289
    :cond_0
    const-string v0, "SharedWithMeSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown playlist id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    const/4 v0, -0x1

    .line 295
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 292
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 295
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    const/4 v4, 0x0

    .line 147
    invoke-virtual {p0, p1, v4}, Lcom/google/android/music/medialist/SharedWithMeSongList;->addToStore(Landroid/content/Context;Z)[J

    move-result-object v1

    .line 148
    .local v1, "localIds":[J
    if-eqz v1, :cond_0

    array-length v5, v1

    if-lez v5, :cond_0

    .line 150
    const-string v2, ""

    .line 151
    .local v2, "name":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mShareToken:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v4, v2, v5}, Lcom/google/android/music/store/ContainerDescriptor;->newSharedWithMePlaylistDescriptor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 153
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v3, Lcom/google/android/music/medialist/SelectedSongList;

    invoke-direct {v3, v0, v1}, Lcom/google/android/music/medialist/SelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[J)V

    .line 154
    .local v3, "songs":Lcom/google/android/music/medialist/SelectedSongList;
    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/music/medialist/SelectedSongList;->appendToPlaylist(Landroid/content/Context;J)I

    move-result v4

    .line 156
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "songs":Lcom/google/android/music/medialist/SelectedSongList;
    :cond_0
    return v4
.end method

.method public canFollow(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 317
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canStopFollowing(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 327
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistType:I

    const/16 v1, 0x47

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method public followPlaylist(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mDescription:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mShareToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mArtUrl:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/store/MusicContent$Playlists;->followSharedPlaylist(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 191
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    .line 192
    return-void
.end method

.method public getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 178
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mShareToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mDescription:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mArtUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getArtUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 200
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 201
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getShareToken()Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "extId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "extData":Ljava/lang/String;
    invoke-static {v1, v2, v0}, Lcom/google/android/music/store/ContainerDescriptor;->newSharedWithMePlaylistDescriptor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    return-object v3
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mShareToken:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist$Members;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 239
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mDescription:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mId:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 240
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "playlist_description"

    aput-object v0, v2, v7

    .line 243
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 247
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mDescription:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 255
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mDescription:Ljava/lang/String;

    return-object v0

    .line 252
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getDownloadedSongCount(Landroid/content/Context;)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 260
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 261
    const/4 v1, -0x1

    .line 267
    :goto_0
    return v1

    .line 264
    :cond_0
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "keeponDownloadedSongCount"

    aput-object v2, v0, v1

    .line 267
    .local v0, "cols":[Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getSongCount(Landroid/content/Context;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 167
    const/4 v0, 0x0

    return-object v0
.end method

.method public getKeepOnSongCount(Landroid/content/Context;)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 272
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 273
    const/4 v1, -0x1

    .line 280
    :goto_0
    return v1

    .line 276
    :cond_0
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "keeponSongCount"

    aput-object v2, v0, v1

    .line 280
    .local v0, "cols":[Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getSongCount(Landroid/content/Context;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerName:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerProfilePhotoUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistId(Landroid/content/Context;)J
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 335
    iget-wide v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 336
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->findPlaylistIdAndType(Landroid/content/Context;)V

    .line 338
    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    return-wide v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mOwnerName:Ljava/lang/String;

    .line 118
    :goto_0
    return-object v2

    .line 111
    :cond_0
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 112
    .local v1, "prefsHolder":Ljava/lang/Object;
    invoke-static {p1, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 115
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/16 v2, 0x46

    :try_start_0
    invoke-static {p1, v0, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 118
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public getShareToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mShareToken:Ljava/lang/String;

    return-object v0
.end method

.method public hasArtistArt()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    return v0
.end method

.method public isAddToStoreSupported()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/AvailableSpaceTracker;Lcom/google/android/music/store/IStoreService;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "offlineMusicManager"    # Lcom/google/android/music/AvailableSpaceTracker;
    .param p3, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 226
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lcom/google/android/music/AvailableSpaceTracker;->isPlaylistSelected(J)Ljava/lang/Boolean;

    move-result-object v0

    .line 227
    .local v0, "result":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 230
    invoke-virtual {p0, p1, p3}, Lcom/google/android/music/medialist/SharedWithMeSongList;->isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z

    move-result v1

    .line 232
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeService"    # Lcom/google/android/music/store/IStoreService;

    .prologue
    .line 215
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    move-result-wide v2

    invoke-interface {p2, v2, v3}, Lcom/google/android/music/store/IStoreService;->isPlaylistSelectedAsKeepOn(J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 219
    :goto_0
    return v1

    .line 216
    :catch_0
    move-exception v0

    .line 217
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SharedWithMeSongList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error trying to get offline status for shared playlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 219
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method protected supportsOfflineCaching()Z
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x1

    return v0
.end method

.method public toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keepOnManager"    # Lcom/google/android/music/activitymanagement/KeepOnManager;
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 303
    iget-wide v0, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 304
    const-string v0, "SharedWithMeSongList"

    const-string v1, "Followed share playlist cannot have id -1."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/medialist/SharedWithMeSongList;->mLocalPlaylistId:J

    move-object v1, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/activitymanagement/KeepOnManager;->togglePlaylistKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V

    goto :goto_0
.end method
