.class Lcom/google/android/music/playback/LocalDevicePlayback$12;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 1532
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$12;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 1535
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$12;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-static {p2}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/cache/ICacheManager;

    move-result-object v2

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5502(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/cache/ICacheManager;)Lcom/google/android/music/download/cache/ICacheManager;

    .line 1537
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$12;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/cache/ICacheManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$12;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/cache/IDeleteFilter;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/music/download/cache/ICacheManager;->registerDeleteFilter(Lcom/google/android/music/download/cache/IDeleteFilter;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1541
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$12;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->tryCreatingStreamingSchedulingClient()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5400(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 1542
    return-void

    .line 1538
    :catch_0
    move-exception v0

    .line 1539
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Failed to register delete filter"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 1547
    return-void
.end method
