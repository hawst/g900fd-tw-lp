.class Lcom/google/android/music/xdi/ExploreGenresItemsCursor;
.super Landroid/database/MatrixCursor;
.source "ExploreGenresItemsCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mGenreId:Ljava/lang/String;

.field private final mGenreName:Ljava/lang/String;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "genreId"    # Ljava/lang/String;
    .param p4, "genreName"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    .line 36
    iput-object p3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreName:Ljava/lang/String;

    .line 38
    new-instance v1, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v1, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 39
    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v1}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 43
    .local v0, "values":[Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addRadio([Ljava/lang/Object;)V

    .line 44
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addSubgenres([Ljava/lang/Object;)V

    .line 45
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addFeatured([Ljava/lang/Object;)V

    .line 46
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addNewReleases([Ljava/lang/Object;)V

    .line 47
    return-void
.end method


# virtual methods
.method addFeatured([Ljava/lang/Object;)V
    .locals 7
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 106
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 111
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 120
    :goto_0
    return-void

    .line 116
    :cond_0
    :try_start_0
    invoke-virtual {p0, v6, p1}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addItems(Landroid/database/Cursor;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method addItems(Landroid/database/Cursor;[Ljava/lang/Object;)V
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 140
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->getCount()I

    move-result v5

    .line 142
    .local v5, "index":I
    const/4 v7, 0x0

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 143
    .local v2, "groupId":J
    const/4 v7, 0x4

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 144
    .local v4, "groupType":I
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "groupTitle":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "groupDescription":Ljava/lang/String;
    sget-object v7, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v8, "explore/genre"

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v8, "null"

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v6

    .line 154
    .local v6, "intent":Landroid/content/Intent;
    const-string v7, "meta_uri"

    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreName:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/music/xdi/XdiUtils;->getMetaTitleUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    iget-object v7, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v8, "_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, p2, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    iget-object v7, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v8, "display_name"

    invoke-virtual {v7, p2, v8, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 159
    iget-object v7, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v8, "display_description"

    invoke-virtual {v7, p2, v8, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    iget-object v7, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v8, "image_uri"

    iget-object v9, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, p2, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    iget-object v7, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v8, "width"

    invoke-virtual {v7, p2, v8, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    iget-object v7, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v8, "height"

    invoke-virtual {v7, p2, v8, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 165
    iget-object v7, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v8, "intent_uri"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, p2, v8, v9}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 167
    invoke-virtual {p0, p2}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addRow([Ljava/lang/Object;)V

    .line 168
    add-int/lit8 v5, v5, 0x1

    .line 169
    goto/16 :goto_0

    .line 170
    .end local v0    # "groupDescription":Ljava/lang/String;
    .end local v1    # "groupTitle":Ljava/lang/String;
    .end local v2    # "groupId":J
    .end local v4    # "groupType":I
    .end local v5    # "index":I
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method addNewReleases([Ljava/lang/Object;)V
    .locals 7
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$Explore;->getNewReleaseGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 128
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 137
    :goto_0
    return-void

    .line 133
    :cond_0
    :try_start_0
    invoke-virtual {p0, v6, p1}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addItems(Landroid/database/Cursor;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method addRadio([Ljava/lang/Object;)V
    .locals 8
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->getCount()I

    move-result v0

    .line 52
    .local v0, "index":I
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0099

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "name":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "explore/genre"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "null"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v1

    .line 60
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "meta_uri"

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreName:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/music/xdi/XdiUtils;->getMetaTitleUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "_id"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "display_name"

    invoke-virtual {v3, p1, v4, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "display_description"

    invoke-virtual {v3, p1, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "image_uri"

    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    const v6, 0x7f02012f

    invoke-static {v5, v6}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "width"

    invoke-virtual {v3, p1, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "height"

    invoke-virtual {v3, p1, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "intent_uri"

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addRow([Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method addSubgenres([Ljava/lang/Object;)V
    .locals 8
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 76
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/music/xdi/XdiUtils;->hasSubgenres(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 103
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->getCount()I

    move-result v0

    .line 82
    .local v0, "index":I
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00a8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "name":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "explore/genre"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "null"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v1

    .line 90
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "meta_uri"

    iget-object v4, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreName:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/music/xdi/XdiUtils;->getMetaTitleUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "_id"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "display_name"

    invoke-virtual {v3, p1, v4, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 95
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "display_description"

    invoke-virtual {v3, p1, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "image_uri"

    iget-object v5, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v5, v6, v7}, Lcom/google/android/music/xdi/XdiUtils;->getGenreArtUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "width"

    invoke-virtual {v3, p1, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 99
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "height"

    invoke-virtual {v3, p1, v4, v7}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    iget-object v3, p0, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "intent_uri"

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 102
    invoke-virtual {p0, p1}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
