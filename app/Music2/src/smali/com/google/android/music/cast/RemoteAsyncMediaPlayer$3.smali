.class Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;
.super Landroid/support/v7/media/MediaRouter$ControlRequestCallback;
.source "RemoteAsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoteResumeOnMainThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 829
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 837
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error encountered requesting remote resume: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 839
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 840
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    .line 841
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processControlRequestResultBundle(Landroid/os/Bundle;)V
    invoke-static {v0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Landroid/os/Bundle;)V

    .line 833
    return-void
.end method
