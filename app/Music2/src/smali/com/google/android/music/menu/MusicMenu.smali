.class public abstract Lcom/google/android/music/menu/MusicMenu;
.super Ljava/lang/Object;
.source "MusicMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/menu/MusicMenu$Callback;
    }
.end annotation


# instance fields
.field protected mCallback:Lcom/google/android/music/menu/MusicMenu$Callback;

.field protected mContext:Landroid/content/Context;

.field protected mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/menu/MusicMenuItem;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public abstract close()V
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/menu/MusicMenu;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getRadioButtonsEnabled()Z
    .locals 1

    .prologue
    .line 93
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getVisibleItems()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/menu/MusicMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v1, "visible":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/menu/MusicMenuItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/menu/MusicMenu;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 109
    iget-object v2, p0, Lcom/google/android/music/menu/MusicMenu;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/menu/MusicMenuItem;

    invoke-virtual {v2}, Lcom/google/android/music/menu/MusicMenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/google/android/music/menu/MusicMenu;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_1
    return-object v1
.end method

.method protected onItemChecked(Lcom/google/android/music/menu/MusicMenuItem;Z)V
    .locals 1
    .param p1, "item"    # Lcom/google/android/music/menu/MusicMenuItem;
    .param p2, "checked"    # Z

    .prologue
    .line 99
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract performItemAction(Lcom/google/android/music/menu/MusicMenuItem;)Z
.end method
