.class public Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowItemJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/ListenNowItemJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListenNowItemIdJson"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;
    }
.end annotation


# instance fields
.field public mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "album_id"
    .end annotation
.end field

.field public mPlaylistId:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlist_id"
    .end annotation
.end field

.field public mRadioId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "radio_station_id"
    .end annotation
.end field

.field public mType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 58
    return-void
.end method
