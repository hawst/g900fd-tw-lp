.class public Lcom/google/android/music/log/Log;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field private static final LOGV:Z

.field private static volatile sLogFile:Lcom/google/android/music/log/LogFile;

.field private static sLogFilesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/log/LogFile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LOG_FILE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/log/Log;->LOGV:Z

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/log/Log;->sLogFilesMap:Ljava/util/Map;

    return-void
.end method

.method public static declared-synchronized configure(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p0, "dir"    # Ljava/io/File;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 195
    const-class v2, Lcom/google/android/music/log/Log;

    monitor-enter v2

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 196
    :cond_0
    :try_start_0
    const-string v1, "Log"

    const-string v3, "Invalid configuration provided"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    :goto_0
    monitor-exit v2

    return-void

    .line 201
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/music/log/LogFile;

    const-string v3, "com.google.android.music"

    invoke-direct {v1, v3, p0, p1}, Lcom/google/android/music/log/LogFile;-><init>(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    const-string v1, "Log"

    const-string v3, "Failed to create log file."

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 195
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized configureLogFile(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 5
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 208
    const-class v3, Lcom/google/android/music/log/Log;

    monitor-enter v3

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 209
    :cond_0
    :try_start_0
    const-string v2, "Log"

    const-string v4, "Invalid configuration provided"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :goto_0
    monitor-exit v3

    return-void

    .line 214
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/music/log/LogFile;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/music/log/LogFile;-><init>(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    .line 215
    .local v1, "logFile":Lcom/google/android/music/log/LogFile;
    sget-object v2, Lcom/google/android/music/log/Log;->sLogFilesMap:Ljava/util/Map;

    invoke-interface {v2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 216
    .end local v1    # "logFile":Lcom/google/android/music/log/LogFile;
    :catch_0
    move-exception v0

    .line 217
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    const-string v2, "Log"

    const-string v4, "Failed to create log file."

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 208
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 82
    invoke-static {p0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 83
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 84
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    :cond_0
    return-void
.end method

.method public static declared-synchronized dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p0, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 186
    const-class v3, Lcom/google/android/music/log/Log;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v2, :cond_0

    .line 187
    sget-object v2, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v2, p0}, Lcom/google/android/music/log/LogFile;->dump(Ljava/io/PrintWriter;)V

    .line 188
    sget-object v2, Lcom/google/android/music/log/Log;->sLogFilesMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/log/LogFile;

    .line 189
    .local v1, "log":Lcom/google/android/music/log/LogFile;
    invoke-virtual {v1, p0}, Lcom/google/android/music/log/LogFile;->dump(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 186
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "log":Lcom/google/android/music/log/LogFile;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 192
    :cond_0
    monitor-exit v3

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 126
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/log/LogFile;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 131
    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 132
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 133
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/log/LogFile;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 135
    :cond_0
    return-void
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 163
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/log/LogFile;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_0
    return-void
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 180
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 181
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/log/LogFile;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 183
    :cond_0
    return-void
.end method

.method public static declared-synchronized getLogFile(Ljava/lang/String;)Lcom/google/android/music/log/LogFile;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 222
    const-class v1, Lcom/google/android/music/log/Log;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFilesMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/log/LogFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/log/LogFile;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 96
    invoke-static {p0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 97
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/log/LogFile;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 100
    :cond_0
    return-void
.end method

.method public static isLoggable(Ljava/lang/String;I)Z
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "level"    # I

    .prologue
    .line 57
    invoke-static {p0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/log/LogFile;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 68
    invoke-static {p0, p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 69
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 70
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/log/LogFile;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 105
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/log/LogFile;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 110
    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 111
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/log/LogFile;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 114
    :cond_0
    return-void
.end method

.method public static wtf(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-static {p0, p1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/log/LogFile;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    return-void
.end method

.method public static wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 152
    invoke-static {p0, p1, p2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 153
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v0, :cond_0

    .line 154
    sget-object v0, Lcom/google/android/music/log/Log;->sLogFile:Lcom/google/android/music/log/LogFile;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/music/log/LogFile;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 156
    :cond_0
    return-void
.end method
