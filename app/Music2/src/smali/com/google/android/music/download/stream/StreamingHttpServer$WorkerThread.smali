.class Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
.super Ljava/lang/Thread;
.source "StreamingHttpServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/stream/StreamingHttpServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkerThread"
.end annotation


# instance fields
.field private mHandler:Lcom/google/android/music/download/stream/StreamRequestHandler;

.field private final mSocket:Ljava/net/Socket;

.field final synthetic this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/stream/StreamingHttpServer;Ljava/net/Socket;)V
    .locals 1
    .param p2, "socket"    # Ljava/net/Socket;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    .line 190
    const-string v0, "StreamingHttpServer.WorkerThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 191
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->setDaemon(Z)V

    .line 192
    iput-object p2, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    .line 193
    return-void
.end method


# virtual methods
.method public getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mHandler:Lcom/google/android/music/download/stream/StreamRequestHandler;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mHandler:Lcom/google/android/music/download/stream/StreamRequestHandler;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamRequestHandler;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v0

    .line 254
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 10

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 198
    .local v0, "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    new-instance v7, Lcom/google/android/music/download/stream/StreamRequestHandler;

    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;
    invoke-static {v8}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$400(Lcom/google/android/music/download/stream/StreamingHttpServer;)Lcom/google/android/music/download/stream/AllowedStreams;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/music/download/stream/StreamRequestHandler;-><init>(Lcom/google/android/music/download/stream/AllowedStreams;)V

    iput-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mHandler:Lcom/google/android/music/download/stream/StreamRequestHandler;

    .line 200
    :try_start_0
    new-instance v1, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v1}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 202
    .end local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .local v1, "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    :try_start_1
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    iget-object v8, v8, Lcom/google/android/music/download/stream/StreamingHttpServer;->mParams:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v1, v7, v8}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 204
    new-instance v4, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v4}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 205
    .local v4, "processor":Lorg/apache/http/protocol/BasicHttpProcessor;
    new-instance v7, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v7}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v4, v7}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 206
    new-instance v7, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v7}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v4, v7}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 208
    new-instance v5, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v5}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    .line 210
    .local v5, "registry":Lorg/apache/http/protocol/HttpRequestHandlerRegistry;
    const-string v7, "*"

    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mHandler:Lcom/google/android/music/download/stream/StreamRequestHandler;

    invoke-virtual {v5, v7, v8}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 212
    new-instance v6, Lorg/apache/http/protocol/HttpService;

    new-instance v7, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v7}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v8, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v8}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v6, v4, v7, v8}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    .line 215
    .local v6, "service":Lorg/apache/http/protocol/HttpService;
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    iget-object v7, v7, Lcom/google/android/music/download/stream/StreamingHttpServer;->mParams:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v6, v7}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 216
    invoke-virtual {v6, v5}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 218
    new-instance v3, Lorg/apache/http/protocol/BasicHttpContext;

    const/4 v7, 0x0

    invoke-direct {v3, v7}, Lorg/apache/http/protocol/BasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    .line 220
    .local v3, "httpContext":Lorg/apache/http/protocol/HttpContext;
    invoke-virtual {v6, v1, v3}, Lorg/apache/http/protocol/HttpService;->handleRequest(Lorg/apache/http/HttpServerConnection;Lorg/apache/http/protocol/HttpContext;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 236
    if-eqz v1, :cond_1

    .line 237
    :try_start_2
    invoke-virtual {v1}, Lorg/apache/http/impl/DefaultHttpServerConnection;->shutdown()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 244
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v7}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v8

    monitor-enter v8

    .line 245
    :try_start_3
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v7}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 246
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v0, v1

    .line 248
    .end local v1    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v3    # "httpContext":Lorg/apache/http/protocol/HttpContext;
    .end local v4    # "processor":Lorg/apache/http/protocol/BasicHttpProcessor;
    .end local v5    # "registry":Lorg/apache/http/protocol/HttpRequestHandlerRegistry;
    .end local v6    # "service":Lorg/apache/http/protocol/HttpService;
    .restart local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    :goto_1
    return-void

    .line 238
    .end local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v1    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v3    # "httpContext":Lorg/apache/http/protocol/HttpContext;
    .restart local v4    # "processor":Lorg/apache/http/protocol/BasicHttpProcessor;
    .restart local v5    # "registry":Lorg/apache/http/protocol/HttpRequestHandlerRegistry;
    .restart local v6    # "service":Lorg/apache/http/protocol/HttpService;
    :cond_1
    :try_start_4
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    if-eqz v7, :cond_0

    .line 239
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 241
    :catch_0
    move-exception v7

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v7

    :try_start_5
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v7

    .line 221
    .end local v1    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v3    # "httpContext":Lorg/apache/http/protocol/HttpContext;
    .end local v4    # "processor":Lorg/apache/http/protocol/BasicHttpProcessor;
    .end local v5    # "registry":Lorg/apache/http/protocol/HttpRequestHandlerRegistry;
    .end local v6    # "service":Lorg/apache/http/protocol/HttpService;
    .restart local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    :catch_1
    move-exception v2

    .line 226
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_6
    instance-of v7, v2, Lorg/apache/http/ConnectionClosedException;

    if-nez v7, :cond_2

    instance-of v7, v2, Ljava/net/SocketException;

    if-eqz v7, :cond_5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Connection reset by peer"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 229
    :cond_2
    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$300()Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "StreamingHttpServer"

    const-string v8, "StreamingHttpServer.Worker connection closed"

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 236
    :cond_3
    :goto_3
    if-eqz v0, :cond_7

    .line 237
    :try_start_7
    invoke-virtual {v0}, Lorg/apache/http/impl/DefaultHttpServerConnection;->shutdown()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 244
    :cond_4
    :goto_4
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v7}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v8

    monitor-enter v8

    .line 245
    :try_start_8
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v7}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 246
    monitor-exit v8

    goto :goto_1

    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v7

    .line 231
    :cond_5
    :try_start_9
    const-string v7, "StreamingHttpServer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HTTP server disrupted: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_3

    .line 235
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_2
    move-exception v7

    .line 236
    :goto_5
    if-eqz v0, :cond_8

    .line 237
    :try_start_a
    invoke-virtual {v0}, Lorg/apache/http/impl/DefaultHttpServerConnection;->shutdown()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 244
    :cond_6
    :goto_6
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v8}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v8

    monitor-enter v8

    .line 245
    :try_start_b
    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v9}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 246
    monitor-exit v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v7

    .line 238
    .restart local v2    # "e":Ljava/lang/Exception;
    :cond_7
    :try_start_c
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    if-eqz v7, :cond_4

    .line 239
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/Socket;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    goto :goto_4

    .line 241
    :catch_2
    move-exception v7

    goto :goto_4

    .line 238
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_8
    :try_start_d
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    if-eqz v8, :cond_6

    .line 239
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    invoke-virtual {v8}, Ljava/net/Socket;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    goto :goto_6

    .line 241
    :catch_3
    move-exception v8

    goto :goto_6

    .line 246
    :catchall_3
    move-exception v7

    :try_start_e
    monitor-exit v8
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    throw v7

    .line 235
    .end local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v1    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    :catchall_4
    move-exception v7

    move-object v0, v1

    .end local v1    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    goto :goto_5

    .line 221
    .end local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v1    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    goto/16 :goto_2
.end method

.method public shutdown()V
    .locals 3

    .prologue
    .line 259
    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    const-string v0, "StreamingHttpServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Worker.shutdown() for request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->interrupt()V

    .line 264
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_0
    return-void

    .line 265
    :catch_0
    move-exception v0

    goto :goto_0
.end method
