.class Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackLauncherActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackLauncherActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RecentActivityAdapter"
.end annotation


# instance fields
.field private final mNowPlayingItem:Lcom/google/android/music/leanback/Item;

.field private mOffset:I

.field private final mRecentsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mSize:I


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 2
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;
    .param p2, "nowPlayingItem"    # Lcom/google/android/music/leanback/Item;
    .param p3, "recentsAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 879
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 880
    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mNowPlayingItem:Lcom/google/android/music/leanback/Item;

    .line 881
    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mRecentsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 882
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->recalculateSize()V

    .line 883
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mRecentsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 907
    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    .prologue
    .line 871
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->recalculateSize()V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    .prologue
    .line 871
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;

    .prologue
    .line 871
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 871
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 871
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 871
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method private recalculateSize()V
    .locals 2

    .prologue
    .line 945
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->shouldShowNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    .line 946
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mRecentsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mSize:I

    .line 947
    return-void

    .line 945
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowNowPlaying()Z
    .locals 2

    .prologue
    .line 950
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v0

    .line 951
    .local v0, "state":Lcom/google/android/music/playback/PlaybackState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/playback/PlaybackState;->getQueueSize()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 929
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mSize:I

    if-ge p1, v0, :cond_1

    .line 930
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    if-lt p1, v0, :cond_0

    .line 931
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mRecentsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 936
    :goto_0
    return-object v0

    .line 934
    :cond_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 935
    if-nez p1, :cond_1

    .line 936
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mNowPlayingItem:Lcom/google/android/music/leanback/Item;

    goto :goto_0

    .line 940
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get unexpected position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onNowPlayingChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 911
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    .line 912
    .local v0, "offset":I
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->recalculateSize()V

    .line 913
    iget v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    if-ge v0, v1, :cond_0

    .line 914
    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeInserted(II)V

    .line 920
    :goto_0
    return-void

    .line 915
    :cond_0
    iget v1, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mOffset:I

    if-le v0, v1, :cond_1

    .line 916
    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeRemoved(II)V

    goto :goto_0

    .line 918
    :cond_1
    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->notifyItemRangeChanged(II)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 924
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackLauncherActivity$RecentActivityAdapter;->mSize:I

    return v0
.end method
