.class Lcom/google/android/music/preferences/MusicPreferences$3;
.super Ljava/lang/Object;
.source "MusicPreferences.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/preferences/MusicPreferences;->switchAccountAsync(Landroid/accounts/Account;Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/preferences/MusicPreferences;

.field final synthetic val$newAccount:Landroid/accounts/Account;

.field final synthetic val$oldAccount:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/music/preferences/MusicPreferences;Landroid/accounts/Account;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 911
    iput-object p1, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    iput-object p2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$newAccount:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$oldAccount:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 916
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->clearPlayQueue(Landroid/content/Context;)V

    .line 917
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent;->deleteAllRemoteContent(Landroid/content/ContentResolver;)V

    .line 918
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->deleteAllServerSettings()V

    .line 919
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.music.accountchanged"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 920
    .local v0, "accountChangedIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$newAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_0

    .line 921
    const-string v2, "com.google.android.music.newaccount"

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$newAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 924
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$oldAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_1

    .line 925
    const-string v2, "com.google.android.music.oldaccount"

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$oldAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 928
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/preferences/MusicPreferences;->access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 931
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$newAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_2

    .line 932
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$newAccount:Landroid/accounts/Account;

    const-string v3, "com.google.android.music.MusicContent"

    invoke-static {v2, v3, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 933
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$newAccount:Landroid/accounts/Account;

    const-string v3, "com.google.android.music.MusicContent"

    invoke-static {v2, v3, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 934
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 935
    .local v1, "syncExtras":Landroid/os/Bundle;
    const-string v2, "expedited"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 936
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences$3;->val$newAccount:Landroid/accounts/Account;

    const-string v3, "com.google.android.music.MusicContent"

    invoke-static {v2, v3, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 938
    .end local v1    # "syncExtras":Landroid/os/Bundle;
    :cond_2
    return-void
.end method
