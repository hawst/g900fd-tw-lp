.class Lcom/google/android/music/activities/CreateMixActivity$1;
.super Ljava/lang/Object;
.source "CreateMixActivity.java"

# interfaces
.implements Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/activities/CreateMixActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/activities/CreateMixActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/activities/CreateMixActivity;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;)V
    .locals 2
    .param p1, "errorMsg"    # Ljava/lang/String;

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/music/activities/CreateMixActivity$1$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/activities/CreateMixActivity$1$2;-><init>(Lcom/google/android/music/activities/CreateMixActivity$1;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    invoke-virtual {v1}, Lcom/google/android/music/activities/CreateMixActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 92
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "mixName"    # Ljava/lang/String;

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/music/activities/CreateMixActivity$1$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/activities/CreateMixActivity$1$3;-><init>(Lcom/google/android/music/activities/CreateMixActivity$1;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    invoke-virtual {v1}, Lcom/google/android/music/activities/CreateMixActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 110
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/music/activities/CreateMixActivity$1$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/activities/CreateMixActivity$1$1;-><init>(Lcom/google/android/music/activities/CreateMixActivity$1;)V

    iget-object v1, p0, Lcom/google/android/music/activities/CreateMixActivity$1;->this$0:Lcom/google/android/music/activities/CreateMixActivity;

    invoke-virtual {v1}, Lcom/google/android/music/activities/CreateMixActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 69
    return-void
.end method
