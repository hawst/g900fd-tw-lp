.class public Lcom/google/android/music/playback/StopWatch;
.super Ljava/lang/Object;
.source "StopWatch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/StopWatch$Listener;
    }
.end annotation


# instance fields
.field private mCumulativeTime:J

.field private mIsRunning:Z

.field private mListener:Lcom/google/android/music/playback/StopWatch$Listener;

.field private mStart:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-wide v0, p0, Lcom/google/android/music/playback/StopWatch;->mCumulativeTime:J

    .line 11
    iput-wide v0, p0, Lcom/google/android/music/playback/StopWatch;->mStart:J

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/StopWatch;->mIsRunning:Z

    .line 15
    return-void
.end method


# virtual methods
.method public declared-synchronized getTime()J
    .locals 6

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/music/playback/StopWatch;->mIsRunning:Z

    if-nez v2, :cond_0

    .line 56
    iget-wide v2, p0, Lcom/google/android/music/playback/StopWatch;->mCumulativeTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :goto_0
    monitor-exit p0

    return-wide v2

    .line 58
    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 59
    .local v0, "now":J
    iget-wide v2, p0, Lcom/google/android/music/playback/StopWatch;->mCumulativeTime:J

    iget-wide v4, p0, Lcom/google/android/music/playback/StopWatch;->mStart:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long v4, v0, v4

    add-long/2addr v2, v4

    goto :goto_0

    .line 55
    .end local v0    # "now":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public pause()V
    .locals 8

    .prologue
    .line 40
    const/4 v0, 0x0

    .line 41
    .local v0, "listener":Lcom/google/android/music/playback/StopWatch$Listener;
    monitor-enter p0

    .line 42
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/music/playback/StopWatch;->mIsRunning:Z

    if-eqz v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/music/playback/StopWatch;->mListener:Lcom/google/android/music/playback/StopWatch$Listener;

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 45
    .local v2, "now":J
    iget-wide v4, p0, Lcom/google/android/music/playback/StopWatch;->mCumulativeTime:J

    iget-wide v6, p0, Lcom/google/android/music/playback/StopWatch;->mStart:J

    sub-long v6, v2, v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/music/playback/StopWatch;->mCumulativeTime:J

    .line 46
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/playback/StopWatch;->mIsRunning:Z

    .line 48
    .end local v2    # "now":J
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    if-eqz v0, :cond_1

    .line 50
    invoke-interface {v0, p0}, Lcom/google/android/music/playback/StopWatch$Listener;->onPause(Lcom/google/android/music/playback/StopWatch;)V

    .line 52
    :cond_1
    return-void

    .line 48
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 65
    .local v0, "listener":Lcom/google/android/music/playback/StopWatch$Listener;
    monitor-enter p0

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/StopWatch;->mListener:Lcom/google/android/music/playback/StopWatch$Listener;

    .line 67
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/playback/StopWatch;->mCumulativeTime:J

    .line 68
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/StopWatch;->mStart:J

    .line 69
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    if-eqz v0, :cond_0

    .line 71
    invoke-interface {v0, p0}, Lcom/google/android/music/playback/StopWatch$Listener;->onReset(Lcom/google/android/music/playback/StopWatch;)V

    .line 73
    :cond_0
    return-void

    .line 69
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public start()V
    .locals 4

    .prologue
    .line 26
    const/4 v0, 0x0

    .line 27
    .local v0, "listener":Lcom/google/android/music/playback/StopWatch$Listener;
    monitor-enter p0

    .line 28
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/music/playback/StopWatch;->mIsRunning:Z

    if-nez v1, :cond_0

    .line 29
    iget-object v0, p0, Lcom/google/android/music/playback/StopWatch;->mListener:Lcom/google/android/music/playback/StopWatch$Listener;

    .line 30
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/playback/StopWatch;->mStart:J

    .line 31
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/playback/StopWatch;->mIsRunning:Z

    .line 33
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    if-eqz v0, :cond_1

    .line 35
    invoke-interface {v0, p0}, Lcom/google/android/music/playback/StopWatch$Listener;->onStart(Lcom/google/android/music/playback/StopWatch;)V

    .line 37
    :cond_1
    return-void

    .line 33
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
