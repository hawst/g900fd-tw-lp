.class Lcom/google/android/music/eventlog/MusicEventLogger$2;
.super Ljava/lang/Object;
.source "MusicEventLogger.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/eventlog/MusicEventLogger;->logPlayEventAsync(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

.field final synthetic val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field final synthetic val$duration:J

.field final synthetic val$isExplicit:Z

.field final synthetic val$isRemote:Z

.field final synthetic val$musicFile:Lcom/google/android/music/store/MusicFile;

.field final synthetic val$position:J


# direct methods
.method constructor <init>(Lcom/google/android/music/eventlog/MusicEventLogger;Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iput-object p2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iput-object p3, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$musicFile:Lcom/google/android/music/store/MusicFile;

    iput-boolean p4, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$isRemote:Z

    iput-boolean p5, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$isExplicit:Z

    iput-wide p6, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$position:J

    iput-wide p8, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$duration:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 319
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-object v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$musicFile:Lcom/google/android/music/store/MusicFile;

    iget-boolean v4, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$isRemote:Z

    iget-boolean v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$isExplicit:Z

    iget-wide v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$position:J

    iget-wide v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger$2;->val$duration:J

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->logPlayEvent(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V

    .line 321
    return-void
.end method
