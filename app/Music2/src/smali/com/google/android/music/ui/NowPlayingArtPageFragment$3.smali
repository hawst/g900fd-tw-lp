.class Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;
.super Landroid/content/BroadcastReceiver;
.source "NowPlayingArtPageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingArtPageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 139
    const-string v2, "ListPosition"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 145
    .local v0, "currentPos":J
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mQueuePosition:I
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$700(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mQueuePosition:I
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$700(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)I

    move-result v2

    int-to-long v4, v2

    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    # setter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mIsCurrentlyActive:Z
    invoke-static {v3, v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$102(Lcom/google/android/music/ui/NowPlayingArtPageFragment;Z)Z

    .line 147
    const-string v2, "com.android.music.metachanged"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateArtVisibility()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$800(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 149
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeVisibility()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$900(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 151
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$3;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->startPanningIfNeeded()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$1000(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 152
    return-void

    .line 145
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
