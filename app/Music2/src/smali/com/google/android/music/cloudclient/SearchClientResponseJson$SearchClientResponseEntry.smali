.class public Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
.super Ljava/lang/Object;
.source "SearchClientResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/SearchClientResponseJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchClientResponseEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    }
.end annotation


# instance fields
.field public mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "album"
    .end annotation
.end field

.field public mArtist:Lcom/google/android/music/cloudclient/ArtistJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist"
    .end annotation
.end field

.field public mGenre:Lcom/google/android/music/cloudclient/MusicGenreJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "music_genre"
    .end annotation
.end field

.field public mNavigationalConfidence:F
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "navigational_confidence"
    .end annotation
.end field

.field public mNavigationalResult:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "navigational_result"
    .end annotation
.end field

.field public mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlist"
    .end annotation
.end field

.field public mStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "station"
    .end annotation
.end field

.field public mTrack:Lcom/google/android/music/sync/google/model/Track;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "track"
    .end annotation
.end field

.field public mType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public getType()Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    .locals 3

    .prologue
    .line 60
    iget-object v1, p0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mType:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 61
    .local v0, "ordinal":I
    invoke-static {}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->values()[Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    return-object v1
.end method
