.class Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;
.super Ljava/lang/Object;
.source "PlayQueueFeeder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/mix/PlayQueueFeeder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MixRequestData"
.end annotation


# instance fields
.field private final mAction:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

.field private final mCanStream:Z

.field private final mMix:Lcom/google/android/music/mix/MixDescriptor;

.field private final mRecentlyPlayed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 0
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "canStream"    # Z
    .param p4, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/mix/MixDescriptor;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;Z",
            "Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 624
    .local p2, "recentlyPlayed":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 625
    iput-object p1, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    .line 626
    iput-object p2, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mRecentlyPlayed:Ljava/util/List;

    .line 627
    iput-boolean p3, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mCanStream:Z

    .line 628
    iput-object p4, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mAction:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .line 629
    return-void
.end method


# virtual methods
.method public getCanStream()Z
    .locals 1

    .prologue
    .line 640
    iget-boolean v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mCanStream:Z

    return v0
.end method

.method public getMixDescriptor()Lcom/google/android/music/mix/MixDescriptor;
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    return-object v0
.end method

.method public getPostProcessingAction()Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mAction:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    return-object v0
.end method

.method public getRecentlyPlayed()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->mRecentlyPlayed:Ljava/util/List;

    return-object v0
.end method
