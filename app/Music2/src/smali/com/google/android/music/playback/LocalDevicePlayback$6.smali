.class Lcom/google/android/music/playback/LocalDevicePlayback$6;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Lcom/google/android/music/mix/PlayQueueFeederListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 4
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p2, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    .line 537
    sget-object v1, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    invoke-virtual {p2}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 563
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unhandled listener action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 539
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->handleMediaButtonSeek(Z)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1300(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 565
    :goto_0
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v0

    .line 566
    .local v0, "mixState":Lcom/google/android/music/mix/MixGenerationState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixGenerationState;->getMix()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/music/mix/MixDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567
    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->FAILED:Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v0, v1}, Lcom/google/android/music/mix/MixGenerationState;->setState(Lcom/google/android/music/mix/MixGenerationState$State;)V

    .line 570
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 571
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$6$5;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$6$5;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$6;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 582
    return-void

    .line 549
    .end local v0    # "mixState":Lcom/google/android/music/mix/MixGenerationState;
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 550
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$6$4;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$6$4;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$6;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 537
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onSuccess(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 5
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p2, "newMix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p4, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    const/4 v3, -0x1

    const/4 v4, 0x1

    .line 445
    sget-object v1, Lcom/google/android/music/playback/LocalDevicePlayback$37;->$SwitchMap$com$google$android$music$mix$PlayQueueFeeder$PostProcessingAction:[I

    invoke-virtual {p4}, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 505
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unhandled listener action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 447
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1, p3, v3, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    .line 508
    :goto_0
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v0

    .line 509
    .local v0, "mixState":Lcom/google/android/music/mix/MixGenerationState;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/music/mix/MixGenerationState;->isMyMix(Lcom/google/android/music/mix/MixDescriptor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 510
    if-eqz p2, :cond_2

    .line 511
    invoke-virtual {v0, p2}, Lcom/google/android/music/mix/MixGenerationState;->transitionToNewMix(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 516
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 517
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$6$3;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$6$3;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$6;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 533
    .end local v0    # "mixState":Lcom/google/android/music/mix/MixGenerationState;
    :goto_2
    return-void

    .line 450
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->queue(Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;
    invoke-static {v1, p3, v4, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1200(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;

    .line 451
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->handleMediaButtonSeek(Z)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1300(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    goto :goto_0

    .line 454
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->queue(Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;
    invoke-static {v1, p3, v4, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1200(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;

    goto :goto_0

    .line 461
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 462
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$6$1;

    invoke-direct {v2, p0, p3}, Lcom/google/android/music/playback/LocalDevicePlayback$6$1;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$6;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 474
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->next()V

    goto :goto_2

    .line 482
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v1

    if-nez v1, :cond_1

    .line 483
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-string v2, "com.google.android.music.refreshfailed"

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    goto :goto_2

    .line 486
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1, p3, v3, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    .line 491
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$6;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 492
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$6$2;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$6$2;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$6;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 513
    .restart local v0    # "mixState":Lcom/google/android/music/mix/MixGenerationState;
    :cond_2
    sget-object v1, Lcom/google/android/music/mix/MixGenerationState$State;->FINISHED:Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v0, v1}, Lcom/google/android/music/mix/MixGenerationState;->setState(Lcom/google/android/music/mix/MixGenerationState$State;)V

    goto :goto_1

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
