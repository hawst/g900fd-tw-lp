.class final Lcom/google/android/music/utils/MusicUtils$1;
.super Landroid/database/ContentObserver;
.source "MusicUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/MusicUtils;->openPlaylistCursor(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Handler;

    .prologue
    .line 865
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 868
    # getter for: Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->access$000()Lcom/google/android/music/AsyncCursor;

    move-result-object v1

    monitor-enter v1

    .line 869
    :try_start_0
    # getter for: Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->access$000()Lcom/google/android/music/AsyncCursor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/AsyncCursor;->requery()Z

    .line 870
    # getter for: Lcom/google/android/music/utils/MusicUtils;->sPlaylists:Lcom/google/android/music/AsyncCursor;
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->access$000()Lcom/google/android/music/AsyncCursor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/AsyncCursor;->moveToFirst()Z

    .line 871
    monitor-exit v1

    .line 872
    return-void

    .line 871
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
