.class public Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;
.super Ljava/lang/Object;
.source "DocumentMenuHandler.java"

# interfaces
.implements Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DocumentContextMenuDelegate"
.end annotation


# instance fields
.field private mMenuHandler:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

.field private final mMusicFragment:Lcom/google/android/music/ui/MusicFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/MusicFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/android/music/ui/MusicFragment;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    .line 128
    return-void
.end method


# virtual methods
.method public dismissContextMenu()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->mMenuHandler:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->mMenuHandler:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->dismissPopupMenu()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->mMenuHandler:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 142
    :cond_0
    return-void
.end method

.method public onContextMenuPressed(Lcom/google/android/music/ui/cardlib/layout/PlayCardView;Landroid/view/View;)V
    .locals 3
    .param p1, "cardView"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .param p2, "anchor"    # Landroid/view/View;

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->getDocument()Lcom/google/android/music/ui/cardlib/PlayDocument;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 133
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-direct {v1, v2, v0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;)V

    iput-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->mMenuHandler:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 134
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->mMenuHandler:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    invoke-virtual {v1, p2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->showPopupMenu(Landroid/view/View;)V

    .line 135
    return-void
.end method
