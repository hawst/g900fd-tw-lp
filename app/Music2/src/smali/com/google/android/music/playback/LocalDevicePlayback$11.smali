.class Lcom/google/android/music/playback/LocalDevicePlayback$11;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 1508
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$11;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 1511
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$11;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1512
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Service connected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514
    :cond_0
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 1515
    .local v0, "className":Ljava/lang/String;
    const-class v1, Lcom/google/android/music/download/TrackDownloadQueueService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1516
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$11;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-static {p2}, Lcom/google/android/music/download/IDownloadQueueManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/IDownloadQueueManager;

    move-result-object v2

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mDownloadQueueManager:Lcom/google/android/music/download/IDownloadQueueManager;
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5302(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/IDownloadQueueManager;)Lcom/google/android/music/download/IDownloadQueueManager;

    .line 1520
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$11;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->tryCreatingStreamingSchedulingClient()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5400(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 1521
    return-void

    .line 1518
    :cond_1
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown connection to class"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 1525
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 1526
    .local v0, "className":Ljava/lang/String;
    const-class v1, Lcom/google/android/music/download/TrackDownloadQueueService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1527
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown disconnection from class"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 1529
    :cond_0
    return-void
.end method
