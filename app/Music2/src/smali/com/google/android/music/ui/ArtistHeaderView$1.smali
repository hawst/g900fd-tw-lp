.class Lcom/google/android/music/ui/ArtistHeaderView$1;
.super Ljava/lang/Object;
.source "ArtistHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ArtistHeaderView;->setAlbumList(Lcom/google/android/music/medialist/AlbumList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mSavedArtistArtUrl:Ljava/lang/String;

.field private mSavedArtistId:J

.field private mSavedArtistName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/ArtistHeaderView;

.field final synthetic val$albumlist:Lcom/google/android/music/medialist/AlbumList;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ArtistHeaderView;Lcom/google/android/music/medialist/AlbumList;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    iput-object p2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 102
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ArtistHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 103
    .local v0, "context":Landroid/content/Context;
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v3}, Lcom/google/android/music/ui/ArtistHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/medialist/AlbumList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistName:Ljava/lang/String;

    .line 104
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v3}, Lcom/google/android/music/ui/ArtistHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/medialist/AlbumList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistId:J

    .line 106
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    instance-of v2, v2, Lcom/google/android/music/medialist/NautilusAlbumList;

    if-eqz v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    check-cast v2, Lcom/google/android/music/medialist/NautilusAlbumList;

    invoke-virtual {v2}, Lcom/google/android/music/medialist/NautilusAlbumList;->getNautilusId()Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "nautilusId":Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->getNautilusArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistArtUrl:Ljava/lang/String;

    .line 113
    .end local v1    # "nautilusId":Ljava/lang/String;
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistId:J

    invoke-static {v0, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->getArtistArtUrl(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistArtUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    .line 117
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ArtistHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistName:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/ArtistHeaderView;->mArtistName:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistHeaderView;->access$002(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)Ljava/lang/String;

    .line 120
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    iget-wide v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistId:J

    # setter for: Lcom/google/android/music/ui/ArtistHeaderView;->mArtistId:J
    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/ArtistHeaderView;->access$102(Lcom/google/android/music/ui/ArtistHeaderView;J)J

    .line 121
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistArtUrl:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/ArtistHeaderView;->mArtistArtUrl:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistHeaderView;->access$202(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistArtUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 124
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->mSavedArtistArtUrl:Ljava/lang/String;

    # invokes: Lcom/google/android/music/ui/ArtistHeaderView;->getBitmap(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistHeaderView;->access$300(Lcom/google/android/music/ui/ArtistHeaderView;Ljava/lang/String;)V

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    instance-of v1, v1, Lcom/google/android/music/medialist/NautilusAlbumList;

    if-eqz v1, :cond_3

    .line 128
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->val$albumlist:Lcom/google/android/music/medialist/AlbumList;

    check-cast v1, Lcom/google/android/music/medialist/NautilusAlbumList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/NautilusAlbumList;->getNautilusId()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "nautilusId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    new-instance v2, Lcom/google/android/music/medialist/NautilusArtistSongList;

    iget-object v3, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    # getter for: Lcom/google/android/music/ui/ArtistHeaderView;->mArtistName:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/ui/ArtistHeaderView;->access$000(Lcom/google/android/music/ui/ArtistHeaderView;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    # setter for: Lcom/google/android/music/ui/ArtistHeaderView;->mPlayRadioSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/ArtistHeaderView;->access$402(Lcom/google/android/music/ui/ArtistHeaderView;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/medialist/SongList;

    .line 134
    .end local v0    # "nautilusId":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    # getter for: Lcom/google/android/music/ui/ArtistHeaderView;->mArtistId:J
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistHeaderView;->access$100(Lcom/google/android/music/ui/ArtistHeaderView;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    # getter for: Lcom/google/android/music/ui/ArtistHeaderView;->mRadioButtonWrapper:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistHeaderView;->access$500(Lcom/google/android/music/ui/ArtistHeaderView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v1, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    # getter for: Lcom/google/android/music/ui/ArtistHeaderView;->mShuffleButtonWrapper:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistHeaderView;->access$600(Lcom/google/android/music/ui/ArtistHeaderView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 131
    :cond_3
    iget-object v7, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    new-instance v1, Lcom/google/android/music/medialist/ArtistSongList;

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    # getter for: Lcom/google/android/music/ui/ArtistHeaderView;->mArtistId:J
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistHeaderView;->access$100(Lcom/google/android/music/ui/ArtistHeaderView;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/ui/ArtistHeaderView$1;->this$0:Lcom/google/android/music/ui/ArtistHeaderView;

    # getter for: Lcom/google/android/music/ui/ArtistHeaderView;->mArtistName:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/ui/ArtistHeaderView;->access$000(Lcom/google/android/music/ui/ArtistHeaderView;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    # setter for: Lcom/google/android/music/ui/ArtistHeaderView;->mPlayRadioSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v7, v1}, Lcom/google/android/music/ui/ArtistHeaderView;->access$402(Lcom/google/android/music/ui/ArtistHeaderView;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/medialist/SongList;

    goto :goto_1
.end method
