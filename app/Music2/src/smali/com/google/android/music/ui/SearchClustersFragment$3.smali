.class Lcom/google/android/music/ui/SearchClustersFragment$3;
.super Ljava/lang/Object;
.source "SearchClustersFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SearchClustersFragment;->getSuggestedQuery()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final columns:[Ljava/lang/String;

.field private mSearchStringForSuggestedQuery:Ljava/lang/String;

.field private mSuggestedQuery:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/SearchClustersFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SearchClustersFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    iput-object p1, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "suggestedQuery"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->columns:[Ljava/lang/String;

    .line 185
    iput-object v3, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->mSuggestedQuery:Ljava/lang/String;

    .line 186
    iput-object v3, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->mSearchStringForSuggestedQuery:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 189
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/SearchClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    iget-object v0, v0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->mSearchStringForSuggestedQuery:Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/SearchClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->mSearchStringForSuggestedQuery:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$SearchSuggestedQuery;->getSearchUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->columns:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 198
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->mSuggestedQuery:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/SearchClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    iget-object v0, v0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->mSearchStringForSuggestedQuery:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment$3;->mSuggestedQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/SearchClustersFragment;->setSuggestedQuery(Ljava/lang/String;)V

    goto :goto_0
.end method
