.class public final Lcom/google/android/music/wear/WearDataTransferConnectionController;
.super Ljava/lang/Object;
.source "WearDataTransferConnectionController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

.field private final mImageResizer:Lcom/google/android/music/wear/ImageResizer;


# direct methods
.method constructor <init>(Lcom/google/android/music/wear/WearMusicDataProvider;Ljava/util/concurrent/Executor;Lcom/google/android/music/wear/ImageResizer;)V
    .locals 1
    .param p1, "dataProvider"    # Lcom/google/android/music/wear/WearMusicDataProvider;
    .param p2, "backgroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "imageResizer"    # Lcom/google/android/music/wear/ImageResizer;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "dataProvider cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/wear/WearMusicDataProvider;

    iput-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController;->mDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    .line 36
    const-string v0, "backgroundExecutor cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 38
    const-string v0, "imageResizer cannot be null"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/wear/ImageResizer;

    iput-object v0, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController;->mImageResizer:Lcom/google/android/music/wear/ImageResizer;

    .line 39
    return-void
.end method

.method private createFdForInputStream(Ljava/io/InputStream;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 7
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    const/4 v6, 0x0

    .line 114
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 120
    .local v1, "pipe":[Landroid/os/ParcelFileDescriptor;
    aget-object v2, v1, v6

    .line 121
    .local v2, "readPfd":Landroid/os/ParcelFileDescriptor;
    const/4 v4, 0x1

    aget-object v3, v1, v4

    .line 123
    .local v3, "writePfd":Landroid/os/ParcelFileDescriptor;
    iget-object v4, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v5, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;

    new-instance v6, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v6, v3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-direct {v5, p1, v6}, Lcom/google/android/music/wear/WearDataTransferConnectionController$StreamCopyRunnable;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 126
    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forSuccess(Landroid/os/ParcelFileDescriptor;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v4

    .end local v1    # "pipe":[Landroid/os/ParcelFileDescriptor;
    .end local v2    # "readPfd":Landroid/os/ParcelFileDescriptor;
    .end local v3    # "writePfd":Landroid/os/ParcelFileDescriptor;
    :goto_0
    return-object v4

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "WearTransferController"

    const-string v5, "Failed to open PFD pipe"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 117
    invoke-static {v6}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v4

    goto :goto_0
.end method

.method private getFdForArtwork(J)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 7
    .param p1, "artworkId"    # J

    .prologue
    const/4 v6, 0x0

    .line 91
    iget-object v3, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController;->mDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    invoke-interface {v3, p1, p2}, Lcom/google/android/music/wear/WearMusicDataProvider;->getArtworkFile(J)Ljava/io/File;

    move-result-object v0

    .line 92
    .local v0, "artworkFile":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController;->mImageResizer:Lcom/google/android/music/wear/ImageResizer;

    invoke-interface {v3, v0}, Lcom/google/android/music/wear/ImageResizer;->getResizedImageInputStream(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v2

    .line 95
    .local v2, "inputStream":Ljava/io/InputStream;
    invoke-direct {p0, v2}, Lcom/google/android/music/wear/WearDataTransferConnectionController;->createFdForInputStream(Ljava/io/InputStream;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 102
    .end local v2    # "inputStream":Ljava/io/InputStream;
    :goto_0
    return-object v3

    .line 96
    :catch_0
    move-exception v1

    .line 97
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v3, "WearTransferController"

    const-string v4, "Artwork file not found"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 98
    invoke-static {v6}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v3

    goto :goto_0

    .line 101
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    const-string v3, "WearTransferController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Artwork file doesn\'t exist for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-static {v6}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v3

    goto :goto_0
.end method

.method private getFdForTrack(J)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 3
    .param p1, "trackId"    # J

    .prologue
    .line 80
    iget-object v1, p0, Lcom/google/android/music/wear/WearDataTransferConnectionController;->mDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    invoke-interface {v1, p1, p2}, Lcom/google/android/music/wear/WearMusicDataProvider;->getDecryptedInputStream(J)Ljava/io/InputStream;

    move-result-object v0

    .line 81
    .local v0, "inputStream":Ljava/io/InputStream;
    if-nez v0, :cond_0

    .line 82
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v1

    .line 84
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/music/wear/WearDataTransferConnectionController;->createFdForInputStream(Ljava/io/InputStream;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public openFileDescriptor(Ljava/lang/String;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 9
    .param p1, "resourcePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 46
    const-string v5, "WearTransferController"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 47
    const-string v5, "WearTransferController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Open connection with path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    :cond_0
    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "pathParts":[Ljava/lang/String;
    array-length v5, v1

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    .line 52
    const-string v5, "WearTransferController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Malformed path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-static {v8}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v5

    .line 72
    :goto_0
    return-object v5

    .line 57
    :cond_1
    aget-object v4, v1, v8

    .line 60
    .local v4, "type":Ljava/lang/String;
    const/4 v5, 0x1

    :try_start_0
    aget-object v5, v1, v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 66
    .local v2, "entityId":J
    const-string v5, "tracks"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 67
    invoke-direct {p0, v2, v3}, Lcom/google/android/music/wear/WearDataTransferConnectionController;->getFdForTrack(J)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v5

    goto :goto_0

    .line 61
    .end local v2    # "entityId":J
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v5, "WearTransferController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Malformed entity ID: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-static {v8}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v5

    goto :goto_0

    .line 68
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "entityId":J
    :cond_2
    const-string v5, "artwork"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 69
    invoke-direct {p0, v2, v3}, Lcom/google/android/music/wear/WearDataTransferConnectionController;->getFdForArtwork(J)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v5

    goto :goto_0

    .line 71
    :cond_3
    const-string v5, "WearTransferController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown entity type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {v8}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v5

    goto :goto_0
.end method
