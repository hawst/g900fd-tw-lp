.class public Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson$SetGroupRelativeVolumeCommandBody;
.super Lcom/google/api/client/json/GenericJson;
.source "SetGroupRelativeVolumeCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/model/SetGroupRelativeVolumeCommandJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SetGroupRelativeVolumeCommandBody"
.end annotation


# instance fields
.field public mGroupId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groupId"
    .end annotation
.end field

.field public mVolumeDelta:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "volumeDelta"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
