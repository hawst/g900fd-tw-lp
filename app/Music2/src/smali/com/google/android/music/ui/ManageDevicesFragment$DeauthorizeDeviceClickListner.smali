.class Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;
.super Ljava/lang/Object;
.source "ManageDevicesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ManageDevicesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeauthorizeDeviceClickListner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ManageDevicesFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/ManageDevicesFragment;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/ManageDevicesFragment;Lcom/google/android/music/ui/ManageDevicesFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/ManageDevicesFragment$1;

    .prologue
    .line 334
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 338
    iget-object v6, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 339
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;

    .line 340
    .local v5, "vh":Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;
    iget-object v6, v5, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceName:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 341
    .local v4, "deviceName":Ljava/lang/CharSequence;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 342
    iget-object v6, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    const v7, 0x7f0b032e

    invoke-virtual {v6, v7}, Lcom/google/android/music/ui/ManageDevicesFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 344
    :cond_0
    iget-object v3, v5, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceId:Ljava/lang/String;

    .line 346
    .local v3, "deviceId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 348
    .local v1, "content":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mCurrentDeviceId:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$700(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mCurrentDeviceId:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$700(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 349
    const v6, 0x7f0b0329

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 356
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 357
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v6, 0x7f0b0328

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0b032c

    new-instance v8, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner$2;

    invoke-direct {v8, p0, v3}, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner$2;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0b032b

    new-instance v8, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner$1;

    invoke-direct {v8, p0}, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner$1;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 373
    return-void

    .line 351
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_1
    const v6, 0x7f0b032a

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "<b>%s</b>"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v4, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v2, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
