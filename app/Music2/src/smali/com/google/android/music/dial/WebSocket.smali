.class Lcom/google/android/music/dial/WebSocket;
.super Ljava/lang/Object;
.source "WebSocket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/WebSocket$1;,
        Lcom/google/android/music/dial/WebSocket$Listener;
    }
.end annotation


# static fields
.field private static final CRLF_PAIR:[B

.field private static final LOGV:Z

.field public static final SSL_PROTOCOL_VERSIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ZERO_SIZE_BYTE_BUFFER:[Ljava/nio/ByteBuffer;

.field private static sSSLContext:Ljavax/net/ssl/SSLContext;


# instance fields
.field private final mBufferSize:I

.field private final mCharset:Ljava/nio/charset/Charset;

.field private final mCharsetDecoder:Ljava/nio/charset/CharsetDecoder;

.field private mCloseCode:I

.field private mConnectStartTime:J

.field private mConnectTimeout:J

.field private final mContext:Landroid/content/Context;

.field private mDisconnectStartTime:J

.field private final mDisconnectTimeout:J

.field private mDisconnecting:Z

.field private mInitialSSLHandshakeComplete:Z

.field private mIsSecure:Z

.field private mListener:Lcom/google/android/music/dial/WebSocket$Listener;

.field private final mMaxMessageLength:I

.field private final mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

.field private final mOrigin:Ljava/lang/String;

.field private mPingData:[B

.field private mPingReceived:Z

.field private mPongReceived:Z

.field private mPort:I

.field private mPrevDataOpcode:B

.field private mProtocol:Ljava/lang/String;

.field private mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

.field private mRemoteAddress:Ljava/net/InetSocketAddress;

.field private mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

.field private mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

.field private mSSLEngine:Ljavax/net/ssl/SSLEngine;

.field private mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

.field private mSocket:Ljava/nio/channels/SocketChannel;

.field private mState:I

.field private mTornDown:Z

.field private mUri:Landroid/net/Uri;

.field private mWebSocketClosingHandshakeReceived:Z

.field private mWebSocketClosingHandshakeSent:Z

.field private mWebSocketKey:Ljava/lang/String;

.field private mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    .line 101
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/music/dial/WebSocket;->ZERO_SIZE_BYTE_BUFFER:[Ljava/nio/ByteBuffer;

    .line 121
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/music/dial/WebSocket;->CRLF_PAIR:[B

    .line 153
    const-string v0, "TLSv1.2"

    const-string v1, "TLSv1.1"

    const-string v2, "TLS"

    invoke-static {v0, v1, v2}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/dial/WebSocket;->SSL_PROTOCOL_VERSIONS:Ljava/util/List;

    return-void

    .line 121
    nop

    :array_0
    .array-data 1
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "protocol"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 284
    const v0, 0x8000

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/music/dial/WebSocket;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "protocol"    # Ljava/lang/String;
    .param p4, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    if-nez p1, :cond_0

    .line 297
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_0
    const/16 v0, 0x400

    if-ge p4, v0, :cond_1

    .line 300
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize < MIN_BUFFER_SIZE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_1
    iput-object p1, p0, Lcom/google/android/music/dial/WebSocket;->mContext:Landroid/content/Context;

    .line 304
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 305
    iput-object p2, p0, Lcom/google/android/music/dial/WebSocket;->mOrigin:Ljava/lang/String;

    .line 306
    iput-object p3, p0, Lcom/google/android/music/dial/WebSocket;->mProtocol:Ljava/lang/String;

    .line 307
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->getInstance(Landroid/content/Context;)Lcom/google/android/music/dial/WebSocketMultiplexer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    .line 308
    iput p4, p0, Lcom/google/android/music/dial/WebSocket;->mBufferSize:I

    .line 309
    iget v0, p0, Lcom/google/android/music/dial/WebSocket;->mBufferSize:I

    add-int/lit8 v0, v0, -0xe

    iput v0, p0, Lcom/google/android/music/dial/WebSocket;->mMaxMessageLength:I

    .line 310
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->getCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mCharset:Ljava/nio/charset/Charset;

    .line 311
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mCharset:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mCharsetDecoder:Ljava/nio/charset/CharsetDecoder;

    .line 312
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnectTimeout:J

    .line 313
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->resetState()V

    .line 314
    return-void
.end method

.method private declared-synchronized checkConnected()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 1057
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 1058
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not connected; state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1057
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1060
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static createInetAddressObject(Ljava/lang/String;)Ljava/net/InetAddress;
    .locals 12
    .param p0, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 562
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 564
    .local v0, "addr":Ljava/net/InetAddress;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 565
    .local v2, "bout":Ljava/io/ByteArrayOutputStream;
    new-instance v9, Ljava/io/ObjectOutputStream;

    invoke-direct {v9, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 566
    .local v9, "oout":Ljava/io/ObjectOutputStream;
    invoke-virtual {v9, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 567
    invoke-virtual {v9}, Ljava/io/ObjectOutputStream;->close()V

    .line 569
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 571
    .local v3, "data":[B
    array-length v10, v3

    add-int/lit8 v10, v10, 0x2

    new-array v7, v10, [B

    .line 572
    .local v7, "newData":[B
    array-length v10, v3

    add-int/lit8 v4, v10, -0x2

    .line 573
    .local v4, "n":I
    invoke-static {v3, v11, v7, v11, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 576
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "n":I
    .local v5, "n":I
    const/16 v10, 0x74

    aput-byte v10, v7, v4

    .line 578
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "n":I
    .restart local v4    # "n":I
    aput-byte v11, v7, v5

    .line 579
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "n":I
    .restart local v5    # "n":I
    aput-byte v11, v7, v4

    .line 581
    const/16 v10, 0x78

    aput-byte v10, v7, v5

    .line 584
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 585
    .local v1, "bin":Ljava/io/ByteArrayInputStream;
    new-instance v8, Ljava/io/ObjectInputStream;

    invoke-direct {v8, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 587
    .local v8, "oin":Ljava/io/ObjectInputStream;
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/net/Inet4Address;

    .line 588
    .local v6, "newAddr":Ljava/net/Inet4Address;
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->close()V

    .line 589
    return-object v6
.end method

.method private declared-synchronized doTeardown(I)V
    .locals 7
    .param p1, "reason"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1023
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v2, :cond_0

    .line 1024
    const-string v2, "WebSocket"

    const-string v3, "doTeardown with reason=%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    .line 1030
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 1032
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1034
    :goto_0
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    .line 1037
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    .line 1038
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    .line 1039
    iget v2, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    if-eq v2, v1, :cond_2

    iget v2, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1042
    .local v0, "wasConnecting":Z
    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 1043
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnectStartTime:J

    iput-wide v2, p0, Lcom/google/android/music/dial/WebSocket;->mConnectStartTime:J

    .line 1044
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mTornDown:Z

    .line 1046
    if-eqz v0, :cond_4

    .line 1047
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    invoke-interface {v1, p1}, Lcom/google/android/music/dial/WebSocket$Listener;->onConnectionFailed(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1051
    :goto_1
    monitor-exit p0

    return-void

    .line 1049
    :cond_4
    :try_start_3
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    iget v2, p0, Lcom/google/android/music/dial/WebSocket;->mCloseCode:I

    invoke-interface {v1, p1, v2}, Lcom/google/android/music/dial/WebSocket$Listener;->onDisconnected(II)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1023
    .end local v0    # "wasConnecting":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1033
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method static generateAcceptValue(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 7
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "charset"    # Ljava/nio/charset/Charset;

    .prologue
    .line 1068
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1069
    .local v3, "value":Ljava/lang/String;
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 1070
    .local v2, "md":Ljava/security/MessageDigest;
    invoke-virtual {v3, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 1071
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 1072
    .local v0, "data":[B
    const/4 v4, 0x0

    array-length v5, v0

    const/4 v6, 0x2

    invoke-static {v0, v4, v5, v6}, Landroid/util/Base64;->encodeToString([BIII)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1076
    .end local v0    # "data":[B
    .end local v2    # "md":Ljava/security/MessageDigest;
    .end local v3    # "value":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 1073
    :catch_0
    move-exception v1

    .line 1075
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v4, "WebSocket"

    const-string v5, "unexpected Base64 encoding error"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1076
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getBasicOps()I
    .locals 2

    .prologue
    .line 1153
    const/4 v0, 0x0

    .line 1155
    .local v0, "ops":I
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    if-eqz v1, :cond_1

    .line 1156
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->getSSLInterestSet()I

    move-result v1

    or-int/2addr v0, v1

    .line 1168
    :cond_0
    :goto_0
    return v0

    .line 1159
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v1}, Lcom/google/android/music/dial/CircularByteBuffer;->isFull()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1160
    or-int/lit8 v0, v0, 0x1

    .line 1163
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v1}, Lcom/google/android/music/dial/CircularByteBuffer;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mPingReceived:Z

    if-eqz v1, :cond_0

    .line 1164
    :cond_3
    or-int/lit8 v0, v0, 0x4

    goto :goto_0
.end method

.method private static getDialTrustManager(Landroid/content/Context;)Lcom/google/android/music/dial/DialTrustManager;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1643
    :try_start_0
    const-string v14, "X.509"

    invoke-static {v14}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v3

    .line 1646
    .local v3, "certFactory":Ljava/security/cert/CertificateFactory;
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v12

    .line 1647
    .local v12, "keyStoreType":Ljava/lang/String;
    invoke-static {v12}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v11

    .line 1648
    .local v11, "keyStore":Ljava/security/KeyStore;
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v11, v14, v15}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 1650
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 1651
    .local v1, "assetMgr":Landroid/content/res/AssetManager;
    const-string v14, "dial/certs"

    invoke-virtual {v1, v14}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1652
    .local v7, "certFiles":[Ljava/lang/String;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v13, v0

    .local v13, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v13, :cond_2

    aget-object v5, v0, v10

    .line 1653
    .local v5, "certFileName":Ljava/lang/String;
    const-string v14, "README"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 1654
    new-instance v4, Ljava/io/File;

    const-string v14, "dial/certs"

    invoke-direct {v4, v14, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    .local v4, "certFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 1656
    .local v6, "certFilePath":Ljava/lang/String;
    invoke-static {v3, v1, v6}, Lcom/google/android/music/dial/WebSocket;->loadCertificate(Ljava/security/cert/CertificateFactory;Landroid/content/res/AssetManager;Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v2

    check-cast v2, Ljava/security/cert/X509Certificate;

    .line 1658
    .local v2, "cert":Ljava/security/cert/X509Certificate;
    if-eqz v2, :cond_1

    .line 1659
    invoke-virtual {v11, v6, v2}, Ljava/security/KeyStore;->setCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V

    .line 1660
    const-string v14, "WebSocket"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Loaded CA cert file "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1652
    .end local v2    # "cert":Ljava/security/cert/X509Certificate;
    .end local v4    # "certFile":Ljava/io/File;
    .end local v6    # "certFilePath":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1662
    .restart local v2    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v4    # "certFile":Ljava/io/File;
    .restart local v6    # "certFilePath":Ljava/lang/String;
    :cond_1
    const-string v14, "WebSocket"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error loading CA cert file "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 1670
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "assetMgr":Landroid/content/res/AssetManager;
    .end local v2    # "cert":Ljava/security/cert/X509Certificate;
    .end local v3    # "certFactory":Ljava/security/cert/CertificateFactory;
    .end local v4    # "certFile":Ljava/io/File;
    .end local v5    # "certFileName":Ljava/lang/String;
    .end local v6    # "certFilePath":Ljava/lang/String;
    .end local v7    # "certFiles":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v11    # "keyStore":Ljava/security/KeyStore;
    .end local v12    # "keyStoreType":Ljava/lang/String;
    .end local v13    # "len$":I
    :catch_0
    move-exception v9

    .line 1671
    .local v9, "e":Ljava/io/IOException;
    const-string v14, "WebSocket"

    const-string v15, "Error creating DialTrustManager"

    invoke-static {v14, v15, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1679
    .end local v9    # "e":Ljava/io/IOException;
    :goto_2
    const/4 v8, 0x0

    :goto_3
    return-object v8

    .line 1667
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "assetMgr":Landroid/content/res/AssetManager;
    .restart local v3    # "certFactory":Ljava/security/cert/CertificateFactory;
    .restart local v7    # "certFiles":[Ljava/lang/String;
    .restart local v10    # "i$":I
    .restart local v11    # "keyStore":Ljava/security/KeyStore;
    .restart local v12    # "keyStoreType":Ljava/lang/String;
    .restart local v13    # "len$":I
    :cond_2
    :try_start_1
    new-instance v8, Lcom/google/android/music/dial/DialTrustManager;

    invoke-direct {v8}, Lcom/google/android/music/dial/DialTrustManager;-><init>()V

    .line 1668
    .local v8, "dialTrustManager":Lcom/google/android/music/dial/DialTrustManager;
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/security/KeyStore;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    invoke-virtual {v8, v14}, Lcom/google/android/music/dial/DialTrustManager;->init([Ljava/security/KeyStore;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_3

    .line 1672
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "assetMgr":Landroid/content/res/AssetManager;
    .end local v3    # "certFactory":Ljava/security/cert/CertificateFactory;
    .end local v7    # "certFiles":[Ljava/lang/String;
    .end local v8    # "dialTrustManager":Lcom/google/android/music/dial/DialTrustManager;
    .end local v10    # "i$":I
    .end local v11    # "keyStore":Ljava/security/KeyStore;
    .end local v12    # "keyStoreType":Ljava/lang/String;
    .end local v13    # "len$":I
    :catch_1
    move-exception v9

    .line 1673
    .local v9, "e":Ljava/security/cert/CertificateException;
    const-string v14, "WebSocket"

    const-string v15, "Error creating DialTrustManager"

    invoke-static {v14, v15, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1674
    .end local v9    # "e":Ljava/security/cert/CertificateException;
    :catch_2
    move-exception v9

    .line 1675
    .local v9, "e":Ljava/security/KeyStoreException;
    const-string v14, "WebSocket"

    const-string v15, "Error creating DialTrustManager"

    invoke-static {v14, v15, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1676
    .end local v9    # "e":Ljava/security/KeyStoreException;
    :catch_3
    move-exception v9

    .line 1677
    .local v9, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v14, "WebSocket"

    const-string v15, "Error creating DialTrustManager"

    invoke-static {v14, v15, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method private static declared-synchronized getSSLContext(Landroid/content/Context;)Ljavax/net/ssl/SSLContext;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyManagementException;,
            Lcom/google/android/music/dial/UnsupportedSslProtocolsException;
        }
    .end annotation

    .prologue
    .line 1596
    const-class v6, Lcom/google/android/music/dial/WebSocket;

    monitor-enter v6

    :try_start_0
    sget-object v5, Lcom/google/android/music/dial/WebSocket;->sSSLContext:Ljavax/net/ssl/SSLContext;

    if-eqz v5, :cond_0

    .line 1597
    sget-object v5, Lcom/google/android/music/dial/WebSocket;->sSSLContext:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1631
    :goto_0
    monitor-exit v6

    return-object v5

    .line 1602
    :cond_0
    :try_start_1
    sget-object v5, Lcom/google/android/music/dial/WebSocket;->SSL_PROTOCOL_VERSIONS:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1604
    .local v4, "version":Ljava/lang/String;
    :try_start_2
    invoke-static {v4}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v5

    sput-object v5, Lcom/google/android/music/dial/WebSocket;->sSSLContext:Ljavax/net/ssl/SSLContext;

    .line 1605
    sget-boolean v5, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v5, :cond_2

    .line 1606
    const-string v5, "WebSocket"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "got SSLContext instance for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1615
    .end local v4    # "version":Ljava/lang/String;
    :cond_2
    :try_start_3
    sget-object v5, Lcom/google/android/music/dial/WebSocket;->sSSLContext:Ljavax/net/ssl/SSLContext;

    if-nez v5, :cond_3

    .line 1616
    const-string v5, "WebSocket"

    const-string v7, "Error creating SSLContext.  No supported protocol versions."

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1617
    new-instance v5, Lcom/google/android/music/dial/UnsupportedSslProtocolsException;

    const-string v7, "Protocol versions not supported"

    invoke-direct {v5, v7}, Lcom/google/android/music/dial/UnsupportedSslProtocolsException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1596
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    .line 1609
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "version":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1610
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_4
    sget-boolean v5, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v5, :cond_1

    .line 1611
    const-string v5, "WebSocket"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SSL protocol version not supported.  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1619
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .end local v4    # "version":Ljava/lang/String;
    :cond_3
    invoke-static {p0}, Lcom/google/android/music/dial/WebSocket;->getDialTrustManager(Landroid/content/Context;)Lcom/google/android/music/dial/DialTrustManager;

    move-result-object v0

    .line 1620
    .local v0, "customTrustManager":Lcom/google/android/music/dial/DialTrustManager;
    const/4 v3, 0x0

    .line 1621
    .local v3, "trustManagers":[Ljavax/net/ssl/TrustManager;
    if-eqz v0, :cond_4

    .line 1622
    const/4 v5, 0x1

    new-array v3, v5, [Ljavax/net/ssl/TrustManager;

    .end local v3    # "trustManagers":[Ljavax/net/ssl/TrustManager;
    const/4 v5, 0x0

    aput-object v0, v3, v5

    .line 1630
    .restart local v3    # "trustManagers":[Ljavax/net/ssl/TrustManager;
    :goto_2
    sget-object v5, Lcom/google/android/music/dial/WebSocket;->sSSLContext:Ljavax/net/ssl/SSLContext;

    const/4 v7, 0x0

    new-instance v8, Ljava/security/SecureRandom;

    invoke-direct {v8}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v5, v7, v3, v8}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 1631
    sget-object v5, Lcom/google/android/music/dial/WebSocket;->sSSLContext:Ljavax/net/ssl/SSLContext;

    goto/16 :goto_0

    .line 1626
    :cond_4
    const-string v5, "WebSocket"

    const-string v7, "No TrustManager while initializing SSLContext"

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method private declared-synchronized getSSLInterestSet()I
    .locals 3

    .prologue
    .line 1177
    monitor-enter p0

    const/4 v0, 0x0

    .line 1180
    .local v0, "ops":I
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->sslEngineCanRead()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v1}, Lcom/google/android/music/dial/CircularByteBuffer;->isFull()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1181
    or-int/lit8 v0, v0, 0x1

    .line 1183
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->sslEngineCanWrite()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnecting:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mPingReceived:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mInitialSSLHandshakeComplete:Z

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v1}, Lcom/google/android/music/dial/CircularByteBuffer;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_3

    .line 1189
    :cond_2
    or-int/lit8 v0, v0, 0x4

    .line 1192
    :cond_3
    monitor-exit p0

    return v0

    .line 1177
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized handleSSLRead()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1394
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->sslEngineCanRead()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1395
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1394
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1397
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->readFromChannel()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 1438
    :goto_0
    monitor-exit p0

    return-void

    .line 1400
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1402
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-lez v2, :cond_4

    .line 1403
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v2}, Lcom/google/android/music/dial/CircularByteBuffer;->getWritableRegions()[Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1404
    .local v1, "targets":[Ljava/nio/ByteBuffer;
    if-nez v1, :cond_2

    .line 1407
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Trying to read into a full buffer."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1409
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3, v1}, Ljavax/net/ssl/SSLEngine;->unwrap(Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v0

    .line 1410
    .local v0, "result":Ljavax/net/ssl/SSLEngineResult;
    sget-boolean v2, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v2, :cond_3

    .line 1411
    const-string v2, "WebSocket"

    const-string v3, "<<<<<< handleRead called SSLEngine.unwrap: status=%s handshakeStatus=%s bytesConsumed=%d bytesProduced=%d"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1417
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/WebSocket;->processHandshakeStatus(Ljavax/net/ssl/SSLEngineResult;)V

    .line 1418
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/dial/CircularByteBuffer;->updateWritePos(I)V

    .line 1420
    sget-object v2, Lcom/google/android/music/dial/WebSocket$1;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 1422
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 1426
    :pswitch_1
    new-instance v2, Ljava/io/IOException;

    const-string v3, "unexpected buffer overflow condition."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1429
    :pswitch_2
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2

    .line 1437
    .end local v0    # "result":Ljavax/net/ssl/SSLEngineResult;
    .end local v1    # "targets":[Ljava/nio/ByteBuffer;
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1420
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized handleSSLWrite()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1453
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->sslEngineCanWrite()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1454
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1453
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1456
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    if-lez v2, :cond_1

    .line 1457
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->writeToChannel()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1508
    :goto_0
    monitor-exit p0

    return-void

    .line 1461
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v2}, Lcom/google/android/music/dial/CircularByteBuffer;->getReadableRegions()[Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1462
    .local v1, "sources":[Ljava/nio/ByteBuffer;
    if-nez v1, :cond_4

    .line 1464
    sget-boolean v2, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v2, :cond_2

    .line 1465
    const-string v2, "WebSocket"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "There is nothing to write. disconnecting? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnecting:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1467
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnecting:Z

    if-eqz v2, :cond_3

    .line 1468
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLEngine;->closeOutbound()V

    goto :goto_0

    .line 1472
    :cond_3
    sget-object v1, Lcom/google/android/music/dial/WebSocket;->ZERO_SIZE_BYTE_BUFFER:[Ljava/nio/ByteBuffer;

    .line 1474
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1, v3}, Ljavax/net/ssl/SSLEngine;->wrap([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v0

    .line 1475
    .local v0, "result":Ljavax/net/ssl/SSLEngineResult;
    sget-boolean v2, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v2, :cond_5

    .line 1476
    const-string v2, "WebSocket"

    const-string v3, ">>>>>> handleWrite called SSLEngine.wrap: status=%s handshakeStatus=%s bytesConsumed=%d bytesProduced=%d"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1482
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/music/dial/WebSocket;->processHandshakeStatus(Ljavax/net/ssl/SSLEngineResult;)V

    .line 1483
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/dial/CircularByteBuffer;->updateReadPos(I)V

    .line 1485
    sget-object v2, Lcom/google/android/music/dial/WebSocket$1;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1503
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    if-lez v2, :cond_6

    .line 1504
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->writeToChannel()V

    .line 1507
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v2}, Lcom/google/android/music/dial/CircularByteBuffer;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v2, v3, :cond_1

    goto/16 :goto_0

    .line 1488
    :pswitch_0
    new-instance v2, Ljava/io/IOException;

    const-string v3, "unexpected buffer underflow condition."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1493
    :pswitch_1
    new-instance v2, Ljava/io/IOException;

    const-string v3, "unexpected buffer overflow condition."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1496
    :pswitch_2
    new-instance v2, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v2}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1485
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static loadCertificate(Ljava/security/cert/CertificateFactory;Landroid/content/res/AssetManager;Ljava/lang/String;)Ljava/security/cert/Certificate;
    .locals 5
    .param p0, "certificateFactory"    # Ljava/security/cert/CertificateFactory;
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1693
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1695
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1697
    :try_start_2
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 1704
    .end local v1    # "inputStream":Ljava/io/InputStream;
    :goto_0
    return-object v2

    .line 1697
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1699
    .end local v1    # "inputStream":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 1700
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "WebSocket"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error loading certificate "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1704
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1701
    :catch_1
    move-exception v0

    .line 1702
    .local v0, "e":Ljava/security/cert/CertificateException;
    const-string v2, "WebSocket"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error loading certificate "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private declared-synchronized processHandshakeStatus(Ljavax/net/ssl/SSLEngineResult;)V
    .locals 4
    .param p1, "result"    # Ljavax/net/ssl/SSLEngineResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1352
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    .line 1353
    sget-object v1, Lcom/google/android/music/dial/WebSocket$1;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 1380
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1356
    :pswitch_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mInitialSSLHandshakeComplete:Z

    .line 1357
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_1

    .line 1358
    const-string v1, "WebSocket"

    const-string v2, "Initial SSL Handshake complete"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1360
    :cond_1
    iget v1, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    if-ne v1, v3, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mInitialSSLHandshakeComplete:Z

    if-eqz v1, :cond_0

    .line 1361
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->writeWebSocketOpeningHandshake()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1352
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1370
    :goto_1
    :pswitch_1
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->getDelegatedTask()Ljava/lang/Runnable;

    move-result-object v0

    .local v0, "task":Ljava/lang/Runnable;
    if-eqz v0, :cond_2

    .line 1371
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 1373
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1353
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized readFromChannel()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1520
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    iget-object v4, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 1521
    .local v0, "count":I
    if-gez v0, :cond_1

    .line 1522
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_0

    .line 1523
    const-string v1, "WebSocket"

    const-string v2, "readFromChannel: count %d; throwing ClosedChannelException"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1527
    :cond_0
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1520
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1529
    .restart local v0    # "count":I
    :cond_1
    :try_start_1
    sget-boolean v3, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v3, :cond_2

    .line 1530
    const-string v3, "WebSocket"

    const-string v4, "readFromChannel: count %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1532
    :cond_2
    if-lez v0, :cond_3

    :goto_0
    monitor-exit p0

    return v1

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private declared-synchronized readMessages()Z
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 768
    monitor-enter p0

    :try_start_0
    sget-boolean v14, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v14, :cond_0

    .line 769
    const-string v14, "WebSocket"

    const-string v15, "readMessages when state is %d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/dial/WebSocket;->mState:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    :cond_0
    const/4 v11, 0x0

    .line 772
    .local v11, "partialMessageRead":Z
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_2

    .line 773
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->markReadPosition()V

    .line 776
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->readByte()Ljava/lang/Byte;

    move-result-object v2

    .line 777
    .local v2, "b":Ljava/lang/Byte;
    if-nez v2, :cond_3

    .line 778
    const/4 v11, 0x1

    .line 958
    .end local v2    # "b":Ljava/lang/Byte;
    :cond_2
    :goto_1
    if-eqz v11, :cond_1f

    .line 961
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->rewindReadPosition()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 966
    :goto_2
    const/4 v14, 0x1

    :goto_3
    monitor-exit p0

    return v14

    .line 781
    .restart local v2    # "b":Ljava/lang/Byte;
    :cond_3
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v14

    and-int/lit8 v14, v14, 0xf

    int-to-byte v10, v14

    .line 782
    .local v10, "opcode":B
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v14

    and-int/lit8 v14, v14, -0x80

    if-eqz v14, :cond_4

    const/4 v3, 0x1

    .line 785
    .local v3, "isFinal":Z
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->readByte()Ljava/lang/Byte;

    move-result-object v2

    .line 786
    if-nez v2, :cond_5

    .line 787
    const/4 v11, 0x1

    .line 788
    goto :goto_1

    .line 782
    .end local v3    # "isFinal":Z
    :cond_4
    const/4 v3, 0x0

    goto :goto_4

    .line 790
    .restart local v3    # "isFinal":Z
    :cond_5
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v14

    and-int/lit8 v14, v14, -0x80

    if-eqz v14, :cond_6

    const/4 v4, 0x1

    .line 791
    .local v4, "isMasked":Z
    :goto_5
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v14

    and-int/lit8 v14, v14, 0x7f

    int-to-long v8, v14

    .line 793
    .local v8, "messageLength":J
    const-wide/16 v14, 0x7e

    cmp-long v14, v8, v14

    if-nez v14, :cond_9

    .line 794
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->readShort()Ljava/lang/Integer;

    move-result-object v13

    .line 795
    .local v13, "val":Ljava/lang/Integer;
    if-nez v13, :cond_7

    .line 796
    const/4 v11, 0x1

    .line 797
    goto :goto_1

    .line 790
    .end local v4    # "isMasked":Z
    .end local v8    # "messageLength":J
    .end local v13    # "val":Ljava/lang/Integer;
    :cond_6
    const/4 v4, 0x0

    goto :goto_5

    .line 799
    .restart local v4    # "isMasked":Z
    .restart local v8    # "messageLength":J
    .restart local v13    # "val":Ljava/lang/Integer;
    :cond_7
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v14

    int-to-long v8, v14

    .line 809
    .end local v13    # "val":Ljava/lang/Integer;
    :cond_8
    :goto_6
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/music/dial/WebSocket;->mMaxMessageLength:I

    int-to-long v14, v14

    cmp-long v14, v8, v14

    if-lez v14, :cond_b

    .line 810
    new-instance v14, Ljava/io/IOException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "message too large: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " bytes"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 768
    .end local v2    # "b":Ljava/lang/Byte;
    .end local v3    # "isFinal":Z
    .end local v4    # "isMasked":Z
    .end local v8    # "messageLength":J
    .end local v10    # "opcode":B
    .end local v11    # "partialMessageRead":Z
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 800
    .restart local v2    # "b":Ljava/lang/Byte;
    .restart local v3    # "isFinal":Z
    .restart local v4    # "isMasked":Z
    .restart local v8    # "messageLength":J
    .restart local v10    # "opcode":B
    .restart local v11    # "partialMessageRead":Z
    :cond_9
    const-wide/16 v14, 0x7f

    cmp-long v14, v8, v14

    if-nez v14, :cond_8

    .line 801
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->readLong()Ljava/lang/Long;

    move-result-object v13

    .line 802
    .local v13, "val":Ljava/lang/Long;
    if-nez v13, :cond_a

    .line 803
    const/4 v11, 0x1

    .line 804
    goto/16 :goto_1

    .line 806
    :cond_a
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    goto :goto_6

    .line 812
    .end local v13    # "val":Ljava/lang/Long;
    :cond_b
    long-to-int v5, v8

    .line 815
    .local v5, "len":I
    const/4 v6, 0x0

    .line 816
    .local v6, "mask":[B
    if-eqz v4, :cond_c

    .line 817
    const/4 v14, 0x4

    new-array v6, v14, [B

    .line 818
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    const/4 v15, 0x0

    invoke-virtual {v14, v6, v15}, Lcom/google/android/music/dial/CircularByteBuffer;->readBytes([B[B)Z

    move-result v14

    if-nez v14, :cond_c

    .line 819
    const/4 v11, 0x1

    .line 820
    goto/16 :goto_1

    .line 825
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToRead()I

    move-result v14

    if-ge v14, v5, :cond_d

    .line 826
    const/4 v11, 0x1

    .line 827
    goto/16 :goto_1

    .line 830
    :cond_d
    packed-switch v10, :pswitch_data_0

    .line 948
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14, v5}, Lcom/google/android/music/dial/CircularByteBuffer;->skipBytes(I)Z

    move-result v14

    if-nez v14, :cond_1e

    .line 949
    const/4 v11, 0x1

    .line 950
    goto/16 :goto_1

    .line 832
    :pswitch_1
    move-object/from16 v0, p0

    iget-byte v14, v0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    const/4 v15, 0x1

    if-ne v14, v15, :cond_f

    .line 833
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/dial/WebSocket;->mCharsetDecoder:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v14, v5, v6, v15}, Lcom/google/android/music/dial/CircularByteBuffer;->readString(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v7

    .line 834
    .local v7, "message":Ljava/lang/String;
    if-nez v7, :cond_e

    .line 835
    const/4 v11, 0x1

    .line 836
    goto/16 :goto_1

    .line 838
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    if-eqz v14, :cond_1

    .line 839
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    invoke-interface {v14, v7, v3}, Lcom/google/android/music/dial/WebSocket$Listener;->onContinuationMessageReceived(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 841
    .end local v7    # "message":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-byte v14, v0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    const/4 v15, 0x2

    if-ne v14, v15, :cond_11

    .line 842
    new-array v7, v5, [B

    .line 843
    .local v7, "message":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14, v7, v6}, Lcom/google/android/music/dial/CircularByteBuffer;->readBytes([B[B)Z

    move-result v14

    if-nez v14, :cond_10

    .line 844
    const/4 v11, 0x1

    .line 845
    goto/16 :goto_1

    .line 847
    :cond_10
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    if-eqz v14, :cond_1

    .line 848
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    invoke-interface {v14, v7, v3}, Lcom/google/android/music/dial/WebSocket$Listener;->onContinuationMessageReceived([BZ)V

    goto/16 :goto_0

    .line 851
    .end local v7    # "message":[B
    :cond_11
    new-instance v14, Ljava/io/IOException;

    const-string v15, "Unexpected continuation frame received"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 857
    :pswitch_2
    sget-boolean v14, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v14, :cond_12

    .line 858
    const-string v14, "WebSocket"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "reading a text message of length "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", mask "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    :cond_12
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/dial/WebSocket;->mCharsetDecoder:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v14, v5, v6, v15}, Lcom/google/android/music/dial/CircularByteBuffer;->readString(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v7

    .line 862
    .local v7, "message":Ljava/lang/String;
    if-nez v7, :cond_13

    .line 863
    const/4 v11, 0x1

    .line 864
    goto/16 :goto_1

    .line 867
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    if-eqz v14, :cond_14

    .line 868
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    invoke-interface {v14, v7, v3}, Lcom/google/android/music/dial/WebSocket$Listener;->onMessageReceived(Ljava/lang/String;Z)V

    .line 870
    :cond_14
    move-object/from16 v0, p0

    iput-byte v10, v0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    goto/16 :goto_0

    .line 875
    .end local v7    # "message":Ljava/lang/String;
    :pswitch_3
    new-array v7, v5, [B

    .line 876
    .local v7, "message":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14, v7, v6}, Lcom/google/android/music/dial/CircularByteBuffer;->readBytes([B[B)Z

    move-result v14

    if-nez v14, :cond_15

    .line 877
    const/4 v11, 0x1

    .line 878
    goto/16 :goto_1

    .line 881
    :cond_15
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    if-eqz v14, :cond_16

    .line 882
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    invoke-interface {v14, v7, v3}, Lcom/google/android/music/dial/WebSocket$Listener;->onMessageReceived([BZ)V

    .line 884
    :cond_16
    move-object/from16 v0, p0

    iput-byte v10, v0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    goto/16 :goto_0

    .line 889
    .end local v7    # "message":[B
    :pswitch_4
    const/4 v14, 0x4

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 890
    const/4 v14, 0x2

    if-lt v5, v14, :cond_18

    .line 891
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->readShort()Ljava/lang/Integer;

    move-result-object v13

    .line 892
    .local v13, "val":Ljava/lang/Integer;
    if-nez v13, :cond_17

    .line 893
    const/4 v11, 0x1

    .line 894
    goto/16 :goto_1

    .line 896
    :cond_17
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/music/dial/WebSocket;->mCloseCode:I

    .line 897
    add-int/lit8 v5, v5, -0x2

    .line 899
    .end local v13    # "val":Ljava/lang/Integer;
    :cond_18
    if-lez v5, :cond_19

    .line 900
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14, v5}, Lcom/google/android/music/dial/CircularByteBuffer;->skipBytes(I)Z

    move-result v14

    if-nez v14, :cond_19

    .line 901
    const/4 v11, 0x1

    .line 902
    goto/16 :goto_1

    .line 905
    :cond_19
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeReceived:Z

    .line 906
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeSent:Z

    if-eqz v14, :cond_1

    .line 907
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 915
    :pswitch_5
    new-array v14, v5, [B

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mPingData:[B

    .line 916
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/dial/WebSocket;->mPingData:[B

    invoke-virtual {v14, v15, v6}, Lcom/google/android/music/dial/CircularByteBuffer;->readBytes([B[B)Z

    move-result v14

    if-nez v14, :cond_1a

    .line 917
    const/4 v11, 0x1

    .line 918
    goto/16 :goto_1

    .line 920
    :cond_1a
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/music/dial/WebSocket;->mPingReceived:Z

    .line 921
    sget-boolean v14, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v14, :cond_1b

    .line 922
    const-string v14, "WebSocket"

    const-string v15, "Got Ping with %d bytes data"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    :cond_1b
    const/4 v14, -0x1

    move-object/from16 v0, p0

    iput-byte v14, v0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    goto/16 :goto_0

    .line 931
    :pswitch_6
    if-lez v5, :cond_1c

    .line 932
    new-array v12, v5, [B

    .line 933
    .local v12, "pongData":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14, v12, v6}, Lcom/google/android/music/dial/CircularByteBuffer;->readBytes([B[B)Z

    move-result v14

    if-nez v14, :cond_1c

    .line 934
    const/4 v11, 0x1

    .line 935
    goto/16 :goto_1

    .line 938
    .end local v12    # "pongData":[B
    :cond_1c
    sget-boolean v14, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v14, :cond_1d

    .line 939
    const-string v14, "WebSocket"

    const-string v15, "Got Pong with %d bytes data"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    :cond_1d
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/music/dial/WebSocket;->mPongReceived:Z

    .line 942
    const/4 v14, -0x1

    move-object/from16 v0, p0

    iput-byte v14, v0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    goto/16 :goto_0

    .line 952
    :cond_1e
    const/4 v14, -0x1

    move-object/from16 v0, p0

    iput-byte v14, v0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    goto/16 :goto_0

    .line 963
    .end local v2    # "b":Ljava/lang/Byte;
    .end local v3    # "isFinal":Z
    .end local v4    # "isMasked":Z
    .end local v5    # "len":I
    .end local v6    # "mask":[B
    .end local v8    # "messageLength":J
    .end local v10    # "opcode":B
    :cond_1f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v14}, Lcom/google/android/music/dial/CircularByteBuffer;->clearReadPosition()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 830
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private declared-synchronized readWebSocketOpeningHandshakeReply()Z
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 641
    monitor-enter p0

    :try_start_0
    iget-object v10, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    sget-object v11, Lcom/google/android/music/dial/WebSocket;->CRLF_PAIR:[B

    invoke-virtual {v10, v11}, Lcom/google/android/music/dial/CircularByteBuffer;->peekBytes([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 642
    .local v6, "len":I
    if-gez v6, :cond_1

    .line 705
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 647
    :cond_1
    const/4 v7, 0x0

    .line 650
    .local v7, "reply":Ljava/lang/String;
    :try_start_1
    iget-object v10, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/music/dial/WebSocket;->mCharsetDecoder:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v10, v6, v11, v12}, Lcom/google/android/music/dial/CircularByteBuffer;->readString(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v7

    .line 651
    if-nez v7, :cond_2

    .line 652
    new-instance v9, Ljava/io/IOException;

    const-string v10, "Invalid opening handshake reply"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 654
    :catch_0
    move-exception v1

    .line 655
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_2
    new-instance v9, Lcom/google/android/music/dial/DataFormatException;

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/google/android/music/dial/DataFormatException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 641
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v6    # "len":I
    .end local v7    # "reply":Ljava/lang/String;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 658
    .restart local v6    # "len":I
    .restart local v7    # "reply":Ljava/lang/String;
    :cond_2
    :try_start_3
    sget-boolean v10, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v10, :cond_3

    .line 659
    const-string v10, "WebSocket"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "WebSocket handshake reply:\n "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    :cond_3
    const-string v10, "\r\n"

    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 664
    .local v4, "headers":[Ljava/lang/String;
    array-length v10, v4

    if-nez v10, :cond_4

    .line 665
    new-instance v9, Ljava/io/IOException;

    const-string v10, "WebSocket Invalid opening handshake reply"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 668
    :cond_4
    const/4 v10, 0x0

    aget-object v10, v4, v10

    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    const-string v11, "^HTTP\\/[0-9\\.]+\\s+101\\s+.*$"

    invoke-virtual {v10, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 669
    const-string v10, "WebSocket"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "WebSocket handshake unexpected HTTP status: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v4, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v9

    .line 670
    goto :goto_0

    .line 673
    :cond_5
    const/4 v0, 0x0

    .line 674
    .local v0, "accepted":Z
    const/4 v5, 0x1

    .local v5, "i":I
    :goto_1
    array-length v9, v4

    if-ge v5, v9, :cond_9

    .line 675
    aget-object v9, v4, v5

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 676
    .local v3, "header":Ljava/lang/String;
    const-string v9, "sec-websocket-accept: "

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 677
    aget-object v9, v4, v5

    const-string v10, "sec-websocket-accept: "

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 679
    .local v8, "value":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketKey:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/dial/WebSocket;->mCharset:Ljava/nio/charset/Charset;

    invoke-static {v9, v10}, Lcom/google/android/music/dial/WebSocket;->generateAcceptValue(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v2

    .line 680
    .local v2, "expectedValue":Ljava/lang/String;
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 681
    const/4 v0, 0x1

    .line 674
    .end local v2    # "expectedValue":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_6
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 683
    .restart local v2    # "expectedValue":Ljava/lang/String;
    .restart local v8    # "value":Ljava/lang/String;
    :cond_7
    const-string v9, "WebSocket"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Expected accept value "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " but got "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 686
    .end local v2    # "expectedValue":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_8
    const-string v9, "sec-websocket-protocol: "

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 687
    aget-object v9, v4, v5

    const-string v10, "sec-websocket-protocol: "

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/music/dial/WebSocket;->mProtocol:Ljava/lang/String;

    goto :goto_2

    .line 691
    .end local v3    # "header":Ljava/lang/String;
    :cond_9
    if-eqz v0, :cond_b

    .line 692
    const/4 v9, 0x3

    iput v9, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 693
    sget-boolean v9, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v9, :cond_a

    .line 694
    const-string v9, "WebSocket"

    const-string v10, "WebSocket connected"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    :cond_a
    iget-object v9, p0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    if-eqz v9, :cond_b

    .line 698
    iget-object v9, p0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;

    invoke-interface {v9}, Lcom/google/android/music/dial/WebSocket$Listener;->onConnected()V

    .line 702
    :cond_b
    sget-boolean v9, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v9, :cond_0

    .line 703
    const-string v9, "WebSocket"

    const-string v10, "WebSocket accepted %b."

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0
.end method

.method private resetState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 317
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v0}, Lcom/google/android/music/dial/CircularByteBuffer;->clear()V

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v0}, Lcom/google/android/music/dial/CircularByteBuffer;->clear()V

    .line 323
    :cond_1
    iput v1, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 324
    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mInitialSSLHandshakeComplete:Z

    .line 325
    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnecting:Z

    .line 326
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    .line 327
    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mTornDown:Z

    .line 328
    return-void
.end method

.method private declared-synchronized sslEngineCanRead()Z
    .locals 1

    .prologue
    .line 1385
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isOutboundDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized sslEngineCanWrite()Z
    .locals 1

    .prologue
    .line 1443
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isOutboundDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized writeMessage(B[BZ)V
    .locals 6
    .param p1, "opcode"    # B
    .param p2, "data"    # [B
    .param p3, "isFinal"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 981
    monitor-enter p0

    if-nez p2, :cond_0

    const/4 v1, 0x0

    .line 982
    .local v1, "dataLength":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v3}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToWrite()I

    move-result v3

    add-int/lit8 v4, v1, 0xe

    if-ge v3, v4, :cond_1

    .line 983
    new-instance v3, Ljava/io/IOException;

    const-string v4, "no room in buffer"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 981
    .end local v1    # "dataLength":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_0
    :try_start_1
    array-length v1, p2

    goto :goto_0

    .line 987
    .restart local v1    # "dataLength":I
    :cond_1
    move v0, p1

    .line 988
    .local v0, "b":B
    if-eqz p3, :cond_2

    .line 989
    or-int/lit8 v3, v0, -0x80

    int-to-byte v0, v3

    .line 991
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v3, v0}, Lcom/google/android/music/dial/CircularByteBuffer;->writeByte(B)Z

    .line 994
    if-nez p2, :cond_4

    .line 995
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    const/16 v4, -0x80

    invoke-virtual {v3, v4}, Lcom/google/android/music/dial/CircularByteBuffer;->writeByte(B)Z

    .line 1007
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v3}, Lcom/google/android/music/dial/WebSocketMultiplexer;->generateMask()[B

    move-result-object v2

    .line 1008
    .local v2, "mask":[B
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/android/music/dial/CircularByteBuffer;->writeBytes([B[B)Z

    .line 1011
    if-eqz p2, :cond_3

    .line 1012
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v3, p2, v2}, Lcom/google/android/music/dial/CircularByteBuffer;->writeBytes([B[B)Z

    .line 1014
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v3}, Lcom/google/android/music/dial/WebSocketMultiplexer;->wakeup()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1015
    monitor-exit p0

    return-void

    .line 996
    .end local v2    # "mask":[B
    :cond_4
    :try_start_2
    array-length v3, p2

    const/16 v4, 0x7e

    if-ge v3, v4, :cond_5

    .line 997
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    array-length v4, p2

    or-int/lit8 v4, v4, -0x80

    int-to-byte v4, v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/dial/CircularByteBuffer;->writeByte(B)Z

    goto :goto_1

    .line 998
    :cond_5
    array-length v3, p2

    const/high16 v4, 0x10000

    if-ge v3, v4, :cond_6

    .line 999
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    const/4 v4, -0x2

    invoke-virtual {v3, v4}, Lcom/google/android/music/dial/CircularByteBuffer;->writeByte(B)Z

    .line 1000
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    array-length v4, p2

    invoke-virtual {v3, v4}, Lcom/google/android/music/dial/CircularByteBuffer;->writeShort(I)Z

    goto :goto_1

    .line 1002
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/google/android/music/dial/CircularByteBuffer;->writeByte(B)Z

    .line 1003
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    array-length v4, p2

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/dial/CircularByteBuffer;->writeLong(J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized writeMessages()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1286
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v0, :cond_0

    .line 1287
    const-string v0, "WebSocket"

    const-string v1, "writeMessages when state is %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1289
    :cond_0
    iget v0, p0, Lcom/google/android/music/dial/WebSocket;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 1305
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 1291
    :pswitch_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeSent:Z

    if-nez v0, :cond_1

    .line 1292
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/WebSocket;->writeWebSocketClosingHandshake(S)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1298
    :pswitch_1
    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/music/dial/WebSocket;->mPingReceived:Z

    if-eqz v0, :cond_1

    .line 1299
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->writePong()V

    .line 1300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/WebSocket;->mPingReceived:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1289
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized writePong()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 736
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v0, :cond_0

    .line 737
    const-string v0, "WebSocket"

    const-string v1, "Writing Pong with %d bytes data"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/WebSocket;->mPingData:[B

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    :cond_0
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mPingData:[B

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/dial/WebSocket;->writeMessage(B[BZ)V

    .line 740
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mPingData:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 741
    monitor-exit p0

    return-void

    .line 736
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized writeToChannel()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1543
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1544
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    iget-object v2, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 1545
    .local v0, "count":I
    if-gez v0, :cond_1

    .line 1546
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_0

    .line 1547
    const-string v1, "WebSocket"

    const-string v2, "writeToChannel: throwing ClosedChannelException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1549
    :cond_0
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1543
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1551
    .restart local v0    # "count":I
    :cond_1
    :try_start_1
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_2

    .line 1552
    const-string v1, "WebSocket"

    const-string v2, "writeToChannel: count %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1554
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1555
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized writeWebSocketClosingHandshake(S)V
    .locals 3
    .param p1, "status"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 751
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_0

    .line 752
    const-string v1, "WebSocket"

    const-string v2, "writing WebSocket closing handshake"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 755
    .local v0, "buf":[B
    const/4 v1, 0x0

    shr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 756
    const/4 v1, 0x1

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 757
    const/16 v1, 0x8

    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/music/dial/WebSocket;->writeMessage(B[BZ)V

    .line 758
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeSent:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 759
    monitor-exit p0

    return-void

    .line 751
    .end local v0    # "buf":[B
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized writeWebSocketOpeningHandshake()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 598
    monitor-enter p0

    const/4 v5, 0x2

    :try_start_0
    iput v5, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 599
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v5}, Lcom/google/android/music/dial/WebSocketMultiplexer;->generateRandomKey()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketKey:Ljava/lang/String;

    .line 600
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 601
    .local v4, "sb":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v2

    .line 602
    .local v2, "query":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 603
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 604
    const-string v1, "/"

    .line 606
    :cond_0
    const-string v5, "GET %s HTTP/1.1\r\nHost: %s\r\nUpgrade: WebSocket\r\nConnection: Upgrade\r\nSec-WebSocket-Key: %s\r\nOrigin: %s\r\nSec-WebSocket-Version: 13\r\n"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-eqz v2, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "?"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .end local v1    # "path":Ljava/lang/String;
    :cond_1
    aput-object v1, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/google/android/music/dial/WebSocket;->mUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketKey:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/google/android/music/dial/WebSocket;->mOrigin:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mProtocol:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 613
    const-string v5, "Sec-WebSocket-Protocol"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/dial/WebSocket;->mProtocol:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    :cond_2
    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 618
    .local v3, "request":Ljava/lang/String;
    sget-boolean v5, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v5, :cond_3

    .line 619
    const-string v5, "WebSocket"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sending WebSocket handshake:\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mCharset:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 624
    .local v0, "data":[B
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v5}, Lcom/google/android/music/dial/CircularByteBuffer;->availableToWrite()I

    move-result v5

    array-length v6, v0

    if-ge v5, v6, :cond_4

    .line 625
    new-instance v5, Ljava/io/IOException;

    const-string v6, "no room in buffer"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 598
    .end local v0    # "data":[B
    .end local v2    # "query":Ljava/lang/String;
    .end local v3    # "request":Ljava/lang/String;
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 628
    .restart local v0    # "data":[B
    .restart local v2    # "query":Ljava/lang/String;
    .restart local v3    # "request":Ljava/lang/String;
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    :try_start_1
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Lcom/google/android/music/dial/CircularByteBuffer;->writeBytes([B[B)Z

    .line 629
    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v5}, Lcom/google/android/music/dial/WebSocketMultiplexer;->wakeup()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 630
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized connect(Landroid/net/Uri;I)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "connectTimeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 355
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/music/dial/WebSocket;->mUri:Landroid/net/Uri;

    .line 356
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPort()I

    move-result v3

    iput v3, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    .line 357
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 359
    .local v2, "uriScheme":Ljava/lang/String;
    const-string v3, "ws"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 360
    iget v3, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    if-ne v3, v4, :cond_0

    .line 361
    const/16 v3, 0x50

    iput v3, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    .line 363
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mIsSecure:Z

    .line 374
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v3}, Lcom/google/android/music/dial/WebSocketMultiplexer;->start()V

    .line 375
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->resetState()V

    .line 377
    const/4 v1, 0x0

    .line 378
    .local v1, "inetAddress":Ljava/net/InetAddress;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v4, 0xd

    if-gt v3, v4, :cond_1

    .line 380
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/dial/WebSocket;->createInetAddressObject(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 387
    :cond_1
    :goto_1
    if-eqz v1, :cond_5

    :try_start_2
    new-instance v3, Ljava/net/InetSocketAddress;

    iget v4, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    invoke-direct {v3, v1, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    :goto_2
    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mRemoteAddress:Ljava/net/InetSocketAddress;

    .line 389
    const/16 v3, 0x3e8

    if-lt p2, v3, :cond_6

    int-to-long v4, p2

    :goto_3
    iput-wide v4, p0, Lcom/google/android/music/dial/WebSocket;->mConnectTimeout:J

    .line 391
    const/4 v3, 0x1

    iput v3, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 393
    const/4 v3, -0x1

    iput-byte v3, p0, Lcom/google/android/music/dial/WebSocket;->mPrevDataOpcode:B

    .line 394
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeSent:Z

    .line 395
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeReceived:Z

    .line 396
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mPingReceived:Z

    .line 397
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mPongReceived:Z

    .line 398
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mPingData:[B

    .line 399
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/music/dial/WebSocket;->mCloseCode:I

    .line 401
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v3, p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->addSocket(Lcom/google/android/music/dial/WebSocket;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 402
    monitor-exit p0

    return-void

    .line 364
    .end local v1    # "inetAddress":Ljava/net/InetAddress;
    :cond_2
    :try_start_3
    const-string v3, "wss"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 365
    iget v3, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    if-ne v3, v4, :cond_3

    .line 366
    const/16 v3, 0x1bb

    iput v3, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    .line 368
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mIsSecure:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 355
    .end local v2    # "uriScheme":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 370
    .restart local v2    # "uriScheme":Ljava/lang/String;
    :cond_4
    :try_start_4
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scheme not valid for websocket "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 381
    .restart local v1    # "inetAddress":Ljava/net/InetAddress;
    :catch_0
    move-exception v0

    .line 382
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "WebSocket"

    const-string v4, "Unable to create InetAddress object."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 383
    const/4 v1, 0x0

    goto :goto_1

    .line 387
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    new-instance v3, Ljava/net/InetSocketAddress;

    iget-object v4, p0, Lcom/google/android/music/dial/WebSocket;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/google/android/music/dial/WebSocket;->mPort:I

    invoke-direct {v3, v4, v5}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 389
    :cond_6
    const-wide/16 v4, 0x3e8

    goto :goto_3
.end method

.method public declared-synchronized disconnect()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 461
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnectStartTime:J

    .line 462
    iget v0, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 463
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    .line 464
    iget-boolean v0, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeSent:Z

    if-nez v0, :cond_0

    .line 465
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/WebSocket;->writeWebSocketClosingHandshake(S)V

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mMuxer:Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-virtual {v0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->wakeup()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    :goto_0
    monitor-exit p0

    return-void

    .line 470
    :cond_1
    :try_start_1
    sget-boolean v0, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v0, :cond_2

    .line 471
    const-string v0, "WebSocket"

    const-string v1, "force-closing in disconnect()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/WebSocket;->doTeardown(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getSocketChannel()Ljava/nio/channels/SocketChannel;
    .locals 1

    .prologue
    .line 1561
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isConnected()Z
    .locals 2

    .prologue
    .line 1575
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/music/dial/WebSocket;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isConnecting()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1568
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/music/dial/WebSocket;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDisconnecting()Z
    .locals 2

    .prologue
    .line 1582
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/music/dial/WebSocket;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isReceiverAlive()Z
    .locals 1

    .prologue
    .line 725
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/dial/WebSocket;->mPongReceived:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized onConnectable()I
    .locals 3

    .prologue
    .line 1203
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_0

    .line 1204
    const-string v1, "WebSocket"

    const-string v2, "onConnectable"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1206
    :cond_0
    iget v1, p0, Lcom/google/android/music/dial/WebSocket;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1207
    const/4 v1, 0x0

    .line 1227
    :goto_0
    monitor-exit p0

    return v1

    .line 1210
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    .line 1212
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    if-eqz v1, :cond_3

    .line 1213
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    .line 1214
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    .line 1215
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_2

    .line 1216
    const-string v1, "WebSocket"

    const-string v2, "connectable. SSL handshake done"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    :cond_2
    :goto_1
    const/4 v1, -0x1

    goto :goto_0

    .line 1219
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->writeWebSocketOpeningHandshake()V
    :try_end_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1222
    :catch_0
    move-exception v0

    .line 1223
    .local v0, "e":Ljavax/net/ssl/SSLException;
    :try_start_2
    const-string v1, "WebSocket"

    const-string v2, "exception in onConnectable"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1224
    const/4 v1, 0x4

    goto :goto_0

    .line 1225
    .end local v0    # "e":Ljavax/net/ssl/SSLException;
    :catch_1
    move-exception v0

    .line 1226
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "WebSocket"

    const-string v2, "exception in onConnectable"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1227
    const/4 v1, 0x2

    goto :goto_0

    .line 1203
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized onReadable()I
    .locals 11

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x4

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1238
    monitor-enter p0

    :try_start_0
    sget-boolean v6, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v6, :cond_0

    .line 1239
    const-string v6, "WebSocket"

    const-string v7, "onReadable when state is %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1242
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    if-eqz v6, :cond_2

    .line 1243
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->handleSSLRead()V

    .line 1247
    :goto_0
    iget v6, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    if-ne v6, v5, :cond_3

    .line 1248
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->readWebSocketOpeningHandshakeReply()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1249
    const-string v1, "WebSocket"

    const-string v6, "Error reading WebSocket opening handshake reply.  Tearing down..."

    invoke-static {v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/google/android/music/dial/WebSocket;->doTeardown(I)V
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v3

    .line 1278
    :cond_1
    :goto_1
    monitor-exit p0

    return v1

    .line 1245
    :cond_2
    :try_start_2
    iget-object v6, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    iget-object v7, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v6, v7}, Lcom/google/android/music/dial/CircularByteBuffer;->readFromSocketChannel(Ljava/nio/channels/SocketChannel;)I
    :try_end_2
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1270
    :catch_0
    move-exception v0

    .line 1271
    .local v0, "e":Ljava/nio/channels/ClosedChannelException;
    :try_start_3
    const-string v1, "WebSocket"

    const-string v3, "ClosedChannelException when state was %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v1, v2

    .line 1272
    goto :goto_1

    .line 1254
    .end local v0    # "e":Ljava/nio/channels/ClosedChannelException;
    :cond_3
    :try_start_4
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->readMessages()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1255
    sget-boolean v3, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v3, :cond_4

    .line 1256
    const-string v3, "WebSocket"

    const-string v6, "readMessages() returned false"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    if-eqz v3, :cond_5

    .line 1259
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnecting:Z
    :try_end_4
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1261
    :try_start_5
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->closeInbound()V
    :try_end_5
    .catch Ljavax/net/ssl/SSLException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1266
    :cond_5
    :goto_2
    :try_start_6
    iget v3, p0, Lcom/google/android/music/dial/WebSocket;->mState:I
    :try_end_6
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eq v3, v4, :cond_1

    move v1, v2

    goto :goto_1

    .line 1269
    :cond_6
    const/4 v1, -0x1

    goto :goto_1

    .line 1273
    :catch_1
    move-exception v0

    .line 1274
    .local v0, "e":Ljavax/net/ssl/SSLException;
    :try_start_7
    const-string v1, "WebSocket"

    const-string v2, "onReadable SSLException."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v4

    .line 1275
    goto :goto_1

    .line 1276
    .end local v0    # "e":Ljavax/net/ssl/SSLException;
    :catch_2
    move-exception v0

    .line 1277
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "WebSocket"

    const-string v2, "onReadable IOException."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v1, v5

    .line 1278
    goto :goto_1

    .line 1238
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1262
    :catch_3
    move-exception v3

    goto :goto_2
.end method

.method declared-synchronized onWritable()I
    .locals 9

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1314
    monitor-enter p0

    :try_start_0
    sget-boolean v4, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v4, :cond_0

    .line 1315
    const-string v4, "WebSocket"

    const-string v5, "onWritable when state is %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1318
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->writeMessages()V

    .line 1320
    iget-object v4, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    if-eqz v4, :cond_1

    .line 1321
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->handleSSLWrite()V

    .line 1326
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    invoke-virtual {v4}, Lcom/google/android/music/dial/CircularByteBuffer;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1327
    iget v4, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    if-ne v4, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/android/music/dial/WebSocket;->mWebSocketClosingHandshakeReceived:Z
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_2

    .line 1340
    :goto_1
    monitor-exit p0

    return v1

    .line 1323
    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    iget-object v5, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v4, v5}, Lcom/google/android/music/dial/CircularByteBuffer;->writeToSocketChannel(Ljava/nio/channels/SocketChannel;)I
    :try_end_2
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    .local v0, "e":Ljava/nio/channels/ClosedChannelException;
    :try_start_3
    const-string v1, "WebSocket"

    const-string v3, "ClosedChannelException when state was %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 1334
    goto :goto_1

    .line 1331
    .end local v0    # "e":Ljava/nio/channels/ClosedChannelException;
    :cond_2
    const/4 v1, -0x1

    goto :goto_1

    .line 1335
    :catch_1
    move-exception v0

    .line 1336
    .local v0, "e":Ljavax/net/ssl/SSLException;
    const-string v1, "WebSocket"

    const-string v2, "onWritable SSLException."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v3

    .line 1337
    goto :goto_1

    .line 1338
    .end local v0    # "e":Ljavax/net/ssl/SSLException;
    :catch_2
    move-exception v0

    .line 1339
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "WebSocket"

    const-string v2, "onWritable IOException."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1340
    const/4 v1, 0x2

    goto :goto_1

    .line 1314
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized sendMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 506
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->checkConnected()V

    .line 507
    if-nez p1, :cond_0

    .line 508
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "message cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 506
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 511
    :cond_0
    :try_start_1
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_1

    .line 512
    const-string v1, "WebSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mCharset:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 516
    .local v0, "data":[B
    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/music/dial/WebSocket;->writeMessage(B[BZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 517
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setListener(Lcom/google/android/music/dial/WebSocket$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/dial/WebSocket$Listener;

    .prologue
    .line 334
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/music/dial/WebSocket;->mListener:Lcom/google/android/music/dial/WebSocket$Listener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    monitor-exit p0

    return-void

    .line 334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized shutdown(I)V
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 486
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/dial/WebSocket;->doTeardown(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    monitor-exit p0

    return-void

    .line 486
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized startConnect()Ljava/nio/channels/SocketChannel;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/KeyManagementException;,
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/google/android/music/dial/UnsupportedSslProtocolsException;
        }
    .end annotation

    .prologue
    .line 415
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v3, :cond_0

    .line 416
    const-string v3, "WebSocket"

    const-string v4, "startConnect"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/dial/WebSocket;->mConnectStartTime:J

    .line 420
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    .line 421
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 422
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v3}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Ljava/net/Socket;->setSoLinger(ZI)V

    .line 424
    new-instance v3, Lcom/google/android/music/dial/CircularByteBuffer;

    iget v4, p0, Lcom/google/android/music/dial/WebSocket;->mBufferSize:I

    invoke-direct {v3, v4}, Lcom/google/android/music/dial/CircularByteBuffer;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mReadBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    .line 425
    new-instance v3, Lcom/google/android/music/dial/CircularByteBuffer;

    iget v4, p0, Lcom/google/android/music/dial/WebSocket;->mBufferSize:I

    invoke-direct {v3, v4}, Lcom/google/android/music/dial/CircularByteBuffer;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mWriteBuffer:Lcom/google/android/music/dial/CircularByteBuffer;

    .line 427
    iget-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mIsSecure:Z

    if-eqz v3, :cond_1

    .line 428
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/dial/WebSocket;->getSSLContext(Landroid/content/Context;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    .line 429
    .local v2, "sslContext":Ljavax/net/ssl/SSLContext;
    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->createSSLEngine()Ljavax/net/ssl/SSLEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    .line 430
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljavax/net/ssl/SSLEngine;->setUseClientMode(Z)V

    .line 431
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v1

    .line 432
    .local v1, "session":Ljavax/net/ssl/SSLSession;
    invoke-interface {v1}, Ljavax/net/ssl/SSLSession;->getPacketBufferSize()I

    move-result v0

    .line 433
    .local v0, "channelBufferSize":I
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelReadBuffer:Ljava/nio/ByteBuffer;

    .line 434
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLChannelWriteBuffer:Ljava/nio/ByteBuffer;

    .line 435
    sget-object v3, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    .line 436
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mInitialSSLHandshakeComplete:Z

    .line 437
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnecting:Z

    .line 440
    .end local v0    # "channelBufferSize":I
    .end local v1    # "session":Ljavax/net/ssl/SSLSession;
    .end local v2    # "sslContext":Ljavax/net/ssl/SSLContext;
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    iget-object v4, p0, Lcom/google/android/music/dial/WebSocket;->mRemoteAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 444
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    if-eqz v3, :cond_3

    .line 445
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    .line 446
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSSLHandshakeStatus:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    .line 452
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v3

    .line 448
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->writeWebSocketOpeningHandshake()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 415
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method declared-synchronized updateSelectionKey(Ljava/nio/channels/SelectionKey;J)I
    .locals 8
    .param p1, "key"    # Ljava/nio/channels/SelectionKey;
    .param p2, "now"    # J

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 1090
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v3, :cond_0

    .line 1091
    const-string v3, "WebSocket"

    const-string v4, "updateSelectionKey when state=%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/music/dial/WebSocket;->mTornDown:Z

    if-eqz v3, :cond_2

    .line 1094
    const-string v2, "WebSocket"

    const-string v3, "Socket is no longer connected"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/dial/WebSocket;->mTornDown:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1146
    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    .line 1099
    :cond_2
    const/4 v0, 0x0

    .line 1101
    .local v0, "ops":I
    :try_start_1
    iget v3, p0, Lcom/google/android/music/dial/WebSocket;->mState:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1103
    :pswitch_0
    iget-wide v4, p0, Lcom/google/android/music/dial/WebSocket;->mConnectStartTime:J

    sub-long v4, p2, v4

    iget-wide v6, p0, Lcom/google/android/music/dial/WebSocket;->mConnectTimeout:J

    cmp-long v1, v4, v6

    if-ltz v1, :cond_4

    .line 1104
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_3

    .line 1105
    const-string v1, "WebSocket"

    const-string v3, "Connect timed out."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v1, v2

    .line 1108
    goto :goto_0

    .line 1109
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSocket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1110
    const/16 v0, 0x8

    .line 1145
    :cond_5
    :goto_1
    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    .line 1146
    const/4 v1, -0x1

    goto :goto_0

    .line 1111
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocket;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    if-eqz v1, :cond_5

    .line 1112
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->getSSLInterestSet()I

    move-result v1

    or-int/2addr v0, v1

    goto :goto_1

    .line 1117
    :pswitch_1
    iget-wide v4, p0, Lcom/google/android/music/dial/WebSocket;->mConnectStartTime:J

    sub-long v4, p2, v4

    iget-wide v6, p0, Lcom/google/android/music/dial/WebSocket;->mConnectTimeout:J

    cmp-long v1, v4, v6

    if-ltz v1, :cond_8

    .line 1118
    sget-boolean v1, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v1, :cond_7

    .line 1119
    const-string v1, "WebSocket"

    const-string v3, "Connect timed out."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move v1, v2

    .line 1122
    goto :goto_0

    .line 1124
    :cond_8
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->getBasicOps()I

    move-result v1

    or-int/2addr v0, v1

    .line 1125
    goto :goto_1

    .line 1128
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->getBasicOps()I

    move-result v1

    or-int/2addr v0, v1

    .line 1129
    goto :goto_1

    .line 1132
    :pswitch_3
    iget-wide v2, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnectStartTime:J

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/android/music/dial/WebSocket;->mDisconnectTimeout:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 1136
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocket;->getBasicOps()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    or-int/2addr v0, v1

    .line 1137
    goto :goto_1

    .line 1090
    .end local v0    # "ops":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1101
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized writePing()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 714
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/music/dial/WebSocket;->LOGV:Z

    if-eqz v0, :cond_0

    .line 715
    const-string v0, "WebSocket"

    const-string v1, "Writing Ping"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/WebSocket;->mPongReceived:Z

    .line 718
    const/16 v0, 0x9

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/dial/WebSocket;->writeMessage(B[BZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 719
    monitor-exit p0

    return-void

    .line 714
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
