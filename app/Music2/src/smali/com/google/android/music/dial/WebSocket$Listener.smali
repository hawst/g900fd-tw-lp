.class public interface abstract Lcom/google/android/music/dial/WebSocket$Listener;
.super Ljava/lang/Object;
.source "WebSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/WebSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onConnected()V
.end method

.method public abstract onConnectionFailed(I)V
.end method

.method public abstract onContinuationMessageReceived(Ljava/lang/String;Z)V
.end method

.method public abstract onContinuationMessageReceived([BZ)V
.end method

.method public abstract onDisconnected(II)V
.end method

.method public abstract onMessageReceived(Ljava/lang/String;Z)V
.end method

.method public abstract onMessageReceived([BZ)V
.end method
