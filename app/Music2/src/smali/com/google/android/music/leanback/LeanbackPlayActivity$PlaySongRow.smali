.class Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;
.super Lcom/google/android/music/leanback/SongRow;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PlaySongRow"
.end annotation


# instance fields
.field private final mSongList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILcom/google/android/music/medialist/SongList;ZZ)V
    .locals 8
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p4, "position"    # I
    .param p5, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p6, "isPlaying"    # Z
    .param p7, "isLast"    # Z

    .prologue
    const/4 v4, 0x0

    .line 79
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v5, v4

    move v6, p6

    move v7, p7

    .line 80
    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/leanback/SongRow;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;ZZ)V

    .line 81
    iput-object p5, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 82
    return-void
.end method


# virtual methods
.method getRadioIntentListener()Lcom/google/android/music/leanback/SongRow$RadioIntentListener;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;)V

    return-object v0
.end method

.method onClicked()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;->getPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    .line 87
    return-void
.end method

.method onRadioClicked()V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/google/android/music/leanback/SongRow;->onRadioClicked()V

    .line 102
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    const v1, 0x7f0e014f

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/VerticalGridView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPositionSmooth(I)V

    .line 104
    return-void
.end method
