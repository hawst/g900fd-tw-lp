.class public Lcom/google/android/music/browse/MediaBrowserService;
.super Landroid/service/media/MediaBrowserService;
.source "MediaBrowserService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBrowserUtil:Lcom/google/android/music/browse/BrowserUtil;

.field private mConfigurationManager:Lcom/google/android/music/browse/config/ClientConfigurationManager;

.field private mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/music/browse/MediaBrowserService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/service/media/MediaBrowserService;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method getMediaItem(Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;)Ljava/util/List;
    .locals 12
    .param p1, "item"    # Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v9, "results":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    invoke-virtual {p1}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getContent()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    .line 128
    iget-object v10, p0, Lcom/google/android/music/browse/MediaBrowserService;->mBrowserUtil:Lcom/google/android/music/browse/BrowserUtil;

    invoke-virtual {p1}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getContent()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/music/browse/BrowserUtil;->retrieveContent(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 166
    .end local v9    # "results":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    :cond_0
    return-object v9

    .line 129
    .restart local v9    # "results":Ljava/util/List;, "Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;"
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getItems()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 131
    invoke-virtual {p1}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getItems()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;

    .line 132
    .local v0, "child":Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;
    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getDisplayResourceId()I

    move-result v3

    .line 133
    .local v3, "displayResource":I
    if-nez v3, :cond_2

    .line 134
    sget-object v10, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    const-string v11, "Display name is not set or invalid! skipping..."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/music/browse/MediaBrowserService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 142
    .local v2, "displayName":Ljava/lang/String;
    const v10, 0x7f0b0099

    if-ne v3, v10, :cond_4

    .line 143
    new-instance v8, Ljava/lang/Object;

    invoke-direct {v8}, Ljava/lang/Object;-><init>()V

    .line 144
    .local v8, "prefsOwner":Ljava/lang/Object;
    invoke-static {p0, v8}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    .line 147
    .local v7, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v5

    .line 148
    .local v5, "isNautilusEnabled":Z
    if-nez v5, :cond_3

    .line 149
    const v10, 0x7f0b009a

    invoke-virtual {p0, v10}, Lcom/google/android/music/browse/MediaBrowserService;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 152
    :cond_3
    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 156
    .end local v5    # "isNautilusEnabled":Z
    .end local v7    # "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v8    # "prefsOwner":Ljava/lang/Object;
    :cond_4
    new-instance v10, Landroid/media/MediaDescription$Builder;

    invoke-direct {v10}, Landroid/media/MediaDescription$Builder;-><init>()V

    invoke-virtual {v10, v2}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    move-result-object v10

    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration$BrowseItem;->getFullId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    move-result-object v10

    invoke-virtual {v10}, Landroid/media/MediaDescription$Builder;->build()Landroid/media/MediaDescription;

    move-result-object v1

    .line 160
    .local v1, "description":Landroid/media/MediaDescription;
    new-instance v6, Landroid/media/browse/MediaBrowser$MediaItem;

    const/4 v10, 0x1

    invoke-direct {v6, v1, v10}, Landroid/media/browse/MediaBrowser$MediaItem;-><init>(Landroid/media/MediaDescription;I)V

    .line 162
    .local v6, "mediaItem":Landroid/media/browse/MediaBrowser$MediaItem;
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    .end local v1    # "description":Landroid/media/MediaDescription;
    .end local v6    # "mediaItem":Landroid/media/browse/MediaBrowser$MediaItem;
    .restart local v7    # "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v8    # "prefsOwner":Ljava/lang/Object;
    :catchall_0
    move-exception v10

    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v10
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 44
    invoke-super {p0}, Landroid/service/media/MediaBrowserService;->onCreate()V

    .line 45
    new-instance v1, Lcom/google/android/music/browse/config/ClientConfigurationManager;

    invoke-virtual {p0}, Lcom/google/android/music/browse/MediaBrowserService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/music/browse/config/ClientConfigurationManager;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    iput-object v1, p0, Lcom/google/android/music/browse/MediaBrowserService;->mConfigurationManager:Lcom/google/android/music/browse/config/ClientConfigurationManager;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/music/browse/MediaBrowserService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00e3

    invoke-virtual {p0, v2}, Lcom/google/android/music/browse/MediaBrowserService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4, v4}, Lcom/google/android/music/playback/MediaSessionUtil;->getSessionInstance(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/os/Looper;)Lcom/google/android/music/playback/session/MediaSessionCompat;

    move-result-object v0

    .line 51
    .local v0, "mediaSession":Lcom/google/android/music/playback/session/MediaSessionCompat;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat;->getSessionToken()Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 52
    invoke-virtual {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat;->getSessionToken()Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Token;->getToken()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/session/MediaSession$Token;

    invoke-virtual {p0, v1}, Lcom/google/android/music/browse/MediaBrowserService;->setSessionToken(Landroid/media/session/MediaSession$Token;)V

    .line 57
    :goto_0
    new-instance v1, Lcom/google/android/music/browse/BrowserUtil;

    invoke-direct {v1, p0}, Lcom/google/android/music/browse/BrowserUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/music/browse/MediaBrowserService;->mBrowserUtil:Lcom/google/android/music/browse/BrowserUtil;

    .line 59
    new-instance v1, Lcom/google/android/music/browse/MediaBrowserService$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/browse/MediaBrowserService$1;-><init>(Lcom/google/android/music/browse/MediaBrowserService;)V

    invoke-static {p0, v1}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/browse/MediaBrowserService;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 73
    return-void

    .line 54
    :cond_0
    sget-object v1, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    const-string v2, "Cannot get media session token! Playback control won\'t work!"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Landroid/service/media/MediaBrowserService;->onDestroy()V

    .line 97
    iget-object v0, p0, Lcom/google/android/music/browse/MediaBrowserService;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/music/browse/MediaBrowserService;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V

    .line 100
    :cond_0
    return-void
.end method

.method public onGetRoot(Ljava/lang/String;ILandroid/os/Bundle;)Landroid/service/media/MediaBrowserService$BrowserRoot;
    .locals 5
    .param p1, "clientPkgName"    # Ljava/lang/String;
    .param p2, "clientUid"    # I
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 78
    sget-object v2, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onGetRoot for"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v2, "user"

    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/browse/MediaBrowserService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/android/music/utils/PackageValidator;->isGoogleSignedGearHead(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 83
    sget-object v2, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not allowed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_0
    :goto_0
    return-object v1

    .line 87
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/browse/MediaBrowserService;->mConfigurationManager:Lcom/google/android/music/browse/config/ClientConfigurationManager;

    invoke-virtual {v2, p1}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->getConfiguration(Ljava/lang/String;)Lcom/google/android/music/browse/config/ClientConfiguration;

    move-result-object v0

    .line 88
    .local v0, "client":Lcom/google/android/music/browse/config/ClientConfiguration;
    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v0}, Lcom/google/android/music/browse/config/ClientConfiguration;->getBrowserRoot()Landroid/service/media/MediaBrowserService$BrowserRoot;

    move-result-object v1

    goto :goto_0
.end method

.method public onLoadChildren(Ljava/lang/String;Landroid/service/media/MediaBrowserService$Result;)V
    .locals 5
    .param p1, "mediaId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/service/media/MediaBrowserService$Result",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/media/browse/MediaBrowser$MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p2, "listResult":Landroid/service/media/MediaBrowserService$Result;, "Landroid/service/media/MediaBrowserService$Result<Ljava/util/List<Landroid/media/browse/MediaBrowser$MediaItem;>;>;"
    sget-object v2, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLoadChildren "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/service/media/MediaBrowserService$Result;->detach()V

    .line 109
    invoke-static {p1}, Lcom/google/android/music/browse/BrowsableMediaId;->fromString(Ljava/lang/String;)Lcom/google/android/music/browse/BrowsableMediaId;

    move-result-object v1

    .line 110
    .local v1, "id":Lcom/google/android/music/browse/BrowsableMediaId;
    iget-object v2, p0, Lcom/google/android/music/browse/MediaBrowserService;->mConfigurationManager:Lcom/google/android/music/browse/config/ClientConfigurationManager;

    invoke-virtual {v1}, Lcom/google/android/music/browse/BrowsableMediaId;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/browse/config/ClientConfigurationManager;->getConfiguration(Ljava/lang/String;)Lcom/google/android/music/browse/config/ClientConfiguration;

    move-result-object v0

    .line 111
    .local v0, "client":Lcom/google/android/music/browse/config/ClientConfiguration;
    if-eqz v0, :cond_0

    .line 112
    new-instance v2, Lcom/google/android/music/browse/MediaBrowserService$2;

    invoke-direct {v2, p0, p2, v0, v1}, Lcom/google/android/music/browse/MediaBrowserService$2;-><init>(Lcom/google/android/music/browse/MediaBrowserService;Landroid/service/media/MediaBrowserService$Result;Lcom/google/android/music/browse/config/ClientConfiguration;Lcom/google/android/music/browse/BrowsableMediaId;)V

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    sget-object v2, Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No client configuration registered for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/music/browse/BrowsableMediaId;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
