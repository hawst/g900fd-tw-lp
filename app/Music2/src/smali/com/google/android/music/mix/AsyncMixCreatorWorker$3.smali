.class Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;
.super Ljava/lang/Object;
.source "AsyncMixCreatorWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/mix/AsyncMixCreatorWorker;->createMix()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;


# direct methods
.method constructor <init>(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    .line 285
    :try_start_0
    const-string v2, "MusicMixCreator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Creating mix: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v4}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$600(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget-object v3, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v3}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$600(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    iget-object v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v2}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$600(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "title":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 295
    iget-object v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    iget-object v3, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v4}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$600(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v4

    invoke-static {v3, v5, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->sendMessage(Landroid/os/Message;)Z

    .line 300
    .end local v1    # "title":Ljava/lang/String;
    :goto_0
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    iget-object v3, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    iget-object v4, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mMix:Lcom/google/android/music/mix/MixDescriptor;
    invoke-static {v4}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$600(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v4

    invoke-static {v3, v5, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 299
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "title":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/mix/AsyncMixCreatorWorker$3;->this$0:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    # getter for: Lcom/google/android/music/mix/AsyncMixCreatorWorker;->mCallback:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;
    invoke-static {v2}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->access$300(Lcom/google/android/music/mix/AsyncMixCreatorWorker;)Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;->onStart(Ljava/lang/String;)V

    goto :goto_0
.end method
