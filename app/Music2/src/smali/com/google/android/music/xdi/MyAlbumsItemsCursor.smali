.class Lcom/google/android/music/xdi/MyAlbumsItemsCursor;
.super Landroid/database/MatrixCursor;
.source "MyAlbumsItemsCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 39
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mImageWidth:I

    .line 40
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mImageHeight:I

    .line 42
    invoke-direct {p0}, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->addRowsForArtists()V

    .line 43
    return-void
.end method

.method private addRowsForArtists()V
    .locals 18

    .prologue
    .line 46
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/google/android/music/store/MusicContent$Albums;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v12

    .line 51
    .local v12, "cursor":Landroid/database/Cursor;
    if-nez v12, :cond_0

    .line 80
    :goto_0
    return-void

    .line 56
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 58
    .local v14, "id":J
    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 59
    .local v16, "name":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 60
    .local v11, "artist":Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mImageWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mImageHeight:I

    invoke-static {v14, v15, v2, v3, v4}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v10

    .line 62
    .local v10, "artUri":Landroid/net/Uri;
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    .line 64
    .local v13, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v17, v0

    .line 65
    .local v17, "values":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "parent_id"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_description"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "image_uri"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "width"

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "height"

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "intent_uri"

    const/4 v4, 0x1

    invoke-virtual {v13, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 78
    .end local v10    # "artUri":Landroid/net/Uri;
    .end local v11    # "artist":Ljava/lang/String;
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v14    # "id":J
    .end local v16    # "name":Ljava/lang/String;
    .end local v17    # "values":[Ljava/lang/Object;
    :catchall_0
    move-exception v2

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    :cond_1
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method
