.class Lcom/google/android/music/AsyncAlbumArtImageView$1;
.super Ljava/lang/Object;
.source "AsyncAlbumArtImageView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final color:I

.field final synthetic this$0:Lcom/google/android/music/AsyncAlbumArtImageView;


# direct methods
.method constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView;)V
    .locals 2

    .prologue
    .line 1094
    iput-object p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1095
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->color:I

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1099
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1120
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v1, 0x0

    return v1

    .line 1101
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    # getter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;
    invoke-static {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$300(Lcom/google/android/music/AsyncAlbumArtImageView;)Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->FOREGROUND:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1103
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    if-eqz v0, :cond_0

    .line 1104
    iget v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->color:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1105
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->invalidate()V

    goto :goto_0

    .line 1101
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 1111
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    # getter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;
    invoke-static {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$300(Lcom/google/android/music/AsyncAlbumArtImageView;)Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->FOREGROUND:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1113
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    if-eqz v0, :cond_0

    .line 1114
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 1115
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->invalidate()V

    goto :goto_0

    .line 1111
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_2

    .line 1099
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
