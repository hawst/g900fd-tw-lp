.class Lcom/google/android/music/ui/NowPlayingScreenFragment$4;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueuePlayingFromHeaderListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRefreshClicked()V
    .locals 4

    .prologue
    .line 324
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mLockNowPlayingScreen:Z
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$700(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    :goto_0
    return-void

    .line 325
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->lockNowPlayingScreen()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$800(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    .line 328
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 329
    .local v1, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->refreshRadio()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 330
    .end local v1    # "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "NowPlayingFragment"

    const-string v3, "Unable to get playback service, refreshing station failed."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->unlockNowPlayingScreen()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto :goto_0
.end method
