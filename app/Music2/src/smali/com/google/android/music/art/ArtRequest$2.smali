.class Lcom/google/android/music/art/ArtRequest$2;
.super Ljava/lang/Object;
.source "ArtRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtRequest;->onLoadFinished(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtRequest;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtRequest;)V
    .locals 0

    .prologue
    .line 157
    .local p0, "this":Lcom/google/android/music/art/ArtRequest$2;, "Lcom/google/android/music/art/ArtRequest.2;"
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest$2;->this$0:Lcom/google/android/music/art/ArtRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 160
    .local p0, "this":Lcom/google/android/music/art/ArtRequest$2;, "Lcom/google/android/music/art/ArtRequest.2;"
    # getter for: Lcom/google/android/music/art/ArtRequest;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/art/ArtRequest;->access$000()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 161
    const-string v3, "ArtRequest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onLoadFinished "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/art/ArtRequest$2;->this$0:Lcom/google/android/music/art/ArtRequest;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest$2;->this$0:Lcom/google/android/music/art/ArtRequest;

    # getter for: Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/google/android/music/art/ArtRequest;->access$100(Lcom/google/android/music/art/ArtRequest;)Ljava/util/HashSet;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 165
    .local v0, "copy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/art/ArtRequest$Listener<TT;>;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/art/ArtRequest$Listener;

    .line 166
    .local v2, "l":Lcom/google/android/music/art/ArtRequest$Listener;, "Lcom/google/android/music/art/ArtRequest$Listener<TT;>;"
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest$2;->this$0:Lcom/google/android/music/art/ArtRequest;

    invoke-interface {v2, v3}, Lcom/google/android/music/art/ArtRequest$Listener;->onLoadFinished(Lcom/google/android/music/art/ArtRequest;)V

    goto :goto_0

    .line 168
    .end local v2    # "l":Lcom/google/android/music/art/ArtRequest$Listener;, "Lcom/google/android/music/art/ArtRequest$Listener<TT;>;"
    :cond_1
    return-void
.end method
