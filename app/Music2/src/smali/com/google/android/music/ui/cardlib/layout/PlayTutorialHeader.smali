.class public Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
.super Landroid/widget/LinearLayout;
.source "PlayTutorialHeader.java"


# instance fields
.field private mActionsView:Landroid/view/ViewGroup;

.field private mBodyView:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 36
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 37
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0400c7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    return-object v1
.end method

.method private inflateIconsAndTextList(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Landroid/widget/AdapterView$OnItemClickListener;I)V
    .locals 19
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;
    .param p3, "itemResId"    # I
    .param p4, "entries"    # [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    .param p5, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;
    .param p6, "dividerDrawableResId"    # I

    .prologue
    .line 105
    const/16 v16, 0x0

    .line 106
    .local v16, "index":I
    move-object/from16 v4, p4

    .local v4, "arr$":[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    array-length v0, v4

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_0
    move/from16 v0, v17

    if-ge v14, v0, :cond_2

    aget-object v13, v4, v14

    .line 107
    .local v13, "entry":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    if-eqz p6, :cond_0

    .line 108
    new-instance v12, Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v12, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 109
    .local v12, "divider":Landroid/view/View;
    move/from16 v0, p6

    invoke-virtual {v12, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 110
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    .end local v12    # "divider":Landroid/view/View;
    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p3

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 116
    .local v8, "row":Landroid/view/View;
    const v5, 0x7f0e0235

    invoke-virtual {v8, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 117
    .local v15, "iconView":Landroid/widget/ImageView;
    const v5, 0x7f0e0236

    invoke-virtual {v8, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 118
    .local v18, "textView":Landroid/widget/TextView;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 120
    iget v5, v13, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;->mIconId:I

    invoke-virtual {v15, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 121
    iget v5, v13, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;->mDescriptionId:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 123
    if-eqz p5, :cond_1

    .line 124
    move/from16 v9, v16

    .line 125
    .local v9, "position":I
    iget v5, v13, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;->mDescriptionId:I

    int-to-long v10, v5

    .line 126
    .local v10, "id":J
    new-instance v5, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader$1;

    move-object/from16 v6, p0

    move-object/from16 v7, p5

    invoke-direct/range {v5 .. v11}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader$1;-><init>(Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;Landroid/widget/AdapterView$OnItemClickListener;Landroid/view/View;IJ)V

    invoke-virtual {v8, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    .end local v9    # "position":I
    .end local v10    # "id":J
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 135
    add-int/lit8 v16, v16, 0x1

    .line 106
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 137
    .end local v8    # "row":Landroid/view/View;
    .end local v13    # "entry":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    .end local v15    # "iconView":Landroid/widget/ImageView;
    .end local v18    # "textView":Landroid/widget/TextView;
    :cond_2
    return-void
.end method


# virtual methods
.method public inflateBody(I)Landroid/view/View;
    .locals 2
    .param p1, "bodyLayoutResId"    # I

    .prologue
    .line 69
    const v1, 0x7f0e0238

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 70
    .local v0, "stub":Landroid/view/ViewStub;
    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 71
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->mBodyView:Landroid/view/View;

    .line 72
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->mBodyView:Landroid/view/View;

    return-object v1
.end method

.method public inflateBody([Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;)V
    .locals 7
    .param p1, "bullets"    # [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    .prologue
    .line 88
    const v0, 0x7f0400c4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->inflateBody(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 89
    .local v2, "body":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 91
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f0400c6

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->inflateIconsAndTextList(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Landroid/widget/AdapterView$OnItemClickListener;I)V

    .line 93
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 52
    const v0, 0x7f0e0237

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->mTitleView:Landroid/widget/TextView;

    .line 53
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->mTitleView:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 55
    const v0, 0x7f0e023a

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->mActionsView:Landroid/view/ViewGroup;

    .line 56
    return-void
.end method

.method public setTitleResId(I)V
    .locals 1
    .param p1, "titleStringResId"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 63
    return-void
.end method

.method public setupActionsList([Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 7
    .param p1, "actions"    # [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    .param p2, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 80
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->mActionsView:Landroid/view/ViewGroup;

    const v3, 0x7f0400c3

    const v6, 0x7f020246

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->inflateIconsAndTextList(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Landroid/widget/AdapterView$OnItemClickListener;I)V

    .line 82
    return-void
.end method
