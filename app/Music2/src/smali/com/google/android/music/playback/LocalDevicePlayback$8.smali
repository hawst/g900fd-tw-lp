.class Lcom/google/android/music/playback/LocalDevicePlayback$8;
.super Landroid/os/Handler;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCurrentVolume:F

.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 1

    .prologue
    .line 872
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 874
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x5

    const/high16 v5, 0x3f800000    # 1.0f

    const v3, 0x3e4ccccd    # 0.2f

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 877
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mMediaplayerHandler.handleMessage "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 878
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1036
    :cond_0
    :goto_0
    return-void

    .line 880
    :pswitch_0
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    const v1, 0x3d4ccccd    # 0.05f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    .line 881
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    .line 882
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 886
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    invoke-interface {v0, v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setVolume(F)V

    goto :goto_0

    .line 884
    :cond_1
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    goto :goto_1

    .line 889
    :pswitch_1
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    const v1, 0x3c23d70a    # 0.01f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    .line 890
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_2

    .line 891
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 895
    :goto_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    invoke-interface {v0, v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setVolume(F)V

    goto :goto_0

    .line 893
    :cond_2
    iput v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    goto :goto_2

    .line 898
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 899
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V
    invoke-static {v0, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3700(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    goto :goto_0

    .line 905
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V

    goto :goto_0

    .line 909
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 910
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling: TRACK_WENT_TO_NEXT: mMixGenerationState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayPos:I
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 914
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$8$1;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$8;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 936
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 937
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TRACK_ENDED: mMixGenerationState="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4100(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 940
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndPlay(Z)V
    invoke-static {v0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4200(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    goto/16 :goto_0

    .line 942
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V
    invoke-static {v0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3700(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    goto/16 :goto_0

    .line 946
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2300(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .line 952
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_1

    .line 990
    :pswitch_7
    const-string v0, "LocalDevicePlayback"

    const-string v1, "Unknown audio focus change code"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 957
    :pswitch_8
    const-string v0, "LocalDevicePlayback"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->pause()V

    goto/16 :goto_0

    .line 961
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 962
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 965
    :pswitch_a
    const-string v0, "LocalDevicePlayback"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->pause(Z)V
    invoke-static {v0, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4300(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    goto/16 :goto_0

    .line 969
    :pswitch_b
    const-string v0, "LocalDevicePlayback"

    const-string v1, "AudioFocus: received AUDIOFOCUS_GAIN"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 971
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$8;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 994
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 995
    const-string v0, "LocalDevicePlayback"

    const-string v1, "TRYNEXT"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    :cond_7
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->next(Z)V
    invoke-static {v0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3700(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    goto/16 :goto_0

    .line 1000
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1001
    const-string v0, "LocalDevicePlayback"

    const-string v1, "SET_MEDIA_ROUTE"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    :cond_8
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;

    .line 1004
    .local v10, "selectedRoute":Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-boolean v1, v10, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mLocalRoute:Z

    iget-object v2, v10, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mRouteId:Ljava/lang/String;

    iget-boolean v3, v10, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mAutoPlay:Z

    iget-wide v4, v10, Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;->mPosition:J

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaRouteImpl(ZLjava/lang/String;ZJ)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4500(Lcom/google/android/music/playback/LocalDevicePlayback;ZLjava/lang/String;ZJ)V

    goto/16 :goto_0

    .line 1008
    .end local v10    # "selectedRoute":Lcom/google/android/music/playback/LocalDevicePlayback$SelectedRoute;
    :pswitch_e
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;

    .line 1009
    .local v9, "routeVolume":Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, v9, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;->mRouteId:Ljava/lang/String;

    iget-wide v2, v9, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;->mValue:D

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaRouteVolumeImpl(Ljava/lang/String;D)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4600(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;D)V

    goto/16 :goto_0

    .line 1013
    .end local v9    # "routeVolume":Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;
    :pswitch_f
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;

    .line 1014
    .restart local v9    # "routeVolume":Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, v9, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;->mRouteId:Ljava/lang/String;

    iget-wide v2, v9, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;->mValue:D

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->updateMediaRouteVolumeImpl(Ljava/lang/String;D)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4700(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;D)V

    goto/16 :goto_0

    .line 1018
    .end local v9    # "routeVolume":Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1019
    const-string v0, "LocalDevicePlayback"

    const-string v1, "SELECT_DEFAULT_MEDIA_ROUTE"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    :cond_9
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->selectDefaultMediaRouteImpl()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4800(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    goto/16 :goto_0

    .line 1024
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1025
    const-string v0, "LocalDevicePlayback"

    const-string v1, "RESET_VOLUME_AND_PLAY"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    :cond_a
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    .line 1028
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->mCurrentVolume:F

    invoke-interface {v0, v1}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setVolume(F)V

    .line 1031
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    goto/16 :goto_0

    .line 878
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_c
        :pswitch_3
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 955
    :pswitch_data_1
    .packed-switch -0x3
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_7
        :pswitch_b
    .end packed-switch
.end method
