.class Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;
.super Ljava/lang/Object;
.source "MediaSessionCompatPreL.java"

# interfaces
.implements Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;
.implements Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;
.implements Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompatPreL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CallbackProxy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;",
        "Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;",
        "Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;"
    }
.end annotation


# instance fields
.field protected final mCallback:Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;, "Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy<TT;>;"
    .local p1, "callback":Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;->mCallback:Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;

    .line 96
    return-void
.end method


# virtual methods
.method public onGetPlaybackPosition()J
    .locals 2

    .prologue
    .line 105
    .local p0, "this":Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;, "Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy<TT;>;"
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;->mCallback:Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;

    invoke-interface {v0}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;->onGetPlaybackPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public onMetadataUpdate(ILjava/lang/Object;)V
    .locals 1
    .param p1, "key"    # I
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 110
    .local p0, "this":Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;, "Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy<TT;>;"
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;->mCallback:Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;->onMetadataUpdate(ILjava/lang/Object;)V

    .line 111
    return-void
.end method

.method public onPlaybackPositionUpdate(J)V
    .locals 1
    .param p1, "newPosition"    # J

    .prologue
    .line 100
    .local p0, "this":Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;, "Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy<TT;>;"
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$CallbackProxy;->mCallback:Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL$Callback;->onPlaybackPositionUpdate(J)V

    .line 101
    return-void
.end method
