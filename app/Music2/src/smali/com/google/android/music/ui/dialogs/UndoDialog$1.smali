.class Lcom/google/android/music/ui/dialogs/UndoDialog$1;
.super Ljava/lang/Object;
.source "UndoDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/dialogs/UndoDialog;-><init>(Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/dialogs/UndoDialog;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$1;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$1;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mRemoveHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$000(Lcom/google/android/music/ui/dialogs/UndoDialog;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$1;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mListener:Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$100(Lcom/google/android/music/ui/dialogs/UndoDialog;)Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;->undoClicked()V

    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$1;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    # getter for: Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->access$200(Lcom/google/android/music/ui/dialogs/UndoDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 69
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog$1;->this$0:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->dismiss()V

    .line 70
    return-void
.end method
