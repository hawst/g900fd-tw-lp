.class public abstract Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.super Landroid/support/v17/leanback/widget/CursorObjectAdapter;
.source "LeanbackCursorObjectAdapter.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/CursorObjectAdapter;-><init>()V

    .line 17
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->setMapper()V

    .line 18
    return-void
.end method

.method constructor <init>(Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0
    .param p1, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/CursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 22
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->setMapper()V

    .line 23
    return-void
.end method

.method private setMapper()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->setMapper(Landroid/support/v17/leanback/database/CursorMapper;)V

    .line 48
    return-void
.end method


# virtual methods
.method protected abstract bind(Landroid/database/Cursor;)Ljava/lang/Object;
.end method

.method protected bindColumns(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 31
    return-void
.end method

.method notifyPositionChanged(I)V
    .locals 1
    .param p1, "cursorPosition"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->invalidateCache(I)V

    .line 27
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->notifyItemRangeChanged(II)V

    .line 28
    return-void
.end method
