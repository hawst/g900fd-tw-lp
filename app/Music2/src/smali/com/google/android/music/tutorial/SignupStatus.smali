.class public Lcom/google/android/music/tutorial/SignupStatus;
.super Ljava/lang/Object;
.source "SignupStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/tutorial/SignupStatus$NetworkChangedReceiver;,
        Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;
    }
.end annotation


# direct methods
.method static synthetic access$200(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->isBackgroundDataEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Landroid/content/SharedPreferences;)I
    .locals 1
    .param p0, "x0"    # Landroid/content/SharedPreferences;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getVerifiedStatus(Landroid/content/SharedPreferences;)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/accounts/Account;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/content/SharedPreferences;
    .param p2, "x2"    # Landroid/accounts/Account;

    .prologue
    .line 40
    invoke-static {p0, p1, p2}, Lcom/google/android/music/tutorial/SignupStatus;->isAccountVerified(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method public static getAllAccounts(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 69
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v7, "com.google"

    invoke-virtual {v1, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 70
    .local v2, "accounts":[Landroid/accounts/Account;
    if-eqz v2, :cond_2

    array-length v7, v2

    if-eqz v7, :cond_2

    .line 72
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 73
    .local v6, "purgedAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    move-object v3, v2

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v5, v3

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v3, v4

    .line 74
    .local v0, "account":Landroid/accounts/Account;
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v8, "@youtube.com"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 75
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 78
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    array-length v8, v2

    if-eq v7, v8, :cond_2

    .line 79
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_3

    .line 80
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Landroid/accounts/Account;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "accounts":[Landroid/accounts/Account;
    check-cast v2, [Landroid/accounts/Account;

    .line 86
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "purgedAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_2
    :goto_1
    return-object v2

    .line 82
    .restart local v3    # "arr$":[Landroid/accounts/Account;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "purgedAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "mode":I
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 53
    or-int/lit8 v0, v0, 0x4

    .line 55
    :cond_0
    const-string v1, "signup.pref"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    return-object v1
.end method

.method private static getVerifiedAccounts(Landroid/content/Context;Landroid/content/SharedPreferences;)[Landroid/accounts/Account;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 59
    invoke-static {p1}, Lcom/google/android/music/tutorial/SignupStatus;->getVerifiedStatus(Landroid/content/SharedPreferences;)I

    move-result v0

    .line 60
    .local v0, "status":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 61
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getAllAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    .line 63
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getVerifiedStatus(Landroid/content/SharedPreferences;)I
    .locals 2
    .param p0, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 102
    const-string v0, "status"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static isAccountVerified(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/accounts/Account;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v5, 0x0

    .line 91
    invoke-static {p0, p1}, Lcom/google/android/music/tutorial/SignupStatus;->getVerifiedAccounts(Landroid/content/Context;Landroid/content/SharedPreferences;)[Landroid/accounts/Account;

    move-result-object v4

    .line 92
    .local v4, "verifiedAccounts":[Landroid/accounts/Account;
    if-nez v4, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v5

    .line 93
    :cond_1
    move-object v0, v4

    .local v0, "arr$":[Landroid/accounts/Account;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 94
    .local v3, "verifiedAccount":Landroid/accounts/Account;
    invoke-virtual {p2, v3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 95
    const/4 v5, 0x1

    goto :goto_0

    .line 93
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static isBackgroundDataEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 117
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v1

    .line 120
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static launchVerificationCheck(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 111
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 112
    return-void
.end method
