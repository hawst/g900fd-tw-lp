.class public Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSoundSearchPlaylistResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field public mNextPageToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "nextPageToken"
    .end annotation
.end field

.field public mSoundSearchPlaylistData:Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson$SoundSearchPlaylistData;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "data"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, "GetSoundSearchPlaylist"

    sput-object v0, Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 14
    return-void
.end method
