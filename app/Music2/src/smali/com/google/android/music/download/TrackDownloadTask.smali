.class public Lcom/google/android/music/download/TrackDownloadTask;
.super Lcom/google/android/music/download/BaseDownloadTask;
.source "TrackDownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/TrackDownloadTask$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/download/BaseDownloadTask",
        "<",
        "Lcom/google/android/music/download/TrackDownloadRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final LOGV:Z

.field private mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

.field private final mMplayHandler:Lcom/google/android/music/download/MplayHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/cloudclient/MusicHttpClient;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadRequest"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p3, "downloadProgressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p4, "musicPreferences"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p5, "httpClient"    # Lcom/google/android/music/cloudclient/MusicHttpClient;
    .param p6, "networkMonitorServiceConnection"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .prologue
    .line 46
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/download/BaseDownloadTask;-><init>(Landroid/content/Context;Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/IDownloadProgressListener;Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V

    .line 37
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/TrackDownloadTask;->LOGV:Z

    .line 47
    new-instance v0, Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadState()Lcom/google/android/music/download/DownloadState;

    move-result-object v5

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/download/MplayHandler;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/cloudclient/MusicHttpClient;Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)V

    iput-object v0, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    .line 49
    return-void
.end method

.method private getWriteToStream()Ljava/io/OutputStream;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadState()Lcom/google/android/music/download/DownloadState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/download/DownloadState;->resetCompletedBytes()V

    .line 237
    iget-boolean v4, p0, Lcom/google/android/music/download/TrackDownloadTask;->LOGV:Z

    if-eqz v4, :cond_0

    const-string v4, "SongDownloadTask"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Opening file for download:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_0
    iget-boolean v4, p0, Lcom/google/android/music/download/TrackDownloadTask;->LOGV:Z

    if-eqz v4, :cond_1

    .line 239
    const-string v5, "SongDownloadTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Opening file for download:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/music/log/Log;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 246
    .local v2, "out":Ljava/io/OutputStream;
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 247
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    .line 248
    .local v3, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/music/store/Store;->getCpData(JZ)[B

    move-result-object v0

    .line 249
    .local v0, "cpData":[B
    if-eqz v0, :cond_2

    .line 250
    new-instance v1, Lcom/google/android/music/download/cp/CpOutputStream;

    invoke-direct {v1, v2, v0}, Lcom/google/android/music/download/cp/CpOutputStream;-><init>(Ljava/io/OutputStream;[B)V

    .line 251
    .local v1, "cpOut":Lcom/google/android/music/download/cp/CpOutputStream;
    new-instance v2, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;

    .end local v2    # "out":Ljava/io/OutputStream;
    invoke-direct {v2, v1}, Lcom/google/android/music/io/ChunkedOutputStreamAdapter;-><init>(Lcom/google/android/music/io/ChunkedOutputStream;)V

    .line 252
    .restart local v2    # "out":Ljava/io/OutputStream;
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadState()Lcom/google/android/music/download/DownloadState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/download/DownloadState;->setCp()V

    .line 256
    .end local v0    # "cpData":[B
    .end local v1    # "cpOut":Lcom/google/android/music/download/cp/CpOutputStream;
    .end local v3    # "store":Lcom/google/android/music/store/Store;
    :cond_2
    return-object v2
.end method

.method private logDownloadSucceededEvent()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 287
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/download/DownloadUtils;->getActiveNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 288
    .local v0, "network":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v10

    .line 289
    .local v10, "networkType":I
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v11

    .line 290
    .local v11, "networkSubtype":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadState()Lcom/google/android/music/download/DownloadState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/download/DownloadState;->calculateLatency()J

    move-result-wide v8

    .line 292
    .local v8, "latency":J
    const-string v3, "SongDownloadTask"

    const-string v4, "downloadCompleted: id=%s, remoteId=%s, owner=%s, priority=%s, seek=%s"

    const/4 v1, 0x5

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getMusicEventLogger()Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v5}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v6}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadSucceeded(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJJII)V

    .line 309
    return-void

    .line 288
    .end local v8    # "latency":J
    .end local v10    # "networkType":I
    .end local v11    # "networkSubtype":I
    :cond_0
    const/16 v10, 0x2710

    goto/16 :goto_0

    .restart local v10    # "networkType":I
    :cond_1
    move v11, v2

    .line 289
    goto/16 :goto_1
.end method

.method private logHttpErrorEvent(I)V
    .locals 11
    .param p1, "httpError"    # I

    .prologue
    const/4 v2, 0x0

    .line 260
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/download/DownloadUtils;->getActiveNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 261
    .local v0, "network":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    .line 262
    .local v9, "networkType":I
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v10

    .line 264
    .local v10, "networkSubtype":I
    :goto_1
    const-string v3, "SongDownloadTask"

    const-string v4, "httpError: id=%s, remoteId=%s, owner=%s, priority=%s, seek=%s, error=%s, network=%s"

    const/4 v1, 0x7

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v1, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getMusicEventLogger()Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v5}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v6}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    move v8, p1

    invoke-virtual/range {v1 .. v10}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadHttpError(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJIII)V

    .line 284
    return-void

    .line 261
    .end local v9    # "networkType":I
    .end local v10    # "networkSubtype":I
    :cond_0
    const/16 v9, 0x2710

    goto/16 :goto_0

    .restart local v9    # "networkType":I
    :cond_1
    move v10, v2

    .line 262
    goto/16 :goto_1
.end method

.method private logIOExceptionEvent()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 312
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/download/DownloadUtils;->getActiveNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 313
    .local v0, "network":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v8

    .line 314
    .local v8, "networkType":I
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v9

    .line 316
    .local v9, "networkSubtype":I
    :goto_1
    const-string v3, "SongDownloadTask"

    const-string v4, "IOException: id=%s, remoteId=%s, owner=%s, priority=%s, seek=%s"

    const/4 v1, 0x5

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getMusicEventLogger()Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v5}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v6}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadIOException(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJII)V

    .line 332
    return-void

    .line 313
    .end local v8    # "networkType":I
    .end local v9    # "networkSubtype":I
    :cond_0
    const/16 v8, 0x2710

    goto/16 :goto_0

    .restart local v8    # "networkType":I
    :cond_1
    move v9, v2

    .line 314
    goto/16 :goto_1
.end method

.method private logServiceUnavailableEvent()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 335
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/download/DownloadUtils;->getActiveNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 336
    .local v0, "network":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v8

    .line 337
    .local v8, "networkType":I
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v9

    .line 339
    .local v9, "networkSubtype":I
    :goto_1
    const-string v3, "SongDownloadTask"

    const-string v4, "ServiceUnavailable: id=%s, remoteId=%s, owner=%s, priority=%s, seek=%s"

    const/4 v1, 0x5

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getMusicEventLogger()Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v5}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v6}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadServiceUnavailable(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJII)V

    .line 355
    return-void

    .line 336
    .end local v8    # "networkType":I
    .end local v9    # "networkSubtype":I
    :cond_0
    const/16 v8, 0x2710

    goto/16 :goto_0

    .restart local v8    # "networkType":I
    :cond_1
    move v9, v2

    .line 337
    goto/16 :goto_1
.end method


# virtual methods
.method protected canDownload()Z
    .locals 4

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/TrackOwner;

    .line 75
    .local v0, "requestOwner":Lcom/google/android/music/download/TrackOwner;
    sget-object v1, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$TrackOwner:[I

    invoke-virtual {v0}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 82
    const-string v1, "SongDownloadTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported owner type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 77
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->isStreamingEnabled()Z

    move-result v1

    goto :goto_0

    .line 80
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->isOfflineDownloadingEnabled()Z

    move-result v1

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v0}, Lcom/google/android/music/download/MplayHandler;->cancel()V

    .line 54
    invoke-super {p0}, Lcom/google/android/music/download/BaseDownloadTask;->cancel()V

    .line 55
    return-void
.end method

.method protected bridge synthetic createDownloadProgress(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;
    .locals 1
    .param p1, "x0"    # Lcom/google/android/music/download/DownloadRequest;
    .param p2, "x1"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/music/download/TrackDownloadRequest;

    .end local p1    # "x0":Lcom/google/android/music/download/DownloadRequest;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/TrackDownloadTask;->createDownloadProgress(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;

    move-result-object v0

    return-object v0
.end method

.method protected createDownloadProgress(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)Lcom/google/android/music/download/DownloadProgress;
    .locals 4
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "state"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/google/android/music/download/TrackDownloadProgress;-><init>(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;J)V

    return-object v0
.end method

.method protected download()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 105
    :try_start_0
    iget-boolean v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->LOGV:Z

    if-eqz v7, :cond_0

    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Downloading: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_0
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v7}, Lcom/google/android/music/download/MplayHandler;->prepareInputStream()V

    .line 110
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-nez v7, :cond_2

    .line 111
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v7}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v1

    .line 112
    .local v1, "localLocation":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Lcom/google/android/music/download/ServiceUnavailableException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Lcom/google/android/music/download/ServerRejectionException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Lcom/google/android/music/download/UnsupportedAudioTypeException; {:try_start_0 .. :try_end_0} :catch_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_13
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_1

    .line 114
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v7

    if-nez v7, :cond_1

    .line 115
    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not create file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lcom/google/android/music/download/ServiceUnavailableException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Lcom/google/android/music/download/ServerRejectionException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Lcom/google/android/music/download/UnsupportedAudioTypeException; {:try_start_1 .. :try_end_1} :catch_11
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    :cond_1
    :goto_0
    const/4 v3, 0x0

    .line 125
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getWriteToStream()Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/google/android/music/download/ServiceUnavailableException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Lcom/google/android/music/download/ServerRejectionException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Lcom/google/android/music/download/UnsupportedAudioTypeException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_13
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 132
    :try_start_3
    new-instance v8, Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadProgressListener()Lcom/google/android/music/download/IDownloadProgressListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadState()Lcom/google/android/music/download/DownloadState;

    move-result-object v10

    invoke-direct {v8, v9, v3, v7, v10}, Lcom/google/android/music/download/BufferProgressOutputStream;-><init>(Lcom/google/android/music/download/IDownloadProgressListener;Ljava/io/OutputStream;Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)V

    iput-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    .line 136
    .end local v1    # "localLocation":Ljava/io/File;
    .end local v3    # "out":Ljava/io/OutputStream;
    :cond_2
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v7, v8}, Lcom/google/android/music/download/MplayHandler;->downloadTo(Ljava/io/OutputStream;)V

    .line 138
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v7}, Lcom/google/android/music/download/MplayHandler;->downloadSucceeded()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 139
    invoke-direct {p0}, Lcom/google/android/music/download/TrackDownloadTask;->logDownloadSucceededEvent()V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v7}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v10

    invoke-static {v8, v10, v11}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->downloadAlbumArtIfMissing(Landroid/content/Context;J)V
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Lcom/google/android/music/download/ServiceUnavailableException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Lcom/google/android/music/download/ServerRejectionException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Lcom/google/android/music/download/UnsupportedAudioTypeException; {:try_start_3 .. :try_end_3} :catch_11
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_13
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 142
    const/4 v7, 0x4

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_4
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 230
    :cond_3
    :goto_1
    return v7

    .line 117
    .restart local v1    # "localLocation":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/io/IOException;
    :try_start_5
    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not create file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/InterruptedIOException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Lcom/google/android/music/download/ServiceUnavailableException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Lcom/google/android/music/download/ServerRejectionException; {:try_start_5 .. :try_end_5} :catch_c
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_5 .. :try_end_5} :catch_e
    .catch Lcom/google/android/music/download/UnsupportedAudioTypeException; {:try_start_5 .. :try_end_5} :catch_11
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_13
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 144
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "localLocation":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 149
    .local v0, "e":Ljava/net/SocketTimeoutException;
    :try_start_6
    iget-boolean v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->LOGV:Z

    if-eqz v7, :cond_4

    .line 150
    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Download: \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\" socket timed out"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 152
    :cond_4
    const/4 v7, 0x3

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_7
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    .line 222
    :catch_2
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 126
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "localLocation":Ljava/io/File;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_3
    move-exception v0

    .line 127
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_8
    const-string v7, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/io/InterruptedIOException; {:try_start_8 .. :try_end_8} :catch_9
    .catch Lcom/google/android/music/download/ServiceUnavailableException; {:try_start_8 .. :try_end_8} :catch_a
    .catch Lcom/google/android/music/download/ServerRejectionException; {:try_start_8 .. :try_end_8} :catch_c
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_8 .. :try_end_8} :catch_e
    .catch Lcom/google/android/music/download/UnsupportedAudioTypeException; {:try_start_8 .. :try_end_8} :catch_11
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_13
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 130
    const/4 v7, 0x5

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_9
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_1

    .line 222
    :catch_4
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 222
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "localLocation":Ljava/io/File;
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_5
    move-exception v0

    .line 223
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 217
    .end local v0    # "e":Ljava/io/IOException;
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v7}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_a
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v7, :cond_6

    .line 220
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v7}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 226
    :cond_6
    :goto_2
    iget-boolean v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->LOGV:Z

    if-eqz v7, :cond_7

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->shouldStopDownload()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 227
    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Stopping download ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") because manager said to stop"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_7
    const/4 v7, 0x1

    goto/16 :goto_1

    .line 222
    :catch_6
    move-exception v0

    .line 223
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v7, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 153
    .end local v0    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v0

    .line 154
    .local v0, "e":Lorg/apache/http/conn/ConnectTimeoutException;
    :try_start_b
    iget-boolean v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->LOGV:Z

    if-eqz v7, :cond_8

    .line 155
    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Download: \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\" connection timed out"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 157
    :cond_8
    const/4 v7, 0x3

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_c
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    goto/16 :goto_1

    .line 222
    :catch_8
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 158
    .end local v0    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v0

    .line 160
    .local v0, "e":Ljava/io/InterruptedIOException;
    :try_start_d
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    .line 161
    .local v2, "newEx":Ljava/lang/InterruptedException;
    invoke-virtual {v2, v0}, Ljava/lang/InterruptedException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 162
    throw v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 217
    .end local v0    # "e":Ljava/io/InterruptedIOException;
    .end local v2    # "newEx":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_e
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_9

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_15

    .line 224
    :cond_9
    :goto_3
    throw v7

    .line 163
    :catch_a
    move-exception v0

    .line 164
    .local v0, "e":Lcom/google/android/music/download/ServiceUnavailableException;
    :try_start_f
    invoke-direct {p0}, Lcom/google/android/music/download/TrackDownloadTask;->logServiceUnavailableEvent()V

    .line 165
    invoke-virtual {v0}, Lcom/google/android/music/download/ServiceUnavailableException;->getRetryAfterInSeconds()J

    move-result-wide v4

    .line 166
    .local v4, "serverSpecifiedRetryTime":J
    invoke-virtual {p0, v4, v5}, Lcom/google/android/music/download/TrackDownloadTask;->setServerSpecificRetryTime(J)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 167
    const/4 v7, 0x2

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_10
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    goto/16 :goto_1

    .line 222
    :catch_b
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 168
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "serverSpecifiedRetryTime":J
    :catch_c
    move-exception v0

    .line 169
    .local v0, "e":Lcom/google/android/music/download/ServerRejectionException;
    :try_start_11
    sget-object v7, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$ServerRejectionException$RejectionReason:[I

    invoke-virtual {v0}, Lcom/google/android/music/download/ServerRejectionException;->getRejectionReason()Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 183
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 185
    :goto_4
    const/4 v7, 0x5

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_12
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_d

    goto/16 :goto_1

    .line 222
    :catch_d
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 171
    .local v0, "e":Lcom/google/android/music/download/ServerRejectionException;
    :pswitch_0
    const/4 v7, 0x6

    :try_start_13
    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V

    goto :goto_4

    .line 174
    :pswitch_1
    const/4 v7, 0x5

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V

    goto :goto_4

    .line 177
    :pswitch_2
    const/4 v7, 0x7

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V

    goto :goto_4

    .line 180
    :pswitch_3
    const/16 v7, 0xd

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V

    goto :goto_4

    .line 187
    .end local v0    # "e":Lcom/google/android/music/download/ServerRejectionException;
    :catch_e
    move-exception v0

    .line 188
    .local v0, "e":Lorg/apache/http/client/HttpResponseException;
    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v6

    .line 189
    .local v6, "statusCode":I
    invoke-direct {p0, v6}, Lcom/google/android/music/download/TrackDownloadTask;->logHttpErrorEvent(I)V

    .line 191
    const/16 v7, 0x190

    if-lt v6, v7, :cond_c

    const/16 v7, 0x1f4

    if-ge v6, v7, :cond_c

    .line 195
    const/16 v7, 0x191

    if-ne v6, v7, :cond_a

    .line 196
    const/4 v7, 0x4

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 202
    :goto_5
    const/4 v7, 0x5

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_14
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_f

    goto/16 :goto_1

    .line 222
    :catch_f
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 197
    .local v0, "e":Lorg/apache/http/client/HttpResponseException;
    :cond_a
    const/16 v7, 0x194

    if-ne v6, v7, :cond_b

    .line 198
    const/16 v7, 0xc

    :try_start_15
    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V

    goto :goto_5

    .line 200
    :cond_b
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto :goto_5

    .line 217
    :cond_c
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v7}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_16
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v7, :cond_6

    .line 220
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v7}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_10

    goto/16 :goto_2

    .line 222
    :catch_10
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v7, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 206
    .end local v0    # "e":Ljava/io/IOException;
    .end local v6    # "statusCode":I
    :catch_11
    move-exception v0

    .line 207
    .local v0, "e":Lcom/google/android/music/download/UnsupportedAudioTypeException;
    :try_start_17
    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Download failed because of unsupported audio type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/music/download/UnsupportedAudioTypeException;->getAudioType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const/16 v7, 0x8

    invoke-virtual {p0, v7}, Lcom/google/android/music/download/TrackDownloadTask;->updateFailed(I)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 209
    const/4 v7, 0x5

    .line 217
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v8}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_18
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v8, :cond_3

    .line 220
    iget-object v8, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v8}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_12

    goto/16 :goto_1

    .line 222
    :catch_12
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 210
    .end local v0    # "e":Ljava/io/IOException;
    :catch_13
    move-exception v0

    .line 211
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_19
    invoke-direct {p0}, Lcom/google/android/music/download/TrackDownloadTask;->logIOExceptionEvent()V

    .line 215
    const-string v7, "SongDownloadTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Download failed because of IO Error: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 217
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mMplayHandler:Lcom/google/android/music/download/MplayHandler;

    invoke-virtual {v7}, Lcom/google/android/music/download/MplayHandler;->releaseConnection()V

    .line 219
    :try_start_1a
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    if-eqz v7, :cond_6

    .line 220
    iget-object v7, p0, Lcom/google/android/music/download/TrackDownloadTask;->mBufferOut:Lcom/google/android/music/download/BufferProgressOutputStream;

    invoke-virtual {v7}, Lcom/google/android/music/download/BufferProgressOutputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_14

    goto/16 :goto_2

    .line 222
    :catch_14
    move-exception v0

    .line 223
    const-string v7, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 222
    .end local v0    # "e":Ljava/io/IOException;
    :catch_15
    move-exception v0

    .line 223
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v8, "SongDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected networkChangedDuringDownload(J)Z
    .locals 7
    .param p1, "startTime"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/TrackOwner;

    .line 60
    .local v0, "requestOwner":Lcom/google/android/music/download/TrackOwner;
    sget-object v1, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$TrackOwner:[I

    invoke-virtual {v0}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 67
    const-string v1, "SongDownloadTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported owner type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v3

    .line 68
    :goto_0
    return v1

    .line 62
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getStreamingEnabledChangedTime()J

    move-result-wide v4

    cmp-long v1, v4, p1

    if-lez v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_0

    .line 65
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/download/TrackDownloadTask;->getOfflineDownloadingEnabledChangedTime()J

    move-result-wide v4

    cmp-long v1, v4, p1

    if-lez v1, :cond_1

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
