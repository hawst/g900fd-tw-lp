.class Lcom/google/android/music/download/cache/StorageMigrationWorker$3;
.super Ljava/lang/Object;
.source "StorageMigrationWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/download/cache/StorageMigrationWorker;->abortMigration()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$3;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$3;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # getter for: Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$100(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Ljava/util/LinkedList;

    move-result-object v1

    monitor-enter v1

    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$3;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # getter for: Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationManager:Landroid/app/NotificationManager;
    invoke-static {v0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$200(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Landroid/app/NotificationManager;

    move-result-object v0

    const/16 v2, 0x425

    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 170
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$3;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # invokes: Lcom/google/android/music/download/cache/StorageMigrationWorker;->shutdownBackgroundWorker()V
    invoke-static {v0}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$300(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$3;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/download/cache/StorageMigrationWorker;->mCallbacks:Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;
    invoke-static {v0, v2}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$402(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;)Lcom/google/android/music/download/cache/StorageMigrationWorker$Callbacks;

    .line 172
    monitor-exit v1

    .line 173
    return-void

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
