.class Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RouteVolume"
.end annotation


# instance fields
.field public final mRouteId:Ljava/lang/String;

.field public final mValue:D


# direct methods
.method public constructor <init>(Ljava/lang/String;D)V
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "value"    # D

    .prologue
    .line 5447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5448
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;->mRouteId:Ljava/lang/String;

    .line 5449
    iput-wide p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$RouteVolume;->mValue:D

    .line 5450
    return-void
.end method
