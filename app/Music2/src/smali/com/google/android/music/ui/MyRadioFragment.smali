.class public Lcom/google/android/music/ui/MyRadioFragment;
.super Lcom/google/android/music/ui/MediaListGridFragment;
.source "MyRadioFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/MyRadioFragment$1;,
        Lcom/google/android/music/ui/MyRadioFragment$OnCreateNewStationListener;,
        Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

.field private mCreateRadioStationContainer:Landroid/view/ViewGroup;

.field private mCreateRadioStationText:Landroid/widget/TextView;

.field private mPlayTutorialHeader:Landroid/view/ViewGroup;

.field private mShowKeepOnView:Z

.field private mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "radio_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "radio_description"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "radio_art"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "radio_seed_source_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "radio_seed_source_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "radio_source_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/MyRadioFragment;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    new-instance v0, Lcom/google/android/music/medialist/MyRadioList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/MyRadioList;-><init>()V

    sget-object v1, Lcom/google/android/music/ui/MyRadioFragment;->PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/ui/MediaListGridFragment;-><init>(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 52
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED_RADIO_MUTANT:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 60
    iput-boolean v2, p0, Lcom/google/android/music/ui/MyRadioFragment;->mShowKeepOnView:Z

    .line 64
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/MyRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MyRadioFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/MyRadioFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MyRadioFragment;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mShowKeepOnView:Z

    return v0
.end method

.method private updateCardHeader()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/music/ui/TutorialCardsFactory;->setupRadioTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MyRadioFragment;->setIsCardHeaderShowing(Z)V

    .line 173
    return-void

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateCreateRadioStationButton()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 140
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 147
    :cond_1
    :goto_1
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationText:Landroid/widget/TextView;

    const v1, 0x7f0b0258

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 150
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationText:Landroid/widget/TextView;

    const v1, 0x7f0b025a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;-><init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/MyRadioFragment$1;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    .line 76
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;-><init>(Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment;Lcom/google/android/music/ui/MyRadioFragment$1;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    .line 70
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_enable_keepon_radio"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mShowKeepOnView:Z

    .line 102
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListGridFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 177
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->onDestroyView()V

    .line 178
    iput-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    .line 179
    iput-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationText:Landroid/widget/TextView;

    .line 180
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->setShowFakeEvenIfEmpty(Z)V

    .line 89
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/MyRadioFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mAdapter:Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MyRadioFragment$MyRadioFragmentAdapter;->setShowFakeEvenIfEmpty(Z)V

    .line 83
    :cond_0
    return-void
.end method

.method protected onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 1
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 156
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 159
    invoke-direct {p0}, Lcom/google/android/music/ui/MyRadioFragment;->updateCardHeader()V

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->showStartRadioButtonsInActionBar()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 163
    invoke-direct {p0}, Lcom/google/android/music/ui/MyRadioFragment;->updateCreateRadioStationButton()V

    .line 165
    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 108
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 110
    .local v1, "listView":Landroid/widget/ListView;
    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 111
    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->showStartRadioButtonsInActionBar()Z

    move-result v3

    if-nez v3, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 117
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f040064

    invoke-virtual {v0, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 119
    .local v2, "root":Landroid/view/ViewGroup;
    invoke-virtual {v1, v2, v6, v5}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 120
    const v3, 0x7f0e011e

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationContainer:Landroid/view/ViewGroup;

    .line 122
    const v3, 0x7f0e0120

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationText:Landroid/widget/TextView;

    .line 123
    invoke-direct {p0}, Lcom/google/android/music/ui/MyRadioFragment;->updateCreateRadioStationButton()V

    .line 124
    iget-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationContainer:Landroid/view/ViewGroup;

    new-instance v4, Lcom/google/android/music/ui/MyRadioFragment$OnCreateNewStationListener;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/MyRadioFragment$OnCreateNewStationListener;-><init>(Lcom/google/android/music/ui/MyRadioFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/MyRadioFragment;->mCreateRadioStationContainer:Landroid/view/ViewGroup;

    invoke-static {v3, v4}, Lcom/google/android/music/utils/ViewUtils;->setWidthToShortestEdge(Landroid/content/Context;Landroid/view/ViewGroup;)I

    .line 131
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "root":Landroid/view/ViewGroup;
    :cond_0
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MyRadioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    .line 132
    iget-object v3, p0, Lcom/google/android/music/ui/MyRadioFragment;->mPlayTutorialHeader:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v6, v5}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 133
    invoke-direct {p0}, Lcom/google/android/music/ui/MyRadioFragment;->updateCardHeader()V

    .line 134
    return-void
.end method
