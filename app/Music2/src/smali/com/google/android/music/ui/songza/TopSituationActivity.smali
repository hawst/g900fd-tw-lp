.class public Lcom/google/android/music/ui/songza/TopSituationActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "TopSituationActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_1

    .line 22
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 23
    .local v1, "intent":Landroid/content/Intent;
    const/4 v4, 0x0

    .line 24
    .local v4, "situationId":Ljava/lang/String;
    const/4 v5, 0x0

    .line 25
    .local v5, "situationTitle":Ljava/lang/String;
    const/4 v3, 0x0

    .line 26
    .local v3, "situationDescription":Ljava/lang/String;
    const/4 v2, 0x0

    .line 27
    .local v2, "situationArtUrl":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 28
    const-string v6, "situationId"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 29
    const-string v6, "situationTitle"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 30
    const-string v6, "situationDescription"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 31
    const-string v6, "situationArtUrl"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33
    :cond_0
    invoke-static {v4, v5, v3, v2}, Lcom/google/android/music/ui/songza/TopSituationFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/songza/TopSituationFragment;

    move-result-object v0

    .line 35
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6}, Lcom/google/android/music/ui/songza/TopSituationActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 37
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "situationArtUrl":Ljava/lang/String;
    .end local v3    # "situationDescription":Ljava/lang/String;
    .end local v4    # "situationId":Ljava/lang/String;
    .end local v5    # "situationTitle":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected usesActionBarOverlay()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method
