.class public Lcom/google/android/music/cloudclient/CloudQueueContainerReferenceJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueueContainerReferenceJson.java"


# instance fields
.field public mAlbumReference:Lcom/google/android/music/cloudclient/CloudQueueAlbumReferenceJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumReference"
    .end annotation
.end field

.field public mArtistReference:Lcom/google/android/music/cloudclient/CloudQueueArtistReferenceJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistReference"
    .end annotation
.end field

.field public mGenreReference:Lcom/google/android/music/cloudclient/CloudQueueGenreReferenceJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "genreReference"
    .end annotation
.end field

.field public mPlaylistShareToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlistShareToken"
    .end annotation
.end field

.field public mRadioReference:Lcom/google/android/music/cloudclient/CloudQueueRadioReferenceJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "radioReference"
    .end annotation
.end field

.field public mTrackReference:Lcom/google/android/music/cloudclient/CloudQueueTrackReferenceJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackReference"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
