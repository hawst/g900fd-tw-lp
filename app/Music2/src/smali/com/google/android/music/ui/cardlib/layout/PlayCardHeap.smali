.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;
.super Ljava/lang/Object;
.source "PlayCardHeap.java"


# instance fields
.field private mHeap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardView;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;->mHeap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getCard(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .locals 5
    .param p1, "cardMetadata"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    const/4 v4, 0x0

    .line 70
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Utils;->ensureOnMainThread()V

    .line 72
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;->mHeap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 73
    .local v0, "available":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/layout/PlayCardView;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object v1, v2

    .line 80
    :cond_1
    :goto_0
    return-object v1

    .line 76
    :cond_2
    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 77
    .local v1, "card":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 78
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;->mHeap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
