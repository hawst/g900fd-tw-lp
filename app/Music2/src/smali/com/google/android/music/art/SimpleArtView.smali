.class public Lcom/google/android/music/art/SimpleArtView;
.super Lcom/google/android/play/imageview/PlayImageView;
.source "SimpleArtView.java"

# interfaces
.implements Lcom/google/android/music/art/ArtResolver2$RequestListener;


# static fields
.field private static final LOGV:Z


# instance fields
.field protected mAspectRatio:F

.field private mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

.field protected mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

.field private mFadeInAnimation:Landroid/view/animation/Animation;

.field protected mRequestStartTime:J

.field protected mRetainedCurrentRequest:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/SimpleArtView;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/music/art/SimpleArtView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/imageview/PlayImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/music/art/SimpleArtView;->mAspectRatio:F

    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/art/SimpleArtView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/imageview/PlayImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/music/art/SimpleArtView;->mAspectRatio:F

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/art/SimpleArtView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 56
    sget-object v1, Lcom/google/android/music/R$styleable;->SimpleArtView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/music/art/SimpleArtView;->mAspectRatio:F

    .line 60
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 61
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mFadeInAnimation:Landroid/view/animation/Animation;

    .line 62
    iget-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 64
    return-void
.end method

.method private makeArtRequest()V
    .locals 4

    .prologue
    .line 94
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/music/art/SimpleArtView;->reset(Z)V

    .line 95
    iget-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    if-nez v1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver2;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver2;

    move-result-object v0

    .line 97
    .local v0, "resolver":Lcom/google/android/music/art/ArtResolver2;
    iget-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/music/art/ArtResolver2;->getArt(Lcom/google/android/music/art/ArtDescriptor;Lcom/google/android/music/art/ArtResolver2$RequestListener;)Lcom/google/android/music/art/ArtRequest2;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    .line 98
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/art/SimpleArtView;->mRequestStartTime:J

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/music/art/ArtDescriptor;Z)V
    .locals 3
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;
    .param p2, "force"    # Z

    .prologue
    .line 73
    sget-boolean v0, Lcom/google/android/music/art/SimpleArtView;->LOGV:Z

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "SimpleArtView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bind descriptor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    if-nez p1, :cond_2

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/art/SimpleArtView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    :cond_1
    :goto_0
    return-void

    .line 79
    :cond_2
    if-nez p2, :cond_3

    iget-object v0, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-virtual {p1, v0}, Lcom/google/android/music/art/ArtDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/music/art/SimpleArtView;->loadImage(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public loadImage(Ljava/lang/Object;)V
    .locals 2
    .param p1, "imageToken"    # Ljava/lang/Object;

    .prologue
    .line 207
    instance-of v1, p1, Lcom/google/android/music/art/ArtDescriptor;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 208
    check-cast v0, Lcom/google/android/music/art/ArtDescriptor;

    .line 209
    .local v0, "descriptor":Lcom/google/android/music/art/ArtDescriptor;
    iget-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 210
    check-cast p1, Lcom/google/android/music/art/ArtDescriptor;

    .end local p1    # "imageToken":Ljava/lang/Object;
    iput-object p1, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    .line 211
    invoke-direct {p0}, Lcom/google/android/music/art/SimpleArtView;->makeArtRequest()V

    .line 216
    .end local v0    # "descriptor":Lcom/google/android/music/art/ArtDescriptor;
    :cond_0
    :goto_0
    return-void

    .line 214
    .restart local p1    # "imageToken":Ljava/lang/Object;
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/play/imageview/PlayImageView;->loadImage(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onArtRequestComplete(Lcom/google/android/music/art/ArtRequest2;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 103
    sget-boolean v1, Lcom/google/android/music/art/SimpleArtView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 104
    const-string v1, "SimpleArtView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onArtRequestComplete request="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    if-eq p1, v1, :cond_2

    .line 129
    :cond_1
    :goto_0
    return-void

    .line 107
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v0

    .line 108
    .local v0, "descriptor":Lcom/google/android/music/art/ArtDescriptor;
    iget-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 111
    sget-boolean v1, Lcom/google/android/music/art/SimpleArtView;->LOGV:Z

    if-eqz v1, :cond_1

    .line 112
    const-string v1, "SimpleArtView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Got a request for a descriptor we don\'t care about. mCurrentDescriptor="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", descriptor="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->didRenderSuccessfully()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 118
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->retain()I

    .line 119
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/art/SimpleArtView;->mRetainedCurrentRequest:Z

    .line 120
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getResultBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/art/SimpleArtView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->onArtRequestCompletedSuccessfully()V

    .line 122
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 123
    .local v2, "now":J
    iget-wide v4, p0, Lcom/google/android/music/art/SimpleArtView;->mRequestStartTime:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x32

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 124
    iget-object v1, p0, Lcom/google/android/music/art/SimpleArtView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v1}, Lcom/google/android/music/art/SimpleArtView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 127
    .end local v2    # "now":J
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->onArtRequestFailed()V

    goto :goto_0
.end method

.method protected onArtRequestCompletedSuccessfully()V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method protected onArtRequestFailed()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method protected onAspectRatioChanged()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, -0x80000000

    .line 149
    invoke-super {p0, p1, p2}, Lcom/google/android/play/imageview/PlayImageView;->onMeasure(II)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->getMeasuredWidth()I

    move-result v3

    .line 153
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->getMeasuredHeight()I

    move-result v1

    .line 154
    .local v1, "height":I
    if-nez v3, :cond_3

    .line 155
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 156
    .local v2, "mode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 160
    .local v0, "constraint":I
    int-to-float v4, v1

    iget v5, p0, Lcom/google/android/music/art/SimpleArtView;->mAspectRatio:F

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 161
    if-ne v2, v7, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    if-ne v2, v6, :cond_2

    if-le v3, v0, :cond_2

    .line 166
    move v3, v0

    .line 169
    :cond_2
    invoke-virtual {p0, v3, v1}, Lcom/google/android/music/art/SimpleArtView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 173
    .end local v0    # "constraint":I
    .end local v2    # "mode":I
    :cond_3
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 174
    .restart local v2    # "mode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 176
    .restart local v0    # "constraint":I
    iget v4, p0, Lcom/google/android/music/art/SimpleArtView;->mAspectRatio:F

    int-to-float v5, v3

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 177
    if-eq v2, v7, :cond_0

    .line 179
    if-ne v2, v6, :cond_4

    if-le v1, v0, :cond_4

    .line 180
    move v1, v0

    .line 182
    :cond_4
    invoke-virtual {p0, v3, v1}, Lcom/google/android/music/art/SimpleArtView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method protected onReset()V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 220
    invoke-super {p0}, Lcom/google/android/play/imageview/PlayImageView;->onStartTemporaryDetach()V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->clearLoadedImage()V

    .line 222
    return-void
.end method

.method protected reset(Z)V
    .locals 3
    .param p1, "clearDescriptor"    # Z

    .prologue
    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->didRenderSuccessfully()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->cancelRequest()V

    .line 244
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/art/SimpleArtView;->mRetainedCurrentRequest:Z

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->release()I

    .line 247
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/music/art/SimpleArtView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 248
    iput-object v2, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/art/SimpleArtView;->mRetainedCurrentRequest:Z

    .line 250
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/art/SimpleArtView;->mRequestStartTime:J

    .line 251
    iget-object v0, p0, Lcom/google/android/music/art/SimpleArtView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 252
    iget-object v0, p0, Lcom/google/android/music/art/SimpleArtView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 253
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 254
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->onReset()V

    .line 256
    :cond_2
    if-eqz p1, :cond_3

    .line 257
    iput-object v2, p0, Lcom/google/android/music/art/SimpleArtView;->mCurrentDescriptor:Lcom/google/android/music/art/ArtDescriptor;

    .line 259
    :cond_3
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 2
    .param p1, "aspectRatio"    # F

    .prologue
    .line 191
    iget v0, p0, Lcom/google/android/music/art/SimpleArtView;->mAspectRatio:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3c23d70a    # 0.01f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 192
    iput p1, p0, Lcom/google/android/music/art/SimpleArtView;->mAspectRatio:F

    .line 193
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->requestLayout()V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/music/art/SimpleArtView;->onAspectRatioChanged()V

    .line 196
    :cond_0
    return-void
.end method
