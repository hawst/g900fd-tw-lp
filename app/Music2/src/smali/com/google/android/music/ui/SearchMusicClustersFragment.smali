.class public Lcom/google/android/music/ui/SearchMusicClustersFragment;
.super Lcom/google/android/music/ui/SearchClustersFragment;
.source "SearchMusicClustersFragment.java"


# static fields
.field private static final LOGV:Z

.field private static final SEARCH_TYPE:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/SearchMusicClustersFragment;->LOGV:Z

    .line 30
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "bestmatch"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "genre"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "track"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "radio"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "playlist"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchClustersFragment;-><init>()V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/SearchClustersFragment;-><init>(Ljava/lang/String;)V

    .line 46
    return-void
.end method


# virtual methods
.method protected createCluster(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
    .locals 16
    .param p1, "clusterIndex"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 61
    const-string v4, ""

    .line 62
    .local v4, "title":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 63
    .local v3, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 64
    .local v6, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 65
    .local v7, "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getScreenColumns()I

    move-result v8

    .line 66
    .local v8, "nbColumns":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 68
    .local v9, "nbRows":I
    const-string v1, "bestmatch"

    sget-object v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 69
    const-string v1, "searchType"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 70
    .local v15, "itemTypeIdx":I
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 71
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 72
    .local v14, "itemType":I
    const/4 v1, 0x3

    if-eq v14, v1, :cond_0

    const/4 v1, 0x7

    if-ne v14, v1, :cond_2

    .line 74
    :cond_0
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 75
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 76
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 98
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 140
    .end local v14    # "itemType":I
    .end local v15    # "itemTypeIdx":I
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/google/android/music/ui/SearchMusicClustersFragment;->LOGV:Z

    if-eqz v1, :cond_1

    .line 141
    const-string v1, "SearchMusicClusters"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Empty doclist for clusterIndex: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 145
    .local v12, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    const/4 v1, 0x1

    invoke-virtual {v12, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsFromSearch(Z)V

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/SearchMusicClustersFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v12, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSearchString(Ljava/lang/String;)V

    goto :goto_2

    .line 77
    .end local v12    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v13    # "i$":Ljava/util/Iterator;
    .restart local v14    # "itemType":I
    .restart local v15    # "itemTypeIdx":I
    :cond_2
    const/4 v1, 0x2

    if-eq v14, v1, :cond_3

    const/4 v1, 0x1

    if-eq v14, v1, :cond_3

    const/4 v1, 0x6

    if-ne v14, v1, :cond_4

    .line 81
    :cond_3
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 82
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 83
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto :goto_0

    .line 84
    :cond_4
    const/4 v1, 0x5

    if-eq v14, v1, :cond_5

    const/16 v1, 0x8

    if-ne v14, v1, :cond_6

    .line 86
    :cond_5
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 87
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 88
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 89
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto :goto_0

    .line 90
    :cond_6
    const/16 v1, 0x9

    if-ne v14, v1, :cond_7

    .line 91
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 92
    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildGenreDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;

    move-result-object v6

    .line 93
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_0

    .line 95
    :cond_7
    const-string v1, "SearchMusicClusters"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unhandled itemType when populating BEST_MATCH cluster: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 100
    .end local v14    # "itemType":I
    :cond_8
    const-string v1, "SearchMusicClusters"

    const-string v2, "Failed to move the cursor"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 102
    .end local v15    # "itemTypeIdx":I
    :cond_9
    const-string v1, "artist"

    sget-object v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 103
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b009e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 105
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 106
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 107
    :cond_a
    const-string v1, "album"

    sget-object v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 108
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b009f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 110
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 111
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 112
    :cond_b
    const-string v1, "genre"

    sget-object v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 113
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 114
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 115
    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildGenreDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;

    move-result-object v6

    .line 116
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 117
    :cond_c
    const-string v1, "track"

    sget-object v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 118
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 119
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 120
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 122
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 123
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 124
    :cond_d
    const-string v1, "playlist"

    sget-object v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 125
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 126
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-static {v1, v0}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildPlaylistDocumentList(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 128
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 129
    :cond_e
    const-string v1, "radio"

    sget-object v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 130
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED_RADIO_MUTANT:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b027d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 132
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-static {v1, v0}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildRadioDocumentList(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 133
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 135
    :cond_f
    const-string v1, "SearchMusicClusters"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unhandled clusterIndex: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " defaulting to Track doclist."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 137
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    goto/16 :goto_1

    .line 150
    .restart local v13    # "i$":Ljava/util/Iterator;
    :cond_10
    new-instance v1, Lcom/google/android/music/ui/Cluster;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchMusicClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/ui/SearchMusicClustersFragment;->mSearchString:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/music/ui/Cluster;-><init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;Lcom/google/android/music/store/ContainerDescriptor;)V

    return-object v1
.end method

.method protected getClusterContentUri(I)Landroid/net/Uri;
    .locals 2
    .param p1, "clusterIndex"    # I

    .prologue
    .line 55
    sget-object v1, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 56
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/SearchMusicClustersFragment;->mSearchString:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/music/store/MusicContent$Search;->getSearchUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method protected getNumberOfClusters()I
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/music/ui/SearchMusicClustersFragment;->SEARCH_TYPE:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method
