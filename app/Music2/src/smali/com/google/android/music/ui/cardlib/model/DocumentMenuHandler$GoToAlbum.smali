.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GoToAlbum"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1350
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1351
    const v0, 0x7f0e002d

    const v1, 0x7f0b0248

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1352
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1356
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_2

    .line 1358
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1359
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v4}, Lcom/google/android/music/ui/TrackContainerActivity;->showNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 1369
    :goto_0
    return-void

    .line 1363
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v2

    const/4 v5, 0x1

    move-object v6, v4

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/ui/TrackContainerActivity;->showAlbum(Landroid/content/Context;JLcom/google/android/music/ui/cardlib/model/Document;ZLandroid/view/View;)V

    goto :goto_0

    .line 1367
    :cond_2
    const-string v0, "DocumentMenuHandler"

    const-string v1, "Unexpected doc type"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
