.class public Lcom/google/android/music/PlayingIndicator;
.super Landroid/widget/LinearLayout;
.source "PlayingIndicator.java"


# instance fields
.field private mAnimation1:Landroid/graphics/drawable/AnimationDrawable;

.field private mAnimation2:Landroid/graphics/drawable/AnimationDrawable;

.field private mAnimation3:Landroid/graphics/drawable/AnimationDrawable;

.field private mAttachedToWindow:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mImage1:Landroid/widget/ImageView;

.field private mImage2:Landroid/widget/ImageView;

.field private mImage3:Landroid/widget/ImageView;

.field private mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

.field private mReceiverRegistered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x2

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    iput v2, p0, Lcom/google/android/music/PlayingIndicator;->mCurrentState:I

    .line 45
    iput-boolean v2, p0, Lcom/google/android/music/PlayingIndicator;->mReceiverRegistered:Z

    .line 125
    new-instance v2, Lcom/google/android/music/PlayingIndicator$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/PlayingIndicator$1;-><init>(Lcom/google/android/music/PlayingIndicator;)V

    iput-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    .line 50
    iput-object p1, p0, Lcom/google/android/music/PlayingIndicator;->mContext:Landroid/content/Context;

    .line 53
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 55
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v1, v2

    .line 57
    .local v1, "px":I
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 59
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage1:Landroid/widget/ImageView;

    .line 60
    iget-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    iget-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/google/android/music/PlayingIndicator;->addView(Landroid/view/View;)V

    .line 63
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage2:Landroid/widget/ImageView;

    .line 64
    iget-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    iget-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/google/android/music/PlayingIndicator;->addView(Landroid/view/View;)V

    .line 67
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage3:Landroid/widget/ImageView;

    .line 68
    iget-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/google/android/music/PlayingIndicator;->addView(Landroid/view/View;)V

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation1:Landroid/graphics/drawable/AnimationDrawable;

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation2:Landroid/graphics/drawable/AnimationDrawable;

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation3:Landroid/graphics/drawable/AnimationDrawable;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/PlayingIndicator;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/PlayingIndicator;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/PlayingIndicator;->updatePlayState(ZZZ)V

    return-void
.end method

.method private registerReceiver()V
    .locals 3

    .prologue
    .line 99
    iget-boolean v1, p0, Lcom/google/android/music/PlayingIndicator;->mAttachedToWindow:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/PlayingIndicator;->mReceiverRegistered:Z

    if-nez v1, :cond_0

    .line 100
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 101
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 102
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 103
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 104
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 105
    iget-object v1, p0, Lcom/google/android/music/PlayingIndicator;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/PlayingIndicator;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 106
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/PlayingIndicator;->mReceiverRegistered:Z

    .line 108
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private setPlayState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 140
    iget v0, p0, Lcom/google/android/music/PlayingIndicator;->mCurrentState:I

    if-ne v0, p1, :cond_0

    .line 160
    :goto_0
    return-void

    .line 142
    :cond_0
    iput p1, p0, Lcom/google/android/music/PlayingIndicator;->mCurrentState:I

    .line 143
    packed-switch p1, :pswitch_data_0

    .line 157
    const-string v0, "PlayingIndicator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPlayState: unexpected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->unregisterReceiver()V

    .line 146
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->stopAnimation()V

    goto :goto_0

    .line 149
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->registerReceiver()V

    .line 150
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->stopAnimation()V

    goto :goto_0

    .line 153
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->registerReceiver()V

    .line 154
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->startAnimation()V

    goto :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startAnimation()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mImage1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation1:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mImage2:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation2:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mImage3:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation3:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation1:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 171
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation2:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 172
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation3:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 173
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation1:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 174
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation2:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 175
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation3:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 176
    return-void
.end method

.method private stopAnimation()V
    .locals 2

    .prologue
    const v1, 0x7f020186

    .line 179
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation1:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 180
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation2:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 181
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mAnimation3:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 183
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mImage1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mImage2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 185
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mImage3:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    return-void
.end method

.method private unregisterReceiver()V
    .locals 2

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/music/PlayingIndicator;->mReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/music/PlayingIndicator;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/PlayingIndicator;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/PlayingIndicator;->mReceiverRegistered:Z

    .line 115
    :cond_0
    return-void
.end method

.method private updatePlayState(ZZZ)V
    .locals 1
    .param p1, "isPlaying"    # Z
    .param p2, "isStreaming"    # Z
    .param p3, "isPreparing"    # Z

    .prologue
    const/4 v0, 0x1

    .line 203
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 204
    invoke-direct {p0, v0}, Lcom/google/android/music/PlayingIndicator;->setPlayState(I)V

    .line 210
    :goto_0
    return-void

    .line 205
    :cond_0
    if-eqz p1, :cond_1

    .line 206
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/music/PlayingIndicator;->setPlayState(I)V

    goto :goto_0

    .line 208
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/music/PlayingIndicator;->setPlayState(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 80
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 81
    iput-boolean v1, p0, Lcom/google/android/music/PlayingIndicator;->mAttachedToWindow:Z

    .line 82
    iget v0, p0, Lcom/google/android/music/PlayingIndicator;->mCurrentState:I

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->registerReceiver()V

    .line 90
    iget v0, p0, Lcom/google/android/music/PlayingIndicator;->mCurrentState:I

    if-ne v0, v1, :cond_1

    .line 91
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->stopAnimation()V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget v0, p0, Lcom/google/android/music/PlayingIndicator;->mCurrentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 93
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->startAnimation()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->unregisterReceiver()V

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/PlayingIndicator;->mAttachedToWindow:Z

    .line 121
    invoke-direct {p0}, Lcom/google/android/music/PlayingIndicator;->stopAnimation()V

    .line 122
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 123
    return-void
.end method

.method public setVisibility(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 192
    if-nez p1, :cond_2

    .line 193
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v0

    .line 194
    .local v0, "state":Lcom/google/android/music/playback/PlaybackState;
    if-nez v0, :cond_1

    .line 199
    .end local v0    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_0
    :goto_0
    return-void

    .line 195
    .restart local v0    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/music/playback/PlaybackState;->isStreaming()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/music/PlayingIndicator;->updatePlayState(ZZZ)V

    goto :goto_0

    .line 196
    .end local v0    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_2
    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 197
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/music/PlayingIndicator;->setPlayState(I)V

    goto :goto_0
.end method
