.class public Lcom/google/android/music/sync/google/model/SyncableRadioStation;
.super Lcom/google/api/client/json/GenericJson;
.source "SyncableRadioStation.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;


# static fields
.field public static final ARTWORK_TYPE_VALUE_RADIO:I = 0x2

.field public static final ARTWORK_TYPE_VALUE_SEED:I = 0x1

.field public static final ARTWORK_TYPE_VALUE_UNKNOWN:I


# instance fields
.field public mClientId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "clientId"
    .end annotation
.end field

.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mImageType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "imageType"
    .end annotation
.end field

.field public mImageUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "imageUrl"
    .end annotation
.end field

.field public mImageUrls:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "imageUrls"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/ImageRefJson;",
            ">;"
        }
    .end annotation
.end field

.field public mIsDeleted:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "deleted"
    .end annotation
.end field

.field public mLastModifiedTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lastModifiedTimestamp"
    .end annotation
.end field

.field private mLocalId:J

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field

.field private mRadioStation:Lcom/google/android/music/store/RadioStation;

.field public mRecentTimestampMicrosec:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "recentTimestamp"
    .end annotation
.end field

.field public mRemoteId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seed"
    .end annotation
.end field

.field public mTracks:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "tracks"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 25
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 40
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    .line 43
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRecentTimestampMicrosec:J

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    .line 70
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLocalId:J

    return-void
.end method

.method private static convertClientArtworkToServerArtworkType(I)I
    .locals 3
    .param p0, "clientArtworkType"    # I

    .prologue
    .line 327
    packed-switch p0, :pswitch_data_0

    .line 335
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a syncable artworkType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :pswitch_0
    const/4 v0, 0x1

    .line 333
    :goto_0
    return v0

    .line 331
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 333
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 327
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static convertServerArtworkToClientArtworkType(I)I
    .locals 3
    .param p0, "serverArtworkType"    # I

    .prologue
    .line 341
    packed-switch p0, :pswitch_data_0

    .line 349
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid server artwork type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 343
    :pswitch_0
    const/4 v0, 0x1

    .line 347
    :goto_0
    return v0

    .line 345
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 347
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static parse(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .locals 7
    .param p0, "radioStation"    # Lcom/google/android/music/store/RadioStation;

    .prologue
    const/4 v6, 0x0

    .line 251
    new-instance v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-direct {v3}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;-><init>()V

    .line 252
    .local v3, "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    iput-object p0, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    .line 255
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getSourceId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getClientId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mClientId:Ljava/lang/String;

    .line 259
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getSourceVersion()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getRecentTimestampMicrosec()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRecentTimestampMicrosec:J

    .line 265
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    .line 266
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getDescription()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mDescription:Ljava/lang/String;

    .line 267
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getSeedSourceId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getSeedSourceType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/music/sync/google/model/RadioSeed;->createRadioSeed(Ljava/lang/String;I)Lcom/google/android/music/sync/google/model/RadioSeed;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    .line 269
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getArtworkLocation()Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "artLocation":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 271
    .local v2, "strings":[Ljava/lang/String;
    if-nez v2, :cond_0

    .line 272
    iput-object v6, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrl:Ljava/lang/String;

    .line 273
    iput-object v6, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    .line 278
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getArtworkType()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->convertClientArtworkToServerArtworkType(I)I

    move-result v4

    iput v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageType:I

    .line 281
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getId()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLocalId:J

    .line 283
    return-object v3

    .line 260
    .end local v0    # "artLocation":Ljava/lang/String;
    .end local v2    # "strings":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 261
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    goto :goto_0

    .line 275
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .restart local v0    # "artLocation":Ljava/lang/String;
    .restart local v2    # "strings":[Ljava/lang/String;
    :cond_0
    invoke-virtual {v3, v2}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->setImageUrls([Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static parseForTombstone(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .locals 4
    .param p0, "radioStation"    # Lcom/google/android/music/store/RadioStation;

    .prologue
    .line 233
    new-instance v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-direct {v1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;-><init>()V

    .line 234
    .local v1, "syncable":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    iput-object p0, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    .line 237
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getSourceId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    .line 240
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getSourceVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    .line 245
    invoke-virtual {p0}, Lcom/google/android/music/store/RadioStation;->getId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLocalId:J

    .line 247
    return-object v1

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    goto :goto_0
.end method


# virtual methods
.method public formatAsRadioStation(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;
    .locals 6
    .param p1, "radioStation"    # Lcom/google/android/music/store/RadioStation;

    .prologue
    .line 287
    if-nez p1, :cond_0

    .line 288
    new-instance p1, Lcom/google/android/music/store/RadioStation;

    .end local p1    # "radioStation":Lcom/google/android/music/store/RadioStation;
    invoke-direct {p1}, Lcom/google/android/music/store/RadioStation;-><init>()V

    .line 290
    .restart local p1    # "radioStation":Lcom/google/android/music/store/RadioStation;
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setSourceId(Ljava/lang/String;)V

    .line 291
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mClientId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setClientId(Ljava/lang/String;)V

    .line 292
    iget-wide v4, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setSourceVersion(Ljava/lang/String;)V

    .line 293
    iget-wide v4, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRecentTimestampMicrosec:J

    invoke-virtual {p1, v4, v5}, Lcom/google/android/music/store/RadioStation;->setRecentTimestampMicrosec(J)V

    .line 294
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setName(Ljava/lang/String;)V

    .line 295
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setDescription(Ljava/lang/String;)V

    .line 296
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-virtual {v3}, Lcom/google/android/music/sync/google/model/RadioSeed;->getSourceIdAndType()Landroid/util/Pair;

    move-result-object v1

    .line 297
    .local v1, "seedSourceAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setSeedSourceId(Ljava/lang/String;)V

    .line 298
    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setSeedSourceType(I)V

    .line 301
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 302
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v2, v3, [Ljava/lang/String;

    .line 303
    .local v2, "urls":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 304
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 306
    :cond_1
    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setArtworkLocation(Ljava/lang/String;)V

    .line 310
    .end local v0    # "i":I
    .end local v2    # "urls":[Ljava/lang/String;
    :goto_1
    iget v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageType:I

    invoke-static {v3}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->convertServerArtworkToClientArtworkType(I)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setArtworkType(I)V

    .line 313
    return-object p1

    .line 308
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/google/android/music/store/RadioStation;->setArtworkLocation(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forRadioEditStations()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getCreationTimestamp()J
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Creation timestamp not suppoted"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getEncapsulatedRadioStation()Lcom/google/android/music/store/RadioStation;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    return-object v0
.end method

.method public getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 174
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forRadioGetStations()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getLastModifiedTimestamp()J
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    return-wide v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLocalId:J

    return-wide v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 169
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    return v0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdate()Z
    .locals 1

    .prologue
    .line 323
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serializeAsJson()[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 159
    :try_start_0
    invoke-static {p0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MusicSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to serialize a radio station entry as JSON: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 162
    new-instance v1, Lcom/google/android/music/store/InvalidDataException;

    const-string v2, "Unable to serialize radio station for upstream sync."

    invoke-direct {v1, v2, v0}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setCreationTimestamp(J)V
    .locals 2
    .param p1, "time"    # J

    .prologue
    .line 142
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Creation timestamp not suppoted"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setImageUrls([Ljava/lang/String;)V
    .locals 7
    .param p1, "urls"    # [Ljava/lang/String;

    .prologue
    .line 215
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 216
    .local v2, "imageRefs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ImageRefJson;>;"
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v0, v1

    .line 217
    .local v5, "url":Ljava/lang/String;
    new-instance v3, Lcom/google/android/music/cloudclient/ImageRefJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/ImageRefJson;-><init>()V

    .line 218
    .local v3, "imgref":Lcom/google/android/music/cloudclient/ImageRefJson;
    iput-object v5, v3, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    .line 219
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 221
    .end local v3    # "imgref":Lcom/google/android/music/cloudclient/ImageRefJson;
    .end local v5    # "url":Ljava/lang/String;
    :cond_0
    iput-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    .line 222
    return-void
.end method

.method public setIsDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 153
    iput-boolean p1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    .line 154
    return-void
.end method

.method public setLastModifiedTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    .line 132
    return-void
.end method

.method public setRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .local v0, "s":Ljava/lang/StringBuilder;
    const-string v1, "; localid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLocalId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; remoteId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; clientId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mClientId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mtime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; rtime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRecentTimestampMicrosec:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; description:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; seed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; imageUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; imageType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; isDeleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public validateForUpstreamDelete()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid radio station for upstream delete."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    return-void
.end method

.method public validateForUpstreamInsert()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-virtual {v0}, Lcom/google/android/music/sync/google/model/RadioSeed;->isValidForUpstreamInsert()Z

    move-result v0

    if-nez v0, :cond_1

    .line 190
    :cond_0
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid radio station for upstream insert."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_1
    return-void
.end method

.method public validateForUpstreamUpdate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    :cond_0
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid radio station for upstream update."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    return-void
.end method

.method public wipeAllFields()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 76
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mClientId:Ljava/lang/String;

    .line 78
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    .line 79
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRecentTimestampMicrosec:J

    .line 80
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mDescription:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    .line 83
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    .line 84
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrl:Ljava/lang/String;

    .line 85
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageType:I

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    .line 88
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLocalId:J

    .line 89
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    .line 90
    return-void
.end method
