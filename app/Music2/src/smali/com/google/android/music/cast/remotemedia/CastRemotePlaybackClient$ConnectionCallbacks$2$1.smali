.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2$1;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;->onStatusUpdated()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 781
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;

    iget-object v1, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v1, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 782
    const-string v1, "MusicCastRemote"

    const-string v2, "onStatusUpdated"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;

    iget-object v1, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v1, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getMediaStatus()Lcom/google/android/gms/cast/MediaStatus;

    move-result-object v0

    .line 786
    .local v0, "mediaStatus":Lcom/google/android/gms/cast/MediaStatus;
    if-eqz v0, :cond_1

    .line 787
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2$1;->this$2:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;

    iget-object v1, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->handleStatusUpdate(Lcom/google/android/gms/cast/MediaStatus;)V
    invoke-static {v1, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->access$1900(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;Lcom/google/android/gms/cast/MediaStatus;)V

    .line 789
    :cond_1
    return-void
.end method
