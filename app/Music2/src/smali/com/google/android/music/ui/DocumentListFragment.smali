.class public Lcom/google/android/music/ui/DocumentListFragment;
.super Lcom/google/android/music/ui/BaseListFragment;
.source "DocumentListFragment.java"


# instance fields
.field protected final mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mDocList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;-><init>()V

    .line 27
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 31
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DocumentListFragment;->setRetainInstance(Z)V

    .line 32
    return-void
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;ZLcom/google/android/music/store/ContainerDescriptor;)V
    .locals 2
    .param p2, "type"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p3, "playAll"    # Z
    .param p4, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "Z",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "doclist":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    iput-object p1, p0, Lcom/google/android/music/ui/DocumentListFragment;->mDocList:Ljava/util/List;

    .line 44
    iput-object p2, p0, Lcom/google/android/music/ui/DocumentListFragment;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 45
    if-eqz p3, :cond_1

    .line 46
    if-nez p4, :cond_0

    .line 47
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object p4

    .line 49
    :cond_0
    new-instance v0, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;

    iget-object v1, p0, Lcom/google/android/music/ui/DocumentListFragment;->mDocList:Ljava/util/List;

    invoke-direct {v0, v1, p4}, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;-><init>(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)V

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentListFragment;->mClickListener:Landroid/view/View$OnClickListener;

    .line 51
    :cond_1
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/music/ui/DocumentListFragment;->mDocList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/DocumentListFragment;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-nez v0, :cond_1

    .line 57
    :cond_0
    const-string v0, "DocumentList"

    const-string v1, "Arguments not initialized!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 64
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 62
    new-instance v0, Lcom/google/android/music/ui/DocumentListAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/DocumentListFragment;->mDocList:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/music/ui/DocumentListFragment;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    iget-object v4, p0, Lcom/google/android/music/ui/DocumentListFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/ui/DocumentListFragment;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/DocumentListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;ZLandroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DocumentListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
