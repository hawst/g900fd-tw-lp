.class public Lcom/google/android/music/cloudclient/UserDeviceInfoJson;
.super Lcom/google/api/client/json/GenericJson;
.source "UserDeviceInfoJson.java"


# static fields
.field public static final ANDROID_TYPE:Ljava/lang/String; = "ANDROID"

.field public static final DESKTOP_APP_TYPE:Ljava/lang/String; = "DESKTOP_APP"

.field public static final IOS_TYPE:Ljava/lang/String; = "IOS"


# instance fields
.field public mDeviceName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "friendlyName"
    .end annotation
.end field

.field public mId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mIsSmartPhone:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "smartPhone"
    .end annotation
.end field

.field public mLastAccessedTimeMs:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lastAccessedTimeMs"
    .end annotation
.end field

.field public mType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 22
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mLastAccessedTimeMs:J

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mIsSmartPhone:Z

    return-void
.end method
