.class Lcom/google/android/music/ui/BaseTrackListView$6;
.super Ljava/lang/Object;
.source "BaseTrackListView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/BaseTrackListView;->setSongList(Lcom/google/android/music/medialist/SongList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mShowTrackArtists:Z

.field final synthetic this$0:Lcom/google/android/music/ui/BaseTrackListView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseTrackListView;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/music/ui/BaseTrackListView$6;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView$6;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/BaseTrackListView;->access$100(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$6;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->hasDifferentTrackArtists(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView$6;->mShowTrackArtists:Z

    .line 244
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView$6;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseTrackListView$6;->mShowTrackArtists:Z

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/BaseTrackListView;->showTrackArtist(Z)V

    .line 249
    return-void
.end method
