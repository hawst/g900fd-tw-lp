.class Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2$1;
.super Ljava/lang/Object;
.source "LeanbackExploreGroupsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;->onChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2$1;->this$1:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 69
    # getter for: Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Selecting row "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2$1;->this$1:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;

    iget v2, v2, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;->val$rowToSelect:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2$1;->this$1:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;

    iget-object v2, v2, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;->val$topChartsGroupsAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2$1;->this$1:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;

    const v1, 0x7f0e014f

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2$1;->this$1:Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;

    iget v1, v1, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;->val$rowToSelect:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 73
    return-void
.end method
