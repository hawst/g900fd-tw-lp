.class Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;
.super Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
.source "QueuePlayingFromHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

.field final synthetic val$transparentColor:I


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;I)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iput p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;->val$transparentColor:I

    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public resize(II)Landroid/graphics/Shader;
    .locals 12
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v11, 0x2

    const/4 v7, 0x1

    const/high16 v6, -0x1000000

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 167
    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$100(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->mBackground:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$100(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 168
    .local v8, "backgroundWidth":I
    :goto_0
    sub-int v3, p1, v8

    int-to-float v3, v3

    int-to-float v4, p1

    div-float v9, v3, v4

    .line 169
    .local v9, "secondPoint":F
    div-int/lit8 v3, v8, 0x2

    sub-int v3, p1, v3

    int-to-float v3, v3

    int-to-float v4, p1

    div-float v10, v3, v4

    .line 170
    .local v10, "thirdPoint":F
    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$200()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    const-string v3, "QueueHeaderView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mGradient.shaderFactory: secondPoint="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", thirdPoint="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v3, p1

    const/4 v4, 0x4

    new-array v5, v4, [I

    aput v6, v5, v2

    aput v6, v5, v7

    iget v4, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;->val$transparentColor:I

    aput v4, v5, v11

    const/4 v4, 0x3

    iget v6, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$2;->val$transparentColor:I

    aput v6, v5, v4

    const/4 v4, 0x4

    new-array v6, v4, [F

    aput v1, v6, v2

    aput v9, v6, v7

    aput v10, v6, v11

    const/4 v2, 0x3

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v6, v2

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 178
    .local v0, "gradient":Landroid/graphics/LinearGradient;
    return-object v0

    .end local v0    # "gradient":Landroid/graphics/LinearGradient;
    .end local v8    # "backgroundWidth":I
    .end local v9    # "secondPoint":F
    .end local v10    # "thirdPoint":F
    :cond_1
    move v8, v2

    .line 167
    goto :goto_0
.end method
