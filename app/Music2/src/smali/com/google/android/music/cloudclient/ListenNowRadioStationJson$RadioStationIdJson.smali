.class public Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowRadioStationJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RadioStationIdJson"
.end annotation


# instance fields
.field public mId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mSeeds:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seeds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/RadioSeed;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mSeeds:Ljava/util/List;

    return-void
.end method
