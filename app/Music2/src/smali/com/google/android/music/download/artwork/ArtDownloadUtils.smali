.class public Lcom/google/android/music/download/artwork/ArtDownloadUtils;
.super Ljava/lang/Object;
.source "ArtDownloadUtils.java"


# static fields
.field private static final ART_PATH:Ljava/lang/String;

.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->LOGV:Z

    .line 46
    new-instance v0, Ljava/io/File;

    const-string v1, "artwork2"

    const-string v2, "folder"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->ART_PATH:Ljava/lang/String;

    return-void
.end method

.method public static clearOrphanedFiles(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/download/artwork/ArtDownloadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.ACTION_CLEAR_ORPHANED_ART"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    invoke-static {p0, v0}, Lcom/google/android/music/utils/SystemUtils;->startServiceOrFail(Landroid/content/Context;Landroid/content/Intent;)V

    .line 148
    return-void
.end method

.method public static downloadAlbumArtIfMissing(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    .line 131
    const/4 v0, 0x0

    .line 133
    .local v0, "fd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/android/music/store/Store;->getRemoteArtLocationForAlbum(J)Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "remoteLocation":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 135
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/music/store/MusicContent$UrlArt;->openFileDescriptor(Landroid/content/Context;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 140
    :cond_0
    invoke-static {v0}, Lcom/google/android/music/utils/IOUtils;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    .line 142
    .end local v1    # "remoteLocation":Ljava/lang/String;
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v2

    .line 140
    invoke-static {v0}, Lcom/google/android/music/utils/IOUtils;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/google/android/music/utils/IOUtils;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    throw v2
.end method

.method public static getArtCacheDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string v0, "artwork2"

    return-object v0
.end method

.method public static getArtSubFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "folder"

    return-object v0
.end method

.method public static startArtDownload(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "prefetch"    # Z

    .prologue
    .line 79
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->startArtDownloads(Landroid/content/Context;Ljava/util/Collection;Z)V

    .line 80
    return-void
.end method

.method public static startArtDownloads(Landroid/content/Context;Ljava/util/Collection;Z)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "prefetch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "remoteUrls":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 93
    .local v3, "validUrls":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 95
    .local v2, "url":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Lcom/google/android/music/download/cache/CacheUtils;->isRemoteLocationSideLoaded(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 97
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    .end local v2    # "url":Ljava/lang/String;
    :cond_1
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 115
    :goto_1
    return-void

    .line 105
    :cond_2
    sget-boolean v4, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->LOGV:Z

    if-eqz v4, :cond_3

    .line 106
    const-string v4, "ArtDownloadUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Attempting to download art, remoteUrls: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/download/artwork/ArtDownloadService;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 111
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "com.google.android.music.ACTION_REMOTE_ART_REQUESTED"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v4, "remoteUrls"

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 113
    const-string v4, "prefetch"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1
.end method
