.class public Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;
.super Ljava/lang/Object;
.source "StringGetterBitmapGetter.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapGetter;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mArtUri:Lcom/google/android/music/leanback/Item$StringGetter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/leanback/Item$StringGetter;)V
    .locals 0
    .param p1, "artUri"    # Lcom/google/android/music/leanback/Item$StringGetter;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;->mArtUri:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 28
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "artUri"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/google/android/music/leanback/Item$ConstantStringGetter;

    invoke-direct {v0, p1}, Lcom/google/android/music/leanback/Item$ConstantStringGetter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;->mArtUri:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 24
    return-void
.end method


# virtual methods
.method public getBitmap(Landroid/content/Context;IIZ)Landroid/util/Pair;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIZ)",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 33
    iget-object v5, p0, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;->mArtUri:Lcom/google/android/music/leanback/Item$StringGetter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;->mArtUri:Lcom/google/android/music/leanback/Item$StringGetter;

    invoke-interface {v5}, Lcom/google/android/music/leanback/Item$StringGetter;->getString()Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "stringUrl":Ljava/lang/String;
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 35
    sget-object v5, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;->TAG:Ljava/lang/String;

    const-string v6, "Failed to load bitmap - empty url!"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    new-instance v5, Landroid/util/Pair;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v5, v4, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    .line 49
    :goto_1
    return-object v4

    .end local v3    # "stringUrl":Ljava/lang/String;
    :cond_0
    move-object v3, v4

    .line 33
    goto :goto_0

    .line 39
    .restart local v3    # "stringUrl":Ljava/lang/String;
    :cond_1
    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "artUrls":[Ljava/lang/String;
    if-eqz v0, :cond_2

    array-length v5, v0

    if-nez v5, :cond_3

    .line 41
    :cond_2
    sget-object v5, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;->TAG:Ljava/lang/String;

    const-string v6, "Failed to load bitmap - no decoded urls!"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    new-instance v5, Landroid/util/Pair;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v5, v4, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    goto :goto_1

    .line 45
    :cond_3
    array-length v4, v0

    new-array v1, v4, [Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    .line 46
    .local v1, "bitmapGetters":[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    array-length v4, v1

    if-ge v2, v4, :cond_4

    .line 47
    new-instance v4, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;

    aget-object v5, v0, v2

    invoke-direct {v4, v5}, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;-><init>(Ljava/lang/String;)V

    aput-object v4, v1, v2

    .line 46
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 49
    :cond_4
    invoke-static {p1, v1, p2, p3, p4}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;[Lcom/google/android/music/leanback/bitmap/BitmapGetter;IIZ)Landroid/util/Pair;

    move-result-object v4

    goto :goto_1
.end method
