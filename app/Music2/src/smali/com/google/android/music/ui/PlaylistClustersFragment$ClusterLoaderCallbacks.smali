.class Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;
.super Ljava/lang/Object;
.source "PlaylistClustersFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/PlaylistClustersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClusterLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/PlaylistClustersFragment;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/PlaylistClustersFragment$1;

    .prologue
    .line 252
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;-><init>(Lcom/google/android/music/ui/PlaylistClustersFragment;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 256
    packed-switch p1, :pswitch_data_0

    .line 267
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid loader id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :pswitch_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->getRecentPlaylistUri(IZ)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/PlaylistClustersFragment;->CURSOR_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/music/ui/PlaylistClustersFragment;->CURSOR_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 273
    if-nez p2, :cond_0

    .line 298
    :goto_0
    return-void

    .line 279
    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 289
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid loader id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 281
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # setter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCursor:Landroid/database/Cursor;
    invoke-static {v2, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$102(Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 282
    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    iget-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # getter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$200(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    # invokes: Lcom/google/android/music/ui/PlaylistClustersFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    invoke-static {v2, v3, v0, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$300(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    .line 295
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # getter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$200(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # getter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$500(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 297
    .local v0, "hasCards":Z
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->setIsCardHeaderShowing(Z)V

    goto :goto_0

    .line 285
    .end local v0    # "hasCards":Z
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # setter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCursor:Landroid/database/Cursor;
    invoke-static {v2, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$402(Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 286
    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    iget-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # getter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v3}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$500(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    # invokes: Lcom/google/android/music/ui/PlaylistClustersFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    invoke-static {v2, v3, v1, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$300(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    goto :goto_1

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 252
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 302
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid loader id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # setter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCursor:Landroid/database/Cursor;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$102(Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 305
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # getter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$200(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 314
    :goto_0
    return-void

    .line 308
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # setter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCursor:Landroid/database/Cursor;
    invoke-static {v0, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$402(Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 309
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/PlaylistClustersFragment;

    # getter for: Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->access$500(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    goto :goto_0

    .line 302
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
