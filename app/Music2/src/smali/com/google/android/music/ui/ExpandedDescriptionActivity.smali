.class public Lcom/google/android/music/ui/ExpandedDescriptionActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "ExpandedDescriptionActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method public static final buildStartIntent(Landroid/content/Context;Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "metadata"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/ExpandedDescriptionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "containerMetadata"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 36
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExpandedDescriptionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "containerMetadata"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .line 25
    .local v2, "metadata":Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExpandedDescriptionActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_0

    .line 26
    new-instance v1, Lcom/google/android/music/ui/ExpandedDescriptionFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;-><init>()V

    .line 27
    .local v1, "fragment":Lcom/google/android/music/ui/ExpandedDescriptionFragment;
    invoke-static {v2}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->createArguments(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)Landroid/os/Bundle;

    move-result-object v0

    .line 28
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 29
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/ui/ExpandedDescriptionActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 31
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "fragment":Lcom/google/android/music/ui/ExpandedDescriptionFragment;
    :cond_0
    return-void
.end method
