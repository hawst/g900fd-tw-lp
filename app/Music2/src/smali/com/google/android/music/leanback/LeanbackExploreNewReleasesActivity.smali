.class public Lcom/google/android/music/leanback/LeanbackExploreNewReleasesActivity;
.super Lcom/google/android/music/leanback/LeanbackGridActivity;
.source "LeanbackExploreNewReleasesActivity.java"


# instance fields
.field private mGenreId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected bindCursor(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 9
    .param p1, "leanbackCursorObjectAdapter"    # Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x6

    const/4 v7, 0x5

    .line 36
    const/4 v6, 0x1

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "albumName":Ljava/lang/String;
    const/4 v6, 0x2

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "artistName":Ljava/lang/String;
    invoke-interface {p2, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v2, v5

    .line 40
    .local v2, "artUrl":Ljava/lang/String;
    :goto_0
    invoke-interface {p2, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v0, v5

    .line 43
    .local v0, "albumMetajamId":Ljava/lang/String;
    :goto_1
    invoke-static {p0, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 44
    .local v4, "intent":Landroid/content/Intent;
    new-instance v5, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v5}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v5, v1}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v5

    return-object v5

    .line 38
    .end local v0    # "albumMetajamId":Ljava/lang/String;
    .end local v2    # "artUrl":Ljava/lang/String;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 40
    .restart local v2    # "artUrl":Ljava/lang/String;
    :cond_1
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected createItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method protected getGridTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreNewReleasesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreNewReleasesActivity;->mGenreId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/music/medialist/ExploreNewReleasesAlbumList;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreNewReleasesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "genre_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackExploreNewReleasesActivity;->mGenreId:Ljava/lang/String;

    .line 62
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackGridActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method
