.class Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExternalAlbumMode"
.end annotation


# instance fields
.field public mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 325
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    .line 326
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 336
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->cropToSquare()Z

    move-result v5

    move-object v0, p1

    move v2, p2

    move v3, p3

    move-object v4, p5

    # invokes: Lcom/google/android/music/AsyncAlbumArtImageView;->getBitmapForRemoteUrlOrDefault(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$100(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 347
    instance-of v1, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    if-nez v1, :cond_0

    .line 348
    const/4 v1, 0x0

    .line 351
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 350
    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    .line 351
    .local v0, "o":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getExternalUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExternalAlbumMode<url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
