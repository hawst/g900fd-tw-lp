.class public Lcom/google/android/music/dial/DialTrustManager;
.super Ljava/lang/Object;
.source "DialTrustManager.java"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# instance fields
.field private final x509TrustManagers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljavax/net/ssl/X509TrustManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/DialTrustManager;->x509TrustManagers:Ljava/util/List;

    .line 34
    return-void
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 2
    .param p1, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p2, "authType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Checking of client certificates is not supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 3
    .param p1, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p2, "authType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v2, p0, Lcom/google/android/music/dial/DialTrustManager;->x509TrustManagers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/X509TrustManager;

    .line 89
    .local v1, "tm":Ljavax/net/ssl/X509TrustManager;
    :try_start_0
    invoke-interface {v1, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    return-void

    .line 95
    .end local v1    # "tm":Ljavax/net/ssl/X509TrustManager;
    :cond_0
    new-instance v2, Ljava/security/cert/CertificateException;

    invoke-direct {v2}, Ljava/security/cert/CertificateException;-><init>()V

    throw v2

    .line 91
    .restart local v1    # "tm":Ljavax/net/ssl/X509TrustManager;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 4

    .prologue
    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    iget-object v3, p0, Lcom/google/android/music/dial/DialTrustManager;->x509TrustManagers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljavax/net/ssl/X509TrustManager;

    .line 102
    .local v2, "tm":Ljavax/net/ssl/X509TrustManager;
    invoke-interface {v2}, Ljavax/net/ssl/X509TrustManager;->getAcceptedIssuers()[Ljava/security/cert/X509Certificate;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 104
    .end local v2    # "tm":Ljavax/net/ssl/X509TrustManager;
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/security/cert/X509Certificate;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/security/cert/X509Certificate;

    return-object v3
.end method

.method public varargs init([Ljava/security/KeyStore;)V
    .locals 12
    .param p1, "additionalKeyStores"    # [Ljava/security/KeyStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v2, "factories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljavax/net/ssl/TrustManagerFactory;>;"
    const-string v10, "PKIX"

    invoke-static {v10}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v7

    .line 54
    .local v7, "pkixFactory":Ljavax/net/ssl/TrustManagerFactory;
    const/4 v10, 0x0

    check-cast v10, Ljava/security/KeyStore;

    invoke-virtual {v7, v10}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 55
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    move-object v1, p1

    .local v1, "arr$":[Ljava/security/KeyStore;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v5, v1, v3

    .line 58
    .local v5, "keyStore":Ljava/security/KeyStore;
    const-string v10, "PKIX"

    invoke-static {v10}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    .line 60
    .local v0, "additionalFactory":Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v0, v5}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 61
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 64
    .end local v0    # "additionalFactory":Ljavax/net/ssl/TrustManagerFactory;
    .end local v5    # "keyStore":Ljava/security/KeyStore;
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .end local v1    # "arr$":[Ljava/security/KeyStore;
    .end local v3    # "i$":I
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljavax/net/ssl/TrustManagerFactory;

    .line 65
    .local v9, "tmf":Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v9}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v1

    .local v1, "arr$":[Ljavax/net/ssl/TrustManager;
    array-length v6, v1

    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_1

    aget-object v8, v1, v4

    .line 66
    .local v8, "tm":Ljavax/net/ssl/TrustManager;
    instance-of v10, v8, Ljavax/net/ssl/X509TrustManager;

    if-eqz v10, :cond_2

    .line 67
    iget-object v10, p0, Lcom/google/android/music/dial/DialTrustManager;->x509TrustManagers:Ljava/util/List;

    check-cast v8, Ljavax/net/ssl/X509TrustManager;

    .end local v8    # "tm":Ljavax/net/ssl/TrustManager;
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 72
    .end local v1    # "arr$":[Ljavax/net/ssl/TrustManager;
    .end local v4    # "i$":I
    .end local v9    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    :cond_3
    iget-object v10, p0, Lcom/google/android/music/dial/DialTrustManager;->x509TrustManagers:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_4

    .line 73
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "Couldn\'t find any X509TrustManagers"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 75
    :cond_4
    return-void
.end method
