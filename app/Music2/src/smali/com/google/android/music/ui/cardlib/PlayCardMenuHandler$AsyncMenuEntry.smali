.class public abstract Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
.source "PlayCardMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AsyncMenuEntry"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;II)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "id"    # I
    .param p3, "titleId"    # I

    .prologue
    .line 68
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;IIZ)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "id"    # I
    .param p3, "titleId"    # I
    .param p4, "isEnabled"    # Z

    .prologue
    .line 72
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;Z)V

    .line 73
    return-void
.end method


# virtual methods
.method public shouldRunAsync()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method
