.class Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;
.super Ljava/lang/Object;
.source "OtherWaysToPlayListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;

    .line 115
    .local v1, "tag":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;
    iget-object v3, v1, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntryHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 116
    .local v0, "iconId":I
    const v3, 0x7f0200dd

    if-ne v0, v3, :cond_1

    .line 117
    iget-object v3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b0091

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "url":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/music/ui/AppNavigation;->openHelpLink(Landroid/content/Context;Ljava/lang/String;)V

    .line 131
    .end local v2    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    const v3, 0x7f020154

    if-ne v0, v3, :cond_2

    .line 120
    iget-object v3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v4}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$100(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/purchase/Finsky;->isPlayStoreAvailable(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 121
    iget-object v3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v4}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$100(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/download/IntentConstants;->getMusicStoreIntent(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 123
    :cond_2
    const v3, 0x7f02016f

    if-ne v0, v3, :cond_3

    .line 124
    iget-object v3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b008b

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 127
    .restart local v2    # "url":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/music/ui/AppNavigation;->openHelpLink(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v2    # "url":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$1;->this$0:Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    # getter for: Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;->access$000(Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    sget-object v4, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->UNKNOWN:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {v3, v4}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)Z

    goto :goto_0
.end method
