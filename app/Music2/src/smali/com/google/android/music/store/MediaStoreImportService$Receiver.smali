.class public Lcom/google/android/music/store/MediaStoreImportService$Receiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaStoreImportService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MediaStoreImportService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Receiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 214
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "action":Ljava/lang/String;
    const/4 v3, 0x0

    .line 216
    .local v3, "importMediaStore":Z
    const-string v4, "android.provider.action.MTP_SESSION_END"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 217
    const/4 v3, 0x1

    .line 249
    :cond_0
    :goto_0
    if-eqz v3, :cond_2

    .line 251
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/store/MediaStoreImportService;

    invoke-direct {v2, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    .local v2, "importIntent":Landroid/content/Intent;
    # getter for: Lcom/google/android/music/store/MediaStoreImportService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/store/MediaStoreImportService;->access$500()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "MediaStoreImportService"

    const-string v5, "Scanner finished. Starting media store import"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_1
    const-string v4, "MediaStoreImportService.import"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 258
    .end local v2    # "importIntent":Landroid/content/Intent;
    :cond_2
    return-void

    .line 218
    :cond_3
    const-string v4, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 219
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "dataUri":Ljava/lang/String;
    if-eqz v1, :cond_4

    const-string v4, "file:///system/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 224
    :cond_4
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/store/MediaStoreImportService;

    invoke-direct {v2, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 225
    .restart local v2    # "importIntent":Landroid/content/Intent;
    const-string v4, "MediaStoreImportService.import_pending"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 227
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.music.IMPORT_PENDING"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 229
    .end local v1    # "dataUri":Ljava/lang/String;
    .end local v2    # "importIntent":Landroid/content/Intent;
    :cond_5
    const-string v4, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 230
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    .line 231
    .restart local v1    # "dataUri":Ljava/lang/String;
    if-eqz v1, :cond_6

    const-string v4, "file:///system/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 233
    const/4 v3, 0x0

    goto :goto_0

    .line 240
    :cond_6
    invoke-static {p1}, Lcom/google/android/music/store/MediaStoreImporter;->requestMediaStoreVersionCheck(Landroid/content/Context;)V

    .line 241
    const/4 v3, 0x1

    .line 242
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/store/MediaStoreImportService;

    invoke-direct {v2, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    .restart local v2    # "importIntent":Landroid/content/Intent;
    const-string v4, "MediaStoreImportService.import_pending"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 245
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.music.IMPORT_PENDING"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
