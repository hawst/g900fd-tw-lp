.class public Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "UpdateCloudQueueRequestJson.java"


# instance fields
.field public mContainers:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "containers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueContainerJson;",
            ">;"
        }
    .end annotation
.end field

.field public mItemIdsToDelete:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemIdsToDelete"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mItems:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueItemJson;",
            ">;"
        }
    .end annotation
.end field

.field public mSenderVersion:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "senderVersion"
    .end annotation
.end field

.field public mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "settings"
    .end annotation
.end field

.field public mShuffledItemOrderings:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "shuffledItemOrderings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueItemOrderingJson;",
            ">;"
        }
    .end annotation
.end field

.field public mTracks:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "tracks"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/TrackJson;",
            ">;"
        }
    .end annotation
.end field

.field public mUnshuffledItemOrderings:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "unshuffledItemOrderings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueItemOrderingJson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 41
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .local v4, "s":Ljava/lang/StringBuilder;
    const-string v6, "senderVersion: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSenderVersion:Ljava/lang/Long;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 43
    iget-object v6, p0, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/CloudQueueItemJson;

    .line 44
    .local v2, "item":Lcom/google/android/music/cloudclient/CloudQueueItemJson;
    const-string v6, "; item: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 46
    .end local v2    # "item":Lcom/google/android/music/cloudclient/CloudQueueItemJson;
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mItemIdsToDelete:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 47
    .local v3, "itemIdToDelete":Ljava/lang/String;
    const-string v6, "; itemIdToDelete: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 49
    .end local v3    # "itemIdToDelete":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mTracks:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/TrackJson;

    .line 50
    .local v5, "track":Lcom/google/android/music/cloudclient/TrackJson;
    const-string v6, "; track: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 52
    .end local v5    # "track":Lcom/google/android/music/cloudclient/TrackJson;
    :cond_2
    iget-object v6, p0, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mContainers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/CloudQueueContainerJson;

    .line 53
    .local v0, "container":Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
    const-string v6, "; container: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 55
    .end local v0    # "container":Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
    :cond_3
    const-string v6, "; settings: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
