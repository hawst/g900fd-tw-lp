.class public Lcom/google/android/music/art/SingleUrlArtDescriptor;
.super Lcom/google/android/music/art/ArtDescriptor;
.source "SingleUrlArtDescriptor.java"


# instance fields
.field public final artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ArtType;IFLcom/google/android/music/art/ArtResolver2$ArtUrl;)V
    .locals 6
    .param p1, "artType"    # Lcom/google/android/music/art/ArtType;
    .param p2, "sizeBucket"    # I
    .param p3, "aspectRatio"    # F
    .param p4, "artUrl"    # Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .prologue
    .line 17
    sget-object v2, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->EXACT:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/art/SingleUrlArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtDescriptor$SizeHandling;IFLcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtDescriptor$SizeHandling;IFLcom/google/android/music/art/ArtResolver2$ArtUrl;)V
    .locals 0
    .param p1, "artType"    # Lcom/google/android/music/art/ArtType;
    .param p2, "sizeHandling"    # Lcom/google/android/music/art/ArtDescriptor$SizeHandling;
    .param p3, "sizeBucket"    # I
    .param p4, "aspectRatio"    # F
    .param p5, "artUrl"    # Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/art/ArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtDescriptor$SizeHandling;IF)V

    .line 30
    invoke-static {p5}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    iput-object p5, p0, Lcom/google/android/music/art/SingleUrlArtDescriptor;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 39
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 40
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    .line 44
    .local v0, "that":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    iget-object v2, p0, Lcom/google/android/music/art/SingleUrlArtDescriptor;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    iget-object v3, v0, Lcom/google/android/music/art/SingleUrlArtDescriptor;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-virtual {v2, v3}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    invoke-super {p0, p1}, Lcom/google/android/music/art/ArtDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 50
    invoke-super {p0}, Lcom/google/android/music/art/ArtDescriptor;->hashCode()I

    move-result v0

    .line 51
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/art/SingleUrlArtDescriptor;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 52
    return v0
.end method
