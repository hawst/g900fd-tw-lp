.class public Lcom/google/android/music/AudioPreview;
.super Landroid/app/Activity;
.source "AudioPreview.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/AudioPreview$PreviewPlayer;,
        Lcom/google/android/music/AudioPreview$ProgressRefresher;
    }
.end annotation


# instance fields
.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

.field private mDuration:I

.field private mLoadingText:Landroid/widget/TextView;

.field private mPausedByTransientLossOfFocus:Z

.field private mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

.field private mProgressRefresher:Landroid/os/Handler;

.field private mRegisteredReceiver:Z

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSeeking:Z

.field private mTextLine1:Landroid/widget/TextView;

.field private mTextLine2:Landroid/widget/TextView;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    iput-boolean v0, p0, Lcom/google/android/music/AudioPreview;->mSeeking:Z

    .line 56
    iput-boolean v0, p0, Lcom/google/android/music/AudioPreview;->mRegisteredReceiver:Z

    .line 252
    new-instance v0, Lcom/google/android/music/AudioPreview$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/AudioPreview$2;-><init>(Lcom/google/android/music/AudioPreview;)V

    iput-object v0, p0, Lcom/google/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 331
    new-instance v0, Lcom/google/android/music/AudioPreview$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/AudioPreview$3;-><init>(Lcom/google/android/music/AudioPreview;)V

    iput-object v0, p0, Lcom/google/android/music/AudioPreview;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 402
    new-instance v0, Lcom/google/android/music/AudioPreview$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/AudioPreview$4;-><init>(Lcom/google/android/music/AudioPreview;)V

    iput-object v0, p0, Lcom/google/android/music/AudioPreview;->mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

    .line 418
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/AudioPreview;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/AudioPreview;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/AudioPreview;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/AudioPreview;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/AudioPreview;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/AudioPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/music/AudioPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/music/AudioPreview;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->start()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/AudioPreview;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->updatePlayPause()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/AudioPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/music/AudioPreview;->mSeeking:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/music/AudioPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/android/music/AudioPreview;->mSeeking:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/music/AudioPreview;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AudioPreview;

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/music/AudioPreview;->mDuration:I

    return v0
.end method

.method private showPostPrepareUI()V
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 229
    const v4, 0x7f0e00dd

    invoke-virtual {p0, v4}, Lcom/google/android/music/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 230
    .local v0, "pb":Landroid/widget/ProgressBar;
    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 231
    iget-object v4, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v4}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->getDuration()I

    move-result v4

    iput v4, p0, Lcom/google/android/music/AudioPreview;->mDuration:I

    .line 232
    iget v4, p0, Lcom/google/android/music/AudioPreview;->mDuration:I

    if-eqz v4, :cond_0

    .line 233
    iget-object v4, p0, Lcom/google/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget v5, p0, Lcom/google/android/music/AudioPreview;->mDuration:I

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 234
    iget-object v4, p0, Lcom/google/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 236
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v5, p0, Lcom/google/android/music/AudioPreview;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 237
    iget-object v4, p0, Lcom/google/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 238
    const v4, 0x7f0e00e0

    invoke-virtual {p0, v4}, Lcom/google/android/music/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 239
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 240
    iget-object v4, p0, Lcom/google/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v5, p0, Lcom/google/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v6, 0x3

    const/4 v7, 0x2

    invoke-virtual {v4, v5, v6, v7}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v4

    if-ne v1, v4, :cond_2

    .line 243
    .local v1, "recievedAudioFocus":Z
    :goto_0
    if-nez v1, :cond_1

    .line 244
    const-string v4, "AudioPreview"

    const-string v5, "showPostPrepareUI did not obtain audio focus"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    iput-boolean v3, p0, Lcom/google/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z

    .line 246
    iget-object v3, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v3}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->pause()V

    .line 248
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/music/AudioPreview$ProgressRefresher;

    invoke-direct {v4, p0}, Lcom/google/android/music/AudioPreview$ProgressRefresher;-><init>(Lcom/google/android/music/AudioPreview;)V

    const-wide/16 v6, 0xc8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 249
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->updatePlayPause()V

    .line 250
    return-void

    .end local v1    # "recievedAudioFocus":Z
    :cond_2
    move v1, v3

    .line 240
    goto :goto_0
.end method

.method private start()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 284
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 287
    .local v0, "recievedAudioFocus":Z
    :goto_0
    if-nez v0, :cond_1

    .line 288
    const-string v1, "AudioPreview"

    const-string v2, "start did not obtain audio focus"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :goto_1
    return-void

    .line 284
    .end local v0    # "recievedAudioFocus":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 291
    .restart local v0    # "recievedAudioFocus":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->start()V

    .line 292
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/music/AudioPreview$ProgressRefresher;

    invoke-direct {v2, p0}, Lcom/google/android/music/AudioPreview$ProgressRefresher;-><init>(Lcom/google/android/music/AudioPreview;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method private stopPlayback()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->release()V

    .line 208
    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .line 209
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 211
    :cond_1
    return-void
.end method

.method private updatePlayPause()V
    .locals 3

    .prologue
    .line 318
    const v1, 0x7f0e00e1

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 319
    .local v0, "b":Landroid/widget/ImageButton;
    if-eqz v0, :cond_0

    .line 320
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 321
    const v1, 0x7f020129

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 322
    const v1, 0x7f0b01e6

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    const v1, 0x7f02012c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 325
    const v1, 0x7f0b01e5

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/google/android/music/AudioPreview;->mDuration:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 354
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->updatePlayPause()V

    .line 355
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->getIntent()Landroid/content/Intent;

    move-result-object v10

    .line 63
    .local v10, "intent":Landroid/content/Intent;
    if-nez v10, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->finish()V

    .line 180
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-virtual {v10}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    .line 68
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    if-nez v1, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->finish()V

    goto :goto_0

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v14

    .line 74
    .local v14, "scheme":Ljava/lang/String;
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->setVolumeControlStream(I)V

    .line 75
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->requestWindowFeature(I)Z

    .line 76
    const v1, 0x7f04001d

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->setContentView(I)V

    .line 78
    const v1, 0x7f0e00d3

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    .line 79
    const v1, 0x7f0e00d4

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    .line 80
    const v1, 0x7f0e00de

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    .line 81
    const-string v1, "http"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 82
    const v1, 0x7f0b00ee

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/AudioPreview;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 83
    .local v11, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    .end local v11    # "msg":Ljava/lang/String;
    :goto_1
    const v1, 0x7f0e00df

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    .line 88
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    .line 89
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/google/android/music/AudioPreview;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    .line 91
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .line 92
    .local v13, "player":Lcom/google/android/music/AudioPreview$PreviewPlayer;
    if-nez v13, :cond_5

    .line 93
    new-instance v1, Lcom/google/android/music/AudioPreview$PreviewPlayer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/music/AudioPreview$PreviewPlayer;-><init>(Lcom/google/android/music/AudioPreview$1;)V

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .line 94
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1, p0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->setActivity(Lcom/google/android/music/AudioPreview;)V

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    iget-object v2, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->setDataSourceAndPrepare(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :cond_2
    :goto_2
    new-instance v0, Lcom/google/android/music/AudioPreview$1;

    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/AudioPreview$1;-><init>(Lcom/google/android/music/AudioPreview;Landroid/content/ContentResolver;)V

    .line 149
    .local v0, "mAsyncQueryHandler":Landroid/content/AsyncQueryHandler;
    const-string v1, "content"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 150
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    const-string v2, "media"

    if-ne v1, v2, :cond_6

    .line 152
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "artist"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_3
    :goto_3
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 177
    .local v8, "audioNoisyFilter":Landroid/content/IntentFilter;
    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v8, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v8}, Lcom/google/android/music/AudioPreview;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 179
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/AudioPreview;->mRegisteredReceiver:Z

    goto/16 :goto_0

    .line 85
    .end local v0    # "mAsyncQueryHandler":Landroid/content/AsyncQueryHandler;
    .end local v8    # "audioNoisyFilter":Landroid/content/IntentFilter;
    .end local v13    # "player":Lcom/google/android/music/AudioPreview$PreviewPlayer;
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 97
    .restart local v13    # "player":Lcom/google/android/music/AudioPreview$PreviewPlayer;
    :catch_0
    move-exception v9

    .line 102
    .local v9, "ex":Ljava/lang/Exception;
    const-string v1, "AudioPreview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to open file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const v1, 0x7f0b00e7

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->finish()V

    goto/16 :goto_0

    .line 108
    .end local v9    # "ex":Ljava/lang/Exception;
    :cond_5
    iput-object v13, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .line 109
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1, p0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->setActivity(Lcom/google/android/music/AudioPreview;)V

    .line 110
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->showPostPrepareUI()V

    goto/16 :goto_2

    .line 159
    .restart local v0    # "mAsyncQueryHandler":Landroid/content/AsyncQueryHandler;
    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 161
    :cond_7
    const-string v1, "file"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 164
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v12

    .line 165
    .local v12, "path":Ljava/lang/String;
    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "artist"

    aput-object v6, v4, v5

    const-string v5, "_data=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v12, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 171
    .end local v12    # "path":Ljava/lang/String;
    :cond_8
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 172
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->setNames()V

    goto/16 :goto_3
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->stopPlayback()V

    .line 196
    iget-boolean v0, p0, Lcom/google/android/music/AudioPreview;->mRegisteredReceiver:Z

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/AudioPreview;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 199
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 200
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 347
    const v0, 0x7f0b00e7

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 348
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->finish()V

    .line 349
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 368
    sparse-switch p1, :sswitch_data_0

    .line 399
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    .line 371
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->pause()V

    .line 376
    :goto_1
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->updatePlayPause()V

    goto :goto_0

    .line 374
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->start()V

    goto :goto_1

    .line 379
    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->start()V

    .line 380
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->updatePlayPause()V

    goto :goto_0

    .line 383
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 384
    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->pause()V

    .line 386
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->updatePlayPause()V

    goto :goto_0

    .line 395
    :sswitch_4
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->stopPlayback()V

    .line 396
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->finish()V

    goto :goto_0

    .line 368
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x4f -> :sswitch_1
        0x55 -> :sswitch_1
        0x56 -> :sswitch_4
        0x57 -> :sswitch_0
        0x58 -> :sswitch_0
        0x59 -> :sswitch_0
        0x5a -> :sswitch_0
        0x7e -> :sswitch_2
        0x7f -> :sswitch_3
    .end sparse-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    .end local p1    # "mp":Landroid/media/MediaPlayer;
    :goto_0
    return-void

    .line 222
    .restart local p1    # "mp":Landroid/media/MediaPlayer;
    :cond_0
    check-cast p1, Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .end local p1    # "mp":Landroid/media/MediaPlayer;
    iput-object p1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .line 223
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->setNames()V

    .line 224
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->start()V

    .line 225
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->showPostPrepareUI()V

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .line 189
    .local v0, "player":Lcom/google/android/music/AudioPreview$PreviewPlayer;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    .line 190
    return-object v0
.end method

.method public onUserLeaveHint()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->stopPlayback()V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/music/AudioPreview;->finish()V

    .line 217
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 218
    return-void
.end method

.method public playPauseClicked(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->pause()V

    .line 363
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->updatePlayPause()V

    .line 364
    return-void

    .line 361
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/AudioPreview;->start()V

    goto :goto_0
.end method

.method public setNames()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 304
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
