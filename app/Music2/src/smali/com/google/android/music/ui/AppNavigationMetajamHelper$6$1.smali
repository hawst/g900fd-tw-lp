.class Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;
.super Ljava/lang/Object;
.source "AppNavigationMetajamHelper.java"

# interfaces
.implements Lcom/google/android/music/utils/MusicUtils$QueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryComplete(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 567
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v1, v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v1, v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 596
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 599
    :goto_0
    return-void

    .line 570
    :cond_1
    if-eqz p1, :cond_4

    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 571
    # getter for: Lcom/google/android/music/ui/AppNavigationMetajamHelper;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->access$000()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 572
    const-string v1, "MusicNavigationHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found artist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v1, v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v2, v2, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$metajamId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/ArtistPageActivity;->showNautilusArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v1, v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->isAutoPlay()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 578
    new-instance v0, Lcom/google/android/music/medialist/NautilusArtistSongList;

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v1, v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$metajamId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    .local v0, "songList":Lcom/google/android/music/medialist/SongList;
    new-instance v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1$1;-><init>(Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596
    .end local v0    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_3
    :goto_1
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 598
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v1, v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_0

    .line 589
    :cond_4
    :try_start_2
    const-string v1, "MusicNavigationHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Artist not found for metajam id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v3, v3, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$metajamId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v1, v1, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6$1;->this$0:Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;

    iget-object v2, v2, Lcom/google/android/music/ui/AppNavigationMetajamHelper$6;->val$metajamId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/music/purchase/Finsky;->startBuyArtistActivity(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 596
    :catchall_0
    move-exception v1

    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1
.end method
