.class Lcom/google/android/music/ui/BaseActivity$8;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/BaseActivity;->initDownloadOnlyTextSwitcher()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 869
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$8;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public makeView()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 874
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$8;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 875
    .local v0, "myText":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$8;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 877
    const v1, 0x7f0b0090

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 878
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 880
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 881
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity$8;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v2}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f015b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 883
    return-object v0
.end method
