.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;
.super Landroid/widget/LinearLayout;
.source "PlayCardReason.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/layout/PlayCardReason$1;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mReason:Landroid/widget/TextView;

.field private mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mContext:Landroid/content/Context;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public static getReasonString(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    const v3, 0x7f0b024e

    .line 128
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 171
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "reasonString":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 130
    .end local v0    # "reasonString":Ljava/lang/String;
    :sswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b024f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 131
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 133
    .end local v0    # "reasonString":Ljava/lang/String;
    :sswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0250

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 136
    .end local v0    # "reasonString":Ljava/lang/String;
    :sswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0251

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 140
    .end local v0    # "reasonString":Ljava/lang/String;
    :sswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0252

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 144
    .end local v0    # "reasonString":Ljava/lang/String;
    :sswitch_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0254

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 145
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 147
    .end local v0    # "reasonString":Ljava/lang/String;
    :sswitch_5
    sget-object v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 163
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 165
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 149
    .end local v0    # "reasonString":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0255

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 151
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 153
    .end local v0    # "reasonString":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0253

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 156
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 158
    .end local v0    # "reasonString":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0256

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 161
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto :goto_0

    .line 167
    .end local v0    # "reasonString":Ljava/lang/String;
    :sswitch_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0257

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    .restart local v0    # "reasonString":Ljava/lang/String;
    goto/16 :goto_0

    .line 128
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_4
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x64 -> :sswitch_3
    .end sparse-switch

    .line 147
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public bind(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/CharSequence;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setVisibility(I)V

    .line 48
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    const v0, 0x7f0e01fb

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f0e01fa

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 42
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getHeight()I

    move-result v1

    .line 61
    .local v1, "height":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getWidth()I

    move-result v7

    .line 63
    .local v7, "width":I
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    .line 64
    .local v5, "reasonHeight":I
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v8}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMeasuredHeight()I

    move-result v2

    .line 65
    .local v2, "imageHeight":I
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v8}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMeasuredWidth()I

    move-result v3

    .line 67
    .local v3, "imageWidth":I
    if-le v2, v5, :cond_0

    .line 70
    sub-int v8, v1, v2

    div-int/lit8 v4, v8, 0x2

    .line 71
    .local v4, "imageY":I
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v9, 0x0

    add-int v10, v4, v2

    invoke-virtual {v8, v9, v4, v3, v10}, Lcom/google/android/music/AsyncAlbumArtImageView;->layout(IIII)V

    .line 72
    sub-int v8, v1, v5

    div-int/lit8 v6, v8, 0x2

    .line 73
    .local v6, "reasonY":I
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v9, v6, v5

    invoke-virtual {v8, v3, v6, v7, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 80
    .end local v4    # "imageY":I
    .end local v6    # "reasonY":I
    :goto_0
    return-void

    .line 76
    :cond_0
    sub-int v8, v1, v5

    div-int/lit8 v0, v8, 0x2

    .line 77
    .local v0, "contentY":I
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v9, 0x0

    add-int v10, v0, v5

    invoke-virtual {v8, v9, v0, v3, v10}, Lcom/google/android/music/AsyncAlbumArtImageView;->layout(IIII)V

    .line 78
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v9, v0, v5

    invoke-virtual {v8, v3, v0, v7, v9}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method public setReasonIcon(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    const/4 v3, 0x0

    .line 84
    sget-object v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 124
    :goto_0
    return-void

    .line 86
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 91
    const v0, 0x7f020140

    .line 123
    .local v0, "iconId":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    .line 88
    .end local v0    # "iconId":I
    :pswitch_1
    const v0, 0x7f020142

    .line 89
    .restart local v0    # "iconId":I
    goto :goto_1

    .line 96
    .end local v0    # "iconId":I
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 101
    const v0, 0x7f020144

    .line 102
    .restart local v0    # "iconId":I
    goto :goto_1

    .line 98
    .end local v0    # "iconId":I
    :pswitch_3
    const v0, 0x7f020145

    .line 99
    .restart local v0    # "iconId":I
    goto :goto_1

    .line 106
    .end local v0    # "iconId":I
    :pswitch_4
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v1

    packed-switch v1, :pswitch_data_3

    .line 115
    const v0, 0x7f020143

    .line 116
    .restart local v0    # "iconId":I
    goto :goto_1

    .line 108
    .end local v0    # "iconId":I
    :pswitch_5
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const v0, 0x7f020144

    .restart local v0    # "iconId":I
    goto :goto_1

    .line 111
    .end local v0    # "iconId":I
    :cond_0
    const v0, 0x7f020141

    .line 113
    .restart local v0    # "iconId":I
    goto :goto_1

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 86
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_1
    .end packed-switch

    .line 96
    :pswitch_data_2
    .packed-switch 0x8
        :pswitch_3
    .end packed-switch

    .line 106
    :pswitch_data_3
    .packed-switch 0x32
        :pswitch_5
    .end packed-switch
.end method

.method public setReasonMaxLines(I)V
    .locals 1
    .param p1, "reasonMaxLines"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 56
    return-void
.end method
