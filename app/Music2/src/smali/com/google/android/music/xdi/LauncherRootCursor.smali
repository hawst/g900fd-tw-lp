.class Lcom/google/android/music/xdi/LauncherRootCursor;
.super Landroid/database/MatrixCursor;
.source "LauncherRootCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v2, 0x1

    .line 18
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mContext:Landroid/content/Context;

    .line 20
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 22
    const/4 v1, 0x0

    .line 23
    .local v1, "clusterId":I
    add-int/lit8 v9, v1, 0x1

    .end local v1    # "clusterId":I
    .local v9, "clusterId":I
    const v3, 0x7f0b0096

    const/4 v4, 0x4

    move-object v0, p0

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/xdi/LauncherRootCursor;->addCluster(IIIIZ)V

    .line 29
    add-int/lit8 v1, v9, 0x1

    .end local v9    # "clusterId":I
    .restart local v1    # "clusterId":I
    const v6, 0x7f0b0097

    const/4 v8, 0x0

    move-object v3, p0

    move v4, v9

    move v5, v10

    move v7, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/music/xdi/LauncherRootCursor;->addCluster(IIIIZ)V

    .line 32
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->hasPlaylists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    add-int/lit8 v9, v1, 0x1

    .end local v1    # "clusterId":I
    .restart local v9    # "clusterId":I
    const v6, 0x7f0b0098

    move-object v3, p0

    move v4, v1

    move v5, v11

    move v7, v11

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/music/xdi/LauncherRootCursor;->addCluster(IIIIZ)V

    move v1, v9

    .line 38
    .end local v9    # "clusterId":I
    .restart local v1    # "clusterId":I
    :cond_0
    return-void
.end method

.method private addCluster(IIIIZ)V
    .locals 10
    .param p1, "clusterId"    # I
    .param p2, "rootHeaderId"    # I
    .param p3, "nameResId"    # I
    .param p4, "visibleCount"    # I
    .param p5, "useBrowseIntent"    # Z

    .prologue
    .line 46
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v3}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v3

    new-array v2, v3, [Ljava/lang/Object;

    .line 48
    .local v2, "values":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "name":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "browse"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntentByName(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 54
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "meta_uri"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 58
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "name"

    invoke-virtual {v3, v2, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 59
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "importance"

    rsub-int/lit8 v5, p1, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 60
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "display_name"

    invoke-virtual {v3, v2, v4, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "visible_count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "cache_time_ms"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "image_crop_allowed"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    if-eqz p5, :cond_0

    .line 66
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "browse_items_uri"

    sget-object v5, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "browse"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "intent_uri"

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "notification_text"

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p0, v2}, Lcom/google/android/music/xdi/LauncherRootCursor;->addRow([Ljava/lang/Object;)V

    .line 78
    return-void

    .line 71
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/xdi/LauncherRootCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v4, "browse_items_uri"

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
