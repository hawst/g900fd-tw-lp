.class public Lcom/google/android/music/dial/GenericDialEventFactory;
.super Ljava/lang/Object;
.source "GenericDialEventFactory.java"


# static fields
.field private static final UTF_8:Ljava/nio/charset/Charset;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/dial/GenericDialEventFactory;->UTF_8:Ljava/nio/charset/Charset;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/music/dial/model/GenericDialEvent;
    .locals 7
    .param p0, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/music/dial/MalformedDialEventException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 44
    :try_start_0
    const-class v3, Lcom/google/android/music/dial/model/GenericDialEvent;

    sget-object v5, Lcom/google/android/music/dial/GenericDialEventFactory;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonData(Ljava/lang/Class;[B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/dial/model/GenericDialEvent;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .local v1, "event":Lcom/google/android/music/dial/model/GenericDialEvent;
    invoke-virtual {v1}, Lcom/google/android/music/dial/model/GenericDialEvent;->validate()V

    .line 52
    const-string v3, "subscribe"

    iget-object v5, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v5, v5, Lcom/google/android/music/dial/model/EventHeaderJson;->mResponse:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 54
    const-class v2, Lcom/google/android/music/dial/model/SubscribeResponseJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/SubscribeResponseJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSubscribeResponseJson:Lcom/google/android/music/dial/model/SubscribeResponseJson;

    .line 92
    :cond_0
    :goto_0
    return-object v1

    .line 45
    .end local v1    # "event":Lcom/google/android/music/dial/model/GenericDialEvent;
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v3, Lcom/google/android/music/dial/MalformedDialEventException;

    const-string v5, "Error parsing event JSON string:\n%s\n"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v2

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2, v0}, Lcom/google/android/music/dial/MalformedDialEventException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 56
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "event":Lcom/google/android/music/dial/model/GenericDialEvent;
    :cond_1
    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v3, v3, Lcom/google/android/music/dial/model/EventHeaderJson;->mEvent:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 57
    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mHeader:Lcom/google/android/music/dial/model/EventHeaderJson;

    iget-object v3, v3, Lcom/google/android/music/dial/model/EventHeaderJson;->mEvent:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const/4 v3, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    move v2, v3

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 87
    const-string v2, "GenericDialEventFactory"

    const-string v3, "unrecognized event"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    new-instance v2, Lcom/google/android/music/dial/MalformedDialEventException;

    const-string v3, "unrecognized event"

    invoke-direct {v2, v3}, Lcom/google/android/music/dial/MalformedDialEventException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 57
    :sswitch_0
    const-string v4, "success"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    :sswitch_1
    const-string v2, "sessionerror"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v4

    goto :goto_1

    :sswitch_2
    const-string v2, "playbackerror"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string v2, "groupcoordinatorchanged"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_4
    const-string v2, "groupvolume"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_5
    const-string v2, "playbackstatus"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x5

    goto :goto_1

    :sswitch_6
    const-string v2, "sessionstatus"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x6

    goto :goto_1

    .line 59
    :pswitch_0
    const-class v2, Lcom/google/android/music/dial/model/SuccessResponseJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/SuccessResponseJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSuccessResponseJson:Lcom/google/android/music/dial/model/SuccessResponseJson;

    goto/16 :goto_0

    .line 63
    :pswitch_1
    const-class v2, Lcom/google/android/music/dial/model/SessionErrorEventJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/SessionErrorEventJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionErrorEventJson:Lcom/google/android/music/dial/model/SessionErrorEventJson;

    goto/16 :goto_0

    .line 67
    :pswitch_2
    const-class v2, Lcom/google/android/music/dial/model/PlaybackErrorEventJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/PlaybackErrorEventJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackErrorEventJson:Lcom/google/android/music/dial/model/PlaybackErrorEventJson;

    goto/16 :goto_0

    .line 71
    :pswitch_3
    const-class v2, Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mGroupCoordinatorChangedEventJson:Lcom/google/android/music/dial/model/GroupCoordinatorChangedEventJson;

    goto/16 :goto_0

    .line 75
    :pswitch_4
    const-class v2, Lcom/google/android/music/dial/model/GroupVolumeEventJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/GroupVolumeEventJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mGroupVolumeEventJson:Lcom/google/android/music/dial/model/GroupVolumeEventJson;

    goto/16 :goto_0

    .line 79
    :pswitch_5
    const-class v2, Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mPlaybackStatusEventJson:Lcom/google/android/music/dial/model/PlaybackStatusEventJson;

    goto/16 :goto_0

    .line 83
    :pswitch_6
    const-class v2, Lcom/google/android/music/dial/model/SessionStatusEventJson;

    iget-object v3, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mBody:Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v3}, Lcom/google/api/client/json/GenericJson;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/dial/model/SessionStatusEventJson;

    iput-object v2, v1, Lcom/google/android/music/dial/model/GenericDialEvent;->mSessionStatusEventJson:Lcom/google/android/music/dial/model/SessionStatusEventJson;

    goto/16 :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6f4abffd -> :sswitch_0
        -0x36746b33 -> :sswitch_2
        -0x289382ce -> :sswitch_1
        0xffe2643 -> :sswitch_3
        0x2e1b8a08 -> :sswitch_6
        0x68b2c259 -> :sswitch_4
        0x7fdf65cd -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
