.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;
.super Landroid/widget/FrameLayout;
.source "PlayCardThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$2;
    }
.end annotation


# instance fields
.field private final mContentPadding:I

.field private mContext:Landroid/content/Context;

.field private mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mThumbnailMaxHeight:I

.field private mThumbnailMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 47
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0f00ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mContentPadding:I

    .line 48
    iput v2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxWidth:I

    .line 49
    iput v2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxHeight:I

    .line 50
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;)Lcom/google/android/music/AsyncAlbumArtImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    return-object v0
.end method

.method private setRadioArt(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 159
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedType()I

    move-result v1

    .line 160
    .local v1, "seedSourceType":I
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedId()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "seedSourceId":Ljava/lang/String;
    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 165
    new-instance v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;-><init>(Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;)V

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 205
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalArtRadio(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 11
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    const/4 v10, 0x0

    .line 75
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v7

    .line 76
    .local v7, "mode":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    if-nez v7, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v4, "The art mode must be specified"

    invoke-direct {v0, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getIsEmulateRadio()Z

    move-result v6

    .line 82
    .local v6, "isEmulatedRadio":Z
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v8

    .line 83
    .local v8, "url":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v2

    .line 84
    .local v2, "id":J
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "title":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 87
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v8}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalArtRadio(Ljava/lang/String;)V

    .line 156
    :goto_0
    return-void

    .line 89
    :cond_1
    if-eqz v8, :cond_2

    .line 90
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$2;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {v7}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 105
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v8}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v8}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalArtRadio(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :pswitch_1
    const/4 v9, 0x0

    .line 96
    .local v9, "useFauxArt":Z
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView;->setArtistArt(Ljava/lang/String;JLandroid/net/Uri;Z)V

    goto :goto_0

    .line 99
    .end local v9    # "useFauxArt":Z
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v8}, Lcom/google/android/music/AsyncAlbumArtImageView;->setSharedPlaylistArt(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v8}, Lcom/google/android/music/AsyncAlbumArtImageView;->setSharedPlaylistArt(Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v0

    const/16 v4, 0x46

    if-eq v0, v4, :cond_3

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v0, v4, :cond_3

    .line 111
    const-string v0, "PlayCardThumbnail"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Nautilus item without art url: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    :cond_3
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$2;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {v7}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_1

    .line 152
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->showDefaultArtwork()V

    goto :goto_0

    .line 116
    :pswitch_4
    const/4 v9, 0x1

    .line 117
    .restart local v9    # "useFauxArt":Z
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistsUri(J)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView;->setArtistArt(Ljava/lang/String;JLandroid/net/Uri;Z)V

    goto :goto_0

    .line 121
    .end local v9    # "useFauxArt":Z
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Genres;->getAlbumsOfGenreUri(J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setGenreArt(Ljava/lang/String;JLandroid/net/Uri;)V

    goto/16 :goto_0

    .line 125
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v2, v3, v10, v10}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 128
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setPlaylistAlbumArt(JLjava/lang/String;I)V

    goto/16 :goto_0

    .line 131
    :pswitch_8
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->setRadioArt(Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto/16 :goto_0

    .line 134
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5, v10, v10}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 137
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v10}, Lcom/google/android/music/AsyncAlbumArtImageView;->setSharedPlaylistArt(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 140
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->canStartImFeelingLucky(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->showImFeelingLuckyArtwork(Z)V

    goto/16 :goto_0

    .line 145
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setArtForSonglist(Lcom/google/android/music/medialist/SongList;)V

    goto/16 :goto_0

    .line 148
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getVideoThumbnailUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setVideoThumbnailArt(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 114
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_4
        :pswitch_7
        :pswitch_a
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->clearArtwork()V

    .line 212
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 57
    const v0, 0x7f0e01f2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 58
    return-void
.end method

.method public setThumbnailMetrics(II)V
    .locals 0
    .param p1, "thumbnailMaxWidth"    # I
    .param p2, "thumbnailMaxHeight"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxWidth:I

    .line 62
    iput p2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxHeight:I

    .line 67
    return-void
.end method
