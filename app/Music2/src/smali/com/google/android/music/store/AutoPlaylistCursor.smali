.class public Lcom/google/android/music/store/AutoPlaylistCursor;
.super Lcom/google/android/music/utils/RequeriableCursorWrapper;
.source "AutoPlaylistCursor.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mAutoPlaylistIds:[J

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/AutoPlaylistCursor;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;[J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "columnNames"    # [Ljava/lang/String;
    .param p3, "autoPlaylistIds"    # [J

    .prologue
    .line 21
    invoke-static {p1, p2, p3}, Lcom/google/android/music/store/AutoPlaylistCursor;->buildCursor(Landroid/content/Context;[Ljava/lang/String;[J)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/RequeriableCursorWrapper;-><init>(Landroid/database/CrossProcessCursor;)V

    .line 22
    iput-object p1, p0, Lcom/google/android/music/store/AutoPlaylistCursor;->mContext:Landroid/content/Context;

    .line 23
    iput-object p3, p0, Lcom/google/android/music/store/AutoPlaylistCursor;->mAutoPlaylistIds:[J

    .line 24
    return-void
.end method

.method private static buildCursor(Landroid/content/Context;[Ljava/lang/String;[J)Landroid/database/MatrixCursor;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "columnNames"    # [Ljava/lang/String;
    .param p2, "ids"    # [J

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;

    invoke-direct {v0, p0, p2}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;-><init>(Landroid/content/Context;[J)V

    .line 33
    .local v0, "factory":Lcom/google/android/music/store/AutoPlaylistCursorFactory;
    invoke-virtual {v0, p1}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->buildCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getUpdatedCursor()Landroid/database/CrossProcessCursor;
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/store/AutoPlaylistCursor;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/music/store/AutoPlaylistCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/AutoPlaylistCursor;->mAutoPlaylistIds:[J

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/AutoPlaylistCursor;->buildCursor(Landroid/content/Context;[Ljava/lang/String;[J)Landroid/database/MatrixCursor;

    move-result-object v0

    return-object v0
.end method
