.class public final enum Lcom/google/android/music/art/ArtType;
.super Ljava/lang/Enum;
.source "ArtType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtType$1;,
        Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/art/ArtType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/art/ArtType;

.field public static final enum AVATAR:Lcom/google/android/music/art/ArtType;

.field public static final enum DEFAULT:Lcom/google/android/music/art/ArtType;

.field public static final enum GENERIC_URL:Lcom/google/android/music/art/ArtType;

.field public static final enum MAINSTAGE_CARD:Lcom/google/android/music/art/ArtType;

.field public static final enum PLAY_CARD:Lcom/google/android/music/art/ArtType;

.field public static final enum QUEUE_HEADER:Lcom/google/android/music/art/ArtType;

.field public static final enum SONGZA_SITUATION:Lcom/google/android/music/art/ArtType;

.field public static final enum USER_QUIZ:Lcom/google/android/music/art/ArtType;


# instance fields
.field private final mSizes:Lcom/google/android/music/art/ArtType$ArtBucketSizes;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 18
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "QUEUE_HEADER"

    # getter for: Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    invoke-static {}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->QUEUE_HEADER:Lcom/google/android/music/art/ArtType;

    .line 19
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "DEFAULT"

    # getter for: Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    invoke-static {}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->DEFAULT:Lcom/google/android/music/art/ArtType;

    .line 20
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "GENERIC_URL"

    # getter for: Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    invoke-static {}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->GENERIC_URL:Lcom/google/android/music/art/ArtType;

    .line 22
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "USER_QUIZ"

    # getter for: Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    invoke-static {}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->USER_QUIZ:Lcom/google/android/music/art/ArtType;

    .line 23
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "PLAY_CARD"

    # getter for: Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    invoke-static {}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->PLAY_CARD:Lcom/google/android/music/art/ArtType;

    .line 26
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "MAINSTAGE_CARD"

    const/4 v2, 0x5

    new-instance v3, Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    const/16 v4, 0x80

    const/16 v5, 0x300

    const/16 v6, 0x438

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;-><init>(IIILcom/google/android/music/art/ArtType$1;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->MAINSTAGE_CARD:Lcom/google/android/music/art/ArtType;

    .line 27
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "SONGZA_SITUATION"

    const/4 v2, 0x6

    # getter for: Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    invoke-static {}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->SONGZA_SITUATION:Lcom/google/android/music/art/ArtType;

    .line 28
    new-instance v0, Lcom/google/android/music/art/ArtType;

    const-string v1, "AVATAR"

    const/4 v2, 0x7

    # getter for: Lcom/google/android/music/art/ArtType$ArtBucketSizes;->DEFAULT_ART_BUCKETS:Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    invoke-static {}, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->access$000()Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/art/ArtType;-><init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V

    sput-object v0, Lcom/google/android/music/art/ArtType;->AVATAR:Lcom/google/android/music/art/ArtType;

    .line 17
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/music/art/ArtType;

    sget-object v1, Lcom/google/android/music/art/ArtType;->QUEUE_HEADER:Lcom/google/android/music/art/ArtType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/music/art/ArtType;->DEFAULT:Lcom/google/android/music/art/ArtType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/music/art/ArtType;->GENERIC_URL:Lcom/google/android/music/art/ArtType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/music/art/ArtType;->USER_QUIZ:Lcom/google/android/music/art/ArtType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/music/art/ArtType;->PLAY_CARD:Lcom/google/android/music/art/ArtType;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/art/ArtType;->MAINSTAGE_CARD:Lcom/google/android/music/art/ArtType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/art/ArtType;->SONGZA_SITUATION:Lcom/google/android/music/art/ArtType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/art/ArtType;->AVATAR:Lcom/google/android/music/art/ArtType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/art/ArtType;->$VALUES:[Lcom/google/android/music/art/ArtType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/music/art/ArtType$ArtBucketSizes;)V
    .locals 0
    .param p3, "sizes"    # Lcom/google/android/music/art/ArtType$ArtBucketSizes;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtType$ArtBucketSizes;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-object p3, p0, Lcom/google/android/music/art/ArtType;->mSizes:Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/art/ArtType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/google/android/music/art/ArtType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/art/ArtType;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/music/art/ArtType;->$VALUES:[Lcom/google/android/music/art/ArtType;

    invoke-virtual {v0}, [Lcom/google/android/music/art/ArtType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/art/ArtType;

    return-object v0
.end method


# virtual methods
.method public getBucketSize(I)I
    .locals 2
    .param p1, "bucket"    # I

    .prologue
    .line 38
    packed-switch p1, :pswitch_data_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown size bucket"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtType;->mSizes:Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    iget v0, v0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->smallWidth:I

    .line 44
    :goto_0
    return v0

    .line 42
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/art/ArtType;->mSizes:Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    iget v0, v0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->mediumWidth:I

    goto :goto_0

    .line 44
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/art/ArtType;->mSizes:Lcom/google/android/music/art/ArtType$ArtBucketSizes;

    iget v0, v0, Lcom/google/android/music/art/ArtType$ArtBucketSizes;->largeWidth:I

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
