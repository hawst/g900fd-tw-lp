.class public final enum Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
.super Ljava/lang/Enum;
.source "SearchClientResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

.field public static final enum ALBUM:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

.field public static final enum ARTIST:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

.field public static final enum GENRE:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

.field public static final enum PLAYLIST:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

.field public static final enum STATION:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

.field public static final enum TRACK:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    const-string v1, "TRACK"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->TRACK:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    .line 25
    new-instance v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    const-string v1, "ARTIST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->ARTIST:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    .line 26
    new-instance v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->ALBUM:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    .line 27
    new-instance v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    const-string v1, "PLAYLIST"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->PLAYLIST:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    .line 28
    new-instance v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    const-string v1, "GENRE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->GENRE:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    .line 29
    new-instance v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    const-string v1, "STATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->STATION:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    .line 23
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    sget-object v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->TRACK:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->ARTIST:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->ALBUM:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->PLAYLIST:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->GENRE:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->STATION:Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->$VALUES:[Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->$VALUES:[Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    invoke-virtual {v0}, [Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry$Type;

    return-object v0
.end method
