.class Lcom/google/android/music/xdi/SearchHeadersCursor;
.super Landroid/database/MatrixCursor;
.source "SearchHeadersCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mItemHeight:I

.field private final mItemWidth:I

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mContext:Landroid/content/Context;

    .line 27
    new-instance v1, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v1, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 28
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthDp(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mItemWidth:I

    .line 29
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightDp(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mItemHeight:I

    .line 31
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/google/android/music/xdi/XdiConstants;->SEARCH_HEADER_NAMES:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 32
    invoke-direct {p0, v0, p3}, Lcom/google/android/music/xdi/SearchHeadersCursor;->getColumnValuesForBrowseHeader(ILjava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/SearchHeadersCursor;->addRow([Ljava/lang/Object;)V

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_0
    return-void
.end method

.method private getColumnValuesForBrowseHeader(ILjava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1, "row"    # I
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 37
    iget-object v4, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v4}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 39
    .local v3, "values":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/XdiConstants;->SEARCH_HEADER_NAMES:[I

    aget v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "name":Ljava/lang/String;
    sget-object v4, Lcom/google/android/music/xdi/MusicXdiContract;->SEARCH_HEADER_INDEX:[I

    aget v0, v4, p1

    .line 42
    .local v0, "headerId":I
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "search/headers/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "q"

    invoke-virtual {v4, v5, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 47
    .local v2, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "_id"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    iget-object v4, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "display_name"

    invoke-virtual {v4, v3, v5, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    iget-object v4, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "results_uri"

    invoke-virtual {v4, v3, v5, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 50
    iget-object v4, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "default_item_width"

    iget v6, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mItemWidth:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    iget-object v4, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "default_item_height"

    iget v6, p0, Lcom/google/android/music/xdi/SearchHeadersCursor;->mItemHeight:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 53
    return-object v3
.end method
