.class Lcom/google/android/music/xdi/RootItemsCursor$InstantMixesCursor;
.super Landroid/database/MatrixCursor;
.source "RootItemsCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/xdi/RootItemsCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InstantMixesCursor"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 230
    sget-object v5, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 232
    new-instance v3, Lcom/google/android/music/xdi/ProjectionMap;

    sget-object v5, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v5}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    .line 235
    .local v3, "projectionMap":Lcom/google/android/music/xdi/ProjectionMap;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v5, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_INSTANT_MIXES_HEADER_NAMES:[I

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 236
    sget-object v5, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    array-length v5, v5

    new-array v4, v5, [Ljava/lang/Object;

    .line 238
    .local v4, "values":[Ljava/lang/Object;
    sget-object v5, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_INSTANT_MIXES_HEADER_NAMES:[I

    aget v5, v5, v0

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 239
    .local v2, "name":Ljava/lang/String;
    sget-object v5, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "instantmixes"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntentByName(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 244
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "meta_uri"

    const-wide/16 v6, 0x9

    invoke-static {v6, v7}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    const-string v5, "_id"

    add-int/lit8 v6, v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 248
    const-string v5, "display_name"

    invoke-virtual {v3, v4, v5, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 249
    const-string v5, "image_uri"

    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 251
    const-string v5, "intent_uri"

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 254
    invoke-virtual {p0, v4}, Lcom/google/android/music/xdi/RootItemsCursor$InstantMixesCursor;->addRow([Ljava/lang/Object;)V

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "values":[Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/music/xdi/RootItemsCursor$1;

    .prologue
    .line 228
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RootItemsCursor$InstantMixesCursor;-><init>(Landroid/content/Context;)V

    return-void
.end method
