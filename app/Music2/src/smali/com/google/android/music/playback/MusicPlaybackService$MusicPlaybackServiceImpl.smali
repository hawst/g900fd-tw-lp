.class public Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;
.super Lcom/google/android/music/playback/IMusicPlaybackService$Stub;
.source "MusicPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicPlaybackServiceImpl"
.end annotation


# instance fields
.field mService:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/google/android/music/playback/MusicPlaybackService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 1978
    invoke-direct {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;-><init>()V

    .line 1979
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    .line 1980
    return-void
.end method


# virtual methods
.method public addDownloadProgressListener(Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/download/IDownloadProgressListener;)Z
    .locals 1
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "listener"    # Lcom/google/android/music/download/IDownloadProgressListener;

    .prologue
    .line 2153
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$2100(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/playback/LocalDevicePlayback;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->addDownloadProgressListener(Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/download/IDownloadProgressListener;)Z

    move-result v0

    return v0
.end method

.method public cancelMix()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2219
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->cancelMix()V

    .line 2220
    return-void
.end method

.method public clearQueue()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2184
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->clearQueue()V

    .line 2185
    return-void
.end method

.method public duration()J
    .locals 2

    .prologue
    .line 2084
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAlbumArtUrl(J)Ljava/lang/String;
    .locals 1
    .param p1, "albumid"    # J

    .prologue
    .line 2176
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/DevicePlayback;->getAlbumArtUrl(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 2051
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2048
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtistId()J
    .locals 2

    .prologue
    .line 2057
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getArtistId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2054
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAudioId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 1

    .prologue
    .line 2069
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 2166
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2194
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;

    move-result-object v0

    return-object v0
.end method

.method public getErrorType()I
    .locals 1

    .prologue
    .line 2114
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getErrorType()I

    move-result v0

    return v0
.end method

.method public getLastUserInteractionTime()J
    .locals 2

    .prologue
    .line 2180
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getLastUserInteractionTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMediaList()Lcom/google/android/music/medialist/SongList;
    .locals 1

    .prologue
    .line 2066
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    return-object v0
.end method

.method public getMixState()Lcom/google/android/music/mix/MixGenerationState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2214
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getMixState()Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v0

    return-object v0
.end method

.method public getPlaybackState()Lcom/google/android/music/playback/PlaybackState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2199
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewPlayType()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2087
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getPreviewPlayType()I

    move-result v0

    return v0
.end method

.method public getQueuePosition()I
    .locals 1

    .prologue
    .line 2009
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getQueuePosition()I

    move-result v0

    return v0
.end method

.method public getQueueSize()I
    .locals 1

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getQueueSize()I

    move-result v0

    return v0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 2075
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getRating()I

    move-result v0

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    .prologue
    .line 2102
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getRepeatMode()I

    move-result v0

    return v0
.end method

.method public getSelectedMediaRouteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getSelectedMediaRouteId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShuffleMode()I
    .locals 1

    .prologue
    .line 2096
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getShuffleMode()I

    move-result v0

    return v0
.end method

.method public getSongStoreId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2063
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getSongStoreId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSortableAlbumArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2060
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getSortableAlbumArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrackName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2045
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasLocal()Z
    .locals 1

    .prologue
    .line 2129
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->hasLocal()Z
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1900(Lcom/google/android/music/playback/MusicPlaybackService;)Z

    move-result v0

    return v0
.end method

.method public hasRemote()Z
    .locals 1

    .prologue
    .line 2133
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->hasRemote()Z
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$2000(Lcom/google/android/music/playback/MusicPlaybackService;)Z

    move-result v0

    return v0
.end method

.method public hasValidPlaylist()Z
    .locals 1

    .prologue
    .line 2121
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->hasValidPlaylist()Z

    move-result v0

    return v0
.end method

.method public isAlbumArtInService()Z
    .locals 1

    .prologue
    .line 2172
    const/4 v0, 0x0

    return v0
.end method

.method public isCurrentSongLoaded()Z
    .locals 1

    .prologue
    .line 2042
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->isCurrentSongLoaded()Z

    move-result v0

    return v0
.end method

.method public isInCloudQueueMode()Z
    .locals 1

    .prologue
    .line 2141
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->isInCloudQueueMode()Z

    move-result v0

    return v0
.end method

.method public isInErrorState()Z
    .locals 1

    .prologue
    .line 2111
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isInErrorState()Z

    move-result v0

    return v0
.end method

.method public isInFatalErrorState()Z
    .locals 1

    .prologue
    .line 2117
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isInFatalErrorState()Z

    move-result v0

    return v0
.end method

.method public isInIniniteMixMode()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2204
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->isInfiniteMixMode()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 2024
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isPlaylistLoading()Z
    .locals 1

    .prologue
    .line 2125
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->playlistLoading()Z

    move-result v0

    return v0
.end method

.method public isPreparing()Z
    .locals 1

    .prologue
    .line 2105
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isPreparing()Z

    move-result v0

    return v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 2108
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isStreaming()Z

    move-result v0

    return v0
.end method

.method public isStreamingFullyBuffered()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2189
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isStreamingFullyBuffered()Z

    move-result v0

    return v0
.end method

.method public next()V
    .locals 1

    .prologue
    .line 2039
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->next()V

    .line 2040
    return-void
.end method

.method public open(Lcom/google/android/music/medialist/SongList;IZ)V
    .locals 1
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "position"    # I
    .param p3, "play"    # Z

    .prologue
    .line 1983
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/playback/MusicPlaybackService;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    .line 1984
    return-void
.end method

.method public openAndQueue(Lcom/google/android/music/medialist/SongList;I)V
    .locals 1
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "position"    # I

    .prologue
    .line 1987
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->openAndQueue(Lcom/google/android/music/medialist/SongList;I)V

    .line 1988
    return-void
.end method

.method public openMix(Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 1
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2209
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 2210
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 2027
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    .line 2028
    return-void
.end method

.method public play()V
    .locals 1

    .prologue
    .line 2030
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    .line 2031
    return-void
.end method

.method public playNext(Lcom/google/android/music/medialist/SongList;I)V
    .locals 1
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "fromPos"    # I

    .prologue
    .line 2020
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->playNext(Lcom/google/android/music/medialist/SongList;I)V

    .line 2021
    return-void
.end method

.method public position()J
    .locals 2

    .prologue
    .line 2081
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->position()J

    move-result-wide v0

    return-wide v0
.end method

.method public prev()V
    .locals 1

    .prologue
    .line 2036
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->prev()V

    .line 2037
    return-void
.end method

.method public refreshRadio()V
    .locals 1

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->refreshRadio()V

    .line 2006
    return-void
.end method

.method public registerMusicCastMediaRouterCallback(Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2235
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->registerMusicCastMediaRouterCallback(Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;)V

    .line 2236
    return-void
.end method

.method public removeDownloadProgressListener(Lcom/google/android/music/download/IDownloadProgressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/download/IDownloadProgressListener;

    .prologue
    .line 2158
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$2100(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/playback/LocalDevicePlayback;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->removeDownloadProgressListener(Lcom/google/android/music/download/IDownloadProgressListener;)V

    .line 2159
    return-void
.end method

.method public seek(J)J
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->seek(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public setMediaRoute(ZLjava/lang/String;)V
    .locals 1
    .param p1, "localRoute"    # Z
    .param p2, "routeId"    # Ljava/lang/String;

    .prologue
    .line 2145
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->setMediaRoute(ZLjava/lang/String;)V

    .line 2146
    return-void
.end method

.method public setMediaRouteVolume(Ljava/lang/String;D)V
    .locals 2
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "volume"    # D

    .prologue
    .line 2224
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/playback/MusicPlaybackService;->setMediaRouteVolume(Ljava/lang/String;D)V

    .line 2225
    return-void
.end method

.method public setQueuePosition(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2012
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->setQueuePosition(I)V

    .line 2013
    return-void
.end method

.method public setRating(I)V
    .locals 1
    .param p1, "rating"    # I

    .prologue
    .line 2078
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->setRating(I)V

    .line 2079
    return-void
.end method

.method public setRepeatMode(I)V
    .locals 1
    .param p1, "repeatmode"    # I

    .prologue
    .line 2099
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->setRepeatMode(I)V

    .line 2100
    return-void
.end method

.method public setShuffleMode(I)V
    .locals 1
    .param p1, "shufflemode"    # I

    .prologue
    .line 2093
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->setShuffleMode(I)V

    .line 2094
    return-void
.end method

.method public setUIVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 2162
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->setUIVisible(Z)V

    .line 2163
    return-void
.end method

.method public shuffleAll()V
    .locals 1

    .prologue
    .line 1991
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->shuffleAll()V

    .line 1992
    return-void
.end method

.method public shuffleOnDevice()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1996
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->shuffleOnDevice()V

    .line 1997
    return-void
.end method

.method public shuffleSongs(Lcom/google/android/music/medialist/SongList;)V
    .locals 1
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 2000
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 2001
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 2033
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->stop()V

    .line 2034
    return-void
.end method

.method public supportsRating()Z
    .locals 1

    .prologue
    .line 2072
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->supportsRating()Z

    move-result v0

    return v0
.end method

.method public updateMediaRouteVolume(Ljava/lang/String;D)V
    .locals 2
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "deltaRatio"    # D

    .prologue
    .line 2229
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->mService:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaRouteVolume(Ljava/lang/String;D)V

    .line 2230
    return-void
.end method
