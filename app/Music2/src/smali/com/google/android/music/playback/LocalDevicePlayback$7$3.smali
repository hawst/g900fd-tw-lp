.class Lcom/google/android/music/playback/LocalDevicePlayback$7$3;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$7;->onCloudQueueTrackChanged(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

.field final synthetic val$itemId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 799
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->val$itemId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 802
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v2, v2, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->val$itemId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/music/store/Store;->getPositionInCloudQueueForItemId(Ljava/lang/String;)I

    move-result v2

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 803
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 804
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Receiver now playing itemId: %s.  Setting new play pos: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->val$itemId:Ljava/lang/String;

    aput-object v5, v3, v4

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v4, v4, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v2, v2, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getAudioIdAndListItemId(I)Landroid/util/Pair;

    move-result-object v0

    .line 809
    .local v0, "audioIdAndListItemId":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/Long;>;"
    if-eqz v0, :cond_1

    .line 810
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v2, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/music/download/ContentIdentifier;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->refreshCursor(Lcom/google/android/music/download/ContentIdentifier;)Z
    invoke-static {v2, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 811
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v2, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v2, v4, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1102(Lcom/google/android/music/playback/LocalDevicePlayback;J)J

    .line 814
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->updateDescriptorAndQueue(Z)V
    invoke-static {v1, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3100(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 815
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-string v2, "com.android.music.metachanged"

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->notifyChange(Ljava/lang/String;)V

    .line 816
    return-void
.end method
