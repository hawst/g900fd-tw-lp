.class public Lcom/google/android/music/activitymanagement/KeepOnManager;
.super Ljava/lang/Object;
.source "KeepOnManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;,
        Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;,
        Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    }
.end annotation


# static fields
.field private static sKeepOnManager:Lcom/google/android/music/activitymanagement/KeepOnManager;


# instance fields
.field private mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

.field private mIsKeptAlbum:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mIsKeptAutoPlaylist:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mIsKeptPlaylist:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mIsKeptRadio:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mUnwindAlbumAnimation:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private mUnwindAutoPlaylistAnimation:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private mUnwindPlaylistAnimation:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private mUnwindRadioAnimation:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAlbum:Ljava/util/Map;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptPlaylist:Ljava/util/Map;

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAutoPlaylist:Ljava/util/Map;

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptRadio:Ljava/util/Map;

    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindAlbumAnimation:Ljava/util/Map;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindPlaylistAnimation:Ljava/util/Map;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindAutoPlaylistAnimation:Ljava/util/Map;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindRadioAnimation:Ljava/util/Map;

    .line 88
    return-void
.end method

.method public static getInstance()Lcom/google/android/music/activitymanagement/KeepOnManager;
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 96
    sget-object v0, Lcom/google/android/music/activitymanagement/KeepOnManager;->sKeepOnManager:Lcom/google/android/music/activitymanagement/KeepOnManager;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Lcom/google/android/music/activitymanagement/KeepOnManager;

    invoke-direct {v0}, Lcom/google/android/music/activitymanagement/KeepOnManager;-><init>()V

    sput-object v0, Lcom/google/android/music/activitymanagement/KeepOnManager;->sKeepOnManager:Lcom/google/android/music/activitymanagement/KeepOnManager;

    .line 99
    :cond_0
    sget-object v0, Lcom/google/android/music/activitymanagement/KeepOnManager;->sKeepOnManager:Lcom/google/android/music/activitymanagement/KeepOnManager;

    return-object v0
.end method

.method private initKeepOnViewStatusHelper(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;JLjava/util/Map;Ljava/util/Map;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "hasRemote"    # Z
    .param p4, "isKeptInDb"    # Z
    .param p5, "isAllLocal"    # Z
    .param p6, "downloadedSongCount"    # I
    .param p7, "keepOnSongCount"    # I
    .param p8, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p9, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p10, "oldId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZZZII",
            "Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;",
            "Lcom/google/android/music/animator/AnimatorUpdateListener;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 302
    .local p12, "keepOnStateCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    .local p13, "unwindAnimationStateCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/google/android/music/animator/Animator;>;"
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 303
    if-eqz p9, :cond_0

    .line 305
    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p13

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/animator/Animator;

    .line 306
    .local v4, "oldAnimation":Lcom/google/android/music/animator/Animator;
    if-eqz v4, :cond_0

    .line 307
    move-object/from16 v0, p9

    invoke-virtual {v4, v0}, Lcom/google/android/music/animator/Animator;->removeUpdateListener(Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    .line 311
    .end local v4    # "oldAnimation":Lcom/google/android/music/animator/Animator;
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p12

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 312
    .local v3, "isCachedKeptInDb":Ljava/lang/Boolean;
    if-nez v3, :cond_1

    .line 314
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, p12

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    :goto_0
    if-eqz p3, :cond_6

    .line 321
    const/4 v5, 0x1

    invoke-interface {p8, v5}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setPinnable(Z)V

    .line 323
    if-eqz p9, :cond_2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p13

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/animator/Animator;

    move-object v1, v5

    .line 324
    .local v1, "animation":Lcom/google/android/music/animator/Animator;
    :goto_1
    if-eqz v1, :cond_3

    .line 325
    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, Lcom/google/android/music/animator/Animator;->addUpdateListener(Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    .line 327
    const/4 v5, 0x0

    invoke-interface {p8, v5}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setPinned(Z)V

    .line 331
    :goto_2
    if-lez p7, :cond_4

    const/4 v5, -0x1

    if-eq p6, v5, :cond_4

    .line 332
    int-to-float v5, p6

    int-to-float v6, p7

    div-float v2, v5, v6

    .line 334
    .local v2, "downloadFraction":F
    const/4 v5, 0x0

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-interface {p8, v5}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setDownloadFraction(F)V

    .line 345
    .end local v1    # "animation":Lcom/google/android/music/animator/Animator;
    .end local v2    # "downloadFraction":F
    :goto_3
    invoke-interface {p8}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->invalidate()V

    .line 346
    return-void

    .line 316
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    goto :goto_0

    .line 323
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 329
    .restart local v1    # "animation":Lcom/google/android/music/animator/Animator;
    :cond_3
    invoke-interface {p8, p4}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setPinned(Z)V

    goto :goto_2

    .line 337
    :cond_4
    if-eqz p4, :cond_5

    const/4 v5, 0x0

    :goto_4
    invoke-interface {p8, v5}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setDownloadFraction(F)V

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    .line 341
    .end local v1    # "animation":Lcom/google/android/music/animator/Animator;
    :cond_6
    const/4 v5, 0x0

    invoke-interface {p8, v5}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setPinned(Z)V

    .line 342
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {p8, v5}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setDownloadFraction(F)V

    .line 343
    const/4 v5, 0x0

    invoke-interface {p8, v5}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setPinnable(Z)V

    goto :goto_3
.end method

.method private toggleKeepOnHelper(IJLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Ljava/util/Map;Ljava/util/Map;Landroid/content/Context;)V
    .locals 12
    .param p1, "type"    # I
    .param p2, "id"    # J
    .param p4, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p5, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p8, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;",
            "Lcom/google/android/music/animator/AnimatorUpdateListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/music/animator/Animator;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 352
    .local p6, "keepOnStateCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    .local p7, "unwindAnimationStateCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/google/android/music/animator/Animator;>;"
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 353
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    .line 354
    .local v10, "pinned":Ljava/lang/Boolean;
    if-nez v10, :cond_0

    .line 355
    invoke-interface/range {p4 .. p4}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->isPinned()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 358
    :cond_0
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 360
    invoke-interface/range {p4 .. p4}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->getDownloadFraction()F

    move-result v9

    .line 361
    .local v9, "downloadFraction":F
    const/4 v6, 0x0

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object/from16 v7, p8

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleKeepOnUpdaterForItem(IJZLandroid/content/Context;)V

    .line 362
    if-eqz p5, :cond_1

    .line 364
    new-instance v11, Lcom/google/android/music/animator/Animator;

    const v2, 0x44bb8000    # 1500.0f

    mul-float/2addr v2, v9

    float-to-int v2, v2

    const/4 v3, 0x0

    invoke-direct {v11, v2, v9, v3}, Lcom/google/android/music/animator/Animator;-><init>(IFF)V

    .line 366
    .local v11, "unwindAnimation":Lcom/google/android/music/animator/Animator;
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    move-object/from16 v0, p5

    invoke-virtual {v11, v0}, Lcom/google/android/music/animator/Animator;->addUpdateListener(Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    .line 368
    new-instance v2, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;

    move-object/from16 v0, p7

    move-object/from16 v1, p4

    invoke-direct {v2, v0, p2, p3, v1}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnViewAnimatorListener;-><init>(Ljava/util/Map;JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;)V

    invoke-virtual {v11, v2}, Lcom/google/android/music/animator/Animator;->addListener(Lcom/google/android/music/animator/AnimatorListener;)V

    .line 370
    invoke-virtual {v11}, Lcom/google/android/music/animator/Animator;->start()V

    .line 371
    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setIsUnwindAnimationInProcess(Z)V

    .line 373
    .end local v11    # "unwindAnimation":Lcom/google/android/music/animator/Animator;
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setPinned(Z)V

    .line 389
    .end local v9    # "downloadFraction":F
    :goto_0
    return-void

    .line 375
    :cond_2
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->setKeepOnDownloadPaused(Z)V

    .line 376
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setPinned(Z)V

    .line 377
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->setDownloadFraction(F)V

    .line 378
    const/4 v6, 0x1

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object/from16 v7, p8

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleKeepOnUpdaterForItem(IJZLandroid/content/Context;)V

    .line 379
    if-eqz p5, :cond_3

    .line 381
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/animator/Animator;

    .line 382
    .local v8, "animation":Lcom/google/android/music/animator/Animator;
    if-eqz v8, :cond_3

    .line 383
    invoke-virtual {v8}, Lcom/google/android/music/animator/Animator;->cancel()V

    .line 384
    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Lcom/google/android/music/animator/Animator;->removeUpdateListener(Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    .line 387
    .end local v8    # "animation":Lcom/google/android/music/animator/Animator;
    :cond_3
    invoke-interface/range {p4 .. p4}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->invalidate()V

    goto :goto_0

    .line 376
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static toggleSonglistKeepOn(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p3, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;

    .prologue
    .line 246
    invoke-static {}, Lcom/google/android/music/activitymanagement/KeepOnManager;->getInstance()Lcom/google/android/music/activitymanagement/KeepOnManager;

    move-result-object v0

    .line 247
    .local v0, "keepOnManager":Lcom/google/android/music/activitymanagement/KeepOnManager;
    if-eqz v0, :cond_1

    .line 248
    invoke-interface {p2}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->isPinnable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;->isPinned()Z

    move-result v1

    if-nez v1, :cond_0

    .line 251
    invoke-virtual {v0, p0, p1}, Lcom/google/android/music/activitymanagement/KeepOnManager;->showWillDownloadLaterDialogIfNecessary(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 253
    :cond_0
    invoke-virtual {p1, p0, v0, p2, p3}, Lcom/google/android/music/medialist/SongList;->toggleOfflineCaching(Landroid/content/Context;Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    .line 255
    :cond_1
    return-void
.end method


# virtual methods
.method public initAlbumKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V
    .locals 14
    .param p1, "albumId"    # J
    .param p3, "hasRemote"    # Z
    .param p4, "isKeptInDb"    # Z
    .param p5, "isAllLocal"    # Z
    .param p6, "downloadedSongCount"    # I
    .param p7, "keepOnSongCount"    # I
    .param p8, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p9, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p10, "oldAlbumId"    # J

    .prologue
    .line 154
    iget-object v12, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAlbum:Ljava/util/Map;

    iget-object v13, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindAlbumAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v1, p1

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-wide/from16 v10, p10

    invoke-direct/range {v0 .. v13}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initKeepOnViewStatusHelper(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;JLjava/util/Map;Ljava/util/Map;)V

    .line 157
    return-void
.end method

.method public initAutoPlaylistKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V
    .locals 14
    .param p1, "autoPlaylistId"    # J
    .param p3, "hasRemote"    # Z
    .param p4, "isKeptInDb"    # Z
    .param p5, "isAllLocal"    # Z
    .param p6, "downloadedSongCount"    # I
    .param p7, "keepOnSongCount"    # I
    .param p8, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p9, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p10, "oldAutoPlaylistId"    # J

    .prologue
    .line 184
    iget-object v12, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAutoPlaylist:Ljava/util/Map;

    iget-object v13, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindAutoPlaylistAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v1, p1

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-wide/from16 v10, p10

    invoke-direct/range {v0 .. v13}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initKeepOnViewStatusHelper(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;JLjava/util/Map;Ljava/util/Map;)V

    .line 187
    return-void
.end method

.method public initPlaylistKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V
    .locals 14
    .param p1, "playlistId"    # J
    .param p3, "hasRemote"    # Z
    .param p4, "isKeptInDb"    # Z
    .param p5, "isAllLocal"    # Z
    .param p6, "downloadedSongCount"    # I
    .param p7, "keepOnSongCount"    # I
    .param p8, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p9, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p10, "oldPlaylistId"    # J

    .prologue
    .line 169
    iget-object v12, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptPlaylist:Ljava/util/Map;

    iget-object v13, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindPlaylistAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v1, p1

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-wide/from16 v10, p10

    invoke-direct/range {v0 .. v13}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initKeepOnViewStatusHelper(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;JLjava/util/Map;Ljava/util/Map;)V

    .line 172
    return-void
.end method

.method public initRadioKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V
    .locals 14
    .param p1, "radioId"    # J
    .param p3, "hasRemote"    # Z
    .param p4, "isKeptInDb"    # Z
    .param p5, "isAllLocal"    # Z
    .param p6, "downloadedSongCount"    # I
    .param p7, "keepOnSongCount"    # I
    .param p8, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p9, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p10, "oldRadioId"    # J

    .prologue
    .line 199
    iget-object v12, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptRadio:Ljava/util/Map;

    iget-object v13, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindRadioAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v1, p1

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-wide/from16 v10, p10

    invoke-direct/range {v0 .. v13}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initKeepOnViewStatusHelper(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;JLjava/util/Map;Ljava/util/Map;)V

    .line 202
    return-void
.end method

.method public showWillDownloadLaterDialogIfNecessary(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 478
    if-eqz p1, :cond_3

    .line 479
    const/4 v6, 0x0

    .line 480
    .local v6, "showDialog":Z
    const-string v9, "connectivity"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 483
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 484
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    const/4 v2, 0x0

    .line 485
    .local v2, "isConnected":Z
    const/4 v4, 0x0

    .line 486
    .local v4, "isHighSpeed":Z
    if-eqz v0, :cond_1

    .line 487
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    .line 488
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    if-eq v9, v8, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    const/16 v10, 0x9

    if-ne v9, v10, :cond_4

    :cond_0
    move v4, v8

    .line 491
    :cond_1
    :goto_0
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    .line 493
    .local v5, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isOfflineDLOnlyOnWifi()Z

    move-result v3

    .line 494
    .local v3, "isDLWifiOnly":Z
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 495
    if-eqz v2, :cond_2

    if-eqz v3, :cond_5

    if-nez v4, :cond_5

    :cond_2
    move v6, v8

    .line 497
    :goto_1
    if-eqz v6, :cond_3

    .line 498
    new-instance v7, Lcom/google/android/music/activitymanagement/KeepOnManager$1;

    invoke-direct {v7, p0, p2, p1, v3}, Lcom/google/android/music/activitymanagement/KeepOnManager$1;-><init>(Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;Z)V

    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 533
    .end local v0    # "activeNetwork":Landroid/net/NetworkInfo;
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "isConnected":Z
    .end local v3    # "isDLWifiOnly":Z
    .end local v4    # "isHighSpeed":Z
    .end local v5    # "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v6    # "showDialog":Z
    :cond_3
    return-void

    .restart local v0    # "activeNetwork":Landroid/net/NetworkInfo;
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v2    # "isConnected":Z
    .restart local v4    # "isHighSpeed":Z
    .restart local v6    # "showDialog":Z
    :cond_4
    move v4, v7

    .line 488
    goto :goto_0

    .restart local v3    # "isDLWifiOnly":Z
    .restart local v5    # "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_5
    move v6, v7

    .line 495
    goto :goto_1
.end method

.method public toggleAlbumKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V
    .locals 9
    .param p1, "albumId"    # J
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 210
    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAlbum:Ljava/util/Map;

    iget-object v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindAlbumAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleKeepOnHelper(IJLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Ljava/util/Map;Ljava/util/Map;Landroid/content/Context;)V

    .line 212
    return-void
.end method

.method public toggleAutoPlaylistKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V
    .locals 9
    .param p1, "playlistId"    # J
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 230
    const/4 v1, 0x2

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAutoPlaylist:Ljava/util/Map;

    iget-object v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindAutoPlaylistAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleKeepOnHelper(IJLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Ljava/util/Map;Ljava/util/Map;Landroid/content/Context;)V

    .line 232
    return-void
.end method

.method public toggleKeepOnUpdaterForItem(IJZLandroid/content/Context;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "id"    # J
    .param p4, "select"    # Z
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 401
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 402
    const/4 v0, 0x0

    .line 403
    .local v0, "keepOnStateCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    if-eqz p4, :cond_2

    .line 404
    packed-switch p1, :pswitch_data_0

    .line 465
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 466
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    :cond_1
    return-void

    .line 406
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAlbum:Ljava/util/Map;

    .line 407
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->selectAlbumUpdateKeepOn(Landroid/content/Context;J)V

    .line 408
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->selectAlbum(J)V

    goto :goto_0

    .line 413
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptPlaylist:Ljava/util/Map;

    .line 414
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->selectPlaylistUpdateKeepOn(Landroid/content/Context;J)V

    .line 415
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 416
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->selectPlaylist(J)V

    goto :goto_0

    .line 420
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAutoPlaylist:Ljava/util/Map;

    .line 421
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->selectAutoPlaylistUpdateKeepOn(Landroid/content/Context;J)V

    .line 422
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 423
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->selectAutoPlaylist(J)V

    goto :goto_0

    .line 427
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptRadio:Ljava/util/Map;

    .line 428
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->selectRadioUpdateKeepOn(Landroid/content/Context;J)V

    .line 429
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 430
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->selectRadioStation(J)V

    goto :goto_0

    .line 434
    :cond_2
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 436
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAlbum:Ljava/util/Map;

    .line 437
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->deselectAlbumUpdateKeepOn(Landroid/content/Context;J)V

    .line 438
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 439
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->deselectAlbum(J)V

    goto :goto_0

    .line 443
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptPlaylist:Ljava/util/Map;

    .line 444
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->deselectPlaylistUpdateKeepOn(Landroid/content/Context;J)V

    .line 445
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 446
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->deselectPlaylist(J)V

    goto :goto_0

    .line 450
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptAutoPlaylist:Ljava/util/Map;

    .line 451
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->deselectAutoPlaylistUpdateKeepOn(Landroid/content/Context;J)V

    .line 452
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 453
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->deselectAutoPlaylist(J)V

    goto :goto_0

    .line 457
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptRadio:Ljava/util/Map;

    .line 458
    invoke-static {p5, p2, p3}, Lcom/google/android/music/store/KeepOnUpdater;->deselectRadioUpdateKeepOn(Landroid/content/Context;J)V

    .line 459
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    if-eqz v1, :cond_0

    .line 460
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mAvailableSpaceTracker:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/music/AvailableSpaceTracker;->deselectRadioStation(J)V

    goto/16 :goto_0

    .line 404
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 434
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public togglePlaylistKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V
    .locals 9
    .param p1, "playlistId"    # J
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 220
    const/4 v1, 0x1

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptPlaylist:Ljava/util/Map;

    iget-object v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindPlaylistAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleKeepOnHelper(IJLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Ljava/util/Map;Ljava/util/Map;Landroid/content/Context;)V

    .line 222
    return-void
.end method

.method public toggleRadioKeepOn(JLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Landroid/content/Context;)V
    .locals 9
    .param p1, "radioId"    # J
    .param p3, "keepOnState"    # Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
    .param p4, "animListener"    # Lcom/google/android/music/animator/AnimatorUpdateListener;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 240
    const/4 v1, 0x3

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mIsKeptRadio:Ljava/util/Map;

    iget-object v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager;->mUnwindRadioAnimation:Ljava/util/Map;

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/activitymanagement/KeepOnManager;->toggleKeepOnHelper(IJLcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;Ljava/util/Map;Ljava/util/Map;Landroid/content/Context;)V

    .line 242
    return-void
.end method
