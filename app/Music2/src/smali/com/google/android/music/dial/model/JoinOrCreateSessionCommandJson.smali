.class public Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "JoinOrCreateSessionCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 34
    iget-object v0, p0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "session:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "joinOrCreateSession"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 37
    new-instance v0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    .line 38
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson;->mBody:Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/JoinOrCreateSessionCommandJson$JoinOrCreateSessionCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
