.class public Lcom/google/android/music/utils/NowPlayingWidgetUtils;
.super Ljava/lang/Object;
.source "NowPlayingWidgetUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;,
        Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;,
        Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
    }
.end annotation


# static fields
.field private static mAlbumId:J

.field private static mDelayHandler:Landroid/os/Handler;

.field private static final sToken:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->sToken:Ljava/lang/Object;

    .line 67
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->mAlbumId:J

    return-void
.end method

.method public static createViews(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Integer;Landroid/os/Bundle;)Landroid/widget/RemoteViews;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "musicState"    # Landroid/content/Intent;
    .param p2, "widgetId"    # Ljava/lang/Integer;
    .param p3, "newOptions"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-static {p2, p0, p3}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->getWidgetParams(Ljava/lang/Integer;Landroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;

    move-result-object v1

    .line 88
    .local v1, "params":Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget v4, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;->layoutId:I

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 90
    .local v2, "views":Landroid/widget/RemoteViews;
    new-instance v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget v3, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;->layoutId:I

    invoke-direct {v0, p1, p0, v3}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;-><init>(Landroid/content/Intent;Landroid/content/Context;I)V

    .line 92
    .local v0, "metadata":Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
    iget-boolean v3, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->isEmpty:Z

    if-nez v3, :cond_0

    .line 94
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;->size:I

    invoke-static {p0, v0, v3, v2, v4}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->getAlbumArt(Landroid/content/Context;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;ILandroid/widget/RemoteViews;I)V

    .line 95
    invoke-static {v2, v0}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->displayTextViews(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;)V

    .line 96
    invoke-static {v2, v0, p0}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->setupControls(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;Landroid/content/Context;)V

    .line 97
    iget-boolean v3, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;->hideThumbs:Z

    invoke-static {v2, v0, p0, v3}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->setupThumbs(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;Landroid/content/Context;Z)V

    .line 103
    :goto_0
    return-object v2

    .line 100
    :cond_0
    invoke-static {v2, v0, p0}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->showEmptyState(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static displayTextViews(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;)V
    .locals 5
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "metadata"    # Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    .prologue
    const v4, 0x7f0e01c4

    const v3, 0x7f0e01c3

    const v2, 0x7f0e01c1

    const/4 v1, 0x0

    .line 195
    iget-object v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->track:Ljava/lang/String;

    invoke-virtual {p0, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 196
    iget-object v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->launchIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 197
    invoke-virtual {p0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 200
    iget-object v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->artist:Ljava/lang/String;

    invoke-virtual {p0, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 201
    iget-object v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->launchIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 202
    invoke-virtual {p0, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 205
    const v0, 0x7f0e01c2

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 207
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->album:Ljava/lang/String;

    invoke-virtual {p0, v4, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 210
    iget-object v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->launchIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v4, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 211
    invoke-virtual {p0, v4, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 213
    :cond_0
    return-void
.end method

.method private static getAlbumArt(Landroid/content/Context;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;ILandroid/widget/RemoteViews;I)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "metadata"    # Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
    .param p2, "widgetId"    # I
    .param p3, "views"    # Landroid/widget/RemoteViews;
    .param p4, "size"    # I

    .prologue
    .line 310
    sget-object v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->mDelayHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 311
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->mDelayHandler:Landroid/os/Handler;

    .line 317
    :cond_0
    iget-wide v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->albumId:J

    sget-wide v2, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->mAlbumId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 318
    sget-object v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->mDelayHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->sToken:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 319
    iget-wide v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->albumId:J

    sput-wide v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->mAlbumId:J

    .line 322
    :cond_1
    sget-object v6, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->mDelayHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$1;

    move-object v1, p1

    move v2, p4

    move v3, p2

    move-object v4, p3

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$1;-><init>(Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;IILandroid/widget/RemoteViews;Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->sToken:Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x190

    add-long/2addr v2, v4

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 329
    return-void
.end method

.method public static getRatingIntent(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rating"    # I

    .prologue
    .line 360
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.music.musicservicecommand.rating"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 363
    .local v0, "ratingIntent":Landroid/content/Intent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 364
    const-string v1, "rating"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 365
    return-object v0
.end method

.method private static getWidgetParams(Ljava/lang/Integer;Landroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;
    .locals 5
    .param p0, "widgetId"    # Ljava/lang/Integer;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "options"    # Landroid/os/Bundle;

    .prologue
    .line 132
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1, p1, p2}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->getWidgetParamsJB(ILandroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;

    move-result-object v1

    .line 138
    :goto_0
    return-object v1

    .line 136
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 138
    .local v0, "artSize":I
    new-instance v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;

    const v2, 0x7f040083

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;-><init>(IIZLcom/google/android/music/utils/NowPlayingWidgetUtils$1;)V

    goto :goto_0
.end method

.method private static getWidgetParamsJB(ILandroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;
    .locals 18
    .param p0, "widgetId"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "options"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 146
    .local v9, "res":Landroid/content/res/Resources;
    if-nez p2, :cond_0

    .line 147
    invoke-static/range {p1 .. p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v14

    move/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object p2

    .line 152
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->isTouchWizDevice(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 153
    move/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->populateBundleFromPreferences(ILandroid/os/Bundle;Landroid/content/Context;)V

    .line 157
    :cond_1
    const-string v14, "appWidgetMaxHeight"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 158
    .local v12, "widgetMaxHeight":I
    const-string v14, "appWidgetMaxWidth"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 161
    .local v13, "widgetMaxWidth":I
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    .line 162
    .local v8, "metrics":Landroid/util/DisplayMetrics;
    const/4 v14, 0x1

    int-to-float v15, v12

    invoke-static {v14, v15, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v14

    float-to-int v12, v14

    .line 164
    const/4 v14, 0x1

    int-to-float v15, v13

    invoke-static {v14, v15, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v14

    float-to-int v13, v14

    .line 167
    const v14, 0x7f0f00a8

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    int-to-float v5, v14

    .line 168
    .local v5, "buttonSize":F
    const v14, 0x7f0f00a9

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    int-to-float v11, v14

    .line 170
    .local v11, "trackHeight":F
    const v14, 0x7f0f00f4

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 172
    .local v7, "largeArtSize":I
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/SystemUtils;->isLowMemory(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 173
    div-int/lit8 v7, v7, 0x2

    .line 175
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0f00a5

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 178
    .local v10, "smallAlbumSize":I
    add-float v14, v5, v11

    float-to-double v14, v14

    const-wide/high16 v16, 0x3ffc000000000000L    # 1.75

    mul-double v14, v14, v16

    int-to-double v0, v12

    move-wide/from16 v16, v0

    cmpl-double v14, v14, v16

    if-lez v14, :cond_4

    .line 179
    sub-int v4, v13, v10

    .line 182
    .local v4, "buttonFrameSize":I
    int-to-float v14, v4

    const/high16 v15, 0x40c00000    # 6.0f

    const/high16 v16, 0x41200000    # 10.0f

    add-float v16, v16, v5

    mul-float v15, v15, v16

    cmpg-float v14, v14, v15

    if-gez v14, :cond_3

    const/4 v6, 0x1

    .line 183
    .local v6, "hideThumbs":Z
    :goto_0
    new-instance v14, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;

    const v15, 0x7f040083

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-direct {v14, v15, v10, v6, v0}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;-><init>(IIZLcom/google/android/music/utils/NowPlayingWidgetUtils$1;)V

    .line 185
    .end local v4    # "buttonFrameSize":I
    .end local v6    # "hideThumbs":Z
    :goto_1
    return-object v14

    .line 182
    .restart local v4    # "buttonFrameSize":I
    :cond_3
    const/4 v6, 0x0

    goto :goto_0

    .line 185
    .end local v4    # "buttonFrameSize":I
    :cond_4
    new-instance v14, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;

    const v15, 0x7f040082

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v14, v15, v7, v0, v1}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;-><init>(IIZLcom/google/android/music/utils/NowPlayingWidgetUtils$1;)V

    goto :goto_1
.end method

.method public static isTouchWizDevice(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 373
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 374
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.category.HOME"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v4, 0x10000

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 377
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 379
    .local v0, "currentHomePackage":Ljava/lang/String;
    const-string v3, "com.sec.android.app.launcher"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static populateBundleFromPreferences(ILandroid/os/Bundle;Landroid/content/Context;)V
    .locals 13
    .param p0, "widgetId"    # I
    .param p1, "options"    # Landroid/os/Bundle;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 334
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f00a5

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 337
    .local v0, "defaultMaxHeight":I
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f00a2

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 341
    .local v1, "defaultMaxWidth":I
    const-string v7, "pref_music_widget"

    invoke-virtual {p2, v7, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 344
    .local v3, "preferences":Landroid/content/SharedPreferences;
    const-string v7, "%s//%s"

    new-array v8, v12, [Ljava/lang/Object;

    const-string v9, "pref_music_widget_height"

    aput-object v9, v8, v10

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 346
    .local v2, "heightKey":Ljava/lang/String;
    const-string v7, "%s//%s"

    new-array v8, v12, [Ljava/lang/Object;

    const-string v9, "pref_music_widget_width"

    aput-object v9, v8, v10

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 349
    .local v6, "widthKey":Ljava/lang/String;
    invoke-interface {v3, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 350
    .local v4, "widgetMaxHeight":I
    invoke-interface {v3, v6, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 352
    .local v5, "widgetMaxWidth":I
    const-string v7, "appWidgetMaxHeight"

    invoke-virtual {p1, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 353
    const-string v7, "appWidgetMaxWidth"

    invoke-virtual {p1, v7, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 354
    return-void
.end method

.method private static setupControls(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;Landroid/content/Context;)V
    .locals 8
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "metadata"    # Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const v7, 0x7f0e01b7

    const v6, 0x7f0e01b6

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 223
    const v1, 0x7f0e01b5

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.music.musicservicecommand.previous"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v5, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4, p2, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v4

    invoke-static {p2, v2, v4, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {p0, v1, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 228
    const v1, 0x7f0e01b8

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.music.musicservicecommand.next"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v5, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v4, p2, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v4

    invoke-static {p2, v2, v4, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {p0, v1, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 233
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.music.musicservicecommand.togglepause"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v4, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v1, p2, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 235
    .local v0, "togglePlay":Landroid/content/Intent;
    const-string v1, "removeNotification"

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 236
    const-string v1, "googlemusic"

    const-string v4, "togglePause"

    const-string v5, "removeNotification"

    invoke-static {v1, v4, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 238
    invoke-static {p2, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p0, v6, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 241
    invoke-static {p2, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p0, v7, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 245
    iget-object v1, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    iget-boolean v1, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->isPlaying:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p0, v7, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 248
    iget-object v1, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    iget-boolean v1, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->isPlaying:Z

    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {p0, v6, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 252
    const v1, 0x7f0e01bb

    invoke-virtual {p0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 253
    return-void

    :cond_1
    move v1, v3

    .line 245
    goto :goto_0
.end method

.method private static setupThumbs(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;Landroid/content/Context;Z)V
    .locals 7
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "metadata"    # Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "hideThumbs"    # Z

    .prologue
    const v6, 0x7f0e01b3

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 261
    if-eqz p3, :cond_0

    .line 263
    invoke-virtual {p0, v6, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 264
    const v0, 0x7f0e01b4

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 265
    const v0, 0x7f0e01b9

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 266
    const v0, 0x7f0e01ba

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 267
    const v0, 0x7f0e01b2

    const-string v1, "setWeightSum"

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/RemoteViews;->setFloat(ILjava/lang/String;F)V

    .line 302
    :goto_0
    return-void

    .line 270
    :cond_0
    invoke-static {p2, v5}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->getRatingIntent(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p2, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 274
    const v0, 0x7f0e01b4

    invoke-static {p2, v2}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->getRatingIntent(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    invoke-static {p2, v2, v3, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 278
    const v0, 0x7f0e01b9

    invoke-static {p2, v4}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->getRatingIntent(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    invoke-static {p2, v2, v3, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 282
    const v0, 0x7f0e01ba

    invoke-static {p2, v2}, Lcom/google/android/music/utils/NowPlayingWidgetUtils;->getRatingIntent(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    invoke-static {p2, v2, v3, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 288
    iget v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->thumbs:I

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v6, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 291
    const v3, 0x7f0e01b4

    iget v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->thumbs:I

    if-ne v0, v5, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {p0, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 294
    const v3, 0x7f0e01b9

    iget v0, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->thumbs:I

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p0, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 297
    const v0, 0x7f0e01ba

    iget v3, p1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->thumbs:I

    if-ne v3, v4, :cond_4

    :goto_4
    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 300
    const v0, 0x7f0e01b2

    const-string v1, "setWeightSum"

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/RemoteViews;->setFloat(ILjava/lang/String;F)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 288
    goto :goto_1

    :cond_2
    move v0, v1

    .line 291
    goto :goto_2

    :cond_3
    move v0, v2

    .line 294
    goto :goto_3

    :cond_4
    move v2, v1

    .line 297
    goto :goto_4
.end method

.method private static showEmptyState(Landroid/widget/RemoteViews;Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;Landroid/content/Context;)V
    .locals 3
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "metadata"    # Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x7f0e01c1

    .line 113
    const v0, 0x7f0e01bd

    const v1, 0x7f020170

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 114
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 116
    const v0, 0x7f0e01c2

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 117
    const v0, 0x7f0e01c3

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 118
    const v0, 0x7f0e01c4

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 119
    const v0, 0x7f0e01bb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 122
    const v0, 0x7f0e01bc

    invoke-static {p2}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenApp(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 124
    invoke-static {p2}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenApp(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 125
    return-void
.end method
