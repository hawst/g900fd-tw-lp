.class public Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "InfiniteMixRequestJson.java"


# instance fields
.field public mNumEntries:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "numEntries"
    .end annotation
.end field

.field public mRecentlyPlayed:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "recentlyPlayed"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;",
            ">;"
        }
    .end annotation
.end field

.field public mSeedId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seedId"
    .end annotation
.end field

.field public mSeedType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seedType"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static serialize(Lcom/google/android/music/cloudclient/MixTrackId;ILjava/util/List;)[B
    .locals 5
    .param p0, "seedId"    # Lcom/google/android/music/cloudclient/MixTrackId;
    .param p1, "maxEntries"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "recentlyPlayed":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    new-instance v2, Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;

    invoke-direct {v2}, Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;-><init>()V

    .line 31
    .local v2, "request":Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/MixTrackId;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;->mSeedId:Ljava/lang/String;

    .line 32
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/MixTrackId;->getType()Lcom/google/android/music/cloudclient/MixTrackId$Type;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/cloudclient/MixTrackId;->trackIdTypeToServerType(Lcom/google/android/music/cloudclient/MixTrackId$Type;)I

    move-result v4

    iput v4, v2, Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;->mSeedType:I

    .line 33
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, v2, Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;->mRecentlyPlayed:Ljava/util/List;

    .line 34
    iput p1, v2, Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;->mNumEntries:I

    .line 35
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/MixTrackId;

    .line 36
    .local v3, "trackId":Lcom/google/android/music/cloudclient/MixTrackId;
    new-instance v0, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;-><init>()V

    .line 37
    .local v0, "entry":Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;
    invoke-virtual {v3}, Lcom/google/android/music/cloudclient/MixTrackId;->getRemoteId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;->mId:Ljava/lang/String;

    .line 38
    invoke-virtual {v3}, Lcom/google/android/music/cloudclient/MixTrackId;->getType()Lcom/google/android/music/cloudclient/MixTrackId$Type;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/cloudclient/MixTrackId;->trackIdTypeToServerType(Lcom/google/android/music/cloudclient/MixTrackId$Type;)I

    move-result v4

    iput v4, v0, Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;->mType:I

    .line 39
    iget-object v4, v2, Lcom/google/android/music/cloudclient/InfiniteMixRequestJson;->mRecentlyPlayed:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    .end local v0    # "entry":Lcom/google/android/music/cloudclient/RecentlyPlayedEntryJson;
    .end local v3    # "trackId":Lcom/google/android/music/cloudclient/MixTrackId;
    :cond_0
    invoke-static {v2}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v4

    return-object v4
.end method
