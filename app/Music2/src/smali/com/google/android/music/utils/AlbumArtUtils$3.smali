.class final Lcom/google/android/music/utils/AlbumArtUtils$3;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/AlbumArtUtils;->getMusicUserContentPromoteNautilusPipe(Landroid/content/Context;)Landroid/os/ParcelFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$bm:Landroid/graphics/Bitmap;

.field final synthetic val$out:Ljava/io/OutputStream;


# direct methods
.method constructor <init>(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 3013
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$3;->val$bm:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lcom/google/android/music/utils/AlbumArtUtils$3;->val$out:Ljava/io/OutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 3017
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$3;->val$bm:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    iget-object v3, p0, Lcom/google/android/music/utils/AlbumArtUtils$3;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3018
    const-string v0, "AlbumArtUtils"

    const-string v1, "Could not compress bitmap."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3021
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$3;->val$out:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 3023
    return-void

    .line 3021
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$3;->val$out:Ljava/io/OutputStream;

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v0
.end method
