.class Lcom/google/android/music/download/artwork/ArtMonitorImpl$2;
.super Ljava/lang/Object;
.source "ArtMonitorImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/ArtMonitorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$2;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$2;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mBound:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$300(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$2;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$500(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$2;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;
    invoke-static {v1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$400(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Lcom/google/android/music/utils/SafeServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 95
    :cond_0
    return-void
.end method
