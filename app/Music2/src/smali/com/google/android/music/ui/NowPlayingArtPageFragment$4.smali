.class Lcom/google/android/music/ui/NowPlayingArtPageFragment$4;
.super Landroid/content/BroadcastReceiver;
.source "NowPlayingArtPageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingArtPageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 161
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.google.android.music.nowplaying.DRAWER_STATE_CHANGED_ACTION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateArtVisibility()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$800(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 164
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeVisibility()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$900(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 165
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$4;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->startPanningIfNeeded()V
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$1000(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 167
    :cond_0
    return-void
.end method
