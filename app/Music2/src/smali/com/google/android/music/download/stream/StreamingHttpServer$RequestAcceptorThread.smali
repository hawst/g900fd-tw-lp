.class Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;
.super Ljava/lang/Thread;
.source "StreamingHttpServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/stream/StreamingHttpServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestAcceptorThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/stream/StreamingHttpServer;)V
    .locals 1

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    .line 130
    const-string v0, "RequestAcceptorThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->shutdownOldWorkers()V

    return-void
.end method

.method private shutdownOldWorkers()V
    .locals 6

    .prologue
    .line 166
    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v5

    monitor-enter v5

    .line 167
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 168
    .local v3, "workers":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 169
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;

    .line 170
    .local v1, "worker":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    invoke-virtual {v1}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v2

    .line 172
    .local v2, "workerDownloadRequest":Lcom/google/android/music/download/TrackDownloadRequest;
    if-eqz v2, :cond_0

    .line 173
    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;
    invoke-static {v4}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$400(Lcom/google/android/music/download/stream/StreamingHttpServer;)Lcom/google/android/music/download/stream/AllowedStreams;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/music/download/stream/AllowedStreams;->findStreamByRequest(Lcom/google/android/music/download/TrackDownloadRequest;)Lcom/google/android/music/download/stream/StreamingContent;

    move-result-object v0

    .line 175
    .local v0, "content":Lcom/google/android/music/download/stream/StreamingContent;
    if-nez v0, :cond_0

    .line 176
    invoke-virtual {v1}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->shutdown()V

    .line 177
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 181
    .end local v0    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    .end local v1    # "worker":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    .end local v2    # "workerDownloadRequest":Lcom/google/android/music/download/TrackDownloadRequest;
    .end local v3    # "workers":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v3    # "workers":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 135
    const/4 v3, 0x0

    .line 137
    .local v3, "socket":Ljava/net/Socket;
    :goto_0
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mShutdown:Z
    invoke-static {v6}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$100(Lcom/google/android/music/download/stream/StreamingHttpServer;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 138
    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    iget-object v6, v6, Lcom/google/android/music/download/stream/StreamingHttpServer;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v6}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    .line 143
    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v6}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v7

    monitor-enter v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :try_start_1
    new-instance v4, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;

    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    invoke-direct {v4, v6, v3}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;-><init>(Lcom/google/android/music/download/stream/StreamingHttpServer;Ljava/net/Socket;)V

    .line 145
    .local v4, "worker":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v6}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 146
    invoke-virtual {v4}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->start()V

    .line 148
    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$300()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v6}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    const/4 v8, 0x2

    if-le v6, v8, :cond_2

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    .local v2, "log":Ljava/lang/StringBuilder;
    const-string v6, "More than 2 worker running: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;
    invoke-static {v6}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;

    .line 152
    .local v5, "wt":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    invoke-virtual {v5}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 156
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "log":Ljava/lang/StringBuilder;
    .end local v4    # "worker":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    .end local v5    # "wt":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->this$0:Lcom/google/android/music/download/stream/StreamingHttpServer;

    # getter for: Lcom/google/android/music/download/stream/StreamingHttpServer;->mShutdown:Z
    invoke-static {v6}, Lcom/google/android/music/download/stream/StreamingHttpServer;->access$100(Lcom/google/android/music/download/stream/StreamingHttpServer;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 160
    const-string v6, "StreamingHttpServer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RequestAcceptorThread exited abnormally: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 163
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    return-void

    .line 154
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "log":Ljava/lang/StringBuilder;
    .restart local v4    # "worker":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    :cond_1
    :try_start_3
    const-string v6, "StreamingHttpServer"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "log":Ljava/lang/StringBuilder;
    :cond_2
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0
.end method
