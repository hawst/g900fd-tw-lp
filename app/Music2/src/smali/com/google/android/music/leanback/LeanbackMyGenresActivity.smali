.class public Lcom/google/android/music/leanback/LeanbackMyGenresActivity;
.super Lcom/google/android/music/leanback/LeanbackBrowseActivity;
.source "LeanbackMyGenresActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getBrowseTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const v0, 0x7f0b00a0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;

    new-instance v1, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackMyGenresActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 77
    .local v0, "genresAdapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/music/medialist/AllGenreList;

    invoke-direct {v2}, Lcom/google/android/music/medialist/AllGenreList;-><init>()V

    sget-object v3, Lcom/google/android/music/ui/GenreGridFragment;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 78
    return-object v0
.end method
