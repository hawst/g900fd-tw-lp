.class public final Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
.super Ljava/lang/Object;
.source "CacheLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/CacheLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "StorageMetadata"
.end annotation


# instance fields
.field private mDescription:Ljava/lang/String;

.field private mDiscoveryMethods:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;",
            ">;"
        }
    .end annotation
.end field

.field private mEmulated:Z

.field private mMountPoint:Ljava/lang/String;

.field private mUsable:Z

.field final synthetic this$0:Lcom/google/android/music/download/cache/CacheLocationManager;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/cache/CacheLocationManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 563
    iput-object p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->this$0:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566
    const-class v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDiscoveryMethods:Ljava/util/EnumSet;

    .line 571
    iput-boolean v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mEmulated:Z

    .line 572
    iput-boolean v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mUsable:Z

    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .param p1, "x1"    # Z

    .prologue
    .line 563
    iput-boolean p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mUsable:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .prologue
    .line 563
    iget-boolean v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mEmulated:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .param p1, "x1"    # Z

    .prologue
    .line 563
    iput-boolean p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mEmulated:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mMountPoint:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDescription:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;)Ljava/util/EnumSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDiscoveryMethods:Ljava/util/EnumSet;

    return-object v0
.end method


# virtual methods
.method public getDescription(ZLcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;
    .locals 9
    .param p1, "debug"    # Z
    .param p2, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 597
    invoke-virtual {p2}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    if-ne v4, v5, :cond_1

    .line 598
    iget-object v4, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->this$0:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-virtual {v4, p2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v3

    .line 599
    .local v3, "volInfo":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    invoke-virtual {v3}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->getTotalSpace()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->this$0:Lcom/google/android/music/download/cache/CacheLocationManager;

    # getter for: Lcom/google/android/music/download/cache/CacheLocationManager;->mUnitStrings:[Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/download/cache/CacheLocationManager;->access$1100(Lcom/google/android/music/download/cache/CacheLocationManager;)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/music/utils/IOUtils;->readableFileSize(J[Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 601
    .local v2, "totalSpace":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->this$0:Lcom/google/android/music/download/cache/CacheLocationManager;

    # getter for: Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/music/download/cache/CacheLocationManager;->access$000(Lcom/google/android/music/download/cache/CacheLocationManager;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b02bf

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 603
    .local v0, "description":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 604
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " (Discovered name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDescription:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 605
    .local v1, "extended":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 610
    .end local v1    # "extended":Ljava/lang/String;
    .end local v2    # "totalSpace":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "volInfo":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    :cond_0
    :goto_0
    return-object v0

    .line 608
    .end local v0    # "description":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDescription:Ljava/lang/String;

    .restart local v0    # "description":Ljava/lang/String;
    goto :goto_0
.end method

.method public getDiscoveryMethods()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDiscoveryMethods:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getMountPoint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mMountPoint:Ljava/lang/String;

    return-object v0
.end method

.method public isEmulated()Z
    .locals 1

    .prologue
    .line 631
    iget-boolean v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mEmulated:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StorageMetadata{mDescription=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mDiscoveryMethods="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDiscoveryMethods:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMountPoint=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mMountPoint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mEmulated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mEmulated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mUsable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mUsable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
