.class public Lcom/google/android/music/download/cache/CacheLocationManager;
.super Ljava/lang/Object;
.source "CacheLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;,
        Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

.field private static volatile sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mKnownLocationMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocationMetadataMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            "Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/util/concurrent/locks/ReadWriteLock;

.field private mStorageReceiver:Landroid/content/BroadcastReceiver;

.field private final mUnitStrings:[Ljava/lang/String;

.field private final mUnusableLocations:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            ">;"
        }
    .end annotation
.end field

.field private final sStatFs:Landroid/os/StatFs;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/cache/CacheLocationManager;->LOGV:Z

    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

    .line 49
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 50
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 53
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mVolumeIdMap:Ljava/util/HashMap;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    .line 64
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mUnusableLocations:Ljava/util/HashSet;

    .line 65
    new-instance v0, Landroid/os/StatFs;

    const-string v1, "/"

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->sStatFs:Landroid/os/StatFs;

    .line 66
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 69
    new-instance v0, Lcom/google/android/music/download/cache/CacheLocationManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/cache/CacheLocationManager$1;-><init>(Lcom/google/android/music/download/cache/CacheLocationManager;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 91
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mStorageReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/music/download/cache/CacheLocationManager;->MEDIA_STORAGE_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    .line 93
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/utils/IOUtils;->getSizeUnitStrings(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mUnitStrings:[Ljava/lang/String;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/cache/CacheLocationManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/cache/CacheLocationManager;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/net/Uri;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/download/cache/CacheLocationManager;->mainProcessInitialize(Ljava/lang/String;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/download/cache/CacheLocationManager;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mUnitStrings:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/download/cache/CacheLocationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->initialize()V

    return-void
.end method

.method private addKnownLocationNoLock(Ljava/io/File;Ljava/io/File;Ljava/lang/String;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Lcom/google/android/music/download/cache/CacheUtils$StorageType;ZZ)V
    .locals 11
    .param p1, "path"    # Ljava/io/File;
    .param p2, "mountPoint"    # Ljava/io/File;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "discoveryMethod"    # Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
    .param p5, "storageType"    # Lcom/google/android/music/download/cache/CacheUtils$StorageType;
    .param p6, "emulated"    # Z
    .param p7, "usable"    # Z

    .prologue
    .line 298
    sget-boolean v6, Lcom/google/android/music/download/cache/CacheLocationManager;->LOGV:Z

    if-eqz v6, :cond_0

    .line 299
    const-string v6, "CacheLocationMgr"

    const-string v7, "addKnownLocationNoLock: path: %s, mountPoint: %s, description: %s, discoveryMethod: %s, storageType: %s"

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    aput-object p2, v8, v9

    const/4 v9, 0x2

    aput-object p3, v8, v9

    const/4 v9, 0x3

    aput-object p4, v8, v9

    const/4 v9, 0x4

    aput-object p5, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/cache/CacheLocation;

    .line 304
    .local v2, "location":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v2, :cond_9

    .line 305
    sget-boolean v6, Lcom/google/android/music/download/cache/CacheLocationManager;->LOGV:Z

    if-eqz v6, :cond_1

    .line 306
    const-string v6, "CacheLocationMgr"

    const-string v7, "addKnownLocationNoLock: Creating new location as path is unknown"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_1
    new-instance v2, Lcom/google/android/music/download/cache/CacheLocation;

    .end local v2    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    move-object/from16 v0, p5

    invoke-direct {v2, p1, v0}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    .line 309
    .restart local v2    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    if-eqz p7, :cond_2

    .line 312
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->createVolumeIdIfMissing()Ljava/util/UUID;

    move-result-object v5

    .line 313
    .local v5, "volId":Ljava/util/UUID;
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mVolumeIdMap:Ljava/util/HashMap;

    invoke-virtual {v6, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    .end local v5    # "volId":Ljava/util/UUID;
    :cond_2
    :goto_0
    if-nez p7, :cond_3

    .line 348
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mUnusableLocations:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 350
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .line 351
    .local v3, "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    if-nez v3, :cond_5

    .line 352
    sget-boolean v6, Lcom/google/android/music/download/cache/CacheLocationManager;->LOGV:Z

    if-eqz v6, :cond_4

    .line 353
    const-string v6, "CacheLocationMgr"

    const-string v7, "addKnownLocationNoLock: storageMetadata is null, creating new"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_4
    new-instance v3, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .end local v3    # "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    invoke-direct {v3, p0}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;-><init>(Lcom/google/android/music/download/cache/CacheLocationManager;)V

    .line 356
    .restart local v3    # "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    :cond_5
    if-eqz p2, :cond_6

    .line 361
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mMountPoint:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->access$702(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Ljava/lang/String;)Ljava/lang/String;

    .line 363
    :cond_6
    if-eqz p3, :cond_7

    .line 364
    # setter for: Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDescription:Ljava/lang/String;
    invoke-static {v3, p3}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->access$802(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Ljava/lang/String;)Ljava/lang/String;

    .line 366
    :cond_7
    if-eqz p4, :cond_8

    .line 367
    # getter for: Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mDiscoveryMethods:Ljava/util/EnumSet;
    invoke-static {v3}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->access$900(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;)Ljava/util/EnumSet;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 369
    :cond_8
    move/from16 v0, p6

    # setter for: Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mEmulated:Z
    invoke-static {v3, v0}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->access$602(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Z)Z

    .line 370
    move/from16 v0, p7

    # setter for: Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mUsable:Z
    invoke-static {v3, v0}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->access$1002(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Z)Z

    .line 371
    .end local v3    # "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    :goto_1
    return-void

    .line 314
    :catch_0
    move-exception v1

    .line 315
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    const-string v6, "CacheLocationMgr"

    const-string v7, "Unable to create volume ID, %s path at %s is missing. Skipping."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 321
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    :cond_9
    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v6

    move-object/from16 v0, p5

    if-eq v6, v0, :cond_2

    .line 322
    sget-boolean v6, Lcom/google/android/music/download/cache/CacheLocationManager;->LOGV:Z

    if-eqz v6, :cond_a

    .line 323
    const-string v6, "CacheLocationMgr"

    const-string v7, "addKnownLocationNoLock: Known path exists, reconciling storage types"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_a
    sget-object v6, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-object/from16 v0, p5

    if-ne v0, v6, :cond_2

    .line 328
    sget-boolean v6, Lcom/google/android/music/download/cache/CacheLocationManager;->LOGV:Z

    if-eqz v6, :cond_b

    .line 329
    const-string v6, "CacheLocationMgr"

    const-string v7, "addKnownLocationNoLock: new type is EXTERNAL, replacing old known location with new one"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_b
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .line 333
    .local v4, "oldMetadata":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    new-instance v2, Lcom/google/android/music/download/cache/CacheLocation;

    .end local v2    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    move-object/from16 v0, p5

    invoke-direct {v2, p1, v0}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    .line 334
    .restart local v2    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    if-eqz v4, :cond_c

    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v6, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    :cond_c
    # getter for: Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->mEmulated:Z
    invoke-static {v4}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->access$600(Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;)Z

    move-result v6

    if-eqz v6, :cond_d

    const/16 p6, 0x1

    .line 342
    :cond_d
    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->createVolumeIdIfMissing()Ljava/util/UUID;

    move-result-object v5

    .line 343
    .restart local v5    # "volId":Ljava/util/UUID;
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mVolumeIdMap:Ljava/util/HashMap;

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    iget-object v6, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mVolumeIdMap:Ljava/util/HashMap;

    invoke-virtual {v6, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private calculateSpace(Ljava/io/File;)J
    .locals 10
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 486
    const-wide/16 v6, 0x0

    .line 487
    .local v6, "total":J
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 488
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 489
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_1

    .line 490
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 491
    .local v1, "f":Ljava/io/File;
    invoke-direct {p0, v1}, Lcom/google/android/music/download/cache/CacheLocationManager;->calculateSpace(Ljava/io/File;)J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 490
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 495
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "files":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 497
    :cond_1
    return-wide v6
.end method

.method private checkMountPointsMatch(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "intentMountPoint"    # Landroid/net/Uri;
    .param p2, "metaMountPoint"    # Ljava/lang/String;
    .param p3, "volumeId"    # Ljava/lang/String;
    .param p4, "loggerKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 184
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 185
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v3, "file"

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 186
    .local v1, "fileUri":Landroid/net/Uri;
    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    iget-object v3, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 190
    const/4 v2, 0x1

    .line 192
    :cond_0
    return v2
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    if-nez v0, :cond_1

    .line 98
    const-class v1, Lcom/google/android/music/download/cache/CacheLocationManager;

    monitor-enter v1

    .line 99
    :try_start_0
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/cache/CacheLocationManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    .line 101
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    invoke-direct {v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->initialize()V

    .line 103
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_1
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    return-object v0

    .line 103
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getInstanceNoContext()Lcom/google/android/music/download/cache/CacheLocationManager;
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access CacheLocationManager before it has been initialized with a context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    sget-object v0, Lcom/google/android/music/download/cache/CacheLocationManager;->sInstance:Lcom/google/android/music/download/cache/CacheLocationManager;

    return-object v0
.end method

.method private initialize()V
    .locals 13

    .prologue
    .line 196
    sget-boolean v0, Lcom/google/android/music/download/cache/CacheLocationManager;->LOGV:Z

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "CacheLocationMgr"

    const-string v1, "initializing CacheLocation manager"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/download/cache/StorageProbeUtils;->findAllLocations(Landroid/content/Context;)Ljava/util/Collection;

    move-result-object v12

    .line 201
    .local v12, "results":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 204
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 205
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mVolumeIdMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 206
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mUnusableLocations:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 208
    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;

    .line 211
    .local v8, "discovered":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    iget-object v0, v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->location:Lcom/google/android/music/download/cache/CacheLocation;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v1

    iget-object v2, v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->mountPoint:Ljava/io/File;

    iget-object v3, v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->description:Ljava/lang/String;

    iget-object v4, v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->discoveryMethod:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    iget-object v0, v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->location:Lcom/google/android/music/download/cache/CacheLocation;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v5

    iget-boolean v6, v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->isEmulated:Z

    iget-boolean v7, v8, Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;->isUsable:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/download/cache/CacheLocationManager;->addKnownLocationNoLock(Ljava/io/File;Ljava/io/File;Ljava/lang/String;Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;Lcom/google/android/music/download/cache/CacheUtils$StorageType;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 217
    .end local v8    # "discovered":Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;
    .end local v9    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 220
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v11

    .line 221
    .local v11, "manager":Landroid/support/v4/content/LocalBroadcastManager;
    new-instance v10, Landroid/content/Intent;

    const-string v0, "com.google.android.music.download.cache.CacheLocationManager.LocationsChanged"

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 222
    .local v10, "intent":Landroid/content/Intent;
    invoke-virtual {v11, v10}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 223
    return-void
.end method

.method private isLocationEmulated(Lcom/google/android/music/download/cache/CacheLocation;)Z
    .locals 2
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 408
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .line 409
    .local v0, "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->isEmulated()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 410
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private mainProcessInitialize(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 17
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "mountPoint"    # Landroid/net/Uri;

    .prologue
    .line 121
    new-instance v10, Ljava/lang/Object;

    invoke-direct {v10}, Ljava/lang/Object;-><init>()V

    .line 122
    .local v10, "reference":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v13, v10}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v9

    .line 125
    .local v9, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v11, 0x0

    .line 127
    .local v11, "selectedId":Ljava/util/UUID;
    :try_start_0
    invoke-virtual {v9}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 129
    invoke-static {v10}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 133
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v8

    .line 134
    .local v8, "oldSelectedVolume":Lcom/google/android/music/download/cache/CacheLocation;
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v7

    .line 136
    .local v7, "oldSelectedMeta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/cache/CacheLocationManager;->initialize()V

    .line 140
    if-eqz v11, :cond_1

    .line 141
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v12

    .line 143
    .local v12, "selectedVolume":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v12, :cond_0

    if-nez v8, :cond_0

    .line 145
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v5

    .line 146
    .local v5, "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    invoke-virtual {v5}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->getMountPoint()Ljava/lang/String;

    move-result-object v6

    .line 147
    .local v6, "metaMountPoint":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, "externalStorageCardMounted"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v6, v13, v14}, Lcom/google/android/music/download/cache/CacheLocationManager;->checkMountPointsMatch(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 150
    .local v2, "didMount":Z
    if-eqz v2, :cond_0

    .line 151
    const-string v13, "CacheLocationMgr"

    const-string v14, "Music storage %s at %s now available"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v11, v15, v16

    const/16 v16, 0x1

    aput-object v6, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    new-instance v4, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    const-class v14, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v4, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 154
    .local v4, "intent":Landroid/content/Intent;
    const-string v13, "com.google.android.music.download.cache.CacheService.CLEAR_ORPHANED"

    invoke-virtual {v4, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v13, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 159
    .end local v2    # "didMount":Z
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .end local v6    # "metaMountPoint":Ljava/lang/String;
    :cond_0
    if-nez v12, :cond_1

    if-eqz v8, :cond_1

    .line 161
    invoke-virtual {v7}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->getMountPoint()Ljava/lang/String;

    move-result-object v6

    .line 162
    .restart local v6    # "metaMountPoint":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, "externalStorageCardUnmounted"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v6, v13, v14}, Lcom/google/android/music/download/cache/CacheLocationManager;->checkMountPointsMatch(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 165
    .local v3, "didUnmount":Z
    if-eqz v3, :cond_1

    .line 166
    const-string v13, "CacheLocationMgr"

    const-string v14, "Music storage %s at %s now unavailable"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v11, v15, v16

    const/16 v16, 0x1

    aput-object v6, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    .end local v3    # "didUnmount":Z
    .end local v6    # "metaMountPoint":Ljava/lang/String;
    .end local v12    # "selectedVolume":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_1
    return-void

    .line 129
    .end local v7    # "oldSelectedMeta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .end local v8    # "oldSelectedVolume":Lcom/google/android/music/download/cache/CacheLocation;
    :catchall_0
    move-exception v13

    invoke-static {v10}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v13
.end method


# virtual methods
.method public asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 3
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 442
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v1

    const-string v2, "music"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 444
    .local v0, "musicDir":Ljava/io/File;
    new-instance v1, Lcom/google/android/music/download/cache/CacheLocation;

    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    .line 446
    .end local v0    # "musicDir":Ljava/io/File;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public calculateMusicUsedSpace(Lcom/google/android/music/download/cache/CacheLocation;)J
    .locals 4
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 474
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    .line 475
    .local v0, "cl":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v0, :cond_0

    move-object v0, p1

    .line 476
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/music/download/cache/CacheLocationManager;->calculateSpace(Ljava/io/File;)J

    move-result-wide v2

    .line 477
    .local v2, "spaceInUse":J
    return-wide v2
.end method

.method public deviceHasExternalStorage()Z
    .locals 13

    .prologue
    const/4 v9, 0x1

    .line 501
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v10}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 502
    const/4 v8, 0x0

    .line 507
    .local v8, "printDebugInfo":Z
    :try_start_0
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mUnusableLocations:Ljava/util/HashSet;

    invoke-virtual {v10}, Ljava/util/HashSet;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    if-nez v10, :cond_0

    .line 556
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v10}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 560
    :goto_0
    return v9

    .line 510
    :cond_0
    :try_start_1
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/download/cache/CacheLocation;

    .line 511
    .local v6, "location":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v6, :cond_2

    .line 512
    const-string v10, "CacheLocationMgr"

    const-string v11, "mKnownLocationMap has a null value. This shouldn\'t happen. Skipping this entry, and debugging info will be printed below"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const/4 v8, 0x1

    .line 515
    goto :goto_1

    .line 517
    :cond_2
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v10, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .line 518
    .local v7, "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    if-nez v7, :cond_3

    .line 519
    const-string v10, "CacheLocationMgr"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLocationMetadataMap does not have a value for location "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ". This should never happen. Debugging info will be "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "printed"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    const/4 v8, 0x1

    .line 523
    goto :goto_1

    .line 525
    :cond_3
    invoke-virtual {v6}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v10

    sget-object v11, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    if-ne v10, v11, :cond_1

    invoke-virtual {v7}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->isEmulated()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v10

    if-nez v10, :cond_1

    .line 556
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v10}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 535
    .end local v6    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v7    # "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    :cond_4
    :try_start_2
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 536
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/content/Context;->getExternalFilesDirs(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v1

    .line 537
    .local v1, "dirs":[Ljava/io/File;
    move-object v0, v1

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_2
    if-ge v4, v5, :cond_6

    aget-object v3, v0, v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 538
    .local v3, "f":Ljava/io/File;
    if-nez v3, :cond_5

    .line 556
    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v10}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 537
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 541
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "dirs":[Ljava/io/File;
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_6
    if-eqz v8, :cond_8

    .line 542
    :try_start_3
    const-string v9, "CacheLocationMgr"

    const-string v10, "========================================"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const-string v9, "CacheLocationMgr"

    const-string v10, "mKnownLocationMap contents:"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    const-string v9, "CacheLocationMgr"

    const-string v10, "========================================"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iget-object v9, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 546
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<**>;"
    const-string v9, "CacheLocationMgr"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " => "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 556
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<**>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v9

    iget-object v10, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v10}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v9

    .line 548
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_7
    :try_start_4
    const-string v9, "CacheLocationMgr"

    const-string v10, "========================================"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    const-string v9, "CacheLocationMgr"

    const-string v10, "mLocationMetadataMap contents:"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const-string v9, "CacheLocationMgr"

    const-string v10, "========================================"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    iget-object v9, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 552
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<**>;"
    const-string v9, "CacheLocationMgr"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " => "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 556
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<**>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_8
    iget-object v9, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v9}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 560
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 418
    const-string v5, "CacheLocation Manager known locations information:"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 419
    iget-object v5, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/cache/CacheLocation;

    .line 420
    .local v2, "loc":Lcom/google/android/music/download/cache/CacheLocation;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Location: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 421
    iget-object v5, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    .line 422
    .local v3, "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 426
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v4

    .line 427
    .local v4, "volumeInformation":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 429
    .end local v4    # "volumeInformation":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to get volume information for location "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", invalid path"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 434
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "loc":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v3    # "meta":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    :cond_0
    const-string v5, "End CacheLocation Manager known locations information"

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 435
    return-void
.end method

.method public getDefaultLocation()Lcom/google/android/music/download/cache/CacheLocation;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 457
    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 458
    .local v0, "external":Ljava/io/File;
    iget-object v2, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 459
    .local v1, "internal":Ljava/io/File;
    invoke-static {}, Lcom/google/android/music/utils/PostFroyoUtils$EnvironmentCompat;->isExternalStorageEmulated()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 460
    new-instance v2, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v3, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {v2, v1, v3}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    .line 464
    :goto_0
    return-object v2

    .line 461
    :cond_0
    if-eqz v0, :cond_1

    .line 462
    new-instance v2, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v3, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {v2, v0, v3}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    goto :goto_0

    .line 464
    :cond_1
    new-instance v2, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v3, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {v2, v1, v3}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    goto :goto_0
.end method

.method public getInternal(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 274
    invoke-static {p1, v1}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 277
    .local v0, "internalDir":Ljava/io/File;
    if-nez v0, :cond_0

    .line 281
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/music/download/cache/CacheLocation;

    sget-object v2, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {v1, v0, v2}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    goto :goto_0
.end method

.method public getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 2
    .param p1, "volumeId"    # Ljava/util/UUID;

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 381
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mVolumeIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/cache/CacheLocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public getKnownUsableLocations()Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/music/download/cache/CacheLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392
    iget-object v4, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 394
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mKnownLocationMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    .line 395
    .local v3, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 396
    .local v0, "filteredValues":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/music/download/cache/CacheLocation;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/cache/CacheLocation;

    .line 397
    .local v2, "location":Lcom/google/android/music/download/cache/CacheLocation;
    iget-object v4, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mUnusableLocations:Ljava/util/HashSet;

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->isLocationEmulated(Lcom/google/android/music/download/cache/CacheLocation;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 398
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 403
    .end local v0    # "filteredValues":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/music/download/cache/CacheLocation;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "location":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v3    # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4

    .restart local v0    # "filteredValues":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/music/download/cache/CacheLocation;>;"
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0
.end method

.method public getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .locals 2
    .param p1, "loc"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 258
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 263
    :goto_0
    return-object v0

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 261
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLocationMetadataMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    .locals 6
    .param p1, "loc"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 235
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    .line 236
    :cond_0
    new-instance v0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    invoke-direct {v0}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;-><init>()V

    .line 237
    .local v0, "volumeInformation":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->sStatFs:Landroid/os/StatFs;

    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->sStatFs:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v2, v1

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->sStatFs:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    # setter for: Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mFreeSpace:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->access$302(Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;J)J

    .line 240
    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->sStatFs:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager;->sStatFs:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    # setter for: Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mTotalSpace:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->access$402(Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;J)J

    .line 242
    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v1

    # setter for: Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mVolumeId:Ljava/util/UUID;
    invoke-static {v0, v1}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->access$502(Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;Ljava/util/UUID;)Ljava/util/UUID;

    goto :goto_0
.end method
