.class Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TvHomeMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/TvHomeMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HomeMenuAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivity:Lcom/google/android/music/ui/BaseActivity;

.field private mItemWithIconStartPosition:I

.field private mItems:[Ljava/lang/String;

.field private mSelectedIndex:I


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/BaseActivity;II[Ljava/lang/String;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p2, "layoutId"    # I
    .param p3, "textViewResourceId"    # I
    .param p4, "objects"    # [Ljava/lang/String;

    .prologue
    .line 143
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 132
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mSelectedIndex:I

    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mItemWithIconStartPosition:I

    .line 144
    iput-object p4, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mItems:[Ljava/lang/String;

    .line 145
    iput-object p1, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mActivity:Lcom/google/android/music/ui/BaseActivity;

    .line 147
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

    # invokes: Lcom/google/android/music/ui/TvHomeMenu;->getScreenIndex(Lcom/google/android/music/ui/HomeActivity$Screen;)I
    invoke-static {v0}, Lcom/google/android/music/ui/TvHomeMenu;->access$000(Lcom/google/android/music/ui/HomeActivity$Screen;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mItemWithIconStartPosition:I

    .line 148
    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mItemWithIconStartPosition:I

    if-lt p1, v0, :cond_0

    .line 225
    const/4 v0, 0x1

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedIndex()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mSelectedIndex:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v10, 0x7f0e007d

    const/4 v5, 0x0

    const/4 v11, 0x0

    const/4 v9, 0x1

    .line 168
    invoke-virtual {p0}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 170
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getItemViewType(I)I

    move-result v8

    if-ne v8, v9, :cond_5

    .line 171
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 172
    .local v1, "infl":Landroid/view/LayoutInflater;
    const v8, 0x7f0400d8

    invoke-virtual {v1, v8, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 173
    .local v4, "row":Landroid/view/View;
    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 174
    .local v7, "tv":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0xe

    if-lt v8, v10, :cond_0

    .line 176
    const v8, 0x7f0e0103

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Switch;

    .line 180
    .local v6, "switchButton":Landroid/widget/Switch;
    if-eqz v6, :cond_0

    .line 181
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/Switch;->setVisibility(I)V

    .line 187
    .end local v6    # "switchButton":Landroid/widget/Switch;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const v10, 0x7f0b0058

    invoke-virtual {v0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 189
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f0200ff

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 197
    .local v2, "left":Landroid/graphics/drawable/Drawable;
    :goto_0
    invoke-virtual {v7, v2, v11, v11, v11}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 198
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f0f00c2

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 201
    const v8, 0x7f0e0259

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 202
    .local v3, "line":Landroid/view/View;
    iget-object v8, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mItems:[Ljava/lang/String;

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    if-ne p1, v8, :cond_1

    .line 203
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 210
    .end local v1    # "infl":Landroid/view/LayoutInflater;
    .end local v2    # "left":Landroid/graphics/drawable/Drawable;
    .end local v3    # "line":Landroid/view/View;
    :cond_1
    :goto_1
    iget v8, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mSelectedIndex:I

    if-ne p1, v8, :cond_2

    move v5, v9

    .line 211
    .local v5, "selected":Z
    :cond_2
    if-eqz v5, :cond_6

    .line 212
    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 219
    :goto_2
    return-object v4

    .line 190
    .end local v5    # "selected":Z
    .restart local v1    # "infl":Landroid/view/LayoutInflater;
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const v10, 0x7f0b008d

    invoke-virtual {v0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 192
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f020103

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .restart local v2    # "left":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 194
    .end local v2    # "left":Landroid/graphics/drawable/Drawable;
    :cond_4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f0200fd

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .restart local v2    # "left":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 206
    .end local v1    # "infl":Landroid/view/LayoutInflater;
    .end local v2    # "left":Landroid/graphics/drawable/Drawable;
    .end local v4    # "row":Landroid/view/View;
    .end local v7    # "tv":Landroid/widget/TextView;
    :cond_5
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 207
    .restart local v4    # "row":Landroid/view/View;
    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    move-object v7, v8

    check-cast v7, Landroid/widget/TextView;

    .restart local v7    # "tv":Landroid/widget/TextView;
    goto :goto_1

    .line 213
    .restart local v5    # "selected":Z
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->getItemViewType(I)I

    move-result v8

    if-ne v8, v9, :cond_7

    .line 214
    invoke-static {v7, v9}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    goto :goto_2

    .line 216
    :cond_7
    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x2

    return v0
.end method

.method public onItemSelected(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mSelectedIndex:I

    if-eq v0, p1, :cond_0

    .line 156
    iput p1, p0, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->mSelectedIndex:I

    .line 157
    invoke-virtual {p0}, Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;->notifyDataSetChanged()V

    .line 158
    const/4 v0, 0x1

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
