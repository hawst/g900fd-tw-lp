.class Lcom/google/android/music/preferences/MusicPreferences$4;
.super Ljava/lang/Object;
.source "MusicPreferences.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/preferences/MusicPreferences;->setDisplayOptions(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/preferences/MusicPreferences;

.field final synthetic val$choice:I


# direct methods
.method constructor <init>(Lcom/google/android/music/preferences/MusicPreferences;I)V
    .locals 0

    .prologue
    .line 1098
    iput-object p1, p0, Lcom/google/android/music/preferences/MusicPreferences$4;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    iput p2, p0, Lcom/google/android/music/preferences/MusicPreferences$4;->val$choice:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1100
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences$4;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1103
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.music.DOWNLOADED_ONLY_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1104
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "download_only"

    iget v4, p0, Lcom/google/android/music/preferences/MusicPreferences$4;->val$choice:I

    if-ne v4, v1, :cond_0

    :goto_0
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1106
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$4;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1107
    return-void

    :cond_0
    move v1, v2

    .line 1104
    goto :goto_0
.end method
