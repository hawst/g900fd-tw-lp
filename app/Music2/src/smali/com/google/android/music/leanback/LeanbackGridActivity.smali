.class public abstract Lcom/google/android/music/leanback/LeanbackGridActivity;
.super Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;
.source "LeanbackGridActivity.java"


# instance fields
.field private mAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackSearchOrbItemActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract bindCursor(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/database/Cursor;)Ljava/lang/Object;
.end method

.method protected createFragment()Landroid/app/Fragment;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/music/leanback/LeanbackGridFragment;

    invoke-direct {v0}, Lcom/google/android/music/leanback/LeanbackGridFragment;-><init>()V

    .line 45
    .local v0, "verticalGridFragment":Lcom/google/android/music/leanback/LeanbackGridFragment;
    new-instance v1, Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-direct {v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;-><init>()V

    .line 46
    .local v1, "verticalGridPresenter":Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->getNumberOfColumns()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->setNumberOfColumns(I)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->getGridTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setTitle(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setGridPresenter(Landroid/support/v17/leanback/widget/VerticalGridPresenter;)V

    .line 49
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->setupFragment(Landroid/app/Fragment;)V

    .line 50
    return-object v0
.end method

.method protected createItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected abstract getGridTitle()Ljava/lang/String;
.end method

.method protected abstract getMediaList()Lcom/google/android/music/medialist/MediaList;
.end method

.method protected getNumberOfColumns()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x5

    return v0
.end method

.method protected abstract getProjection()[Ljava/lang/String;
.end method

.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 4
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 72
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackGridActivity;->mAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->getNumberOfColumns()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .local v1, "size":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 73
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackGridActivity;->mAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p2, v2, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->getBackgroundImageMessageHandler()Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessage(Ljava/lang/Object;)V

    .line 79
    :goto_1
    return-void

    .line 72
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->getBackgroundImageMessageHandler()Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessage(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected setupFragment(Landroid/app/Fragment;)V
    .locals 5
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 55
    move-object v0, p1

    check-cast v0, Lcom/google/android/music/leanback/LeanbackGridFragment;

    .line 56
    .local v0, "gridFragment":Lcom/google/android/music/leanback/LeanbackGridFragment;
    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 57
    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 58
    invoke-virtual {v0, p0}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 59
    new-instance v1, Lcom/google/android/music/leanback/LeanbackGridActivity$1;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->createItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/music/leanback/LeanbackGridActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackGridActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    iput-object v1, p0, Lcom/google/android/music/leanback/LeanbackGridActivity;->mAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    .line 65
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackGridActivity;->mAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;->getProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/music/leanback/LeanbackGridActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackGridActivity;->mAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackGridFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 67
    return-void
.end method
