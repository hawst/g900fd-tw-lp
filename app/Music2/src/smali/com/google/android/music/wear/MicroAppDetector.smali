.class public Lcom/google/android/music/wear/MicroAppDetector;
.super Ljava/lang/Object;
.source "MicroAppDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;
    }
.end annotation


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

.field private final mPreferences:Landroid/content/SharedPreferences;

.field private final mRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mApplicationContext:Landroid/content/Context;

    .line 45
    new-instance v0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    invoke-direct {v0, p1}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    .line 46
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mApplicationContext:Landroid/content/Context;

    const-string v1, "wear_prefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mPreferences:Landroid/content/SharedPreferences;

    .line 48
    new-instance v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;

    iget-object v1, p0, Lcom/google/android/music/wear/MicroAppDetector;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/wear/MicroAppDetector;)Lcom/google/android/music/wear/WearMusicDataProvider;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/wear/MicroAppDetector;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gms/common/api/GoogleApiClient;

    .prologue
    .line 28
    invoke-static {p0}, Lcom/google/android/music/wear/MicroAppDetector;->pingConnectedNodesAndDisconnect(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/wear/MicroAppDetector;)Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/wear/MicroAppDetector;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    return-object v0
.end method

.method private hasSeenWearApp()Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "seen_wear_device_v2"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static pingConnectedNodesAndDisconnect(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 5
    .param p0, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;

    .prologue
    .line 136
    sget-object v1, Lcom/google/android/gms/wearable/Wearable;->NodeApi:Lcom/google/android/gms/wearable/NodeApi;

    invoke-interface {v1, p0}, Lcom/google/android/gms/wearable/NodeApi;->getConnectedNodes(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 139
    .local v0, "connectedNodes":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/wearable/NodeApi$GetConnectedNodesResult;>;"
    new-instance v1, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;

    invoke-direct {v1, p0}, Lcom/google/android/music/wear/MicroAppDetector$WakeUpAndDisconnectCallback;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    const-wide/16 v2, 0x1e

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;JLjava/util/concurrent/TimeUnit;)V

    .line 142
    return-void
.end method


# virtual methods
.method public refreshMicroAppPresence()V
    .locals 4

    .prologue
    .line 69
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/wear/MicroAppDetector;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    invoke-interface {v2}, Lcom/google/android/music/wear/WearMusicDataProvider;->isWearSyncMarkedAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/wear/MicroAppDetector;->hasSeenWearApp()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 79
    iget-object v2, p0, Lcom/google/android/music/wear/MicroAppDetector;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;

    invoke-interface {v2}, Lcom/google/android/music/wear/WearMusicDataProvider;->markWearSyncAvailable()V

    .line 83
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/wear/MicroAppDetector;->mApplicationContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/wear/GmsUtils;->newClientBuilderWithErrorNotification(Landroid/content/Context;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/wearable/Wearable;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    .line 89
    .local v0, "client":Lcom/google/android/gms/common/api/GoogleApiClient;
    new-instance v1, Lcom/google/android/music/wear/MicroAppDetector$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/wear/MicroAppDetector$1;-><init>(Lcom/google/android/music/wear/MicroAppDetector;Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 105
    .local v1, "microAppNodeCallback":Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;
    new-instance v2, Lcom/google/android/music/wear/MicroAppDetector$2;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/music/wear/MicroAppDetector$2;-><init>(Lcom/google/android/music/wear/MicroAppDetector;Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/GoogleApiClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    .line 119
    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    goto :goto_0
.end method

.method public shouldRefreshForDataItem(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "dataItemUri"    # Landroid/net/Uri;

    .prologue
    .line 57
    const-string v0, "music_micro_app_v2"

    invoke-static {p1, v0}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->uriIsForCapability(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
