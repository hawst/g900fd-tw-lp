.class public Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PlayIconAndTextListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$1;,
        Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;,
        Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mData:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

.field mLayoutResouceId:I


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 49
    move-object v3, p2

    .line 50
    .local v3, "row":Landroid/view/View;
    const/4 v1, 0x0

    .line 51
    .local v1, "holder":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;
    if-nez p2, :cond_0

    .line 52
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 53
    .local v2, "inflater":Landroid/view/LayoutInflater;
    iget v4, p0, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter;->mLayoutResouceId:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 54
    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;

    .end local v1    # "holder":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;
    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;-><init>(Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter;Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$1;)V

    .line 55
    .restart local v1    # "holder":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;
    const v4, 0x7f0e0235

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;->mIconView:Landroid/widget/ImageView;

    .line 56
    const v4, 0x7f0e0236

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;->mTextView:Landroid/widget/TextView;

    .line 57
    iget-object v4, v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;->mTextView:Landroid/widget/TextView;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 58
    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 63
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter;->mData:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    aget-object v0, v4, p1

    .line 64
    .local v0, "entry":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    iget-object v4, v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;->mIconView:Landroid/widget/ImageView;

    iget v5, v0, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;->mIconId:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    iget-object v4, v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;->mTextView:Landroid/widget/TextView;

    iget v5, v0, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;->mDescriptionId:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 67
    return-object v3

    .line 60
    .end local v0    # "entry":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "holder":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;
    check-cast v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;

    .restart local v1    # "holder":Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$TryNautilusListEntryHolder;
    goto :goto_0
.end method
