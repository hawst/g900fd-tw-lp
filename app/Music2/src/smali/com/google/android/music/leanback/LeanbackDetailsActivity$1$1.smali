.class Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;
.super Ljava/lang/Object;
.source "LeanbackDetailsActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;->onBindDescription(Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDescription:Ljava/lang/String;

.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;

.field final synthetic val$item:Lcom/google/android/music/leanback/Item;

.field final synthetic val$vh:Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;->this$1:Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;->val$item:Lcom/google/android/music/leanback/Item;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;->val$vh:Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/leanback/Item$StringGetter;->getString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;->mDescription:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;->val$vh:Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getSubtitle()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1$1;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method
