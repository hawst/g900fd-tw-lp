.class public Lcom/google/android/music/download/artwork/ArtMonitorFactory;
.super Ljava/lang/Object;
.source "ArtMonitorFactory.java"


# static fields
.field private static final sInitLock:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/music/download/artwork/ArtMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->sInitLock:Ljava/lang/Object;

    return-void
.end method

.method public static getArtMonitor(Landroid/content/Context;)Lcom/google/android/music/download/artwork/ArtMonitor;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->sInstance:Lcom/google/android/music/download/artwork/ArtMonitor;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->sInitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->sInstance:Lcom/google/android/music/download/artwork/ArtMonitor;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->sInstance:Lcom/google/android/music/download/artwork/ArtMonitor;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->sInstance:Lcom/google/android/music/download/artwork/ArtMonitor;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
