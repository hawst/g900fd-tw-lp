.class Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;
.super Ljava/lang/Object;
.source "LeanbackMyArtistsActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/Item$StringGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;->bindCursor(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/database/Cursor;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDescription:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;

.field final synthetic val$id:J


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;J)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;

    iput-wide p2, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->val$id:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 49
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->mDescription:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->mDescription:Ljava/lang/String;

    .line 58
    :goto_0
    return-object v1

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;

    iget-wide v2, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->val$id:J

    invoke-static {v1, v2, v3, v5}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistCount(Landroid/content/Context;JZ)I

    move-result v0

    .line 56
    .local v0, "count":I
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120004

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->mDescription:Ljava/lang/String;

    .line 58
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;->mDescription:Ljava/lang/String;

    goto :goto_0
.end method
