.class public Lcom/google/android/music/ui/cardlib/layout/MainstageExploreCardView;
.super Landroid/widget/LinearLayout;
.source "MainstageExploreCardView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/MainstageExploreCardView;->getMeasuredWidth()I

    move-result v0

    .line 27
    .local v0, "width":I
    invoke-virtual {p0, v0, v0}, Lcom/google/android/music/ui/cardlib/layout/MainstageExploreCardView;->setMeasuredDimension(II)V

    .line 28
    return-void
.end method
