.class public Lcom/google/android/music/download/MusicCommunicator;
.super Landroid/content/BroadcastReceiver;
.source "MusicCommunicator.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/MusicCommunicator;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 33
    const/4 v2, 0x0

    .line 34
    .local v2, "cleanupRequested":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "action":Ljava/lang/String;
    const-string v4, "com.google.android.music.accountchanged"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 37
    const/4 v2, 0x1

    .line 60
    :goto_0
    if-eqz v2, :cond_0

    .line 61
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/download/cache/TrackCacheService;

    invoke-direct {v1, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 62
    .local v1, "cacheIntent":Landroid/content/Intent;
    const-string v4, "com.google.android.music.download.cache.CacheService.CLEAR_ORPHANED"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    invoke-static {p1, v1}, Lcom/google/android/music/utils/SystemUtils;->startServiceOrFail(Landroid/content/Context;Landroid/content/Intent;)V

    .line 65
    .end local v1    # "cacheIntent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 38
    :cond_1
    const-string v4, "com.google.android.music.NEW_SHOULDKEEPON"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 39
    const-string v4, "deleteCachedFiles"

    const/4 v5, 0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 41
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    invoke-direct {v3, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    .local v3, "serviceIntent":Landroid/content/Intent;
    const-string v4, "com.google.android.music.download.keepon.KeeponSchedulingService.START_DOWNLOAD"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    invoke-virtual {v3, p2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 45
    invoke-static {p1, v3}, Lcom/google/android/music/utils/SystemUtils;->startServiceOrFail(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 46
    .end local v3    # "serviceIntent":Landroid/content/Intent;
    :cond_2
    const-string v4, "com.google.android.music.CLEAN_ORPHANED_FILES"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 47
    const/4 v2, 0x1

    goto :goto_0

    .line 48
    :cond_3
    const-string v4, "com.google.android.music.SYNC_COMPLETE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 51
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/music/store/KeepOnUpdater$SyncListener;

    invoke-direct {v3, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .restart local v3    # "serviceIntent":Landroid/content/Intent;
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-static {p1, v3}, Lcom/google/android/music/utils/SystemUtils;->startServiceOrFail(Landroid/content/Context;Landroid/content/Intent;)V

    .line 55
    invoke-static {p1}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->clearOrphanedFiles(Landroid/content/Context;)V

    goto :goto_0

    .line 57
    .end local v3    # "serviceIntent":Landroid/content/Intent;
    :cond_4
    const-string v4, "MusicCommunicator"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received unknown broadcast: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
