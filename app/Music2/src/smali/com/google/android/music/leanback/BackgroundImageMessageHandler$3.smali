.class Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;
.super Ljava/lang/Object;
.source "BackgroundImageMessageHandler.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->loadItemBackground(JLcom/google/android/music/leanback/Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

.field final synthetic val$item:Lcom/google/android/music/leanback/Item;

.field final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;JLcom/google/android/music/leanback/Item;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    iput-wide p2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->val$startTime:J

    iput-object p4, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapLoaded(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 212
    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->DEBUG:Z
    invoke-static {}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBitmapLoaded "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mLastLoadingStartTime:J
    invoke-static {v2}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$300(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->val$startTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mLastLoadingStartTime:J
    invoke-static {v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$300(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->val$startTime:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 242
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # setter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mItem:Lcom/google/android/music/leanback/Item;
    invoke-static {v0, v4}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$202(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;Lcom/google/android/music/leanback/Item;)Lcom/google/android/music/leanback/Item;

    .line 220
    if-nez p1, :cond_2

    .line 221
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    iget-wide v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;->val$startTime:J

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postDisplayBitmapMessage(JLandroid/graphics/Bitmap;)V

    goto :goto_0

    .line 223
    :cond_2
    new-instance v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3$1;-><init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler$3;Landroid/graphics/Bitmap;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0
.end method
