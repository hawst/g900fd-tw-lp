.class Lcom/google/android/music/playback/LocalDevicePlayback$23;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->openAndQueue(Lcom/google/android/music/medialist/SongList;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$medialist:Lcom/google/android/music/medialist/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V
    .locals 0

    .prologue
    .line 2578
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->val$medialist:Lcom/google/android/music/medialist/SongList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2582
    const/4 v9, 0x0

    .line 2583
    .local v9, "reload":Z
    const-wide/16 v10, 0x0

    .line 2584
    .local v10, "currentlListItemId":J
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isUsingPlayQueue()Z
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8900(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2585
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v10

    .line 2586
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v3

    if-nez v3, :cond_3

    move v9, v1

    .line 2589
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->val$medialist:Lcom/google/android/music/medialist/SongList;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isSuggestedMix(Lcom/google/android/music/medialist/SongList;)Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6900(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    .line 2590
    .local v0, "isSuggestedMix":Z
    if-eqz v0, :cond_1

    .line 2591
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->val$medialist:Lcom/google/android/music/medialist/SongList;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->enableInfiniteMixModeForSuggestedMix(Lcom/google/android/music/medialist/SongList;)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V

    .line 2592
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->updateLastTimeSuggestedMixPlayed()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7100(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2593
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->requestCreateRadioStationSync()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8000(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2596
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->val$medialist:Lcom/google/android/music/medialist/SongList;

    const/4 v3, 0x2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->queue(Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;
    invoke-static {v1, v2, v3, v10, v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1200(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;IJ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v12

    .line 2599
    .local v12, "result":Lcom/google/android/music/store/PlayQueueAddResult;
    if-nez v12, :cond_4

    .line 2600
    const-string v1, "LocalDevicePlayback"

    const-string v2, "Cannot add non-default domain to the queue"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2614
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2616
    return-void

    .end local v0    # "isSuggestedMix":Z
    .end local v12    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_3
    move v9, v2

    .line 2586
    goto :goto_0

    .line 2601
    .restart local v0    # "isSuggestedMix":Z
    .restart local v12    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    if-nez v1, :cond_5

    invoke-virtual {v12}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v1

    if-lez v1, :cond_5

    .line 2603
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayQueueSongList()Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaList(Lcom/google/android/music/medialist/SongList;)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V

    .line 2604
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2605
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 2606
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1600(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2608
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2614
    .end local v0    # "isSuggestedMix":Z
    .end local v12    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v1

    .line 2609
    .restart local v0    # "isSuggestedMix":Z
    .restart local v12    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_5
    if-eqz v9, :cond_2

    .line 2610
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 2611
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$23;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
