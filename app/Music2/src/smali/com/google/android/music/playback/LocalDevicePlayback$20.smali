.class Lcom/google/android/music/playback/LocalDevicePlayback$20;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$containerExtData:Ljava/lang/String;

.field final synthetic val$containerExtId:Ljava/lang/String;

.field final synthetic val$containerId:J

.field final synthetic val$containerName:Ljava/lang/String;

.field final synthetic val$containerType:I

.field final synthetic val$curAlbum:Ljava/lang/String;

.field final synthetic val$curAlbumId:J

.field final synthetic val$curArtist:Ljava/lang/String;

.field final synthetic val$curAudioId:J

.field final synthetic val$curDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

.field final synthetic val$curListItemId:J

.field final synthetic val$curTitle:Ljava/lang/String;

.field final synthetic val$full:Z

.field final synthetic val$isSeedIdLocal:Z

.field final synthetic val$lastUserExplicitPlayTime:J

.field final synthetic val$lastUserInteractionTime:J

.field final synthetic val$mediaList:Lcom/google/android/music/medialist/SongList;

.field final synthetic val$mixArtLocation:Ljava/lang/String;

.field final synthetic val$mixName:Ljava/lang/String;

.field final synthetic val$mixSeed:Ljava/lang/String;

.field final synthetic val$mixType:I

.field final synthetic val$playPos:I

.field final synthetic val$playerPosition:J

.field final synthetic val$rating:I

.field final synthetic val$repeatMode:I

.field final synthetic val$shuffleMode:I


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;ZLcom/google/android/music/medialist/SongList;IJLjava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/google/android/music/download/ContentIdentifier$Domain;JJJJIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1945
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-boolean p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$full:Z

    iput-object p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mediaList:Lcom/google/android/music/medialist/SongList;

    iput p4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$playPos:I

    iput-wide p5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curAlbumId:J

    iput-object p7, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curArtist:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curAlbum:Ljava/lang/String;

    iput p9, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$rating:I

    iput-object p10, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curTitle:Ljava/lang/String;

    iput-wide p11, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curAudioId:J

    iput-object p13, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curListItemId:J

    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$playerPosition:J

    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$lastUserInteractionTime:J

    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$lastUserExplicitPlayTime:J

    move/from16 v0, p22

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$repeatMode:I

    move/from16 v0, p23

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$shuffleMode:I

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixSeed:Ljava/lang/String;

    move/from16 v0, p25

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$isSeedIdLocal:Z

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixName:Ljava/lang/String;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixArtLocation:Ljava/lang/String;

    move/from16 v0, p28

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixType:I

    move/from16 v0, p29

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerType:I

    move-wide/from16 v0, p30

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerId:J

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerName:Ljava/lang/String;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerExtId:Ljava/lang/String;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerExtData:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1950
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->getPreferences()Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6300(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1951
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    iget-boolean v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$full:Z

    if-eqz v2, :cond_0

    .line 1952
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mediaList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v2}, Lcom/google/android/music/medialist/SongList;->freeze()Ljava/lang/String;

    move-result-object v1

    .line 1953
    .local v1, "embryo":Ljava/lang/String;
    const-string v2, "medialist"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1955
    .end local v1    # "embryo":Ljava/lang/String;
    :cond_0
    const-string v2, "curpos"

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$playPos:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1956
    const-string v2, "curalbumid"

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curAlbumId:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1957
    const-string v2, "curartist"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curArtist:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1958
    const-string v2, "curalbum"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curAlbum:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1959
    const-string v2, "rating"

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$rating:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1960
    const-string v2, "curtitle"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curTitle:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1961
    const-string v2, "isplaying"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1962
    const-string v2, "curAudioId"

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curAudioId:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1963
    const-string v3, "curDomainId"

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1964
    const-string v2, "curListItemId"

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curListItemId:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1965
    const-string v2, "seekpos"

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$playerPosition:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1966
    const-string v2, "lastUserInteract"

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$lastUserInteractionTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1967
    const-string v2, "lastUserExplicitPlay"

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$lastUserExplicitPlayTime:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1969
    const-string v2, "repeatMode"

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$repeatMode:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1970
    const-string v2, "shufflemode"

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$shuffleMode:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1971
    const-string v2, "infiniteMixSeedId"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixSeed:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1972
    const-string v2, "infiniteMixSeedTypeLocal"

    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$isSeedIdLocal:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1973
    const-string v2, "infiniteMixName"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixName:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1974
    const-string v2, "infiniteMixArtLocation"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixArtLocation:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1975
    const-string v2, "infiniteMixType"

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$mixType:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1977
    const-string v2, "containerType"

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerType:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1978
    const-string v2, "containerId"

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerId:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1979
    const-string v2, "containerName"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerName:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1980
    const-string v2, "containerExtId"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerExtId:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1981
    const-string v2, "containerExtData"

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$containerExtData:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1983
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1985
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1987
    return-void

    .line 1963
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->val$curDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier$Domain;->name()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto/16 :goto_0

    .line 1985
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$20;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v2
.end method
