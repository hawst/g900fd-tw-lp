.class Lcom/google/android/music/playback/LocalDevicePlayback$31;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->pause(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$transientPause:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0

    .prologue
    .line 3823
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$31;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-boolean p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$31;->val$transientPause:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 3828
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$31;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$31;->val$transientPause:Z

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->pauseSync(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9800(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3830
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$31;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 3832
    return-void

    .line 3830
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$31;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
