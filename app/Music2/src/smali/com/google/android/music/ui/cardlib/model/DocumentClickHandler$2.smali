.class final Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;
.super Ljava/lang/Object;
.source "DocumentClickHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$doc:Lcom/google/android/music/ui/cardlib/model/Document;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getGenreId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$2;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->playGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method
