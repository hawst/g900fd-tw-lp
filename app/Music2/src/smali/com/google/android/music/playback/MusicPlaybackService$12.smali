.class Lcom/google/android/music/playback/MusicPlaybackService$12;
.super Ljava/lang/Object;
.source "MusicPlaybackService.java"

# interfaces
.implements Lcom/google/android/music/art/ArtLoader$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionMetadata(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0

    .prologue
    .line 1809
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$12;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/String;)V
    .locals 3
    .param p1, "remoteArtUrl"    # Ljava/lang/String;

    .prologue
    .line 1825
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$100()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826
    const-string v0, "MusicPlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error downloading album art "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1828
    :cond_0
    return-void
.end method

.method public onSuccess(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .locals 4
    .param p1, "remoteArtUrl"    # Ljava/lang/String;
    .param p2, "pfd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 1812
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->MSG_ALBUM_ART_UPDATE:I
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1400()I

    move-result v1

    new-instance v2, Lcom/google/android/music/playback/MusicPlaybackService$12$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/music/playback/MusicPlaybackService$12$1;-><init>(Lcom/google/android/music/playback/MusicPlaybackService$12;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;ILjava/lang/Runnable;Z)V

    .line 1821
    return-void
.end method
