.class public Lcom/google/android/music/ui/ArtistPageActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "ArtistPageActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method public static final buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/AlbumList;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songList"    # Lcom/google/android/music/medialist/AlbumList;

    .prologue
    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/ArtistPageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "albumlist"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 55
    return-object v0
.end method

.method public static final showArtist(Landroid/content/Context;JLjava/lang/String;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # J
    .param p3, "artistName"    # Ljava/lang/String;
    .param p4, "shouldFilter"    # Z

    .prologue
    .line 42
    new-instance v1, Lcom/google/android/music/medialist/ArtistAlbumList;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/google/android/music/medialist/ArtistAlbumList;-><init>(JLjava/lang/String;Z)V

    invoke-static {p0, v1}, Lcom/google/android/music/ui/ArtistPageActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/AlbumList;)Landroid/content/Intent;

    move-result-object v0

    .line 43
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method public static final showNautilusArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nautilusId"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v1, Lcom/google/android/music/medialist/NautilusArtistAlbumList;

    invoke-direct {v1, p1, p2}, Lcom/google/android/music/medialist/NautilusArtistAlbumList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v1}, Lcom/google/android/music/ui/ArtistPageActivity;->buildStartIntent(Landroid/content/Context;Lcom/google/android/music/medialist/AlbumList;)Landroid/content/Intent;

    move-result-object v0

    .line 49
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 50
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistPageActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistPageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "albumlist"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/medialist/AlbumList;

    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->newInstance(Lcom/google/android/music/medialist/AlbumList;)Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    move-result-object v0

    .line 36
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/ArtistPageActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 38
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    return-void
.end method

.method protected usesActionBarOverlay()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method
