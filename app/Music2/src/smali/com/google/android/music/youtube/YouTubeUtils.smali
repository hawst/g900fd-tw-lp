.class public Lcom/google/android/music/youtube/YouTubeUtils;
.super Ljava/lang/Object;
.source "YouTubeUtils.java"


# static fields
.field public static final YOUTUBE_FORMAT_SHOCKWAVE_FLASH_STRING:Ljava/lang/String;

.field private static final YOUTUBE_VIDEO_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "^http[s]?://www.youtube.com/v/([^\\?]+)\\?.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/youtube/YouTubeUtils;->YOUTUBE_VIDEO_PATTERN:Ljava/util/regex/Pattern;

    .line 58
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/youtube/YouTubeUtils;->YOUTUBE_FORMAT_SHOCKWAVE_FLASH_STRING:Ljava/lang/String;

    return-void
.end method

.method public static canStartVideo(Landroid/app/Activity;)Z
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 225
    invoke-static {p0}, Lcom/google/android/youtube/player/YouTubeIntents;->canResolvePlayVideoIntent(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static formatDurationSeconds(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "durationSeconds"    # I

    .prologue
    const v5, 0x7f0b004c

    const v4, 0x7f0b004b

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 176
    div-int/lit8 v2, p1, 0x3c

    .line 177
    .local v2, "minutes":I
    div-int/lit8 v1, v2, 0x3c

    .line 178
    .local v1, "hours":I
    if-lez v1, :cond_0

    .line 179
    rem-int/lit8 v2, v2, 0x3c

    .line 181
    :cond_0
    rem-int/lit8 v3, p1, 0x3c

    .line 182
    .local v3, "seconds":I
    if-lez v1, :cond_1

    .line 183
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "bar":Ljava/lang/String;
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 188
    :goto_0
    return-object v4

    .line 187
    .end local v0    # "bar":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 188
    .restart local v0    # "bar":Ljava/lang/String;
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method static getEntries(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 3
    .param p0, "jsonString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 138
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 139
    .local v1, "result":Lorg/json/JSONObject;
    const-string v2, "feed"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 140
    .local v0, "feed":Lorg/json/JSONObject;
    const-string v2, "entry"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 141
    const/4 v2, 0x0

    .line 143
    :goto_0
    return-object v2

    :cond_0
    const-string v2, "entry"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    goto :goto_0
.end method

.method static getVideoId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "videoUrl"    # Ljava/lang/String;

    .prologue
    .line 161
    sget-object v1, Lcom/google/android/music/youtube/YouTubeUtils;->YOUTUBE_VIDEO_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 162
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 165
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static parseEntries(Lorg/json/JSONArray;Ljava/util/List;)V
    .locals 5
    .param p0, "entries"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/youtube/YouTubeVideo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "videos":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/youtube/YouTubeVideo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 149
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 150
    .local v2, "videoJson":Lorg/json/JSONObject;
    new-instance v1, Lcom/google/android/music/youtube/YouTubeVideo;

    invoke-direct {v1, v2}, Lcom/google/android/music/youtube/YouTubeVideo;-><init>(Lorg/json/JSONObject;)V

    .line 151
    .local v1, "v":Lcom/google/android/music/youtube/YouTubeVideo;
    invoke-virtual {v1}, Lcom/google/android/music/youtube/YouTubeVideo;->parse()Z

    move-result v3

    if-nez v3, :cond_0

    .line 152
    const-string v3, "YouTubeUtils"

    const-string v4, "Error parsing entry in YouTube feed."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157
    .end local v1    # "v":Lcom/google/android/music/youtube/YouTubeVideo;
    .end local v2    # "videoJson":Lorg/json/JSONObject;
    :cond_1
    return-void
.end method

.method public static searchForMatchingVideos(Ljava/lang/String;II)Ljava/util/List;
    .locals 13
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "startIndex"    # I
    .param p2, "maxResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/youtube/YouTubeVideo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v9, "videos":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/youtube/YouTubeVideo;>;"
    const/4 v1, 0x0

    .line 82
    .local v1, "encodedQuery":Ljava/lang/String;
    :try_start_0
    const-string v10, "utf-8"

    invoke-static {p0, v10}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 90
    const-string v10, "https://gdata.youtube.com/feeds/api/videos"

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "q"

    invoke-virtual {v10, v11, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "alt"

    const-string v12, "json"

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "format"

    sget-object v12, Lcom/google/android/music/youtube/YouTubeUtils;->YOUTUBE_FORMAT_SHOCKWAVE_FLASH_STRING:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "v"

    const-string v12, "2"

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "key"

    const-string v12, "AI39si7E6CHtSMfmqL04cFEUgotVb0C6eozj6Nb9M4EsFxZ-hCKWVwpFfRemzEQ0o8pTlkW_U6d6lF23MY1etGLlY18QkfBDeA"

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "orderby"

    const-string v12, "relevance"

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "start-index"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "max-results"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v11, "fields"

    const-string v12, "entry(author,yt:statistics,yt:hd,media:group(media:content,media:title,media:thumbnail))"

    invoke-virtual {v10, v11, v12}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 109
    .local v7, "searchUri":Ljava/lang/String;
    :try_start_1
    new-instance v4, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v4}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 110
    .local v4, "httpParameters":Lorg/apache/http/params/HttpParams;
    const-string v10, "http.protocol.handle-redirects"

    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v4, v10, v11}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 111
    const/16 v10, 0x2710

    invoke-static {v4, v10}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 112
    const/16 v10, 0x2710

    invoke-static {v4, v10}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 114
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 115
    .local v3, "httpClient":Lorg/apache/http/client/HttpClient;
    new-instance v10, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v10, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v10}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 116
    .local v6, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    .line 117
    .local v8, "statusLine":Lorg/apache/http/StatusLine;
    if-eqz v8, :cond_0

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v10

    const/16 v11, 0xc8

    if-ne v10, v11, :cond_0

    .line 118
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v10

    invoke-static {v10}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v5

    .line 119
    .local v5, "jsonString":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 123
    invoke-static {v5}, Lcom/google/android/music/youtube/YouTubeUtils;->getEntries(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 124
    .local v2, "entries":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 125
    invoke-static {v2, v9}, Lcom/google/android/music/youtube/YouTubeUtils;->parseEntries(Lorg/json/JSONArray;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 134
    .end local v2    # "entries":Lorg/json/JSONArray;
    .end local v3    # "httpClient":Lorg/apache/http/client/HttpClient;
    .end local v4    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v5    # "jsonString":Ljava/lang/String;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v7    # "searchUri":Ljava/lang/String;
    .end local v8    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_0
    :goto_0
    return-object v9

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v10, "YouTubeUtils"

    const-string v11, "Unsupported encoding"

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v7    # "searchUri":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 130
    .local v0, "e":Ljava/io/IOException;
    const-string v10, "YouTubeUtils"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error querying YouTube gdata API.  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 132
    .local v0, "e":Lorg/json/JSONException;
    const-string v10, "YouTubeUtils"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error in JSON.  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static startVideo(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "startTimeMs"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 203
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const-string v0, "YouTubeUtils"

    const-string v1, "Failed to load video id"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logYouTubeVideoViewed(Ljava/lang/String;)V

    .line 210
    const-string v1, "AIzaSyCQ8d_gKWCBARS-s47D9rCG9QtnNxxSz-I"

    move-object v0, p0

    move-object v2, p1

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/player/YouTubeStandalonePlayer;->createVideoIntent(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;IZZ)Landroid/content/Intent;

    move-result-object v6

    .line 214
    .local v6, "intent":Landroid/content/Intent;
    const-string v0, "google_account_name"

    invoke-virtual {v6, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const-string v0, "YouTubeUtils"

    const-string v1, "Start video: videoId=%s account=%s startTimeMs=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    aput-object p2, v2, v4

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const/16 v0, 0x3e8

    invoke-virtual {p0, v6, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
