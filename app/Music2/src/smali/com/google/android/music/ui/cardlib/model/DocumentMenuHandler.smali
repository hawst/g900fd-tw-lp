.class public Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.super Ljava/lang/Object;
.source "DocumentMenuHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$RemoveFromMyLibrary;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShopArtist;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FollowPlaylist;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FindVideo;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToQueue;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartVideo;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartInstantMix;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;,
        Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final LOGV:Z


# instance fields
.field protected mContext:Landroid/app/Activity;

.field protected final mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

.field protected mHasRemote:Z

.field protected mIsSelectedForOfflineCaching:Z

.field private mIsValidStoreService:Z

.field protected final mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

.field private mPlayCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

.field private mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-class v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->$assertionsDisabled:Z

    .line 98
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->LOGV:Z

    return-void

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p3, "cardView"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsValidStoreService:Z

    .line 152
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    .line 153
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v0}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    .line 154
    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 155
    iput-object p3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPlayCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsValidStoreService:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsValidStoreService:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->getIsAllNautilus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPlayCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPlayCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isDownloadedOnlyMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->showNotAvailableToast()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->startVideo()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->shareWithExternalApps(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->sharePlaylistWithExternalApps()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
    .param p1, "x1"    # J

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->setShareStateTask(J)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->getIsSideloaded()Z

    move-result v0

    return v0
.end method

.method private addKeepOnDeviceEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 632
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mHasRemote:Z

    if-nez v2, :cond_1

    .line 643
    :cond_0
    :goto_0
    return-void

    .line 635
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsValidStoreService:Z

    if-eqz v2, :cond_0

    .line 636
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->doesSupportKeepOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 637
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 638
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->supportsOfflineCaching(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 639
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 640
    .local v0, "keepOnDev":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$KeepOnDevice;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getIsAllNautilus()Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 410
    const/4 v7, 0x0

    .line 411
    .local v7, "ret":Z
    sget-boolean v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 414
    :cond_0
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "isAllPersistentNautilus"

    aput-object v0, v2, v9

    .line 423
    .local v2, "cols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-nez v0, :cond_2

    .line 424
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$XAudio;->getAudioUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 425
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 427
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    .line 428
    :cond_1
    const-string v0, "DocumentMenuHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown track id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 436
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_2
    return v7

    .line 430
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    move v7, v8

    :goto_1
    goto :goto_0

    :cond_4
    move v7, v9

    goto :goto_1

    .line 433
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getIsSideloaded()Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 469
    const/4 v7, 0x0

    .line 470
    .local v7, "ret":Z
    sget-boolean v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 471
    :cond_0
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "SourceAccount"

    aput-object v0, v2, v9

    .line 474
    .local v2, "cols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$XAudio;->getAudioUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 475
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 477
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 478
    :cond_1
    const-string v0, "DocumentMenuHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown track id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 487
    return v7

    .line 482
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    move v7, v8

    :goto_1
    goto :goto_0

    :cond_3
    move v7, v9

    goto :goto_1

    .line 485
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private isDownloadedOnlyMode()Z
    .locals 1

    .prologue
    .line 596
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    return v0
.end method

.method private isShowingMyLibrary()Z
    .locals 1

    .prologue
    .line 406
    const-class v0, Lcom/google/android/music/ui/MyLibraryFragment;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isSubFragmentOf(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method private isShowingPlayQueue()Z
    .locals 1

    .prologue
    .line 540
    const-class v0, Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isSubFragmentOf(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method private isSubFragmentOf(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/support/v4/app/Fragment;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "fragmentClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 396
    .local v0, "curFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    if-eqz v0, :cond_1

    .line 397
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 398
    const/4 v1, 0x1

    .line 402
    :goto_1
    return v1

    .line 400
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0

    .line 402
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private setShareStateTask(J)V
    .locals 1
    .param p1, "playlistId"    # J

    .prologue
    .line 491
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$2;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;J)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 502
    return-void
.end method

.method private sharePlaylistWithExternalApps()V
    .locals 9

    .prologue
    .line 450
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareToken()Ljava/lang/String;

    move-result-object v2

    .line 452
    .local v2, "shareToken":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0320

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistOwnerName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 456
    .local v1, "emailSubject":Ljava/lang/String;
    const-string v4, "https://play.google.com/music/playlist/"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 457
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 458
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 460
    .local v3, "shareUrl":Ljava/lang/String;
    invoke-direct {p0, v1, v3}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->shareWithExternalApps(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    return-void
.end method

.method private shareWithExternalApps(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "emailSubject"    # Ljava/lang/String;
    .param p2, "shareUrl"    # Ljava/lang/String;

    .prologue
    .line 440
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 441
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 442
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 444
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0311

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 447
    return-void
.end method

.method private shouldHideGoToAlbum()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 507
    const-class v9, Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-direct {p0, v9}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isSubFragmentOf(Ljava/lang/Class;)Z

    move-result v3

    .line 508
    .local v3, "onAlbumPage":Z
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 509
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v9}, Lcom/google/android/music/ui/MusicFragment;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v6

    .line 510
    .local v6, "parentMetajamId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v2

    .line 511
    .local v2, "metajamId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    if-eqz v3, :cond_1

    if-eqz v6, :cond_1

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    move v7, v8

    .line 516
    .end local v2    # "metajamId":Ljava/lang/String;
    .end local v6    # "parentMetajamId":Ljava/lang/String;
    :cond_1
    :goto_0
    return v7

    .line 514
    :cond_2
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v9}, Lcom/google/android/music/ui/MusicFragment;->getAlbumId()J

    move-result-wide v4

    .line 515
    .local v4, "parentAlbumId":J
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v0

    .line 516
    .local v0, "albumId":J
    const-wide/16 v10, -0x1

    cmp-long v9, v0, v10

    if-eqz v9, :cond_3

    if-eqz v3, :cond_1

    cmp-long v9, v4, v0

    if-nez v9, :cond_1

    :cond_3
    move v7, v8

    goto :goto_0
.end method

.method private shouldHideGoToArtist()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 525
    const-class v9, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-direct {p0, v9}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isSubFragmentOf(Ljava/lang/Class;)Z

    move-result v3

    .line 526
    .local v3, "onArtistPage":Z
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 527
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v9}, Lcom/google/android/music/ui/MusicFragment;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v6

    .line 528
    .local v6, "parentMetajamId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v2

    .line 529
    .local v2, "metajamId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    if-eqz v3, :cond_1

    if-eqz v6, :cond_1

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    move v7, v8

    .line 534
    .end local v2    # "metajamId":Ljava/lang/String;
    .end local v6    # "parentMetajamId":Ljava/lang/String;
    :cond_1
    :goto_0
    return v7

    .line 532
    :cond_2
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v9}, Lcom/google/android/music/ui/MusicFragment;->getArtistId()J

    move-result-wide v4

    .line 533
    .local v4, "parentArtistId":J
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistId()J

    move-result-wide v0

    .line 534
    .local v0, "artistId":J
    const-wide/16 v10, -0x1

    cmp-long v9, v0, v10

    if-eqz v9, :cond_3

    if-eqz v3, :cond_1

    cmp-long v9, v4, v0

    if-nez v9, :cond_1

    :cond_3
    move v7, v8

    goto :goto_0
.end method

.method private showNotAvailableToast()V
    .locals 3

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const v2, 0x7f0b0358

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 602
    return-void
.end method

.method private startVideo()V
    .locals 6

    .prologue
    .line 544
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getVideoId()Ljava/lang/String;

    move-result-object v2

    .line 545
    .local v2, "videoId":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 546
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 547
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 548
    const-string v3, "DocumentMenuHandler"

    const-string v4, "Missing account"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    :goto_0
    return-void

    .line 551
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/music/youtube/YouTubeUtils;->startVideo(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method protected addAddToMyLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 605
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->canAddToLibrary()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 606
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToMyLibrary;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 607
    .local v0, "addToMyLibrary":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    .end local v0    # "addToMyLibrary":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method protected addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 556
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/music/medialist/SongList;->supportsAppendToPlaylist()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 557
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 558
    .local v0, "addToPlaylist":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 560
    .end local v0    # "addToPlaylist":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToPlaylist;
    :cond_0
    return-void
.end method

.method protected addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 646
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isShowingPlayQueue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 647
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToQueue;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToQueue;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 648
    .local v0, "add2Q":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToQueue;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 650
    .end local v0    # "add2Q":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$AddToQueue;
    :cond_0
    return-void
.end method

.method protected addBuyEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 814
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    .line 815
    .local v1, "isSubscription":Z
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 816
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 817
    .local v0, "buy":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 819
    .end local v0    # "buy":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Buy;
    :cond_0
    return-void
.end method

.method protected addDeleteEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 6
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 659
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_6

    .line 660
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v3}, Lcom/google/android/music/ui/MusicFragment;->getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v0

    .line 661
    .local v0, "contextList":Lcom/google/android/music/medialist/MediaList;
    instance-of v3, v0, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    if-eqz v3, :cond_1

    .line 662
    const v2, 0x7f0b006e

    .line 721
    .end local v0    # "contextList":Lcom/google/android/music/medialist/MediaList;
    .local v2, "titleId":I
    :goto_0
    if-eqz v2, :cond_0

    .line 722
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;I)V

    .line 723
    .local v1, "del":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 725
    .end local v1    # "del":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Delete;
    :cond_0
    return-void

    .line 663
    .end local v2    # "titleId":I
    .restart local v0    # "contextList":Lcom/google/android/music/medialist/MediaList;
    :cond_1
    instance-of v3, v0, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v3, :cond_2

    move-object v3, v0

    check-cast v3, Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {v3}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v3

    const/16 v4, 0x47

    if-eq v3, v4, :cond_2

    move-object v3, v0

    check-cast v3, Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {v3}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v3

    const/16 v4, 0x50

    if-eq v3, v4, :cond_2

    .line 668
    const v2, 0x7f0b00ed

    .restart local v2    # "titleId":I
    goto :goto_0

    .line 669
    .end local v2    # "titleId":I
    :cond_2
    instance-of v3, v0, Lcom/google/android/music/medialist/SharedSongList;

    if-eqz v3, :cond_3

    .line 671
    const/4 v2, 0x0

    .restart local v2    # "titleId":I
    goto :goto_0

    .line 677
    .end local v2    # "titleId":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->canRemoveFromLibrary()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 679
    const v2, 0x7f0b0243

    .restart local v2    # "titleId":I
    goto :goto_0

    .line 680
    .end local v2    # "titleId":I
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v3

    if-nez v3, :cond_5

    .line 682
    const v2, 0x7f0b0247

    .restart local v2    # "titleId":I
    goto :goto_0

    .line 685
    .end local v2    # "titleId":I
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "titleId":I
    goto :goto_0

    .line 688
    .end local v0    # "contextList":Lcom/google/android/music/medialist/MediaList;
    .end local v2    # "titleId":I
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_7

    .line 689
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 706
    const-string v3, "DocumentMenuHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected playlist type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const/4 v2, 0x0

    .restart local v2    # "titleId":I
    goto :goto_0

    .line 692
    .end local v2    # "titleId":I
    :sswitch_0
    const v2, 0x7f0b0247

    .line 693
    .restart local v2    # "titleId":I
    goto :goto_0

    .line 695
    .end local v2    # "titleId":I
    :sswitch_1
    const v2, 0x7f0b0330

    .line 696
    .restart local v2    # "titleId":I
    goto/16 :goto_0

    .line 703
    .end local v2    # "titleId":I
    :sswitch_2
    const/4 v2, 0x0

    .line 704
    .restart local v2    # "titleId":I
    goto/16 :goto_0

    .line 709
    .end local v2    # "titleId":I
    :cond_7
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_9

    .line 712
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioRemoteId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 713
    const/4 v2, 0x0

    .restart local v2    # "titleId":I
    goto/16 :goto_0

    .line 715
    .end local v2    # "titleId":I
    :cond_8
    const v2, 0x7f0b0247

    .restart local v2    # "titleId":I
    goto/16 :goto_0

    .line 718
    .end local v2    # "titleId":I
    :cond_9
    const v2, 0x7f0b0247

    .restart local v2    # "titleId":I
    goto/16 :goto_0

    .line 689
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x32 -> :sswitch_2
        0x33 -> :sswitch_2
        0x3c -> :sswitch_2
        0x46 -> :sswitch_2
        0x47 -> :sswitch_1
        0x50 -> :sswitch_2
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method protected addDismissEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 831
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->isOnMainstage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 832
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 833
    .local v0, "dismiss":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 835
    .end local v0    # "dismiss":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;
    :cond_0
    return-void
.end method

.method protected addEditPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 563
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 564
    .local v0, "editPlaylist":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    return-void
.end method

.method protected addFindVideo(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 612
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FindVideo;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FindVideo;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 613
    .local v0, "findVideo":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FindVideo;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 614
    return-void
.end method

.method protected addFollowSharedPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->canFollow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 618
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FollowPlaylist;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FollowPlaylist;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 619
    .local v0, "followPlaylist":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FollowPlaylist;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621
    .end local v0    # "followPlaylist":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$FollowPlaylist;
    :cond_0
    return-void
.end method

.method protected addGoToAlbum(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 768
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->shouldHideGoToAlbum()Z

    move-result v1

    if-nez v1, :cond_0

    .line 769
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToAlbum;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 770
    .local v0, "goToAlbum":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 772
    .end local v0    # "goToAlbum":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method protected addGoToArtist(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 775
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->shouldHideGoToArtist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 776
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 777
    .local v0, "goToArtist":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 779
    .end local v0    # "goToArtist":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method public addMenuEntries(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    sget-boolean v3, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->LOGV:Z

    if-eqz v3, :cond_0

    .line 296
    const-string v3, "DocumentMenuHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Displaying menu for: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 301
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_2

    .line 302
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 303
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShuffleEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 304
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 305
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 306
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 307
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addKeepOnDeviceEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 308
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addGoToArtist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 309
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToMyLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 310
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addRemoveFromLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 311
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShareAlbumEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 312
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShopArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 313
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addBuyEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 314
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addDismissEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 392
    :cond_1
    :goto_0
    return-void

    .line 315
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_4

    .line 319
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistId()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    const/4 v0, 0x1

    .line 320
    .local v0, "isUnknownArtist":Z
    :goto_1
    if-nez v0, :cond_1

    .line 321
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 322
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShopArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 323
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShareArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 324
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShuffleArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    goto :goto_0

    .line 319
    .end local v0    # "isUnknownArtist":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 326
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_b

    .line 327
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v1

    .line 329
    .local v1, "playlistType":I
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v3, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->isRadioSupportedForPlaylistType(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 330
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 334
    :cond_5
    const/16 v3, 0x32

    if-eq v1, v3, :cond_6

    .line 335
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShuffleEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 336
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 337
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 341
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v3

    if-nez v3, :cond_8

    if-eqz v1, :cond_7

    const/16 v3, 0x64

    if-eq v1, v3, :cond_7

    const/16 v3, 0x50

    if-eq v1, v3, :cond_7

    const/16 v3, 0x47

    if-ne v1, v3, :cond_8

    .line 346
    :cond_7
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addKeepOnDeviceEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 348
    :cond_8
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 349
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v3

    if-nez v3, :cond_9

    .line 350
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addEditPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 352
    :cond_9
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addSharePlaylistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 353
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addDeleteEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 354
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v3

    const/16 v4, 0x46

    if-ne v3, v4, :cond_a

    .line 355
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addFollowSharedPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 356
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addKeepOnDeviceEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 358
    :cond_a
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addDismissEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 359
    .end local v1    # "playlistType":I
    :cond_b
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_c

    .line 360
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addStartVideo(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 361
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 362
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 363
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 364
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToPlaylist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 365
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addGoToArtist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 366
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addGoToAlbum(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 367
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToMyLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 368
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addDeleteEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 371
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShareTrackEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 372
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addShopArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 373
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addBuyEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 374
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isFindVideoEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 375
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addFindVideo(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 377
    :cond_c
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_d

    .line 379
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isShowingMyLibrary()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 380
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 381
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addAddToQueueEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 383
    :cond_d
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_e

    .line 384
    invoke-direct {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addKeepOnDeviceEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 385
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addGoToArtist(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 386
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addGoToAlbum(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 387
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addDeleteEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    .line 388
    invoke-virtual {p0, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addDismissEntry(Landroid/content/res/Resources;Ljava/util/List;)V

    goto/16 :goto_0

    .line 390
    :cond_e
    const-string v3, "DocumentMenuHandler"

    const-string v4, "Unexpected doc type"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected addPlayNextEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 625
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 626
    .local v0, "playNext":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 628
    .end local v0    # "playNext":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$PlayNext;
    :cond_0
    return-void
.end method

.method protected addRemoveFromLibraryEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 728
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->canRemoveFromLibrary()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 729
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$RemoveFromMyLibrary;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$RemoveFromMyLibrary;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 730
    .local v0, "removeFromMyLibrary":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 732
    .end local v0    # "removeFromMyLibrary":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method protected addShareAlbumEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 750
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 751
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 753
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 754
    .local v1, "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 756
    .end local v1    # "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method protected addShareArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 759
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 760
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 762
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 763
    .local v1, "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 765
    .end local v1    # "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method protected addSharePlaylistEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 568
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isSharePlaylistAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 569
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$SharePlaylist;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 570
    .local v0, "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    .end local v0    # "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method protected addShareTrackEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 735
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 736
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 737
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 738
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 739
    .local v1, "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 747
    .end local v1    # "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    :goto_0
    return-void

    .line 742
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongStoreId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/music/GPlusShareActivity;->isSharingSupported(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 743
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Share;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 744
    .restart local v1    # "share":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected addShopArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 822
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 823
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x1

    .line 824
    .local v0, "isFree":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 825
    new-instance v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShopArtist;

    invoke-direct {v2, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShopArtist;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 826
    .local v2, "shopArtist":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShopArtist;
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 828
    .end local v2    # "shopArtist":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShopArtist;
    :cond_0
    return-void

    .line 823
    .end local v0    # "isFree":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected addShuffleArtistEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 806
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 807
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v2

    if-nez v2, :cond_0

    .line 811
    :goto_0
    return-void

    .line 809
    :cond_0
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 810
    .local v1, "shuffleArtist":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected addShuffleEntry(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 653
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 654
    .local v0, "shuffle":Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 655
    return-void
.end method

.method protected addStartRadio(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 782
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 783
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v2

    if-nez v2, :cond_0

    .line 793
    :goto_0
    return-void

    .line 787
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 788
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartRadio;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 792
    .local v1, "startRadio":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :goto_1
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 790
    .end local v1    # "startRadio":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_1
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartInstantMix;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartInstantMix;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .restart local v1    # "startRadio":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    goto :goto_1
.end method

.method protected addStartVideo(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 796
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 798
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isVsAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 800
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartVideo;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$StartVideo;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V

    .line 801
    .local v1, "startVideo":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 803
    .end local v1    # "startVideo":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    :cond_0
    return-void
.end method

.method public dismissPopupMenu()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->dismiss()V

    .line 162
    :cond_0
    return-void
.end method

.method protected isSharePlaylistAvailable()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 575
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    .line 576
    .local v0, "manager":Lcom/google/android/music/ui/UIStateManager;
    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 578
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 592
    :cond_0
    :goto_0
    return v3

    .line 581
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v1

    .line 582
    .local v1, "playlistType":I
    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/model/Document;->getSourceAccount()I

    move-result v4

    if-eqz v4, :cond_0

    .line 587
    :cond_2
    if-eqz v1, :cond_3

    const/16 v4, 0x46

    if-eq v1, v4, :cond_3

    const/16 v4, 0x47

    if-ne v1, v4, :cond_0

    .line 590
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    .line 284
    return-void
.end method

.method public showPopupMenu(Landroid/view/View;)V
    .locals 3
    .param p1, "anchor"    # Landroid/view/View;

    .prologue
    .line 168
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    .line 170
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->addSpinnerItem()V

    .line 171
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->show()V

    .line 174
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 279
    return-void
.end method
