.class public Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSharedPlaylistEntriesRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public mMaxResults:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "maxResults"
    .end annotation
.end field

.field public mSharePlaylistToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "shareToken"
    .end annotation
.end field

.field public mStartToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "startToken"
    .end annotation
.end field

.field public mUpdateMin:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "updatedMin"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
