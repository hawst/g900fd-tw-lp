.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceSignupPageViewInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;


# instance fields
.field public deviceSignupStep:I

.field public signupFlowEntryPoint:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2637
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 2638
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2650
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    .line 2653
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    .line 2638
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2668
    const/4 v0, 0x0

    .line 2669
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    if-eq v1, v3, :cond_0

    .line 2670
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2673
    :cond_0
    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    if-eq v1, v3, :cond_1

    .line 2674
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2677
    :cond_1
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->cachedSize:I

    .line 2678
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2686
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2687
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2691
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2692
    :sswitch_0
    return-object p0

    .line 2697
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2698
    .local v1, "temp":I
    if-eqz v1, :cond_1

    if-eq v1, v4, :cond_1

    if-eq v1, v5, :cond_1

    if-eq v1, v6, :cond_1

    if-ne v1, v7, :cond_2

    .line 2703
    :cond_1
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    goto :goto_0

    .line 2705
    :cond_2
    iput v3, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    goto :goto_0

    .line 2710
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2711
    .restart local v1    # "temp":I
    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_3

    if-eq v1, v5, :cond_3

    if-eq v1, v6, :cond_3

    if-eq v1, v7, :cond_3

    const/4 v2, 0x5

    if-ne v1, v2, :cond_4

    .line 2717
    :cond_3
    iput v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    goto :goto_0

    .line 2719
    :cond_4
    iput v3, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    goto :goto_0

    .line 2687
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2634
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v2, -0x80000000

    .line 2658
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    if-eq v0, v2, :cond_0

    .line 2659
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2661
    :cond_0
    iget v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    if-eq v0, v2, :cond_1

    .line 2662
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2664
    :cond_1
    return-void
.end method
