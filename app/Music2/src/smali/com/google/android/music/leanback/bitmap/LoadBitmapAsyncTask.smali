.class public abstract Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;
.super Landroid/os/AsyncTask;
.source "LoadBitmapAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field private static sOnIdleListener:Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

.field private static sTaskCount:I


# instance fields
.field private final mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

.field private final mCancelledSemaphore:Ljava/util/concurrent/Semaphore;

.field private final mContext:Landroid/content/Context;

.field private final mCropToSquare:Z

.field private final mHeight:I

.field private final mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bitmapGettersGetter"    # Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "cropToSquare"    # Z

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mContext:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    .line 47
    iput p3, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mWidth:I

    .line 48
    iput p4, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mHeight:I

    .line 49
    iput-boolean p5, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mCropToSquare:Z

    .line 50
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mCancelledSemaphore:Ljava/util/concurrent/Semaphore;

    .line 51
    return-void
.end method

.method public static getTaskCount()I
    .locals 1

    .prologue
    .line 134
    sget v0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sTaskCount:I

    return v0
.end method

.method private releaseRef()V
    .locals 1

    .prologue
    .line 143
    sget v0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sTaskCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sTaskCount:I

    if-nez v0, :cond_0

    .line 145
    sget-object v0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sOnIdleListener:Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

    if-eqz v0, :cond_0

    .line 146
    sget-object v0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sOnIdleListener:Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

    invoke-interface {v0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;->onIdle()V

    .line 149
    :cond_0
    return-void
.end method

.method public static setOnIdleListener(Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

    .prologue
    .line 36
    sput-object p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sOnIdleListener:Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;

    .line 37
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 119
    :goto_0
    return-object v0

    .line 59
    :cond_0
    const-wide/16 v10, 0x1f4

    .line 60
    .local v10, "retryTimeout":J
    const/4 v8, 0x0

    .line 61
    .local v8, "retryCount":I
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    invoke-interface {v0}, Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v1

    .line 62
    .local v1, "bitmapGetters":[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    :goto_1
    if-eqz v1, :cond_1

    array-length v0, v1

    if-nez v0, :cond_6

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_6

    .line 65
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mCancelledSemaphore:Ljava/util/concurrent/Semaphore;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v10, v11, v2}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v6

    .line 70
    .local v6, "e1":Ljava/lang/InterruptedException;
    const/4 v0, 0x0

    goto :goto_0

    .line 76
    .end local v6    # "e1":Ljava/lang/InterruptedException;
    :cond_2
    const-wide/16 v2, 0xfa0

    add-long v12, v10, v10

    invoke-static {v2, v3, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    .line 77
    add-int/lit8 v8, v8, 0x1

    .line 78
    const/4 v0, 0x5

    if-le v8, v0, :cond_3

    .line 79
    const-string v0, "LoadBitmapAsyncTask"

    const-string v2, "Getters retry count exceeded"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    if-le v8, v0, :cond_5

    .line 82
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    invoke-interface {v0}, Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v1

    goto :goto_1

    .line 87
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 88
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :cond_7
    const-wide/16 v10, 0x1f4

    .line 92
    const/4 v8, 0x0

    .line 93
    const/4 v5, 0x0

    .line 94
    .local v5, "withFiltering":Z
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mWidth:I

    iget v3, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mHeight:I

    iget-boolean v4, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mCropToSquare:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;[Lcom/google/android/music/leanback/bitmap/BitmapGetter;IIZZ)Landroid/util/Pair;

    move-result-object v7

    .line 96
    .local v7, "results":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Ljava/lang/Boolean;>;"
    :goto_2
    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v0, :cond_c

    iget-object v0, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_c

    .line 99
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mCancelledSemaphore:Ljava/util/concurrent/Semaphore;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v10, v11, v2}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    if-eqz v0, :cond_8

    .line 100
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 102
    :catch_1
    move-exception v6

    .line 104
    .restart local v6    # "e1":Ljava/lang/InterruptedException;
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 107
    .end local v6    # "e1":Ljava/lang/InterruptedException;
    :cond_8
    const-wide/16 v2, 0xfa0

    add-long v12, v10, v10

    invoke-static {v2, v3, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    .line 108
    add-int/lit8 v8, v8, 0x1

    .line 109
    const/4 v0, 0x5

    if-le v8, v0, :cond_9

    .line 110
    const-string v0, "LoadBitmapAsyncTask"

    const-string v2, "getBitamp retry count exceeded"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x5

    if-le v8, v0, :cond_b

    .line 113
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 115
    :cond_b
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mWidth:I

    iget v3, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mHeight:I

    iget-boolean v4, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mCropToSquare:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;[Lcom/google/android/music/leanback/bitmap/BitmapGetter;IIZZ)Landroid/util/Pair;

    move-result-object v7

    goto :goto_2

    .line 119
    :cond_c
    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->mCancelledSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 130
    invoke-direct {p0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->releaseRef()V

    .line 131
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->onCancelled(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->releaseRef()V

    .line 140
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 124
    sget v0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sTaskCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask;->sTaskCount:I

    .line 125
    return-void
.end method
