.class public abstract Lcom/google/android/music/ui/SearchClustersFragment;
.super Lcom/google/android/music/ui/BaseClusterListFragment;
.source "SearchClustersFragment.java"


# instance fields
.field protected final mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field protected mSearchString:Ljava/lang/String;

.field private mSearchView:Landroid/support/v7/widget/SearchView;

.field private mSuggestedQuery:Ljava/lang/String;

.field private mSuggestedQueryTextView:Landroid/widget/TextView;

.field private mSuggestedQueryTextWrapperView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 59
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/SearchClustersFragment;->init(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/SearchClustersFragment;)Landroid/support/v7/widget/SearchView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SearchClustersFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchView:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/SearchClustersFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SearchClustersFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/SearchClustersFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SearchClustersFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextWrapperView:Landroid/view/View;

    return-object v0
.end method

.method private getSuggestedQuery()V
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/google/android/music/ui/SearchClustersFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/SearchClustersFragment$3;-><init>(Lcom/google/android/music/ui/SearchClustersFragment;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 216
    return-void
.end method


# virtual methods
.method protected getCardsContextMenuDelegate()Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    return-object v0
.end method

.method protected getClusterProjection(I)[Ljava/lang/String;
    .locals 1
    .param p1, "clusterIndex"    # I

    .prologue
    .line 234
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->CURSOR_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method protected init(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    .line 165
    return-void
.end method

.method protected initEmptyScreen()V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->initEmptyScreen()V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->setupSearchEmptyScreen()V

    .line 241
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f0e0270

    .line 88
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseClusterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0400ef

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 91
    .local v1, "suggestedQueryView":Landroid/view/View;
    const v3, 0x7f0e026f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextWrapperView:Landroid/view/View;

    .line 93
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextView:Landroid/widget/TextView;

    .line 95
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 96
    iget-object v3, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextWrapperView:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 98
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/music/ui/SearchClustersFragment$1;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/SearchClustersFragment$1;-><init>(Lcom/google/android/music/ui/SearchClustersFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "restore":Z
    if-eqz p1, :cond_0

    .line 113
    const-string v3, "com.google.android.music.ui.searchclusterfragment.searchstring"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    .line 114
    const-string v3, "com.google.android.music.ui.searchclusterfragment.suggestedsearch"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "suggestedSearch":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/SearchClustersFragment;->setSuggestedQuery(Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x1

    .line 119
    .end local v2    # "suggestedSearch":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 120
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Search string is not initialized"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 123
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/SearchClustersFragment;->startLoading(Z)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    new-instance v4, Lcom/google/android/music/ui/SearchClustersFragment$2;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/SearchClustersFragment$2;-><init>(Lcom/google/android/music/ui/SearchClustersFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 143
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseClusterListFragment;->onAttach(Landroid/app/Activity;)V

    .line 71
    instance-of v0, p1, Lcom/google/android/music/ui/SearchActivity;

    if-eqz v0, :cond_0

    .line 72
    check-cast p1, Lcom/google/android/music/ui/SearchActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-virtual {p1}, Lcom/google/android/music/ui/SearchActivity;->getSearchView()Landroid/support/v7/widget/SearchView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchView:Landroid/support/v7/widget/SearchView;

    .line 76
    :goto_0
    return-void

    .line 74
    .restart local p1    # "activity":Landroid/app/Activity;
    :cond_0
    const-string v0, "SearchClustersFragment"

    const-string v1, "attached activity was not a SearchActivity!"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->onDetach()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchView:Landroid/support/v7/widget/SearchView;

    .line 83
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseClusterListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 149
    const-string v0, "com.google.android.music.ui.searchclusterfragment.searchstring"

    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_0
    const-string v0, "com.google.android.music.ui.searchclusterfragment.suggestedsearch"

    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public setSearchString(Ljava/lang/String;)V
    .locals 1
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    .line 173
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/SearchClustersFragment;->setSuggestedQuery(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->startLoading()V

    .line 175
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->setupSearchEmptyScreen()V

    .line 176
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->getSuggestedQuery()V

    .line 177
    return-void
.end method

.method public setSuggestedQuery(Ljava/lang/String;)V
    .locals 5
    .param p1, "suggestedQuery"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 219
    iput-object p1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQuery:Ljava/lang/String;

    .line 220
    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextWrapperView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 229
    :goto_0
    return-void

    .line 223
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQuery:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "htmlSuggestion":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextView:Landroid/widget/TextView;

    const v2, 0x7f0b0387

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/ui/SearchClustersFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextWrapperView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setupSearchEmptyScreen()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 245
    iget-object v2, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/SearchClustersFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 247
    .local v0, "isEmptyQuery":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 248
    const v2, 0x7f0200f5

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/SearchClustersFragment;->setEmptyImageView(I)V

    .line 249
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchClustersFragment;->clearEmptyScreenText()V

    .line 254
    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/SearchClustersFragment;->setEmptyScreenPadding(Z)V

    .line 255
    return-void

    .line 245
    .end local v0    # "isEmptyQuery":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 251
    .restart local v0    # "isEmptyQuery":Z
    :cond_2
    const v2, 0x7f0200f4

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/SearchClustersFragment;->setEmptyImageView(I)V

    .line 252
    const v2, 0x7f0b02aa

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/SearchClustersFragment;->setEmptyScreenText(I)V

    goto :goto_1
.end method
