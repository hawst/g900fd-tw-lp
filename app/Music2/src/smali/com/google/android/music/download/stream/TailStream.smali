.class public Lcom/google/android/music/download/stream/TailStream;
.super Ljava/io/InputStream;
.source "TailStream.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private isClosed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mContext:Landroid/content/Context;

.field private mInput:Lcom/google/android/music/download/stream/StreamingInput;

.field private volatile mStartReadPoint:J

.field private final mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

.field private mTailFillCnt:I

.field private final mTailFillSize:I

.field private mTotalRead:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/stream/TailStream;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/stream/StreamingContent;J)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "streamingContent"    # Lcom/google/android/music/download/stream/StreamingContent;
    .param p3, "startRangeByte"    # J

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 38
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/download/stream/TailStream;->mTotalRead:J

    .line 39
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillCnt:I

    .line 45
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->isClosed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 48
    sget-boolean v1, Lcom/google/android/music/download/stream/TailStream;->LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "TailStream"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New TailStream for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/download/stream/TailStream;->mContext:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 51
    iput-wide p3, p0, Lcom/google/android/music/download/stream/TailStream;->mStartReadPoint:J

    .line 53
    const/high16 v0, 0x10000

    .line 55
    .local v0, "defaultPaddingSize":I
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    const/4 v0, 0x0

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_local_http_stuffing"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillSize:I

    .line 62
    return-void
.end method

.method private readFromFile([BII)I
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    iget-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    if-nez v1, :cond_1

    .line 160
    iget-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    iget-wide v2, p0, Lcom/google/android/music/download/stream/TailStream;->mStartReadPoint:J

    iget-wide v4, p0, Lcom/google/android/music/download/stream/TailStream;->mTotalRead:J

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/download/stream/StreamingContent;->getStreamingInput(J)Lcom/google/android/music/download/stream/StreamingInput;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    .line 161
    iget-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    if-nez v1, :cond_1

    .line 162
    sget-boolean v1, Lcom/google/android/music/download/stream/TailStream;->LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "TailStream"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") returning -1 since the file location doesn\'t exists"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_0
    const/4 v0, -0x1

    .line 178
    :goto_0
    return v0

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/music/download/stream/StreamingInput;->read([BII)I

    move-result v0

    .line 169
    .local v0, "read":I
    if-lez v0, :cond_2

    .line 170
    iget-wide v2, p0, Lcom/google/android/music/download/stream/TailStream;->mTotalRead:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/music/download/stream/TailStream;->mTotalRead:J

    goto :goto_0

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    invoke-interface {v1}, Lcom/google/android/music/download/stream/StreamingInput;->close()V

    .line 176
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    invoke-interface {v0}, Lcom/google/android/music/download/stream/StreamingInput;->close()V

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/stream/TailStream;->mInput:Lcom/google/android/music/download/stream/StreamingInput;

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/download/stream/TailStream;->isClosed:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 188
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/TailStream;->isClosed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 189
    iget-object v0, p0, Lcom/google/android/music/download/stream/TailStream;->isClosed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 190
    monitor-exit v1

    .line 191
    return-void

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public read()I
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public read([BII)I
    .locals 12
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 76
    iget-object v6, p0, Lcom/google/android/music/download/stream/TailStream;->isClosed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 77
    sget-boolean v5, Lcom/google/android/music/download/stream/TailStream;->LOGV:Z

    if-eqz v5, :cond_0

    const-string v5, "TailStream"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") returning -1 since we were closed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v2, v4

    .line 147
    :goto_0
    return v2

    .line 91
    :cond_1
    iget v6, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillCnt:I

    if-ltz v6, :cond_4

    .line 103
    iget v6, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillCnt:I

    invoke-static {p3, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 104
    .local v2, "numtoread":I
    if-gtz v2, :cond_2

    move v2, v4

    .line 105
    goto :goto_0

    .line 107
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_3

    .line 108
    add-int v4, p2, v1

    aput-byte v5, p1, v4

    .line 107
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 110
    :cond_3
    iget v4, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillCnt:I

    sub-int/2addr v4, v2

    iput v4, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillCnt:I

    goto :goto_0

    .line 116
    .end local v1    # "i":I
    .end local v2    # "numtoread":I
    :cond_4
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    iget-wide v8, p0, Lcom/google/android/music/download/stream/TailStream;->mStartReadPoint:J

    iget-wide v10, p0, Lcom/google/android/music/download/stream/TailStream;->mTotalRead:J

    add-long/2addr v8, v10

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Lcom/google/android/music/download/stream/StreamingContent;->waitForData(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    iget-object v6, p0, Lcom/google/android/music/download/stream/TailStream;->isClosed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 124
    sget-boolean v5, Lcom/google/android/music/download/stream/TailStream;->LOGV:Z

    if-eqz v5, :cond_5

    const-string v5, "TailStream"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") returning -1 since we were closed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move v2, v4

    .line 125
    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-boolean v5, Lcom/google/android/music/download/stream/TailStream;->LOGV:Z

    if-eqz v5, :cond_6

    const-string v5, "TailStream"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TailStream for: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " interrupted"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v2, v4

    .line 119
    goto :goto_0

    .line 128
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_7
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/download/stream/TailStream;->readFromFile([BII)I

    move-result v3

    .line 129
    .local v3, "read":I
    if-gez v3, :cond_a

    .line 130
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/download/stream/TailStream;->readFromFile([BII)I

    move-result v3

    .line 131
    if-gez v3, :cond_a

    iget-object v6, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v6}, Lcom/google/android/music/download/stream/StreamingContent;->isFinished()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 133
    sget-boolean v6, Lcom/google/android/music/download/stream/TailStream;->LOGV:Z

    if-eqz v6, :cond_8

    .line 134
    const-string v6, "TailStream"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "read("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/google/android/music/download/stream/TailStream;->mTotalRead:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; returning -1"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_8
    iget v6, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillCnt:I

    if-gez v6, :cond_9

    iget v6, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillSize:I

    if-lez v6, :cond_9

    .line 140
    iget v4, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillSize:I

    iput v4, p0, Lcom/google/android/music/download/stream/TailStream;->mTailFillCnt:I

    move v2, v5

    .line 141
    goto/16 :goto_0

    :cond_9
    move v2, v4

    .line 143
    goto/16 :goto_0

    :cond_a
    move v2, v3

    .line 147
    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/music/download/stream/TailStream;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingContent;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
