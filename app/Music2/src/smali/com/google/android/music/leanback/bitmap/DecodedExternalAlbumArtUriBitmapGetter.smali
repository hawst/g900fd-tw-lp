.class public Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;
.super Ljava/lang/Object;
.source "DecodedExternalAlbumArtUriBitmapGetter.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapGetter;


# instance fields
.field private final mDecodedArtUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "decodedArtUri"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;->mDecodedArtUri:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getBitmap(Landroid/content/Context;IIZ)Landroid/util/Pair;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIZ)",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 22
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;->mDecodedArtUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 23
    .local v7, "uri":Landroid/net/Uri;
    const-string v0, "content"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 26
    :goto_0
    return-object v0

    :cond_0
    new-instance v8, Landroid/util/Pair;

    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/DecodedExternalAlbumArtUriBitmapGetter;->mDecodedArtUri:Ljava/lang/String;

    move-object v0, p1

    move v2, p2

    move v3, p3

    move v6, v4

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/utils/AlbumArtUtils;->getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v8, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v8

    goto :goto_0
.end method
