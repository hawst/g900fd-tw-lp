.class Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;
.super Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
.source "SDCardSelectorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/SDCardSelectorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DebugViewHolder"
.end annotation


# instance fields
.field mDiscoveryMethodView:Landroid/widget/TextView;

.field mMountPointView:Landroid/widget/TextView;

.field mPathView:Landroid/widget/TextView;

.field mStorageTypeView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V
    .locals 1

    .prologue
    .line 295
    iput-object p1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;Lcom/google/android/music/ui/SDCardSelectorFragment$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/SDCardSelectorFragment;Lcom/google/android/music/ui/SDCardSelectorFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/SDCardSelectorFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/SDCardSelectorFragment$1;

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V

    return-void
.end method


# virtual methods
.method createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 318
    const v1, 0x7f0400dc

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 320
    .local v0, "v":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->bindBaseViews(Landroid/view/View;)V

    .line 321
    const v1, 0x7f0e025c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mPathView:Landroid/widget/TextView;

    .line 322
    const v1, 0x7f0e025d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mMountPointView:Landroid/widget/TextView;

    .line 323
    const v1, 0x7f0e025e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mDiscoveryMethodView:Landroid/widget/TextView;

    .line 324
    const v1, 0x7f0e025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mStorageTypeView:Landroid/widget/TextView;

    .line 325
    invoke-virtual {v0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 326
    return-object v0
.end method

.method formatStorageSize(Lcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;
    .locals 9
    .param p1, "loc"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 331
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-static {v4}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getVolumeInformation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    move-result-object v3

    .line 333
    .local v3, "volInfo":Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->formatStorageSize(Lcom/google/android/music/download/cache/CacheLocation;)Ljava/lang/String;

    move-result-object v0

    .line 334
    .local v0, "parent":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-static {v4}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->calculateMusicUsedSpace(Lcom/google/android/music/download/cache/CacheLocation;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnitStrings:[Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$1200(Lcom/google/android/music/ui/SDCardSelectorFragment;)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/music/utils/IOUtils;->readableFileSize(J[Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 336
    .local v2, "used":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v3}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->getTotalSpace()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mUnitStrings:[Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$1200(Lcom/google/android/music/ui/SDCardSelectorFragment;)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/music/utils/IOUtils;->readableFileSize(J[Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 338
    .local v1, "total":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (%s %s total, %s %s used for Music)"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-object v8, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method update(Lcom/google/android/music/download/cache/CacheLocation;)V
    .locals 6
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    .line 303
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->update(Lcom/google/android/music/download/cache/CacheLocation;)V

    .line 304
    const-string v0, ""

    .line 305
    .local v0, "emulatedString":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationManager:Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-static {v2}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$000(Lcom/google/android/music/ui/SDCardSelectorFragment;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getStorageMetadata(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;

    move-result-object v1

    .line 306
    .local v1, "sm":Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    if-eqz v1, :cond_0

    .line 307
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mDiscoveryMethodView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Discovered by: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v1}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->getDiscoveryMethods()Ljava/util/EnumSet;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mMountPointView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Mounted at: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->getMountPoint()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    invoke-virtual {v1}, Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;->isEmulated()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, " storage is Emulated"

    .line 312
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mPathView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Path to cache: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v2, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;->mStorageTypeView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Storage Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    return-void

    .line 310
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
