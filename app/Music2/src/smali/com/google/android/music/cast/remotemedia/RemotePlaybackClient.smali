.class public interface abstract Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
.super Ljava/lang/Object;
.source "RemotePlaybackClient.java"


# virtual methods
.method public abstract getPosition()J
.end method

.method public abstract isPreparing()Z
.end method

.method public abstract isQueueLoaded()Z
.end method

.method public abstract isSessionInitialized()Z
.end method

.method public abstract isStreaming()Z
.end method

.method public abstract joinSession(Ljava/lang/String;)V
.end method

.method public abstract leaveSession()V
.end method

.method public abstract loadCloudQueue(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract refreshCloudQueue()V
.end method

.method public abstract seek(Ljava/lang/String;Ljava/lang/String;J)V
.end method

.method public abstract setVolume(D)V
.end method

.method public abstract skipToItem(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
.end method
