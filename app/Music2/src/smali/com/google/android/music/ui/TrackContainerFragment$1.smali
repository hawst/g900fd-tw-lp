.class Lcom/google/android/music/ui/TrackContainerFragment$1;
.super Ljava/lang/Object;
.source "TrackContainerFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TrackContainerFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/TrackContainerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/TrackContainerFragment;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 7
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    const/16 v6, 0xff

    .line 123
    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;
    invoke-static {v4}, Lcom/google/android/music/ui/TrackContainerFragment;->access$000(Lcom/google/android/music/ui/TrackContainerFragment;)Lcom/google/android/music/ui/ContainerHeaderView;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3, p4}, Lcom/google/android/music/ui/ContainerHeaderView;->onScroll(Landroid/widget/AbsListView;III)V

    .line 125
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 126
    .local v3, "v":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;
    invoke-static {v4}, Lcom/google/android/music/ui/TrackContainerFragment;->access$000(Lcom/google/android/music/ui/TrackContainerFragment;)Lcom/google/android/music/ui/ContainerHeaderView;

    move-result-object v4

    if-ne v3, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;
    invoke-static {v4}, Lcom/google/android/music/ui/TrackContainerFragment;->access$000(Lcom/google/android/music/ui/TrackContainerFragment;)Lcom/google/android/music/ui/ContainerHeaderView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/ContainerHeaderView;->isArtistArtShown()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/TrackContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v4

    if-nez v4, :cond_2

    .line 128
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v4

    neg-int v1, v4

    .line 129
    .local v1, "offset":I
    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;
    invoke-static {v4}, Lcom/google/android/music/ui/TrackContainerFragment;->access$000(Lcom/google/android/music/ui/TrackContainerFragment;)Lcom/google/android/music/ui/ContainerHeaderView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/ContainerHeaderView;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/TrackContainerFragment;->mHeader:Lcom/google/android/music/ui/ContainerHeaderView;
    invoke-static {v5}, Lcom/google/android/music/ui/TrackContainerFragment;->access$000(Lcom/google/android/music/ui/TrackContainerFragment;)Lcom/google/android/music/ui/ContainerHeaderView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/ui/ContainerHeaderView;->getAlbumArtHeight()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v2, v4

    .line 130
    .local v2, "spaceAboveAlbumArt":F
    int-to-float v4, v1

    div-float/2addr v4, v2

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v4, v5

    float-to-int v0, v4

    .line 131
    .local v0, "alpha":I
    if-le v0, v6, :cond_0

    const/16 v0, 0xff

    .line 132
    :cond_0
    if-gez v0, :cond_1

    const/4 v0, 0x0

    .line 133
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/TrackContainerFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/google/android/music/ui/ActionBarController;->setActionBarAlpha(I)V

    .line 138
    .end local v0    # "alpha":I
    .end local v1    # "offset":I
    .end local v2    # "spaceAboveAlbumArt":F
    :goto_0
    return-void

    .line 136
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/ui/TrackContainerFragment$1;->this$0:Lcom/google/android/music/ui/TrackContainerFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/TrackContainerFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v4

    invoke-interface {v4, v6}, Lcom/google/android/music/ui/ActionBarController;->setActionBarAlpha(I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 141
    return-void
.end method
