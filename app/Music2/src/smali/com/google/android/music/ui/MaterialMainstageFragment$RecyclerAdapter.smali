.class Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;
.super Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;
.source "MaterialMainstageFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialMainstageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecyclerAdapter"
.end annotation


# instance fields
.field private final STABLE_ID_EXPLORE_CARD:J

.field private final STABLE_ID_HISTORY_CARD:J

.field private final STABLE_ID_IFL_CARD:J

.field private final STABLE_ID_RECOMMENDATIONS_HEADER:J

.field private final STABLE_ID_SITUATIONS_CARD:J

.field private final STABLE_ID_TUTORIAL_CARD:J

.field private final VIEW_TYPE_EXPLORE_CARD:I

.field private final VIEW_TYPE_FAKE_CLUSTER_HEADER:I

.field private final VIEW_TYPE_HISTORY_CARD:I

.field private final VIEW_TYPE_IFL_CARD:I

.field private final VIEW_TYPE_RECOMMENDATION_1X1_CARD:I

.field private final VIEW_TYPE_RECOMMENDATION_2X1_CARD:I

.field private final VIEW_TYPE_SITUATIONS_CARD:I

.field private final VIEW_TYPE_TUTORIAL_CARD:I

.field private final mContext:Landroid/content/Context;

.field private final mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

.field private mMainStageCursorLoaded:Z

.field private mMainstageCursor:Landroid/database/Cursor;

.field private mRecentCardCursor:Landroid/database/Cursor;

.field private mRecentCardCursorLoaded:Z

.field private mSituationCard:Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

.field private mSituationCardHeaderCursor:Landroid/database/Cursor;

.field private mSituationCardHeaderCursorLoaded:Z

.field private mTopSituationsCursor:Landroid/database/Cursor;

.field private mTopSituationsCursorLoaded:Z


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/MaterialMainstageFragment;)V
    .locals 3
    .param p1, "fragment"    # Lcom/google/android/music/ui/MaterialMainstageFragment;

    .prologue
    const/4 v2, 0x0

    .line 308
    invoke-direct {p0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;-><init>()V

    .line 278
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_SITUATIONS_CARD:I

    .line 279
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_TUTORIAL_CARD:I

    .line 280
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_RECOMMENDATION_1X1_CARD:I

    .line 281
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_RECOMMENDATION_2X1_CARD:I

    .line 282
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_HISTORY_CARD:I

    .line 283
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_EXPLORE_CARD:I

    .line 284
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_FAKE_CLUSTER_HEADER:I

    .line 285
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->VIEW_TYPE_IFL_CARD:I

    .line 287
    const-wide/16 v0, -0x64

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->STABLE_ID_SITUATIONS_CARD:J

    .line 288
    const-wide/16 v0, -0x65

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->STABLE_ID_TUTORIAL_CARD:J

    .line 289
    const-wide/16 v0, -0x66

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->STABLE_ID_HISTORY_CARD:J

    .line 290
    const-wide/16 v0, -0x67

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->STABLE_ID_EXPLORE_CARD:J

    .line 291
    const-wide/16 v0, -0x68

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->STABLE_ID_RECOMMENDATIONS_HEADER:J

    .line 292
    const-wide/16 v0, -0x69

    iput-wide v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->STABLE_ID_IFL_CARD:J

    .line 299
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainStageCursorLoaded:Z

    .line 300
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursorLoaded:Z

    .line 301
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursorLoaded:Z

    .line 302
    iput-boolean v2, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mTopSituationsCursorLoaded:Z

    .line 309
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    .line 310
    invoke-virtual {p1}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    .line 311
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/MaterialMainstageFragment;Lcom/google/android/music/ui/MaterialMainstageFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/MaterialMainstageFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/MaterialMainstageFragment$1;

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;-><init>(Lcom/google/android/music/ui/MaterialMainstageFragment;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->startRecentActivity()V

    return-void
.end method

.method private bindDocument(Lcom/google/android/music/ui/PlayCardViewHolder;I)V
    .locals 4
    .param p1, "holder"    # Lcom/google/android/music/ui/PlayCardViewHolder;
    .param p2, "index"    # I

    .prologue
    .line 471
    sget-boolean v1, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v1, :cond_0

    .line 472
    const-string v1, "MusicMaterialMainstage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bindDocument: index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    :cond_0
    new-instance v1, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v1}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    iput-object v1, p1, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 475
    invoke-virtual {p0, p2}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->getItemData(I)Landroid/database/Cursor;

    move-result-object v0

    .line 476
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v1, v2, v0}, Lcom/google/android/music/ui/MainstageDocumentHelper;->buildMainstageDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 478
    return-void
.end method

.method private static cursorHasContent(Landroid/database/Cursor;)Z
    .locals 1
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 541
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setFullSpan(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 399
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;-><init>(II)V

    .line 403
    .local v0, "slp":Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->setFullSpan(Z)V

    .line 404
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 405
    return-void
.end method

.method private setupMatrixAdapterItems()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 545
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    invoke-virtual {v10}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v3

    .line 546
    .local v3, "isDownloadedOnlyMode":Z
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v4

    .line 547
    .local v4, "isOnline":Z
    if-eqz v4, :cond_3

    if-nez v3, :cond_3

    move v6, v8

    .line 549
    .local v6, "showOnlineContent":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->clearItems()V

    .line 551
    const/4 v1, 0x0

    .line 553
    .local v1, "index":I
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isConciergeListenNowEnabled()Z

    move-result v10

    if-eqz v10, :cond_4

    if-eqz v6, :cond_4

    move v7, v8

    .line 556
    .local v7, "showSituationCard":Z
    :goto_1
    if-eqz v7, :cond_0

    .line 559
    const-wide/16 v10, -0x64

    invoke-virtual {p0, v10, v11, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemId(JI)V

    .line 560
    invoke-virtual {p0, v8, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 561
    add-int/lit8 v1, v1, 0x1

    .line 565
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    # invokes: Lcom/google/android/music/ui/MaterialMainstageFragment;->initSituationsLoaders(Lcom/google/android/music/ui/MaterialMainstageFragment;)V
    invoke-static {v10}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$800(Lcom/google/android/music/ui/MaterialMainstageFragment;)V

    .line 568
    :cond_0
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowMainstageTutorialCard(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 569
    const-wide/16 v10, -0x65

    invoke-virtual {p0, v10, v11, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemId(JI)V

    .line 570
    const/4 v10, 0x2

    invoke-virtual {p0, v10, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 571
    add-int/lit8 v1, v1, 0x1

    .line 576
    :cond_1
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    invoke-static {v10}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->cursorHasContent(Landroid/database/Cursor;)Z

    move-result v5

    .line 578
    .local v5, "showHistoryCard":Z
    if-eqz v5, :cond_2

    .line 579
    const/4 v10, 0x5

    invoke-virtual {p0, v10, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 580
    const-wide/16 v10, -0x66

    invoke-virtual {p0, v10, v11, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemId(JI)V

    .line 581
    add-int/lit8 v1, v1, 0x1

    .line 584
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->shouldShowRecommendationCluster()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 588
    const/4 v10, 0x7

    invoke-virtual {p0, v10, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 589
    const-wide/16 v10, -0x68

    invoke-virtual {p0, v10, v11, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemId(JI)V

    .line 590
    add-int/lit8 v1, v1, 0x1

    .line 593
    const/16 v10, 0x8

    invoke-virtual {p0, v10, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 594
    const-wide/16 v10, -0x69

    invoke-virtual {p0, v10, v11, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemId(JI)V

    .line 595
    add-int/lit8 v1, v1, 0x1

    .line 597
    const/4 v0, 0x0

    .line 598
    .local v0, "cursorIndex":I
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    const/4 v11, -0x1

    invoke-interface {v10, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 599
    :goto_2
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 604
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    const/16 v11, 0x14

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-ne v10, v12, :cond_5

    move v2, v8

    .line 609
    .local v2, "isArtistRadio":Z
    :goto_3
    if-eqz v2, :cond_6

    .line 610
    invoke-virtual {p0, v12, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 614
    :goto_4
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    invoke-interface {v10, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {p0, v10, v11, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemId(JI)V

    .line 616
    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v10, v0, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemData(Landroid/database/Cursor;II)V

    .line 617
    add-int/lit8 v1, v1, 0x1

    .line 618
    add-int/lit8 v0, v0, 0x1

    .line 619
    goto :goto_2

    .end local v0    # "cursorIndex":I
    .end local v1    # "index":I
    .end local v2    # "isArtistRadio":Z
    .end local v5    # "showHistoryCard":Z
    .end local v6    # "showOnlineContent":Z
    .end local v7    # "showSituationCard":Z
    :cond_3
    move v6, v9

    .line 547
    goto/16 :goto_0

    .restart local v1    # "index":I
    .restart local v6    # "showOnlineContent":Z
    :cond_4
    move v7, v9

    .line 553
    goto/16 :goto_1

    .restart local v0    # "cursorIndex":I
    .restart local v5    # "showHistoryCard":Z
    .restart local v7    # "showSituationCard":Z
    :cond_5
    move v2, v9

    .line 604
    goto :goto_3

    .line 612
    .restart local v2    # "isArtistRadio":Z
    :cond_6
    const/4 v10, 0x3

    invoke-virtual {p0, v10, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    goto :goto_4

    .line 621
    .end local v2    # "isArtistRadio":Z
    :cond_7
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v8

    if-eqz v8, :cond_8

    if-eqz v6, :cond_8

    .line 622
    const-wide/16 v8, -0x67

    invoke-virtual {p0, v8, v9, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemId(JI)V

    .line 623
    const/4 v8, 0x6

    invoke-virtual {p0, v8, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setItemViewType(II)V

    .line 624
    add-int/lit8 v1, v1, 0x1

    .line 627
    .end local v0    # "cursorIndex":I
    :cond_8
    return-void
.end method

.method private shouldShowRecommendationCluster()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 635
    iget-object v6, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v1

    .line 636
    .local v1, "isDownloadedOnlyMode":Z
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v2

    .line 637
    .local v2, "isOnline":Z
    if-eqz v2, :cond_2

    if-nez v1, :cond_2

    move v3, v4

    .line 638
    .local v3, "showOnlineContent":Z
    :goto_0
    iget-object v6, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    invoke-static {v6}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->cursorHasContent(Landroid/database/Cursor;)Z

    move-result v0

    .line 642
    .local v0, "hasRecommendations":Z
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    move v5, v4

    :cond_1
    return v5

    .end local v0    # "hasRecommendations":Z
    .end local v3    # "showOnlineContent":Z
    :cond_2
    move v3, v5

    .line 637
    goto :goto_0
.end method

.method private startRecentActivity()V
    .locals 2

    .prologue
    .line 708
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/ui/RecentActivity;->newRecentActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 709
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 710
    return-void
.end method

.method private tryUpdateContent()V
    .locals 1

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainStageCursorLoaded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursorLoaded:Z

    if-eqz v0, :cond_0

    .line 521
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setupMatrixAdapterItems()V

    .line 522
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->updateEmptyViewVisibility()V

    .line 523
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->notifyDataSetChanged()V

    .line 525
    :cond_0
    return-void
.end method

.method private updateEmptyViewVisibility()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 528
    iget-boolean v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursorLoaded:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v3

    .line 530
    .local v2, "hasSituationContent":Z
    :goto_0
    iget-boolean v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursorLoaded:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v3

    .line 531
    .local v0, "hasRecentContent":Z
    :goto_1
    iget-boolean v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainStageCursorLoaded:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    move v1, v3

    .line 533
    .local v1, "hasRecommendationContent":Z
    :goto_2
    if-nez v2, :cond_3

    if-nez v0, :cond_3

    if-nez v1, :cond_3

    .line 534
    iget-object v4, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    # invokes: Lcom/google/android/music/ui/MaterialMainstageFragment;->setEmptyViewVisibility(Z)V
    invoke-static {v4, v3}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$700(Lcom/google/android/music/ui/MaterialMainstageFragment;Z)V

    .line 538
    :goto_3
    return-void

    .end local v0    # "hasRecentContent":Z
    .end local v1    # "hasRecommendationContent":Z
    .end local v2    # "hasSituationContent":Z
    :cond_0
    move v2, v4

    .line 528
    goto :goto_0

    .restart local v2    # "hasSituationContent":Z
    :cond_1
    move v0, v4

    .line 530
    goto :goto_1

    .restart local v0    # "hasRecentContent":Z
    :cond_2
    move v1, v4

    .line 531
    goto :goto_2

    .line 536
    .restart local v1    # "hasRecommendationContent":Z
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    # invokes: Lcom/google/android/music/ui/MaterialMainstageFragment;->setEmptyViewVisibility(Z)V
    invoke-static {v3, v4}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$700(Lcom/google/android/music/ui/MaterialMainstageFragment;Z)V

    goto :goto_3
.end method

.method private updateRecentActivityCard(Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;)V
    .locals 14
    .param p1, "viewHolder"    # Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;

    .prologue
    const/4 v13, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 669
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->shouldShowRecommendationCluster()Z

    move-result v11

    if-nez v11, :cond_1

    move v6, v9

    .line 670
    .local v6, "showIFLInRecent":Z
    :goto_0
    iget-object v1, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;->headerView:Lcom/google/android/music/ui/MultiRowClusterView;

    .line 673
    .local v1, "headerView":Lcom/google/android/music/ui/MultiRowClusterView;
    if-eqz v6, :cond_2

    move v7, v9

    .line 674
    .local v7, "trueRecentStart":I
    :goto_1
    iget-object v11, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 675
    .local v0, "cursorCount":I
    if-eqz v6, :cond_3

    :goto_2
    add-int/2addr v9, v0

    invoke-virtual {v1, v9}, Lcom/google/android/music/ui/MultiRowClusterView;->setShownCardCount(I)V

    .line 676
    if-eqz v6, :cond_0

    .line 677
    invoke-virtual {v1, v10}, Lcom/google/android/music/ui/MultiRowClusterView;->getCard(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-result-object v8

    .line 678
    .local v8, "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v9, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9, v8}, Lcom/google/android/music/ui/RecentCardDocumentHelper;->populateIFLPlayCard(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;)V

    .line 680
    .end local v8    # "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    move v2, v7

    .line 681
    .local v2, "i":I
    :goto_3
    invoke-virtual {v1}, Lcom/google/android/music/ui/MultiRowClusterView;->getCardCount()I

    move-result v9

    if-ge v2, v9, :cond_4

    sub-int v9, v2, v7

    if-ge v9, v0, :cond_4

    .line 683
    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/MultiRowClusterView;->getCard(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-result-object v8

    .line 684
    .restart local v8    # "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v9, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    sub-int v11, v2, v7

    iget-object v12, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    iget-object v12, v12, Lcom/google/android/music/ui/MaterialMainstageFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-static {v9, v10, v8, v11, v12}, Lcom/google/android/music/ui/RecentCardDocumentHelper;->populateRecentPlayCard(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView;ILcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 682
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .end local v0    # "cursorCount":I
    .end local v1    # "headerView":Lcom/google/android/music/ui/MultiRowClusterView;
    .end local v2    # "i":I
    .end local v6    # "showIFLInRecent":Z
    .end local v7    # "trueRecentStart":I
    .end local v8    # "view":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_1
    move v6, v10

    .line 669
    goto :goto_0

    .restart local v1    # "headerView":Lcom/google/android/music/ui/MultiRowClusterView;
    .restart local v6    # "showIFLInRecent":Z
    :cond_2
    move v7, v10

    .line 673
    goto :goto_1

    .restart local v0    # "cursorCount":I
    .restart local v7    # "trueRecentStart":I
    :cond_3
    move v9, v10

    .line 675
    goto :goto_2

    .line 687
    .restart local v2    # "i":I
    :cond_4
    iget-object v9, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b024b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 689
    .local v5, "recentTitle":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b024c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 691
    .local v4, "recentSubtitle":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b024d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 693
    .local v3, "moreButton":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v1}, Lcom/google/android/music/ui/MultiRowClusterView;->getCardCount()I

    move-result v10

    if-le v9, v10, :cond_5

    .line 694
    invoke-virtual {v1, v5, v4, v3}, Lcom/google/android/music/ui/MultiRowClusterView;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    new-instance v9, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter$2;

    invoke-direct {v9, p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter$2;-><init>(Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;)V

    invoke-virtual {v1, v9}, Lcom/google/android/music/ui/MultiRowClusterView;->setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V

    .line 705
    :goto_4
    return-void

    .line 702
    :cond_5
    invoke-virtual {v1, v5, v4, v13}, Lcom/google/android/music/ui/MultiRowClusterView;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    invoke-virtual {v1, v13}, Lcom/google/android/music/ui/MultiRowClusterView;->setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V

    goto :goto_4
.end method

.method private updateSituationCard(Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;)V
    .locals 4
    .param p1, "viewHolder"    # Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 713
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mTopSituationsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mTopSituationsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    .line 715
    :cond_0
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_1

    .line 716
    const-string v0, "MusicMaterialMainstage"

    const-string v1, "Failed to get valid header cursor"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mTopSituationsCursorLoaded:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursorLoaded:Z

    if-eqz v0, :cond_2

    .line 722
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->failureView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 723
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 736
    :cond_2
    :goto_0
    return-void

    .line 726
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursor:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 727
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->headerView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursor:Landroid/database/Cursor;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 731
    :goto_1
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->headerView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 732
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 733
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 734
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->failureView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 729
    :cond_4
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;->headerView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 17
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 409
    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    .line 466
    const-string v12, "MusicMaterialMainstage"

    const-string v13, "wrong type"

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 411
    :pswitch_0
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setFullSpan(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    move-object/from16 v11, p1

    .line 412
    check-cast v11, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

    .line 414
    .local v11, "situationCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->updateSituationCard(Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;)V

    goto :goto_0

    .end local v11    # "situationCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;
    :pswitch_1
    move-object/from16 v9, p1

    .line 418
    check-cast v9, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 419
    .local v9, "playCardViewHolder":Lcom/google/android/music/ui/PlayCardViewHolder;
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v9, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->bindDocument(Lcom/google/android/music/ui/PlayCardViewHolder;I)V

    .line 420
    iget-object v12, v9, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v13, v9, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    iget-object v14, v14, Lcom/google/android/music/ui/MaterialMainstageFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v12, v13, v14}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 422
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v12

    iget-object v13, v9, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v12, v13}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageDocumentDisplayed(Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto :goto_0

    .line 429
    .end local v9    # "playCardViewHolder":Lcom/google/android/music/ui/PlayCardViewHolder;
    :pswitch_2
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setFullSpan(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_0

    .line 433
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v12

    const/4 v13, 0x1

    if-lt v12, v13, :cond_0

    .line 434
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setFullSpan(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    move-object/from16 v10, p1

    .line 435
    check-cast v10, Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;

    .line 436
    .local v10, "recentCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->updateRecentActivityCard(Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;)V

    goto :goto_0

    .end local v10    # "recentCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;
    :pswitch_4
    move-object/from16 v6, p1

    .line 439
    check-cast v6, Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;

    .line 441
    .local v6, "exploreCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;
    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;->mMainstageExploreCardView:Landroid/view/View;
    invoke-static {v6}, Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;->access$500(Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;)Landroid/view/View;

    move-result-object v5

    .line 442
    .local v5, "exploreCard":Landroid/view/View;
    const v12, 0x7f0e0118

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b036f

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 444
    const v12, 0x7f0e0119

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b0370

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 449
    .end local v5    # "exploreCard":Landroid/view/View;
    .end local v6    # "exploreCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;
    :pswitch_5
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->setFullSpan(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    move-object/from16 v2, p1

    .line 450
    check-cast v2, Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;

    .line 452
    .local v2, "clusterHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    invoke-virtual {v12}, Lcom/google/android/music/ui/MaterialMainstageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0b024a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 454
    .local v3, "clusterTitle":Ljava/lang/String;
    iget-object v12, v2, Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;->headerView:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v12, v3, v13, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 457
    .end local v2    # "clusterHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;
    .end local v3    # "clusterTitle":Ljava/lang/String;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-static/range {v12 .. v16}, Lcom/google/android/music/ui/cardlib/model/Document;->getImFeelingLuckyDocument(Landroid/content/Context;ZZZZ)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v4

    .local v4, "d":Lcom/google/android/music/ui/cardlib/model/Document;
    move-object/from16 v7, p1

    .line 459
    check-cast v7, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 460
    .local v7, "luckyViewHolder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iput-object v4, v7, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 461
    iget-object v8, v7, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 462
    .local v8, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    iget-object v12, v12, Lcom/google/android/music/ui/MaterialMainstageFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v8, v4, v12}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 463
    invoke-virtual {v8, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 409
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 647
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 648
    .local v0, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 662
    :pswitch_0
    const-string v3, "MusicMaterialMainstage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown view type clicked: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    :goto_0
    return-void

    .line 652
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 653
    .local v2, "playCardViewHolderholder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    iget-object v4, v2, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v3, v4, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    goto :goto_0

    .line 657
    .end local v2    # "playCardViewHolderholder":Lcom/google/android/music/ui/PlayCardViewHolder;
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPLORE:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-static {v3, v4}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;Lcom/google/android/music/ui/HomeActivity$Screen;)Landroid/content/Intent;

    move-result-object v1

    .line 659
    .local v1, "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 648
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 21
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 315
    sget-boolean v18, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v18, :cond_0

    .line 316
    const-string v18, "MusicMaterialMainstage"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "onCreateViewHolder viewType="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    .line 320
    .local v12, "inflater":Landroid/view/LayoutInflater;
    packed-switch p2, :pswitch_data_0

    .line 395
    const/4 v9, 0x0

    :goto_0
    return-object v9

    .line 322
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCard:Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    .line 323
    const v18, 0x7f0400de

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    .line 324
    .local v14, "s":Landroid/view/View;
    new-instance v18, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTopSituationsAdapter:Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$200(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v14, v1}, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;-><init>(Landroid/view/View;Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCard:Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

    .line 327
    .end local v14    # "s":Landroid/view/View;
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCard:Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

    goto :goto_0

    .line 329
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$300(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v18

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    .line 331
    .local v16, "v1x1":Landroid/view/View;
    new-instance v9, Lcom/google/android/music/ui/PlayCardViewHolder;

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Lcom/google/android/music/ui/PlayCardViewHolder;-><init>(Landroid/view/View;)V

    .line 332
    .local v9, "holder1x1":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v0, v9, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iget-object v0, v9, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$300(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    goto :goto_0

    .line 337
    .end local v9    # "holder1x1":Lcom/google/android/music/ui/PlayCardViewHolder;
    .end local v16    # "v1x1":Landroid/view/View;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$400(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v18

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    .line 339
    .local v17, "v2x1":Landroid/view/View;
    new-instance v10, Lcom/google/android/music/ui/PlayCardViewHolder;

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Lcom/google/android/music/ui/PlayCardViewHolder;-><init>(Landroid/view/View;)V

    .line 340
    .local v10, "holder2x1":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v0, v10, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    iget-object v0, v10, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$400(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    move-object v9, v10

    .line 343
    goto/16 :goto_0

    .line 345
    .end local v10    # "holder2x1":Lcom/google/android/music/ui/PlayCardViewHolder;
    .end local v17    # "v2x1":Landroid/view/View;
    :pswitch_3
    new-instance v4, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter$1;-><init>(Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;)V

    .line 355
    .local v4, "dismissHandler":Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainstageTutorialCardToShow(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/view/View;

    move-result-object v15

    .line 360
    .local v15, "tc":Landroid/view/View;
    if-nez v15, :cond_2

    .line 361
    new-instance v15, Landroid/view/View;

    .end local v15    # "tc":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 362
    .restart local v15    # "tc":Landroid/view/View;
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 364
    :cond_2
    new-instance v9, Lcom/google/android/music/ui/TutorialCardViewHolder;

    invoke-direct {v9, v15}, Lcom/google/android/music/ui/TutorialCardViewHolder;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 366
    .end local v4    # "dismissHandler":Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;
    .end local v15    # "tc":Landroid/view/View;
    :pswitch_4
    const v18, 0x7f0400cf

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/ui/MultiRowClusterView;

    .line 368
    .local v7, "hc":Lcom/google/android/music/ui/MultiRowClusterView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    .line 369
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/google/android/music/ui/MultiRowClusterView;->setVisibility(I)V

    .line 371
    :cond_3
    new-instance v9, Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;

    invoke-direct {v9, v7}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecentCardViewHolder;-><init>(Lcom/google/android/music/ui/MultiRowClusterView;)V

    goto/16 :goto_0

    .line 373
    .end local v7    # "hc":Lcom/google/android/music/ui/MultiRowClusterView;
    :pswitch_5
    const v18, 0x7f040033

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 375
    .local v5, "exploreCard":Landroid/view/View;
    new-instance v6, Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;

    invoke-direct {v6, v5}, Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;-><init>(Landroid/view/View;)V

    .line 377
    .local v6, "exploreCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;
    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;->mMainstageExploreCardView:Landroid/view/View;
    invoke-static {v6}, Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;->access$500(Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;)Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v9, v6

    .line 378
    goto/16 :goto_0

    .line 380
    .end local v5    # "exploreCard":Landroid/view/View;
    .end local v6    # "exploreCardViewHolder":Lcom/google/android/music/ui/MaterialMainstageFragment$ExploreCardViewHolder;
    :pswitch_6
    const v18, 0x7f040093

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    .line 383
    .local v3, "clusterHeader":Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;
    new-instance v8, Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;

    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-direct {v8, v3, v0}, Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;-><init>(Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;Lcom/google/android/music/ui/MaterialMainstageFragment$1;)V

    .local v8, "holder":Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;
    move-object v9, v8

    .line 385
    goto/16 :goto_0

    .line 387
    .end local v3    # "clusterHeader":Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;
    .end local v8    # "holder":Lcom/google/android/music/ui/MaterialMainstageFragment$PlayClusterHeaderViewHolder;
    :pswitch_7
    const v18, 0x7f0400e5

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    .line 389
    .local v11, "ifl":Landroid/view/View;
    new-instance v13, Lcom/google/android/music/ui/PlayCardViewHolder;

    invoke-direct {v13, v11}, Lcom/google/android/music/ui/PlayCardViewHolder;-><init>(Landroid/view/View;)V

    .line 390
    .local v13, "luckyHolder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v0, v13, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 391
    iget-object v0, v13, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTileMetadata1x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$300(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    move-object v9, v13

    .line 393
    goto/16 :goto_0

    .line 320
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public swapMainstageCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 481
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_0

    .line 482
    const-string v0, "MusicMaterialMainstage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapMainstageCursor newCursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainstageCursor:Landroid/database/Cursor;

    .line 485
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mMainStageCursorLoaded:Z

    .line 486
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->tryUpdateContent()V

    .line 487
    return-void

    .line 485
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public swapRecentCardCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 490
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_0

    .line 491
    const-string v0, "MusicMaterialMainstage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapRecentCursor newCursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursor:Landroid/database/Cursor;

    .line 494
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mRecentCardCursorLoaded:Z

    .line 495
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->tryUpdateContent()V

    .line 496
    return-void

    .line 494
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public swapSituationHeaderCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 499
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_0

    .line 500
    const-string v0, "MusicMaterialMainstage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapSituationHeaderCursor newCursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursor:Landroid/database/Cursor;

    .line 503
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mSituationCardHeaderCursorLoaded:Z

    .line 504
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->notifyDataSetChanged()V

    .line 505
    return-void

    .line 503
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public swapTopSituationsCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 508
    sget-boolean v0, Lcom/google/android/music/ui/MaterialMainstageFragment;->LOGV:Z

    if-eqz v0, :cond_0

    .line 509
    const-string v0, "MusicMaterialMainstage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapTopSituationsCursor newCursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mTopSituationsCursor:Landroid/database/Cursor;

    .line 512
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mTopSituationsCursorLoaded:Z

    .line 513
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTopSituationsAdapter:Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$200(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->mFragment:Lcom/google/android/music/ui/MaterialMainstageFragment;

    # getter for: Lcom/google/android/music/ui/MaterialMainstageFragment;->mTopSituationsAdapter:Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialMainstageFragment;->access$200(Lcom/google/android/music/ui/MaterialMainstageFragment;)Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/songza/SituationCardHelper$SituationsAdapter;->swapCursor(Landroid/database/Cursor;)V

    .line 516
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialMainstageFragment$RecyclerAdapter;->notifyDataSetChanged()V

    .line 517
    return-void

    .line 512
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
