.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;
.super Ljava/lang/Object;
.source "LeanbackArtistDetailsActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/Item$StringGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;->bind(Landroid/database/Cursor;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mArtUri:Ljava/lang/String;

.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;

.field final synthetic val$doc:Lcom/google/android/music/ui/cardlib/model/Document;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->mArtUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->mArtUri:Ljava/lang/String;

    .line 240
    :goto_0
    return-object v0

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;

    iget-object v3, v3, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;

    iget-object v4, v4, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3;->val$context:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->mArtUri:Ljava/lang/String;

    .line 240
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$3$1;->mArtUri:Ljava/lang/String;

    goto :goto_0
.end method
