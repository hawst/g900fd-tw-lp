.class public Lcom/google/android/music/ui/mrp/MusicMediaRouteActionProvider;
.super Landroid/support/v7/app/MediaRouteActionProvider;
.source "MusicMediaRouteActionProvider.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-static {p1}, Lcom/google/android/music/ui/mrp/MusicMediaRouteActionProvider;->getThemedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/app/MediaRouteActionProvider;-><init>(Landroid/content/Context;)V

    .line 18
    new-instance v0, Lcom/google/android/music/ui/mrp/MusicMediaRouteDialogFactory;

    invoke-direct {v0}, Lcom/google/android/music/ui/mrp/MusicMediaRouteDialogFactory;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/mrp/MusicMediaRouteActionProvider;->setDialogFactory(Landroid/support/v7/app/MediaRouteDialogFactory;)V

    .line 19
    return-void
.end method

.method private static getThemedContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0a00b4

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method
