.class public Lcom/google/android/music/cloudclient/RadioFeedRequest$MixRequest;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioFeedRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/RadioFeedRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MixRequest"
.end annotation


# instance fields
.field public mNumEntries:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "numEntries"
    .end annotation
.end field

.field public mNumSeeds:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "numSeeds"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
