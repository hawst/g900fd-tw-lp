.class public final Lcom/google/android/music/wear/DefaultWearMusicDataProvider;
.super Ljava/lang/Object;
.source "DefaultWearMusicDataProvider.java"

# interfaces
.implements Lcom/google/android/music/wear/WearMusicDataProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/wear/DefaultWearMusicDataProvider$1;
    }
.end annotation


# static fields
.field private static final ALBUM_PROJECTION:[Ljava/lang/String;

.field private static final KEEP_ON_PROJECTION:[Ljava/lang/String;

.field private static final LOGV:Z

.field private static final MUSIC_SUMMARY_PROJECTION:[Ljava/lang/String;

.field private static final RADIO_STATION_PROJECTION:[Ljava/lang/String;

.field private static final SUPPORTED_AUTO_PLAYLIST_IDS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 51
    const-string v0, "MusicWearProvider"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->LOGV:Z

    .line 53
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "AlbumId"

    aput-object v1, v0, v4

    const-string v1, "ListId"

    aput-object v1, v0, v5

    const-string v1, "AutoListId"

    aput-object v1, v0, v3

    const-string v1, "RadioStationId"

    aput-object v1, v0, v6

    const-string v1, "downloadedSongCount"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "songCount"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->KEEP_ON_PROJECTION:[Ljava/lang/String;

    .line 68
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "album_name"

    aput-object v1, v0, v4

    const-string v1, "album_artist"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->ALBUM_PROJECTION:[Ljava/lang/String;

    .line 75
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "radio_name"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->RADIO_STATION_PROJECTION:[Ljava/lang/String;

    .line 80
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MUSIC.Id"

    aput-object v1, v0, v4

    const-string v1, "Title"

    aput-object v1, v0, v5

    const-string v1, "AlbumId"

    aput-object v1, v0, v3

    const-string v1, "Artist"

    aput-object v1, v0, v6

    const-string v1, "LocalCopySize"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->MUSIC_SUMMARY_PROJECTION:[Ljava/lang/String;

    .line 105
    new-array v0, v3, [Ljava/lang/Long;

    const-wide/16 v2, -0x4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v4

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/google/android/play/utils/collections/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->SUPPORTED_AUTO_PLAYLIST_IDS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    .line 114
    return-void
.end method

.method private static createContainerDescriptor(Lcom/google/android/music/wear/WearMediaList;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 3
    .param p0, "mediaList"    # Lcom/google/android/music/wear/WearMediaList;

    .prologue
    .line 513
    sget-object v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider$1;->$SwitchMap$com$google$android$music$wear$WearMediaList$MediaListType:[I

    iget-object v1, p0, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    invoke-virtual {v1}, Lcom/google/android/music/wear/WearMediaList$MediaListType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 524
    const-string v0, "MusicWearProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to parse container descriptor for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 515
    :pswitch_0
    iget-wide v0, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newAlbumDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    goto :goto_0

    .line 517
    :pswitch_1
    iget-wide v0, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newPlaylistDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    goto :goto_0

    .line 519
    :pswitch_2
    iget-wide v0, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    iget-object v2, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newRadioDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    goto :goto_0

    .line 521
    :pswitch_3
    iget-wide v0, p0, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-static {v0, v1}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getAutoPlaylistTypeFromId(J)Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/wear/WearMediaList;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/store/ContainerDescriptor;->newAutoPlaylistDescriptor(Lcom/google/android/music/store/ContainerDescriptor$Type;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    goto :goto_0

    .line 513
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getAutoPlaylistSortOrder(J)Ljava/lang/String;
    .locals 2
    .param p0, "autoPlaylistId"    # J

    .prologue
    .line 544
    const-wide/16 v0, -0x4

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 545
    const-string v0, "MUSIC.RatingTimestampMicrosec DESC, MUSIC.CanonicalName"

    .line 549
    :goto_0
    return-object v0

    .line 546
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    .line 547
    const-string v0, "MUSIC.FileDate DESC "

    goto :goto_0

    .line 549
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getAutoPlaylistTypeFromId(J)Lcom/google/android/music/store/ContainerDescriptor$Type;
    .locals 4
    .param p0, "autoPlaylistId"    # J

    .prologue
    .line 530
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 531
    sget-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->RECENTLY_ADDED_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 533
    :goto_0
    return-object v0

    .line 532
    :cond_0
    const-wide/16 v0, -0x4

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    .line 533
    sget-object v0, Lcom/google/android/music/store/ContainerDescriptor$Type;->THUMBS_UP_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    goto :goto_0

    .line 535
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported auto playlist ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getNameForAutoPlaylist(J)Ljava/lang/String;
    .locals 3
    .param p1, "autoPlaylistId"    # J

    .prologue
    .line 574
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    const v1, 0x7f0b00ba

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 577
    :goto_0
    return-object v0

    .line 576
    :cond_0
    const-wide/16 v0, -0x4

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 577
    iget-object v0, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    const v1, 0x7f0b00c2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 579
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected auto playlist ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getSyncedTracksForAutoPlaylistId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/util/List;
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "autoPlaylistId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/Store;->getKeepOnIdFromAutoPlaylistId(Landroid/database/sqlite/SQLiteDatabase;J)J

    move-result-wide v0

    .line 449
    .local v0, "keepOnId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 450
    const-string v2, "MusicWearProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get keep on ID for auto playlist ID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 453
    :goto_0
    return-object v2

    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getAutoPlaylistSortOrder(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getSyncedTracksForKeepOnId(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private static getSyncedTracksForKeepOnId(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/util/List;
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 343
    const-string v2, "SHOULDKEEPON JOIN MUSIC ON (SHOULDKEEPON.MusicId = MUSIC.Id) "

    sget-object v3, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->MUSIC_SUMMARY_PROJECTION:[Ljava/lang/String;

    const-string v4, "KeepOnId = ? AND (LocalCopyType = 200)"

    new-array v5, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    move-object v0, p0

    move-object v7, v6

    move-object v8, p3

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 353
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 354
    invoke-static {v10}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getTracksFromMusicSummaryCursor(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    .line 357
    :goto_0
    return-object v0

    .line 356
    :cond_0
    const-string v0, "MusicWearProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Null cursor returned while querying tracks for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static getSyncedTracksForPlaylistId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/util/List;
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 425
    const-string v2, "MUSIC JOIN LISTITEMS ON (LISTITEMS.MusicId=MUSIC.Id) "

    sget-object v3, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->MUSIC_SUMMARY_PROJECTION:[Ljava/lang/String;

    const-string v4, "ListId = ? AND (LocalCopyType = 200)"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const-string v8, "ServerOrder, ClientPosition"

    move-object v0, p0

    move-object v7, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 435
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 436
    invoke-static {v10}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getTracksFromMusicSummaryCursor(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    .line 439
    :goto_0
    return-object v0

    .line 438
    :cond_0
    const-string v0, "MusicWearProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Null cursor returned while querying tracks for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static getSyncedTracksForRadioStationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/util/List;
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "radioStationId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/Store;->getKeepOnIdFromRadioStationId(Landroid/database/sqlite/SQLiteDatabase;J)J

    move-result-wide v0

    .line 463
    .local v0, "keepOnId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 464
    const-string v2, "MusicWearProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get keep on ID for radio station ID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 467
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getSyncedTracksForKeepOnId(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private static getTracksFromMusicSummaryCursor(Landroid/database/Cursor;)Ljava/util/List;
    .locals 13
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 478
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 479
    .local v0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearTrack;>;"
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 480
    const/4 v10, 0x0

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 481
    .local v2, "id":J
    const/4 v10, 0x1

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 483
    .local v4, "title":Ljava/lang/String;
    const/4 v5, 0x0

    .line 484
    .local v5, "artistName":Ljava/lang/String;
    const/4 v10, 0x3

    invoke-interface {p0, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-nez v10, :cond_1

    .line 485
    const/4 v10, 0x3

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 488
    :cond_1
    const/4 v10, 0x4

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 491
    .local v8, "sizeBytes":J
    const-wide/16 v6, -0x1

    .line 492
    .local v6, "artworkId":J
    const/4 v10, 0x2

    invoke-interface {p0, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-nez v10, :cond_2

    .line 493
    const/4 v10, 0x2

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 495
    :cond_2
    new-instance v1, Lcom/google/android/music/wear/WearTrack;

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/wear/WearTrack;-><init>(JLjava/lang/String;Ljava/lang/String;JJ)V

    .line 496
    .local v1, "track":Lcom/google/android/music/wear/WearTrack;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    sget-boolean v10, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->LOGV:Z

    if-eqz v10, :cond_0

    .line 498
    const-string v10, "MusicWearProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Added track: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 503
    .end local v0    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearTrack;>;"
    .end local v1    # "track":Lcom/google/android/music/wear/WearTrack;
    .end local v2    # "id":J
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "artistName":Ljava/lang/String;
    .end local v6    # "artworkId":J
    .end local v8    # "sizeBytes":J
    :catchall_0
    move-exception v10

    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    throw v10

    .restart local v0    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearTrack;>;"
    :cond_3
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private final getWearMediaListForAlbum(JZ)Lcom/google/android/music/wear/WearMediaList;
    .locals 9
    .param p1, "albumId"    # J
    .param p3, "containerCompleted"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 386
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    :goto_0
    const-string v2, "albumId must be non-negative"

    invoke-static {v0, v2}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 387
    iget-object v0, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->ALBUM_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 394
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 395
    const-string v0, "MusicWearProvider"

    const-string v2, "Album query returned null cursor."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :goto_1
    return-object v3

    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_0
    move v0, v2

    .line 386
    goto :goto_0

    .line 399
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 400
    new-instance v1, Lcom/google/android/music/wear/WearMediaList;

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->ALBUM:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-wide v2, p1

    move v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/wear/WearMediaList;-><init>(JLjava/lang/String;Lcom/google/android/music/wear/WearMediaList$MediaListType;Ljava/lang/String;Z)V

    .line 406
    .local v1, "mediaList":Lcom/google/android/music/wear/WearMediaList;
    sget-boolean v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->LOGV:Z

    if-eqz v0, :cond_2

    .line 407
    const-string v0, "MusicWearProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added album: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v3, v1

    goto :goto_1

    .line 411
    .end local v1    # "mediaList":Lcom/google/android/music/wear/WearMediaList;
    :cond_3
    :try_start_1
    const-string v0, "MusicWearProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to load album with "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private getWearMediaListForAutoPlaylist(JZ)Lcom/google/android/music/wear/WearMediaList;
    .locals 9
    .param p1, "autoPlaylistId"    # J
    .param p3, "containerCompleted"    # Z

    .prologue
    .line 559
    new-instance v1, Lcom/google/android/music/wear/WearMediaList;

    invoke-direct {p0, p1, p2}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getNameForAutoPlaylist(J)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->AUTO_PLAYLIST:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    const/4 v6, 0x0

    move-wide v2, p1

    move v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/wear/WearMediaList;-><init>(JLjava/lang/String;Lcom/google/android/music/wear/WearMediaList$MediaListType;Ljava/lang/String;Z)V

    .line 563
    .local v1, "mediaList":Lcom/google/android/music/wear/WearMediaList;
    sget-boolean v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->LOGV:Z

    if-eqz v0, :cond_0

    .line 564
    const-string v0, "MusicWearProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added auto playlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    :cond_0
    return-object v1
.end method

.method private final getWearMediaListForPlaylist(JZ)Lcom/google/android/music/wear/WearMediaList;
    .locals 9
    .param p1, "playlistId"    # J
    .param p3, "containerCompleted"    # Z

    .prologue
    const/4 v6, 0x0

    .line 366
    iget-object v2, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, p2, v6}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/content/Context;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    move-result-object v0

    .line 368
    .local v0, "playList":Lcom/google/android/music/store/PlayList;
    if-eqz v0, :cond_1

    .line 369
    new-instance v1, Lcom/google/android/music/wear/WearMediaList;

    invoke-virtual {v0}, Lcom/google/android/music/store/PlayList;->getName()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->PLAYLIST:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    move-wide v2, p1

    move v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/wear/WearMediaList;-><init>(JLjava/lang/String;Lcom/google/android/music/wear/WearMediaList$MediaListType;Ljava/lang/String;Z)V

    .line 372
    .local v1, "mediaList":Lcom/google/android/music/wear/WearMediaList;
    sget-boolean v2, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->LOGV:Z

    if-eqz v2, :cond_0

    .line 373
    const-string v2, "MusicWearProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Added playlist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    .end local v1    # "mediaList":Lcom/google/android/music/wear/WearMediaList;
    :cond_0
    :goto_0
    return-object v1

    .line 377
    :cond_1
    const-string v2, "MusicWearProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to read playlist with ID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v6

    .line 378
    goto :goto_0
.end method

.method private getWearMediaListForRadioStation(JZ)Lcom/google/android/music/wear/WearMediaList;
    .locals 9
    .param p1, "radioStationId"    # J
    .param p3, "containerCompleted"    # Z

    .prologue
    const/4 v3, 0x0

    .line 589
    iget-object v0, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->RADIO_STATION_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 596
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 597
    const-string v0, "MusicWearProvider"

    const-string v2, "Radio station query returned null cursor."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :goto_0
    return-object v3

    .line 601
    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 602
    new-instance v1, Lcom/google/android/music/wear/WearMediaList;

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->RADIO:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    const/4 v6, 0x0

    move-wide v2, p1

    move v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/wear/WearMediaList;-><init>(JLjava/lang/String;Lcom/google/android/music/wear/WearMediaList$MediaListType;Ljava/lang/String;Z)V

    .line 608
    .local v1, "mediaList":Lcom/google/android/music/wear/WearMediaList;
    sget-boolean v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->LOGV:Z

    if-eqz v0, :cond_1

    .line 609
    const-string v0, "MusicWearProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added radio station: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v3, v1

    goto :goto_0

    .line 613
    .end local v1    # "mediaList":Lcom/google/android/music/wear/WearMediaList;
    :cond_2
    :try_start_1
    const-string v0, "MusicWearProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to load radio station with "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 617
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public getArtworkFile(J)Ljava/io/File;
    .locals 7
    .param p1, "artworkId"    # J

    .prologue
    .line 319
    iget-object v3, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 320
    .local v2, "store":Lcom/google/android/music/store/Store;
    new-instance v1, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;

    iget-object v3, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;-><init>(Landroid/content/Context;)V

    .line 322
    .local v1, "artworkPathResolver":Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;
    const-wide/16 v4, -0x1

    cmp-long v3, p1, v4

    if-eqz v3, :cond_0

    .line 323
    invoke-virtual {v2, p1, p2}, Lcom/google/android/music/store/Store;->getArtwork(J)Landroid/util/Pair;

    move-result-object v0

    .line 324
    .local v0, "artworkLocationPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 325
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;->resolveArtworkPath(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    .line 329
    .end local v0    # "artworkLocationPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getDecryptedInputStream(J)Ljava/io/InputStream;
    .locals 11
    .param p1, "trackId"    # J

    .prologue
    const/4 v7, 0x0

    .line 220
    iget-object v6, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    .line 221
    .local v5, "store":Lcom/google/android/music/store/Store;
    const/4 v4, 0x0

    .line 223
    .local v4, "musicFile":Lcom/google/android/music/store/MusicFile;
    const/4 v6, 0x0

    :try_start_0
    invoke-static {v5, v6, p1, p2}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 228
    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->getLocalCopyType()I

    move-result v3

    .line 231
    .local v3, "localCopyType":I
    const/16 v6, 0xc8

    if-ne v3, v6, :cond_0

    .line 232
    iget-object v6, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v6, v4}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Lcom/google/android/music/store/MusicFile;)Ljava/io/File;

    move-result-object v2

    .line 233
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 235
    :try_start_1
    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v8

    const/4 v6, 0x0

    invoke-virtual {v5, v8, v9, v6}, Lcom/google/android/music/store/Store;->getCpData(JZ)[B
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    .line 237
    .local v0, "cpData":[B
    if-eqz v0, :cond_1

    .line 239
    :try_start_2
    new-instance v6, Lcom/google/android/music/io/ChunkedInputStreamAdapter;

    new-instance v8, Lcom/google/android/music/download/cp/CpInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v8, v9, v0}, Lcom/google/android/music/download/cp/CpInputStream;-><init>(Ljava/io/InputStream;[B)V

    invoke-direct {v6, v8}, Lcom/google/android/music/io/ChunkedInputStreamAdapter;-><init>(Lcom/google/android/music/io/ChunkedInputStream;)V
    :try_end_2
    .catch Lcom/google/android/music/download/cp/UnrecognizedDataCpException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 255
    .end local v0    # "cpData":[B
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "localCopyType":I
    :goto_0
    return-object v6

    .line 224
    :catch_0
    move-exception v1

    .line 225
    .local v1, "exception":Lcom/google/android/music/store/DataNotFoundException;
    const-string v6, "MusicWearProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Data not found for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v6, v7

    .line 226
    goto :goto_0

    .line 241
    .end local v1    # "exception":Lcom/google/android/music/store/DataNotFoundException;
    .restart local v0    # "cpData":[B
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "localCopyType":I
    :catch_1
    move-exception v1

    .line 242
    .local v1, "exception":Lcom/google/android/music/download/cp/UnrecognizedDataCpException;
    :try_start_3
    const-string v6, "MusicWearProvider"

    const-string v8, "Invalid CP data"

    invoke-static {v6, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .line 254
    .end local v0    # "cpData":[B
    .end local v1    # "exception":Lcom/google/android/music/download/cp/UnrecognizedDataCpException;
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    :goto_1
    const-string v6, "MusicWearProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Couldn\'t open the requested stream for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .line 255
    goto :goto_0

    .line 243
    .restart local v0    # "cpData":[B
    .restart local v2    # "file":Ljava/io/File;
    :catch_2
    move-exception v1

    .line 244
    .local v1, "exception":Ljava/io/IOException;
    :try_start_4
    const-string v6, "MusicWearProvider"

    const-string v8, "IO exception while creating CpInputStream"

    invoke-static {v6, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    .line 249
    .end local v0    # "cpData":[B
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 250
    .local v1, "exception":Ljava/io/FileNotFoundException;
    const-string v6, "MusicWearProvider"

    const-string v8, "Media file not found"

    invoke-static {v6, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 247
    .end local v1    # "exception":Ljava/io/FileNotFoundException;
    .restart local v0    # "cpData":[B
    :cond_1
    :try_start_5
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0
.end method

.method public getSyncedMediaLists()Ljava/util/List;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearMediaList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "music_sync_auto_playlists_to_wear"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v30

    .line 122
    .local v30, "syncAutoPlaylists":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "music_sync_radio_stations_to_wear"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v31

    .line 127
    .local v31, "syncRadioStations":Z
    new-instance v32, Ljava/util/ArrayList;

    invoke-direct/range {v32 .. v32}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v32, "syncedMediaLists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearMediaList;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/store/MusicContent$KeepOn;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->KEEP_ON_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 135
    .local v17, "cursor":Landroid/database/Cursor;
    if-nez v17, :cond_0

    .line 136
    const-string v4, "MusicWearProvider"

    const-string v5, "Media list query returned null cursor."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v32

    .line 188
    .end local v32    # "syncedMediaLists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearMediaList;>;"
    :goto_0
    return-object v32

    .line 140
    .restart local v32    # "syncedMediaLists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/wear/WearMediaList;>;"
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 141
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    const/16 v21, 0x1

    .line 142
    .local v21, "hasPlaylistId":Z
    :goto_2
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_2

    const/16 v19, 0x1

    .line 143
    .local v19, "hasAlbumId":Z
    :goto_3
    const/4 v4, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_3

    const/16 v20, 0x1

    .line 144
    .local v20, "hasAutoPlaylistId":Z
    :goto_4
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_4

    const/16 v22, 0x1

    .line 146
    .local v22, "hasRadioStationId":Z
    :goto_5
    const/4 v4, 0x4

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 147
    .local v18, "downloadedSongCount":I
    const/4 v4, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    .line 148
    .local v27, "songCount":I
    move/from16 v0, v18

    move/from16 v1, v27

    if-lt v0, v1, :cond_5

    const/16 v16, 0x1

    .line 150
    .local v16, "containerCompleted":Z
    :goto_6
    if-eqz v21, :cond_6

    .line 151
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 152
    .local v24, "playlistId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getWearMediaListForPlaylist(JZ)Lcom/google/android/music/wear/WearMediaList;

    move-result-object v23

    .line 154
    .local v23, "playlist":Lcom/google/android/music/wear/WearMediaList;
    if-eqz v23, :cond_0

    .line 155
    move-object/from16 v0, v32

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 186
    .end local v16    # "containerCompleted":Z
    .end local v18    # "downloadedSongCount":I
    .end local v19    # "hasAlbumId":Z
    .end local v20    # "hasAutoPlaylistId":Z
    .end local v21    # "hasPlaylistId":Z
    .end local v22    # "hasRadioStationId":Z
    .end local v23    # "playlist":Lcom/google/android/music/wear/WearMediaList;
    .end local v24    # "playlistId":J
    .end local v27    # "songCount":I
    :catchall_0
    move-exception v4

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    throw v4

    .line 141
    :cond_1
    const/16 v21, 0x0

    goto :goto_2

    .line 142
    .restart local v21    # "hasPlaylistId":Z
    :cond_2
    const/16 v19, 0x0

    goto :goto_3

    .line 143
    .restart local v19    # "hasAlbumId":Z
    :cond_3
    const/16 v20, 0x0

    goto :goto_4

    .line 144
    .restart local v20    # "hasAutoPlaylistId":Z
    :cond_4
    const/16 v22, 0x0

    goto :goto_5

    .line 148
    .restart local v18    # "downloadedSongCount":I
    .restart local v22    # "hasRadioStationId":Z
    .restart local v27    # "songCount":I
    :cond_5
    const/16 v16, 0x0

    goto :goto_6

    .line 157
    .restart local v16    # "containerCompleted":Z
    :cond_6
    if-eqz v19, :cond_8

    .line 158
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 161
    .local v12, "albumId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v12, v4

    if-ltz v4, :cond_7

    .line 162
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v12, v13, v1}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getWearMediaListForAlbum(JZ)Lcom/google/android/music/wear/WearMediaList;

    move-result-object v10

    .line 163
    .local v10, "album":Lcom/google/android/music/wear/WearMediaList;
    if-eqz v10, :cond_0

    .line 164
    move-object/from16 v0, v32

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 167
    .end local v10    # "album":Lcom/google/android/music/wear/WearMediaList;
    :cond_7
    const-string v4, "MusicWearProvider"

    const-string v5, "Associated album ID was negative, skipping."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 169
    .end local v12    # "albumId":J
    :cond_8
    if-eqz v20, :cond_9

    if-eqz v30, :cond_9

    .line 170
    const/4 v4, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 171
    .local v14, "autoPlaylistId":J
    sget-object v4, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->SUPPORTED_AUTO_PLAYLIST_IDS:Ljava/util/Set;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 172
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v14, v15, v1}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getWearMediaListForAutoPlaylist(JZ)Lcom/google/android/music/wear/WearMediaList;

    move-result-object v11

    .line 174
    .local v11, "autoPlaylist":Lcom/google/android/music/wear/WearMediaList;
    move-object/from16 v0, v32

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 176
    .end local v11    # "autoPlaylist":Lcom/google/android/music/wear/WearMediaList;
    .end local v14    # "autoPlaylistId":J
    :cond_9
    if-eqz v22, :cond_0

    if-eqz v31, :cond_0

    .line 177
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    .line 178
    .local v28, "radioStationId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getWearMediaListForRadioStation(JZ)Lcom/google/android/music/wear/WearMediaList;

    move-result-object v26

    .line 180
    .local v26, "radioStation":Lcom/google/android/music/wear/WearMediaList;
    if-eqz v26, :cond_0

    .line 181
    move-object/from16 v0, v32

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 186
    .end local v16    # "containerCompleted":Z
    .end local v18    # "downloadedSongCount":I
    .end local v19    # "hasAlbumId":Z
    .end local v20    # "hasAutoPlaylistId":Z
    .end local v21    # "hasPlaylistId":Z
    .end local v22    # "hasRadioStationId":Z
    .end local v26    # "radioStation":Lcom/google/android/music/wear/WearMediaList;
    .end local v27    # "songCount":I
    .end local v28    # "radioStationId":J
    :cond_a
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public getSyncedTracks(Lcom/google/android/music/wear/WearMediaList;)Ljava/util/List;
    .locals 7
    .param p1, "mediaList"    # Lcom/google/android/music/wear/WearMediaList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/wear/WearMediaList;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/wear/WearTrack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v4, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 194
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 196
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v4, p1, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->PLAYLIST:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    if-ne v4, v5, :cond_0

    .line 197
    iget-wide v4, p1, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-static {v0, v4, v5}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getSyncedTracksForPlaylistId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 214
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    :goto_0
    return-object v4

    .line 198
    :cond_0
    :try_start_1
    iget-object v4, p1, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->ALBUM:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    if-ne v4, v5, :cond_2

    .line 199
    iget-wide v4, p1, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/Store;->getKeepOnIdFromAlbumId(Landroid/database/sqlite/SQLiteDatabase;J)J

    move-result-wide v2

    .line 200
    .local v2, "keepOnId":J
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 201
    const-string v4, "MusicWearProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t get keep on ID for album "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 214
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 204
    :cond_1
    :try_start_2
    const-string v4, "DiscNumber, TrackNumber, CanonicalName"

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getSyncedTracksForKeepOnId(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 214
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 206
    .end local v2    # "keepOnId":J
    :cond_2
    :try_start_3
    iget-object v4, p1, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->AUTO_PLAYLIST:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    if-ne v4, v5, :cond_3

    .line 207
    iget-wide v4, p1, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-static {v0, v4, v5}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getSyncedTracksForAutoPlaylistId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 214
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 208
    :cond_3
    :try_start_4
    iget-object v4, p1, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    sget-object v5, Lcom/google/android/music/wear/WearMediaList$MediaListType;->RADIO:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    if-ne v4, v5, :cond_4

    .line 209
    iget-wide v4, p1, Lcom/google/android/music/wear/WearMediaList;->id:J

    invoke-static {v0, v4, v5}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->getSyncedTracksForRadioStationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/util/List;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v4

    .line 214
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 211
    :cond_4
    :try_start_5
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected media list type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/google/android/music/wear/WearMediaList;->type:Lcom/google/android/music/wear/WearMediaList$MediaListType;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 214
    :catchall_0
    move-exception v4

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4
.end method

.method public isWearSyncEnabled()Z
    .locals 3

    .prologue
    .line 282
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 283
    .local v1, "reference":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 285
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isWearSyncAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isWearSyncEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 287
    :goto_0
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v2

    .line 285
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 287
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public isWearSyncMarkedAvailable()Z
    .locals 3

    .prologue
    .line 271
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 272
    .local v1, "reference":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 274
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isWearSyncAvailable()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 276
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public logPlayEvent(JLcom/google/android/music/wear/WearMediaList;ZJ)V
    .locals 9
    .param p1, "trackId"    # J
    .param p3, "mediaList"    # Lcom/google/android/music/wear/WearMediaList;
    .param p4, "isExplicit"    # Z
    .param p5, "eventTimeMs"    # J

    .prologue
    .line 300
    iget-object v3, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v7

    .line 303
    .local v7, "store":Lcom/google/android/music/store/Store;
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v7, v3, p1, p2}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 309
    .local v2, "musicFile":Lcom/google/android/music/store/MusicFile;
    const/4 v1, 0x0

    .line 310
    .local v1, "containerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    if-eqz p3, :cond_0

    .line 311
    invoke-static {p3}, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->createContainerDescriptor(Lcom/google/android/music/wear/WearMediaList;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 313
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    .local v0, "logger":Lcom/google/android/music/eventlog/MusicEventLogger;
    move v3, p4

    move-wide v4, p5

    .line 314
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/eventlog/MusicEventLogger;->logWearPlayEvent(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZJ)V

    .line 315
    .end local v0    # "logger":Lcom/google/android/music/eventlog/MusicEventLogger;
    .end local v1    # "containerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v2    # "musicFile":Lcom/google/android/music/store/MusicFile;
    :goto_0
    return-void

    .line 304
    :catch_0
    move-exception v6

    .line 305
    .local v6, "exception":Lcom/google/android/music/store/DataNotFoundException;
    const-string v3, "MusicWearProvider"

    const-string v4, "Couldn\'t load MusicFile for provided track"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public markSongPlayed(JJ)V
    .locals 3
    .param p1, "trackId"    # J
    .param p3, "eventTimeMs"    # J

    .prologue
    .line 293
    iget-object v1, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    .line 294
    .local v0, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/music/store/Store;->markSongPlayed(JJ)V

    .line 295
    return-void
.end method

.method public markWearSyncAvailable()V
    .locals 3

    .prologue
    .line 260
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 261
    .local v1, "reference":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/wear/DefaultWearMusicDataProvider;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 263
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setWearSyncAvailable(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 267
    return-void

    .line 265
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method
