.class Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;
.super Landroid/os/AsyncTask;
.source "VideoSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/youtube/VideoSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "YouTubeQueryAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/music/youtube/YouTubeVideo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/youtube/VideoSearchActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/music/youtube/VideoSearchActivity;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/youtube/VideoSearchActivity;Lcom/google/android/music/youtube/VideoSearchActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/youtube/VideoSearchActivity;
    .param p2, "x1"    # Lcom/google/android/music/youtube/VideoSearchActivity$1;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;-><init>(Lcom/google/android/music/youtube/VideoSearchActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 61
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/youtube/YouTubeVideo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 64
    const/4 v4, 0x0

    aget-object v3, p1, v4

    .line 65
    .local v3, "trackName":Ljava/lang/String;
    aget-object v0, p1, v6

    .line 66
    .local v0, "artistName":Ljava/lang/String;
    const/4 v2, 0x1

    .line 69
    .local v2, "startIndex":I
    move-object v1, v3

    .line 70
    .local v1, "query":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 71
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 75
    :cond_0
    const-string v4, "[,&\\-\\|\\s]+"

    const-string v5, " "

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    const/4 v4, 0x5

    invoke-static {v1, v6, v4}, Lcom/google/android/music/youtube/YouTubeUtils;->searchForMatchingVideos(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v4

    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 61
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/youtube/YouTubeVideo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "videos":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/youtube/YouTubeVideo;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    # getter for: Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->access$000(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/content/Context;

    move-result-object v1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/youtube/YouTubeVideo;

    invoke-virtual {v0}, Lcom/google/android/music/youtube/YouTubeVideo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/music/utils/MusicUtils;->startVideoPlayerActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->finish()V

    .line 105
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    # getter for: Lcom/google/android/music/youtube/VideoSearchActivity;->mList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->access$100(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;

    iget-object v2, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    iget-object v3, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    # getter for: Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/music/youtube/VideoSearchActivity;->access$000(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040108

    invoke-direct {v1, v2, v3, v4, p1}, Lcom/google/android/music/youtube/VideoSearchActivity$VideoListAdapter;-><init>(Lcom/google/android/music/youtube/VideoSearchActivity;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->makeListShown()V

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/youtube/VideoSearchActivity;->makeListShown()V

    .line 94
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    # getter for: Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/youtube/VideoSearchActivity;->access$000(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    const v2, 0x7f0b0048

    invoke-virtual {v1, v2}, Lcom/google/android/music/youtube/VideoSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b004e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask$1;-><init>(Lcom/google/android/music/youtube/VideoSearchActivity$YouTubeQueryAsyncTask;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method
