.class Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;
.super Landroid/database/ContentObserver;
.source "ConfigCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/ConfigCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConfigContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/utils/ConfigCache;


# direct methods
.method public constructor <init>(Lcom/google/android/music/utils/ConfigCache;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;->this$0:Lcom/google/android/music/utils/ConfigCache;

    .line 34
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 35
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;->this$0:Lcom/google/android/music/utils/ConfigCache;

    # getter for: Lcom/google/android/music/utils/ConfigCache;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/utils/ConfigCache;->access$000(Lcom/google/android/music/utils/ConfigCache;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;->this$0:Lcom/google/android/music/utils/ConfigCache;

    # getter for: Lcom/google/android/music/utils/ConfigCache;->mCache:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/music/utils/ConfigCache;->access$100(Lcom/google/android/music/utils/ConfigCache;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 41
    iget-object v0, p0, Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;->this$0:Lcom/google/android/music/utils/ConfigCache;

    # operator++ for: Lcom/google/android/music/utils/ConfigCache;->mCacheVersion:J
    invoke-static {v0}, Lcom/google/android/music/utils/ConfigCache;->access$208(Lcom/google/android/music/utils/ConfigCache;)J

    .line 42
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/utils/ConfigCache$ConfigContentObserver;->this$0:Lcom/google/android/music/utils/ConfigCache;

    # getter for: Lcom/google/android/music/utils/ConfigCache;->mType:I
    invoke-static {v1}, Lcom/google/android/music/utils/ConfigCache;->access$300(Lcom/google/android/music/utils/ConfigCache;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->onConfigChange(I)V

    .line 47
    :cond_0
    return-void

    .line 42
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
