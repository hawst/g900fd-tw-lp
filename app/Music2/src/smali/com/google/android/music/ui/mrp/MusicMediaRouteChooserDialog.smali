.class public Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;
.super Landroid/support/v7/app/MediaRouteChooserDialog;
.source "MusicMediaRouteChooserDialog.java"


# instance fields
.field private final mRouteChecker:Lcom/google/android/music/utils/RouteChecker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/support/v7/app/MediaRouteChooserDialog;-><init>(Landroid/content/Context;)V

    .line 19
    new-instance v0, Lcom/google/android/music/utils/RouteChecker;

    invoke-direct {v0, p1}, Lcom/google/android/music/utils/RouteChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    .line 20
    new-instance v0, Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog$1;-><init>(Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;)Lcom/google/android/music/utils/RouteChecker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    return-object v0
.end method


# virtual methods
.method public onFilterRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 3
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v0, 0x0

    .line 31
    if-nez p1, :cond_1

    .line 32
    const-string v1, "MusicCast"

    const-string v2, "onFilterRoute passed null route"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MusicMediaRouteChooserDialog;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    invoke-virtual {v1, p1}, Lcom/google/android/music/utils/RouteChecker;->isAcceptableRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Landroid/support/v7/app/MediaRouteChooserDialog;->onFilterRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
