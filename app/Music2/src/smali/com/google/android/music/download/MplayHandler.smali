.class public Lcom/google/android/music/download/MplayHandler;
.super Ljava/lang/Object;
.source "MplayHandler.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mAcceptHeaderValue:Ljava/lang/String;

.field private mAccount:Landroid/accounts/Account;

.field private final mAndroidId:Ljava/lang/String;

.field private volatile mContentType:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

.field private final mDownloadState:Lcom/google/android/music/download/DownloadState;

.field private mDownloadSucceeded:Z

.field private final mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private final mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

.field private mHttpContentLength:J

.field private mInputStream:Ljava/io/InputStream;

.field private final mIsSmartphone:Z

.field private final mLogHttp:Z

.field private final mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mPartialLength:J

.field private mPassthroughCookies:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

.field private volatile mResponse:Lorg/apache/http/HttpResponse;

.field private final mSupportedInternetMediaTypes:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/cloudclient/MusicHttpClient;Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/DownloadState;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferences"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p3, "httpClient"    # Lcom/google/android/music/cloudclient/MusicHttpClient;
    .param p4, "downloadRequest"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p5, "downloadState"    # Lcom/google/android/music/download/DownloadState;

    .prologue
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 61
    sget-object v13, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v13}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/music/download/MplayHandler;->mLogHttp:Z

    .line 100
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadSucceeded:Z

    .line 102
    const-wide/16 v14, -0x1

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/google/android/music/download/MplayHandler;->mHttpContentLength:J

    .line 104
    const-wide/16 v14, -0x1

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/google/android/music/download/MplayHandler;->mPartialLength:J

    .line 116
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    .line 117
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 118
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 119
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    .line 120
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    .line 121
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "android_id"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-static {v13, v14, v0, v1}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, "androidId":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/music/download/MplayHandler;->getCustomizedDeviceId(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mAndroidId:Ljava/lang/String;

    .line 126
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/DeviceType;->isSmartphone(Landroid/content/Context;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/music/download/MplayHandler;->mIsSmartphone:Z

    .line 128
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v13}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mAccount:Landroid/accounts/Account;

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/download/MplayHandler;->setSyncAccount()V

    .line 130
    new-instance v13, Lcom/google/android/music/sync/google/MusicAuthInfo;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lcom/google/android/music/sync/google/MusicAuthInfo;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

    .line 131
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "music_download_passthrough_cookies"

    const-string v15, "sjsc"

    invoke-static {v13, v14, v15}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 133
    .local v10, "passthroughCookies":Ljava/lang/String;
    new-instance v13, Ljava/util/TreeSet;

    invoke-direct {v13}, Ljava/util/TreeSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mPassthroughCookies:Ljava/util/TreeSet;

    .line 134
    if-eqz v10, :cond_1

    .line 138
    const-string v13, ","

    invoke-virtual {v10, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .local v4, "arr$":[Ljava/lang/String;
    array-length v9, v4

    .local v9, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v9, :cond_1

    aget-object v8, v4, v6

    .line 139
    .local v8, "key":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_0

    .line 140
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mPassthroughCookies:Ljava/util/TreeSet;

    invoke-virtual {v13, v8}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 147
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "len$":I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "music_supported_audio"

    const-string v15, "mpeg,mp3"

    invoke-static {v13, v14, v15}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 150
    .local v11, "supportedAudioTypes":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .local v2, "acceptHeaderValue":Ljava/lang/StringBuilder;
    new-instance v13, Ljava/util/TreeSet;

    invoke-direct {v13}, Ljava/util/TreeSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mSupportedInternetMediaTypes:Ljava/util/TreeSet;

    .line 152
    const-string v13, ","

    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 153
    .local v12, "types":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v13, v12

    if-ge v5, v13, :cond_4

    .line 154
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "audio/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v12, v5

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 155
    .local v7, "internetMediaType":Ljava/lang/String;
    sget-object v13, Lcom/google/android/music/download/DownloadUtils;->MimeToExtensionMap:Lcom/google/common/collect/ImmutableMap;

    invoke-virtual {v13}, Lcom/google/common/collect/ImmutableMap;->keySet()Lcom/google/common/collect/ImmutableSet;

    move-result-object v13

    invoke-virtual {v13, v7}, Lcom/google/common/collect/ImmutableSet;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 156
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mSupportedInternetMediaTypes:Ljava/util/TreeSet;

    invoke-virtual {v13, v7}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 157
    if-eqz v5, :cond_2

    .line 158
    const-string v13, ", "

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    :cond_2
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 162
    :cond_3
    const-string v13, "MplayHandler"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Can\'t enable unsupported audio type: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 165
    .end local v7    # "internetMediaType":Ljava/lang/String;
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mAcceptHeaderValue:Ljava/lang/String;

    .line 166
    sget-boolean v13, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v13, :cond_5

    .line 167
    const-string v13, "MplayHandler"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Accept header value: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/download/MplayHandler;->mAcceptHeaderValue:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_5
    return-void
.end method

.method private getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Lorg/apache/http/client/HttpResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 747
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/MplayHandler;->mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

    invoke-virtual {v2, p1}, Lcom/google/android/music/sync/google/MusicAuthInfo;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 748
    :catch_0
    move-exception v0

    .line 749
    .local v0, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Landroid/accounts/OperationCanceledException;

    if-eqz v2, :cond_0

    .line 750
    const-string v2, "MplayHandler"

    const-string v3, "Getting auth token canceled"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    new-instance v1, Ljava/lang/InterruptedException;

    invoke-direct {v1}, Ljava/lang/InterruptedException;-><init>()V

    .line 752
    .local v1, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v1, v0}, Ljava/lang/InterruptedException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 753
    throw v1

    .line 755
    .end local v1    # "ex":Ljava/lang/InterruptedException;
    :cond_0
    const-string v2, "MplayHandler"

    const-string v3, "Failed to get auth token"

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 756
    new-instance v2, Lorg/apache/http/client/HttpResponseException;

    const/16 v3, 0x191

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to obtain auth token for music streaming: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v2
.end method

.method private static getCustomizedDeviceId(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "baseAndroidId"    # Ljava/lang/String;

    .prologue
    .line 183
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->isLeanbackEnvironment(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->isDeviceIdPrefixEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    invoke-static {p1}, Lcom/google/android/music/leanback/LeanbackUtils;->getDeviceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 187
    .end local p1    # "baseAndroidId":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private getDownloadChunkSize()I
    .locals 5

    .prologue
    const/16 v4, 0x400

    .line 328
    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_chunk_size_bytes"

    invoke-static {v1, v2, v4}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 331
    .local v0, "chunkSize":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    const/high16 v1, 0x300000

    if-le v0, v1, :cond_1

    .line 333
    :cond_0
    const-string v1, "MplayHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid download chunk size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Using default: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const/16 v0, 0x400

    .line 337
    :cond_1
    return v0
.end method

.method private logDownloadedSize(J)V
    .locals 3
    .param p1, "size"    # J

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadFinishedSize(Ljava/lang/String;J)V

    .line 818
    return-void
.end method

.method private logFinalStreamingUrl(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadFinalStreamingUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    return-void
.end method

.method private logFirstBufferReceivedEvent()V
    .locals 12

    .prologue
    .line 787
    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/download/DownloadUtils;->getActiveNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 788
    .local v0, "network":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v10

    .line 789
    .local v10, "networkType":I
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v11

    .line 790
    .local v11, "networkSubtype":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v1}, Lcom/google/android/music/download/DownloadState;->calculateLatency()J

    move-result-wide v8

    .line 792
    .local v8, "latency":J
    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v2, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/TrackOwner;

    iget-object v5, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v5}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v6}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadFirstBufferReceived(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJJII)V

    .line 801
    return-void

    .line 788
    .end local v8    # "latency":J
    .end local v10    # "networkType":I
    .end local v11    # "networkSubtype":I
    :cond_0
    const/16 v10, 0x2710

    goto :goto_0

    .line 789
    .restart local v10    # "networkType":I
    :cond_1
    const/4 v11, 0x0

    goto :goto_1
.end method

.method private logHttpRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "rangeHeaderValue"    # Ljava/lang/String;

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    return-void
.end method

.method private logHttpResponse(I)V
    .locals 2
    .param p1, "responseCode"    # I

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logDownloadHttpResponse(Ljava/lang/String;I)V

    .line 810
    return-void
.end method

.method private notifySharedPreviewMetadataUpdate(Lcom/google/android/music/sharedpreview/PreviewResponse;)V
    .locals 4
    .param p1, "previewResponse"    # Lcom/google/android/music/sharedpreview/PreviewResponse;

    .prologue
    .line 341
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.music.sharedpreviewmetadataupdate"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 343
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "sharedurl"

    iget-object v3, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackDownloadRequest;->getDomainParam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v2, "duration"

    iget v3, p1, Lcom/google/android/music/sharedpreview/PreviewResponse;->mPreviewDurationMillis:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 347
    iget-object v2, p1, Lcom/google/android/music/sharedpreview/PreviewResponse;->mPlayType:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/music/sharedpreview/PreviewResponse;->convertPreviewType(Ljava/lang/String;)I

    move-result v1

    .line 348
    .local v1, "playType":I
    const-string v2, "playtype"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 350
    iget-object v2, p0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 351
    return-void
.end method

.method private parseContentType(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 5
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    const/4 v4, 0x0

    .line 762
    const-string v2, "Content-Type"

    invoke-interface {p1, v2}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v1

    .line 763
    .local v1, "contentTypes":[Lorg/apache/http/Header;
    const/4 v0, 0x0

    .line 764
    .local v0, "contentType":Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eqz v2, :cond_0

    .line 766
    aget-object v2, v1, v4

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 769
    :cond_0
    return-object v0
.end method

.method private wrapInThrottledStreamIfNeed(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 10
    .param p1, "originalStream"    # Ljava/io/InputStream;

    .prologue
    const/16 v8, 0x800

    const/16 v7, 0x140

    .line 223
    iget-object v4, p0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 224
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string v4, "music_throttle_download"

    const/4 v5, 0x0

    invoke-static {v1, v4, v5}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    .line 227
    .local v2, "throttle":Z
    if-eqz v2, :cond_3

    .line 228
    const-string v4, "music_throttle_download_kbps"

    invoke-static {v1, v4, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 231
    .local v3, "throttleKbps":I
    const/16 v4, 0x200

    if-ge v3, v4, :cond_0

    .line 232
    const-string v4, "MplayHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Throttle speed is too low: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Using default: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const/16 v3, 0x800

    .line 236
    :cond_0
    const-string v4, "music_throttle_initial_kb"

    invoke-static {v1, v4, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 240
    .local v0, "initialChunkInKb":I
    if-ltz v0, :cond_1

    const/16 v4, 0x2800

    if-le v0, v4, :cond_2

    .line 243
    :cond_1
    const-string v4, "MplayHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Throttle initial chunk is invalid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Using default: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/16 v0, 0x140

    .line 248
    :cond_2
    const-string v4, "MplayHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Throttling download at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "kbps. Initial chunk: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "KB."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    new-instance v4, Lcom/google/android/music/io/ThrottledInputStream;

    const-wide/16 v6, 0x400

    int-to-long v8, v0

    mul-long/2addr v6, v8

    invoke-direct {v4, p1, v3, v6, v7}, Lcom/google/android/music/io/ThrottledInputStream;-><init>(Ljava/io/InputStream;IJ)V

    move-object p1, v4

    .line 252
    .end local v0    # "initialChunkInKb":I
    .end local v3    # "throttleKbps":I
    .end local p1    # "originalStream":Ljava/io/InputStream;
    :cond_3
    return-object p1
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    .line 784
    :cond_0
    return-void
.end method

.method public downloadSucceeded()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadSucceeded:Z

    return v0
.end method

.method public downloadTo(Ljava/io/OutputStream;)V
    .locals 18
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 257
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    if-nez v7, :cond_0

    .line 258
    new-instance v7, Ljava/io/IOException;

    const-string v12, "Missing input stream"

    invoke-direct {v7, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 260
    :cond_0
    const/4 v6, 0x0

    .line 261
    .local v6, "success":Z
    const-wide/16 v8, 0x0

    .line 263
    .local v8, "totalRead":J
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/MplayHandler;->getDownloadChunkSize()I

    move-result v7

    new-array v2, v7, [B

    .line 265
    .local v2, "buff":[B
    const/4 v4, 0x0

    .line 266
    .local v4, "first_buffer_received":Z
    const-wide/16 v10, 0x0

    .line 267
    .local v10, "written":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v7, v2}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .local v5, "read":I
    const/4 v7, -0x1

    if-eq v5, v7, :cond_6

    .line 268
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 269
    new-instance v7, Ljava/lang/InterruptedException;

    invoke-direct {v7}, Ljava/lang/InterruptedException;-><init>()V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    .end local v2    # "buff":[B
    .end local v4    # "first_buffer_received":Z
    .end local v5    # "read":I
    .end local v10    # "written":J
    :catch_0
    move-exception v3

    .line 290
    .local v3, "e":Ljava/io/IOException;
    :try_start_1
    sget-boolean v7, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v7, :cond_2

    .line 291
    const-string v7, "MplayHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "MplayHandler.downloadTo: IOException: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12, v3}, Lcom/google/android/music/log/Log;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 293
    :cond_2
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 304
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    move-object v12, v7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    if-nez v6, :cond_d

    const/4 v7, 0x1

    :goto_0
    invoke-static {v13, v7}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    .line 305
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    .line 306
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/music/download/MplayHandler;->mLogHttp:Z

    if-eqz v7, :cond_3

    .line 307
    sget-object v7, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Download finished: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " state="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v7, v13}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/google/android/music/download/MplayHandler;->logDownloadedSize(J)V

    throw v12

    .line 271
    .restart local v2    # "buff":[B
    .restart local v4    # "first_buffer_received":Z
    .restart local v5    # "read":I
    .restart local v10    # "written":J
    :cond_4
    int-to-long v12, v5

    add-long/2addr v8, v12

    .line 272
    if-nez v4, :cond_5

    .line 273
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/MplayHandler;->logFirstBufferReceivedEvent()V

    .line 274
    const/4 v4, 0x1

    .line 276
    :cond_5
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 277
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/download/MplayHandler;->mPartialLength:J

    const-wide/16 v14, -0x1

    cmp-long v7, v12, v14

    if-eqz v7, :cond_1

    .line 278
    int-to-long v12, v5

    add-long/2addr v10, v12

    .line 279
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/download/MplayHandler;->mPartialLength:J

    cmp-long v7, v10, v12

    if-ltz v7, :cond_1

    .line 280
    sget-boolean v7, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v7, :cond_6

    const-string v7, "MplayHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Reached partial length of "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/download/MplayHandler;->mPartialLength:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_6
    const/4 v6, 0x1

    .line 286
    sget-boolean v7, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v7, :cond_7

    .line 287
    const-string v7, "MplayHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "downloadTo: done "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12}, Lcom/google/android/music/log/Log;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 304
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    if-nez v6, :cond_c

    const/4 v7, 0x1

    :goto_1
    invoke-static {v12, v7}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    .line 305
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    .line 306
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/music/download/MplayHandler;->mLogHttp:Z

    if-eqz v7, :cond_8

    .line 307
    sget-object v7, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Download finished: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " state="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/google/android/music/download/MplayHandler;->logDownloadedSize(J)V

    .line 312
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadSucceeded:Z

    .line 313
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/download/MplayHandler;->mHttpContentLength:J

    const-wide/16 v14, -0x1

    cmp-long v7, v12, v14

    if-eqz v7, :cond_9

    .line 315
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/music/download/MplayHandler;->mHttpContentLength:J

    cmp-long v7, v8, v12

    if-nez v7, :cond_e

    const/4 v7, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadSucceeded:Z

    .line 317
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadSucceeded:Z

    if-nez v7, :cond_a

    .line 318
    const-string v7, "MplayHandler"

    const-string v12, "Failed to download complete content: mHttpContentLength=%s totalRead=%s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/MplayHandler;->mHttpContentLength:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_a
    sget-boolean v7, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v7, :cond_b

    .line 323
    const-string v7, "MplayHandler"

    const-string v12, "Download finished gracefully"

    invoke-static {v7, v12}, Lcom/google/android/music/log/Log;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_b
    return-void

    .line 304
    :cond_c
    const/4 v7, 0x0

    goto/16 :goto_1

    .end local v2    # "buff":[B
    .end local v4    # "first_buffer_received":Z
    .end local v5    # "read":I
    .end local v10    # "written":J
    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 315
    .restart local v2    # "buff":[B
    .restart local v4    # "first_buffer_received":Z
    .restart local v5    # "read":I
    .restart local v10    # "written":J
    :cond_e
    const/4 v7, 0x0

    goto :goto_2
.end method

.method protected getDownloadStream()Ljava/io/InputStream;
    .locals 58
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getSeekMs()J

    move-result-wide v44

    .line 357
    .local v44, "seekToMs":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/download/MplayHandler;->mAccount:Landroid/accounts/Account;

    .line 358
    .local v4, "account":Landroid/accounts/Account;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/music/download/MplayHandler;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    .line 359
    .local v6, "authToken":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/download/MplayHandler;->mAndroidId:Ljava/lang/String;

    .line 360
    .local v14, "deviceId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/preferences/MusicPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v28

    .line 361
    .local v28, "loggingId":Ljava/lang/String;
    const/16 v41, 0x1

    .line 362
    .local v41, "retryAuthFailure":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v15

    .line 364
    .local v15, "downloadRequestId":Lcom/google/android/music/download/ContentIdentifier;
    const/16 v36, 0x0

    .line 366
    .local v36, "redirectCount":I
    new-instance v52, Ljava/lang/StringBuilder;

    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    .line 367
    .local v52, "urlBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getRemoteId()Ljava/lang/String;

    move-result-object v40

    .line 368
    .local v40, "remoteId":Ljava/lang/String;
    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "https://mclients.googleapis.com/music/mplay?songid="

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    .line 370
    .local v50, "streamingUrl":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/google/android/music/download/ContentIdentifier;->isSharedDomain()Z

    move-result v54

    if-eqz v54, :cond_7

    .line 371
    new-instance v7, Lcom/google/android/music/sharedpreview/SharedPreviewClient;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-direct {v7, v0}, Lcom/google/android/music/sharedpreview/SharedPreviewClient;-><init>(Landroid/content/Context;)V

    .line 372
    .local v7, "client":Lcom/google/android/music/sharedpreview/SharedPreviewClient;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getDomainParam()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v7, v0}, Lcom/google/android/music/sharedpreview/SharedPreviewClient;->getPreviewResponse(Ljava/lang/String;)Lcom/google/android/music/sharedpreview/PreviewResponse;

    move-result-object v34

    .line 374
    .local v34, "previewResponse":Lcom/google/android/music/sharedpreview/PreviewResponse;
    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/google/android/music/sharedpreview/PreviewResponse;->mUrl:Ljava/lang/String;

    move-object/from16 v50, v0

    .line 375
    if-nez v50, :cond_0

    .line 376
    new-instance v54, Ljava/io/IOException;

    const-string v55, "Failed to retrieve streaming url"

    invoke-direct/range {v54 .. v55}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v54

    .line 378
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/google/android/music/download/MplayHandler;->notifySharedPreviewMetadataUpdate(Lcom/google/android/music/sharedpreview/PreviewResponse;)V

    .line 385
    .end local v7    # "client":Lcom/google/android/music/sharedpreview/SharedPreviewClient;
    .end local v34    # "previewResponse":Lcom/google/android/music/sharedpreview/PreviewResponse;
    :cond_1
    :goto_0
    move-object/from16 v0, v52

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    const-string v54, "&targetkbps="

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Lcom/google/android/music/download/DownloadState;->getRecommendedBitrate()I

    move-result v55

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 388
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/MplayHandler;->mIsSmartphone:Z

    move/from16 v54, v0

    if-eqz v54, :cond_9

    .line 389
    const-string v54, "&p=1"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/preferences/MusicPreferences;->isHighStreamQuality()Z

    move-result v54

    if-eqz v54, :cond_a

    .line 395
    const-string v54, "&opt=hi"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    move-object/from16 v54, v0

    invoke-static/range {v54 .. v54}, Lcom/google/android/music/download/DownloadUtils;->getActiveNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v32

    .line 403
    .local v32, "network":Landroid/net/NetworkInfo;
    if-eqz v32, :cond_3

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    move-object/from16 v54, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v54

    invoke-static {v0, v1}, Lcom/google/android/music/download/DownloadUtils;->isMobileOrMeteredNetworkType(Landroid/net/NetworkInfo;Landroid/content/Context;)Z

    move-result v54

    if-eqz v54, :cond_c

    .line 405
    const-string v54, "&net=mob"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v54

    sget-object v55, Lcom/google/android/music/download/TrackOwner;->MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    if-ne v0, v1, :cond_10

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->isExplicit()Z

    move-result v24

    .line 428
    .local v24, "isExplicit":Z
    if-eqz v24, :cond_f

    .line 429
    const-string v54, "&pt=e"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getPriority()I

    move-result v54

    sget v55, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_STREAM:I

    move/from16 v0, v54

    move/from16 v1, v55

    if-eq v0, v1, :cond_4

    .line 435
    const-string v54, "&dt=pc"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .end local v24    # "isExplicit":Z
    :cond_4
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v8

    .line 447
    .local v8, "completedLength":J
    const-wide/16 v54, -0x1

    move-wide/from16 v0, v54

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/download/MplayHandler;->mPartialLength:J

    .line 449
    const-wide/16 v54, 0x0

    cmp-long v54, v44, v54

    if-eqz v54, :cond_5

    .line 450
    const-string v54, "&start="

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, v54

    move-wide/from16 v1, v44

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 455
    :cond_5
    move-object/from16 v0, v40

    move-object/from16 v1, v52

    invoke-static {v0, v1}, Lcom/google/android/music/download/RequestSigningUtil;->appendMplayUrlSignatureParams(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 457
    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    .line 458
    .local v51, "url":Ljava/lang/String;
    new-instance v13, Ljava/util/LinkedList;

    invoke-direct {v13}, Ljava/util/LinkedList;-><init>()V

    .line 459
    .local v13, "cookies":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    :cond_6
    :goto_6
    const/16 v54, 0xa

    move/from16 v0, v36

    move/from16 v1, v54

    if-ge v0, v1, :cond_28

    .line 460
    const/16 v54, 0x0

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    .line 461
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v54

    if-eqz v54, :cond_13

    .line 462
    new-instance v54, Ljava/lang/InterruptedException;

    invoke-direct/range {v54 .. v54}, Ljava/lang/InterruptedException;-><init>()V

    throw v54

    .line 379
    .end local v8    # "completedLength":J
    .end local v13    # "cookies":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    .end local v32    # "network":Landroid/net/NetworkInfo;
    .end local v51    # "url":Ljava/lang/String;
    :cond_7
    invoke-virtual {v15}, Lcom/google/android/music/download/ContentIdentifier;->isNautilusDomain()Z

    move-result v54

    if-nez v54, :cond_8

    const-string v54, "T"

    move-object/from16 v0, v40

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v54

    if-eqz v54, :cond_1

    .line 383
    :cond_8
    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "https://mclients.googleapis.com/music/mplay?mjck="

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    goto/16 :goto_0

    .line 391
    :cond_9
    const-string v54, "&p=0"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 396
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/preferences/MusicPreferences;->isNormalStreamQuality()Z

    move-result v54

    if-eqz v54, :cond_b

    .line 397
    const-string v54, "&opt=med"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 398
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/preferences/MusicPreferences;->isLowStreamQuality()Z

    move-result v54

    if-eqz v54, :cond_2

    .line 399
    const-string v54, "&opt=low"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 406
    .restart local v32    # "network":Landroid/net/NetworkInfo;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    move-object/from16 v54, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v54

    invoke-static {v0, v1}, Lcom/google/android/music/download/DownloadUtils;->isUnmeteredWifiNetworkType(Landroid/net/NetworkInfo;Landroid/content/Context;)Z

    move-result v54

    if-eqz v54, :cond_d

    .line 407
    const-string v54, "&net=wifi"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 408
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    move-object/from16 v54, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v54

    invoke-static {v0, v1}, Lcom/google/android/music/download/DownloadUtils;->isUnmeteredEthernetNetworkType(Landroid/net/NetworkInfo;Landroid/content/Context;)Z

    move-result v54

    if-eqz v54, :cond_e

    .line 409
    const-string v54, "&net=ether"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 411
    :cond_e
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Unsupported network type: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v32 .. v32}, Landroid/net/NetworkInfo;->getType()I

    move-result v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 431
    .restart local v24    # "isExplicit":Z
    :cond_f
    const-string v54, "&pt=a"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 437
    .end local v24    # "isExplicit":Z
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v54

    sget-object v55, Lcom/google/android/music/download/TrackOwner;->KEEPON:Lcom/google/android/music/download/TrackOwner;

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    if-ne v0, v1, :cond_11

    .line 438
    const-string v54, "&dt=uc"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 439
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lcom/google/android/music/download/TrackDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v54

    sget-object v55, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    if-ne v0, v1, :cond_12

    .line 440
    const-string v54, "&dt=sc"

    move-object/from16 v0, v52

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 442
    :cond_12
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Unexpected download request: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v56, v0

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 465
    .restart local v8    # "completedLength":J
    .restart local v13    # "cookies":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    .restart local v51    # "url":Ljava/lang/String;
    :cond_13
    new-instance v54, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, v54

    move-object/from16 v1, v51

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    invoke-virtual/range {v54 .. v54}, Lorg/apache/http/client/methods/HttpRequestBase;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v33

    .line 468
    .local v33, "params":Lorg/apache/http/params/HttpParams;
    const/16 v54, 0x2ee0

    move-object/from16 v0, v33

    move/from16 v1, v54

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 469
    const/16 v54, 0x2710

    move-object/from16 v0, v33

    move/from16 v1, v54

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 472
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v54

    if-eqz v54, :cond_14

    .line 473
    new-instance v54, Ljava/io/IOException;

    const-string v55, "No auth token available."

    invoke-direct/range {v54 .. v55}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v54

    .line 475
    :cond_14
    if-nez v36, :cond_1a

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    const-string v55, "Authorization"

    new-instance v56, Ljava/lang/StringBuilder;

    invoke-direct/range {v56 .. v56}, Ljava/lang/StringBuilder;-><init>()V

    const-string v57, "GoogleLogin auth="

    invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v56

    move-object/from16 v0, v56

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v56

    invoke-virtual/range {v56 .. v56}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v56

    invoke-virtual/range {v54 .. v56}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    const-string v55, "X-Device-ID"

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    invoke-virtual {v0, v1, v14}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    const-string v55, "X-Device-Logging-ID"

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    const-string v55, "Accept"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mAcceptHeaderValue:Ljava/lang/String;

    move-object/from16 v56, v0

    invoke-virtual/range {v54 .. v56}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const/16 v35, 0x0

    .line 493
    .local v35, "rangeHeaderValue":Ljava/lang/String;
    const-wide/16 v54, 0x0

    cmp-long v54, v8, v54

    if-eqz v54, :cond_17

    .line 496
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_16

    .line 497
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Setting range headers to start at byte "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_16
    new-instance v54, Ljava/lang/StringBuilder;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuilder;-><init>()V

    const-string v55, "bytes="

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v54

    const-string v55, "-"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    const-string v55, "Range"

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_17
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_18

    .line 513
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Requesting URL: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v56, v0

    invoke-virtual/range {v56 .. v56}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    :cond_18
    if-nez v35, :cond_19

    const-string v35, ""

    .end local v35    # "rangeHeaderValue":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, v51

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/download/MplayHandler;->logHttpRequest(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v55, v0

    invoke-virtual/range {v54 .. v55}, Lcom/google/android/music/cloudclient/MusicHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    invoke-interface/range {v54 .. v54}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v48

    .line 519
    .local v48, "statusLine":Lorg/apache/http/StatusLine;
    if-nez v48, :cond_1b

    .line 520
    const-string v54, "MplayHandler"

    const-string v55, "Stream-download response status line is null."

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    new-instance v54, Ljava/io/IOException;

    const-string v55, "StatusLine is null -- should not happen."

    invoke-direct/range {v54 .. v55}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v54

    .line 481
    .end local v48    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_1a
    invoke-virtual {v13}, Ljava/util/LinkedList;->size()I

    move-result v54

    if-lez v54, :cond_15

    .line 482
    invoke-virtual {v13}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v54

    if-eqz v54, :cond_15

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 483
    .local v10, "cookie":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v54, v0

    const-string v55, "Cookie"

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    invoke-virtual {v0, v1, v10}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 524
    .end local v10    # "cookie":Ljava/lang/String;
    .end local v21    # "i$":Ljava/util/Iterator;
    .restart local v48    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_1b
    invoke-interface/range {v48 .. v48}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v47

    .line 525
    .local v47, "status":I
    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-direct {v0, v1}, Lcom/google/android/music/download/MplayHandler;->logHttpResponse(I)V

    .line 526
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_1c

    .line 527
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Response: status="

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/download/MplayHandler;->mLogHttp:Z

    move/from16 v54, v0

    if-eqz v54, :cond_1e

    .line 531
    sget-object v54, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "status="

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    invoke-interface/range {v54 .. v54}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    .local v5, "arr$":[Lorg/apache/http/Header;
    array-length v0, v5

    move/from16 v25, v0

    .local v25, "len$":I
    const/16 v21, 0x0

    .local v21, "i$":I
    :goto_8
    move/from16 v0, v21

    move/from16 v1, v25

    if-ge v0, v1, :cond_1e

    aget-object v20, v5, v21

    .line 533
    .local v20, "header":Lorg/apache/http/Header;
    invoke-interface/range {v20 .. v20}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v54

    const-string v55, "Set-Cookie"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v54

    if-nez v54, :cond_1d

    .line 532
    :goto_9
    add-int/lit8 v21, v21, 0x1

    goto :goto_8

    .line 536
    :cond_1d
    sget-object v54, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Response header: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    const-string v56, ": "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 540
    .end local v5    # "arr$":[Lorg/apache/http/Header;
    .end local v20    # "header":Lorg/apache/http/Header;
    .end local v21    # "i$":I
    .end local v25    # "len$":I
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    invoke-interface/range {v54 .. v54}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    .line 542
    .local v18, "entity":Lorg/apache/http/HttpEntity;
    const/16 v54, 0xc8

    move/from16 v0, v47

    move/from16 v1, v54

    if-lt v0, v1, :cond_27

    const/16 v54, 0x12c

    move/from16 v0, v47

    move/from16 v1, v54

    if-ge v0, v1, :cond_27

    if-eqz v18, :cond_27

    .line 543
    move-object/from16 v0, p0

    move-object/from16 v1, v51

    invoke-direct {v0, v1}, Lcom/google/android/music/download/MplayHandler;->logFinalStreamingUrl(Ljava/lang/String;)V

    .line 544
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Lcom/google/android/music/download/MplayHandler;->parseContentType(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/MplayHandler;->mContentType:Ljava/lang/String;

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContentType:Ljava/lang/String;

    move-object/from16 v54, v0

    if-eqz v54, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mSupportedInternetMediaTypes:Ljava/util/TreeSet;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContentType:Ljava/lang/String;

    move-object/from16 v55, v0

    invoke-virtual/range {v54 .. v55}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v54

    if-nez v54, :cond_20

    .line 546
    :cond_1f
    new-instance v54, Lcom/google/android/music/download/UnsupportedAudioTypeException;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContentType:Ljava/lang/String;

    move-object/from16 v55, v0

    invoke-direct/range {v54 .. v55}, Lcom/google/android/music/download/UnsupportedAudioTypeException;-><init>(Ljava/lang/String;)V

    throw v54

    .line 549
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContentType:Ljava/lang/String;

    move-object/from16 v55, v0

    invoke-virtual/range {v54 .. v55}, Lcom/google/android/music/download/DownloadState;->setHttpContentType(Ljava/lang/String;)V

    .line 551
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_21

    .line 552
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Received valid response for playback with content type: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContentType:Ljava/lang/String;

    move-object/from16 v56, v0

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "Content-Length"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v26

    .line 555
    .local v26, "lengthHeaders":[Lorg/apache/http/Header;
    if-eqz v26, :cond_25

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v54, v0

    if-lez v54, :cond_25

    const/16 v19, 0x1

    .line 556
    .local v19, "hasContentLength":Z
    :goto_a
    if-eqz v19, :cond_22

    .line 557
    const/16 v54, 0x0

    aget-object v54, v26, v54

    invoke-interface/range {v54 .. v54}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v53

    .line 559
    .local v53, "value":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v53 .. v53}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v54

    move-wide/from16 v0, v54

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/download/MplayHandler;->mHttpContentLength:J

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/download/MplayHandler;->mHttpContentLength:J

    move-wide/from16 v56, v0

    move-object/from16 v0, v54

    move-wide/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/download/DownloadState;->adjustDownloadLengthUsingHttpContentLength(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    .end local v53    # "value":Ljava/lang/String;
    :cond_22
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "X-Estimated-Content-Length"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v26

    .line 568
    if-eqz v26, :cond_26

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v54, v0

    if-lez v54, :cond_26

    .line 569
    const/16 v54, 0x0

    aget-object v54, v26, v54

    invoke-interface/range {v54 .. v54}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v53

    .line 571
    .restart local v53    # "value":Ljava/lang/String;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v54, v0

    invoke-static/range {v53 .. v53}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v56

    move-object/from16 v0, v54

    move-wide/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/download/DownloadState;->setEstimatedDownloadByteLength(J)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 579
    .end local v53    # "value":Ljava/lang/String;
    :cond_23
    :goto_c
    new-instance v23, Lcom/google/android/music/download/AbortRequestOnCloseInputStream;

    invoke-static/range {v18 .. v18}, Landroid/net/http/AndroidHttpClient;->getUngzippedContent(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v54

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v55, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v54

    move-object/from16 v2, v55

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/download/AbortRequestOnCloseInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    .line 582
    .local v23, "in":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "X-ID3-Footer-Attached"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v22

    .line 583
    .local v22, "id3Footer":[Lorg/apache/http/Header;
    if-eqz v22, :cond_24

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v54, v0

    if-eqz v54, :cond_24

    .line 584
    new-instance v54, Lcom/google/android/music/download/ID3v1FooterInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v55, v0

    move-object/from16 v0, v54

    move-object/from16 v1, v23

    move-object/from16 v2, v55

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/download/ID3v1FooterInputStream;-><init>(Ljava/io/InputStream;Lcom/google/android/music/download/DownloadState;)V

    move-object/from16 v23, v54

    .line 586
    .end local v23    # "in":Ljava/io/InputStream;
    :cond_24
    return-object v23

    .line 555
    .end local v19    # "hasContentLength":Z
    .end local v22    # "id3Footer":[Lorg/apache/http/Header;
    :cond_25
    const/16 v19, 0x0

    goto/16 :goto_a

    .line 562
    .restart local v19    # "hasContentLength":Z
    .restart local v53    # "value":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 563
    .local v16, "e":Ljava/lang/NumberFormatException;
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Server sent invalid content length: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_b

    .line 572
    .end local v16    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v16

    .line 573
    .restart local v16    # "e":Ljava/lang/NumberFormatException;
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Server sent invalid estimated content length: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_c

    .line 575
    .end local v16    # "e":Ljava/lang/NumberFormatException;
    .end local v53    # "value":Ljava/lang/String;
    :cond_26
    if-nez v19, :cond_23

    .line 576
    const-string v54, "MplayHandler"

    const-string v55, "No Content-Length or X-Estimated-Content-Length provided"

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 590
    .end local v19    # "hasContentLength":Z
    .end local v26    # "lengthHeaders":[Lorg/apache/http/Header;
    :cond_27
    const/16 v54, 0x12e

    move/from16 v0, v47

    move/from16 v1, v54

    if-ne v0, v1, :cond_32

    .line 594
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "Location"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v27

    .line 596
    .local v27, "location":Lorg/apache/http/Header;
    if-nez v27, :cond_29

    .line 597
    const-string v54, "MplayHandler"

    const/16 v55, 0x3

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v54

    if-eqz v54, :cond_28

    .line 598
    const-string v54, "MplayHandler"

    const-string v55, "Redirect requested but no Location specified."

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    .end local v18    # "entity":Lorg/apache/http/HttpEntity;
    .end local v27    # "location":Lorg/apache/http/Header;
    .end local v33    # "params":Lorg/apache/http/params/HttpParams;
    .end local v47    # "status":I
    .end local v48    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_28
    const/16 v54, 0xa

    move/from16 v0, v36

    move/from16 v1, v54

    if-lt v0, v1, :cond_42

    .line 736
    new-instance v54, Ljava/io/IOException;

    const-string v55, "Unable to download stream due to too many redirects."

    invoke-direct/range {v54 .. v55}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v54

    .line 602
    .restart local v18    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v27    # "location":Lorg/apache/http/Header;
    .restart local v33    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v47    # "status":I
    .restart local v48    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_29
    const-string v54, "MplayHandler"

    const/16 v55, 0x3

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v54

    if-eqz v54, :cond_2a

    .line 603
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Following redirect to "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-interface/range {v27 .. v27}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v56

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_2a
    invoke-interface/range {v27 .. v27}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v51

    .line 607
    add-int/lit8 v36, v36, 0x1

    .line 611
    const/16 v54, 0x1

    move/from16 v0, v36

    move/from16 v1, v54

    if-ne v0, v1, :cond_2b

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "X-Stream-Fidelity"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v49

    .line 615
    .local v49, "streamFidelityHeaders":[Lorg/apache/http/Header;
    if-eqz v49, :cond_2e

    move-object/from16 v0, v49

    array-length v0, v0

    move/from16 v54, v0

    if-lez v54, :cond_2e

    .line 616
    const/16 v54, 0x0

    aget-object v54, v49, v54

    invoke-interface/range {v54 .. v54}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v53

    .line 617
    .restart local v53    # "value":Ljava/lang/String;
    const-string v54, "None more high"

    move-object/from16 v0, v54

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v54

    if-nez v54, :cond_2d

    .line 618
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v54, v0

    const/16 v55, 0x1

    invoke-virtual/range {v54 .. v55}, Lcom/google/android/music/download/DownloadState;->setStreamFidelity(I)V

    .line 632
    .end local v49    # "streamFidelityHeaders":[Lorg/apache/http/Header;
    .end local v53    # "value":Ljava/lang/String;
    :cond_2b
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "Set-Cookie"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v11

    .line 633
    .local v11, "cookieHeaders":[Lorg/apache/http/Header;
    move-object v5, v11

    .restart local v5    # "arr$":[Lorg/apache/http/Header;
    array-length v0, v5

    move/from16 v25, v0

    .restart local v25    # "len$":I
    const/16 v21, 0x0

    .restart local v21    # "i$":I
    :goto_e
    move/from16 v0, v21

    move/from16 v1, v25

    if-ge v0, v1, :cond_6

    aget-object v10, v5, v21

    .line 634
    .local v10, "cookie":Lorg/apache/http/Header;
    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v46

    .line 635
    .local v46, "setCookieString":Ljava/lang/String;
    if-eqz v46, :cond_2c

    invoke-virtual/range {v46 .. v46}, Ljava/lang/String;->length()I

    move-result v54

    if-nez v54, :cond_2f

    .line 633
    :cond_2c
    :goto_f
    add-int/lit8 v21, v21, 0x1

    goto :goto_e

    .line 621
    .end local v5    # "arr$":[Lorg/apache/http/Header;
    .end local v10    # "cookie":Lorg/apache/http/Header;
    .end local v11    # "cookieHeaders":[Lorg/apache/http/Header;
    .end local v21    # "i$":I
    .end local v25    # "len$":I
    .end local v46    # "setCookieString":Ljava/lang/String;
    .restart local v49    # "streamFidelityHeaders":[Lorg/apache/http/Header;
    .restart local v53    # "value":Ljava/lang/String;
    :cond_2d
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Server sent unrecognized stream fidelity value: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    invoke-virtual/range {v54 .. v55}, Lcom/google/android/music/download/DownloadState;->setStreamFidelity(I)V

    goto :goto_d

    .line 626
    .end local v53    # "value":Ljava/lang/String;
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    invoke-virtual/range {v54 .. v55}, Lcom/google/android/music/download/DownloadState;->setStreamFidelity(I)V

    goto :goto_d

    .line 638
    .end local v49    # "streamFidelityHeaders":[Lorg/apache/http/Header;
    .restart local v5    # "arr$":[Lorg/apache/http/Header;
    .restart local v10    # "cookie":Lorg/apache/http/Header;
    .restart local v11    # "cookieHeaders":[Lorg/apache/http/Header;
    .restart local v21    # "i$":I
    .restart local v25    # "len$":I
    .restart local v46    # "setCookieString":Ljava/lang/String;
    :cond_2f
    const/16 v54, 0x3d

    move-object/from16 v0, v46

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v30

    .line 639
    .local v30, "nameLength":I
    const/16 v54, 0x3b

    move-object/from16 v0, v46

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v17

    .line 640
    .local v17, "endOfCookieIndex":I
    const/16 v54, -0x1

    move/from16 v0, v30

    move/from16 v1, v54

    if-eq v0, v1, :cond_30

    const/16 v54, -0x1

    move/from16 v0, v17

    move/from16 v1, v54

    if-ne v0, v1, :cond_31

    .line 641
    :cond_30
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Invalid cookie format: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 644
    :cond_31
    const/16 v54, 0x0

    move-object/from16 v0, v46

    move/from16 v1, v54

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 645
    .local v12, "cookieName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mPassthroughCookies:Ljava/util/TreeSet;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-virtual {v0, v12}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_2c

    .line 646
    const/16 v54, 0x0

    add-int/lit8 v55, v17, 0x1

    move-object/from16 v0, v46

    move/from16 v1, v54

    move/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v13, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 649
    .end local v5    # "arr$":[Lorg/apache/http/Header;
    .end local v10    # "cookie":Lorg/apache/http/Header;
    .end local v11    # "cookieHeaders":[Lorg/apache/http/Header;
    .end local v12    # "cookieName":Ljava/lang/String;
    .end local v17    # "endOfCookieIndex":I
    .end local v21    # "i$":I
    .end local v25    # "len$":I
    .end local v27    # "location":Lorg/apache/http/Header;
    .end local v30    # "nameLength":I
    .end local v46    # "setCookieString":Ljava/lang/String;
    :cond_32
    const/16 v54, 0x191

    move/from16 v0, v47

    move/from16 v1, v54

    if-ne v0, v1, :cond_35

    .line 650
    const-string v29, "Received 401 Unauthorized from server."

    .line 651
    .local v29, "msg":Ljava/lang/String;
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_33

    .line 652
    const-string v54, "MplayHandler"

    move-object/from16 v0, v54

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    :cond_33
    if-eqz v4, :cond_34

    .line 655
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mMusicAuthInfo:Lcom/google/android/music/sync/google/MusicAuthInfo;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-virtual {v0, v4, v6}, Lcom/google/android/music/sync/google/MusicAuthInfo;->invalidateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 656
    if-eqz v41, :cond_34

    if-nez v36, :cond_34

    .line 658
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/music/download/MplayHandler;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    .line 659
    const/16 v41, 0x0

    .line 660
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_6

    const-string v54, "MplayHandler"

    const-string v55, "Will retry with updated token"

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 664
    :cond_34
    new-instance v54, Lorg/apache/http/client/HttpResponseException;

    move-object/from16 v0, v54

    move/from16 v1, v47

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v54

    .line 665
    .end local v29    # "msg":Ljava/lang/String;
    :cond_35
    const/16 v54, 0x193

    move/from16 v0, v47

    move/from16 v1, v54

    if-ne v0, v1, :cond_3c

    .line 666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "X-Rejected-Reason"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v39

    .line 668
    .local v39, "rejectionReasonHeader":Lorg/apache/http/Header;
    if-eqz v39, :cond_3b

    .line 669
    invoke-interface/range {v39 .. v39}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v37

    .line 670
    .local v37, "rejectionReason":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v54

    if-nez v54, :cond_3b

    .line 671
    const/16 v38, 0x0

    .line 672
    .local v38, "rejectionReasonEnum":Lcom/google/android/music/download/ServerRejectionException$RejectionReason;
    const-string v54, "DEVICE_NOT_AUTHORIZED"

    move-object/from16 v0, v54

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v54

    if-eqz v54, :cond_37

    .line 674
    sget-object v38, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->DEVICE_NOT_AUTHORIZED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    .line 688
    :cond_36
    :goto_10
    if-eqz v38, :cond_3a

    .line 691
    new-instance v54, Lcom/google/android/music/download/ServerRejectionException;

    move-object/from16 v0, v54

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/google/android/music/download/ServerRejectionException;-><init>(Lcom/google/android/music/download/ServerRejectionException$RejectionReason;)V

    throw v54

    .line 675
    :cond_37
    const-string v54, "ANOTHER_STREAM_BEING_PLAYED"

    move-object/from16 v0, v54

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v54

    if-eqz v54, :cond_38

    .line 677
    sget-object v38, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ANOTHER_STREAM_BEING_PLAYED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    goto :goto_10

    .line 678
    :cond_38
    const-string v54, "STREAM_RATE_LIMIT_REACHED"

    move-object/from16 v0, v54

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v54

    if-eqz v54, :cond_39

    .line 680
    sget-object v38, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->STREAM_RATE_LIMIT_REACHED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    goto :goto_10

    .line 681
    :cond_39
    const-string v54, "TRACK_NOT_IN_SUBSCRIPTION"

    move-object/from16 v0, v54

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v54

    if-eqz v54, :cond_36

    .line 683
    sget-object v38, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->TRACK_NOT_IN_SUBSCRIPTION:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/download/DownloadUtils;->purgeNautilusTrackByLocalId(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;)V

    goto :goto_10

    .line 693
    :cond_3a
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Server returned an unknown rejection reason: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    .end local v37    # "rejectionReason":Ljava/lang/String;
    .end local v38    # "rejectionReasonEnum":Lcom/google/android/music/download/ServerRejectionException$RejectionReason;
    :cond_3b
    new-instance v54, Lorg/apache/http/client/HttpResponseException;

    const-string v55, "Unable to stream due to 403 error"

    move-object/from16 v0, v54

    move/from16 v1, v47

    move-object/from16 v2, v55

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v54

    .line 700
    .end local v39    # "rejectionReasonHeader":Lorg/apache/http/Header;
    :cond_3c
    const/16 v54, 0x194

    move/from16 v0, v47

    move/from16 v1, v54

    if-ne v0, v1, :cond_3e

    .line 701
    const-string v29, "Unable to download stream due to 404 (file not found) error"

    .line 702
    .restart local v29    # "msg":Ljava/lang/String;
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_3d

    .line 703
    const-string v54, "MplayHandler"

    move-object/from16 v0, v54

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    :cond_3d
    new-instance v54, Lorg/apache/http/client/HttpResponseException;

    move-object/from16 v0, v54

    move/from16 v1, v47

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v54

    .line 706
    .end local v29    # "msg":Ljava/lang/String;
    :cond_3e
    const/16 v54, 0x1f7

    move/from16 v0, v47

    move/from16 v1, v54

    if-ne v0, v1, :cond_40

    .line 707
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "Retry-After"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v54

    if-eqz v54, :cond_3f

    .line 709
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    move-object/from16 v54, v0

    const-string v55, "Retry-After"

    invoke-interface/range {v54 .. v55}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v54

    invoke-interface/range {v54 .. v54}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v54 .. v54}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    .line 711
    .local v42, "retryAfterInSeconds":J
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Server said to retry after "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v55

    const-string v56, " seconds"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    new-instance v54, Lcom/google/android/music/download/ServiceUnavailableException;

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Unable to download stream due to 503 (Service Unavailable) error.  Unavailable for "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v55

    const-string v56, " seconds."

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, v54

    move-wide/from16 v1, v42

    move-object/from16 v3, v55

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/download/ServiceUnavailableException;-><init>(JLjava/lang/String;)V

    throw v54
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    .line 716
    .end local v42    # "retryAfterInSeconds":J
    :catch_2
    move-exception v31

    .line 719
    .local v31, "ne":Ljava/lang/NumberFormatException;
    new-instance v54, Lorg/apache/http/client/HttpResponseException;

    const-string v55, "Unable to download stream due to 503 error."

    move-object/from16 v0, v54

    move/from16 v1, v47

    move-object/from16 v2, v55

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v54

    .line 723
    .end local v31    # "ne":Ljava/lang/NumberFormatException;
    :cond_3f
    const-string v54, "MplayHandler"

    const-string v55, "Received 503 with no Retry-After header"

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    new-instance v54, Lorg/apache/http/client/HttpResponseException;

    const-string v55, "Unable to download stream due to 503 error."

    move-object/from16 v0, v54

    move/from16 v1, v47

    move-object/from16 v2, v55

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v54

    .line 728
    :cond_40
    sget-boolean v54, Lcom/google/android/music/download/MplayHandler;->LOGV:Z

    if-eqz v54, :cond_41

    .line 729
    const-string v54, "MplayHandler"

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Unable to download stream due to HTTP error "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-static/range {v54 .. v55}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    :cond_41
    new-instance v54, Lorg/apache/http/client/HttpResponseException;

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v56, "Unable to download stream due to HTTP error "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    move-object/from16 v0, v55

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, v54

    move/from16 v1, v47

    move-object/from16 v2, v55

    invoke-direct {v0, v1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v54

    .line 741
    .end local v18    # "entity":Lorg/apache/http/HttpEntity;
    .end local v33    # "params":Lorg/apache/http/params/HttpParams;
    .end local v47    # "status":I
    .end local v48    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_42
    new-instance v54, Ljava/io/IOException;

    const-string v55, "Unable to retrieve stream"

    invoke-direct/range {v54 .. v55}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v54
.end method

.method public prepareInputStream()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/android/music/download/MplayHandler;->getDownloadStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/download/MplayHandler;->wrapInThrottledStreamIfNeed(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    .line 217
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mInputStream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadState:Lcom/google/android/music/download/DownloadState;

    invoke-virtual {v0}, Lcom/google/android/music/download/DownloadState;->isCpOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->updateNautilusTimestamp()V

    .line 220
    :cond_0
    return-void
.end method

.method public releaseConnection()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/music/download/MplayHandler;->mRequest:Lorg/apache/http/client/methods/HttpRequestBase;

    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-static {v0, v1}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    .line 207
    return-void
.end method

.method public setSyncAccount()V
    .locals 4

    .prologue
    .line 195
    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/download/ContentIdentifier;->isDefaultDomain()Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    :goto_0
    return-void

    .line 199
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getSourceAccount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/Store;->getAccountByHash(I)Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/download/MplayHandler;->mAccount:Landroid/accounts/Account;
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, "e":Lcom/google/android/music/sync/common/ProviderException;
    const-string v1, "MplayHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find account for sourceAccount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/download/MplayHandler;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackDownloadRequest;->getSourceAccount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
