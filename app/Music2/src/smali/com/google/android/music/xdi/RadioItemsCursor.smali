.class Lcom/google/android/music/xdi/RadioItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "RadioItemsCursor.java"


# static fields
.field static final PROJECTION_PLAYLIST:[Ljava/lang/String;

.field static final PROJECTION_RADIO_STATION:[Ljava/lang/String;


# instance fields
.field private final mArtistItemHeight:I

.field private final mArtistItemWidth:I

.field private final mContext:Landroid/content/Context;

.field private final mHeaderId:J

.field private final mImageHeight:I

.field private final mImageWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "radio_name"

    aput-object v1, v0, v3

    const-string v1, "radio_art"

    aput-object v1, v0, v4

    const-string v1, "radio_seed_source_type"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/xdi/RadioItemsCursor;->PROJECTION_RADIO_STATION:[Ljava/lang/String;

    .line 44
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "playlist_name"

    aput-object v1, v0, v3

    const-string v1, "playlist_type"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/xdi/RadioItemsCursor;->PROJECTION_PLAYLIST:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "headerId"    # J

    .prologue
    .line 63
    invoke-static {p1, p3, p4}, Lcom/google/android/music/xdi/RadioItemsCursor;->getCursorForHeader(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 64
    iput-object p1, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mContext:Landroid/content/Context;

    .line 65
    iput-wide p3, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mHeaderId:J

    .line 67
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mImageWidth:I

    .line 68
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mImageHeight:I

    .line 69
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemWidthDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mArtistItemWidth:I

    .line 70
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistItemHeightDp(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mArtistItemHeight:I

    .line 71
    return-void
.end method

.method private extractDataForMyStations([Ljava/lang/Object;)V
    .locals 14
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 114
    .local v3, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 115
    .local v6, "id":J
    const/4 v11, 0x1

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 116
    .local v9, "name":Ljava/lang/String;
    const/4 v11, 0x3

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 117
    .local v10, "seedType":I
    const/4 v11, 0x4

    if-ne v10, v11, :cond_3

    const/4 v8, 0x1

    .line 118
    .local v8, "isArtistSeedType":Z
    :goto_0
    const/4 v11, 0x2

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "artUriEncoded":Ljava/lang/String;
    const/4 v0, 0x0

    .line 120
    .local v0, "artUri":Ljava/lang/String;
    iget-object v11, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0b0109

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 125
    .local v4, "description":Ljava/lang/String;
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 126
    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "artUris":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v11, v2

    if-lez v11, :cond_0

    .line 128
    const/4 v11, 0x0

    aget-object v0, v2, v11

    .line 131
    .end local v2    # "artUris":[Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 132
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultRadioArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 135
    :cond_1
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "container"

    const/4 v13, 0x4

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v11

    const-string v12, "id"

    invoke-virtual {v11, v12, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v11

    const-string v12, "name"

    invoke-virtual {v11, v12, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    const-string v12, "art_uri"

    invoke-virtual {v11, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 141
    .local v5, "intent":Landroid/content/Intent;
    const-string v11, "_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {p0, p1, v11, v12}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 142
    const-string v11, "display_name"

    invoke-virtual {p0, p1, v11, v9}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 143
    const-string v11, "display_description"

    invoke-virtual {p0, p1, v11, v4}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 144
    const-string v11, "image_uri"

    invoke-virtual {p0, p1, v11, v0}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 147
    if-eqz v8, :cond_2

    .line 148
    const-string v11, "width"

    iget v12, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mArtistItemWidth:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {p0, p1, v11, v12}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    const-string v11, "height"

    iget v12, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mArtistItemHeight:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {p0, p1, v11, v12}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151
    :cond_2
    const-string v11, "intent_uri"

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, p1, v11, v12}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 152
    return-void

    .line 117
    .end local v0    # "artUri":Ljava/lang/String;
    .end local v1    # "artUriEncoded":Ljava/lang/String;
    .end local v4    # "description":Ljava/lang/String;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v8    # "isArtistSeedType":Z
    :cond_3
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 120
    .restart local v0    # "artUri":Ljava/lang/String;
    .restart local v1    # "artUriEncoded":Ljava/lang/String;
    .restart local v8    # "isArtistSeedType":Z
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0b010a

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1
.end method

.method private extractDataForSuggestedMixes([Ljava/lang/Object;)V
    .locals 14
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v13, 0x1

    .line 155
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 157
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 158
    .local v4, "id":J
    invoke-interface {v1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 159
    .local v6, "name":Ljava/lang/String;
    const/4 v10, 0x2

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 160
    .local v9, "type":I
    iget v10, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mImageWidth:I

    iget v11, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mImageHeight:I

    invoke-static {v4, v5, v10, v11}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "artUri":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "container"

    const/4 v12, 0x3

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "name"

    invoke-virtual {v10, v11, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    const-string v11, "id"

    invoke-virtual {v10, v11, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    .line 171
    .local v3, "intent":Landroid/content/Intent;
    move-object v8, p0

    .line 172
    .local v8, "ref":Lcom/google/android/music/xdi/RadioItemsCursor;
    iget-object v10, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v10, v8}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    .line 174
    .local v7, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    iget-object v10, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mContext:Landroid/content/Context;

    invoke-static {v10, v7, v9}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 176
    .local v2, "description":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 179
    const-string v10, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {p0, p1, v10, v11}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 180
    const-string v10, "display_name"

    invoke-virtual {p0, p1, v10, v6}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 181
    const-string v10, "display_description"

    invoke-virtual {p0, p1, v10, v2}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 182
    const-string v10, "image_uri"

    invoke-virtual {p0, p1, v10, v0}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 183
    const-string v10, "intent_uri"

    invoke-virtual {v3, v13}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, p1, v10, v11}, Lcom/google/android/music/xdi/RadioItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 184
    return-void

    .line 176
    .end local v2    # "description":Ljava/lang/String;
    :catchall_0
    move-exception v10

    invoke-static {v8}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v10
.end method

.method private static getCursorForHeader(Landroid/content/Context;J)Landroid/database/Cursor;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerId"    # J

    .prologue
    .line 74
    long-to-int v0, p1

    packed-switch v0, :pswitch_data_0

    .line 84
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 76
    :pswitch_0
    new-instance v0, Lcom/google/android/music/xdi/IAmFeelingLuckyRadioCursor;

    invoke-direct {v0, p0}, Lcom/google/android/music/xdi/IAmFeelingLuckyRadioCursor;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 79
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getMyRadioStations(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 82
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getSuggestedMixes(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getMyRadioStations(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 187
    sget-object v1, Lcom/google/android/music/store/MusicContent$RadioStations;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/xdi/RadioItemsCursor;->PROJECTION_RADIO_STATION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static getSuggestedMixes(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 193
    invoke-static {}, Lcom/google/android/music/store/MusicContent$Playlists;->getSuggestedMixesUri()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/RadioItemsCursor;->PROJECTION_PLAYLIST:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mHeaderId:J

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_0

    .line 104
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/xdi/RadioItemsCursor;->mHeaderId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v0, 0x0

    .line 108
    :goto_0
    return v0

    .line 93
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RadioItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/music/xdi/XdiUtils;->extractDataFromItemCursor(Landroid/database/Cursor;[Ljava/lang/Object;)V

    .line 108
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 97
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RadioItemsCursor;->extractDataForMyStations([Ljava/lang/Object;)V

    goto :goto_1

    .line 101
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RadioItemsCursor;->extractDataForSuggestedMixes([Ljava/lang/Object;)V

    goto :goto_1

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
