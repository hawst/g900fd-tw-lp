.class public Lcom/google/android/music/leanback/Icon$Builder;
.super Ljava/lang/Object;
.source "Icon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/Icon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field mIcon:Lcom/google/android/music/leanback/Icon;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/google/android/music/leanback/Icon;

    invoke-direct {v0}, Lcom/google/android/music/leanback/Icon;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/Icon$Builder;->mIcon:Lcom/google/android/music/leanback/Icon;

    .line 16
    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/music/leanback/Icon;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/leanback/Icon$Builder;->mIcon:Lcom/google/android/music/leanback/Icon;

    return-object v0
.end method

.method public iconResourceId(I)Lcom/google/android/music/leanback/Icon$Builder;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/leanback/Icon$Builder;->mIcon:Lcom/google/android/music/leanback/Icon;

    iput p1, v0, Lcom/google/android/music/leanback/Icon;->resourceId:I

    .line 25
    return-object p0
.end method

.method public intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Icon$Builder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/music/leanback/Icon$Builder;->mIcon:Lcom/google/android/music/leanback/Icon;

    iput-object p1, v0, Lcom/google/android/music/leanback/Icon;->intent:Landroid/content/Intent;

    .line 30
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/google/android/music/leanback/Icon$Builder;
    .locals 1
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/leanback/Icon$Builder;->mIcon:Lcom/google/android/music/leanback/Icon;

    iput-object p1, v0, Lcom/google/android/music/leanback/Icon;->label:Ljava/lang/String;

    .line 20
    return-object p0
.end method
