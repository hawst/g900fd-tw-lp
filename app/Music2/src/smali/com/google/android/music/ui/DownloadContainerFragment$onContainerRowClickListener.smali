.class final Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;
.super Ljava/lang/Object;
.source "DownloadContainerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/DownloadContainerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "onContainerRowClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/DownloadContainerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/DownloadContainerFragment;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;

    .line 122
    .local v12, "vh":Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;
    sget-object v2, Lcom/google/android/music/ui/DownloadContainerFragment$2;->$SwitchMap$com$google$android$music$ui$DownloadContainerFragment$ContainerType:[I

    iget-object v3, v12, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    invoke-virtual {v3}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 143
    const-string v2, "DownloadContainer"

    const-string v3, "Invalid download type"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :goto_0
    :pswitch_0
    return-void

    .line 124
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, v12, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->id:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/ui/TrackContainerActivity;->showAlbum(Landroid/content/Context;JLcom/google/android/music/ui/cardlib/model/Document;ZLandroid/view/View;)V

    goto :goto_0

    .line 128
    :pswitch_2
    new-instance v1, Lcom/google/android/music/medialist/PlaylistSongList;

    iget-wide v2, v12, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->id:J

    iget-object v4, v12, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 132
    .local v1, "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    iget-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/music/ui/TrackContainerActivity;->showPlaylist(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 135
    .end local v1    # "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    :pswitch_3
    iget-wide v2, v12, Lcom/google/android/music/ui/DownloadContainerFragment$ViewHolder;->id:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-virtual {v5}, Lcom/google/android/music/ui/DownloadContainerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v0

    .line 137
    .local v0, "autolist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    iget-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment$onContainerRowClickListener;->this$0:Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/DownloadContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/music/ui/TrackContainerActivity;->showPlaylist(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
