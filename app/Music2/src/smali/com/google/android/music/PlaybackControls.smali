.class public Lcom/google/android/music/PlaybackControls;
.super Ljava/lang/Object;
.source "PlaybackControls.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mContext:Landroid/content/Context;

.field private mNextButton:Lcom/google/android/music/RepeatingImageButton;

.field private mPauseButton:Lcom/google/android/music/PlayPauseButton;

.field private mPrevButton:Lcom/google/android/music/RepeatingImageButton;

.field private mRepeatButton:Landroid/widget/ImageView;

.field private mShuffleButton:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/RepeatingImageButton;Lcom/google/android/music/PlayPauseButton;Lcom/google/android/music/RepeatingImageButton;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prevButton"    # Lcom/google/android/music/RepeatingImageButton;
    .param p3, "playPauseButton"    # Lcom/google/android/music/PlayPauseButton;
    .param p4, "nextButton"    # Lcom/google/android/music/RepeatingImageButton;
    .param p5, "repeatButton"    # Landroid/widget/ImageView;
    .param p6, "shuffleButton"    # Landroid/widget/ImageView;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/google/android/music/PlaybackControls;->mPrevButton:Lcom/google/android/music/RepeatingImageButton;

    .line 45
    iput-object p3, p0, Lcom/google/android/music/PlaybackControls;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    .line 46
    iput-object p4, p0, Lcom/google/android/music/PlaybackControls;->mNextButton:Lcom/google/android/music/RepeatingImageButton;

    .line 47
    iput-object p6, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    .line 48
    iput-object p5, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    .line 49
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/music/PlaybackControls;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 51
    invoke-direct {p0}, Lcom/google/android/music/PlaybackControls;->initButtons()V

    .line 52
    return-void
.end method

.method private cycleRepeat()V
    .locals 9

    .prologue
    .line 134
    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 135
    .local v2, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-nez v2, :cond_0

    .line 156
    :goto_0
    return-void

    .line 139
    :cond_0
    :try_start_0
    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getRepeatMode()I

    move-result v1

    .line 140
    .local v1, "mode":I
    const/4 v3, 0x0

    .line 141
    .local v3, "targetRepeatMode":I
    if-nez v1, :cond_1

    .line 142
    const/4 v3, 0x2

    .line 148
    :goto_1
    invoke-interface {v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->setRepeatMode(I)V

    .line 149
    invoke-direct {p0, v3}, Lcom/google/android/music/PlaybackControls;->setRepeatButtonImage(I)V

    .line 150
    iget-object v4, p0, Lcom/google/android/music/PlaybackControls;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v5, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    invoke-direct {p0, v3}, Lcom/google/android/music/PlaybackControls;->getRepeatAccessibleText(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v6, v7, v8}, Lcom/google/android/music/utils/ViewUtils;->announceTextForAccessibility(Landroid/view/accessibility/AccessibilityManager;Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    .end local v1    # "mode":I
    .end local v3    # "targetRepeatMode":I
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "PlaybackControls"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 143
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "mode":I
    .restart local v3    # "targetRepeatMode":I
    :cond_1
    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 144
    const/4 v3, 0x1

    goto :goto_1

    .line 146
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private doPauseResume()V
    .locals 5

    .prologue
    .line 179
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 180
    .local v1, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-eqz v1, :cond_0

    .line 181
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v2

    .line 183
    .local v2, "state":Lcom/google/android/music/playback/PlaybackState;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 184
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->stop()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    invoke-direct {p0, v2}, Lcom/google/android/music/PlaybackControls;->setPauseButtonImage(Lcom/google/android/music/playback/PlaybackState;)V

    .line 195
    .end local v2    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_0
    return-void

    .line 185
    .restart local v2    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 186
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->pause()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "PlaybackControls"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 188
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_2
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->play()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method private getRepeatAccessibleText(I)Ljava/lang/String;
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 160
    packed-switch p1, :pswitch_data_0

    .line 171
    const-string v1, "PlaybackControls"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid repeat mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    const-string v0, ""

    .line 174
    :goto_0
    return-object v0

    .line 162
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    const v2, 0x7f0b01ee

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "accessibility":Ljava/lang/String;
    goto :goto_0

    .line 165
    .end local v0    # "accessibility":Ljava/lang/String;
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    const v2, 0x7f0b01f0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    .restart local v0    # "accessibility":Ljava/lang/String;
    goto :goto_0

    .line 168
    .end local v0    # "accessibility":Ljava/lang/String;
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    const v2, 0x7f0b01ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 169
    .restart local v0    # "accessibility":Ljava/lang/String;
    goto :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getShuffleAccessibleText(I)Ljava/lang/String;
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 119
    packed-switch p1, :pswitch_data_0

    .line 127
    const-string v1, "PlaybackControls"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid shuffle mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const-string v0, ""

    .line 130
    :goto_0
    return-object v0

    .line 121
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    const v2, 0x7f0b01ec

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "accessibility":Ljava/lang/String;
    goto :goto_0

    .line 124
    .end local v0    # "accessibility":Ljava/lang/String;
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    const v2, 0x7f0b01eb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 125
    .restart local v0    # "accessibility":Ljava/lang/String;
    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private initButtons()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mPrevButton:Lcom/google/android/music/RepeatingImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->prepButton(Landroid/view/View;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->prepButton(Landroid/view/View;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mNextButton:Lcom/google/android/music/RepeatingImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->prepButton(Landroid/view/View;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->prepButton(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->prepButton(Landroid/view/View;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/music/PlaybackControls;->refreshButtonImages()V

    .line 61
    return-void
.end method

.method private prepButton(Landroid/view/View;)V
    .locals 0
    .param p1, "button"    # Landroid/view/View;

    .prologue
    .line 251
    if-nez p1, :cond_0

    .line 256
    :goto_0
    return-void

    .line 255
    :cond_0
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private setPauseButtonImage(Lcom/google/android/music/playback/PlaybackState;)V
    .locals 2
    .param p1, "state"    # Lcom/google/android/music/playback/PlaybackState;

    .prologue
    .line 198
    if-nez p1, :cond_0

    .line 209
    :goto_0
    return-void

    .line 202
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/music/playback/PlaybackState;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/PlayPauseButton;->setCurrentPlayState(I)V

    goto :goto_0

    .line 204
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/music/PlayPauseButton;->setCurrentPlayState(I)V

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/music/PlayPauseButton;->setCurrentPlayState(I)V

    goto :goto_0
.end method

.method private setRepeatButtonImage(I)V
    .locals 2
    .param p1, "repeatMode"    # I

    .prologue
    .line 219
    packed-switch p1, :pswitch_data_0

    .line 227
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    const v1, 0x7f020147

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 230
    :goto_0
    return-void

    .line 221
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    const v1, 0x7f020148

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 224
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    const v1, 0x7f020149

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 219
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setRepeatButtonImage(Lcom/google/android/music/playback/PlaybackState;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/music/playback/PlaybackState;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/playback/PlaybackState;->getRepeatMode()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->setRepeatButtonImage(I)V

    goto :goto_0
.end method

.method private setShuffleButtonImage(I)V
    .locals 2
    .param p1, "shuffleMode"    # I

    .prologue
    .line 240
    packed-switch p1, :pswitch_data_0

    .line 245
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    const v1, 0x7f020130

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 248
    :goto_0
    return-void

    .line 242
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    const v1, 0x7f020156

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private setShuffleButtonImage(Lcom/google/android/music/playback/PlaybackState;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/music/playback/PlaybackState;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/playback/PlaybackState;->getShuffleMode()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->setShuffleButtonImage(I)V

    goto :goto_0
.end method

.method private toggleShuffle()V
    .locals 9

    .prologue
    .line 93
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 94
    .local v1, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-nez v1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 98
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getShuffleMode()I

    move-result v2

    .line 99
    .local v2, "shuffle":I
    const/4 v3, 0x0

    .line 100
    .local v3, "targetShuffleMode":I
    if-nez v2, :cond_1

    .line 101
    const/4 v3, 0x1

    .line 107
    :goto_1
    invoke-interface {v1, v3}, Lcom/google/android/music/playback/IMusicPlaybackService;->setShuffleMode(I)V

    .line 108
    invoke-direct {p0, v3}, Lcom/google/android/music/PlaybackControls;->setShuffleButtonImage(I)V

    .line 109
    iget-object v4, p0, Lcom/google/android/music/PlaybackControls;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v5, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    invoke-direct {p0, v3}, Lcom/google/android/music/PlaybackControls;->getShuffleAccessibleText(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/PlaybackControls;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v6, v7, v8}, Lcom/google/android/music/utils/ViewUtils;->announceTextForAccessibility(Landroid/view/accessibility/AccessibilityManager;Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    .end local v2    # "shuffle":I
    .end local v3    # "targetShuffleMode":I
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "PlaybackControls"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 102
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v2    # "shuffle":I
    .restart local v3    # "targetShuffleMode":I
    :cond_1
    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    .line 103
    const/4 v3, 0x0

    goto :goto_1

    .line 105
    :cond_2
    :try_start_1
    const-string v4, "MediaPlaybackActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid shuffle mode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 66
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 67
    .local v1, "service":Lcom/google/android/music/playback/IMusicPlaybackService;
    iget-object v2, p0, Lcom/google/android/music/PlaybackControls;->mShuffleButton:Landroid/widget/ImageView;

    if-ne p1, v2, :cond_1

    .line 68
    invoke-direct {p0}, Lcom/google/android/music/PlaybackControls;->toggleShuffle()V

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/PlaybackControls;->mRepeatButton:Landroid/widget/ImageView;

    if-ne p1, v2, :cond_2

    .line 70
    invoke-direct {p0}, Lcom/google/android/music/PlaybackControls;->cycleRepeat()V

    goto :goto_0

    .line 71
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/PlaybackControls;->mPauseButton:Lcom/google/android/music/PlayPauseButton;

    if-ne p1, v2, :cond_3

    .line 72
    invoke-direct {p0}, Lcom/google/android/music/PlaybackControls;->doPauseResume()V

    goto :goto_0

    .line 73
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/PlaybackControls;->mPrevButton:Lcom/google/android/music/RepeatingImageButton;

    if-ne p1, v2, :cond_4

    .line 74
    if-eqz v1, :cond_0

    .line 76
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->prev()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "PlaybackControls"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 80
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/PlaybackControls;->mNextButton:Lcom/google/android/music/RepeatingImageButton;

    if-ne p1, v2, :cond_5

    .line 81
    if-eqz v1, :cond_0

    .line 83
    :try_start_1
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->next()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 84
    :catch_1
    move-exception v0

    .line 85
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v2, "PlaybackControls"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 88
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_5
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown view clicked on: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public refreshButtonImages()V
    .locals 1

    .prologue
    .line 259
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v0

    .line 260
    .local v0, "state":Lcom/google/android/music/playback/PlaybackState;
    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->setPauseButtonImage(Lcom/google/android/music/playback/PlaybackState;)V

    .line 261
    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->setRepeatButtonImage(Lcom/google/android/music/playback/PlaybackState;)V

    .line 262
    invoke-direct {p0, v0}, Lcom/google/android/music/PlaybackControls;->setShuffleButtonImage(Lcom/google/android/music/playback/PlaybackState;)V

    .line 263
    return-void
.end method
