.class public Lcom/google/android/music/store/ProjectionUtils;
.super Ljava/lang/Object;
.source "ProjectionUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/ProjectionUtils$1;,
        Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    }
.end annotation


# static fields
.field private static sProjectionMaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 51
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    .line 457
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 459
    .local v0, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    const-string v1, "Album"

    const-string v2, "album"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v1, "AlbumArtist"

    const-string v2, "albumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v1, "Artist"

    const-string v2, "artist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v1, "duration"

    const-string v2, "durationMillis"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v1, "hasLocal"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 464
    const-string v1, "hasRemote"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 465
    const-string v1, "searchName"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const-string v1, "searchSortName"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v1, "year"

    const-string v2, "year"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v1, "searchType"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 471
    const-string v1, "album"

    const-string v2, "album"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const-string v1, "AlbumArtist"

    const-string v2, "albumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string v1, "CanonicalAlbum"

    const-string v2, "album"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const-string v1, "artistSort"

    const-string v2, "artist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v1, "composer"

    const-string v2, "composer"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string v1, "duration"

    const-string v2, "durationMillis"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const-string v1, "Genre"

    const-string v2, "genre"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v1, "CanonicalName"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v1, "artist"

    const-string v2, "artist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const-string v1, "track"

    const-string v2, "trackNumber"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string v1, "year"

    const-string v2, "year"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const-string v1, "StoreAlbumId"

    const-string v2, "albumId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const-string v1, "StoreId"

    const-string v2, "storeId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const-string v1, "isAllPersistentNautilus"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 488
    const-string v1, "Size"

    const-string v2, "estimatedSize"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    const-string v1, "suggest_text_1"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const-string v1, "suggest_text_2"

    const-string v2, "album"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/sync/google/model/Track;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .end local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 499
    .restart local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    const-string v1, "Album"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const-string v1, "AlbumArtist"

    const-string v2, "albumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    const-string v1, "AlbumId"

    const-string v2, "albumId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    const-string v1, "Artist"

    const-string v2, "artist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v1, "hasLocal"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 504
    const-string v1, "hasRemote"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 505
    const-string v1, "searchName"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const-string v1, "searchSortName"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    const-string v1, "searchType"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 510
    const-string v1, "album_name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const-string v1, "album_art"

    const-string v2, "albumArtRef"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const-string v1, "album_id"

    const-string v2, "albumId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const-string v1, "album_sort"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-string v1, "album_artist"

    const-string v2, "albumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    const-string v1, "album_artist_sort"

    const-string v2, "albumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    const-string v1, "StoreAlbumId"

    const-string v2, "albumId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const-string v1, "hasPersistNautilus"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 518
    const-string v1, "album_year"

    const-string v2, "year"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    const-string v1, "album_description"

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    const-string v1, "suggest_text_1"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    const-string v1, "suggest_text_2"

    const-string v2, "albumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    const-string v1, "suggest_icon_1"

    const-string v2, "albumArtRef"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    const-string v1, "artworkUrl"

    const-string v2, "albumArtRef"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/cloudclient/AlbumJson;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .end local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 534
    .restart local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    const-string v1, "AlbumArtist"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const-string v1, "AlbumArtistId"

    const-string v2, "artistId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    const-string v1, "Artist"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v1, "hasLocal"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 538
    const-string v1, "hasRemote"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 539
    const-string v1, "searchName"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const-string v1, "searchSortName"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    const-string v1, "searchType"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 544
    const-string v1, "artist"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const-string v1, "artistSort"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    const-string v1, "ArtistMetajamId"

    const-string v2, "artistId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string v1, "suggest_text_1"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v1, "suggest_icon_1"

    const-string v2, "artistArtRef"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const-string v1, "artworkUrl"

    const-string v2, "artistArtRef"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/cloudclient/ArtistJson;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .end local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 558
    .restart local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    const-string v1, "_id"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const-string v1, "description"

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .end local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 567
    .restart local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    const-string v1, "genreServerId"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    const-string v1, "parentGenreId"

    const-string v2, "parentId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const-string v1, "searchType"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 572
    const-string v1, "searchName"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/cloudclient/MusicGenreJson;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .end local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 578
    .restart local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->CREATION_TIMESTAMP:Ljava/lang/String;

    const-string v2, "creationTimestamp"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->LAST_MODIFIED_TIMESTAMP:Ljava/lang/String;

    const-string v2, "lastModifiedTimestamp"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->NAME:Ljava/lang/String;

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->SHARE_TOKEN:Ljava/lang/String;

    const-string v2, "shareToken"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->OWNER_NAME:Ljava/lang/String;

    const-string v2, "ownerName"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->DESCRIPTION:Ljava/lang/String;

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    sget-object v1, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->OWNER_PROFILE_PHOTO_URL:Ljava/lang/String;

    const-string v2, "ownerProfilePhotoUrl"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const-string v1, "searchName"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    const-string v1, "Name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    const-string v1, "ShareToken"

    const-string v2, "shareToken"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const-string v1, "OwnerName"

    const-string v2, "ownerName"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v1, "Description"

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    const-string v1, "OwnerProfilePhotoUrl"

    const-string v2, "ownerProfilePhotoUrl"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 599
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .end local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 600
    .restart local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    const-string v1, "situation_id"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const-string v1, "situation_title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    const-string v1, "situation_description"

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const-string v1, "situation_image_url"

    const-string v2, "imageUrl"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    const-string v1, "situation_wide_image_url"

    const-string v2, "wideImageUrl"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/cloudclient/SituationJson;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    new-instance v0, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .end local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    invoke-direct {v0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;-><init>(Lcom/google/android/music/store/ProjectionUtils$1;)V

    .line 609
    .restart local v0    # "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    const-string v1, "radio_name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const-string v1, "radio_description"

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->putField(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    sget-object v1, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    const-class v2, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/music/store/ProjectionUtils;->getRandomId()I

    move-result v0

    return v0
.end method

.method private static getRandomId()I
    .locals 4

    .prologue
    .line 353
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x40c3880000000000L    # 10000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x64

    return v0
.end method

.method public static isFauxNautilusId(J)Z
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 342
    const-wide/16 v0, -0x64

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHasDifferentArtistProjection([Ljava/lang/String;)Z
    .locals 4
    .param p0, "cols"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 334
    if-eqz p0, :cond_0

    array-length v2, p0

    if-ne v2, v0, :cond_0

    aget-object v2, p0, v1

    const-string v3, "HasDifferentTrackArtists"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static project(Lcom/google/android/music/cloudclient/AlbumJson;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 5
    .param p0, "album"    # Lcom/google/android/music/cloudclient/AlbumJson;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    .line 79
    if-nez p0, :cond_1

    .line 80
    const/4 v2, 0x0

    .line 117
    :cond_0
    return-object v2

    .line 83
    :cond_1
    sget-object v3, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 84
    .local v1, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v3, p1

    new-array v2, v3, [Ljava/lang/Object;

    .line 85
    .local v2, "row":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 86
    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 87
    aget-object v3, p1, v0

    invoke-virtual {v1, p0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 85
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_3
    aget-object v3, p1, v0

    const-string v4, "itemCount"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    aget-object v3, p1, v0

    const-string v4, "SongCount"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 90
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 91
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 93
    :cond_5
    aget-object v3, p1, v0

    const-string v4, "ArtistMetajamId"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 94
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mArtistId:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mArtistId:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 95
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mArtistId:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 97
    :cond_6
    aget-object v3, p1, v0

    const-string v4, "album_description_attr_source_title"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 98
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    if-eqz v3, :cond_2

    .line 99
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;->mSourceTitle:Ljava/lang/String;

    aput-object v3, v2, v0

    goto :goto_1

    .line 101
    :cond_7
    aget-object v3, p1, v0

    const-string v4, "album_description_attr_source_url"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 102
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    if-eqz v3, :cond_2

    .line 103
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;->mSourceUrl:Ljava/lang/String;

    aput-object v3, v2, v0

    goto :goto_1

    .line 105
    :cond_8
    aget-object v3, p1, v0

    const-string v4, "album_description_attr_license_title"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 106
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    if-eqz v3, :cond_2

    .line 107
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;->mLicenseTitle:Ljava/lang/String;

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 109
    :cond_9
    aget-object v3, p1, v0

    const-string v4, "album_description_attr_license_url"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 110
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    if-eqz v3, :cond_2

    .line 111
    iget-object v3, p0, Lcom/google/android/music/cloudclient/AlbumJson;->mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;->mLicenseUrl:Ljava/lang/String;

    aput-object v3, v2, v0

    goto/16 :goto_1
.end method

.method public static project(Lcom/google/android/music/cloudclient/ArtistJson;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 5
    .param p0, "artist"    # Lcom/google/android/music/cloudclient/ArtistJson;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    .line 55
    if-nez p0, :cond_1

    .line 56
    const/4 v2, 0x0

    .line 71
    :cond_0
    return-object v2

    .line 58
    :cond_1
    sget-object v3, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 59
    .local v1, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v3, p1

    new-array v2, v3, [Ljava/lang/Object;

    .line 60
    .local v2, "row":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 61
    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 62
    aget-object v3, p1, v0

    invoke-virtual {v1, p0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 60
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_3
    aget-object v3, p1, v0

    const-string v4, "albumCount"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 64
    iget-object v3, p0, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 65
    iget-object v3, p0, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1
.end method

.method public static project(Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 9
    .param p0, "group"    # Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 180
    if-nez p0, :cond_1

    .line 181
    const/4 v4, 0x0

    .line 215
    :cond_0
    return-object v4

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;->mEntities:Ljava/util/List;

    .line 185
    .local v0, "entities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ExploreEntityJson;>;"
    sget-object v5, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 186
    .local v3, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v5, p1

    new-array v4, v5, [Ljava/lang/Object;

    .line 187
    .local v4, "row":[Ljava/lang/Object;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, p1

    if-ge v2, v5, :cond_0

    .line 188
    aget-object v5, p1, v2

    invoke-virtual {v3, v5}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 189
    aget-object v5, p1, v2

    invoke-virtual {v3, p0, v5}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v2

    .line 187
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 190
    :cond_3
    aget-object v5, p1, v2

    const-string v7, "size"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 192
    if-nez v0, :cond_4

    move v5, v6

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_2

    .line 193
    :cond_5
    aget-object v5, p1, v2

    const-string v7, "groupType"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 195
    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_9

    .line 197
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;

    .line 198
    .local v1, "firstEntity":Lcom/google/android/music/cloudclient/ExploreEntityJson;
    iget-object v5, v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    if-eqz v5, :cond_6

    .line 199
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    .line 200
    :cond_6
    iget-object v5, v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    if-eqz v5, :cond_7

    .line 201
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    .line 202
    :cond_7
    iget-object v5, v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v5, :cond_8

    .line 203
    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    .line 206
    :cond_8
    const-string v5, "ProjectionUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not determine the entity type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 209
    .end local v1    # "firstEntity":Lcom/google/android/music/cloudclient/ExploreEntityJson;
    :cond_9
    const-string v5, "ProjectionUtils"

    const-string v7, "Could not determine the entity type. Entity group empty."

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static project(Lcom/google/android/music/cloudclient/MusicGenreJson;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 8
    .param p0, "genre"    # Lcom/google/android/music/cloudclient/MusicGenreJson;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    .line 219
    if-nez p0, :cond_1

    .line 220
    const/4 v4, 0x0

    .line 245
    :cond_0
    return-object v4

    .line 223
    :cond_1
    sget-object v5, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 224
    .local v3, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v5, p1

    new-array v4, v5, [Ljava/lang/Object;

    .line 225
    .local v4, "row":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p1

    if-ge v1, v5, :cond_0

    .line 226
    aget-object v5, p1, v1

    invoke-virtual {v3, v5}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 227
    aget-object v5, p1, v1

    invoke-virtual {v3, p0, v5}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v1

    .line 225
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 228
    :cond_3
    aget-object v5, p1, v1

    const-string v6, "_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 230
    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicGenreJson;->mId:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_1

    .line 231
    :cond_4
    aget-object v5, p1, v1

    const-string v6, "subgenreCount"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 232
    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicGenreJson;->mChildren:Ljava/util/List;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicGenreJson;->mChildren:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    .line 233
    :cond_6
    aget-object v5, p1, v1

    const-string v6, "genreArtUris"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 234
    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicGenreJson;->mImages:Ljava/util/List;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicGenreJson;->mImages:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 235
    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicGenreJson;->mImages:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v0, v5, [Ljava/lang/String;

    .line 236
    .local v0, "artUrls":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    array-length v5, v0

    if-ge v2, v5, :cond_7

    .line 237
    iget-object v5, p0, Lcom/google/android/music/cloudclient/MusicGenreJson;->mImages:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v5, v5, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    aput-object v5, v0, v2

    .line 236
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 239
    :cond_7
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_1
.end method

.method public static project(Lcom/google/android/music/cloudclient/SituationJson;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 5
    .param p0, "situation"    # Lcom/google/android/music/cloudclient/SituationJson;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    .line 278
    if-nez p0, :cond_1

    .line 279
    const/4 v2, 0x0

    .line 293
    :cond_0
    return-object v2

    .line 282
    :cond_1
    sget-object v3, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 283
    .local v1, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v3, p1

    new-array v2, v3, [Ljava/lang/Object;

    .line 284
    .local v2, "row":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 285
    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 286
    aget-object v3, p1, v0

    invoke-virtual {v1, p0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 284
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_3
    aget-object v3, p1, v0

    const-string v4, "situation_has_subsituations"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 288
    iget-object v3, p0, Lcom/google/android/music/cloudclient/SituationJson;->mSituations:Ljava/util/List;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/music/cloudclient/SituationJson;->mSituations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p0, "track"    # Lcom/google/android/music/cloudclient/TrackJson;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 121
    if-nez p0, :cond_1

    .line 122
    const/4 v2, 0x0

    .line 176
    :cond_0
    return-object v2

    .line 125
    :cond_1
    sget-object v3, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 126
    .local v1, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v3, p1

    new-array v2, v3, [Ljava/lang/Object;

    .line 127
    .local v2, "row":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 128
    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 129
    aget-object v3, p1, v0

    invoke-virtual {v1, p0, v3}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 127
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_3
    aget-object v3, p1, v0

    const-string v4, "Nid"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    aget-object v3, p1, v0

    const-string v4, "SourceId"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 132
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/TrackJson;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 133
    :cond_5
    aget-object v3, p1, v0

    const-string v4, "ArtistMetajamId"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 134
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistId:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistId:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 135
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistId:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 137
    :cond_6
    aget-object v3, p1, v0

    const-string v4, "Domain"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 138
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/TrackJson;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/download/ContentIdentifier$Domain;->getDBValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 139
    :cond_7
    aget-object v3, p1, v0

    const-string v4, "artworkUrl"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    aget-object v3, p1, v0

    const-string v4, "suggest_icon_1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    aget-object v3, p1, v0

    const-string v4, "AlbumArtLocation"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 142
    :cond_8
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtRef:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtRef:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 143
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mAlbumArtRef:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 145
    :cond_9
    aget-object v3, p1, v0

    const-string v4, "ArtistArtLocation"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 146
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistArtRef:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistArtRef:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 147
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mArtistArtRef:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 149
    :cond_a
    aget-object v3, p1, v0

    const-string v4, "trackAvailableForPurchase"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 150
    iget-boolean v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForPurchase:Z

    if-eqz v3, :cond_b

    .line 151
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 153
    :cond_b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 155
    :cond_c
    aget-object v3, p1, v0

    const-string v4, "trackAvailableForSubscription"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 156
    iget-boolean v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mTrackAvailableForSubscription:Z

    if-eqz v3, :cond_d

    .line 157
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 159
    :cond_d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 161
    :cond_e
    aget-object v3, p1, v0

    const-string v4, "Vid"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 162
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    if-eqz v3, :cond_2

    .line 163
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mId:Ljava/lang/String;

    aput-object v3, v2, v0

    goto/16 :goto_1

    .line 165
    :cond_f
    aget-object v3, p1, v0

    const-string v4, "VThumbnailUrl"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 166
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 169
    iget-object v3, p0, Lcom/google/android/music/cloudclient/TrackJson;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mThumbnails:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v3, v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    aput-object v3, v2, v0

    goto/16 :goto_1
.end method

.method public static project(Lcom/google/android/music/sync/google/model/SyncablePlaylist;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 8
    .param p0, "sharedPlaylist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    .line 249
    if-nez p0, :cond_1

    .line 250
    const/4 v4, 0x0

    .line 274
    :cond_0
    return-object v4

    .line 253
    :cond_1
    sget-object v6, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 254
    .local v3, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v6, p1

    new-array v4, v6, [Ljava/lang/Object;

    .line 255
    .local v4, "row":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, p1

    if-ge v1, v6, :cond_0

    .line 256
    aget-object v6, p1, v1

    invoke-virtual {v3, v6}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 257
    aget-object v6, p1, v1

    invoke-virtual {v3, p0, v6}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v1

    .line 255
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 258
    :cond_3
    aget-object v6, p1, v1

    sget-object v7, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;->ART_URL:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    aget-object v6, p1, v1

    const-string v7, "artworkUrl"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 260
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    .line 261
    .local v0, "artUrls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/TrackJson$ImageRef;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 262
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    new-array v5, v6, [Ljava/lang/String;

    .line 263
    .local v5, "urls":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    array-length v6, v5

    if-ge v2, v6, :cond_5

    .line 264
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    aput-object v6, v5, v2

    .line 263
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 266
    :cond_5
    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v1

    goto :goto_1

    .line 268
    .end local v0    # "artUrls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/TrackJson$ImageRef;>;"
    .end local v2    # "j":I
    .end local v5    # "urls":[Ljava/lang/String;
    :cond_6
    aget-object v6, p1, v1

    const-string v7, "ListType"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 269
    const/16 v6, 0x46

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v1

    goto :goto_1
.end method

.method public static project(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 1
    .param p0, "playlistEntry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    invoke-static {v0, p1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static project(Lcom/google/android/music/sync/google/model/SyncableRadioStation;[Ljava/lang/String;)[Ljava/lang/Object;
    .locals 9
    .param p0, "radioStation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .param p1, "cols"    # [Ljava/lang/String;

    .prologue
    .line 297
    if-nez p0, :cond_1

    .line 298
    const/4 v4, 0x0

    .line 330
    :cond_0
    return-object v4

    .line 301
    :cond_1
    sget-object v7, Lcom/google/android/music/store/ProjectionUtils;->sProjectionMaps:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;

    .line 302
    .local v3, "mappings":Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;
    array-length v7, p1

    new-array v4, v7, [Ljava/lang/Object;

    .line 303
    .local v4, "row":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, p1

    if-ge v1, v7, :cond_0

    .line 304
    aget-object v7, p1, v1

    invoke-virtual {v3, v7}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->hasColumn(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 305
    aget-object v7, p1, v1

    invoke-virtual {v3, p0, v7}, Lcom/google/android/music/store/ProjectionUtils$ColumnMappings;->evaluate(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v4, v1

    .line 303
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 306
    :cond_3
    aget-object v7, p1, v1

    const-string v8, "radio_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 307
    iget-object v7, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    aput-object v7, v4, v1

    goto :goto_1

    .line 308
    :cond_4
    aget-object v7, p1, v1

    const-string v8, "radio_art"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    aget-object v7, p1, v1

    const-string v8, "artworkUrl"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 310
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    .line 311
    .local v0, "artUrls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ImageRefJson;>;"
    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    .line 312
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    new-array v6, v7, [Ljava/lang/String;

    .line 313
    .local v6, "urls":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    array-length v7, v6

    if-ge v2, v7, :cond_6

    .line 314
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/cloudclient/ImageRefJson;

    iget-object v7, v7, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    aput-object v7, v6, v2

    .line 313
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 316
    :cond_6
    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v1

    goto :goto_1

    .line 317
    .end local v2    # "j":I
    .end local v6    # "urls":[Ljava/lang/String;
    :cond_7
    iget-object v7, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrl:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 318
    iget-object v7, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrl:Ljava/lang/String;

    aput-object v7, v4, v1

    goto :goto_1

    .line 320
    .end local v0    # "artUrls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ImageRefJson;>;"
    :cond_8
    aget-object v7, p1, v1

    const-string v8, "radio_seed_source_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 321
    iget-object v7, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-virtual {v7}, Lcom/google/android/music/sync/google/model/RadioSeed;->getSourceIdAndType()Landroid/util/Pair;

    move-result-object v5

    .line 322
    .local v5, "sourceIdAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v7, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v7, v4, v1

    goto :goto_1

    .line 323
    .end local v5    # "sourceIdAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_9
    aget-object v7, p1, v1

    const-string v8, "radio_seed_source_type"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 324
    iget-object v7, p0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-virtual {v7}, Lcom/google/android/music/sync/google/model/RadioSeed;->getSourceIdAndType()Landroid/util/Pair;

    move-result-object v5

    .line 325
    .restart local v5    # "sourceIdAndType":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v7, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v7, v4, v1

    goto/16 :goto_1
.end method
