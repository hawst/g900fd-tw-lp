.class Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;
.super Ljava/lang/Object;
.source "AvailableSpaceTracker.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AvailableSpaceTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateFreespaceTask"
.end annotation


# instance fields
.field private mAdded:Z

.field private final mIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mType:I

.field final synthetic this$0:Lcom/google/android/music/AvailableSpaceTracker;


# direct methods
.method public constructor <init>(Lcom/google/android/music/AvailableSpaceTracker;Ljava/util/List;IZ)V
    .locals 0
    .param p3, "type"    # I
    .param p4, "added"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 514
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 515
    iput-object p2, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mIds:Ljava/util/List;

    .line 516
    iput p3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mType:I

    .line 517
    iput-boolean p4, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mAdded:Z

    .line 518
    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v1, 0x1

    .line 521
    const-wide/16 v6, 0x0

    .line 522
    .local v6, "totalSize":J
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # invokes: Lcom/google/android/music/AvailableSpaceTracker;->waitForStoreConnection()V
    invoke-static {v3}, Lcom/google/android/music/AvailableSpaceTracker;->access$900(Lcom/google/android/music/AvailableSpaceTracker;)V

    .line 524
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 525
    .local v4, "id":J
    iget v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mType:I

    packed-switch v3, :pswitch_data_0

    .line 539
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mType:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "id":J
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "AvailableSpaceTracker"

    const-string v8, "Remote error when trying to get size to download"

    invoke-static {v3, v8, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 561
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void

    .line 527
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "id":J
    :pswitch_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # getter for: Lcom/google/android/music/AvailableSpaceTracker;->mStoreService:Lcom/google/android/music/store/IStoreService;
    invoke-static {v3}, Lcom/google/android/music/AvailableSpaceTracker;->access$100(Lcom/google/android/music/AvailableSpaceTracker;)Lcom/google/android/music/store/IStoreService;

    move-result-object v3

    invoke-interface {v3, v4, v5}, Lcom/google/android/music/store/IStoreService;->getSizeAlbum(J)J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 528
    goto :goto_0

    .line 530
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # getter for: Lcom/google/android/music/AvailableSpaceTracker;->mStoreService:Lcom/google/android/music/store/IStoreService;
    invoke-static {v3}, Lcom/google/android/music/AvailableSpaceTracker;->access$100(Lcom/google/android/music/AvailableSpaceTracker;)Lcom/google/android/music/store/IStoreService;

    move-result-object v3

    invoke-interface {v3, v4, v5}, Lcom/google/android/music/store/IStoreService;->getSizePlaylist(J)J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 531
    goto :goto_0

    .line 533
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # getter for: Lcom/google/android/music/AvailableSpaceTracker;->mStoreService:Lcom/google/android/music/store/IStoreService;
    invoke-static {v3}, Lcom/google/android/music/AvailableSpaceTracker;->access$100(Lcom/google/android/music/AvailableSpaceTracker;)Lcom/google/android/music/store/IStoreService;

    move-result-object v3

    invoke-interface {v3, v4, v5}, Lcom/google/android/music/store/IStoreService;->getSizeAutoPlaylist(J)J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 534
    goto :goto_0

    .line 536
    :pswitch_3
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # getter for: Lcom/google/android/music/AvailableSpaceTracker;->mStoreService:Lcom/google/android/music/store/IStoreService;
    invoke-static {v3}, Lcom/google/android/music/AvailableSpaceTracker;->access$100(Lcom/google/android/music/AvailableSpaceTracker;)Lcom/google/android/music/store/IStoreService;

    move-result-object v3

    invoke-interface {v3, v4, v5}, Lcom/google/android/music/store/IStoreService;->getSizeKeeponRadioStation(J)J
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v8

    add-long/2addr v6, v8

    .line 537
    goto :goto_0

    .line 547
    .end local v4    # "id":J
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # getter for: Lcom/google/android/music/AvailableSpaceTracker;->mSpaceVariablesLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/google/android/music/AvailableSpaceTracker;->access$500(Lcom/google/android/music/AvailableSpaceTracker;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 548
    :try_start_2
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v3}, Lcom/google/android/music/AvailableSpaceTracker;->getCombinedFreeSpace()J

    move-result-wide v10

    cmp-long v3, v10, v12

    if-lez v3, :cond_2

    .line 549
    .local v1, "freeSpaceAvailable":Z
    :goto_2
    iget-boolean v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mAdded:Z

    if-eqz v3, :cond_3

    .line 550
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # -= operator for: Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceChange:J
    invoke-static {v3, v6, v7}, Lcom/google/android/music/AvailableSpaceTracker;->access$1022(Lcom/google/android/music/AvailableSpaceTracker;J)J

    .line 551
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v3}, Lcom/google/android/music/AvailableSpaceTracker;->getCombinedFreeSpace()J

    move-result-wide v10

    cmp-long v3, v10, v12

    if-gtz v3, :cond_1

    .line 552
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    const/4 v9, 0x1

    # setter for: Lcom/google/android/music/AvailableSpaceTracker;->mAvailabilityTransition:Z
    invoke-static {v3, v9}, Lcom/google/android/music/AvailableSpaceTracker;->access$1102(Lcom/google/android/music/AvailableSpaceTracker;Z)Z

    .line 560
    :cond_1
    :goto_3
    monitor-exit v8

    goto :goto_1

    .end local v1    # "freeSpaceAvailable":Z
    :catchall_0
    move-exception v3

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 548
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 555
    .restart local v1    # "freeSpaceAvailable":Z
    :cond_3
    :try_start_3
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # += operator for: Lcom/google/android/music/AvailableSpaceTracker;->mFreeSpaceChange:J
    invoke-static {v3, v6, v7}, Lcom/google/android/music/AvailableSpaceTracker;->access$1014(Lcom/google/android/music/AvailableSpaceTracker;J)J

    .line 556
    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    invoke-virtual {v3}, Lcom/google/android/music/AvailableSpaceTracker;->getCombinedFreeSpace()J

    move-result-wide v10

    cmp-long v3, v10, v12

    if-lez v3, :cond_1

    .line 557
    iget-object v3, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    const/4 v9, 0x1

    # setter for: Lcom/google/android/music/AvailableSpaceTracker;->mAvailabilityTransition:Z
    invoke-static {v3, v9}, Lcom/google/android/music/AvailableSpaceTracker;->access$1102(Lcom/google/android/music/AvailableSpaceTracker;Z)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 525
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    # invokes: Lcom/google/android/music/AvailableSpaceTracker;->notifyAvailableSpaceChangedListener()V
    invoke-static {v0}, Lcom/google/android/music/AvailableSpaceTracker;->access$800(Lcom/google/android/music/AvailableSpaceTracker;)V

    .line 565
    iget-object v1, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->this$0:Lcom/google/android/music/AvailableSpaceTracker;

    iget v0, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mType:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AvailableSpaceTracker$UpdateFreespaceTask;->mIds:Ljava/util/List;

    :goto_0
    # invokes: Lcom/google/android/music/AvailableSpaceTracker;->notifyAlbumChangedListeners(Ljava/util/List;)V
    invoke-static {v1, v0}, Lcom/google/android/music/AvailableSpaceTracker;->access$1200(Lcom/google/android/music/AvailableSpaceTracker;Ljava/util/List;)V

    .line 567
    return-void

    .line 565
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
