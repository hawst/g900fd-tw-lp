.class Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;
.super Landroid/support/v7/media/MediaRouter$Callback;
.source "MediaRouteManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/mrp/MediaRouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaRouterCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;Lcom/google/android/music/ui/mrp/MediaRouteManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/mrp/MediaRouteManager;
    .param p2, "x1"    # Lcom/google/android/music/ui/mrp/MediaRouteManager$1;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;-><init>(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    return-void
.end method


# virtual methods
.method public onProviderAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$ProviderInfo;)V
    .locals 1
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "provider"    # Landroid/support/v7/media/MediaRouter$ProviderInfo;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 86
    return-void
.end method

.method public onProviderChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$ProviderInfo;)V
    .locals 1
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "provider"    # Landroid/support/v7/media/MediaRouter$ProviderInfo;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 96
    return-void
.end method

.method public onProviderRemoved(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$ProviderInfo;)V
    .locals 1
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "provider"    # Landroid/support/v7/media/MediaRouter$ProviderInfo;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 91
    return-void
.end method

.method public onRouteAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 4
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 50
    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$000()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Route added "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 52
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 53
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_3

    .line 56
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    sget-object v2, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->getSelectedMediaRouteId()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$202(Lcom/google/android/music/ui/mrp/MediaRouteManager;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 62
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # setter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v1, p2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$302(Lcom/google/android/music/ui/mrp/MediaRouteManager;Landroid/support/v7/media/MediaRouter$RouteInfo;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 63
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$202(Lcom/google/android/music/ui/mrp/MediaRouteManager;Ljava/lang/String;)Ljava/lang/String;

    .line 64
    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$000()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "MediaRouteManager"

    const-string v2, "Route being restored is added"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$400(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$300(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouter;->selectRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 71
    :cond_2
    :goto_1
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "MediaRouteManager"

    const-string v2, "Unable to get currently selected route."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 68
    .end local v0    # "re":Landroid/os/RemoteException;
    :cond_3
    const-string v1, "MediaRouteManager"

    const-string v2, "onRouteAdded -- service not yet bound."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onRouteChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "info"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 81
    return-void
.end method

.method public onRouteRemoved(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "info"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V
    invoke-static {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 76
    return-void
.end method

.method public onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 8
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v7, 0x0

    .line 100
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Route selected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->refreshVisibility()V
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$100(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 103
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "selectedRouteId":Ljava/lang/String;
    new-instance v4, Landroid/support/v7/media/MediaRouteSelector$Builder;

    invoke-direct {v4}, Landroid/support/v7/media/MediaRouteSelector$Builder;-><init>()V

    const-string v5, "com.google.cast.CATEGORY_CAST"

    invoke-virtual {v4, v5}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "com.google.cast.CATEGORY_CAST_APP_NAME:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->getCastAppName()Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$500(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v7/media/MediaRouteSelector$Builder;->addControlCategory(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteSelector$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouteSelector$Builder;->build()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v0

    .line 112
    .local v0, "castSelector":Landroid/support/v7/media/MediaRouteSelector;
    invoke-virtual {p2, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->matchesSelector(Landroid/support/v7/media/MediaRouteSelector;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    invoke-static {p2}, Lcom/google/cast/MediaRouteHelper;->requestCastDeviceForRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    .line 123
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 124
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 126
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Route "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is restored"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$600(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 138
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$300(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 139
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$300(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 141
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Route "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v6}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$300(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is restored"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$600(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    .line 153
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$700(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Lcom/google/android/music/utils/RouteChecker;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/google/android/music/utils/RouteChecker;->isAcceptableRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 154
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Rejecting unacceptable route <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$800(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b02af

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 156
    .local v3, "toastformat":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "message":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$800(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 158
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$400(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter;->getDefaultRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object p2

    .line 159
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$400(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/support/v7/media/MediaRouter;->selectRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 162
    .end local v1    # "message":Ljava/lang/String;
    .end local v3    # "toastformat":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->setMediaRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    invoke-static {v4, p2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$900(Lcom/google/android/music/ui/mrp/MediaRouteManager;Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 163
    :goto_2
    return-void

    .line 128
    :cond_4
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v4

    if-nez v4, :cond_5

    .line 129
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Non-default route selected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$600(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    goto/16 :goto_0

    .line 132
    :cond_5
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Ignoring selected route <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">. Restoring "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteIdToRestore:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$200(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 143
    :cond_6
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v4

    if-nez v4, :cond_7

    .line 144
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Non-default route selected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v4, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # invokes: Lcom/google/android/music/ui/mrp/MediaRouteManager;->resetMediaRouteRestoreState()V
    invoke-static {v4}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$600(Lcom/google/android/music/ui/mrp/MediaRouteManager;)V

    goto/16 :goto_1

    .line 147
    :cond_7
    const-string v4, "MediaRouteManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Ignoring selected route <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">. Restoring "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/ui/mrp/MediaRouteManager$MediaRouterCallback;->this$0:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    # getter for: Lcom/google/android/music/ui/mrp/MediaRouteManager;->mMediaRouteToRestore:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v6}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->access$300(Lcom/google/android/music/ui/mrp/MediaRouteManager;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
