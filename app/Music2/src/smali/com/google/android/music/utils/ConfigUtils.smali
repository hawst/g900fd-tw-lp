.class public Lcom/google/android/music/utils/ConfigUtils;
.super Ljava/lang/Object;
.source "ConfigUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/ConfigUtils$Worker;
    }
.end annotation


# static fields
.field private static LOGV:Z

.field private static final sAppConfigCache:Lcom/google/android/music/utils/ConfigCache;

.field private static volatile sContentResolver:Landroid/content/ContentResolver;

.field private static final sServerConfigCache:Lcom/google/android/music/utils/ConfigCache;

.field private static final sWorker:Lcom/google/android/music/utils/ConfigUtils$Worker;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/utils/ConfigUtils;->LOGV:Z

    .line 21
    new-instance v0, Lcom/google/android/music/utils/ConfigUtils$Worker;

    invoke-direct {v0}, Lcom/google/android/music/utils/ConfigUtils$Worker;-><init>()V

    sput-object v0, Lcom/google/android/music/utils/ConfigUtils;->sWorker:Lcom/google/android/music/utils/ConfigUtils$Worker;

    .line 34
    new-instance v0, Lcom/google/android/music/utils/ConfigCache;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/music/utils/ConfigUtils;->sWorker:Lcom/google/android/music/utils/ConfigUtils$Worker;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/utils/ConfigCache;-><init>(ILandroid/os/Handler;)V

    sput-object v0, Lcom/google/android/music/utils/ConfigUtils;->sServerConfigCache:Lcom/google/android/music/utils/ConfigCache;

    .line 35
    new-instance v0, Lcom/google/android/music/utils/ConfigCache;

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/music/utils/ConfigUtils;->sWorker:Lcom/google/android/music/utils/ConfigUtils$Worker;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/utils/ConfigCache;-><init>(ILandroid/os/Handler;)V

    sput-object v0, Lcom/google/android/music/utils/ConfigUtils;->sAppConfigCache:Lcom/google/android/music/utils/ConfigCache;

    .line 36
    return-void
.end method

.method public static deleteAllServerSettings()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    sget-object v0, Lcom/google/android/music/utils/ConfigUtils;->sContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 119
    return-void
.end method

.method public static enableArtRewrites()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 242
    const-string v0, "enableArtRewrite"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static enableHttpsAlbumArt()Z
    .locals 3

    .prologue
    .line 234
    const/4 v0, 0x1

    const-string v1, "enableHttpsAlbumArt"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static getBoolean(ILjava/lang/String;Z)Z
    .locals 6
    .param p0, "type"    # I
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 377
    invoke-static {p0, p1}, Lcom/google/android/music/utils/ConfigUtils;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 386
    .end local p2    # "defaultValue":Z
    :goto_0
    return p2

    .line 380
    .restart local p2    # "defaultValue":Z
    :cond_0
    const-string v3, "true"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move p2, v1

    .line 381
    goto :goto_0

    .line 382
    :cond_1
    const-string v3, "false"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move p2, v2

    .line 383
    goto :goto_0

    .line 385
    :cond_2
    const-string v3, "ConfigUtils"

    const-string v4, "Attempt to read: (%s, %s) as boolean"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v2

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCookie()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    const/4 v0, 0x1

    const-string v1, "cookie"

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstantSearchDelayMs()J
    .locals 4

    .prologue
    .line 111
    const/4 v0, 0x1

    const-string v1, "instantSearchDelay"

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/ConfigUtils;->getLong(ILjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getLong(ILjava/lang/String;J)J
    .locals 10
    .param p0, "type"    # I
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J

    .prologue
    .line 364
    invoke-static {p0, p1}, Lcom/google/android/music/utils/ConfigUtils;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 365
    .local v1, "valueString":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 367
    .local v2, "value":J
    :try_start_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 372
    :goto_0
    return-wide v2

    :cond_0
    move-wide v2, p2

    .line 367
    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v4, "ConfigUtils"

    const-string v5, "Attempt to read: (%s, %s) as long"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    move-wide v2, p2

    goto :goto_0
.end method

.method public static getMediaRoutePackageSignatures()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    const/4 v0, 0x1

    const-string v1, "mediaRoutePackageSignatures"

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNautilusExpirationTimeInMillisec()J
    .locals 4

    .prologue
    .line 96
    const/4 v0, 0x1

    const-string v1, "nautilusExpirationTimeMs"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/ConfigUtils;->getLong(ILjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getNautilusStatus()Lcom/google/android/music/NautilusStatus;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    const-string v0, "isNautilusUser"

    invoke-static {v2, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    .line 87
    :goto_0
    return-object v0

    .line 80
    :cond_0
    const-string v0, "isTrAvailable"

    invoke-static {v2, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    sget-object v0, Lcom/google/android/music/NautilusStatus;->TRIAL_AVAILABLE:Lcom/google/android/music/NautilusStatus;

    goto :goto_0

    .line 83
    :cond_1
    const-string v0, "isNautilusAvailable"

    invoke-static {v2, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    sget-object v0, Lcom/google/android/music/NautilusStatus;->PURCHASE_AVAILABLE_NO_TRIAL:Lcom/google/android/music/NautilusStatus;

    goto :goto_0

    .line 87
    :cond_2
    sget-object v0, Lcom/google/android/music/NautilusStatus;->UNAVAILABLE:Lcom/google/android/music/NautilusStatus;

    goto :goto_0
.end method

.method public static getShouldValidateMediaRoutes()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 126
    const-string v0, "shouldValidateMediaRoutes"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getSituationsResponseTTLMinutes()J
    .locals 4

    .prologue
    .line 322
    const/4 v0, 0x1

    const-string v1, "situationsResponseTTLMinutes"

    const-wide/16 v2, 0xf

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/ConfigUtils;->getLong(ILjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getString(ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # I
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 339
    packed-switch p0, :pswitch_data_0

    .line 345
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :pswitch_0
    sget-object v0, Lcom/google/android/music/utils/ConfigUtils;->sServerConfigCache:Lcom/google/android/music/utils/ConfigCache;

    invoke-virtual {v0, p1}, Lcom/google/android/music/utils/ConfigCache;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/google/android/music/utils/ConfigUtils;->sAppConfigCache:Lcom/google/android/music/utils/ConfigCache;

    invoke-virtual {v0, p1}, Lcom/google/android/music/utils/ConfigCache;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 339
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static hasIsNautilusAvailableBeenSet()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 184
    const-string v1, "isNautilusAvailable"

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasIsNautilusFreeTrialAvailableBeenSet()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 195
    const-string v1, "isTrAvailable"

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static init(Landroid/content/ContentResolver;)V
    .locals 2
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 42
    if-nez p0, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing ContentResolver"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    sput-object p0, Lcom/google/android/music/utils/ConfigUtils;->sContentResolver:Landroid/content/ContentResolver;

    .line 46
    sget-object v0, Lcom/google/android/music/utils/ConfigUtils;->sServerConfigCache:Lcom/google/android/music/utils/ConfigCache;

    invoke-virtual {v0, p0}, Lcom/google/android/music/utils/ConfigCache;->init(Landroid/content/ContentResolver;)V

    .line 47
    sget-object v0, Lcom/google/android/music/utils/ConfigUtils;->sAppConfigCache:Lcom/google/android/music/utils/ConfigCache;

    invoke-virtual {v0, p0}, Lcom/google/android/music/utils/ConfigCache;->init(Landroid/content/ContentResolver;)V

    .line 48
    return-void
.end method

.method public static isAcceptedUser()Z
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x1

    const-string v1, "isAcceptedUser"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isConciergeListenNowEnabled()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 312
    const-string v2, "conciergeListenNow"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "concListenNow"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method public static isDescriptionAttributionEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 330
    const-string v0, "isDescriptionAttributionEnabled"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isInstantSearchEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 103
    const-string v0, "isInstantSearchEnabled"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isManageDownloadsEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 294
    const-string v0, "isManageDownloadsEnabled"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isNautilusEnabled()Z
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x1

    const-string v1, "isNautilusUser"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isNautilusFreeTrialAvailable()Z
    .locals 3

    .prologue
    .line 173
    const/4 v0, 0x1

    const-string v1, "isTrAvailable"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isNautilusPurchaseAvailable()Z
    .locals 3

    .prologue
    .line 159
    const/4 v0, 0x1

    const-string v1, "isNautilusAvailable"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isPlayLoggingEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 250
    const-string v0, "isPlayLoggingEnabled"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isPlayWidgetPromoteNautilusEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 219
    const-string v0, "playWidgetPromoteNautilus"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isQuizEnabledOnDemand()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 203
    const-string v0, "isQuizEnabledOnDemand"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isQuizEnabledOnSignUp()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 211
    const-string v0, "isQuizEnabledOnSignup"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isRadioSearchEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 303
    const-string v0, "isRadioSearchEnabled"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isStorageLowHandlingEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 285
    const-string v0, "isStorageLowHandlingEnabled"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isStructuredPlayLoggingEnabled()Z
    .locals 3

    .prologue
    .line 258
    const/4 v0, 0x1

    const-string v1, "enableStructuredLogging"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isUpgradeTempCacheForKeeponEnabled()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 267
    const-string v0, "isUpgradeTempCacheForKeeponEnabled"

    invoke-static {v1, v0, v1}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isVsAvailable()Z
    .locals 3

    .prologue
    .line 227
    const/4 v0, 0x1

    const-string v1, "isVsAvailable"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static wasEverInNautilus()Z
    .locals 3

    .prologue
    .line 276
    const/4 v0, 0x1

    const-string v1, "wasEverInNautilus"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/ConfigUtils;->getBoolean(ILjava/lang/String;Z)Z

    move-result v0

    return v0
.end method
