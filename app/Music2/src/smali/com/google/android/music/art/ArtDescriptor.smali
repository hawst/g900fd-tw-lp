.class public abstract Lcom/google/android/music/art/ArtDescriptor;
.super Ljava/lang/Object;
.source "ArtDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtDescriptor$SizeHandling;
    }
.end annotation


# instance fields
.field public final artType:Lcom/google/android/music/art/ArtType;

.field public final aspectRatio:F

.field public final sizeBucket:I

.field public final sizeHandling:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtDescriptor$SizeHandling;IF)V
    .locals 0
    .param p1, "artType"    # Lcom/google/android/music/art/ArtType;
    .param p2, "sizeHandling"    # Lcom/google/android/music/art/ArtDescriptor$SizeHandling;
    .param p3, "sizeBucket"    # I
    .param p4, "aspectRatio"    # F

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/music/art/ArtDescriptor;->artType:Lcom/google/android/music/art/ArtType;

    .line 24
    iput-object p2, p0, Lcom/google/android/music/art/ArtDescriptor;->sizeHandling:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    .line 25
    iput p3, p0, Lcom/google/android/music/art/ArtDescriptor;->sizeBucket:I

    .line 26
    iput p4, p0, Lcom/google/android/music/art/ArtDescriptor;->aspectRatio:F

    .line 27
    return-void
.end method


# virtual methods
.method public checkSizeAcceptable(FF)Z
    .locals 1
    .param p1, "requested"    # F
    .param p2, "given"    # F

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/music/art/ArtDescriptor;->sizeHandling:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->checkSizeAcceptable(FF)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    if-ne p0, p1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 38
    check-cast v0, Lcom/google/android/music/art/ArtDescriptor;

    .line 40
    .local v0, "that":Lcom/google/android/music/art/ArtDescriptor;
    iget v3, v0, Lcom/google/android/music/art/ArtDescriptor;->aspectRatio:F

    iget v4, p0, Lcom/google/android/music/art/ArtDescriptor;->aspectRatio:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 41
    :cond_4
    iget v3, p0, Lcom/google/android/music/art/ArtDescriptor;->sizeBucket:I

    iget v4, v0, Lcom/google/android/music/art/ArtDescriptor;->sizeBucket:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 42
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/art/ArtDescriptor;->artType:Lcom/google/android/music/art/ArtType;

    iget-object v4, v0, Lcom/google/android/music/art/ArtDescriptor;->artType:Lcom/google/android/music/art/ArtType;

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getBucketSize()I
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/art/ArtDescriptor;->artType:Lcom/google/android/music/art/ArtType;

    iget v1, p0, Lcom/google/android/music/art/ArtDescriptor;->sizeBucket:I

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtType;->getBucketSize(I)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 49
    iget-object v1, p0, Lcom/google/android/music/art/ArtDescriptor;->artType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v1}, Lcom/google/android/music/art/ArtType;->hashCode()I

    move-result v0

    .line 50
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/music/art/ArtDescriptor;->sizeBucket:I

    add-int v0, v1, v2

    .line 51
    mul-int/lit8 v2, v0, 0x1f

    iget v1, p0, Lcom/google/android/music/art/ArtDescriptor;->aspectRatio:F

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/music/art/ArtDescriptor;->aspectRatio:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :goto_0
    add-int v0, v2, v1

    .line 52
    return v0

    .line 51
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
