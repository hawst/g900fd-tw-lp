.class public abstract Lcom/google/android/music/leanback/LeanbackItemActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "LeanbackItemActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
.implements Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;


# instance fields
.field private mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method protected static createListRow(Ljava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 3
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 191
    new-instance v0, Landroid/support/v17/leanback/widget/ListRow;

    new-instance v1, Landroid/support/v17/leanback/widget/HeaderItem;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    return-object v0
.end method


# virtual methods
.method protected abstract createFragment()Landroid/app/Fragment;
.end method

.method protected createListRow(IILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 1
    .param p1, "rowId"    # I
    .param p2, "titleResourceId"    # I
    .param p3, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 183
    invoke-virtual {p0, p2}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/music/leanback/LeanbackItemActivity;->createListRow(ILjava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v0

    return-object v0
.end method

.method protected createListRow(ILandroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 4
    .param p1, "titleResourceId"    # I
    .param p2, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 179
    new-instance v0, Landroid/support/v17/leanback/widget/ListRow;

    new-instance v1, Landroid/support/v17/leanback/widget/HeaderItem;

    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, p2}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    return-object v0
.end method

.method protected createListRow(ILjava/lang/String;Landroid/support/v17/leanback/widget/ObjectAdapter;)Landroid/support/v17/leanback/widget/ListRow;
    .locals 5
    .param p1, "rowId"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 187
    new-instance v0, Landroid/support/v17/leanback/widget/ListRow;

    int-to-long v2, p1

    new-instance v1, Landroid/support/v17/leanback/widget/HeaderItem;

    const/4 v4, 0x0

    invoke-direct {v1, p2, v4}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v2, v3, v1, p3}, Landroid/support/v17/leanback/widget/ListRow;-><init>(JLandroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    return-object v0
.end method

.method protected getBackgroundImageMessageHandler()Lcom/google/android/music/leanback/BackgroundImageMessageHandler;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    return-object v0
.end method

.method protected getCurrentFragment()Landroid/app/Fragment;
    .locals 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e00d0

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method protected loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V
    .locals 6
    .param p1, "loaderId"    # I
    .param p2, "adapter"    # Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .param p3, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p4, "projection"    # [Ljava/lang/String;

    .prologue
    .line 119
    move-object v3, p0

    .line 120
    .local v3, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackItemActivity$1;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/LeanbackItemActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/MediaList;Landroid/content/Context;[Ljava/lang/String;Landroid/support/v17/leanback/widget/CursorObjectAdapter;)V

    .line 147
    .local v0, "callbacks":Landroid/support/v4/app/LoaderManager$LoaderCallbacks;, "Landroid/support/v4/app/LoaderManager$LoaderCallbacks<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 148
    return-void
.end method

.method protected loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V
    .locals 6
    .param p1, "loaderId"    # I
    .param p2, "adapter"    # Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "projection"    # [Ljava/lang/String;

    .prologue
    .line 152
    move-object v2, p0

    .line 153
    .local v2, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/music/leanback/LeanbackItemActivity$2;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/LeanbackItemActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackItemActivity;Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Landroid/support/v17/leanback/widget/CursorObjectAdapter;)V

    .line 175
    .local v0, "callbacks":Landroid/support/v4/app/LoaderManager$LoaderCallbacks;, "Landroid/support/v4/app/LoaderManager$LoaderCallbacks<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3, v0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 176
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->showUi()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 51
    :cond_0
    const v0, 0x7f04005b

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->setContentView(I)V

    .line 52
    if-nez p1, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0e00d0

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->createFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 59
    :goto_1
    new-instance v0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;-><init>(Landroid/view/Window;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->setupFragment(Landroid/app/Fragment;)V

    goto :goto_1
.end method

.method public onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 4
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "itemObject"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 74
    instance-of v2, p2, Lcom/google/android/music/leanback/Item;

    if-eqz v2, :cond_1

    move-object v1, p2

    .line 75
    check-cast v1, Lcom/google/android/music/leanback/Item;

    .line 76
    .local v1, "item":Lcom/google/android/music/leanback/Item;
    iget-object v2, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    const v3, 0x7f0e015f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/leanback/ImageCardView;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/ImageCardView;->getMainImageView()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Lcom/google/android/music/leanback/Item;->onClicked(Landroid/content/Context;Landroid/view/View;)V

    .line 82
    .end local v1    # "item":Lcom/google/android/music/leanback/Item;
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    instance-of v2, p2, Lcom/google/android/music/leanback/Icon;

    if-eqz v2, :cond_0

    move-object v0, p2

    .line 79
    check-cast v0, Lcom/google/android/music/leanback/Icon;

    .line 80
    .local v0, "icon":Lcom/google/android/music/leanback/Icon;
    iget-object v2, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v0, p0, v2}, Lcom/google/android/music/leanback/Icon;->onClicked(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method

.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 1
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 87
    instance-of v0, p4, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    if-eqz v0, :cond_2

    .line 88
    check-cast p4, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    .end local p4    # "row":Landroid/support/v17/leanback/widget/Row;
    invoke-virtual {p4}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getItem()Ljava/lang/Object;

    move-result-object p2

    .line 94
    .end local p2    # "item":Ljava/lang/Object;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    invoke-virtual {v0, p2}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessage(Ljava/lang/Object;)V

    .line 97
    :cond_1
    :goto_1
    return-void

    .line 89
    .restart local p2    # "item":Ljava/lang/Object;
    .restart local p4    # "row":Landroid/support/v17/leanback/widget/Row;
    :cond_2
    instance-of v0, p4, Lcom/google/android/music/leanback/SongRow;

    if-eqz v0, :cond_3

    .line 90
    check-cast p4, Lcom/google/android/music/leanback/SongRow;

    .end local p4    # "row":Landroid/support/v17/leanback/widget/Row;
    invoke-virtual {p4}, Lcom/google/android/music/leanback/SongRow;->getItem()Lcom/google/android/music/leanback/Item;

    move-result-object p2

    .local p2, "item":Lcom/google/android/music/leanback/Item;
    goto :goto_0

    .line 91
    .local p2, "item":Ljava/lang/Object;
    .restart local p4    # "row":Landroid/support/v17/leanback/widget/Row;
    :cond_3
    instance-of v0, p2, Lcom/google/android/music/leanback/Icon;

    if-eqz v0, :cond_0

    goto :goto_1
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 101
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newSearchIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->startActivity(Landroid/content/Intent;)V

    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackItemActivity;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->onActivityStarted()V

    .line 68
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 69
    return-void
.end method

.method protected abstract setupFragment(Landroid/app/Fragment;)V
.end method

.method protected showUi()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method
