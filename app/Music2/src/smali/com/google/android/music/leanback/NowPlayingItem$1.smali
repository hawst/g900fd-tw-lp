.class Lcom/google/android/music/leanback/NowPlayingItem$1;
.super Ljava/lang/Object;
.source "NowPlayingItem.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/NowPlayingItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/NowPlayingItem;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/NowPlayingItem;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem$1;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;
    .param p2, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$1;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-static {p2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v1

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$002(Lcom/google/android/music/leanback/NowPlayingItem;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 77
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$1;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # invokes: Lcom/google/android/music/leanback/NowPlayingItem;->updateTrackInfo()V
    invoke-static {v0}, Lcom/google/android/music/leanback/NowPlayingItem;->access$100(Lcom/google/android/music/leanback/NowPlayingItem;)V

    .line 78
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$1;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$002(Lcom/google/android/music/leanback/NowPlayingItem;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 83
    return-void
.end method
