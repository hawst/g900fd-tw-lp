.class Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;
.super Landroid/support/v7/media/MediaRouter$ControlRequestCallback;
.source "RemoteAsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemotePlaybackOnMainThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 729
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error encountered requesting remote playback: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 763
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->handleErrorStatus(Landroid/os/Bundle;)Z
    invoke-static {v0, p2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$3000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 765
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    .line 767
    :cond_0
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 732
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "playIntent:onResult data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 734
    if-eqz p1, :cond_0

    .line 735
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/CastSessionManager;

    move-result-object v1

    const-string v2, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/cast/CastSessionManager;->setSessionId(Ljava/lang/String;)V

    .line 737
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v2, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2402(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)Ljava/lang/String;

    .line 739
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # setter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRetryingInvalidSessionId:Z
    invoke-static {v1, v4}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2502(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)Z

    .line 743
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 744
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const-string v2, "Item ID not initialized!"

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processError(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v1, v2, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 757
    :cond_1
    :goto_0
    return-void

    .line 748
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processControlRequestResultBundle(Landroid/os/Bundle;)V
    invoke-static {v1, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Landroid/os/Bundle;)V

    .line 750
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1500(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 753
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1500(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    move-result-object v1

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->clearCurrentItem(Z)V
    invoke-static {v1, v4}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$2900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    .line 754
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 755
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
