.class Lcom/google/android/music/icing/IcingContentProvider$1$1;
.super Ljava/lang/Object;
.source "IcingContentProvider.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/icing/IcingContentProvider$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/icing/IcingContentProvider$1;


# direct methods
.method constructor <init>(Lcom/google/android/music/icing/IcingContentProvider$1;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/music/icing/IcingContentProvider$1$1;->this$1:Lcom/google/android/music/icing/IcingContentProvider$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/music/icing/IcingContentProvider$1$1;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 53
    # getter for: Lcom/google/android/music/icing/IcingContentProvider;->LEGACY_CORPUS_NAMES:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/icing/IcingContentProvider;->access$000()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v7, :cond_0

    aget-object v6, v0, v4

    .line 54
    .local v6, "legacyCorpusName":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/icing/IcingContentProvider$1$1;->this$1:Lcom/google/android/music/icing/IcingContentProvider$1;

    iget-object v9, v9, Lcom/google/android/music/icing/IcingContentProvider$1;->this$0:Lcom/google/android/music/icing/IcingContentProvider;

    # getter for: Lcom/google/android/music/icing/IcingContentProvider;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
    invoke-static {v9}, Lcom/google/android/music/icing/IcingContentProvider;->access$100(Lcom/google/android/music/icing/IcingContentProvider;)Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->unregisterCorpus(Ljava/lang/String;)Z

    .line 53
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 56
    .end local v6    # "legacyCorpusName":Ljava/lang/String;
    :cond_0
    iget-object v9, p0, Lcom/google/android/music/icing/IcingContentProvider$1$1;->this$1:Lcom/google/android/music/icing/IcingContentProvider$1;

    iget-object v9, v9, Lcom/google/android/music/icing/IcingContentProvider$1;->this$0:Lcom/google/android/music/icing/IcingContentProvider;

    # getter for: Lcom/google/android/music/icing/IcingContentProvider;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
    invoke-static {v9}, Lcom/google/android/music/icing/IcingContentProvider;->access$200(Lcom/google/android/music/icing/IcingContentProvider;)Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->getCorpusNames()[Ljava/lang/String;

    move-result-object v3

    .line 57
    .local v3, "corpusNames":[Ljava/lang/String;
    if-nez v3, :cond_2

    .line 74
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v7    # "len$":I
    :cond_1
    :goto_1
    return-object v12

    .line 63
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v4    # "i$":I
    .restart local v7    # "len$":I
    :cond_2
    move-object v0, v3

    array-length v7, v0

    const/4 v4, 0x0

    move v5, v4

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v7    # "len$":I
    .local v5, "i$":I
    :goto_2
    if-ge v5, v7, :cond_4

    aget-object v2, v0, v5

    .line 64
    .local v2, "corpusName":Ljava/lang/String;
    # getter for: Lcom/google/android/music/icing/IcingContentProvider;->LEGACY_CORPUS_NAMES:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/icing/IcingContentProvider;->access$000()[Ljava/lang/String;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v4, 0x0

    .end local v5    # "i$":I
    .restart local v4    # "i$":I
    :goto_3
    if-ge v4, v8, :cond_3

    aget-object v6, v1, v4

    .line 65
    .restart local v6    # "legacyCorpusName":Ljava/lang/String;
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 64
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 63
    .end local v6    # "legacyCorpusName":Ljava/lang/String;
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_2

    .line 73
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "corpusName":Ljava/lang/String;
    .end local v8    # "len$":I
    :cond_4
    iget-object v9, p0, Lcom/google/android/music/icing/IcingContentProvider$1$1;->this$1:Lcom/google/android/music/icing/IcingContentProvider$1;

    iget-object v9, v9, Lcom/google/android/music/icing/IcingContentProvider$1;->val$prefs:Landroid/content/SharedPreferences;

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    const-string v10, "com.google.android.music.icing.IcingContentProvider.LEGACY_CORPORA_REMOVED"

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method
