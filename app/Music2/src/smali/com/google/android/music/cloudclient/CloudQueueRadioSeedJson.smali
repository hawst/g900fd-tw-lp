.class public Lcom/google/android/music/cloudclient/CloudQueueRadioSeedJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueueRadioSeedJson.java"


# instance fields
.field public mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumId"
    .end annotation
.end field

.field public mArtistId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistId"
    .end annotation
.end field

.field public mCuratedStationId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "curatedStationId"
    .end annotation
.end field

.field public mGenreId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "genreId"
    .end annotation
.end field

.field public mPlaylistShareToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlistShareToken"
    .end annotation
.end field

.field public mSeedType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seedType"
    .end annotation
.end field

.field public mTrackId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackId"
    .end annotation
.end field

.field public mTrackLockerId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackLockerId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
