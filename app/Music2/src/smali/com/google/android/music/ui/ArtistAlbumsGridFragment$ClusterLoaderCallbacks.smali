.class Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;
.super Ljava/lang/Object;
.source "ArtistAlbumsGridFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClusterLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;

    .prologue
    .line 418
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;-><init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 422
    packed-switch p1, :pswitch_data_0

    .line 441
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid loader id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424
    :pswitch_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$500(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByNautilusArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ALBUM_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :goto_0
    return-object v0

    .line 428
    :pswitch_1
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$500(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$Artists;->getTopSongsByArtistUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :pswitch_2
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$500(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$Artists;->getRelatedArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ARTIST_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 436
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$500(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Artists;->getTopSongsByArtistUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 437
    .local v7, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v7}, Lcom/google/android/music/store/MusicContent$Artists;->addHasVideoParam(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 422
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 447
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    if-nez p2, :cond_0

    .line 468
    :goto_0
    return-void

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mSpinner:Lcom/google/android/music/ui/SpinnerHeaderView;
    invoke-static {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/SpinnerHeaderView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 450
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mSpinner:Lcom/google/android/music/ui/SpinnerHeaderView;
    invoke-static {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/SpinnerHeaderView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/SpinnerHeaderView;->hide()V

    .line 452
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid loader id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$900(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    invoke-static {v0, v1, v2, p2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    goto :goto_0

    .line 457
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$700(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    invoke-static {v0, v1, v2, p2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    goto :goto_0

    .line 460
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1000(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v1

    const/4 v2, 0x2

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    invoke-static {v0, v1, v2, p2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    goto :goto_0

    .line 463
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v1

    const/4 v2, 0x3

    # invokes: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    invoke-static {v0, v1, v2, p2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    goto :goto_0

    .line 452
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 418
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v1, 0x4

    .line 472
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 486
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid loader id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$900(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 488
    :goto_0
    return-void

    .line 477
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$700(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    goto :goto_0

    .line 480
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1000(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    goto :goto_0

    .line 483
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-static {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$1100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    goto :goto_0

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
