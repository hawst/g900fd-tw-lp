.class public Lcom/google/android/music/ui/CreateArtistShuffleActivity;
.super Landroid/app/Activity;
.source "CreateArtistShuffleActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final LOGV:Z


# instance fields
.field private mArtistSongList:Lcom/google/android/music/medialist/SongList;

.field private mContext:Landroid/content/Context;

.field private volatile mDestroyed:Z

.field private mDivider:Landroid/view/View;

.field private mMediaController:Landroid/widget/MediaController;

.field private mOkButton:Landroid/widget/Button;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mText:Landroid/widget/TextView;

.field private mVideoView:Landroid/widget/VideoView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDestroyed:Z

    .line 71
    new-instance v0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity$1;-><init>(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 85
    new-instance v0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity$2;-><init>(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 95
    new-instance v0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity$3;-><init>(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/widget/MediaController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mMediaController:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/widget/VideoView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->showFallbackImage()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/CreateArtistShuffleActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->startArtistShuffle(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOkButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDivider:Landroid/view/View;

    return-object v0
.end method

.method public static getArtistShuffleSongList(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistMetajamId"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;

    .prologue
    .line 248
    const/4 v9, 0x0

    .line 249
    .local v9, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    const/4 v8, 0x0

    .line 251
    .local v8, "songList":Lcom/google/android/music/medialist/SongList;
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 252
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    new-instance v6, Ljava/lang/Object;

    invoke-direct {v6}, Ljava/lang/Object;-><init>()V

    .line 253
    .local v6, "prefsOwner":Ljava/lang/Object;
    invoke-static {p0, v6}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    .line 255
    .local v5, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->getContentFilter()I

    move-result v2

    .line 256
    .local v2, "contentFilter":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "music_artist_shuffle_size"

    const/16 v12, 0x64

    invoke-static {v10, v11, v12}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 259
    .local v4, "maxSize":I
    invoke-interface {v0, p1, v4, v2}, Lcom/google/android/music/cloudclient/MusicCloud;->getArtistShuffleFeed(Ljava/lang/String;II)Lcom/google/android/music/cloudclient/RadioFeedResponse;

    move-result-object v7

    .line 261
    .local v7, "result":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    iget-object v10, v7, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    if-eqz v10, :cond_0

    iget-object v10, v7, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v10, v10, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    if-eqz v10, :cond_0

    iget-object v10, v7, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v10, v10, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-eqz v10, :cond_0

    .line 263
    iget-object v10, v7, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v10, v10, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    iget-object v9, v10, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    .line 265
    :cond_0
    if-eqz v9, :cond_1

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_1

    .line 266
    invoke-static {p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;->newArtistShuffleDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 268
    .local v1, "container":Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v9, v1}, Lcom/google/android/music/medialist/TracksSongList;->createList(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/medialist/TracksSongList;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 277
    .end local v1    # "container":Lcom/google/android/music/store/ContainerDescriptor;
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 279
    .end local v2    # "contentFilter":I
    .end local v4    # "maxSize":I
    .end local v7    # "result":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    :goto_1
    return-object v8

    .line 270
    .restart local v2    # "contentFilter":I
    .restart local v4    # "maxSize":I
    .restart local v7    # "result":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    :cond_1
    :try_start_1
    const-string v10, "MusicArtistShuffle"

    const-string v11, "Artist shuffle result is empty or too small."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 272
    .end local v2    # "contentFilter":I
    .end local v4    # "maxSize":I
    .end local v7    # "result":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    :catch_0
    move-exception v3

    .line 273
    .local v3, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v10, "MusicArtistShuffle"

    const-string v11, "Interrupted while getting artist shuffle entries"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 277
    invoke-static {v6}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_1

    .line 274
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v3

    .line 275
    .local v3, "e":Ljava/io/IOException;
    :try_start_3
    const-string v10, "MusicArtistShuffle"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to get artist shuffle entries: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 277
    invoke-static {v6}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_1

    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    invoke-static {v6}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v10
.end method

.method private onFailure()V
    .locals 1

    .prologue
    .line 225
    new-instance v0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;-><init>(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 236
    return-void
.end method

.method private playSongList()V
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDestroyed:Z

    if-nez v0, :cond_0

    .line 202
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->clearPlayQueue()V

    .line 203
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 211
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity$5;-><init>(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 222
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mArtistSongList:Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mArtistSongList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 208
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->onFailure()V

    goto :goto_0
.end method

.method private showFallbackImage()V
    .locals 3

    .prologue
    .line 106
    const v1, 0x7f0e00fb

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 107
    .local v0, "fallback":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 108
    const v1, 0x7f020156

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    return-void
.end method

.method private startArtistShuffle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "artistMetajamId"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->getArtistShuffleSongList(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 197
    invoke-direct {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->playSongList()V

    .line 198
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOkButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 241
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->setResult(I)V

    .line 242
    invoke-virtual {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->finish()V

    .line 244
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 114
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    invoke-virtual {p0, v10}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->setResult(I)V

    .line 119
    const v6, 0x7f040028

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->setContentView(I)V

    .line 120
    const v6, 0x7f0e00fd

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOkButton:Landroid/widget/Button;

    .line 121
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    const v6, 0x7f0e007d

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mText:Landroid/widget/TextView;

    .line 123
    const v6, 0x7f0e00f9

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDivider:Landroid/view/View;

    .line 124
    iput-object p0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mContext:Landroid/content/Context;

    .line 126
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, -0x3

    invoke-virtual {v6, v7}, Landroid/view/Window;->setFormat(I)V

    .line 131
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    .line 132
    .local v2, "builder":Landroid/net/Uri$Builder;
    const-string v6, "android.resource"

    invoke-virtual {v2, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 133
    invoke-virtual {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 135
    const v6, 0x7f090002

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 136
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 137
    .local v5, "videoUri":Landroid/net/Uri;
    new-instance v6, Landroid/widget/MediaController;

    invoke-direct {v6, p0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mMediaController:Landroid/widget/MediaController;

    .line 138
    const v6, 0x7f0e00fc

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/VideoView;

    iput-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    .line 139
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v7, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 140
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v7, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 141
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v7, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 142
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v7, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 143
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v6, v5}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 148
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v6, v9}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    .line 149
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isRadioAnimationEnabled()Z

    move-result v4

    .line 150
    .local v4, "isVideoEnabled":Z
    if-eqz v4, :cond_0

    .line 151
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v6}, Landroid/widget/VideoView;->start()V

    .line 156
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 157
    .local v3, "intent":Landroid/content/Intent;
    const-string v6, "remoteId"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "artistMetajamId":Ljava/lang/String;
    const-string v6, "name"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "artistName":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mContext:Landroid/content/Context;

    const v8, 0x7f0b01d5

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v1, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    const-string v6, "songlist"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/medialist/SongList;

    iput-object v6, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mArtistSongList:Lcom/google/android/music/medialist/SongList;

    .line 164
    new-instance v6, Lcom/google/android/music/ui/CreateArtistShuffleActivity$4;

    invoke-direct {v6, p0, v0, v1}, Lcom/google/android/music/ui/CreateArtistShuffleActivity$4;-><init>(Lcom/google/android/music/ui/CreateArtistShuffleActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 169
    return-void

    .line 153
    .end local v0    # "artistMetajamId":Ljava/lang/String;
    .end local v1    # "artistName":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->showFallbackImage()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDestroyed:Z

    .line 188
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 180
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onPause()V

    .line 181
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 173
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 174
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onResume()V

    .line 175
    return-void
.end method
