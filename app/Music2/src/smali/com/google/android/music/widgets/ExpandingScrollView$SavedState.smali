.class public Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/widgets/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field private exposurePercentages:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->valueOf(Ljava/lang/String;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->createFloatArray()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->exposurePercentages:[F

    .line 146
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/widgets/ExpandingScrollView$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/widgets/ExpandingScrollView$1;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;[F)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;
    .param p2, "expandingState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .param p3, "exposurePercentages"    # [F

    .prologue
    .line 117
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 118
    iput-object p2, p0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 119
    iput-object p3, p0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->exposurePercentages:[F

    .line 120
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    return-object v0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 124
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->exposurePercentages:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 127
    return-void
.end method
