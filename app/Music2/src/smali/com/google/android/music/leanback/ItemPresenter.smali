.class Lcom/google/android/music/leanback/ItemPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "ItemPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;,
        Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolderLoadBitmapAsyncTask;
    }
.end annotation


# instance fields
.field private final mCardImageHeight:I

.field private final mContext:Landroid/content/Context;

.field private final mCropToSquare:Z

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mWideCardImageHeight:I

.field private final mWideCardImageWidth:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;Z)V

    .line 96
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cropToSquare"    # Z

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;ZIII)V

    .line 103
    return-void
.end method

.method constructor <init>(Landroid/content/Context;ZIII)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cropToSquare"    # Z
    .param p3, "cardImageHeight"    # I
    .param p4, "wideCardImageHeight"    # I
    .param p5, "wideCardImageWidth"    # I

    .prologue
    .line 106
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/google/android/music/leanback/ItemPresenter;->mContext:Landroid/content/Context;

    .line 108
    iput-boolean p2, p0, Lcom/google/android/music/leanback/ItemPresenter;->mCropToSquare:Z

    .line 109
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter;->mExecutor:Ljava/util/concurrent/Executor;

    .line 110
    iput p3, p0, Lcom/google/android/music/leanback/ItemPresenter;->mCardImageHeight:I

    .line 111
    iput p4, p0, Lcom/google/android/music/leanback/ItemPresenter;->mWideCardImageHeight:I

    .line 112
    iput p5, p0, Lcom/google/android/music/leanback/ItemPresenter;->mWideCardImageWidth:I

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/ImageCardView;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/ImageCardView;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/google/android/music/leanback/ItemPresenter;->setImageView(Lcom/google/android/music/leanback/ImageCardView;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private static setImageView(Lcom/google/android/music/leanback/ImageCardView;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p0, "imageCardView"    # Lcom/google/android/music/leanback/ImageCardView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 213
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/ImageCardView;->setMainImage(Landroid/graphics/drawable/Drawable;)V

    .line 214
    return-void
.end method


# virtual methods
.method protected getLayoutResourceId()I
    .locals 1

    .prologue
    .line 205
    const v0, 0x7f040057

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 10
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "itemObject"    # Ljava/lang/Object;

    .prologue
    .line 128
    move-object v1, p2

    check-cast v1, Lcom/google/android/music/leanback/Item;

    .line 130
    .local v1, "item":Lcom/google/android/music/leanback/Item;
    invoke-virtual {p0, p1}, Lcom/google/android/music/leanback/ItemPresenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 131
    iget-object v8, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    const v9, 0x7f0e0160

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 133
    .local v3, "overlayView":Landroid/widget/ImageView;
    iget-object v8, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    const v9, 0x7f0e015f

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/leanback/ImageCardView;

    .line 135
    .local v0, "imageCardView":Lcom/google/android/music/leanback/ImageCardView;
    iget-boolean v8, p0, Lcom/google/android/music/leanback/ItemPresenter;->mCropToSquare:Z

    if-nez v8, :cond_0

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getWide()Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v7, 0x1

    .line 136
    .local v7, "useWideDimensions":Z
    :goto_0
    if-eqz v7, :cond_1

    iget v5, p0, Lcom/google/android/music/leanback/ItemPresenter;->mWideCardImageHeight:I

    .line 137
    .local v5, "targetHeight":I
    :goto_1
    if-eqz v7, :cond_2

    iget v6, p0, Lcom/google/android/music/leanback/ItemPresenter;->mWideCardImageWidth:I

    .line 139
    .local v6, "targetWidth":I
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getOverlayResourceId()I

    move-result v8

    if-eqz v8, :cond_3

    .line 140
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 141
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput v6, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 142
    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 143
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 145
    invoke-virtual {p0, v3, v1}, Lcom/google/android/music/leanback/ItemPresenter;->prepareOverlay(Landroid/widget/ImageView;Lcom/google/android/music/leanback/Item;)V

    .line 150
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :goto_3
    invoke-virtual {v0, v6, v5}, Lcom/google/android/music/leanback/ImageCardView;->setMainImageDimensions(II)V

    .line 151
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/ImageCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 152
    const v8, 0x7f0e015f

    invoke-virtual {v0, v8, v1}, Lcom/google/android/music/leanback/ImageCardView;->setTag(ILjava/lang/Object;)V

    .line 154
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 155
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v8

    instance-of v8, v8, Lcom/google/android/music/leanback/Item$ConstantStringGetter;

    if-eqz v8, :cond_4

    .line 156
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/ImageCardView;->setForceKeepContent(Z)V

    .line 157
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/music/leanback/Item$StringGetter;->getString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/ImageCardView;->setContentText(Ljava/lang/CharSequence;)V

    .line 184
    :goto_4
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 185
    check-cast p1, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;

    .end local p1    # "viewHolder":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    iget-boolean v8, p0, Lcom/google/android/music/leanback/ItemPresenter;->mCropToSquare:Z

    invoke-virtual {p1, v1, v6, v5, v8}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->bind(Lcom/google/android/music/leanback/Item;IIZ)V

    .line 193
    :goto_5
    return-void

    .line 135
    .end local v5    # "targetHeight":I
    .end local v6    # "targetWidth":I
    .end local v7    # "useWideDimensions":Z
    .restart local p1    # "viewHolder":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 136
    .restart local v7    # "useWideDimensions":Z
    :cond_1
    iget v5, p0, Lcom/google/android/music/leanback/ItemPresenter;->mCardImageHeight:I

    goto :goto_1

    .restart local v5    # "targetHeight":I
    :cond_2
    move v6, v5

    .line 137
    goto :goto_2

    .line 147
    .restart local v6    # "targetWidth":I
    :cond_3
    const/16 v8, 0x8

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 159
    :cond_4
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/ImageCardView;->setForceKeepContent(Z)V

    .line 160
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/ImageCardView;->setContentText(Ljava/lang/CharSequence;)V

    .line 161
    new-instance v8, Lcom/google/android/music/leanback/ItemPresenter$1;

    invoke-direct {v8, p0, v1, v0}, Lcom/google/android/music/leanback/ItemPresenter$1;-><init>(Lcom/google/android/music/leanback/ItemPresenter;Lcom/google/android/music/leanback/Item;Lcom/google/android/music/leanback/ImageCardView;)V

    invoke-static {v8}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_4

    .line 180
    :cond_5
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/ImageCardView;->setForceKeepContent(Z)V

    .line 181
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lcom/google/android/music/leanback/ImageCardView;->setContentText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 187
    :cond_6
    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getIconResourceId()I

    move-result v4

    .line 188
    .local v4, "resourceId":I
    if-gtz v4, :cond_7

    .line 189
    const v4, 0x7f02003a

    .line 191
    :cond_7
    iget-object v8, p0, Lcom/google/android/music/leanback/ItemPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/google/android/music/leanback/ItemPresenter;->setImageView(Lcom/google/android/music/leanback/ImageCardView;Landroid/graphics/drawable/Drawable;)V

    goto :goto_5
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 117
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 119
    .local v2, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/ItemPresenter;->getLayoutResourceId()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 121
    .local v0, "containerView":Landroid/view/ViewGroup;
    const v3, 0x7f0e015f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/leanback/ImageCardView;

    .line 123
    .local v1, "imageCardView":Lcom/google/android/music/leanback/ImageCardView;
    new-instance v3, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;

    iget-object v4, p0, Lcom/google/android/music/leanback/ItemPresenter;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v4}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;-><init>(Landroid/view/View;Ljava/util/concurrent/Executor;)V

    return-object v3
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 4
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0e015f

    .line 197
    move-object v1, p1

    check-cast v1, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/ItemPresenter$ItemViewHolder;->unbind()V

    .line 198
    iget-object v1, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/leanback/ImageCardView;

    .line 200
    .local v0, "imageCardView":Lcom/google/android/music/leanback/ImageCardView;
    invoke-static {v0, v3}, Lcom/google/android/music/leanback/ItemPresenter;->setImageView(Lcom/google/android/music/leanback/ImageCardView;Landroid/graphics/drawable/Drawable;)V

    .line 201
    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/leanback/ImageCardView;->setTag(ILjava/lang/Object;)V

    .line 202
    return-void
.end method

.method protected prepareOverlay(Landroid/widget/ImageView;Lcom/google/android/music/leanback/Item;)V
    .locals 1
    .param p1, "overlayView"    # Landroid/widget/ImageView;
    .param p2, "item"    # Lcom/google/android/music/leanback/Item;

    .prologue
    .line 209
    invoke-virtual {p2}, Lcom/google/android/music/leanback/Item;->getOverlayResourceId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 210
    return-void
.end method
