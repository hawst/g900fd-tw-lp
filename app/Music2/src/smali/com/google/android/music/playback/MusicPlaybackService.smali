.class public Lcom/google/android/music/playback/MusicPlaybackService;
.super Lcom/google/android/music/service/ForegroundService;
.source "MusicPlaybackService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/MusicPlaybackService$16;,
        Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;,
        Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;,
        Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final MSG_ALBUM_ART_UPDATE:I

.field private static final MSG_METADATA_UPDATE:I

.field private static final MSG_PLAYBACKSTATE_UPDATE:I

.field private static final MSG_QUEUE_UPDATE:I

.field private static final MSG_VOICE_SEARCH:I


# instance fields
.field private mArtLoader:Lcom/google/android/music/art/ArtLoader;

.field private mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

.field private mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/google/android/music/playback/DevicePlayback;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mDelayedStopHandler:Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;

.field private mDismissedNotificationIntent:Landroid/app/PendingIntent;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsLandscape:Z

.field mIsNotificationShowing:Z

.field private mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

.field private mMediaButtonReceiver:Landroid/content/ComponentName;

.field private volatile mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

.field private mNextSongIntent:Landroid/app/PendingIntent;

.field mNotification:Landroid/app/Notification;

.field private final mNotificationCanceller:Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;

.field mNotificationHasButtons:Z

.field private mOrientationMonitor:Landroid/content/BroadcastReceiver;

.field private mPlayPauseIntent:Landroid/app/PendingIntent;

.field private mPreviousSongIntent:Landroid/app/PendingIntent;

.field private mQueueContentObserver:Landroid/database/ContentObserver;

.field private mRemotePlaybackControlEnabled:Z

.field private mRetrievedNotificationHasButtons:Z

.field private mServiceInUse:Z

.field private mServiceStartId:I

.field private mSessionCallback:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

.field mUIVisible:Z

.field private mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

.field private mWatchedAlbumArtUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    .line 146
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_METADATA_UPDATE:I

    .line 148
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_ALBUM_ART_UPDATE:I

    .line 150
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_VOICE_SEARCH:I

    .line 152
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_QUEUE_UPDATE:I

    .line 154
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_PLAYBACKSTATE_UPDATE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 266
    invoke-direct {p0}, Lcom/google/android/music/service/ForegroundService;-><init>()V

    .line 106
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceStartId:I

    .line 113
    iput-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceInUse:Z

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;

    .line 117
    iput-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mRetrievedNotificationHasButtons:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotificationHasButtons:Z

    .line 119
    iput-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    .line 120
    iput-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mUIVisible:Z

    .line 135
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    .line 159
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService$1;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 204
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService$2;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

    .line 229
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService$3;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mOrientationMonitor:Landroid/content/BroadcastReceiver;

    .line 250
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$4;

    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/playback/MusicPlaybackService$4;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mQueueContentObserver:Landroid/database/ContentObserver;

    .line 258
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mDelayedStopHandler:Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;

    .line 264
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotificationCanceller:Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;

    .line 582
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$6;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/MusicPlaybackService$6;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mSessionCallback:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    .line 267
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/playback/MusicPlaybackService;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/playback/MusicPlaybackService;->shouldIgnoreCommand(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 97
    sget-boolean v0, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/utils/VoiceActionHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/playback/MusicPlaybackService;Lcom/google/android/music/utils/VoiceActionHelper;)Lcom/google/android/music/utils/VoiceActionHelper;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # Lcom/google/android/music/utils/VoiceActionHelper;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    return-object p1
.end method

.method static synthetic access$1100()I
    .locals 1

    .prologue
    .line 97
    sget v0, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_VOICE_SEARCH:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/playback/MusicPlaybackService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionMetadata(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/playback/MusicPlaybackService;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->getPlaybackErrorString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400()I
    .locals 1

    .prologue
    .line 97
    sget v0, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_ALBUM_ART_UPDATE:I

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mWatchedAlbumArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/playback/session/MediaSessionCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/android/music/playback/MusicPlaybackService;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/google/android/music/playback/MusicPlaybackService;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getMediaSessionTransportControlFlags()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1900(Lcom/google/android/music/playback/MusicPlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->hasLocal()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/playback/MusicPlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->hasRemote()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/playback/LocalDevicePlayback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/music/playback/MusicPlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceInUse:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/music/playback/MusicPlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceStartId:I

    return v0
.end method

.method static synthetic access$2400(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/playback/MusicPlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsLandscape:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/music/playback/MusicPlaybackService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsLandscape:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->updateNotification()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->setMediaSessionQueue()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/playback/MusicPlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mRemotePlaybackControlEnabled:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/music/playback/MusicPlaybackService;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # J

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->skipToTrack(J)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/playback/MusicPlaybackService;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method private addToRecentAsync(Lcom/google/android/music/medialist/SongList;)V
    .locals 3
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1962
    move-object v0, p0

    .line 1963
    .local v0, "context":Landroid/content/Context;
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/MusicPlaybackService$15;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService$15;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1968
    return-void
.end method

.method private alertFailureIfNecessary(I)V
    .locals 6
    .param p1, "errorType"    # I

    .prologue
    .line 1159
    const/4 v3, 0x0

    invoke-static {p1, p0, v3}, Lcom/google/android/music/playback/ErrorInfo;->createErrorInfo(ILandroid/content/Context;Lcom/google/android/music/net/INetworkMonitor;)Lcom/google/android/music/playback/ErrorInfo;

    move-result-object v0

    .line 1160
    .local v0, "handler":Lcom/google/android/music/playback/ErrorInfo;
    invoke-virtual {v0}, Lcom/google/android/music/playback/ErrorInfo;->canShowNotification()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1162
    sget-boolean v3, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v3, :cond_0

    const-string v3, "MusicPlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not showing notification for error type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    :cond_0
    :goto_0
    return-void

    .line 1165
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mUIVisible:Z

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/google/android/music/playback/ErrorInfo;->canShowAlert()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1170
    :cond_2
    invoke-virtual {v0, p0}, Lcom/google/android/music/playback/ErrorInfo;->createNotification(Landroid/content/ContextWrapper;)Landroid/app/Notification;

    move-result-object v2

    .line 1172
    .local v2, "notification":Landroid/app/Notification;
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 1174
    .local v1, "notifMan":Landroid/app/NotificationManager;
    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method private buildLNotification()Landroid/app/Notification;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x14
    .end annotation

    .prologue
    .line 1416
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v1

    .line 1417
    .local v1, "isPlaying":Z
    if-eqz v1, :cond_0

    const v4, 0x7f0b01e6

    :goto_0
    invoke-virtual {p0, v4}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1419
    .local v2, "playpause":Ljava/lang/String;
    if-eqz v1, :cond_1

    const v3, 0x7f020129

    .line 1421
    .local v3, "playpauseIcon":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00b2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1422
    .local v0, "dimension":I
    new-instance v4, Landroid/app/Notification$Builder;

    invoke-direct {v4, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {p0, v0, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumArt(II)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f02025c

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f02014c

    const v6, 0x7f0b01e9

    invoke-virtual {p0, v6}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mPreviousSongIntent:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5, v6, v7}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mPlayPauseIntent:Landroid/app/PendingIntent;

    invoke-virtual {v4, v3, v2, v5}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f0200fe

    const v6, 0x7f0b01e8

    invoke-virtual {p0, v6}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNextSongIntent:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5, v6, v7}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    new-instance v5, Landroid/app/Notification$MediaStyle;

    invoke-direct {v5}, Landroid/app/Notification$MediaStyle;-><init>()V

    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Landroid/app/Notification$MediaStyle;->setShowActionsInCompactView([I)Landroid/app/Notification$MediaStyle;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenAppWithPlaybackScreen(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mDismissedNotificationIntent:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    return-object v4

    .line 1417
    .end local v0    # "dimension":I
    .end local v2    # "playpause":Ljava/lang/String;
    .end local v3    # "playpauseIcon":I
    :cond_0
    const v4, 0x7f0b01e5

    goto/16 :goto_0

    .line 1419
    .restart local v2    # "playpause":Ljava/lang/String;
    :cond_1
    const v3, 0x7f02012c

    goto/16 :goto_1

    .line 1422
    nop

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method private buildRemoteViewNotification()Landroid/app/Notification;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1449
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0400ea

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1451
    .local v1, "notificationLayout":Landroid/widget/RemoteViews;
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->setUpNotificationLocked(Landroid/widget/RemoteViews;Z)V

    .line 1453
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    if-nez v2, :cond_0

    .line 1454
    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    .line 1455
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    iget v3, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Landroid/app/Notification;->flags:I

    .line 1456
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    const v3, 0x7f02025c

    iput v3, v2, Landroid/app/Notification;->icon:I

    .line 1457
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenAppWithPlaybackScreen(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 1461
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    iput-object v1, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1463
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1464
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0400eb

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1466
    .local v0, "bigLayout":Landroid/widget/RemoteViews;
    invoke-direct {p0, v0, v4}, Lcom/google/android/music/playback/MusicPlaybackService;->setUpNotificationLocked(Landroid/widget/RemoteViews;Z)V

    .line 1467
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    invoke-direct {p0, v2, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->setNotificationBigContentView(Landroid/app/Notification;Landroid/widget/RemoteViews;)V

    .line 1469
    .end local v0    # "bigLayout":Landroid/widget/RemoteViews;
    :cond_1
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1470
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    iput v4, v2, Landroid/app/Notification;->visibility:I

    .line 1472
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotification:Landroid/app/Notification;

    return-object v2
.end method

.method private clearDelayedStops()V
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mDelayedStopHandler:Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 555
    return-void
.end method

.method private clearNotificationCancels()V
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotificationCanceller:Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 559
    return-void
.end method

.method private static doesIntentRequireSongInfo(Ljava/lang/String;)Z
    .locals 2
    .param p0, "what"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1011
    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1026
    :cond_0
    :goto_0
    return v0

    .line 1013
    :cond_1
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1015
    const-string v1, "com.google.android.music.repeatmodechanged"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1017
    const-string v1, "com.google.android.music.shufflemodechanged"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1019
    const-string v1, "com.google.android.music.mix.generationfinished"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1021
    const-string v1, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1023
    const-string v1, "com.android.music.mediarouteinvalidated"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1026
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getMediaSessionTransportControlFlags()J
    .locals 2

    .prologue
    .line 1951
    const-wide/16 v0, 0x1cb6

    return-wide v0
.end method

.method private getPlaybackErrorString(I)Ljava/lang/String;
    .locals 5
    .param p1, "errorType"    # I

    .prologue
    .line 989
    sparse-switch p1, :sswitch_data_0

    .line 1000
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/DevicePlayback;->getTrackName()Ljava/lang/String;

    move-result-object v1

    .line 1001
    .local v1, "track":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1002
    const v2, 0x7f0b00e7

    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1007
    .end local v1    # "track":Ljava/lang/String;
    .local v0, "errorMessage":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 994
    .end local v0    # "errorMessage":Ljava/lang/String;
    :sswitch_0
    const/4 v2, 0x0

    invoke-static {p1, p0, v2}, Lcom/google/android/music/playback/ErrorInfo;->createErrorInfo(ILandroid/content/Context;Lcom/google/android/music/net/INetworkMonitor;)Lcom/google/android/music/playback/ErrorInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/playback/ErrorInfo;->getAlertMessage()Ljava/lang/String;

    move-result-object v0

    .line 995
    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 997
    .end local v0    # "errorMessage":Ljava/lang/String;
    :sswitch_1
    const v2, 0x7f0b00eb

    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 998
    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 1004
    .end local v0    # "errorMessage":Ljava/lang/String;
    .restart local v1    # "track":Ljava/lang/String;
    :cond_0
    const v2, 0x7f0b00ea

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 989
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x7 -> :sswitch_0
        0x10 -> :sswitch_0
    .end sparse-switch
.end method

.method private gotoIdleState()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1553
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V

    .line 1554
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->stopSelfDelayed()V

    .line 1555
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearNotificationCancels()V

    .line 1556
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->sendNotificationCancelDelayed()V

    .line 1559
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1560
    const/4 v0, 0x0

    .line 1566
    .local v0, "remove":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->stopForegroundService(Z)V

    .line 1567
    if-eqz v0, :cond_0

    .line 1568
    iput-boolean v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    .line 1570
    :cond_0
    return-void

    .line 1562
    .end local v0    # "remove":Z
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mUIVisible:Z

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->getState()Lcom/google/android/music/playback/DevicePlayback$State;

    move-result-object v1

    sget-object v3, Lcom/google/android/music/playback/DevicePlayback$State;->NO_PLAYLIST:Lcom/google/android/music/playback/DevicePlayback$State;

    if-ne v1, v3, :cond_3

    :cond_2
    const/4 v0, 0x1

    .restart local v0    # "remove":Z
    :goto_1
    goto :goto_0

    .end local v0    # "remove":Z
    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method private hasLocal()Z
    .locals 1

    .prologue
    .line 1684
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->hasLocal()Z

    move-result v0

    return v0
.end method

.method private hasRemote()Z
    .locals 1

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->hasRemote()Z

    move-result v0

    return v0
.end method

.method public static populateExtras(Landroid/content/Intent;Lcom/google/android/music/playback/IMusicPlaybackService;Landroid/content/Context;)Landroid/content/Intent;
    .locals 12
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "service"    # Lcom/google/android/music/playback/IMusicPlaybackService;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1032
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1033
    .local v0, "action":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v6

    .line 1034
    .local v6, "state":Lcom/google/android/music/playback/PlaybackState;
    const-string v8, "com.android.music.queuechanged"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1035
    const-string v8, "queueLoading"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPlaylistLoading()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1036
    const-string v8, "hasValidPlaylist"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->hasValidPlaylist()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1037
    const-string v8, "ListSize"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getQueueSize()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1038
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 1039
    .local v1, "currentMediaList":Lcom/google/android/music/medialist/MediaList;
    if-eqz v1, :cond_0

    .line 1040
    const-string v8, "queueStoreUrl"

    invoke-virtual {v1}, Lcom/google/android/music/medialist/MediaList;->getStoreUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1102
    .end local v1    # "currentMediaList":Lcom/google/android/music/medialist/MediaList;
    :cond_0
    :goto_0
    const-string v8, "com.android.music.playstatechanged"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1103
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v4

    .line 1104
    .local v4, "isPlaying":Z
    const-string v8, "playstate"

    invoke-virtual {p0, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1108
    .end local v4    # "isPlaying":Z
    :cond_1
    :goto_1
    return-object p0

    .line 1042
    :cond_2
    const-string v8, "com.google.android.music.repeatmodechanged"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1043
    const-string v8, "repeat"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getRepeatMode()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1109
    .end local v0    # "action":Ljava/lang/String;
    .end local v6    # "state":Lcom/google/android/music/playback/PlaybackState;
    :catch_0
    move-exception v3

    .line 1110
    .local v3, "e":Landroid/os/RemoteException;
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error trying to get variables from the MusicPlaybackService: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 1044
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v6    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_3
    :try_start_1
    const-string v8, "com.google.android.music.shufflemodechanged"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1045
    const-string v8, "shuffle"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getShuffleMode()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 1047
    :cond_4
    invoke-interface {p1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;

    move-result-object v7

    .line 1049
    .local v7, "trackInfo":Lcom/google/android/music/playback/TrackInfo;
    if-nez v7, :cond_6

    const/4 v5, 0x0

    .line 1050
    .local v5, "songId":Lcom/google/android/music/download/ContentIdentifier;
    :goto_2
    if-eqz v5, :cond_0

    .line 1051
    const-string v8, "id"

    invoke-virtual {v5}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1052
    const-string v8, "domain"

    invoke-virtual {v5}, Lcom/google/android/music/download/ContentIdentifier;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1053
    const-string v8, "artist"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getArtistName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1054
    const-string v8, "album"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getAlbumName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1055
    const-string v8, "albumId"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getAlbumId()J

    move-result-wide v10

    invoke-virtual {p0, v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1056
    const-string v8, "track"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getTrackName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1057
    const-string v8, "rating"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getRating()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1058
    const-string v8, "duration"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getDuration()J

    move-result-wide v10

    invoke-virtual {p0, v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1059
    const-string v8, "previewPlayType"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getPreviewPlayType()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1060
    const-string v8, "albumArtFromService"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getAlbumArtIsFromService()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1062
    const-string v8, "videoId"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getVideoId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1063
    const-string v8, "videoThumbnailUrl"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getVideoThumbnailUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1064
    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    .line 1065
    .local v2, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    if-eqz v2, :cond_5

    .line 1066
    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    .line 1068
    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    .line 1069
    const-string v8, "currentContainerTypeValue"

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/store/ContainerDescriptor$Type;->getDBValue()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1071
    const-string v8, "currentContainerId"

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v10

    invoke-virtual {p0, v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1073
    const-string v8, "currentContainerName"

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1075
    const-string v8, "currentContainerExtId"

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1077
    const-string v8, "currentContainerExtData"

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getExtData()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1080
    :cond_5
    const-string v8, "playing"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1081
    const-string v8, "streaming"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isStreaming()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1082
    const-string v8, "preparing"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1083
    const-string v8, "inErrorState"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isInErrorState()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1084
    const-string v8, "position"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getPosition()J

    move-result-wide v10

    invoke-virtual {p0, v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1085
    const-string v8, "ListPosition"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getQueuePosition()I

    move-result v9

    int-to-long v10, v9

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1086
    const-string v8, "ListSize"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getQueueSize()I

    move-result v9

    int-to-long v10, v9

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1087
    const-string v8, "currentSongLoaded"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isCurrentSongLoaded()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1089
    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v8

    instance-of v8, v8, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v8, :cond_7

    .line 1090
    const-string v9, "externalAlbumArtUrl"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/medialist/ExternalSongList;

    invoke-virtual {v8, p2}, Lcom/google/android/music/medialist/ExternalSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1094
    const-string v8, "supportsRating"

    const/4 v9, 0x0

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1098
    :goto_3
    const-string v8, "local"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->isLocalDevicePlayback()Z

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 1049
    .end local v2    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v5    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_6
    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getSongId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v5

    goto/16 :goto_2

    .line 1096
    .restart local v2    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .restart local v5    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_7
    const-string v8, "supportsRating"

    invoke-virtual {v7}, Lcom/google/android/music/playback/TrackInfo;->getSupportRating()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_3

    .line 1105
    .end local v2    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v5    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    .end local v7    # "trackInfo":Lcom/google/android/music/playback/TrackInfo;
    :cond_8
    const-string v8, "com.android.music.playbackfailed"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1106
    const-string v8, "errorType"

    invoke-virtual {v6}, Lcom/google/android/music/playback/PlaybackState;->getErrorType()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public static populateExtrasFromSharedPreferences(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1122
    const-string v3, "Music"

    invoke-virtual {p0, v3, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1124
    .local v2, "preferences":Landroid/content/SharedPreferences;
    const-string v3, "curalbumid"

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1125
    .local v0, "albumId":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 1126
    const-string v3, "albumArtResourceId"

    const v4, 0x7f020037

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1127
    const-string v3, "error"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 1146
    :goto_0
    return-object p1

    .line 1130
    :cond_0
    const-string v3, "supportsRating"

    invoke-virtual {p1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1131
    const-string v3, "artist"

    const-string v4, "curartist"

    const v5, 0x7f0b00c4

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1134
    const-string v3, "albumId"

    invoke-virtual {p1, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1135
    const-string v3, "album"

    const-string v4, "curalbum"

    const v5, 0x7f0b00c5

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1138
    const-string v3, "track"

    const-string v4, "curtitle"

    const-string v5, ""

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1140
    const-string v3, "albumArtFromService"

    invoke-virtual {p1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1141
    const-string v3, "playing"

    const-string v4, "isplaying"

    invoke-interface {v2, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1143
    const-string v3, "rating"

    const-string v4, "rating"

    invoke-interface {v2, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private removeFailureIfNecessary()V
    .locals 2

    .prologue
    .line 1178
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1180
    .local v0, "notifMan":Landroid/app/NotificationManager;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1181
    return-void
.end method

.method private sendNotificationCancelDelayed()V
    .locals 6

    .prologue
    .line 562
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotificationCanceller:Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;

    invoke-virtual {v2}, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 563
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 564
    .local v1, "time":Landroid/os/Bundle;
    const-string v2, "cancel_notification"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 565
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 566
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotificationCanceller:Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;

    const-wide/32 v4, 0x112a880

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/music/playback/MusicPlaybackService$NotificationCanceller;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 567
    return-void
.end method

.method private setMediaSessionQueue()V
    .locals 4

    .prologue
    .line 1859
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    sget v1, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_QUEUE_UPDATE:I

    new-instance v2, Lcom/google/android/music/playback/MusicPlaybackService$13;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/MusicPlaybackService$13;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;ILjava/lang/Runnable;Z)V

    .line 1869
    return-void
.end method

.method private setNotificationBigContentView(Landroid/app/Notification;Landroid/widget/RemoteViews;)V
    .locals 3
    .param p1, "n"    # Landroid/app/Notification;
    .param p2, "v"    # Landroid/widget/RemoteViews;

    .prologue
    .line 1359
    :try_start_0
    const-class v1, Landroid/app/Notification;

    const-string v2, "bigContentView"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1362
    .local v0, "sNotificationBigContentView":Ljava/lang/reflect/Field;
    :try_start_1
    invoke-virtual {v0, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1369
    .end local v0    # "sNotificationBigContentView":Ljava/lang/reflect/Field;
    :goto_0
    return-void

    .line 1363
    .restart local v0    # "sNotificationBigContentView":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    goto :goto_0

    .line 1366
    .end local v0    # "sNotificationBigContentView":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private setUpNotificationLocked(Landroid/widget/RemoteViews;Z)V
    .locals 10
    .param p1, "notificationLayout"    # Landroid/widget/RemoteViews;
    .param p2, "big"    # Z

    .prologue
    .line 1288
    const v6, 0x7f0e01b5

    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mPreviousSongIntent:Landroid/app/PendingIntent;

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1289
    const v6, 0x7f0e00e1

    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mPlayPauseIntent:Landroid/app/PendingIntent;

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1290
    const v6, 0x7f0e01b8

    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNextSongIntent:Landroid/app/PendingIntent;

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1291
    const v6, 0x7f0e026a

    const/4 v7, 0x0

    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.android.music.musicservicecommand.veto"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v9, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v8, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {p0, v7, v8, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1296
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v1

    .line 1297
    .local v1, "artist":Ljava/lang/String;
    const v6, 0x7f0e01c1

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1298
    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1299
    const v6, 0x7f0b00c4

    invoke-virtual {p0, v6}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1301
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    .line 1302
    .local v0, "album":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1303
    const v6, 0x7f0b00c5

    invoke-virtual {p0, v6}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1306
    :cond_1
    if-eqz p2, :cond_7

    .line 1308
    const v6, 0x7f0e01c3

    invoke-virtual {p1, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1309
    const v6, 0x7f0e01c4

    invoke-virtual {p1, v6, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1317
    :goto_0
    iget-boolean v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mRetrievedNotificationHasButtons:Z

    if-nez v6, :cond_3

    .line 1318
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 1319
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f0400ea

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1320
    .local v5, "v":Landroid/view/View;
    const v6, 0x7f0e01b8

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1321
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotificationHasButtons:Z

    .line 1323
    :cond_2
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mRetrievedNotificationHasButtons:Z

    .line 1325
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "v":Landroid/view/View;
    :cond_3
    iget-boolean v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNotificationHasButtons:Z

    if-eqz v6, :cond_5

    .line 1326
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1327
    const v6, 0x7f0e00e1

    const v7, 0x7f020069

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1334
    :goto_1
    if-nez p2, :cond_4

    iget-boolean v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsLandscape:Z

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1335
    :cond_4
    const v6, 0x7f0e01b5

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1340
    :cond_5
    :goto_2
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1341
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    if-eqz p2, :cond_a

    const v6, 0x7f0f00b3

    :goto_3
    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1345
    .local v3, "dimension":I
    invoke-virtual {p0, v3, v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumArt(II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1346
    .local v2, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_b

    .line 1347
    const v6, 0x7f0e01bd

    invoke-virtual {p1, v6, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 1352
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "dimension":I
    :cond_6
    :goto_4
    return-void

    .line 1312
    :cond_7
    const v6, 0x7f0e0269

    const v7, 0x7f0b03f2

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    const/4 v9, 0x1

    aput-object v0, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 1330
    :cond_8
    const v6, 0x7f0e00e1

    const v7, 0x7f02006a

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    .line 1337
    :cond_9
    const v6, 0x7f0e01b5

    const/16 v7, 0x8

    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_2

    .line 1341
    :cond_a
    const v6, 0x7f0f00b2

    goto :goto_3

    .line 1349
    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    .restart local v3    # "dimension":I
    :cond_b
    const-string v6, "MusicPlaybackService"

    const-string v7, "Failed to set album art for the notification"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method

.method private shouldIgnoreCommand(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "cmd"    # Ljava/lang/String;

    .prologue
    .line 497
    const/4 v1, 0x0

    .line 498
    .local v1, "localOnly":Z
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/DevicePlayback;->isPlayingLocally()Z

    move-result v2

    if-nez v2, :cond_0

    .line 500
    const-string v2, "device"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 501
    .local v0, "deviceExtra":Ljava/lang/String;
    const-string v2, "any"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 502
    const-string v2, "local"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 503
    const/4 v1, 0x1

    .line 510
    .end local v0    # "deviceExtra":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 511
    const-string v2, "MusicPlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ignoring command "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for non-local playback"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    :cond_1
    return v1

    .line 506
    .restart local v0    # "deviceExtra":Ljava/lang/String;
    :cond_2
    const-string v2, "pause"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "com.android.music.musicservicecommand.pause"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v1, 0x1

    :goto_1
    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private skipToTrack(J)V
    .locals 7
    .param p1, "trackId"    # J

    .prologue
    .line 570
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;

    if-nez v3, :cond_1

    .line 580
    :cond_0
    :goto_0
    return-void

    .line 573
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "size":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 574
    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;

    .line 575
    .local v1, "item":Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;
    invoke-virtual {v1}, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->getQueueId()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_2

    .line 576
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->setQueuePosition(I)V

    goto :goto_0

    .line 573
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private stopSelfDelayed()V
    .locals 4

    .prologue
    .line 547
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mDelayedStopHandler:Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;

    invoke-virtual {v1}, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->isStopped()Z

    move-result v1

    if-nez v1, :cond_0

    .line 548
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mDelayedStopHandler:Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;

    invoke-virtual {v1}, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 549
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mDelayedStopHandler:Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;

    const-wide/16 v2, 0x1770

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 551
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private updateMediaSessionMetadata(Z)V
    .locals 33
    .param p1, "downloadIfMissing"    # Z

    .prologue
    .line 1727
    const-string v4, "updateMediaSessionMetadata on main thread"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 1729
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1730
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;

    move-result-object v24

    .line 1731
    .local v24, "currentTrack":Lcom/google/android/music/playback/TrackInfo;
    if-nez v24, :cond_1

    .line 1732
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setMetadata(Lcom/google/android/music/playback/session/MediaMetadataCompat;)V

    .line 1856
    .end local v24    # "currentTrack":Lcom/google/android/music/playback/TrackInfo;
    :cond_0
    :goto_0
    return-void

    .line 1735
    .restart local v24    # "currentTrack":Lcom/google/android/music/playback/TrackInfo;
    :cond_1
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/TrackInfo;->getAlbumName()Ljava/lang/String;

    move-result-object v15

    .line 1736
    .local v15, "albumName":Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/TrackInfo;->getArtistName()Ljava/lang/String;

    move-result-object v16

    .line 1737
    .local v16, "artistName":Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/TrackInfo;->getTrackName()Ljava/lang/String;

    move-result-object v32

    .line 1738
    .local v32, "trackTitle":Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/TrackInfo;->getRating()I

    move-result v30

    .line 1739
    .local v30, "ratingInt":I
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/TrackInfo;->getAlbumId()J

    move-result-wide v20

    .line 1742
    .local v20, "albumId":J
    const/4 v4, 0x5

    move/from16 v0, v30

    if-ne v0, v4, :cond_9

    .line 1743
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/google/android/music/playback/session/RatingCompat;->newThumbRating(Z)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v29

    .line 1751
    .local v29, "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :goto_1
    new-instance v4, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    invoke-direct {v4}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;-><init>()V

    const-string v8, "android.media.metadata.TITLE"

    move-object/from16 v0, v32

    invoke-virtual {v4, v8, v0}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    move-result-object v4

    const-string v8, "android.media.metadata.ARTIST"

    move-object/from16 v0, v16

    invoke-virtual {v4, v8, v0}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    move-result-object v4

    const-string v8, "android.media.metadata.ALBUM"

    invoke-virtual {v4, v8, v15}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    move-result-object v4

    const-string v8, "android.media.metadata.ALBUM_ARTIST"

    move-object/from16 v0, v16

    invoke-virtual {v4, v8, v0}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    move-result-object v4

    const-string v8, "android.media.metadata.DURATION"

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/TrackInfo;->getDuration()J

    move-result-wide v10

    invoke-virtual {v4, v8, v10, v11}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->putLong(Ljava/lang/String;J)Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    move-result-object v4

    const-string v8, "android.media.metadata.USER_RATING"

    move-object/from16 v0, v29

    invoke-virtual {v4, v8, v0}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->putRating(Ljava/lang/String;Lcom/google/android/music/playback/session/RatingCompat;)Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    move-result-object v27

    .line 1765
    .local v27, "mediaMetadata":Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;
    const/16 v22, 0x0

    .line 1767
    .local v22, "bitmap":Landroid/graphics/Bitmap;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x12

    if-le v4, v8, :cond_b

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/utils/SystemUtils;->isLowMemory(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1769
    const/16 v6, 0x400

    .line 1770
    .local v6, "lockscreenWidth":I
    const/16 v7, 0x400

    .line 1776
    .local v7, "lockscreenHeight":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v4}, Lcom/google/android/music/playback/DevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v26

    .line 1777
    .local v26, "list":Lcom/google/android/music/medialist/MediaList;
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/playback/TrackInfo;->getAlbumArtIsFromService()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    .line 1778
    .local v19, "albumArtIsFromService":Z
    if-nez v19, :cond_2

    move-object/from16 v0, v26

    instance-of v4, v0, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v4, :cond_d

    .line 1780
    :cond_2
    if-eqz v19, :cond_c

    .line 1781
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->getAlbumArtUrl(J)Ljava/lang/String;

    move-result-object v5

    .line 1786
    .local v5, "url":Ljava/lang/String;
    :goto_3
    if-eqz v5, :cond_3

    .line 1787
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 1796
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    :goto_4
    if-nez v22, :cond_7

    .line 1797
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumArtLocation(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v28

    .line 1798
    .local v28, "missingUrl":Ljava/lang/String;
    sget-boolean v4, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v4, :cond_4

    .line 1799
    const-string v4, "MusicPlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Album art is missing for album id "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, v20

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", register listener for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1802
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mWatchedAlbumArtUrl:Ljava/lang/String;

    if-nez v4, :cond_5

    .line 1803
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/playback/MusicPlaybackService;->mWatchedAlbumArtUrl:Ljava/lang/String;

    .line 1806
    :cond_5
    if-eqz p1, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mWatchedAlbumArtUrl:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 1808
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mArtLoader:Lcom/google/android/music/art/ArtLoader;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mWatchedAlbumArtUrl:Ljava/lang/String;

    sget-object v9, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    new-instance v10, Lcom/google/android/music/playback/MusicPlaybackService$12;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/music/playback/MusicPlaybackService$12;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    invoke-interface {v4, v8, v9, v10}, Lcom/google/android/music/art/ArtLoader;->getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;)V

    .line 1832
    :cond_6
    const/4 v10, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object/from16 v9, p0

    move-wide/from16 v11, v20

    move v13, v6

    move v14, v7

    invoke-static/range {v9 .. v18}, Lcom/google/android/music/utils/AlbumArtUtils;->getDefaultArtwork(Landroid/content/Context;ZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 1836
    .end local v28    # "missingUrl":Ljava/lang/String;
    :cond_7
    if-eqz v22, :cond_e

    .line 1843
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    if-nez v4, :cond_8

    .line 1844
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v31

    .line 1846
    .local v31, "temp":Landroid/graphics/Bitmap;
    new-instance v23, Landroid/graphics/Canvas;

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1847
    .local v23, "c":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1848
    move-object/from16 v22, v31

    .line 1850
    .end local v23    # "c":Landroid/graphics/Canvas;
    .end local v31    # "temp":Landroid/graphics/Bitmap;
    :cond_8
    const-string v4, "android.media.metadata.ALBUM_ART"

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;

    .line 1854
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;->build()Lcom/google/android/music/playback/session/MediaMetadataCompat;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setMetadata(Lcom/google/android/music/playback/session/MediaMetadataCompat;)V

    goto/16 :goto_0

    .line 1744
    .end local v6    # "lockscreenWidth":I
    .end local v7    # "lockscreenHeight":I
    .end local v19    # "albumArtIsFromService":Z
    .end local v22    # "bitmap":Landroid/graphics/Bitmap;
    .end local v26    # "list":Lcom/google/android/music/medialist/MediaList;
    .end local v27    # "mediaMetadata":Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;
    .end local v29    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :cond_9
    const/4 v4, 0x1

    move/from16 v0, v30

    if-ne v0, v4, :cond_a

    .line 1745
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/google/android/music/playback/session/RatingCompat;->newThumbRating(Z)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v29

    .restart local v29    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    goto/16 :goto_1

    .line 1747
    .end local v29    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    :cond_a
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/google/android/music/playback/session/RatingCompat;->newUnratedRating(I)Lcom/google/android/music/playback/session/RatingCompat;

    move-result-object v29

    .restart local v29    # "rating":Lcom/google/android/music/playback/session/RatingCompat;
    goto/16 :goto_1

    .line 1772
    .restart local v22    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v27    # "mediaMetadata":Lcom/google/android/music/playback/session/MediaMetadataCompat$Builder;
    :cond_b
    const/16 v6, 0x100

    .line 1773
    .restart local v6    # "lockscreenWidth":I
    const/16 v7, 0x100

    .restart local v7    # "lockscreenHeight":I
    goto/16 :goto_2

    .restart local v19    # "albumArtIsFromService":Z
    .restart local v26    # "list":Lcom/google/android/music/medialist/MediaList;
    :cond_c
    move-object/from16 v25, v26

    .line 1783
    check-cast v25, Lcom/google/android/music/medialist/ExternalSongList;

    .line 1784
    .local v25, "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/ExternalSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "url":Ljava/lang/String;
    goto/16 :goto_3

    .line 1791
    .end local v5    # "url":Ljava/lang/String;
    .end local v25    # "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    :cond_d
    const/4 v14, 0x0

    const/16 v17, 0x1

    move-object/from16 v9, p0

    move-wide/from16 v10, v20

    move v12, v6

    move v13, v7

    invoke-static/range {v9 .. v17}, Lcom/google/android/music/utils/AlbumArtUtils;->getLockScreenArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v22

    goto/16 :goto_4

    .line 1852
    :cond_e
    const-string v4, "MusicPlaybackService"

    const-string v8, "Failed to set album art for the lock screen"

    invoke-static {v4, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private updateMediaSessionPlaystate(ZLjava/lang/String;)V
    .locals 4
    .param p1, "inError"    # Z
    .param p2, "errorMsg"    # Ljava/lang/String;

    .prologue
    .line 1900
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    sget v1, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_PLAYBACKSTATE_UPDATE:I

    new-instance v2, Lcom/google/android/music/playback/MusicPlaybackService$14;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService$14;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;ZLjava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;ILjava/lang/Runnable;Z)V

    .line 1943
    return-void
.end method

.method private declared-synchronized updateNotification()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1372
    monitor-enter p0

    :try_start_0
    const-string v3, "updateNotification called on UI thread"

    invoke-static {p0, v3}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 1374
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1375
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1376
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1377
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    .line 1390
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    if-eqz v3, :cond_1

    .line 1391
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_use_system_media_notificaion"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1396
    .local v2, "useLNotificationApis":Z
    :goto_1
    if-eqz v2, :cond_6

    .line 1397
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->buildLNotification()Landroid/app/Notification;

    move-result-object v1

    .line 1402
    .local v1, "notification":Landroid/app/Notification;
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isForeground()Z

    move-result v3

    if-nez v3, :cond_7

    .line 1403
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->startForegroundService(ILandroid/app/Notification;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1411
    .end local v1    # "notification":Landroid/app/Notification;
    .end local v2    # "useLNotificationApis":Z
    :cond_1
    :goto_3
    monitor-exit p0

    return-void

    .line 1379
    :cond_2
    :try_start_1
    iget-boolean v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mUIVisible:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1380
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1372
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1383
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1384
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    goto :goto_0

    .line 1386
    :cond_4
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->gotoIdleState()V

    goto :goto_0

    .line 1391
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 1399
    .restart local v2    # "useLNotificationApis":Z
    :cond_6
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->buildRemoteViewNotification()Landroid/app/Notification;

    move-result-object v1

    .restart local v1    # "notification":Landroid/app/Notification;
    goto :goto_2

    .line 1405
    :cond_7
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1407
    .local v0, "nmgr":Landroid/app/NotificationManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method public cancelMix()V
    .locals 1

    .prologue
    .line 1896
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->cancelMix()V

    .line 1897
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 2311
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentPlayback:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2312
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "localPlayback:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2314
    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/playback/DevicePlayback;

    .line 2315
    .local v1, "playback":Lcom/google/android/music/playback/DevicePlayback;
    if-eqz v1, :cond_0

    .line 2316
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Has playlist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->hasValidPlaylist()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2317
    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    .line 2318
    .local v0, "list":Lcom/google/android/music/medialist/MediaList;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->getQueueSize()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " items in queue, currently at index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getQueuePosition()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2320
    if-nez v0, :cond_1

    .line 2321
    const-string v2, "MediaList: null"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2327
    :goto_0
    const-string v2, "Currently loaded:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2328
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2329
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2330
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2331
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Playback State: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->getState()Lcom/google/android/music/playback/DevicePlayback$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2332
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Shuffle mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getShuffleMode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Repeat mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getRepeatMode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2334
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Infinite mix mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isInfiniteMixMode()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2335
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->isInErrorState()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2336
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->getErrorType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2337
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preparing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->isPreparing()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2338
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Streaming: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->isStreaming()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2339
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->playlistLoading()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2340
    invoke-virtual {v1, p2}, Lcom/google/android/music/playback/DevicePlayback;->dump(Ljava/io/PrintWriter;)V

    .line 2341
    invoke-static {p2}, Lcom/google/android/music/utils/MusicUtils;->debugDump(Ljava/io/PrintWriter;)V

    .line 2345
    .end local v0    # "list":Lcom/google/android/music/medialist/MediaList;
    :cond_0
    const-string v2, "Background worker:\n"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2346
    sget-object v2, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Landroid/util/PrintWriterPrinter;

    invoke-direct {v3, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/utils/LoggableHandler;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    .line 2347
    return-void

    .line 2323
    .restart local v0    # "list":Lcom/google/android/music/medialist/MediaList;
    :cond_1
    const-string v2, "MediaList:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2324
    invoke-virtual {v0, p2}, Lcom/google/android/music/medialist/MediaList;->dump(Ljava/io/PrintWriter;)V

    goto/16 :goto_0
.end method

.method public duration()J
    .locals 2

    .prologue
    .line 1696
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAlbumArt(II)Landroid/graphics/Bitmap;
    .locals 18
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 1476
    const/4 v15, 0x0

    .line 1477
    .local v15, "bm":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/DevicePlayback;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v17

    .line 1478
    .local v17, "list":Lcom/google/android/music/medialist/MediaList;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    invoke-virtual {v2}, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->isAlbumArtInService()Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, v17

    instance-of v2, v0, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v2, :cond_3

    .line 1480
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    invoke-virtual {v2}, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->isAlbumArtInService()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    invoke-virtual {v4}, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->getAlbumId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->getAlbumArtUrl(J)Ljava/lang/String;

    move-result-object v3

    .line 1486
    .local v3, "url":Ljava/lang/String;
    :goto_0
    if-eqz v3, :cond_1

    .line 1487
    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object/from16 v2, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-static/range {v2 .. v8}, Lcom/google/android/music/utils/AlbumArtUtils;->getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 1494
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v15

    :cond_2
    move-object/from16 v16, v17

    .line 1483
    check-cast v16, Lcom/google/android/music/medialist/ExternalSongList;

    .line 1484
    .local v16, "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/ExternalSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0

    .line 1491
    .end local v3    # "url":Ljava/lang/String;
    .end local v16    # "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getAlbumId()J

    move-result-wide v6

    .line 1492
    .local v6, "aid":J
    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v5, p0

    move/from16 v8, p1

    move/from16 v9, p2

    invoke-static/range {v5 .. v14}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    goto :goto_1
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 1656
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getAlbumId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1652
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtistId()J
    .locals 2

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getArtistId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1636
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAudioId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 1

    .prologue
    .line 1595
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 1723
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;
    .locals 1

    .prologue
    .line 1872
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;

    move-result-object v0

    return-object v0
.end method

.method public getErrorType()I
    .locals 1

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getErrorType()I

    move-result v0

    return v0
.end method

.method getMediaSession()Lcom/google/android/music/playback/session/MediaSessionCompat;
    .locals 1

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    return-object v0
.end method

.method public getMixState()Lcom/google/android/music/mix/MixGenerationState;
    .locals 1

    .prologue
    .line 1892
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getMixState()Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v0

    return-object v0
.end method

.method public getPlaybackState()Lcom/google/android/music/playback/PlaybackState;
    .locals 1

    .prologue
    .line 1876
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewPlayType()I
    .locals 1

    .prologue
    .line 1710
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getPreviewPlayType()I

    move-result v0

    return v0
.end method

.method public getQueuePosition()I
    .locals 1

    .prologue
    .line 1607
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getQueuePosition()I

    move-result v0

    return v0
.end method

.method public getQueueSize()I
    .locals 1

    .prologue
    .line 1624
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getQueueSize()I

    move-result v0

    return v0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getRating()I

    move-result v0

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getRepeatMode()I

    move-result v0

    return v0
.end method

.method public getSelectedMediaRouteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2350
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getSelectedMediaRouteId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShuffleMode()I
    .locals 1

    .prologue
    .line 1577
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getShuffleMode()I

    move-result v0

    return v0
.end method

.method public getSongStoreId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1648
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getSongStoreId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSortableAlbumArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getSortableAlbumArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrackName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->getTrackName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBuffering()Z
    .locals 1

    .prologue
    .line 1524
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isPreparing()Z

    move-result v0

    return v0
.end method

.method public isCurrentSongLoaded()Z
    .locals 1

    .prologue
    .line 1632
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isCurrentSongLoaded()Z

    move-result v0

    return v0
.end method

.method public isInCloudQueueMode()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2354
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    if-nez v5, :cond_1

    .line 2379
    :cond_0
    :goto_0
    return v4

    .line 2364
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2365
    .local v2, "resolver":Landroid/content/ContentResolver;
    const-string v5, "music_enable_chromecast_cloud_queue"

    invoke-static {v2, v5, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 2369
    .local v0, "isCastCloudQueueEnabled":Z
    const-string v5, "music_enable_dial_cloud_queue"

    invoke-static {v2, v5, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 2373
    .local v1, "isDialCloudQueueEnabled":Z
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->getConnectedMediaRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v3

    .line 2375
    .local v3, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-eqz v3, :cond_0

    .line 2379
    if-eqz v0, :cond_2

    invoke-static {v3}, Lcom/google/android/music/cast/CastUtils;->isCastV2Route(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    if-eqz v1, :cond_0

    invoke-static {v3}, Lcom/google/android/music/cast/CastUtils;->isDialRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public isInfiniteMixMode()Z
    .locals 1

    .prologue
    .line 1880
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isInfiniteMixMode()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isPlaylistLoading()Z
    .locals 1

    .prologue
    .line 1599
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->playlistLoading()Z

    move-result v0

    return v0
.end method

.method public isUIVisible()Z
    .locals 1

    .prologue
    .line 1274
    iget-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mUIVisible:Z

    return v0
.end method

.method public next()V
    .locals 1

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->next()V

    .line 1549
    return-void
.end method

.method notifyChange(Ljava/lang/String;Lcom/google/android/music/playback/DevicePlayback;)V
    .locals 13
    .param p1, "what"    # Ljava/lang/String;
    .param p2, "playback"    # Lcom/google/android/music/playback/DevicePlayback;

    .prologue
    const/4 v12, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 883
    sget-boolean v9, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v9, :cond_0

    .line 884
    const-string v9, "MusicPlaybackService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "notifyChange: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    :cond_0
    iget-object v9, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v9

    if-eq p2, v9, :cond_2

    .line 888
    sget-boolean v7, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v7, :cond_1

    .line 889
    const-string v7, "MusicPlaybackService"

    const-string v8, "Received update from non-current device. Ignoring."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    :cond_1
    :goto_0
    return-void

    .line 895
    :cond_2
    iget-object v9, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    invoke-virtual {v9}, Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v9

    if-eqz v9, :cond_3

    move v1, v7

    .line 896
    .local v1, "hasSong":Z
    :goto_1
    if-nez v1, :cond_4

    invoke-static {p1}, Lcom/google/android/music/playback/MusicPlaybackService;->doesIntentRequireSongInfo(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 897
    sget-boolean v7, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v7, :cond_1

    .line 898
    const-string v7, "MusicPlaybackService"

    const-string v8, "Song is not loaded. Ignoring."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v1    # "hasSong":Z
    :cond_3
    move v1, v8

    .line 895
    goto :goto_1

    .line 903
    .restart local v1    # "hasSong":Z
    :cond_4
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 904
    .local v2, "intent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    invoke-static {v2, v9, p0}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtras(Landroid/content/Intent;Lcom/google/android/music/playback/IMusicPlaybackService;Landroid/content/Context;)Landroid/content/Intent;

    .line 906
    const-string v9, "com.android.music.asyncopenstart"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 907
    invoke-direct {p0, v8, v12}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V

    .line 974
    :cond_5
    :goto_2
    sget-boolean v7, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v7, :cond_6

    .line 975
    const-string v7, "MusicPlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sending out broadcast: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Extras: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    :cond_6
    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->sendBroadcast(Landroid/content/Intent;)V

    .line 980
    sget-object v7, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v8, Lcom/google/android/music/playback/MusicPlaybackService$8;

    invoke-direct {v8, p0}, Lcom/google/android/music/playback/MusicPlaybackService$8;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    invoke-static {v7, v8}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 908
    :cond_7
    const-string v9, "com.android.music.asyncopencomplete"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 909
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v7

    if-nez v7, :cond_8

    .line 910
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->gotoIdleState()V

    .line 912
    :cond_8
    invoke-direct {p0, v8, v12}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V

    .line 913
    const-string v7, "com.android.music.asyncopencomplete"

    invoke-static {p0, v2, v7}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_2

    .line 914
    :cond_9
    const-string v9, "com.android.music.playstatechanged"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 915
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v3

    .line 916
    .local v3, "isPlaying":Z
    if-nez v3, :cond_a

    .line 917
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->gotoIdleState()V

    .line 919
    :cond_a
    invoke-static {p0, v2, p1}, Lcom/google/android/music/homewidgets/IFLWidgetProvider;->notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    .line 920
    invoke-static {p0, v2, p1}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    .line 921
    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v7}, Lcom/google/android/music/playback/DevicePlayback;->isInErrorState()Z

    move-result v7

    if-nez v7, :cond_b

    .line 922
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->removeFailureIfNecessary()V

    .line 924
    :cond_b
    invoke-direct {p0, v8, v12}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V

    goto/16 :goto_2

    .line 925
    .end local v3    # "isPlaying":Z
    :cond_c
    const-string v8, "com.android.music.playbackfailed"

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 926
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getErrorType()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->getPlaybackErrorString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V

    .line 927
    invoke-static {p0, v2, p1}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    .line 928
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getErrorType()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/google/android/music/playback/MusicPlaybackService;->alertFailureIfNecessary(I)V

    goto/16 :goto_2

    .line 929
    :cond_d
    const-string v8, "com.android.music.metachanged"

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 930
    sget-object v8, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    sget v9, Lcom/google/android/music/playback/MusicPlaybackService;->MSG_METADATA_UPDATE:I

    new-instance v10, Lcom/google/android/music/playback/MusicPlaybackService$7;

    invoke-direct {v10, p0, v2, p1}, Lcom/google/android/music/playback/MusicPlaybackService$7;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;Landroid/content/Intent;Ljava/lang/String;)V

    invoke-static {v8, v9, v10, v7}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;ILjava/lang/Runnable;Z)V

    goto/16 :goto_2

    .line 952
    :cond_e
    const-string v7, "com.android.music.queuechanged"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 953
    invoke-static {p0, v2, p1}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 954
    :cond_f
    const-string v7, "com.google.android.music.mix.generationfinished"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 955
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getMixState()Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v4

    .line 956
    .local v4, "mixState":Lcom/google/android/music/mix/MixGenerationState;
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/google/android/music/mix/MixGenerationState;->getState()Lcom/google/android/music/mix/MixGenerationState$State;

    move-result-object v7

    sget-object v8, Lcom/google/android/music/mix/MixGenerationState$State;->FAILED:Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v7, v8}, Lcom/google/android/music/mix/MixGenerationState$State;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 958
    new-instance v6, Ljava/lang/Object;

    invoke-direct {v6}, Ljava/lang/Object;-><init>()V

    .line 959
    .local v6, "prefsOwner":Ljava/lang/Object;
    invoke-static {p0, v6}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    .line 962
    .local v5, "musicPrefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v7

    if-eqz v7, :cond_10

    .line 963
    const v7, 0x7f0b01d6

    invoke-virtual {p0, v7}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 967
    .local v0, "errorMessage":Ljava/lang/String;
    :goto_3
    const/4 v7, 0x1

    invoke-direct {p0, v7, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 969
    invoke-static {v6}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 965
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_10
    const v7, 0x7f0b01d7

    :try_start_1
    invoke-virtual {p0, v7}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_3

    .line 969
    .end local v0    # "errorMessage":Ljava/lang/String;
    :catchall_0
    move-exception v7

    invoke-static {v6}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v7
.end method

.method notifyFailure(Lcom/google/android/music/playback/DevicePlayback;)V
    .locals 1
    .param p1, "device"    # Lcom/google/android/music/playback/DevicePlayback;

    .prologue
    .line 1150
    const-string v0, "com.android.music.playbackfailed"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->notifyChange(Ljava/lang/String;Lcom/google/android/music/playback/DevicePlayback;)V

    .line 1151
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 393
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceInUse:Z

    .line 395
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicStub:Lcom/google/android/music/playback/MusicPlaybackService$MusicPlaybackServiceImpl;

    return-object v0
.end method

.method public onCreate()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 271
    invoke-super {p0}, Lcom/google/android/music/service/ForegroundService;->onCreate()V

    .line 273
    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-class v9, Lcom/google/android/music/playback/MediaButtonIntentReceiver;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaButtonReceiver:Landroid/content/ComponentName;

    .line 276
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 277
    .local v0, "audioNoisyFilter":Landroid/content/IntentFilter;
    const-string v5, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 278
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 280
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 281
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v5, v8, :cond_2

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsLandscape:Z

    .line 284
    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.android.music.musicservicecommand.previous"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v8, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v5, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0, v7, v5, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mPreviousSongIntent:Landroid/app/PendingIntent;

    .line 286
    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.android.music.musicservicecommand.togglepause"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v8, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v5, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0, v7, v5, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mPlayPauseIntent:Landroid/app/PendingIntent;

    .line 288
    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.android.music.musicservicecommand.next"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v8, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v5, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0, v7, v5, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mNextSongIntent:Landroid/app/PendingIntent;

    .line 290
    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.android.music.musicservicecommand.veto"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v8, Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v5, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0, v7, v5, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mDismissedNotificationIntent:Landroid/app/PendingIntent;

    .line 293
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "music_enable_rcc_playback_position"

    invoke-static {v5, v8, v6}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mRemotePlaybackControlEnabled:Z

    .line 297
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 298
    .local v3, "i":Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaButtonReceiver:Landroid/content/ComponentName;

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 299
    invoke-static {p0, v7, v3, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 301
    .local v4, "pi":Landroid/app/PendingIntent;
    const v5, 0x7f0b00e3

    invoke-virtual {p0, v5}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v8, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v8}, Lcom/google/android/music/utils/LoggableHandler;->getLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-static {p0, v5, v4, v8}, Lcom/google/android/music/playback/MediaSessionUtil;->getSessionInstance(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/os/Looper;)Lcom/google/android/music/playback/session/MediaSessionCompat;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    .line 303
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->isLeanbackEnvironment(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 304
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->newNowPlayingPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v8

    invoke-static {p0, v7, v8, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setSessionActivity(Landroid/app/PendingIntent;)V

    .line 307
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v7, 0x3

    invoke-virtual {v5, v7}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setFlags(I)V

    .line 309
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const v7, 0x7f0b0385

    invoke-virtual {p0, v7}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setQueueTitle(Ljava/lang/CharSequence;)V

    .line 310
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->setMediaSessionQueue()V

    .line 311
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    invoke-static {}, Lcom/google/android/music/playback/MediaSessionUtil;->getSessionExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setExtras(Landroid/os/Bundle;)V

    .line 313
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mSessionCallback:Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;

    invoke-virtual {v5, v7}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;)V

    .line 314
    invoke-static {p0}, Lcom/google/android/music/art/ArtLoaderFactory;->getArtLoader(Landroid/content/Context;)Lcom/google/android/music/art/ArtLoader;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mArtLoader:Lcom/google/android/music/art/ArtLoader;

    .line 316
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 317
    .local v1, "commandFilter":Landroid/content/IntentFilter;
    const-string v5, "com.android.music.musicservicecommand"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 318
    const-string v5, "com.android.music.musicservicecommand.togglepause"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 319
    const-string v5, "com.android.music.musicservicecommand.pause"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 320
    const-string v5, "com.android.music.musicservicecommand.next"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 321
    const-string v5, "com.android.music.musicservicecommand.previous"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 322
    const-string v5, "com.google.android.music.clearqueue"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 323
    const-string v5, "com.android.music.playstatusrequest"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 324
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 326
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 327
    .local v2, "filter":Landroid/content/IntentFilter;
    new-instance v2, Landroid/content/IntentFilter;

    .end local v2    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 328
    .restart local v2    # "filter":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 329
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mOrientationMonitor:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 333
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->stopSelfDelayed()V

    .line 335
    new-instance v5, Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {v5, p0}, Lcom/google/android/music/playback/LocalDevicePlayback;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    iput-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    .line 336
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->onCreate()V

    .line 338
    iget-object v5, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v7, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v5, v7}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 339
    sget-boolean v5, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v5, :cond_1

    .line 340
    const-string v5, "MusicPlaybackService"

    const-string v7, "Playback service created"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v7, Lcom/google/android/music/store/MusicContent$Queue;->CONTENT_URI:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mQueueContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v5, v7, v6, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 345
    return-void

    .end local v1    # "commandFilter":Landroid/content/IntentFilter;
    .end local v2    # "filter":Landroid/content/IntentFilter;
    .end local v3    # "i":Landroid/content/Intent;
    .end local v4    # "pi":Landroid/app/PendingIntent;
    :cond_2
    move v5, v7

    .line 281
    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 350
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    const-string v0, "MusicPlaybackService"

    const-string v1, "Service being destroyed while still playing."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_0
    sget-boolean v0, Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z

    if-eqz v0, :cond_1

    .line 354
    const-string v0, "MusicPlaybackService"

    const-string v1, "Playback service destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V

    .line 359
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearNotificationCancels()V

    .line 361
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mOrientationMonitor:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mAudioNoisyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/MusicPlaybackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 365
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 366
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onDestroy()V

    .line 367
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    .line 368
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    if-eqz v0, :cond_2

    .line 371
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    invoke-virtual {v0}, Lcom/google/android/music/utils/VoiceActionHelper;->cancelSearch()V

    .line 372
    iput-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;

    .line 375
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mQueueContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_3

    .line 376
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mQueueContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 377
    iput-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mQueueContentObserver:Landroid/database/ContentObserver;

    .line 380
    :cond_3
    invoke-static {p0}, Lcom/google/android/music/playback/MediaSessionUtil;->releaseSessionInstance(Landroid/content/Context;)V

    .line 382
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 383
    invoke-super {p0}, Lcom/google/android/music/service/ForegroundService;->onDestroy()V

    .line 384
    return-void

    .line 368
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 400
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V

    .line 401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceInUse:Z

    .line 402
    return-void
.end method

.method public declared-synchronized onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 388
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "startIntent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 406
    iput p3, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceStartId:I

    .line 407
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V

    .line 408
    move-object v2, p1

    .line 409
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 410
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 411
    .local v0, "action":Ljava/lang/String;
    const-string v6, "command"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 412
    .local v1, "cmd":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStartCommand "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 414
    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->shouldIgnoreCommand(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 415
    const-string v6, "next"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "com.android.music.musicservicecommand.next"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 416
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->next()V

    .line 484
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "cmd":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V

    .line 485
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->stopSelfDelayed()V

    .line 487
    if-eqz p1, :cond_2

    sget-object v6, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->RELEASE_RECEIVER_LOCK:Ljava/lang/String;

    invoke-virtual {p1, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v6, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v6, :cond_2

    .line 491
    sget-object v6, Lcom/google/android/music/playback/MediaButtonIntentReceiver;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 493
    :cond_2
    return v8

    .line 417
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "cmd":Ljava/lang/String;
    :cond_3
    const-string v6, "previous"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "com.android.music.musicservicecommand.previous"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 418
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->prev()V

    goto :goto_0

    .line 419
    :cond_5
    const-string v6, "togglepause"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "com.android.music.musicservicecommand.togglepause"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 420
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 421
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    .line 422
    const-string v6, "removeNotification"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 423
    invoke-virtual {p0, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->stopForegroundService(Z)V

    .line 424
    iput-boolean v9, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    goto :goto_0

    .line 432
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaylistLoading()Z

    move-result v6

    if-nez v6, :cond_8

    iget-object v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v6}, Lcom/google/android/music/playback/DevicePlayback;->getQueueSize()I

    move-result v6

    if-lez v6, :cond_9

    .line 433
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    goto :goto_0

    .line 436
    :cond_9
    invoke-virtual {p0, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->setShuffleMode(I)V

    .line 437
    new-instance v6, Lcom/google/android/music/medialist/AllSongsList;

    invoke-direct {v6, v9}, Lcom/google/android/music/medialist/AllSongsList;-><init>(I)V

    invoke-virtual {p0, v6, v10, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    goto :goto_0

    .line 441
    :cond_a
    const-string v6, "pause"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_b

    const-string v6, "com.android.music.musicservicecommand.pause"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 442
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    .line 443
    invoke-virtual {p0, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->stopForegroundService(Z)V

    .line 444
    iput-boolean v9, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    goto/16 :goto_0

    .line 445
    :cond_c
    const-string v6, "play"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 446
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    goto/16 :goto_0

    .line 447
    :cond_d
    const-string v6, "stop"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 448
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->stop()V

    .line 449
    invoke-virtual {p0, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->stopForegroundService(Z)V

    .line 450
    iput-boolean v9, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    goto/16 :goto_0

    .line 451
    :cond_e
    const-string v6, "com.android.music.musicservicecommand.veto"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 453
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    .line 454
    invoke-virtual {p0, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->stopForegroundService(Z)V

    .line 455
    iput-boolean v9, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    goto/16 :goto_0

    .line 456
    :cond_f
    const-string v6, "com.android.music.musicservicecommand.rating"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 457
    const-string v6, "rating"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 458
    .local v5, "rating":I
    if-ltz v5, :cond_1

    .line 459
    sget-object v6, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v7, Lcom/google/android/music/playback/MusicPlaybackService$5;

    invoke-direct {v7, p0, v5}, Lcom/google/android/music/playback/MusicPlaybackService$5;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;I)V

    invoke-static {v6, v7}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 465
    .end local v5    # "rating":I
    :cond_10
    const-string v6, "com.google.android.music.playSongList"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 466
    const-string v6, "songlist"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/medialist/SongList;

    .line 467
    .local v3, "list":Lcom/google/android/music/medialist/SongList;
    if-nez v3, :cond_11

    .line 468
    const-string v6, "MusicPlaybackService"

    const-string v7, "Missing song list"

    invoke-static {v6, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 470
    :cond_11
    const-string v6, "position"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 471
    .local v4, "position":I
    invoke-virtual {p0, v3, v4, v8}, Lcom/google/android/music/playback/MusicPlaybackService;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    goto/16 :goto_0

    .line 473
    .end local v3    # "list":Lcom/google/android/music/medialist/SongList;
    .end local v4    # "position":I
    :cond_12
    const-string v6, "com.google.android.music.clearqueue"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 474
    iget-object v6, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v6}, Lcom/google/android/music/playback/DevicePlayback;->clearQueue()V

    goto/16 :goto_0

    .line 475
    :cond_13
    const-string v6, "com.google.android.music.shuffleOnDevice"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 476
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->cancelMix()V

    .line 477
    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->shuffleOnDevice()V

    goto/16 :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 519
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mServiceInUse:Z

    .line 522
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->saveState()V

    .line 524
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->getState()Lcom/google/android/music/playback/DevicePlayback$State;

    move-result-object v0

    .line 525
    .local v0, "deviceState":Lcom/google/android/music/playback/DevicePlayback$State;
    sget-object v1, Lcom/google/android/music/playback/MusicPlaybackService$16;->$SwitchMap$com$google$android$music$playback$DevicePlayback$State:[I

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 543
    :goto_0
    :pswitch_0
    return v3

    .line 536
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->stopSelfDelayed()V

    goto :goto_0

    .line 540
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->stopSelfDelayed()V

    goto :goto_0

    .line 525
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public open(Lcom/google/android/music/medialist/SongList;IZ)V
    .locals 2
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "position"    # I
    .param p3, "play"    # Z

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setActive(Z)V

    .line 1193
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->addToRecentAsync(Lcom/google/android/music/medialist/SongList;)V

    .line 1194
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/playback/DevicePlayback;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    .line 1195
    return-void
.end method

.method public openAndQueue(Lcom/google/android/music/medialist/SongList;I)V
    .locals 2
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "position"    # I

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setActive(Z)V

    .line 1206
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->addToRecentAsync(Lcom/google/android/music/medialist/SongList;)V

    .line 1207
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/DevicePlayback;->openAndQueue(Lcom/google/android/music/medialist/SongList;I)V

    .line 1208
    return-void
.end method

.method public openMix(Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 1
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 1884
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/DevicePlayback;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 1885
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 1508
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->pause()V

    .line 1509
    return-void
.end method

.method public play()V
    .locals 2

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setActive(Z)V

    .line 1215
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->play()V

    .line 1216
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearNotificationCancels()V

    .line 1217
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/MusicPlaybackService$9;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/MusicPlaybackService$9;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1222
    return-void
.end method

.method public playNext(Lcom/google/android/music/medialist/SongList;I)V
    .locals 1
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "fromPos"    # I

    .prologue
    .line 1628
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/DevicePlayback;->playNext(Lcom/google/android/music/medialist/SongList;I)V

    .line 1629
    return-void
.end method

.method public position()J
    .locals 2

    .prologue
    .line 1703
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->position()J

    move-result-wide v0

    return-wide v0
.end method

.method public prev()V
    .locals 1

    .prologue
    .line 1544
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->prev()V

    .line 1545
    return-void
.end method

.method public refreshRadio()V
    .locals 1

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->refreshRadio()V

    .line 1889
    return-void
.end method

.method public registerMusicCastMediaRouterCallback(Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    .prologue
    .line 2396
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback;->registerMusicCastMediaRouterCallback(Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;)V

    .line 2397
    return-void
.end method

.method public seek(J)J
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 1719
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/DevicePlayback;->seek(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public setMediaRoute(ZLjava/lang/String;)V
    .locals 1
    .param p1, "localRoute"    # Z
    .param p2, "routeId"    # Ljava/lang/String;

    .prologue
    .line 2384
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaRoute(ZLjava/lang/String;)V

    .line 2385
    return-void
.end method

.method public setMediaRouteVolume(Ljava/lang/String;D)V
    .locals 2
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "volume"    # D

    .prologue
    .line 2388
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaRouteVolume(Ljava/lang/String;D)V

    .line 2389
    return-void
.end method

.method public setQueuePosition(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/DevicePlayback;->setQueuePosition(I)V

    .line 1617
    return-void
.end method

.method public setRating(I)V
    .locals 1
    .param p1, "rating"    # I

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/DevicePlayback;->setRating(I)V

    .line 1673
    return-void
.end method

.method public setRepeatMode(I)V
    .locals 1
    .param p1, "repeatmode"    # I

    .prologue
    .line 1581
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/DevicePlayback;->setRepeatMode(I)V

    .line 1582
    return-void
.end method

.method public setShuffleMode(I)V
    .locals 1
    .param p1, "shufflemode"    # I

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/DevicePlayback;->setShuffleMode(I)V

    .line 1574
    return-void
.end method

.method setUIVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1251
    iput-boolean p1, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mUIVisible:Z

    .line 1252
    if-eqz p1, :cond_0

    .line 1253
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setActive(Z)V

    .line 1255
    :cond_0
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isBuffering()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1256
    :cond_1
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/MusicPlaybackService$10;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/MusicPlaybackService$10;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1271
    :cond_2
    :goto_0
    return-void

    .line 1261
    :cond_3
    if-eqz p1, :cond_4

    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isHoneycombOrGreater()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1262
    invoke-virtual {p0, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->stopForegroundService(Z)V

    .line 1263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    goto :goto_0

    .line 1264
    :cond_4
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isBuffering()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1265
    :cond_5
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/MusicPlaybackService$11;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/MusicPlaybackService$11;-><init>(Lcom/google/android/music/playback/MusicPlaybackService;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public shuffleAll()V
    .locals 2

    .prologue
    .line 1228
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setActive(Z)V

    .line 1229
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->shuffleAll()V

    .line 1230
    return-void
.end method

.method public shuffleOnDevice()V
    .locals 2

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setActive(Z)V

    .line 1238
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->shuffleOnDevice()V

    .line 1239
    return-void
.end method

.method public shuffleSongs(Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1245
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setActive(Z)V

    .line 1246
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/MusicPlaybackService;->addToRecentAsync(Lcom/google/android/music/medialist/SongList;)V

    .line 1247
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/DevicePlayback;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 1248
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->stop()V

    .line 1281
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->clearNotificationCancels()V

    .line 1282
    invoke-direct {p0}, Lcom/google/android/music/playback/MusicPlaybackService;->sendNotificationCancelDelayed()V

    .line 1283
    return-void
.end method

.method public supportsRating()Z
    .locals 1

    .prologue
    .line 1664
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/DevicePlayback;->supportsRating()Z

    move-result v0

    return v0
.end method

.method public updateMediaRouteVolume(Ljava/lang/String;D)V
    .locals 2
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "deltaRatio"    # D

    .prologue
    .line 2392
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService;->mLocalPlayback:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/playback/LocalDevicePlayback;->updateMediaRouteVolume(Ljava/lang/String;D)V

    .line 2393
    return-void
.end method
