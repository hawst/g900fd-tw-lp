.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createSongList(Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

.field final synthetic val$item:Lcom/google/android/music/leanback/Item;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Lcom/google/android/music/leanback/Item;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 327
    new-instance v3, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v3}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 328
    .local v3, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v3, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->getTrackDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 329
    invoke-static {v3, p1}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->populateDocumentFromSongCursor(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 330
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 331
    .local v4, "position":I
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v0, v4, :cond_0

    const/4 v7, 0x1

    .line 332
    .local v7, "isLast":Z
    :goto_0
    new-instance v0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->val$item:Lcom/google/android/music/leanback/Item;

    move-object v1, p0

    move v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;ZI)V

    return-object v0

    .line 331
    .end local v7    # "isLast":Z
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method
