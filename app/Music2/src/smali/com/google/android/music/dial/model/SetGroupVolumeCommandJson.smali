.class public Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "SetGroupVolumeCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 28
    iget-object v0, p0, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "groupvolume:1"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 29
    iget-object v0, p0, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "setVolume"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 31
    new-instance v0, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;->mBody:Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;

    .line 32
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson;->mBody:Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/SetGroupVolumeCommandJson$SetGroupVolumeCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
