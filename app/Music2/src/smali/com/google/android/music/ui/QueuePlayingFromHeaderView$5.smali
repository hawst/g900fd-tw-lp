.class Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;
.super Ljava/lang/Object;
.source "QueuePlayingFromHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doArtistShuffleUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$request:Lcom/google/android/music/art/ContainerArtRequest;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/art/ContainerArtRequest;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iput-object p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    iput-object p3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 10

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    invoke-virtual {v0}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v9

    .line 421
    .local v9, "metajamId":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 422
    invoke-static {v9}, Lcom/google/android/music/store/MusicContent$Artists;->getNautilusArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 423
    .local v1, "uri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 425
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->val$appContext:Landroid/content/Context;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->ARTIST_SHUFFLE_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$800()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 427
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 428
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->mName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 438
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v8    # "c":Landroid/database/Cursor;
    :goto_0
    return-void

    .line 431
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v8    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 436
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    invoke-virtual {v0}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->mName:Ljava/lang/String;

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->mName:Ljava/lang/String;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$500(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$5;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    .line 444
    return-void
.end method
