.class abstract Lcom/google/android/music/quiz/QuizActivity;
.super Landroid/app/ListActivity;
.source "QuizActivity.java"


# instance fields
.field protected mDescription:Landroid/widget/TextView;

.field private mIsDestroyed:Z

.field protected mListView:Landroid/widget/ListView;

.field protected mLoadingProgress:Landroid/widget/ProgressBar;

.field protected mNextButton:Landroid/widget/Button;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mIsDestroyed:Z

    return-void
.end method

.method private initViews()V
    .locals 3

    .prologue
    .line 58
    const v0, 0x7f0400ca

    invoke-virtual {p0, v0}, Lcom/google/android/music/quiz/QuizActivity;->setContentView(I)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/music/quiz/QuizActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400cb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mDescription:Landroid/widget/TextView;

    .line 60
    invoke-virtual {p0}, Lcom/google/android/music/quiz/QuizActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mListView:Landroid/widget/ListView;

    .line 61
    iget-object v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/music/quiz/QuizActivity;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 62
    const v0, 0x7f0e0241

    invoke-virtual {p0, v0}, Lcom/google/android/music/quiz/QuizActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mNextButton:Landroid/widget/Button;

    .line 63
    iget-object v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mNextButton:Landroid/widget/Button;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 64
    const v0, 0x7f0e01d2

    invoke-virtual {p0, v0}, Lcom/google/android/music/quiz/QuizActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mLoadingProgress:Landroid/widget/ProgressBar;

    .line 65
    return-void
.end method


# virtual methods
.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mIsDestroyed:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/quiz/QuizActivity;->requestWindowFeature(I)Z

    .line 44
    invoke-direct {p0}, Lcom/google/android/music/quiz/QuizActivity;->initViews()V

    .line 45
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mIsDestroyed:Z

    .line 50
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 51
    return-void
.end method

.method protected setLoadingProgressVisibility(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/music/quiz/QuizActivity;->mLoadingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 84
    return-void
.end method

.method protected setPrioritizeRecommendations()V
    .locals 3

    .prologue
    .line 73
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 74
    .local v1, "refObject":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 76
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setPrioritizeRecommendationsOnMainstage(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 80
    return-void

    .line 78
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method
