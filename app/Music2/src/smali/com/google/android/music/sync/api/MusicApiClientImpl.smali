.class public Lcom/google/android/music/sync/api/MusicApiClientImpl;
.super Ljava/lang/Object;
.source "MusicApiClientImpl.java"

# interfaces
.implements Lcom/google/android/music/sync/api/MusicApiClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;,
        Lcom/google/android/music/sync/api/MusicApiClientImpl$DeleteRequestCreator;,
        Lcom/google/android/music/sync/api/MusicApiClientImpl$PutRequestCreator;,
        Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;,
        Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;,
        Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final sBlacklistItemInstance:Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

.field private static final sConfigEntryInstance:Lcom/google/android/music/sync/google/model/ConfigEntry;

.field private static final sPlaylistEntryInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

.field private static final sPlaylistInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

.field private static final sRadioStationInstance:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

.field private static final sTrackInstance:Lcom/google/android/music/sync/google/model/Track;


# instance fields
.field private final mAuthInfo:Lcom/google/android/music/sync/common/AuthInfo;

.field private final mContext:Landroid/content/Context;

.field private final mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

.field private final mLogHttp:Z

.field private mLoggingId:Ljava/lang/String;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    .line 106
    new-instance v0, Lcom/google/android/music/sync/google/model/Track;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/Track;-><init>()V

    sput-object v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sTrackInstance:Lcom/google/android/music/sync/google/model/Track;

    .line 107
    new-instance v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;-><init>()V

    sput-object v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sPlaylistInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 108
    new-instance v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;-><init>()V

    sput-object v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sPlaylistEntryInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .line 110
    new-instance v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;-><init>()V

    sput-object v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sRadioStationInstance:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    .line 111
    new-instance v0, Lcom/google/android/music/sync/google/model/ConfigEntry;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/ConfigEntry;-><init>()V

    sput-object v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sConfigEntryInstance:Lcom/google/android/music/sync/google/model/ConfigEntry;

    .line 112
    new-instance v0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;-><init>()V

    sput-object v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sBlacklistItemInstance:Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/sync/common/AuthInfo;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authInfo"    # Lcom/google/android/music/sync/common/AuthInfo;

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mLogHttp:Z

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mLoggingId:Ljava/lang/String;

    .line 191
    iput-object p1, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    .line 192
    iput-object p2, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mAuthInfo:Lcom/google/android/music/sync/common/AuthInfo;

    .line 193
    iget-object v0, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/music/download/DownloadUtils;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/music/GoogleHttpClientFactory;->createGoogleHttpClient(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    .line 195
    iget-object v1, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/android/music/utils/DebugUtils;->isAutoLogAll()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/music/cloudclient/MusicHttpClient;->enableCurlLogging(Ljava/lang/String;I)V

    .line 197
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mResolver:Landroid/content/ContentResolver;

    .line 198
    return-void

    .line 195
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private createAndExecuteMethod(Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    .locals 38
    .param p1, "creator"    # Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/ConflictException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 235
    const/16 v26, 0x0

    .line 236
    .local v26, "response":Lorg/apache/http/HttpResponse;
    const/16 v31, 0x1f4

    .line 237
    .local v31, "status":I
    const/16 v24, 0xa

    .line 238
    .local v24, "redirectsLeft":I
    const/4 v5, 0x1

    .line 240
    .local v5, "authRetriesLeft":I
    const-wide/16 v28, 0x0

    .line 242
    .local v28, "retryAfter":J
    const/16 v32, 0x0

    .line 243
    .local v32, "statusLine":Lorg/apache/http/StatusLine;
    :goto_0
    if-lez v24, :cond_d

    if-lez v5, :cond_d

    .line 244
    sget-boolean v34, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v34, :cond_0

    .line 245
    const-string v34, "MusicApiClient"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Requesting from URI: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;->createRequest(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v25

    .line 248
    .local v25, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static/range {v25 .. v25}, Landroid/net/http/AndroidHttpClient;->modifyRequestToAcceptGzipResponse(Lorg/apache/http/HttpRequest;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mAuthInfo:Lcom/google/android/music/sync/common/AuthInfo;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/google/android/music/sync/common/AuthInfo;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    .line 250
    .local v6, "authToken":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 251
    new-instance v34, Landroid/accounts/AuthenticatorException;

    const-string v35, "Null auth token.  Bailing."

    invoke-direct/range {v34 .. v35}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;)V

    throw v34

    .line 253
    :cond_1
    const-string v34, "Authorization"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "GoogleLogin auth="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v34, "X-Device-Logging-ID"

    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getLoggingId()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/16 v26, 0x0

    .line 258
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mHttpClient:Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/music/cloudclient/MusicHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v26

    .line 260
    if-nez v26, :cond_2

    .line 261
    invoke-static/range {v25 .. v26}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    .line 264
    :cond_2
    if-eqz v26, :cond_3

    .line 265
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v32

    .line 267
    :cond_3
    if-nez v32, :cond_5

    .line 268
    new-instance v34, Ljava/io/IOException;

    const-string v35, "StatusLine is null -- should not happen."

    invoke-direct/range {v34 .. v35}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v34

    .line 260
    :catchall_0
    move-exception v34

    if-nez v26, :cond_4

    .line 261
    invoke-static/range {v25 .. v26}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    :cond_4
    throw v34

    .line 270
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mLogHttp:Z

    move/from16 v34, v0

    if-eqz v34, :cond_6

    .line 271
    sget-object v34, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    .local v4, "arr$":[Lorg/apache/http/Header;
    array-length v0, v4

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v19, 0x0

    .local v19, "i$":I
    :goto_1
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_6

    aget-object v17, v4, v19

    .line 273
    .local v17, "h":Lorg/apache/http/Header;
    sget-object v34, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, ": "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 276
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v17    # "h":Lorg/apache/http/Header;
    .end local v19    # "i$":I
    .end local v21    # "len$":I
    :cond_6
    invoke-interface/range {v32 .. v32}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v31

    .line 277
    sget-boolean v34, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v34, :cond_7

    .line 278
    const-string v34, "MusicApiClient"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Response status="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_7
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    .line 282
    .local v13, "entity":Lorg/apache/http/HttpEntity;
    const/16 v34, 0xc8

    move/from16 v0, v31

    move/from16 v1, v34

    if-lt v0, v1, :cond_b

    const/16 v34, 0x12c

    move/from16 v0, v31

    move/from16 v1, v34

    if-ge v0, v1, :cond_b

    .line 283
    const-string v34, "ETag"

    move-object/from16 v0, v26

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v18

    .line 284
    .local v18, "header":Lorg/apache/http/Header;
    if-nez v18, :cond_8

    const/16 v23, 0x0

    .line 285
    .local v23, "newEtag":Ljava/lang/String;
    :goto_2
    const/16 v34, 0xcc

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_9

    .line 286
    new-instance v34, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 292
    :goto_3
    return-object v34

    .line 284
    .end local v23    # "newEtag":Ljava/lang/String;
    :cond_8
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v23

    goto :goto_2

    .line 288
    .restart local v23    # "newEtag":Ljava/lang/String;
    :cond_9
    invoke-static {v13}, Landroid/net/http/AndroidHttpClient;->getUngzippedContent(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v20

    .line 289
    .local v20, "in":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mLogHttp:Z

    move/from16 v34, v0

    if-eqz v34, :cond_a

    .line 290
    invoke-static/range {v20 .. v20}, Lcom/google/android/music/cloudclient/MusicRequest;->logInputStreamContents(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v20

    .line 292
    :cond_a
    new-instance v34, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;

    move-object/from16 v0, v34

    move-object/from16 v1, v20

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_3

    .line 296
    .end local v18    # "header":Lorg/apache/http/Header;
    .end local v20    # "in":Ljava/io/InputStream;
    .end local v23    # "newEtag":Ljava/lang/String;
    :cond_b
    const/16 v34, 0x12e

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_13

    .line 298
    if-eqz v13, :cond_c

    .line 299
    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 301
    :cond_c
    const-string v34, "Location"

    move-object/from16 v0, v26

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v22

    .line 302
    .local v22, "location":Lorg/apache/http/Header;
    if-nez v22, :cond_11

    .line 303
    const-string v34, "MusicApiClient"

    const/16 v35, 0x3

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v34

    if-eqz v34, :cond_d

    .line 304
    const-string v34, "MusicApiClient"

    const-string v35, "Redirect requested but no Location specified."

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    .end local v6    # "authToken":Ljava/lang/String;
    .end local v13    # "entity":Lorg/apache/http/HttpEntity;
    .end local v22    # "location":Lorg/apache/http/Header;
    .end local v25    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_d
    sget-boolean v34, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v34, :cond_e

    .line 363
    const-string v34, "MusicApiClient"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Received "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " status code."

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_e
    const/4 v14, 0x0

    .line 366
    .local v14, "errorMessage":Ljava/lang/String;
    const/4 v13, 0x0

    .line 368
    .restart local v13    # "entity":Lorg/apache/http/HttpEntity;
    if-eqz v26, :cond_f

    .line 369
    :try_start_1
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    .line 371
    :cond_f
    if-eqz v13, :cond_1e

    .line 372
    invoke-static {v13}, Landroid/net/http/AndroidHttpClient;->getUngzippedContent(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v20

    .line 373
    .restart local v20    # "in":Ljava/io/InputStream;
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 374
    .local v7, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v34, 0x2000

    move/from16 v0, v34

    new-array v8, v0, [B

    .line 375
    .local v8, "buf":[B
    const/4 v9, -0x1

    .line 376
    .local v9, "bytesRead":I
    :goto_4
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/io/InputStream;->read([B)I

    move-result v9

    const/16 v34, -0x1

    move/from16 v0, v34

    if-eq v9, v0, :cond_1c

    .line 377
    const/16 v34, 0x0

    move/from16 v0, v34

    invoke-virtual {v7, v8, v0, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    .line 386
    .end local v7    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "buf":[B
    .end local v9    # "bytesRead":I
    .end local v20    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v34

    :goto_5
    if-eqz v13, :cond_10

    .line 387
    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_10
    throw v34

    .line 309
    .end local v14    # "errorMessage":Ljava/lang/String;
    .restart local v6    # "authToken":Ljava/lang/String;
    .restart local v22    # "location":Lorg/apache/http/Header;
    .restart local v25    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_11
    const-string v34, "MusicApiClient"

    const/16 v35, 0x3

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v34

    if-eqz v34, :cond_12

    .line 310
    const-string v34, "MusicApiClient"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Following redirect to "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_12
    add-int/lit8 v24, v24, -0x1

    .line 313
    goto/16 :goto_0

    .end local v22    # "location":Lorg/apache/http/Header;
    :cond_13
    const/16 v34, 0x130

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_14

    .line 314
    new-instance v34, Lcom/google/android/music/sync/api/NotModifiedException;

    const-string v35, "No content."

    invoke-direct/range {v34 .. v35}, Lcom/google/android/music/sync/api/NotModifiedException;-><init>(Ljava/lang/String;)V

    throw v34

    .line 315
    :cond_14
    const/16 v34, 0x190

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_15

    .line 316
    new-instance v34, Lcom/google/android/music/sync/api/BadRequestException;

    const-string v35, "Bad request"

    invoke-direct/range {v34 .. v35}, Lcom/google/android/music/sync/api/BadRequestException;-><init>(Ljava/lang/String;)V

    throw v34

    .line 317
    :cond_15
    const/16 v34, 0x191

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_17

    .line 318
    if-eqz v13, :cond_16

    .line 319
    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 321
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mAuthInfo:Lcom/google/android/music/sync/common/AuthInfo;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, p2

    invoke-interface {v0, v1, v6}, Lcom/google/android/music/sync/common/AuthInfo;->invalidateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 322
    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_0

    .line 323
    :cond_17
    const/16 v34, 0x193

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_18

    .line 324
    new-instance v34, Lcom/google/android/music/sync/api/ForbiddenException;

    const-string v35, "Forbidden"

    invoke-direct/range {v34 .. v35}, Lcom/google/android/music/sync/api/ForbiddenException;-><init>(Ljava/lang/String;)V

    throw v34

    .line 325
    :cond_18
    const/16 v34, 0x194

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_19

    .line 326
    new-instance v34, Lcom/google/android/music/sync/api/ResourceNotFoundException;

    const-string v35, "Not found"

    invoke-direct/range {v34 .. v35}, Lcom/google/android/music/sync/api/ResourceNotFoundException;-><init>(Ljava/lang/String;)V

    throw v34

    .line 327
    :cond_19
    const/16 v34, 0x199

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_1a

    .line 328
    new-instance v10, Lcom/google/android/music/sync/api/ConflictException;

    const-string v34, "Conflict detected."

    move-object/from16 v0, v34

    invoke-direct {v10, v0}, Lcom/google/android/music/sync/api/ConflictException;-><init>(Ljava/lang/String;)V

    .line 329
    .local v10, "ce":Lcom/google/android/music/sync/api/ConflictException;
    const/16 v34, 0x1

    move/from16 v0, v34

    invoke-virtual {v10, v0}, Lcom/google/android/music/sync/api/ConflictException;->setConflictCount(I)V

    .line 330
    throw v10

    .line 331
    .end local v10    # "ce":Lcom/google/android/music/sync/api/ConflictException;
    :cond_1a
    const/16 v34, 0x1f7

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_d

    .line 332
    const-string v34, "Retry-After"

    move-object/from16 v0, v26

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v27

    .line 333
    .local v27, "retryAfterHeader":Lorg/apache/http/Header;
    if-eqz v27, :cond_1b

    .line 337
    invoke-interface/range {v27 .. v27}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v30

    .line 341
    .local v30, "retryAfterString":Ljava/lang/String;
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v34

    const-wide/16 v36, 0x3e8

    div-long v34, v34, v36

    invoke-static/range {v30 .. v30}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-wide v36

    add-long v28, v34, v36

    .line 354
    .end local v30    # "retryAfterString":Ljava/lang/String;
    :cond_1b
    :goto_6
    new-instance v11, Lcom/google/android/music/sync/api/ServiceUnavailableException;

    const-string v34, "Service unavailable."

    move-object/from16 v0, v34

    invoke-direct {v11, v0}, Lcom/google/android/music/sync/api/ServiceUnavailableException;-><init>(Ljava/lang/String;)V

    .line 356
    .local v11, "e":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    move-wide/from16 v0, v28

    invoke-virtual {v11, v0, v1}, Lcom/google/android/music/sync/api/ServiceUnavailableException;->setRetryAfter(J)V

    .line 357
    throw v11

    .line 343
    .end local v11    # "e":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    .restart local v30    # "retryAfterString":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 346
    .local v11, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    new-instance v33, Landroid/text/format/Time;

    invoke-direct/range {v33 .. v33}, Landroid/text/format/Time;-><init>()V

    .line 347
    .local v33, "time":Landroid/text/format/Time;
    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 348
    const/16 v34, 0x0

    invoke-virtual/range {v33 .. v34}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v34

    const-wide/16 v36, 0x3e8

    div-long v28, v34, v36
    :try_end_3
    .catch Landroid/util/TimeFormatException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_6

    .line 349
    .end local v33    # "time":Landroid/text/format/Time;
    :catch_1
    move-exception v12

    .line 350
    .local v12, "e2":Landroid/util/TimeFormatException;
    const-string v34, "MusicApiClient"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Unable to parse "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-static {v0, v1, v12}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 380
    .end local v6    # "authToken":Ljava/lang/String;
    .end local v11    # "e":Ljava/lang/NumberFormatException;
    .end local v12    # "e2":Landroid/util/TimeFormatException;
    .end local v25    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v27    # "retryAfterHeader":Lorg/apache/http/Header;
    .end local v30    # "retryAfterString":Ljava/lang/String;
    .restart local v7    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v8    # "buf":[B
    .restart local v9    # "bytesRead":I
    .restart local v14    # "errorMessage":Ljava/lang/String;
    .restart local v20    # "in":Ljava/io/InputStream;
    :cond_1c
    :try_start_4
    new-instance v15, Ljava/lang/String;

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v34

    move-object/from16 v0, v34

    invoke-direct {v15, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 381
    .end local v14    # "errorMessage":Ljava/lang/String;
    .local v15, "errorMessage":Ljava/lang/String;
    :try_start_5
    sget-boolean v34, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v34, :cond_1d

    .line 382
    const-string v34, "MusicApiClient"

    move-object/from16 v0, v34

    invoke-static {v0, v15}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_1d
    move-object v14, v15

    .line 386
    .end local v7    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "buf":[B
    .end local v9    # "bytesRead":I
    .end local v15    # "errorMessage":Ljava/lang/String;
    .end local v20    # "in":Ljava/io/InputStream;
    .restart local v14    # "errorMessage":Ljava/lang/String;
    :cond_1e
    if-eqz v13, :cond_1f

    .line 387
    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 390
    :cond_1f
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Received "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " status code"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 391
    .local v16, "exceptionMessage":Ljava/lang/String;
    if-eqz v14, :cond_20

    .line 392
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, ": "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 394
    :cond_20
    const/16 v34, 0x191

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_21

    .line 395
    new-instance v34, Landroid/accounts/AuthenticatorException;

    const-string v35, "Auth token not excepted.  Bailing."

    invoke-direct/range {v34 .. v35}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/String;)V

    throw v34

    .line 397
    :cond_21
    new-instance v11, Lcom/google/android/music/sync/common/SyncHttpException;

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Http request returned an error code: "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v31

    invoke-direct {v11, v0, v1}, Lcom/google/android/music/sync/common/SyncHttpException;-><init>(Ljava/lang/String;I)V

    .line 399
    .local v11, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    move-wide/from16 v0, v28

    invoke-virtual {v11, v0, v1}, Lcom/google/android/music/sync/common/SyncHttpException;->setRetryAfter(J)V

    .line 400
    throw v11

    .line 386
    .end local v11    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    .end local v14    # "errorMessage":Ljava/lang/String;
    .end local v16    # "exceptionMessage":Ljava/lang/String;
    .restart local v7    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v8    # "buf":[B
    .restart local v9    # "bytesRead":I
    .restart local v15    # "errorMessage":Ljava/lang/String;
    .restart local v20    # "in":Ljava/io/InputStream;
    :catchall_2
    move-exception v34

    move-object v14, v15

    .end local v15    # "errorMessage":Ljava/lang/String;
    .restart local v14    # "errorMessage":Ljava/lang/String;
    goto/16 :goto_5
.end method

.method private getItem(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;
        }
    .end annotation

    .prologue
    .line 847
    .local p3, "itemInstance":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    iget-object v5, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    invoke-interface {p3, v5, p2}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v4

    .line 848
    .local v4, "url":Lcom/google/android/music/sync/api/MusicUrl;
    new-instance v2, Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;

    invoke-direct {v2}, Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;-><init>()V

    .line 849
    .local v2, "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    const/4 v1, 0x0

    .line 851
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v2, p1, v5}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->createAndExecuteMethod(Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;

    move-result-object v3

    .line 853
    .local v3, "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    iget-object v1, v3, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;->mInputStream:Ljava/io/InputStream;

    .line 854
    if-nez v1, :cond_0

    .line 855
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Received an empty entity body in a getEntity request."

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 858
    .end local v3    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :catch_0
    move-exception v0

    .line 859
    .local v0, "e":Lcom/google/android/music/store/InvalidDataException;
    :try_start_1
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid data on fetch from server: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/music/store/InvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 867
    .end local v0    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catchall_0
    move-exception v5

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v5

    .line 857
    .restart local v3    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :cond_0
    :try_start_2
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonInputStream(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    :try_end_2
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 867
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    return-object v5

    .line 860
    .end local v3    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :catch_1
    move-exception v0

    .line 861
    .local v0, "e":Lcom/google/android/music/sync/api/ConflictException;
    :try_start_3
    new-instance v5, Lcom/google/android/music/sync/common/SyncHttpException;

    const-string v6, "Conflict detected, but this should not happen for a get."

    const/16 v7, 0x199

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/sync/common/SyncHttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v5

    .line 863
    .end local v0    # "e":Lcom/google/android/music/sync/api/ConflictException;
    :catch_2
    move-exception v0

    .line 864
    .local v0, "e":Lcom/google/android/music/sync/api/NotModifiedException;
    new-instance v5, Lcom/google/android/music/sync/common/SyncHttpException;

    const-string v6, "Not-modified status, but this should not happen for a get."

    const/16 v7, 0x130

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/sync/common/SyncHttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private getLoggingId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mLoggingId:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 219
    .local v0, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mLoggingId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 224
    .end local v0    # "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mLoggingId:Ljava/lang/String;

    return-object v1

    .line 221
    .restart local v0    # "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :catchall_0
    move-exception v1

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v1
.end method

.method private handleBlacklistItemsNewList(Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;Landroid/accounts/Account;Ljava/util/HashMap;)V
    .locals 12
    .param p1, "feed"    # Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;",
            "Landroid/accounts/Account;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 965
    .local p3, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p1}, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;->getItemList()Ljava/util/List;

    move-result-object v4

    .line 967
    .local v4, "syncableBlacklistItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 968
    .local v1, "blacklistItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/store/BlacklistItem;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    .line 969
    .local v3, "syncableBlacklistItem":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    new-instance v0, Lcom/google/android/music/store/BlacklistItem;

    invoke-direct {v0}, Lcom/google/android/music/store/BlacklistItem;-><init>()V

    .line 970
    .local v0, "blacklistItem":Lcom/google/android/music/store/BlacklistItem;
    invoke-virtual {v0}, Lcom/google/android/music/store/BlacklistItem;->reset()V

    .line 971
    invoke-virtual {v3, v0}, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->formatAsBlacklistItem(Lcom/google/android/music/store/BlacklistItem;)Lcom/google/android/music/store/BlacklistItem;

    .line 972
    invoke-static {p2}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/music/store/BlacklistItem;->setSourceAccount(I)V

    .line 973
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/music/store/BlacklistItem;->setNeedsSync(Z)V

    .line 974
    sget-boolean v5, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v5, :cond_0

    .line 975
    const-string v5, "MusicApiClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Inserting blacklist item "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " at timestamp "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, v3, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 982
    .end local v0    # "blacklistItem":Lcom/google/android/music/store/BlacklistItem;
    .end local v3    # "syncableBlacklistItem":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    invoke-static {p2, v5, v1}, Lcom/google/android/music/store/BlacklistItem;->replaceStaleItems(Landroid/accounts/Account;Lcom/google/android/music/store/Store;Ljava/util/List;)J

    move-result-wide v6

    .line 984
    .local v6, "updateMin":J
    const-class v5, Ljava/lang/Long;

    const-string v8, "merger_blacklist_item_version"

    invoke-virtual {p3, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v5, v8, v6

    if-gez v5, :cond_2

    .line 987
    const-string v5, "merger_blacklist_item_version"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p3, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 990
    :cond_2
    return-void
.end method

.method private makeUpdateRequestCreator(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">(TT;)",
            "Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 475
    .local p1, "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->validateForUpstreamUpdate()V

    .line 476
    const/4 v1, 0x0

    .line 477
    .local v1, "entryBytes":[B
    instance-of v5, p1, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v5, :cond_0

    .line 478
    const-class v5, Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {v5, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/Track;

    .line 480
    .local v3, "track":Lcom/google/android/music/sync/google/model/Track;
    invoke-static {v3}, Lcom/google/android/music/sync/google/model/Track;->filterForRatingsUpdate(Lcom/google/android/music/sync/google/model/Track;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v4

    .line 481
    .local v4, "trackMutation":Lcom/google/android/music/sync/google/model/Track;
    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/Track;->serializeAsJson()[B

    move-result-object v1

    .line 487
    .end local v3    # "track":Lcom/google/android/music/sync/google/model/Track;
    .end local v4    # "trackMutation":Lcom/google/android/music/sync/google/model/Track;
    :goto_0
    iget-object v5, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v5}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v0

    .line 489
    .local v0, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    new-instance v2, Lcom/google/android/music/sync/api/MusicApiClientImpl$PutRequestCreator;

    invoke-direct {v2, v0}, Lcom/google/android/music/sync/api/MusicApiClientImpl$PutRequestCreator;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 490
    .local v2, "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    return-object v2

    .line 484
    .end local v0    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .end local v2    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    :cond_0
    invoke-interface {p1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->serializeAsJson()[B

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public getBlacklistItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pageSize"    # I
    .param p3, "etag"    # Ljava/lang/String;
    .param p4, "updatedMin"    # J
    .param p6, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 828
    .local p7, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v7, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sBlacklistItemInstance:Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v10, p7

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;ZZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v0

    return-object v0
.end method

.method public getConfig(Landroid/accounts/Account;ZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    .locals 11
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "refresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<",
            "Lcom/google/android/music/sync/google/model/ConfigEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/NotModifiedException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;
        }
    .end annotation

    .prologue
    .local p3, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 838
    const-wide/16 v4, 0x0

    sget-object v7, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sConfigEntryInstance:Lcom/google/android/music/sync/google/model/ConfigEntry;

    move-object v0, p0

    move-object v1, p1

    move-object v6, v3

    move v8, v2

    move v9, p2

    move-object v10, p3

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;ZZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v0

    return-object v0
.end method

.method public getItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;ZZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    .locals 24
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pageSize"    # I
    .param p3, "etag"    # Ljava/lang/String;
    .param p4, "updatedMin"    # J
    .param p6, "continuationToken"    # Ljava/lang/String;
    .param p8, "supportsPost"    # Z
    .param p9, "refresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "TT;ZZ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/store/InvalidDataException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 654
    .local p7, "itemInstance":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    .local p10, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const/16 v19, 0x0

    .line 655
    .local v19, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const/4 v15, 0x0

    .line 656
    .local v15, "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    if-eqz p8, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    const-string v22, "music_use_downstream_posts"

    const/16 v23, 0x1

    invoke-static/range {v21 .. v23}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v21

    if-eqz v21, :cond_4

    const/16 v20, 0x1

    .line 659
    .local v20, "useDownstreamPosts":Z
    :goto_0
    if-eqz v20, :cond_6

    .line 660
    new-instance v7, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;

    invoke-direct {v7}, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;-><init>()V

    .line 661
    .local v7, "feedAsPostRequest":Lcom/google/android/music/sync/google/model/FeedAsPostRequest;
    if-eqz p6, :cond_5

    .line 662
    move-object/from16 v0, p6

    iput-object v0, v7, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;->mStartToken:Ljava/lang/String;

    .line 668
    :goto_1
    move/from16 v0, p2

    iput v0, v7, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;->mMaxResults:I

    .line 670
    invoke-virtual {v7}, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;->serializeAsJson()[B

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v22}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v5

    .line 672
    .local v5, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    new-instance v15, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;

    .end local v15    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    invoke-direct {v15, v5}, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 674
    .restart local v15    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p7

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v19

    .line 691
    .end local v5    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .end local v7    # "feedAsPostRequest":Lcom/google/android/music/sync/google/model/FeedAsPostRequest;
    :goto_2
    const-wide/16 v22, 0x0

    cmp-long v21, p4, v22

    if-eqz v21, :cond_0

    .line 692
    const-string v21, "updated-min"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 695
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    const-string v22, "music_sync_server_options"

    const/16 v23, 0x0

    invoke-static/range {v21 .. v23}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 697
    .local v17, "serverOptions":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_1

    .line 698
    const-string v21, "server-opts"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 701
    :cond_1
    if-eqz p9, :cond_2

    .line 702
    const-string v21, "refresh"

    const-string v22, "1"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 705
    :cond_2
    new-instance v14, Ljava/lang/Object;

    invoke-direct {v14}, Ljava/lang/Object;-><init>()V

    .line 706
    .local v14, "refObject":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v0, v14}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v13

    .line 708
    .local v13, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/16 v18, 0x0

    .line 709
    .local v18, "tier":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v13}, Lcom/google/android/music/preferences/MusicPreferences;->isValidAccount()Z

    move-result v21

    if-nez v21, :cond_8

    .line 710
    const-string v18, "none"

    .line 717
    :goto_3
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_3

    .line 718
    const-string v21, "tier"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 721
    :cond_3
    invoke-static {v14}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 724
    const/4 v9, 0x0

    .line 726
    .local v9, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    invoke-direct {v0, v15, v1, v2}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->createAndExecuteMethod(Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;

    move-result-object v16

    .line 728
    .local v16, "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;->mInputStream:Ljava/io/InputStream;

    .line 729
    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;->mEtag:Ljava/lang/String;

    .line 730
    .local v11, "newEtag":Ljava/lang/String;
    if-nez v9, :cond_a

    .line 731
    new-instance v21, Ljava/io/IOException;

    const-string v22, "Received an empty entity body in a getEntity request."

    invoke-direct/range {v21 .. v22}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v21
    :try_end_1
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 768
    .end local v11    # "newEtag":Ljava/lang/String;
    .end local v16    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :catch_0
    move-exception v4

    .line 769
    .local v4, "e":Lcom/google/android/music/store/InvalidDataException;
    :try_start_2
    new-instance v21, Ljava/io/IOException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Invalid data on fetch from server: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v4}, Lcom/google/android/music/store/InvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v21
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 774
    .end local v4    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catchall_0
    move-exception v21

    invoke-static {v9}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v21

    .line 656
    .end local v9    # "in":Ljava/io/InputStream;
    .end local v13    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v14    # "refObject":Ljava/lang/Object;
    .end local v17    # "serverOptions":Ljava/lang/String;
    .end local v18    # "tier":Ljava/lang/String;
    .end local v20    # "useDownstreamPosts":Z
    :cond_4
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 666
    .restart local v7    # "feedAsPostRequest":Lcom/google/android/music/sync/google/model/FeedAsPostRequest;
    .restart local v20    # "useDownstreamPosts":Z
    :cond_5
    const-string v21, "0"

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/google/android/music/sync/google/model/FeedAsPostRequest;->mStartToken:Ljava/lang/String;

    goto/16 :goto_1

    .line 676
    .end local v7    # "feedAsPostRequest":Lcom/google/android/music/sync/google/model/FeedAsPostRequest;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p7

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v19

    .line 677
    new-instance v8, Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;

    invoke-direct {v8}, Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;-><init>()V

    .line 678
    .local v8, "getRequestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;
    move-object v15, v8

    .line 679
    move-object/from16 v0, p3

    # setter for: Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;->mEtag:Ljava/lang/String;
    invoke-static {v8, v0}, Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;->access$002(Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;Ljava/lang/String;)Ljava/lang/String;

    .line 680
    if-eqz p6, :cond_7

    .line 681
    const-string v21, "start-token"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, p6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 687
    :goto_4
    const-string v21, "max-results"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 685
    :cond_7
    const-string v21, "start-token"

    const-string v22, "0"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/sync/api/MusicUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_4

    .line 711
    .end local v8    # "getRequestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$GetRequestCreator;
    .restart local v13    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v14    # "refObject":Ljava/lang/Object;
    .restart local v17    # "serverOptions":Ljava/lang/String;
    .restart local v18    # "tier":Ljava/lang/String;
    :cond_8
    :try_start_3
    invoke-virtual {v13}, Lcom/google/android/music/preferences/MusicPreferences;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/NautilusStatus;->isNautilusEnabled()Z

    move-result v21

    if-eqz v21, :cond_9

    .line 712
    const-string v18, "aa"

    goto/16 :goto_3

    .line 714
    :cond_9
    const-string v18, "basic"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_3

    .line 721
    :catchall_1
    move-exception v21

    invoke-static {v14}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v21

    .line 733
    .restart local v9    # "in":Ljava/io/InputStream;
    .restart local v11    # "newEtag":Ljava/lang/String;
    .restart local v16    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :cond_a
    const/4 v6, 0x0

    .line 735
    .local v6, "feed":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;, "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed<TT;>;"
    :try_start_4
    move-object/from16 v0, p7

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/Track;

    move/from16 v21, v0

    if-eqz v21, :cond_d

    .line 736
    invoke-static {v9}, Lcom/google/android/music/sync/google/model/TrackFeed;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/TrackFeed;

    move-result-object v6

    .line 754
    :cond_b
    :goto_5
    const/4 v10, 0x0

    .line 755
    .local v10, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v12, 0x0

    .line 756
    .local v12, "nextPageToken":Ljava/lang/String;
    if-eqz v6, :cond_c

    .line 757
    invoke-interface {v6}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;->getItemList()Ljava/util/List;

    move-result-object v21

    if-nez v21, :cond_12

    .line 758
    const-string v21, "MusicApiClient"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "No content in \'data\' field in GET sync response for "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    :goto_6
    invoke-interface {v6}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;->getNextPageToken()Ljava/lang/String;

    move-result-object v12

    .line 765
    :cond_c
    new-instance v22, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    if-nez v10, :cond_13

    sget-object v21, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_7
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v12, v11}, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;-><init>(Ljava/util/Iterator;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 774
    invoke-static {v9}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    return-object v22

    .line 737
    .end local v10    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .end local v12    # "nextPageToken":Ljava/lang/String;
    :cond_d
    :try_start_5
    move-object/from16 v0, p7

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move/from16 v21, v0

    if-eqz v21, :cond_e

    .line 738
    invoke-static {v9}, Lcom/google/android/music/sync/google/model/SyncablePlaylistFeed;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/SyncablePlaylistFeed;

    move-result-object v6

    goto :goto_5

    .line 739
    :cond_e
    move-object/from16 v0, p7

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 740
    invoke-static {v9}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntryFeed;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntryFeed;

    move-result-object v6

    goto :goto_5

    .line 741
    :cond_f
    move-object/from16 v0, p7

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move/from16 v21, v0

    if-eqz v21, :cond_10

    .line 742
    invoke-static {v9}, Lcom/google/android/music/sync/google/model/RadioGetStationsResponse;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/RadioGetStationsResponse;

    move-result-object v6

    goto :goto_5

    .line 743
    :cond_10
    move-object/from16 v0, p7

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/ConfigEntry;

    move/from16 v21, v0

    if-eqz v21, :cond_11

    .line 744
    invoke-static {v9}, Lcom/google/android/music/sync/google/model/ConfigEntryFeed;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/ConfigEntryFeed;

    move-result-object v6

    goto :goto_5

    .line 745
    :cond_11
    move-object/from16 v0, p7

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    move/from16 v21, v0

    if-eqz v21, :cond_b

    .line 746
    invoke-static {v9}, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;

    move-result-object v6

    .line 747
    move-object v0, v6

    check-cast v0, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;->mMinLastModifiedIgnore:Z

    move/from16 v21, v0

    if-eqz v21, :cond_b

    .line 748
    check-cast v6, Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;

    .end local v6    # "feed":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;, "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed<TT;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p10

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->handleBlacklistItemsNewList(Lcom/google/android/music/cloudclient/GetBlacklistItemsResponseJson;Landroid/accounts/Account;Ljava/util/HashMap;)V

    .line 751
    const/4 v6, 0x0

    .restart local v6    # "feed":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;, "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed<TT;>;"
    goto/16 :goto_5

    .line 761
    .restart local v10    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .restart local v12    # "nextPageToken":Ljava/lang/String;
    :cond_12
    invoke-interface {v6}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;->getItemList()Ljava/util/List;

    move-result-object v10

    goto :goto_6

    .line 765
    :cond_13
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_5
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v21

    goto :goto_7

    .line 770
    .end local v6    # "feed":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed;, "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity$Feed<TT;>;"
    .end local v10    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .end local v11    # "newEtag":Ljava/lang/String;
    .end local v12    # "nextPageToken":Ljava/lang/String;
    .end local v16    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :catch_1
    move-exception v4

    .line 771
    .local v4, "e":Lcom/google/android/music/sync/api/ConflictException;
    :try_start_6
    new-instance v21, Lcom/google/android/music/sync/common/SyncHttpException;

    const-string v22, "Conflict detected, but this should not happen for a get."

    const/16 v23, 0x199

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/music/sync/common/SyncHttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v21
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public getPlaylist(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;
        }
    .end annotation

    .prologue
    .line 882
    const-class v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    sget-object v1, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sPlaylistInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItem(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    return-object v0
.end method

.method public getPlaylistEntries(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pageSize"    # I
    .param p3, "etag"    # Ljava/lang/String;
    .param p4, "updatedMin"    # J
    .param p6, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 806
    .local p7, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v7, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sPlaylistEntryInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v10, p7

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;ZZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v0

    return-object v0
.end method

.method public getPlaylistEntry(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;
        }
    .end annotation

    .prologue
    .line 889
    const-class v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    sget-object v1, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sPlaylistEntryInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItem(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    return-object v0
.end method

.method public getPlaylists(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pageSize"    # I
    .param p3, "etag"    # Ljava/lang/String;
    .param p4, "updatedMin"    # J
    .param p6, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylist;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 795
    .local p7, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v7, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sPlaylistInstance:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v10, p7

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;ZZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v0

    return-object v0
.end method

.method public getRadioStations(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pageSize"    # I
    .param p3, "etag"    # Ljava/lang/String;
    .param p4, "updatedMin"    # J
    .param p6, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncableRadioStation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 817
    .local p7, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v7, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sRadioStationInstance:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v10, p7

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;ZZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v0

    return-object v0
.end method

.method public getTrack(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;
        }
    .end annotation

    .prologue
    .line 875
    const-class v0, Lcom/google/android/music/sync/google/model/Track;

    sget-object v1, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sTrackInstance:Lcom/google/android/music/sync/google/model/Track;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItem(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/Track;

    return-object v0
.end method

.method public getTracks(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pageSize"    # I
    .param p3, "etag"    # Ljava/lang/String;
    .param p4, "updatedMin"    # J
    .param p6, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 784
    .local p7, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v7, Lcom/google/android/music/sync/api/MusicApiClientImpl;->sTrackInstance:Lcom/google/android/music/sync/google/model/Track;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v10, p7

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->getItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;ZZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v0

    return-object v0
.end method

.method public mutateItem(Landroid/accounts/Account;Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;Lcom/google/android/music/sync/api/MusicApiClient$OpType;)V
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;
    .param p3, "opType"    # Lcom/google/android/music/sync/api/MusicApiClient$OpType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">(",
            "Landroid/accounts/Account;",
            "TT;",
            "Lcom/google/android/music/sync/api/MusicApiClient$OpType;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/store/InvalidDataException;,
            Lcom/google/android/music/sync/api/ConflictException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;
        }
    .end annotation

    .prologue
    .line 408
    .local p2, "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    if-nez p2, :cond_0

    .line 409
    new-instance v11, Lcom/google/android/music/store/InvalidDataException;

    const-string v12, "Trying to upsync a null entity in a mutation."

    invoke-direct {v11, v12}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 411
    :cond_0
    const/4 v10, 0x0

    .line 413
    .local v10, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const/4 v7, 0x0

    .line 414
    .local v7, "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    sget-object v11, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->INSERT:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    move-object/from16 v0, p3

    if-ne v0, v11, :cond_2

    .line 415
    invoke-interface/range {p2 .. p2}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->validateForUpstreamInsert()V

    .line 416
    iget-object v11, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v10

    .line 417
    invoke-interface/range {p2 .. p2}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->serializeAsJson()[B

    move-result-object v3

    .line 418
    .local v3, "entryBytes":[B
    iget-object v11, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v3, v11}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v2

    .line 420
    .local v2, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    new-instance v7, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;

    .end local v7    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    invoke-direct {v7, v2}, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 444
    .end local v2    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .end local v3    # "entryBytes":[B
    .restart local v7    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    :cond_1
    :goto_0
    if-nez v10, :cond_5

    .line 445
    new-instance v11, Ljava/lang/IllegalStateException;

    const-string v12, "Missing url"

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 421
    :cond_2
    sget-object v11, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->UPDATE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    move-object/from16 v0, p3

    if-ne v0, v11, :cond_4

    .line 422
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->makeUpdateRequestCreator(Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;)Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;

    move-result-object v7

    .line 423
    invoke-interface/range {p2 .. p2}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v6

    .line 424
    .local v6, "remoteId":Ljava/lang/String;
    move-object/from16 v0, p2

    instance-of v11, v0, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v11, :cond_3

    move-object/from16 v9, p2

    .line 432
    check-cast v9, Lcom/google/android/music/sync/google/model/Track;

    .line 433
    .local v9, "track":Lcom/google/android/music/sync/google/model/Track;
    invoke-virtual {v9}, Lcom/google/android/music/sync/google/model/Track;->hasLockerId()Z

    move-result v11

    if-nez v11, :cond_3

    .line 434
    invoke-virtual {v9}, Lcom/google/android/music/sync/google/model/Track;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v6

    .line 437
    .end local v9    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_3
    iget-object v11, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v6}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v10

    .line 438
    goto :goto_0

    .end local v6    # "remoteId":Ljava/lang/String;
    :cond_4
    sget-object v11, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->DELETE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    move-object/from16 v0, p3

    if-ne v0, v11, :cond_1

    .line 439
    invoke-interface/range {p2 .. p2}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->validateForUpstreamDelete()V

    .line 440
    iget-object v11, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v12}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v10

    .line 441
    new-instance v7, Lcom/google/android/music/sync/api/MusicApiClientImpl$DeleteRequestCreator;

    .end local v7    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    invoke-direct {v7}, Lcom/google/android/music/sync/api/MusicApiClientImpl$DeleteRequestCreator;-><init>()V

    .restart local v7    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    goto :goto_0

    .line 449
    :cond_5
    :try_start_0
    invoke-virtual {v10}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v7, p1, v11}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->createAndExecuteMethod(Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :try_end_0
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 454
    .local v8, "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    iget-object v4, v8, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;->mInputStream:Ljava/io/InputStream;

    .line 457
    .local v4, "in":Ljava/io/InputStream;
    :try_start_1
    sget-object v11, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->DELETE:Lcom/google/android/music/sync/api/MusicApiClient$OpType;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v0, p3

    if-ne v0, v11, :cond_6

    .line 468
    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 470
    :goto_1
    return-void

    .line 450
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v8    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :catch_0
    move-exception v1

    .line 451
    .local v1, "e":Lcom/google/android/music/sync/api/NotModifiedException;
    new-instance v11, Lcom/google/android/music/sync/common/SyncHttpException;

    const-string v12, "Unexpected not-modified status returned on a mutation"

    const/16 v13, 0x130

    invoke-direct {v11, v12, v1, v13}, Lcom/google/android/music/sync/common/SyncHttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v11

    .line 462
    .end local v1    # "e":Lcom/google/android/music/sync/api/NotModifiedException;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v8    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :cond_6
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-static {v11, v4}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonInputStream(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 464
    .local v5, "mutatedEntity":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    sget-object v11, Lcom/google/android/music/sync/api/MusicApiClient$OpType;->INSERT:Lcom/google/android/music/sync/api/MusicApiClient$OpType;

    move-object/from16 v0, p3

    if-ne v0, v11, :cond_7

    .line 465
    invoke-interface {v5}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getRemoteId()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->setRemoteId(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 468
    :cond_7
    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v5    # "mutatedEntity":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;
    :catchall_0
    move-exception v11

    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v11
.end method

.method public mutateItems(Landroid/accounts/Account;Ljava/util/List;)Ljava/util/List;
    .locals 35
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">(",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/store/InvalidDataException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;
        }
    .end annotation

    .prologue
    .line 498
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v30

    if-eqz v30, :cond_0

    .line 499
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 643
    :goto_0
    return-object v30

    .line 502
    :cond_0
    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 503
    .local v12, "firstItem":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-interface {v12, v0}, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;->getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v29

    .line 506
    .local v29, "url":Lcom/google/android/music/sync/api/MusicUrl;
    const/16 v23, 0x0

    .line 507
    .local v23, "requestBytes":[B
    instance-of v0, v12, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move/from16 v30, v0

    if-eqz v30, :cond_4

    .line 508
    new-instance v21, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest;

    invoke-direct/range {v21 .. v21}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest;-><init>()V

    .line 509
    .local v21, "playlistsRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 510
    .local v15, "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    const-class v30, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 511
    .local v20, "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    new-instance v17, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;-><init>()V

    .line 512
    .local v17, "mutatePlaylistRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;
    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    move/from16 v30, v0

    if-eqz v30, :cond_1

    .line 513
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->validateForUpstreamDelete()V

    .line 514
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;->mDeletePlaylistRemoteId:Ljava/lang/String;

    .line 522
    :goto_2
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest;->addRequest(Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;)V

    goto :goto_1

    .line 515
    :cond_1
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    move-object/from16 v30, v0

    if-eqz v30, :cond_2

    .line 516
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->validateForUpstreamUpdate()V

    .line 517
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;->mUpdatePlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    goto :goto_2

    .line 519
    :cond_2
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->validateForUpstreamInsert()V

    .line 520
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;->mCreatePlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    goto :goto_2

    .line 524
    .end local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    .end local v17    # "mutatePlaylistRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest$MutatePlaylistRequest;
    .end local v20    # "playlist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_3
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest;->serializeAsJson()[B

    move-result-object v23

    .line 614
    .end local v21    # "playlistsRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistRequest;
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v30, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v9

    .line 615
    .local v9, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    new-instance v24, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;

    move-object/from16 v0, v24

    invoke-direct {v0, v9}, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 618
    .local v24, "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    :try_start_0
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p1

    move-object/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->createAndExecuteMethod(Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :try_end_0
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/sync/api/ConflictException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v25

    .line 628
    .local v25, "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    move-object/from16 v0, v25

    iget-object v14, v0, Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;->mInputStream:Ljava/io/InputStream;

    .line 630
    .local v14, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v14}, Lcom/google/android/music/sync/google/model/BatchMutateResponse;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/BatchMutateResponse;

    move-result-object v4

    .line 632
    .local v4, "batchMutateResponse":Lcom/google/android/music/sync/google/model/BatchMutateResponse;
    iget-object v0, v4, Lcom/google/android/music/sync/google/model/BatchMutateResponse;->mMutateResponses:Ljava/util/List;

    move-object/from16 v30, v0

    if-nez v30, :cond_18

    .line 633
    new-instance v30, Ljava/io/IOException;

    const-string v31, "Received empty data in batch mutate response."

    invoke-direct/range {v30 .. v31}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v30
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 643
    .end local v4    # "batchMutateResponse":Lcom/google/android/music/sync/google/model/BatchMutateResponse;
    :catchall_0
    move-exception v30

    invoke-static {v14}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v30

    .line 525
    .end local v9    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "in":Ljava/io/InputStream;
    .end local v24    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    .end local v25    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :cond_4
    instance-of v0, v12, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move/from16 v30, v0

    if-eqz v30, :cond_9

    .line 526
    new-instance v10, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest;

    invoke-direct {v10}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest;-><init>()V

    .line 527
    .local v10, "entriesRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 528
    .restart local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    const-class v30, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .line 529
    .local v11, "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    new-instance v16, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;-><init>()V

    .line 530
    .local v16, "mutateEntryRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;
    iget-boolean v0, v11, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    move/from16 v30, v0

    if-eqz v30, :cond_5

    .line 531
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->validateForUpstreamDelete()V

    .line 532
    iget-object v0, v11, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;->mDeletePlaylistEntryRemoteId:Ljava/lang/String;

    .line 540
    :goto_5
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest;->addRequest(Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;)V

    goto :goto_4

    .line 533
    :cond_5
    iget-object v0, v11, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    move-object/from16 v30, v0

    if-eqz v30, :cond_6

    .line 534
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->validateForUpstreamUpdate()V

    .line 535
    move-object/from16 v0, v16

    iput-object v11, v0, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;->mUpdatePlaylistEntry:Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    goto :goto_5

    .line 537
    :cond_6
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->validateForUpstreamInsert()V

    .line 538
    move-object/from16 v0, v16

    iput-object v11, v0, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;->mCreatePlaylistEntry:Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    goto :goto_5

    .line 542
    .end local v11    # "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .end local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    .end local v16    # "mutateEntryRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest$MutatePlaylistEntryRequest;
    :cond_7
    sget-boolean v30, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v30, :cond_8

    .line 543
    const-string v30, "MusicApiClient"

    new-instance v31, Ljava/lang/String;

    invoke-virtual {v10}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest;->serializeAsJson()[B

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/String;-><init>([B)V

    invoke-static/range {v30 .. v31}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_8
    invoke-virtual {v10}, Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest;->serializeAsJson()[B

    move-result-object v23

    .line 546
    goto/16 :goto_3

    .end local v10    # "entriesRequest":Lcom/google/android/music/sync/google/model/BatchMutatePlaylistEntryRequest;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_9
    instance-of v0, v12, Lcom/google/android/music/sync/google/model/Track;

    move/from16 v30, v0

    if-eqz v30, :cond_d

    .line 547
    new-instance v28, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;

    invoke-direct/range {v28 .. v28}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;-><init>()V

    .line 548
    .local v28, "tracksRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 549
    .restart local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    const-class v30, Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/google/android/music/sync/google/model/Track;

    .line 550
    .local v27, "track":Lcom/google/android/music/sync/google/model/Track;
    new-instance v19, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;-><init>()V

    .line 552
    .local v19, "mutateTrackRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;
    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/sync/google/model/Track;->isInsert()Z

    move-result v30

    if-eqz v30, :cond_a

    .line 553
    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/sync/google/model/Track;->validateForUpstreamInsert()V

    .line 554
    move-object/from16 v0, v27

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;->mCreateTrack:Lcom/google/android/music/sync/google/model/Track;

    .line 560
    :goto_7
    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;->addRequest(Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;)V

    goto :goto_6

    .line 556
    :cond_a
    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/sync/google/model/Track;->validateForUpstreamUpdate()V

    .line 558
    invoke-static/range {v27 .. v27}, Lcom/google/android/music/sync/google/model/Track;->filterForRatingsUpdate(Lcom/google/android/music/sync/google/model/Track;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;->mUpdateTrack:Lcom/google/android/music/sync/google/model/Track;

    goto :goto_7

    .line 562
    .end local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    .end local v19    # "mutateTrackRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;
    .end local v27    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_b
    sget-boolean v30, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v30, :cond_c

    .line 563
    const-string v30, "MusicApiClient"

    new-instance v31, Ljava/lang/String;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;->serializeAsJson()[B

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/String;-><init>([B)V

    invoke-static/range {v30 .. v31}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_c
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;->serializeAsJson()[B

    move-result-object v23

    .line 566
    goto/16 :goto_3

    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v28    # "tracksRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;
    :cond_d
    instance-of v0, v12, Lcom/google/android/music/sync/google/model/TrackTombstone;

    move/from16 v30, v0

    if-eqz v30, :cond_f

    .line 567
    new-instance v28, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;

    invoke-direct/range {v28 .. v28}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;-><init>()V

    .line 568
    .restart local v28    # "tracksRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_e

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 569
    .restart local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    const-class v30, Lcom/google/android/music/sync/google/model/TrackTombstone;

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/android/music/sync/google/model/TrackTombstone;

    .line 570
    .local v26, "tombstone":Lcom/google/android/music/sync/google/model/TrackTombstone;
    new-instance v19, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;-><init>()V

    .line 571
    .restart local v19    # "mutateTrackRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;
    invoke-virtual/range {v26 .. v26}, Lcom/google/android/music/sync/google/model/TrackTombstone;->validateForUpstreamDelete()V

    .line 572
    invoke-virtual/range {v26 .. v26}, Lcom/google/android/music/sync/google/model/TrackTombstone;->getRemoteId()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;->mDeleteTrackRemoteId:Ljava/lang/String;

    .line 573
    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;->addRequest(Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;)V

    goto :goto_8

    .line 575
    .end local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    .end local v19    # "mutateTrackRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest$MutateTrackRequest;
    .end local v26    # "tombstone":Lcom/google/android/music/sync/google/model/TrackTombstone;
    :cond_e
    const-string v30, "MusicApiClient"

    new-instance v31, Ljava/lang/String;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;->serializeAsJson()[B

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/String;-><init>([B)V

    invoke-static/range {v30 .. v31}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;->serializeAsJson()[B

    move-result-object v23

    .line 577
    goto/16 :goto_3

    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v28    # "tracksRequest":Lcom/google/android/music/sync/google/model/BatchMutateTrackRequest;
    :cond_f
    instance-of v0, v12, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move/from16 v30, v0

    if-eqz v30, :cond_14

    .line 578
    new-instance v8, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;

    invoke-direct {v8}, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;-><init>()V

    .line 579
    .local v8, "editStationsRequest":Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_12

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 580
    .restart local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    const-class v30, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    .line 582
    .local v22, "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    new-instance v18, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;-><init>()V

    .line 583
    .local v18, "mutateRadioStationRequest":Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;
    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    move/from16 v30, v0

    if-eqz v30, :cond_10

    .line 584
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->validateForUpstreamDelete()V

    .line 585
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mDeleteId:Ljava/lang/String;

    .line 593
    :goto_a
    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;->addRequest(Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;)V

    goto :goto_9

    .line 586
    :cond_10
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    move-object/from16 v30, v0

    if-eqz v30, :cond_11

    .line 587
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->validateForUpstreamUpdate()V

    .line 588
    move-object/from16 v0, v22

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mUpdateRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    goto :goto_a

    .line 590
    :cond_11
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->validateForUpstreamInsert()V

    .line 591
    move-object/from16 v0, v22

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mCreateRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    goto :goto_a

    .line 595
    .end local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    .end local v18    # "mutateRadioStationRequest":Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;
    .end local v22    # "radioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    :cond_12
    sget-boolean v30, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v30, :cond_13

    .line 596
    const-string v30, "MusicApiClient"

    new-instance v31, Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonString(Lcom/google/api/client/json/GenericJson;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static/range {v30 .. v31}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    :cond_13
    invoke-static {v8}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v23

    .line 599
    goto/16 :goto_3

    .end local v8    # "editStationsRequest":Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_14
    instance-of v0, v12, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    move/from16 v30, v0

    if-eqz v30, :cond_17

    .line 600
    new-instance v6, Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;

    invoke-direct {v6}, Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;-><init>()V

    .line 602
    .local v6, "blacklistItemsAddRequest":Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :goto_b
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_15

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;

    .line 603
    .restart local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    const-class v30, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    .line 604
    .local v5, "blacklistItem":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    invoke-virtual {v6, v5}, Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;->addBlacklistItem(Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;)V

    goto :goto_b

    .line 606
    .end local v5    # "blacklistItem":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    .end local v15    # "item":Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;, "TT;"
    :cond_15
    sget-boolean v30, Lcom/google/android/music/sync/api/MusicApiClientImpl;->LOGV:Z

    if-eqz v30, :cond_16

    .line 607
    const-string v30, "MusicApiClient"

    new-instance v31, Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonString(Lcom/google/api/client/json/GenericJson;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static/range {v30 .. v31}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_16
    invoke-static {v6}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v23

    .line 610
    goto/16 :goto_3

    .line 611
    .end local v6    # "blacklistItemsAddRequest":Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_17
    const-string v30, "MusicApiClient"

    const-string v31, "Unrecognized item type in a batch mutate."

    invoke-static/range {v30 .. v31}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    new-instance v30, Lcom/google/android/music/store/InvalidDataException;

    const-string v31, "Unrecognized item type in a batch mutate."

    invoke-direct/range {v30 .. v31}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 619
    .restart local v9    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v24    # "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    :catch_0
    move-exception v7

    .line 620
    .local v7, "e":Lcom/google/android/music/sync/api/NotModifiedException;
    new-instance v30, Lcom/google/android/music/sync/common/SyncHttpException;

    const-string v31, "Unexpected not-modified status returned on a batched mutation"

    const/16 v32, 0x130

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/music/sync/common/SyncHttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v30

    .line 623
    .end local v7    # "e":Lcom/google/android/music/sync/api/NotModifiedException;
    :catch_1
    move-exception v7

    .line 624
    .local v7, "e":Lcom/google/android/music/sync/api/ConflictException;
    new-instance v30, Lcom/google/android/music/sync/common/SyncHttpException;

    const-string v31, "Unexpected conflict status returned on a batched mutation"

    const/16 v32, 0x199

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/music/sync/common/SyncHttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v30

    .line 635
    .end local v7    # "e":Lcom/google/android/music/sync/api/ConflictException;
    .restart local v4    # "batchMutateResponse":Lcom/google/android/music/sync/google/model/BatchMutateResponse;
    .restart local v14    # "in":Ljava/io/InputStream;
    .restart local v25    # "response":Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;
    :cond_18
    :try_start_2
    iget-object v0, v4, Lcom/google/android/music/sync/google/model/BatchMutateResponse;->mMutateResponses:Ljava/util/List;

    move-object/from16 v30, v0

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v30

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_19

    .line 636
    new-instance v30, Ljava/io/IOException;

    const-string v31, "Mismatch between number of entries in request and response (%s, %s)"

    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v34

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x1

    iget-object v0, v4, Lcom/google/android/music/sync/google/model/BatchMutateResponse;->mMutateResponses:Ljava/util/List;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Ljava/util/List;->size()I

    move-result v34

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    aput-object v34, v32, v33

    invoke-static/range {v31 .. v32}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 641
    :cond_19
    iget-object v0, v4, Lcom/google/android/music/sync/google/model/BatchMutateResponse;->mMutateResponses:Ljava/util/List;

    move-object/from16 v30, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 643
    invoke-static {v14}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0
.end method

.method public reportTrackStats(Landroid/accounts/Account;Ljava/util/List;)V
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/music/sync/common/QueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/SyncHttpException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/api/BadRequestException;,
            Lcom/google/android/music/sync/api/ForbiddenException;,
            Lcom/google/android/music/sync/api/ServiceUnavailableException;,
            Lcom/google/android/music/sync/api/ConflictException;,
            Lcom/google/android/music/sync/api/ResourceNotFoundException;,
            Lcom/google/android/music/sync/api/NotModifiedException;
        }
    .end annotation

    .prologue
    .line 935
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/music/sync/common/QueueableSyncEntity;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_1

    .line 936
    :cond_0
    const-string v8, "MusicApiClient"

    const-string v9, "Track stats list is null or empty. Skip reporting."

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    :goto_0
    return-void

    .line 940
    :cond_1
    const-class v8, Lcom/google/android/music/sync/google/model/TrackStat;

    const/4 v9, 0x0

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/TrackStat;

    .line 941
    .local v1, "firstItem":Lcom/google/android/music/sync/google/model/TrackStat;
    iget-object v8, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v8}, Lcom/google/android/music/sync/google/model/TrackStat;->getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v7

    .line 944
    .local v7, "url":Lcom/google/android/music/sync/api/MusicUrl;
    new-instance v6, Lcom/google/android/music/sync/google/model/BatchReportTrackStats;

    invoke-direct {v6}, Lcom/google/android/music/sync/google/model/BatchReportTrackStats;-><init>()V

    .line 945
    .local v6, "trackStatsRequest":Lcom/google/android/music/sync/google/model/BatchReportTrackStats;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/common/QueueableSyncEntity;

    .line 946
    .local v3, "item":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    const-class v8, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-virtual {v8, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-virtual {v6, v8}, Lcom/google/android/music/sync/google/model/BatchReportTrackStats;->addTrackStat(Lcom/google/android/music/sync/google/model/TrackStat;)V

    goto :goto_1

    .line 948
    .end local v3    # "item":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    :cond_2
    invoke-virtual {v6}, Lcom/google/android/music/sync/google/model/BatchReportTrackStats;->serializeAsJson()[B

    move-result-object v4

    .line 950
    .local v4, "requestBytes":[B
    iget-object v8, p0, Lcom/google/android/music/sync/api/MusicApiClientImpl;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v4, v8}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v0

    .line 951
    .local v0, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    new-instance v5, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;

    invoke-direct {v5, v0}, Lcom/google/android/music/sync/api/MusicApiClientImpl$PostRequestCreator;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 952
    .local v5, "requestCreator":Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;
    invoke-virtual {v7}, Lcom/google/android/music/sync/api/MusicUrl;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v5, p1, v8}, Lcom/google/android/music/sync/api/MusicApiClientImpl;->createAndExecuteMethod(Lcom/google/android/music/sync/api/MusicApiClientImpl$HttpRequestCreator;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicApiClientImpl$MethodExecutionResult;

    goto :goto_0
.end method
