.class public Lcom/google/android/music/ui/cardlib/layout/PopupSelector;
.super Landroid/app/Dialog;
.source "PopupSelector.java"


# instance fields
.field private final mAnchorX:I

.field private final mAnchorY:I

.field private mContext:Landroid/content/Context;

.field private final mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mMeasureParent:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchorView"    # Landroid/view/View;
    .param p3, "listAdapter"    # Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    .prologue
    .line 57
    const v1, 0x7f0a001e

    invoke-direct {p0, p1, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 58
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 59
    .local v0, "location":[I
    invoke-virtual {p2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 60
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mAnchorX:I

    .line 61
    const/4 v1, 0x1

    aget v1, v0, v1

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mAnchorY:I

    .line 62
    iput-object p3, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    .line 63
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/cardlib/layout/PopupSelector;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/cardlib/layout/PopupSelector;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->layoutListView()V

    return-void
.end method

.method private layoutListView()V
    .locals 15

    .prologue
    const/high16 v14, 0x40000000    # 2.0f

    const/4 v13, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->getWindow()Landroid/view/Window;

    move-result-object v10

    .line 101
    .local v10, "window":Landroid/view/Window;
    new-instance v6, Landroid/util/DisplayMetrics;

    invoke-direct {v6}, Landroid/util/DisplayMetrics;-><init>()V

    .line 102
    .local v6, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v10}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v11

    invoke-virtual {v11, v6}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 103
    iget v9, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 104
    .local v9, "screenWidth":I
    iget v8, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 106
    .local v8, "screenHeight":I
    const v11, 0x7f0e023b

    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 107
    .local v4, "mainView":Landroid/view/View;
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->measureContent()Landroid/util/Pair;

    move-result-object v5

    .line 108
    .local v5, "measures":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v11, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/google/android/music/utils/ViewUtils;->getActionBarHeight(Landroid/content/Context;)I

    move-result v0

    .line 109
    .local v0, "actionBarHeight":I
    iget-object v11, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/google/android/music/utils/ViewUtils;->getNotificationBarHeight(Landroid/content/Context;)I

    move-result v7

    .line 111
    .local v7, "notificationBarHeight":I
    iget-object v11, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getPaddingLeft()I

    move-result v12

    add-int/2addr v11, v12

    iget-object v12, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getPaddingRight()I

    move-result v12

    add-int/2addr v11, v12

    mul-int/lit8 v12, v9, 0x4

    div-int/lit8 v12, v12, 0x5

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 113
    .local v2, "contentWidth":I
    iget-object v11, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v12

    add-int/2addr v11, v12

    iget-object v12, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v12

    add-int/2addr v11, v12

    sub-int v12, v8, v0

    sub-int/2addr v12, v7

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 118
    .local v1, "contentHeight":I
    invoke-static {v2, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-static {v1, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    invoke-virtual {v4, v11, v12}, Landroid/view/View;->measure(II)V

    .line 120
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    invoke-virtual {v4, v13, v13, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 122
    invoke-virtual {v10}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 123
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 124
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 125
    iget v11, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mAnchorX:I

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 126
    iget v11, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mAnchorY:I

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 128
    const/16 v11, 0x33

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 130
    iget v11, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v12, 0x20100

    or-int/2addr v11, v12

    iput v11, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 132
    invoke-virtual {v10, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 133
    return-void
.end method

.method private measureContent()Landroid/util/Pair;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 146
    const/4 v7, 0x0

    .line 147
    .local v7, "width":I
    const/4 v1, 0x0

    .line 148
    .local v1, "height":I
    const/4 v5, 0x0

    .line 149
    .local v5, "itemView":Landroid/view/View;
    const/4 v4, 0x0

    .line 150
    .local v4, "itemType":I
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 152
    .local v8, "widthMeasureSpec":I
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 154
    .local v2, "heightMeasureSpec":I
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-virtual {v9}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getCount()I

    move-result v0

    .line 155
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 156
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-virtual {v9, v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItemViewType(I)I

    move-result v6

    .line 157
    .local v6, "positionType":I
    if-eq v6, v4, :cond_0

    .line 158
    move v4, v6

    .line 159
    const/4 v5, 0x0

    .line 161
    :cond_0
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mMeasureParent:Landroid/view/ViewGroup;

    if-nez v9, :cond_1

    .line 162
    new-instance v9, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mMeasureParent:Landroid/view/ViewGroup;

    .line 164
    :cond_1
    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mMeasureParent:Landroid/view/ViewGroup;

    invoke-virtual {v9, v3, v5, v10}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 165
    invoke-virtual {v5, v8, v2}, Landroid/view/View;->measure(II)V

    .line 166
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 167
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v5}, Landroid/view/View;->getPaddingTop()I

    move-result v10

    add-int/2addr v9, v10

    add-int/2addr v1, v9

    .line 155
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 170
    .end local v6    # "positionType":I
    :cond_2
    new-instance v9, Landroid/util/Pair;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v9
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 184
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "announcement":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    const/4 v1, 0x1

    .line 191
    .end local v0    # "announcement":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    const v0, 0x7f0400c8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->setContentView(I)V

    .line 70
    const v0, 0x7f0e023c

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    .line 71
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PopupSelector$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector$1;-><init>(Lcom/google/android/music/ui/cardlib/layout/PopupSelector;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PopupSelector$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector$2;-><init>(Lcom/google/android/music/ui/cardlib/layout/PopupSelector;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 96
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->layoutListView()V

    .line 97
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 196
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 197
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 204
    .end local v0    # "handled":Z
    :goto_0
    return v0

    .line 200
    .restart local v0    # "handled":Z
    :cond_0
    const/16 v1, 0x13

    if-ne p1, v1, :cond_1

    .line 201
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->cancel()V

    .line 202
    const/4 v0, 0x1

    goto :goto_0

    .line 204
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->cancel()V

    .line 177
    const/4 v0, 0x1

    .line 179
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
