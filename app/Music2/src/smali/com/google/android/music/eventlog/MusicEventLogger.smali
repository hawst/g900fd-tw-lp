.class public Lcom/google/android/music/eventlog/MusicEventLogger;
.super Ljava/lang/Object;
.source "MusicEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/eventlog/MusicEventLogger$8;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;


# instance fields
.field private final EVENT_TAG:Ljava/lang/String;

.field private final mAccountReceiver:Landroid/content/BroadcastReceiver;

.field private final mAndroidId:J

.field private final mApplicationVersion:Ljava/lang/String;

.field private final mAuthTokenType:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mEventLogger:Lcom/google/android/play/analytics/EventLogger;

.field private final mEventLoggers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/play/analytics/EventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final mLoggingId:Ljava/lang/String;

.field private mMainstageImpressions:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

.field private final mMccMnc:Ljava/lang/String;

.field private final mPlayLoggingEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->EVENT_LOG:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    .line 102
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/eventlog/MusicEventLogger;->sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const-string v8, "MusicEventLog"

    iput-object v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->EVENT_TAG:Ljava/lang/String;

    .line 116
    new-instance v8, Lcom/google/android/music/eventlog/MusicEventLogger$1;

    invoke-direct {v8, p0}, Lcom/google/android/music/eventlog/MusicEventLogger$1;-><init>(Lcom/google/android/music/eventlog/MusicEventLogger;)V

    iput-object v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    .line 127
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    .line 128
    .local v4, "musicPreferenceReference":Ljava/lang/Object;
    iget-object v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v8, v4}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    .line 131
    .local v5, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mLoggingId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    invoke-static {v4}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 136
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iput-object v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLoggers:Ljava/util/HashMap;

    .line 137
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "music_enable_structured_play_logging"

    invoke-static {v8, v9, v6}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isPlayLoggingEnabled()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isStructuredPlayLoggingEnabled()Z

    move-result v8

    if-eqz v8, :cond_0

    :goto_0
    iput-boolean v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mPlayLoggingEnabled:Z

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "android_id"

    const-wide/16 v8, 0x0

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mAndroidId:J

    .line 143
    iget-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/sync/google/MusicAuthInfo;->getAuthTokenType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mAuthTokenType:Ljava/lang/String;

    .line 144
    iget-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mMccMnc:Ljava/lang/String;

    .line 147
    const-string v0, ""

    .line 149
    .local v0, "applicationVersion":Ljava/lang/String;
    :try_start_1
    iget-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 151
    .local v3, "info":Landroid/content/pm/PackageInfo;
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 155
    .end local v3    # "info":Landroid/content/pm/PackageInfo;
    :goto_1
    iput-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mApplicationVersion:Ljava/lang/String;

    .line 157
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v6, "com.google.android.music.accountchanged"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    const-string v6, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    iget-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mAccountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v6, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 161
    invoke-direct {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->maybeUpdateEventLogger()V

    .line 162
    new-instance v6, Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    invoke-direct {v6, p0}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;-><init>(Lcom/google/android/music/eventlog/MusicEventLogger;)V

    iput-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mMainstageImpressions:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    .line 163
    return-void

    .line 133
    .end local v0    # "applicationVersion":Ljava/lang/String;
    .end local v2    # "filter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v6

    invoke-static {v4}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v6

    :cond_0
    move v6, v7

    .line 137
    goto :goto_0

    .line 152
    .restart local v0    # "applicationVersion":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 153
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "MusicLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Package not found (to retrieve version number)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/music/eventlog/MusicEventLogger;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/eventlog/MusicEventLogger;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->maybeUpdateEventLogger()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/eventlog/MusicEventLogger;
    .param p1, "x1"    # Z
    .param p2, "x2"    # J
    .param p4, "x3"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p5, "x4"    # J
    .param p7, "x5"    # Z

    .prologue
    .line 96
    invoke-direct/range {p0 .. p7}, Lcom/google/android/music/eventlog/MusicEventLogger;->logStopEvent(ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/eventlog/MusicEventLogger;
    .param p1, "x1"    # Z
    .param p2, "x2"    # J
    .param p4, "x3"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p5, "x4"    # J
    .param p7, "x5"    # J

    .prologue
    .line 96
    invoke-direct/range {p0 .. p8}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSkipForwardEvent(ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/eventlog/MusicEventLogger;ZJJJ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/eventlog/MusicEventLogger;
    .param p1, "x1"    # Z
    .param p2, "x2"    # J
    .param p4, "x3"    # J
    .param p6, "x4"    # J

    .prologue
    .line 96
    invoke-direct/range {p0 .. p7}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSeekEvent(ZJJJ)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/eventlog/MusicEventLogger;Lcom/google/android/music/store/ContainerDescriptor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/eventlog/MusicEventLogger;
    .param p1, "x1"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logRefreshRadio(Lcom/google/android/music/store/ContainerDescriptor;)V

    return-void
.end method

.method private convertMainstageDocument(Lcom/google/android/music/ui/cardlib/model/Document;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    .locals 8
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x0

    .line 1299
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;-><init>()V

    .line 1300
    .local v1, "card":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    new-instance v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-direct {v5}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;-><init>()V

    iput-object v5, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1301
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    new-instance v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-direct {v6}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;-><init>()V

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    .line 1302
    sget-object v5, Lcom/google/android/music/eventlog/MusicEventLogger$8;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1353
    const-string v5, "MusicLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Attempting to translate unsupported document type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    .line 1359
    .end local v1    # "card":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    :goto_0
    return-object v1

    .line 1304
    .restart local v1    # "card":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v0

    .line 1305
    .local v0, "albumMetajamId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v1, v4

    .line 1306
    goto :goto_0

    .line 1308
    :cond_0
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    const/4 v5, 0x2

    iput v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1309
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iput-object v0, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->albumId:Ljava/lang/String;

    .line 1357
    .end local v0    # "albumMetajamId":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateMainstageReason(I)I

    move-result v4

    iput v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->reason:I

    .line 1358
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPosition()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->position:Ljava/lang/Integer;

    goto :goto_0

    .line 1312
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedId()Ljava/lang/String;

    move-result-object v3

    .line 1313
    .local v3, "suggestedSeedId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    const/16 v6, 0x32

    if-ne v5, v6, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1316
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iput v7, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1317
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    new-instance v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    invoke-direct {v5}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;-><init>()V

    iput-object v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    .line 1318
    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1319
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->METAJAM_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v5}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getRemoteValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->seedType:Ljava/lang/Integer;

    .line 1322
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    iput-object v3, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->trackMetajamId:Ljava/lang/String;

    goto :goto_1

    .line 1324
    :cond_1
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->LOCKER_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v5}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getRemoteValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->seedType:Ljava/lang/Integer;

    .line 1327
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    iput-object v3, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->trackLockerId:Ljava/lang/String;

    goto :goto_1

    .line 1330
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareToken()Ljava/lang/String;

    move-result-object v2

    .line 1331
    .local v2, "shareToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object v1, v4

    .line 1332
    goto/16 :goto_0

    .line 1334
    :cond_3
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    const/4 v5, 0x4

    iput v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1335
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/google/android/music/eventlog/MusicEventLogger;->translatePlaylistType(I)I

    move-result v5

    iput v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->playlistType:I

    .line 1336
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iput-object v2, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->playlistId:Ljava/lang/String;

    goto/16 :goto_1

    .line 1340
    .end local v2    # "shareToken":Ljava/lang/String;
    .end local v3    # "suggestedSeedId":Ljava/lang/String;
    :pswitch_2
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iput v7, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1341
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedType()I

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateRadioSeed(ILjava/lang/String;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    goto/16 :goto_1

    .line 1345
    :pswitch_3
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    const/16 v5, 0xa

    iput v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1346
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSituationId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->situationId:Ljava/lang/String;

    goto/16 :goto_1

    .line 1349
    :pswitch_4
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    const/16 v5, 0xc

    iput v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1350
    iget-object v4, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    iget-object v4, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSituationId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->subSituationId:Ljava/lang/String;

    goto/16 :goto_1

    .line 1302
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static declared-synchronized destroy()V
    .locals 2

    .prologue
    .line 242
    const-class v1, Lcom/google/android/music/eventlog/MusicEventLogger;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/eventlog/MusicEventLogger;->sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;

    if-eqz v0, :cond_0

    .line 243
    sget-object v0, Lcom/google/android/music/eventlog/MusicEventLogger;->sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;

    invoke-direct {v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->onDestroy()V

    .line 244
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/eventlog/MusicEventLogger;->sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :cond_0
    monitor-exit v1

    return-void

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getEventLogger(Landroid/accounts/Account;)Lcom/google/android/play/analytics/EventLogger;
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLoggers:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/play/analytics/EventLogger;

    .line 209
    .local v13, "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    if-nez v13, :cond_2

    .line 210
    new-instance v10, Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-direct {v10}, Lcom/google/android/play/analytics/EventLogger$Configuration;-><init>()V

    .line 211
    .local v10, "config":Lcom/google/android/play/analytics/EventLogger$Configuration;
    const-string v1, "https://play.googleapis.com/play/log"

    iput-object v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->mServerUrl:Ljava/lang/String;

    .line 213
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 214
    const-wide/16 v2, 0x3e8

    iput-wide v2, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218
    const-string v1, "logs:main"

    iput-object v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->logDirectoryName:Ljava/lang/String;

    .line 224
    :goto_0
    :try_start_0
    new-instance v0, Lcom/google/android/play/analytics/EventLogger;

    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mLoggingId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mAuthTokenType:Ljava/lang/String;

    sget-object v4, Lcom/google/android/play/analytics/EventLogger$LogSource;->MUSIC:Lcom/google/android/play/analytics/EventLogger$LogSource;

    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/play/utils/PlayUtils;->getDefaultUserAgentString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    iget-wide v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mAndroidId:J

    iget-object v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mApplicationVersion:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mMccMnc:Ljava/lang/String;

    move-object v11, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/play/analytics/EventLogger;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;Landroid/accounts/Account;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    .end local v13    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    .local v0, "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLoggers:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 235
    .end local v10    # "config":Lcom/google/android/play/analytics/EventLogger$Configuration;
    :goto_1
    return-object v0

    .line 220
    .end local v0    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    .restart local v10    # "config":Lcom/google/android/play/analytics/EventLogger$Configuration;
    .restart local v13    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    :cond_1
    const-string v1, "logs:ui"

    iput-object v1, v10, Lcom/google/android/play/analytics/EventLogger$Configuration;->logDirectoryName:Ljava/lang/String;

    goto :goto_0

    .line 228
    :catch_0
    move-exception v12

    move-object v0, v13

    .line 232
    .end local v13    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    .restart local v0    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    .local v12, "e":Ljava/lang/Exception;
    :goto_2
    const-string v1, "MusicLogger"

    const-string v2, "Error in creating EventLogger"

    invoke-static {v1, v2, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 228
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v12

    goto :goto_2

    .end local v0    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    .end local v10    # "config":Lcom/google/android/play/analytics/EventLogger$Configuration;
    .restart local v13    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    :cond_2
    move-object v0, v13

    .end local v13    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    .restart local v0    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    goto :goto_1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 252
    const-class v1, Lcom/google/android/music/eventlog/MusicEventLogger;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/eventlog/MusicEventLogger;->sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;

    if-nez v0, :cond_0

    .line 255
    new-instance v0, Lcom/google/android/music/eventlog/MusicEventLogger;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/music/eventlog/MusicEventLogger;->sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 257
    :cond_0
    sget-object v0, Lcom/google/android/music/eventlog/MusicEventLogger;->sInstance:Lcom/google/android/music/eventlog/MusicEventLogger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getUploadAccount()Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 170
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 171
    .local v0, "musicPreferenceReference":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 174
    .local v1, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getUploadAccount(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 176
    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method private getUploadAccount(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/accounts/Account;
    .locals 5
    .param p1, "preferences"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 181
    invoke-virtual {p1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 182
    .local v2, "streamingAccount":Landroid/accounts/Account;
    if-eqz v2, :cond_0

    .line 197
    .end local v2    # "streamingAccount":Landroid/accounts/Account;
    :goto_0
    return-object v2

    .line 190
    .restart local v2    # "streamingAccount":Landroid/accounts/Account;
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    const-string v4, "account"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    .line 193
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 194
    .local v1, "accounts":[Landroid/accounts/Account;
    if-eqz v1, :cond_1

    array-length v3, v1

    if-eqz v3, :cond_1

    .line 195
    const/4 v3, 0x0

    aget-object v2, v1, v3

    goto :goto_0

    .line 197
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    .prologue
    .line 1252
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;J)V

    .line 1253
    return-void
.end method

.method private logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;J)V
    .locals 12
    .param p1, "event"    # Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    .param p2, "eventTimeMs"    # J

    .prologue
    const/4 v2, 0x0

    .line 1262
    const/4 v8, 0x0

    .line 1263
    .local v8, "hasNautilusAccount":Z
    const/4 v7, 0x0

    .line 1265
    .local v7, "hasLockerAccount":Z
    new-instance v10, Ljava/lang/Object;

    invoke-direct {v10}, Ljava/lang/Object;-><init>()V

    .line 1266
    .local v10, "refObject":Ljava/lang/Object;
    iget-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v0, v10}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v9

    .line 1268
    .local v9, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v9}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 1270
    invoke-static {v10}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 1272
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isAcceptedUser()Z

    move-result v7

    .line 1275
    if-eqz v8, :cond_1

    .line 1276
    const/4 v0, 0x3

    iput v0, p1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    .line 1283
    :goto_0
    sget-boolean v0, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1284
    const-string v0, "MusicLogger"

    invoke-virtual {p1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1287
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    const-string v1, "MusicEventLog"

    move-object v6, v2

    check-cast v6, [Ljava/lang/String;

    move-object v3, p1

    move-wide v4, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;Lcom/google/protobuf/nano/MessageNano;J[Ljava/lang/String;)V

    .line 1289
    return-void

    .line 1270
    :catchall_0
    move-exception v0

    invoke-static {v10}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v0

    .line 1277
    :cond_1
    if-eqz v7, :cond_2

    .line 1278
    const/4 v0, 0x2

    iput v0, p1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    goto :goto_0

    .line 1280
    :cond_2
    const/4 v0, 0x1

    iput v0, p1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->userServiceLevel:I

    goto :goto_0
.end method

.method private logRefreshRadio(Lcom/google/android/music/store/ContainerDescriptor;)V
    .locals 4
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    .line 1145
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1146
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logRefreshRadio: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 1160
    :goto_0
    return-void

    .line 1152
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1153
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    .line 1155
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    const/4 v2, 0x4

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    .line 1156
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    .line 1157
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    iget-object v1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->stationRefreshedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$StationRefreshedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1159
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method private logSeekEvent(ZJJJ)V
    .locals 8
    .param p1, "isRemote"    # Z
    .param p2, "musicId"    # J
    .param p4, "curPosition"    # J
    .param p6, "seekPosition"    # J

    .prologue
    .line 591
    sget-boolean v5, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 592
    const-string v5, "MusicLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "logSeekEvent: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v5, :cond_1

    .line 626
    :goto_0
    return-void

    .line 599
    :cond_1
    const/4 v2, 0x0

    .line 601
    .local v2, "musicFile":Lcom/google/android/music/store/MusicFile;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6, p2, p3}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 607
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 608
    .local v1, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-direct {v5}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;-><init>()V

    iput-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 610
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v3

    .line 611
    .local v3, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v4

    .line 612
    .local v4, "trackSourceId":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 613
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v3, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackMetajamId:Ljava/lang/String;

    .line 615
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->isSourceTypeLocker()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 616
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v4, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackLockerId:Ljava/lang/String;

    .line 619
    :cond_3
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v6, 0x5

    iput v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->audioPlayer:I

    .line 620
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->isRemote:Ljava/lang/Boolean;

    .line 621
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v6, 0x5

    iput v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playerEventType:I

    .line 622
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    new-instance v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SeekInfo;

    invoke-direct {v6}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SeekInfo;-><init>()V

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->seekInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SeekInfo;

    .line 623
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v5, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->seekInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SeekInfo;

    long-to-int v6, p4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SeekInfo;->playbackPositionMillis:Ljava/lang/Integer;

    .line 624
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v5, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->seekInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SeekInfo;

    long-to-int v6, p6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SeekInfo;->newPlaybackPositionMillis:Ljava/lang/Integer;

    .line 625
    invoke-direct {p0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0

    .line 602
    .end local v1    # "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    .end local v3    # "trackMetajamId":Ljava/lang/String;
    .end local v4    # "trackSourceId":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 603
    .local v0, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v5, "MusicLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Couldn\'t get MusicFile for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private logSkipForwardEvent(ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V
    .locals 11
    .param p1, "isRemote"    # Z
    .param p2, "musicId"    # J
    .param p4, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p5, "curPosition"    # J
    .param p7, "lastUserExplicitPlayTime"    # J

    .prologue
    .line 522
    sget-boolean v7, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v7, :cond_0

    .line 523
    const-string v7, "MusicLogger"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "logSkipForwardEvent: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p5

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p7

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    :cond_0
    iget-object v7, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v7, :cond_1

    .line 560
    :goto_0
    return-void

    .line 530
    :cond_1
    const/4 v4, 0x0

    .line 532
    .local v4, "musicFile":Lcom/google/android/music/store/MusicFile;
    :try_start_0
    iget-object v7, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8, p2, p3}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 538
    new-instance v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v3}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 539
    .local v3, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-direct {v7}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;-><init>()V

    iput-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 541
    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v5

    .line 542
    .local v5, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v6

    .line 543
    .local v6, "trackSourceId":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 544
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v5, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackMetajamId:Ljava/lang/String;

    .line 546
    :cond_2
    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->isSourceTypeLocker()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 547
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v6, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackLockerId:Ljava/lang/String;

    .line 550
    :cond_3
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v8, 0x5

    iput v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->audioPlayer:I

    .line 551
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->isRemote:Ljava/lang/Boolean;

    .line 552
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v8, 0x3

    iput v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playerEventType:I

    .line 553
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    new-instance v8, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;

    invoke-direct {v8}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;-><init>()V

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->skipForwardInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;

    .line 554
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v7, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->skipForwardInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;

    move-wide/from16 v0, p5

    long-to-int v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;->playbackPositionMillis:Ljava/lang/Integer;

    .line 555
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v7, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->skipForwardInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, p7

    long-to-int v8, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;->timePlayedSinceStartMillis:Ljava/lang/Integer;

    .line 557
    iget-object v7, v3, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v7, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->skipForwardInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;

    invoke-direct {p0, p4, v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$SkipForwardInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 559
    invoke-direct {p0, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto/16 :goto_0

    .line 533
    .end local v3    # "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    .end local v5    # "trackMetajamId":Ljava/lang/String;
    .end local v6    # "trackSourceId":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 534
    .local v2, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v7, "MusicLogger"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Couldn\'t get MusicFile for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private logStopEvent(ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V
    .locals 8
    .param p1, "isRemote"    # Z
    .param p2, "musicId"    # J
    .param p4, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p5, "lastUserExplicitPlayTime"    # J
    .param p7, "explicit"    # Z

    .prologue
    .line 448
    sget-boolean v5, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 449
    const-string v5, "MusicLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "logStopEvent: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v5, :cond_1

    .line 486
    :goto_0
    return-void

    .line 456
    :cond_1
    const/4 v2, 0x0

    .line 458
    .local v2, "musicFile":Lcom/google/android/music/store/MusicFile;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6, p2, p3}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 464
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 465
    .local v1, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-direct {v5}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;-><init>()V

    iput-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 467
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v3

    .line 468
    .local v3, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v4

    .line 469
    .local v4, "trackSourceId":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 470
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v3, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackMetajamId:Ljava/lang/String;

    .line 472
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->isSourceTypeLocker()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 473
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v4, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackLockerId:Ljava/lang/String;

    .line 476
    :cond_3
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v6, 0x5

    iput v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->audioPlayer:I

    .line 477
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->isRemote:Ljava/lang/Boolean;

    .line 478
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v6, 0x2

    iput v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playerEventType:I

    .line 479
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    new-instance v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;

    invoke-direct {v6}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;-><init>()V

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->stopInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;

    .line 480
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v5, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->stopInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;

    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;->explicitStop:Ljava/lang/Boolean;

    .line 481
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v5, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->stopInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, p5

    long-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;->timePlayedSinceStartMillis:Ljava/lang/Integer;

    .line 483
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v5, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->stopInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;

    invoke-direct {p0, p4, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$StopInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 485
    invoke-direct {p0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0

    .line 459
    .end local v1    # "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    .end local v3    # "trackMetajamId":Ljava/lang/String;
    .end local v4    # "trackSourceId":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v5, "MusicLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Couldn\'t get MusicFile for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private declared-synchronized maybeUpdateEventLogger()V
    .locals 1

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mPlayLoggingEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 205
    :goto_0
    monitor-exit p0

    return-void

    .line 204
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getUploadAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/play/analytics/EventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private onDestroy()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mAccountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 167
    return-void
.end method

.method private translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;
    .locals 10
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 1645
    if-nez p1, :cond_1

    .line 1646
    const/4 v0, 0x0

    .line 1759
    :cond_0
    :goto_0
    return-object v0

    .line 1648
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;-><init>()V

    .line 1649
    .local v0, "container":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;
    new-instance v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-direct {v7}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;-><init>()V

    iput-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    .line 1650
    sget-object v7, Lcom/google/android/music/eventlog/MusicEventLogger$8;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1757
    const-string v7, "MusicLogger"

    const-string v8, "unknown container type"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1652
    :pswitch_0
    const/4 v7, 0x4

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1653
    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v2

    .line 1654
    .local v2, "playlistId":J
    iget-object v7, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-static {v7, v2, v3, v8}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/content/Context;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    move-result-object v1

    .line 1655
    .local v1, "list":Lcom/google/android/music/store/PlayList;
    if-eqz v1, :cond_0

    .line 1656
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {v1}, Lcom/google/android/music/store/PlayList;->getShareToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->playlistId:Ljava/lang/String;

    goto :goto_0

    .line 1660
    .end local v1    # "list":Lcom/google/android/music/store/PlayList;
    .end local v2    # "playlistId":J
    :pswitch_1
    const/4 v7, 0x4

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1661
    const/4 v7, 0x1

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->playlistType:I

    goto :goto_0

    .line 1664
    :pswitch_2
    const/4 v7, 0x4

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1665
    const/4 v7, 0x2

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->playlistType:I

    goto :goto_0

    .line 1668
    :pswitch_3
    const/4 v7, 0x4

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1669
    const/4 v7, 0x3

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->playlistType:I

    goto :goto_0

    .line 1672
    :pswitch_4
    const/4 v7, 0x4

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1673
    const/4 v7, 0x5

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->playlistType:I

    .line 1674
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->playlistId:Ljava/lang/String;

    goto :goto_0

    .line 1677
    :pswitch_5
    const/4 v7, 0x2

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1678
    if-eqz p2, :cond_0

    .line 1679
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->albumId:Ljava/lang/String;

    goto :goto_0

    .line 1683
    :pswitch_6
    const/4 v7, 0x2

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1684
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->albumId:Ljava/lang/String;

    goto :goto_0

    .line 1687
    :pswitch_7
    const/4 v7, 0x6

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1688
    if-eqz p2, :cond_0

    .line 1689
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->artistId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1693
    :pswitch_8
    const/4 v7, 0x6

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1694
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->artistId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1697
    :pswitch_9
    const/4 v7, 0x3

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1698
    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v4

    .line 1699
    .local v4, "radioId":J
    iget-object v7, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-static {v7, v4, v5, v8}, Lcom/google/android/music/store/RadioStation;->read(Landroid/content/Context;JLcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    move-result-object v6

    .line 1700
    .local v6, "station":Lcom/google/android/music/store/RadioStation;
    if-eqz v6, :cond_0

    .line 1701
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {v6}, Lcom/google/android/music/store/RadioStation;->getSeedSourceType()I

    move-result v8

    invoke-virtual {v6}, Lcom/google/android/music/store/RadioStation;->getSeedSourceId()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateRadioSeed(ILjava/lang/String;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    goto/16 :goto_0

    .line 1707
    .end local v4    # "radioId":J
    .end local v6    # "station":Lcom/google/android/music/store/RadioStation;
    :pswitch_a
    const/4 v7, 0x3

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1708
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    new-instance v8, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    invoke-direct {v8}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;-><init>()V

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    .line 1709
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iget-object v7, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    sget-object v8, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->LUCKY:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v8}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getRemoteValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->seedType:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1714
    :pswitch_b
    const/16 v7, 0x8

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1715
    const/4 v7, 0x1

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->libraryView:I

    goto/16 :goto_0

    .line 1718
    :pswitch_c
    const/4 v7, 0x5

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1719
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->genreId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1722
    :pswitch_d
    const/4 v7, 0x3

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1723
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    new-instance v8, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    invoke-direct {v8}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;-><init>()V

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    .line 1724
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iget-object v7, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    sget-object v8, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ARTIST_SHUFFLE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v8}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getRemoteValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->seedType:Ljava/lang/Integer;

    .line 1727
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    iget-object v7, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->radioSeed:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->artistMetajamId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1730
    :pswitch_e
    const/4 v7, 0x6

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1731
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->artistId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1734
    :pswitch_f
    const/4 v7, 0x5

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1735
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->genreId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1738
    :pswitch_10
    const/16 v7, 0x9

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    goto/16 :goto_0

    .line 1741
    :pswitch_11
    const/4 v7, 0x7

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    goto/16 :goto_0

    .line 1744
    :pswitch_12
    const/4 v7, 0x1

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1745
    if-eqz p2, :cond_0

    .line 1746
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->trackId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1750
    :pswitch_13
    const/4 v7, 0x1

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    .line 1751
    iget-object v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerId:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$ContainerId;->trackId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1754
    :pswitch_14
    const/4 v7, 0x0

    iput v7, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;->containerType:I

    goto/16 :goto_0

    .line 1650
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method private translateEntryPoint(Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)I
    .locals 4
    .param p1, "entryPoint"    # Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    .prologue
    const/4 v0, 0x0

    .line 1614
    if-nez p1, :cond_0

    .line 1631
    :goto_0
    :pswitch_0
    return v0

    .line 1618
    :cond_0
    sget-object v1, Lcom/google/android/music/eventlog/MusicEventLogger$8;->$SwitchMap$com$google$android$music$tutorial$TutorialUtils$EntryPoint:[I

    invoke-virtual {p1}, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1630
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to translate unknown entry point: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1620
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1622
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1624
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1626
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 1618
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private translateLocalCopyType(I)I
    .locals 3
    .param p1, "localCopyType"    # I

    .prologue
    .line 1484
    sparse-switch p1, :sswitch_data_0

    .line 1494
    const-string v0, "MusicLogger"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempting to translate unknown local copy type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1486
    :sswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 1488
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1490
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1492
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1484
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_2
        0x12c -> :sswitch_3
    .end sparse-switch
.end method

.method private translateMainstageReason(I)I
    .locals 4
    .param p1, "reason"    # I

    .prologue
    const/4 v0, 0x0

    .line 1369
    sparse-switch p1, :sswitch_data_0

    .line 1386
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to translate unknown mainstage reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    :goto_0
    :sswitch_0
    return v0

    .line 1371
    :sswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 1373
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1375
    :sswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1377
    :sswitch_4
    const/4 v0, 0x1

    goto :goto_0

    .line 1380
    :sswitch_5
    const/4 v0, 0x4

    goto :goto_0

    .line 1382
    :sswitch_6
    const/16 v0, 0x8

    goto :goto_0

    .line 1369
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x64 -> :sswitch_5
    .end sparse-switch
.end method

.method private translateNavbarSection(Lcom/google/android/music/ui/HomeActivity$Screen;)I
    .locals 4
    .param p1, "screen"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    const/4 v0, 0x0

    .line 1792
    if-nez p1, :cond_0

    .line 1793
    const-string v1, "MusicLogger"

    const-string v2, "null navbar section"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 1820
    :goto_0
    :pswitch_0
    return v0

    .line 1796
    :cond_0
    sget-object v1, Lcom/google/android/music/eventlog/MusicEventLogger$8;->$SwitchMap$com$google$android$music$ui$HomeActivity$Screen:[I

    invoke-virtual {p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1819
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown navbar section: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1798
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1800
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1802
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1804
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 1806
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 1808
    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    .line 1810
    :pswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 1796
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private translateNetworkSubtype(I)I
    .locals 4
    .param p1, "networkSubtype"    # I

    .prologue
    const/4 v0, 0x0

    .line 1541
    packed-switch p1, :pswitch_data_0

    .line 1575
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to translate unknown network type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1577
    :goto_0
    :pswitch_0
    return v0

    .line 1543
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1545
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1547
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1549
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 1551
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 1553
    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 1555
    :pswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 1557
    :pswitch_8
    const/16 v0, 0x8

    goto :goto_0

    .line 1559
    :pswitch_9
    const/16 v0, 0x9

    goto :goto_0

    .line 1561
    :pswitch_a
    const/16 v0, 0xa

    goto :goto_0

    .line 1563
    :pswitch_b
    const/16 v0, 0xb

    goto :goto_0

    .line 1565
    :pswitch_c
    const/16 v0, 0xc

    goto :goto_0

    .line 1567
    :pswitch_d
    const/16 v0, 0xd

    goto :goto_0

    .line 1569
    :pswitch_e
    const/16 v0, 0xe

    goto :goto_0

    .line 1571
    :pswitch_f
    const/16 v0, 0xf

    goto :goto_0

    .line 1541
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_8
        :pswitch_3
        :pswitch_f
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_9
        :pswitch_c
        :pswitch_a
        :pswitch_d
        :pswitch_7
        :pswitch_e
        :pswitch_4
        :pswitch_b
    .end packed-switch
.end method

.method private translateNetworkType(I)I
    .locals 3
    .param p1, "networkType"    # I

    .prologue
    .line 1506
    packed-switch p1, :pswitch_data_0

    .line 1528
    const-string v0, "MusicLogger"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempting to translate unknown network type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1530
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1508
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1510
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1512
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1514
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1516
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 1518
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 1520
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 1522
    :pswitch_7
    const/16 v0, 0x8

    goto :goto_0

    .line 1524
    :pswitch_8
    const/16 v0, 0x9

    goto :goto_0

    .line 1526
    :pswitch_9
    const/16 v0, 0xa

    goto :goto_0

    .line 1506
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private translatePlaylistType(I)I
    .locals 1
    .param p1, "docPlaylistType"    # I

    .prologue
    .line 1400
    sparse-switch p1, :sswitch_data_0

    .line 1417
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1402
    :sswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 1404
    :sswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 1406
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1400
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x46 -> :sswitch_1
        0x47 -> :sswitch_0
    .end sparse-switch
.end method

.method private translateRadioSeed(ILjava/lang/String;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;
    .locals 4
    .param p1, "seedType"    # I
    .param p2, "seedId"    # Ljava/lang/String;

    .prologue
    .line 1432
    new-instance v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;-><init>()V

    .line 1436
    .local v0, "seed":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;
    invoke-static {p1}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->fromSchemaValue(I)Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getRemoteValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->seedType:Ljava/lang/Integer;

    .line 1440
    packed-switch p1, :pswitch_data_0

    .line 1470
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to translate unknown seed type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1473
    :goto_0
    :pswitch_0
    return-object v0

    .line 1442
    :pswitch_1
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->albumMetajamId:Ljava/lang/String;

    goto :goto_0

    .line 1445
    :pswitch_2
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->artistMetajamId:Ljava/lang/String;

    goto :goto_0

    .line 1448
    :pswitch_3
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->artistMetajamId:Ljava/lang/String;

    goto :goto_0

    .line 1451
    :pswitch_4
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->genreId:Ljava/lang/String;

    goto :goto_0

    .line 1454
    :pswitch_5
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->trackLockerId:Ljava/lang/String;

    goto :goto_0

    .line 1457
    :pswitch_6
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->trackMetajamId:Ljava/lang/String;

    goto :goto_0

    .line 1460
    :pswitch_7
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->playlistId:Ljava/lang/String;

    goto :goto_0

    .line 1463
    :pswitch_8
    iput-object p2, v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$RadioSeed;->curatedStationId:Ljava/lang/String;

    goto :goto_0

    .line 1440
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private translateRating(I)I
    .locals 3
    .param p1, "incomingRating"    # I

    .prologue
    .line 1769
    packed-switch p1, :pswitch_data_0

    .line 1781
    const-string v0, "MusicLogger"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got unsupported rating="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; setting to UNKNOWN"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1782
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1771
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1773
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1775
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1777
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1779
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 1769
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private translateSignupPage(Ljava/lang/String;)I
    .locals 3
    .param p1, "signupPage"    # Ljava/lang/String;

    .prologue
    .line 1589
    const-string v0, "signUpSelectAccount"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1590
    const/4 v0, 0x1

    .line 1601
    :goto_0
    return v0

    .line 1591
    :cond_0
    const-string v0, "signUpTryNautilus"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1592
    const/4 v0, 0x2

    goto :goto_0

    .line 1593
    :cond_1
    const-string v0, "signUpInAppBilling"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1594
    const/4 v0, 0x5

    goto :goto_0

    .line 1595
    :cond_2
    const-string v0, "signUpConfirmNautilus"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1596
    const/4 v0, 0x3

    goto :goto_0

    .line 1597
    :cond_3
    const-string v0, "signUpAddMusic"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1598
    const/4 v0, 0x4

    goto :goto_0

    .line 1600
    :cond_4
    const-string v0, "MusicLogger"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempting to translate unknown signup page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1601
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public logArtDownload(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 974
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 975
    const-string v1, "MusicLogger"

    const-string v2, "logArtDownload"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 986
    :goto_0
    return-void

    .line 981
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 982
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 983
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 984
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->initialHttpUrl:Ljava/lang/String;

    .line 985
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadFinalStreamingUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "remoteId"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 795
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 796
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadFinalStreamingUrl"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 808
    :goto_0
    return-void

    .line 802
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 803
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 804
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 805
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 806
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->finalHttpUrl:Ljava/lang/String;

    .line 807
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadFinishedSize(Ljava/lang/String;J)V
    .locals 4
    .param p1, "remoteId"    # Ljava/lang/String;
    .param p2, "size"    # J

    .prologue
    .line 817
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 818
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadFinishedSize"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 830
    :goto_0
    return-void

    .line 824
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 825
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 826
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 827
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 828
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSizeBytes:Ljava/lang/Long;

    .line 829
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadFirstBufferReceived(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJJII)V
    .locals 3
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "remoteId"    # Ljava/lang/String;
    .param p3, "owner"    # Lcom/google/android/music/download/TrackOwner;
    .param p4, "priority"    # I
    .param p5, "seekMs"    # J
    .param p7, "latency"    # J
    .param p9, "networkType"    # I
    .param p10, "networkSubtype"    # I

    .prologue
    .line 721
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 722
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadFirstBufferReceived"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 740
    :goto_0
    return-void

    .line 728
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 729
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 730
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 731
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    .line 732
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 733
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p3}, Lcom/google/android/music/download/TrackOwner;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    .line 734
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    .line 735
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    .line 736
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->firstByteLatencyMillis:Ljava/lang/Long;

    .line 737
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p9}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkType(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    .line 738
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p10}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkSubtype(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    .line 739
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadHttpError(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJIII)V
    .locals 3
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "remoteId"    # Ljava/lang/String;
    .param p3, "owner"    # Lcom/google/android/music/download/TrackOwner;
    .param p4, "priority"    # I
    .param p5, "seekMs"    # J
    .param p7, "httpError"    # I
    .param p8, "networkType"    # I
    .param p9, "networkSubtype"    # I

    .prologue
    .line 947
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 948
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadHttpError"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 966
    :goto_0
    return-void

    .line 954
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 955
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 956
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 957
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    .line 958
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 959
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p3}, Lcom/google/android/music/download/TrackOwner;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    .line 960
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    .line 961
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    .line 962
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpResponseCode:Ljava/lang/Integer;

    .line 963
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p8}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkType(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    .line 964
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p9}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkSubtype(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    .line 965
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "remoteId"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "rangeHeaderValue"    # Ljava/lang/String;

    .prologue
    .line 750
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 751
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadHttpRequest"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 764
    :goto_0
    return-void

    .line 757
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 758
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 759
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 760
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 761
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->initialHttpUrl:Ljava/lang/String;

    .line 762
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p3, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpRangeHeaderValue:Ljava/lang/String;

    .line 763
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadHttpResponse(Ljava/lang/String;I)V
    .locals 3
    .param p1, "remoteId"    # Ljava/lang/String;
    .param p2, "responseCode"    # I

    .prologue
    .line 773
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 774
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadHttpResponse"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 786
    :goto_0
    return-void

    .line 780
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 781
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 782
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 783
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 784
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->httpResponseCode:Ljava/lang/Integer;

    .line 785
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadIOException(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJII)V
    .locals 3
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "remoteId"    # Ljava/lang/String;
    .param p3, "owner"    # Lcom/google/android/music/download/TrackOwner;
    .param p4, "priority"    # I
    .param p5, "seekMs"    # J
    .param p7, "networkType"    # I
    .param p8, "networkSubtype"    # I

    .prologue
    .line 880
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 881
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadIOException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 898
    :goto_0
    return-void

    .line 887
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 888
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 889
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 890
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    .line 891
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 892
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p3}, Lcom/google/android/music/download/TrackOwner;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    .line 893
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    .line 894
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    .line 895
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p7}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkType(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    .line 896
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p8}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkSubtype(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    .line 897
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadServiceUnavailable(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJII)V
    .locals 3
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "remoteId"    # Ljava/lang/String;
    .param p3, "owner"    # Lcom/google/android/music/download/TrackOwner;
    .param p4, "priority"    # I
    .param p5, "seekMs"    # J
    .param p7, "networkType"    # I
    .param p8, "networkSubtype"    # I

    .prologue
    .line 913
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 914
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadServiceUnavailable"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 931
    :goto_0
    return-void

    .line 920
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 921
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 922
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 923
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    .line 924
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 925
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p3}, Lcom/google/android/music/download/TrackOwner;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    .line 926
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    .line 927
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    .line 928
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p7}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkType(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    .line 929
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p8}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkSubtype(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    .line 930
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logDownloadSucceeded(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Lcom/google/android/music/download/TrackOwner;IJJII)V
    .locals 3
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "remoteId"    # Ljava/lang/String;
    .param p3, "owner"    # Lcom/google/android/music/download/TrackOwner;
    .param p4, "priority"    # I
    .param p5, "seekMs"    # J
    .param p7, "latency"    # J
    .param p9, "networkType"    # I
    .param p10, "networkSubtype"    # I

    .prologue
    .line 846
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 847
    const-string v1, "MusicLogger"

    const-string v2, "logDownloadSucceeded"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 865
    :goto_0
    return-void

    .line 853
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 854
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    .line 855
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadContentType:I

    .line 856
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackId:Ljava/lang/String;

    .line 857
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    iput-object p2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->trackRemoteId:Ljava/lang/String;

    .line 858
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-virtual {p3}, Lcom/google/android/music/download/TrackOwner;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadOwner:Ljava/lang/String;

    .line 859
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadPriority:Ljava/lang/Integer;

    .line 860
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->downloadSeekMillis:Ljava/lang/Long;

    .line 861
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->completeDownloadLatencyMillis:Ljava/lang/Long;

    .line 862
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p9}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkType(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkType:I

    .line 863
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->downloadEvent:Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;

    invoke-direct {p0, p10}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNetworkSubtype(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$DownloadEventInfo;->networkSubtype:I

    .line 864
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logMainstageCardClicked(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 5
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 282
    sget-boolean v2, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 283
    const-string v2, "MusicLogger"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "logMainstageCardClicked: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v2, :cond_2

    .line 301
    :cond_1
    :goto_0
    return-void

    .line 289
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->convertMainstageDocument(Lcom/google/android/music/ui/cardlib/model/Document;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;

    move-result-object v0

    .line 290
    .local v0, "card":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    if-eqz v0, :cond_1

    .line 294
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 295
    .local v1, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 296
    iget-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    const/16 v3, 0xb

    iput v3, v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 297
    iget-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    new-instance v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    invoke-direct {v3}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;-><init>()V

    iput-object v3, v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    .line 298
    iget-object v2, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    iget-object v2, v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;

    iput-object v0, v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardClickedInfo;->card:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;

    .line 299
    invoke-direct {p0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logMainstageDocumentDisplayed(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 1
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mMainstageImpressions:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->insertDocument(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 643
    return-void
.end method

.method logMainstageImpressions(Ljava/util/Collection;Z)V
    .locals 9
    .param p2, "continuous"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 654
    .local p1, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-boolean v6, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v6, :cond_0

    .line 655
    const-string v6, "MusicLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "logMainstageImpressions: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v6, :cond_1

    .line 682
    :goto_0
    return-void

    .line 661
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 662
    .local v2, "cardsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 663
    .local v3, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-direct {p0, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->convertMainstageDocument(Lcom/google/android/music/ui/cardlib/model/Document;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;

    move-result-object v0

    .line 664
    .local v0, "card":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    if-eqz v0, :cond_3

    .line 665
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 667
    :cond_3
    sget-boolean v6, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v6, :cond_2

    .line 668
    const-string v6, "MusicLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Couldn\'t convert doc to impression: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 672
    .end local v0    # "card":Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    .end local v3    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    new-array v1, v6, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;

    .line 673
    .local v1, "cards":[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;
    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 675
    new-instance v4, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v4}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 676
    .local v4, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v6}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v6, v4, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 677
    iget-object v6, v4, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    const/4 v7, 0x5

    iput v7, v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 678
    iget-object v6, v4, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    new-instance v7, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    invoke-direct {v7}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;-><init>()V

    iput-object v7, v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    .line 679
    iget-object v6, v4, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    iget-object v6, v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    iput-object v1, v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;->card:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Card;

    .line 680
    iget-object v6, v4, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    iget-object v6, v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->listenNowCardsFetchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$ListenNowCardsFetchInfo;->continuous:Ljava/lang/Boolean;

    .line 681
    invoke-direct {p0, v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logMusicAppLink(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 994
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 995
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logMusicAppLink: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 1007
    :goto_0
    return-void

    .line 1001
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1002
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 1003
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 1004
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    .line 1005
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    iget-object v1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->appStartedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$AppStartedInfo;->url:Ljava/lang/String;

    .line 1006
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logNavBarItemClicked(Lcom/google/android/music/ui/HomeActivity$Screen;)V
    .locals 4
    .param p1, "screen"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 1227
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1228
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logNavBarItemClicked: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 1241
    :goto_0
    return-void

    .line 1234
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1235
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 1236
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    const/4 v2, 0x3

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 1237
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    .line 1239
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    iget-object v1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navbarItemClickedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;

    invoke-direct {p0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateNavbarSection(Lcom/google/android/music/ui/HomeActivity$Screen;)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$NavbarItemClickedInfo;->section:I

    .line 1240
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logNewMainstageSession()V
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mMainstageImpressions:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    invoke-virtual {v0}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->newSession()V

    .line 634
    return-void
.end method

.method public logPlayEvent(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V
    .locals 7
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "isRemote"    # Z
    .param p4, "isExplicit"    # Z
    .param p5, "position"    # J
    .param p7, "duration"    # J

    .prologue
    .line 337
    sget-boolean v3, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 338
    const-string v3, "MusicLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "logPlayEvent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v3, :cond_1

    if-eqz p2, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v3, p5, v4

    if-nez v3, :cond_2

    .line 368
    :cond_1
    :goto_0
    return-void

    .line 344
    :cond_2
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 345
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-direct {v3}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;-><init>()V

    iput-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 347
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v1

    .line 348
    .local v1, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v2

    .line 349
    .local v2, "trackSourceId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 350
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v1, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackMetajamId:Ljava/lang/String;

    .line 352
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->isSourceTypeLocker()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 353
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v2, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackLockerId:Ljava/lang/String;

    .line 356
    :cond_4
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v4, 0x5

    iput v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->audioPlayer:I

    .line 357
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->isRemote:Ljava/lang/Boolean;

    .line 358
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v4, 0x1

    iput v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playerEventType:I

    .line 359
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    new-instance v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    invoke-direct {v4}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;-><init>()V

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    .line 360
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->explicitPlay:Ljava/lang/Boolean;

    .line 361
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getLocalCopyType()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateLocalCopyType(I)I

    move-result v4

    iput v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->storage:I

    .line 362
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    invoke-direct {p0, p1, p2}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 364
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    const-wide/16 v4, 0x3e8

    div-long v4, p5, v4

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->trackLengthPlayedSec:Ljava/lang/Integer;

    .line 365
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    const-wide/16 v4, 0x3e8

    div-long v4, p7, v4

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->totalTrackLengthSec:Ljava/lang/Integer;

    .line 367
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto/16 :goto_0
.end method

.method public logPlayEventAsync(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V
    .locals 11
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "isRemote"    # Z
    .param p4, "isExplicit"    # Z
    .param p5, "position"    # J
    .param p7, "duration"    # J

    .prologue
    .line 316
    new-instance v0, Lcom/google/android/music/eventlog/MusicEventLogger$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger$2;-><init>(Lcom/google/android/music/eventlog/MusicEventLogger;Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 323
    return-void
.end method

.method public logRefreshRadioAsync(Lcom/google/android/music/store/ContainerDescriptor;)V
    .locals 1
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    .line 1130
    new-instance v0, Lcom/google/android/music/eventlog/MusicEventLogger$7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger$7;-><init>(Lcom/google/android/music/eventlog/MusicEventLogger;Lcom/google/android/music/store/ContainerDescriptor;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 1136
    return-void
.end method

.method public logRemoveFromQueue(JLcom/google/android/music/store/ContainerDescriptor;)V
    .locals 7
    .param p1, "musicId"    # J
    .param p3, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    .line 1171
    sget-boolean v3, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 1172
    const-string v3, "MusicLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "logRemoveFromQueue: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v3, :cond_1

    .line 1198
    :goto_0
    return-void

    .line 1178
    :cond_1
    const/4 v2, 0x0

    .line 1180
    .local v2, "musicFile":Lcom/google/android/music/store/MusicFile;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4, p1, p2}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1186
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1187
    .local v1, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    invoke-direct {v3}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;-><init>()V

    iput-object v3, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    .line 1189
    iget-object v3, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    const/4 v4, 0x1

    iput v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->queueEventType:I

    .line 1190
    iget-object v3, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    new-instance v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    invoke-direct {v4}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;-><init>()V

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    .line 1191
    iget-object v3, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    invoke-direct {p0, p3, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1193
    if-eqz v2, :cond_2

    .line 1194
    iget-object v3, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->queueEventInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;->singleTrackRemovedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$SingleTrackRemovedInfo;->removedTrackId:Ljava/lang/String;

    .line 1197
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0

    .line 1181
    .end local v1    # "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    :catch_0
    move-exception v0

    .line 1182
    .local v0, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v3, "MusicLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t get MusicFile for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public logSearchQuery(Ljava/lang/String;)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 690
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 691
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logSearchQuery: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 703
    :cond_1
    :goto_0
    return-void

    .line 697
    :cond_2
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 698
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 699
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    const/16 v2, 0x9

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 700
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    .line 701
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    iget-object v1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->searchInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$SearchInfo;->searchQuery:Ljava/lang/String;

    .line 702
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logSeekEventAsync(ZJJJ)V
    .locals 10
    .param p1, "isRemote"    # Z
    .param p2, "musicId"    # J
    .param p4, "curPosition"    # J
    .param p6, "seekPosition"    # J

    .prologue
    .line 572
    new-instance v1, Lcom/google/android/music/eventlog/MusicEventLogger$5;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger$5;-><init>(Lcom/google/android/music/eventlog/MusicEventLogger;ZJJJ)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 578
    return-void
.end method

.method public logSignupFlowPage(Ljava/lang/String;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)V
    .locals 4
    .param p1, "signupPage"    # Ljava/lang/String;
    .param p2, "entryPoint"    # Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    .prologue
    .line 1033
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1034
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logSignupFlowPage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 1049
    :goto_0
    return-void

    .line 1040
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1041
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    .line 1042
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;->signupEventType:I

    .line 1043
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;->deviceSignupPageViewInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;

    .line 1044
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    iget-object v1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;->deviceSignupPageViewInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;

    invoke-direct {p0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateSignupPage(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->deviceSignupStep:I

    .line 1046
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->signupEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;

    iget-object v1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo;->deviceSignupPageViewInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;

    invoke-direct {p0, p2}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateEntryPoint(Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$SignupEventInfo$DeviceSignupPageViewInfo;->signupFlowEntryPoint:I

    .line 1048
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logSkipForwardEventAsync(ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V
    .locals 11
    .param p1, "isRemote"    # Z
    .param p2, "musicId"    # J
    .param p4, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p5, "curPosition"    # J
    .param p7, "lastUserExplicitPlayTime"    # J

    .prologue
    .line 500
    new-instance v0, Lcom/google/android/music/eventlog/MusicEventLogger$4;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger$4;-><init>(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 507
    return-void
.end method

.method public logSongRating(ILcom/google/android/music/store/ContainerDescriptor;JZ)V
    .locals 9
    .param p1, "rating"    # I
    .param p2, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p3, "musicId"    # J
    .param p5, "isRemote"    # Z

    .prologue
    .line 1081
    sget-boolean v5, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 1082
    const-string v5, "MusicLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "logSongRating: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v5, :cond_2

    .line 1122
    :cond_1
    :goto_0
    return-void

    .line 1088
    :cond_2
    const/4 v2, 0x0

    .line 1090
    .local v2, "musicFile":Lcom/google/android/music/store/MusicFile;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6, p3, p4}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1099
    new-instance v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1101
    .local v1, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-direct {v5}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;-><init>()V

    iput-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 1102
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v6, 0x6

    iput v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playerEventType:I

    .line 1103
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    new-instance v6, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$RatingInfo;

    invoke-direct {v6}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$RatingInfo;-><init>()V

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->ratingInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$RatingInfo;

    .line 1104
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v5, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->ratingInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$RatingInfo;

    invoke-direct {p0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateRating(I)I

    move-result v6

    iput v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$RatingInfo;->rating:I

    .line 1105
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v5, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->ratingInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$RatingInfo;

    invoke-direct {p0, p2, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$RatingInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1108
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v3

    .line 1109
    .local v3, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v4

    .line 1110
    .local v4, "trackSourceId":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1111
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v3, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackMetajamId:Ljava/lang/String;

    .line 1113
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->isSourceTypeLocker()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1114
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v4, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackLockerId:Ljava/lang/String;

    .line 1117
    :cond_4
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v6, 0x5

    iput v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->audioPlayer:I

    .line 1118
    iget-object v5, v1, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->isRemote:Ljava/lang/Boolean;

    .line 1121
    invoke-direct {p0, v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0

    .line 1091
    .end local v1    # "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    .end local v3    # "trackMetajamId":Ljava/lang/String;
    .end local v4    # "trackSourceId":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1092
    .local v0, "e":Lcom/google/android/music/store/DataNotFoundException;
    sget-boolean v5, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v5, :cond_1

    .line 1093
    const-string v5, "MusicLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to get MusicFile to log rating="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for musicId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public logStopEventAsync(ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V
    .locals 9
    .param p1, "isRemote"    # Z
    .param p2, "musicId"    # J
    .param p4, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p5, "lastUserExplicitPlayTime"    # J
    .param p7, "explicit"    # Z

    .prologue
    .line 426
    new-instance v0, Lcom/google/android/music/eventlog/MusicEventLogger$3;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-wide v6, p5

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/eventlog/MusicEventLogger$3;-><init>(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 433
    return-void
.end method

.method public logUIStarted()V
    .locals 3

    .prologue
    .line 1013
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1014
    const-string v1, "MusicLogger"

    const-string v2, "logUIStarted"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 1024
    :goto_0
    return-void

    .line 1020
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1021
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 1022
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 1023
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method

.method public logWearPlayEvent(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZJ)V
    .locals 6
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "isExplicit"    # Z
    .param p4, "eventTimeMs"    # J

    .prologue
    .line 382
    sget-boolean v3, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 383
    const-string v3, "MusicLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "logWearPlayEvent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v3, :cond_1

    if-nez p2, :cond_2

    .line 412
    :cond_1
    :goto_0
    return-void

    .line 389
    :cond_2
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 390
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    invoke-direct {v3}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;-><init>()V

    iput-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    .line 392
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v1

    .line 393
    .local v1, "trackMetajamId":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v2

    .line 394
    .local v2, "trackSourceId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 395
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v1, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackMetajamId:Ljava/lang/String;

    .line 397
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->isSourceTypeLocker()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 398
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iput-object v2, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->trackLockerId:Ljava/lang/String;

    .line 401
    :cond_4
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/16 v4, 0xe

    iput v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->audioPlayer:I

    .line 403
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->isRemote:Ljava/lang/Boolean;

    .line 404
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    const/4 v4, 0x1

    iput v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playerEventType:I

    .line 405
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    new-instance v4, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    invoke-direct {v4}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;-><init>()V

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    .line 406
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->explicitPlay:Ljava/lang/Boolean;

    .line 408
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    const/4 v4, 0x2

    iput v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->storage:I

    .line 409
    iget-object v3, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->playerEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;

    iget-object v3, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo;->playInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;

    invoke-direct {p0, p1, p2}, Lcom/google/android/music/eventlog/MusicEventLogger;->translateContainerDescriptor(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$PlayerEventInfo$PlayInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 411
    invoke-direct {p0, v0, p4, p5}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;J)V

    goto :goto_0
.end method

.method public logYouTubeVideoViewed(Ljava/lang/String;)V
    .locals 4
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 1206
    sget-boolean v1, Lcom/google/android/music/eventlog/MusicEventLogger;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1207
    const-string v1, "MusicLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logYouTubeVideoViewed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1209
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_1

    .line 1219
    :goto_0
    return-void

    .line 1213
    :cond_1
    new-instance v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;

    invoke-direct {v0}, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;-><init>()V

    .line 1214
    .local v0, "event":Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    .line 1215
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    const/16 v2, 0xc

    iput v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->navigationEventType:I

    .line 1216
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    new-instance v2, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    invoke-direct {v2}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    .line 1217
    iget-object v1, v0, Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;->navigationEvent:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;

    iget-object v1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo;->youtubeVideoWatchedInfo:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;

    iput-object p1, v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$NavigationEventInfo$YouTubeVideoWatchedInfo;->videoId:Ljava/lang/String;

    .line 1218
    invoke-direct {p0, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logEvent(Lcom/google/android/music/eventlogprotos/MusicClientEventProto$MusicClientEvent;)V

    goto :goto_0
.end method
