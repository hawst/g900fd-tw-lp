.class Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;
.super Landroid/support/v7/media/MediaRouter$ControlRequestCallback;
.source "RemoteAsyncMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestCastV2RemoteSyncStatusOnMainThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 933
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error encountered requesting cast v2 sync status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V

    .line 948
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 949
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V

    .line 950
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 936
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$1800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/CastSessionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/cast/CastSessionManager;->setCastV2ReceiverLoaded()V

    .line 937
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_RECEIVER_LOADED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-ne v1, v2, :cond_0

    .line 938
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 939
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 940
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;->this$0:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 942
    .end local v0    # "message":Landroid/os/Message;
    :cond_0
    return-void
.end method
