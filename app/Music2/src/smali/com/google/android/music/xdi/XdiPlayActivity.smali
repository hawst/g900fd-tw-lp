.class public Lcom/google/android/music/xdi/XdiPlayActivity;
.super Landroid/app/Activity;
.source "XdiPlayActivity.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

.field private mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/xdi/XdiPlayActivity;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlayActivity;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/xdi/XdiPlayActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlayActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiPlayActivity;->playMusic(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/music/xdi/XdiPlayActivity;Lcom/google/android/music/utils/MusicUtils$ServiceToken;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlayActivity;
    .param p1, "x1"    # Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    return-object p1
.end method

.method private playMusic(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 85
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlayActivity;->LOGV:Z

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "MusicXdi"

    const-string v1, "playMusic"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiPlayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper;->processIntent(Landroid/content/Intent;)V

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_1
    const-string v0, "MusicXdi"

    const-string v1, "Playback service not initialized."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlayActivity;->LOGV:Z

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "MusicXdi"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    new-instance v0, Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-direct {v0, p0}, Lcom/google/android/music/xdi/XdiPlayHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    .line 56
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    new-instance v1, Lcom/google/android/music/xdi/XdiPlayActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/xdi/XdiPlayActivity$1;-><init>(Lcom/google/android/music/xdi/XdiPlayActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper;->setCompletionListener(Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;)V

    .line 66
    new-instance v0, Lcom/google/android/music/xdi/XdiPlayActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/xdi/XdiPlayActivity$2;-><init>(Lcom/google/android/music/xdi/XdiPlayActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 82
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 97
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlayActivity;->LOGV:Z

    if-eqz v0, :cond_0

    .line 98
    const-string v0, "MusicXdi"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 102
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/XdiPlayHelper;->cleanup()V

    .line 104
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayActivity;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V

    .line 107
    :cond_1
    return-void
.end method
