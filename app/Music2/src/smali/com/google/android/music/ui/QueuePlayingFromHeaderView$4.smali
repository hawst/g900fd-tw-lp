.class Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;
.super Ljava/lang/Object;
.source "QueuePlayingFromHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->doLocalRadioArtUpdate(Lcom/google/android/music/art/ContainerArtRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mStationName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$request:Lcom/google/android/music/art/ContainerArtRequest;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Lcom/google/android/music/art/ContainerArtRequest;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iput-object p2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    iput-object p3, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 10

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->val$request:Lcom/google/android/music/art/ContainerArtRequest;

    invoke-virtual {v0}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v8

    .line 392
    .local v8, "localRadioId":J
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 393
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 395
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->val$appContext:Landroid/content/Context;

    # getter for: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->RADIO_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$600()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 396
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->mStationName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 402
    return-void

    .line 397
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 400
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 406
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    iget-object v2, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->mStationName:Ljava/lang/String;

    # invokes: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->getRadioTitleText(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$700(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 407
    .local v0, "stationLabel":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateInfo(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v0, v2}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->access$500(Lcom/google/android/music/ui/QueuePlayingFromHeaderView;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$4;->this$0:Lcom/google/android/music/ui/QueuePlayingFromHeaderView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView;->updateBackground()V

    .line 409
    return-void
.end method
