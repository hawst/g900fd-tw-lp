.class public Lcom/google/android/music/tutorial/GetAccountOffersTask;
.super Ljava/lang/Object;
.source "GetAccountOffersTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAccountDisabledCallbackRunnable:Ljava/lang/Runnable;

.field private mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

.field private final mBackgroundRunnable:Ljava/lang/Runnable;

.field private mCallbackHandler:Landroid/os/Handler;

.field private final mCallbackRunnable:Ljava/lang/Runnable;

.field private mCallbacks:Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;

.field private final mContext:Landroid/content/Context;

.field private final mCoupon:Ljava/lang/String;

.field private final mCouponType:Ljava/lang/String;

.field private volatile mIsCanceled:Z

.field private mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "callbacks"    # Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;
    .param p4, "coupon"    # Ljava/lang/String;
    .param p5, "couponType"    # Ljava/lang/String;

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mIsCanceled:Z

    .line 46
    new-instance v0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;-><init>(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundRunnable:Ljava/lang/Runnable;

    .line 122
    new-instance v0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;-><init>(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackRunnable:Ljava/lang/Runnable;

    .line 143
    new-instance v0, Lcom/google/android/music/tutorial/GetAccountOffersTask$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/GetAccountOffersTask$3;-><init>(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccountDisabledCallbackRunnable:Ljava/lang/Runnable;

    .line 167
    iput-object p1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;

    .line 168
    iput-object p2, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccount:Landroid/accounts/Account;

    .line 169
    iput-object p3, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbacks:Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;

    .line 170
    iput-object p4, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCoupon:Ljava/lang/String;

    .line 171
    iput-object p5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCouponType:Ljava/lang/String;

    .line 172
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mIsCanceled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->cleanup()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccountDisabledCallbackRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/music/tutorial/GetAccountOffersTask;Lcom/google/android/music/cloudclient/OffersResponseJson;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;
    .param p1, "x1"    # Lcom/google/android/music/cloudclient/OffersResponseJson;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCoupon:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCouponType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/GetAccountOffersTask;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbacks:Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;

    return-object v0
.end method

.method private cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 197
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v0}, Lcom/google/android/music/utils/LoggableHandler;->quit()V

    .line 199
    iput-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

    .line 204
    :cond_0
    iput-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbacks:Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;

    .line 208
    return-void
.end method


# virtual methods
.method cancel()V
    .locals 2

    .prologue
    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mIsCanceled:Z

    .line 187
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 193
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->cleanup()V

    .line 194
    return-void
.end method

.method run()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AccountSelectionTask can only run once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackHandler:Landroid/os/Handler;

    .line 180
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler;

    const-string v1, "AccountSelection"

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

    .line 181
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundHandler:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask;->mBackgroundRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 182
    return-void
.end method
