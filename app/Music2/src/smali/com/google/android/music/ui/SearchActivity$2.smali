.class Lcom/google/android/music/ui/SearchActivity$2;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SearchActivity;->handleSearchResult()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/SearchActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SearchActivity;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/music/ui/SearchActivity$2;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 126
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity$2;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-virtual {v2}, Lcom/google/android/music/ui/SearchActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 127
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity$2;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-virtual {v2}, Lcom/google/android/music/ui/SearchActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    instance-of v2, v0, Lcom/google/android/music/ui/SearchClustersFragment;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 128
    check-cast v1, Lcom/google/android/music/ui/SearchClustersFragment;

    .line 129
    .local v1, "searchFrag":Lcom/google/android/music/ui/SearchClustersFragment;
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity$2;->this$0:Lcom/google/android/music/ui/SearchActivity;

    # getter for: Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/SearchActivity;->access$200(Lcom/google/android/music/ui/SearchActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/SearchClustersFragment;->setSearchString(Ljava/lang/String;)V

    .line 130
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity$2;->this$0:Lcom/google/android/music/ui/SearchActivity;

    # getter for: Lcom/google/android/music/ui/SearchActivity;->mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;
    invoke-static {v2}, Lcom/google/android/music/ui/SearchActivity;->access$300(Lcom/google/android/music/ui/SearchActivity;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/SearchActivity$2;->this$0:Lcom/google/android/music/ui/SearchActivity;

    # getter for: Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/ui/SearchActivity;->access$200(Lcom/google/android/music/ui/SearchActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSearchQuery(Ljava/lang/String;)V

    .line 134
    .end local v1    # "searchFrag":Lcom/google/android/music/ui/SearchClustersFragment;
    :goto_0
    return-void

    .line 132
    :cond_0
    const-string v2, "SearchActivity"

    const-string v3, "SearchClustersFragment not set in content"

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
