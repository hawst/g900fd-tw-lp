.class public Lcom/google/android/music/download/artwork/ArtIdRecorder;
.super Ljava/lang/Object;
.source "ArtIdRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;,
        Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ArtId:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TArtId;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    .local p0, "this":Lcom/google/android/music/download/artwork/ArtIdRecorder;, "Lcom/google/android/music/download/artwork/ArtIdRecorder<TArtId;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    return-void
.end method


# virtual methods
.method public extractIds()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TArtId;>;"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/music/download/artwork/ArtIdRecorder;, "Lcom/google/android/music/download/artwork/ArtIdRecorder<TArtId;>;"
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtIdRecorder;->mIds:Ljava/util/Set;

    .line 30
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<TArtId;>;"
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtIdRecorder;->mIds:Ljava/util/Set;

    .line 31
    return-object v0
.end method

.method public report(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TArtId;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/music/download/artwork/ArtIdRecorder;, "Lcom/google/android/music/download/artwork/ArtIdRecorder<TArtId;>;"
    .local p1, "artId":Ljava/lang/Object;, "TArtId;"
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtIdRecorder;->mIds:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtIdRecorder;->mIds:Ljava/util/Set;

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtIdRecorder;->mIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/music/download/artwork/ArtIdRecorder;, "Lcom/google/android/music/download/artwork/ArtIdRecorder<TArtId;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtIdRecorder;->mIds:Ljava/util/Set;

    if-eqz v4, :cond_1

    .line 39
    const/4 v0, 0x1

    .line 40
    .local v0, "first":Z
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtIdRecorder;->mIds:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 41
    .local v2, "id":Ljava/lang/Object;, "TArtId;"
    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    .line 46
    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 44
    :cond_0
    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 49
    .end local v0    # "first":Z
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "id":Ljava/lang/Object;, "TArtId;"
    :cond_1
    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
