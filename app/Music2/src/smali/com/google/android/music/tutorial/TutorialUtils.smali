.class public Lcom/google/android/music/tutorial/TutorialUtils;
.super Ljava/lang/Object;
.source "TutorialUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;
    }
.end annotation


# direct methods
.method static disableTutorial(Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 1
    .param p0, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 450
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setTutorialViewed(Z)V

    .line 451
    return-void
.end method

.method public static externalLaunchTutorial(Landroid/app/Activity;I)Z
    .locals 5
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "requestCode"    # I

    .prologue
    const/4 v1, 0x1

    .line 260
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 261
    const-string v1, "Tutorial"

    const-string v2, "Skipping tutorial - no connectivity"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const/4 v1, 0x0

    .line 273
    :goto_0
    return v1

    .line 265
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 267
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "changeAccountOnly"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 269
    const-string v2, "returnToCaller"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 270
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    const-string v3, "signUpSelectAccount"

    sget-object v4, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->NEW_USER:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSignupFlowPage(Ljava/lang/String;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)V

    .line 272
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V
    .locals 2
    .param p0, "tutorialActivity"    # Landroid/app/Activity;
    .param p1, "forceNautilus"    # Z
    .param p2, "launchQuiz"    # Z
    .param p3, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 420
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setTutorialViewed(Z)V

    .line 421
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    .line 422
    .local v0, "logger":Lcom/google/android/music/eventlog/MusicEventLogger;
    invoke-static {p0, p1, p2}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->finishTutorialPermanently(Landroid/app/Activity;ZZ)V

    .line 424
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 425
    return-void
.end method

.method static finishTutorialTemporarily(Landroid/app/Activity;)V
    .locals 1
    .param p0, "tutorialActivity"    # Landroid/app/Activity;

    .prologue
    .line 438
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    .line 439
    .local v0, "logger":Lcom/google/android/music/eventlog/MusicEventLogger;
    invoke-static {p0}, Lcom/google/android/music/tutorial/TutorialFinishActivity;->finishTutorialTemporary(Landroid/app/Activity;)V

    .line 440
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 441
    return-void
.end method

.method static getEntryPointFromIntent(Landroid/content/Intent;)Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;
    .locals 3
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 472
    const-string v1, "entryPoint"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 473
    .local v0, "o":Ljava/io/Serializable;
    instance-of v1, v0, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    if-nez v1, :cond_0

    .line 474
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Signup flow EntryPoint was not set correctly."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 476
    :cond_0
    check-cast v0, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    .end local v0    # "o":Ljava/io/Serializable;
    return-object v0
.end method

.method public static isDatabaseUpdated(Lcom/google/android/music/preferences/MusicPreferences;)Z
    .locals 2
    .param p0, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 462
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getDatabaseVersion()I

    move-result v0

    invoke-static {}, Lcom/google/android/music/store/Store;->getDatabaseVersion()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPackageGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p1, "callingPackage"    # Ljava/lang/String;

    .prologue
    .line 488
    invoke-static {p0, p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isPackageGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/music/utils/PackageValidator;->isGoogleSignedGearHead(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static launchAccountChooserWithAccount(Landroid/app/Activity;ZLandroid/accounts/Account;)Z
    .locals 4
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "changeAccountOnly"    # Z
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 286
    invoke-static {p0, p1}, Lcom/google/android/music/tutorial/TutorialUtils;->prepareChangeAccountIntent(Landroid/app/Activity;Z)Landroid/content/Intent;

    move-result-object v0

    .line 287
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 288
    const/4 v1, 0x0

    .line 294
    :goto_0
    return v1

    .line 290
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    const-string v2, "signUpSelectAccount"

    sget-object v3, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->NEW_USER:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSignupFlowPage(Ljava/lang/String;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)V

    .line 292
    const-string v1, "accountToChangeTo"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 293
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 294
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)Z
    .locals 1
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "entryPoint"    # Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-static {p0, p1, v0, v0}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "entryPoint"    # Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;
    .param p2, "coupon"    # Ljava/lang/String;
    .param p3, "couponType"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 154
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "entryPoint"    # Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;
    .param p2, "coupon"    # Ljava/lang/String;
    .param p3, "couponType"    # Ljava/lang/String;
    .param p4, "metajamIdDestination"    # Ljava/lang/String;
    .param p5, "playlistShareTokenDestination"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 179
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 180
    const-string v1, "Tutorial"

    const-string v2, "Skipping tutorial - no connectivity"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v1, 0x0

    .line 205
    :goto_0
    return v1

    .line 184
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x14000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 186
    const-string v2, "forceTutorial"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 187
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 188
    const-string v2, "coupon"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 191
    const-string v2, "couponType"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    :cond_2
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 194
    const-string v2, "metajamIdDestination"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    :cond_3
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 197
    const-string v2, "playlistShareTokenDestination"

    invoke-virtual {v0, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    :cond_4
    const-string v2, "entryPoint"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 202
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 203
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public static launchTutorialOnDemandWithMetajamDestination(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;)Z
    .locals 6
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "entryPoint"    # Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;
    .param p2, "metajamIdDestination"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 114
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static launchTutorialOnDemandWithPlaylistDestination(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;)Z
    .locals 6
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "entryPoint"    # Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;
    .param p2, "playlistShareTokenDestination"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 134
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static launchTutorialOnStartupIfNeeded(Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;)Z
    .locals 4
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-virtual {p1}, Lcom/google/android/music/preferences/MusicPreferences;->wasTutorialViewed()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, Lcom/google/android/music/tutorial/TutorialUtils;->isDatabaseUpdated(Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v1

    .line 68
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 69
    const-string v2, "Tutorial"

    const-string v3, "No connectivity"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-static {p1}, Lcom/google/android/music/tutorial/TutorialUtils;->isDatabaseUpdated(Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 76
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 77
    const-string v1, "entryPoint"

    sget-object v2, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->NEW_USER:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 78
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 79
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 80
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static launchTutorialToChooseAccount(Landroid/app/Activity;Z)Z
    .locals 4
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "changeAccountOnly"    # Z

    .prologue
    .line 223
    invoke-static {p0, p1}, Lcom/google/android/music/tutorial/TutorialUtils;->prepareChangeAccountIntent(Landroid/app/Activity;Z)Landroid/content/Intent;

    move-result-object v0

    .line 224
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 225
    const/4 v1, 0x0

    .line 230
    :goto_0
    return v1

    .line 227
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    const-string v2, "signUpSelectAccount"

    sget-object v3, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->NEW_USER:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSignupFlowPage(Ljava/lang/String;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)V

    .line 229
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 230
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static launchTutorialToChooseAccountForResult(Landroid/app/Activity;ZI)Z
    .locals 4
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "changeAccountOnly"    # Z
    .param p2, "requestCode"    # I

    .prologue
    .line 241
    invoke-static {p0, p1}, Lcom/google/android/music/tutorial/TutorialUtils;->prepareChangeAccountIntent(Landroid/app/Activity;Z)Landroid/content/Intent;

    move-result-object v0

    .line 242
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 243
    const/4 v1, 0x0

    .line 248
    :goto_0
    return v1

    .line 245
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    const-string v2, "signUpSelectAccount"

    sget-object v3, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->NEW_USER:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSignupFlowPage(Ljava/lang/String;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)V

    .line 247
    invoke-virtual {p0, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 248
    const/4 v1, 0x1

    goto :goto_0
.end method

.method static openConfirmNautilusActivity(Landroid/app/Activity;)V
    .locals 2
    .param p0, "currentActivity"    # Landroid/app/Activity;

    .prologue
    .line 360
    const-class v0, Lcom/google/android/music/tutorial/TutorialConfirmNautilusActivity;

    const-string v1, "signUpConfirmNautilus"

    invoke-static {v0, v1, p0}, Lcom/google/android/music/tutorial/TutorialUtils;->openTutorialActivity(Ljava/lang/Class;Ljava/lang/String;Landroid/app/Activity;)V

    .line 362
    return-void
.end method

.method static openEnableNautilusActivity(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 2
    .param p0, "currectActivity"    # Landroid/app/Activity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "offerId"    # Ljava/lang/String;

    .prologue
    .line 366
    invoke-static {p0, p1, p2}, Lcom/google/android/music/tutorial/EnableNautilusActivity;->buildStartIntent(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 368
    .local v0, "purchase":Landroid/content/Intent;
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 370
    return-void
.end method

.method static openOtherWaysToPlayActivity(Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 2
    .param p0, "currentActivity"    # Landroid/app/Activity;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 354
    invoke-static {p1}, Lcom/google/android/music/tutorial/TutorialUtils;->disableTutorial(Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 355
    const-class v0, Lcom/google/android/music/tutorial/TutorialOtherWaysToPlayActivity;

    const-string v1, "signUpAddMusic"

    invoke-static {v0, v1, p0}, Lcom/google/android/music/tutorial/TutorialUtils;->openTutorialActivity(Ljava/lang/Class;Ljava/lang/String;Landroid/app/Activity;)V

    .line 357
    return-void
.end method

.method static openSelectAccountActivity(Landroid/app/Activity;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "currentActivity"    # Landroid/app/Activity;
    .param p1, "firstTimeTutorial"    # Z
    .param p2, "coupon"    # Ljava/lang/String;
    .param p3, "couponType"    # Ljava/lang/String;

    .prologue
    .line 344
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 345
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "firstTimeTutorial"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 347
    const-string v1, "coupon"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "couponType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    const-string v1, "signUpSelectAccount"

    invoke-static {v0, v1, p0}, Lcom/google/android/music/tutorial/TutorialUtils;->openTutorialActivity(Landroid/content/Intent;Ljava/lang/String;Landroid/app/Activity;)V

    .line 351
    return-void
.end method

.method private static openTryNautilusActivity(Lcom/google/android/music/cloudclient/OfferJson;ZLandroid/app/Activity;)V
    .locals 2
    .param p0, "json"    # Lcom/google/android/music/cloudclient/OfferJson;
    .param p1, "isYTAvailable"    # Z
    .param p2, "currentActivity"    # Landroid/app/Activity;

    .prologue
    .line 402
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 403
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, p1, v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->putOfferIntoIntent(Lcom/google/android/music/cloudclient/OfferJson;ZLandroid/content/Intent;)V

    .line 404
    const-string v1, "signUpTryNautilus"

    invoke-static {v0, v1, p2}, Lcom/google/android/music/tutorial/TutorialUtils;->openTutorialActivity(Landroid/content/Intent;Ljava/lang/String;Landroid/app/Activity;)V

    .line 406
    return-void
.end method

.method static openTryNautilusOrFinishTutorial(Lcom/google/android/music/cloudclient/OffersResponseJson;Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 4
    .param p0, "availableOffers"    # Lcom/google/android/music/cloudclient/OffersResponseJson;
    .param p1, "currentActivity"    # Landroid/app/Activity;
    .param p2, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 380
    const/4 v1, 0x0

    .line 381
    .local v1, "tryNautilus":Z
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/OffersResponseJson;->isSignedUpForNautilus()Z

    move-result v0

    .line 382
    .local v0, "gotNautilus":Z
    if-eqz v0, :cond_2

    .line 384
    sget-object v2, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    invoke-virtual {p2, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setTempNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 385
    invoke-virtual {p2}, Lcom/google/android/music/preferences/MusicPreferences;->updateNautilusTimestamp()V

    .line 394
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 395
    const/4 v2, 0x0

    invoke-static {p1, v0, v2, p2}, Lcom/google/android/music/tutorial/TutorialUtils;->finishTutorialPermanently(Landroid/app/Activity;ZZLcom/google/android/music/preferences/MusicPreferences;)V

    .line 398
    :cond_1
    return-void

    .line 386
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/OffersResponseJson;->canSignupForNautilus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 387
    invoke-static {p1, p2}, Lcom/google/android/music/purchase/Finsky;->isDirectPurchaseAvailable(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 388
    const/4 v1, 0x1

    .line 389
    invoke-virtual {p0}, Lcom/google/android/music/cloudclient/OffersResponseJson;->getDefaultOffer()Lcom/google/android/music/cloudclient/OfferJson;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/music/cloudclient/OffersResponseJson;->mIsVideoServiceAvailable:Z

    invoke-static {v2, v3, p1}, Lcom/google/android/music/tutorial/TutorialUtils;->openTryNautilusActivity(Lcom/google/android/music/cloudclient/OfferJson;ZLandroid/app/Activity;)V

    goto :goto_0
.end method

.method static openTutorialActivity(Landroid/content/Intent;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 6
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "eventLogPageName"    # Ljava/lang/String;
    .param p2, "currentActivity"    # Landroid/app/Activity;

    .prologue
    .line 324
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/tutorial/TutorialUtils;->getEntryPointFromIntent(Landroid/content/Intent;)Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    move-result-object v0

    .line 325
    .local v0, "entryPoint":Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "metajamIdDestination"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 327
    .local v2, "metajamIdDestination":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "playlistShareTokenDestination"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 329
    .local v3, "playlistShareTokenDestination":Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    .line 330
    .local v1, "logger":Lcom/google/android/music/eventlog/MusicEventLogger;
    invoke-virtual {v1, p1, v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->logSignupFlowPage(Ljava/lang/String;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)V

    .line 332
    const/high16 v4, 0x14000000

    invoke-virtual {p0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 333
    const-string v4, "entryPoint"

    invoke-virtual {p0, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 334
    const-string v4, "metajamIdDestination"

    invoke-virtual {p0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 335
    const-string v4, "playlistShareTokenDestination"

    invoke-virtual {p0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    invoke-virtual {p2, p0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 338
    invoke-virtual {p2}, Landroid/app/Activity;->finish()V

    .line 339
    return-void
.end method

.method static openTutorialActivity(Ljava/lang/Class;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 1
    .param p1, "eventLogPageName"    # Ljava/lang/String;
    .param p2, "currentActivity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 317
    .local p0, "newActivityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v0, p1, p2}, Lcom/google/android/music/tutorial/TutorialUtils;->openTutorialActivity(Landroid/content/Intent;Ljava/lang/String;Landroid/app/Activity;)V

    .line 319
    return-void
.end method

.method private static prepareChangeAccountIntent(Landroid/app/Activity;Z)Landroid/content/Intent;
    .locals 3
    .param p0, "hostActivity"    # Landroid/app/Activity;
    .param p1, "changeAccountsOnly"    # Z

    .prologue
    .line 300
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 301
    const-string v1, "Tutorial"

    const-string v2, "Skipping tutorial - no connectivity"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    const/4 v0, 0x0

    .line 311
    :goto_0
    return-object v0

    .line 305
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "firstTimeTutorial"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 307
    const-string v1, "changeAccountOnly"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 309
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 310
    const-string v1, "entryPoint"

    sget-object v2, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->NEW_USER:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method
