.class Lcom/google/android/music/xdi/RootItemsCursor$SearchCursor;
.super Landroid/database/MatrixCursor;
.source "RootItemsCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/xdi/RootItemsCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchCursor"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 143
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 145
    new-instance v1, Lcom/google/android/music/xdi/ProjectionMap;

    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    .line 147
    .local v1, "projectionMap":Lcom/google/android/music/xdi/ProjectionMap;
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    array-length v3, v3

    new-array v2, v3, [Ljava/lang/Object;

    .line 151
    .local v2, "values":[Ljava/lang/Object;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.xdi.action.SEARCH"

    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "search"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 155
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "display_mode"

    const/4 v4, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 157
    const-string v3, "_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    const-string v3, "image_uri"

    const v4, 0x7f02003a

    invoke-static {p1, v4}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    const-string v3, "intent_uri"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    invoke-virtual {p0, v2}, Lcom/google/android/music/xdi/RootItemsCursor$SearchCursor;->addRow([Ljava/lang/Object;)V

    .line 164
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/music/xdi/RootItemsCursor$1;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RootItemsCursor$SearchCursor;-><init>(Landroid/content/Context;)V

    return-void
.end method
