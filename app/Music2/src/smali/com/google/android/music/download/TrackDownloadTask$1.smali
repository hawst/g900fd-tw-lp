.class synthetic Lcom/google/android/music/download/TrackDownloadTask$1;
.super Ljava/lang/Object;
.source "TrackDownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/TrackDownloadTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$google$android$music$download$ServerRejectionException$RejectionReason:[I

.field static final synthetic $SwitchMap$com$google$android$music$download$TrackOwner:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 169
    invoke-static {}, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->values()[Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$ServerRejectionException$RejectionReason:[I

    :try_start_0
    sget-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$ServerRejectionException$RejectionReason:[I

    sget-object v1, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ANOTHER_STREAM_BEING_PLAYED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    invoke-virtual {v1}, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$ServerRejectionException$RejectionReason:[I

    sget-object v1, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->DEVICE_NOT_AUTHORIZED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    invoke-virtual {v1}, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$ServerRejectionException$RejectionReason:[I

    sget-object v1, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->STREAM_RATE_LIMIT_REACHED:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    invoke-virtual {v1}, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$ServerRejectionException$RejectionReason:[I

    sget-object v1, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->TRACK_NOT_IN_SUBSCRIPTION:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    invoke-virtual {v1}, Lcom/google/android/music/download/ServerRejectionException$RejectionReason;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 60
    :goto_3
    invoke-static {}, Lcom/google/android/music/download/TrackOwner;->values()[Lcom/google/android/music/download/TrackOwner;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$TrackOwner:[I

    :try_start_4
    sget-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$TrackOwner:[I

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->MUSIC_PLAYBACK:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$TrackOwner:[I

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->KEEPON:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, Lcom/google/android/music/download/TrackDownloadTask$1;->$SwitchMap$com$google$android$music$download$TrackOwner:[I

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->AUTOCACHE:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    .line 169
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_1

    :catch_6
    move-exception v0

    goto :goto_0
.end method
