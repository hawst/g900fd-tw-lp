.class public Lcom/google/android/music/store/NautilusContentCache;
.super Ljava/lang/Object;
.source "NautilusContentCache.java"


# static fields
.field private static final LOGV:Z

.field private static sInstance:Lcom/google/android/music/store/NautilusContentCache;


# instance fields
.field private mAlbumCache:Lcom/google/android/music/utils/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/music/cloudclient/AlbumJson;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mArtistCache:Lcom/google/android/music/utils/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/music/cloudclient/ArtistJson;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mGenericCache:Lcom/google/android/music/utils/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTrackCache:Lcom/google/android/music/utils/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/NautilusContentCache;->LOGV:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    .line 56
    iget-object v3, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 58
    .local v2, "resolver":Landroid/content/ContentResolver;
    const-string v3, "music_memory_cache_capacity"

    const/16 v4, 0xc8

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 62
    .local v0, "cacheCapacity":I
    const-string v3, "music_memory_cache_capacity_small"

    const/16 v4, 0x14

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 66
    .local v1, "cacheCapacitySmall":I
    new-instance v3, Lcom/google/android/music/utils/LruCache;

    invoke-direct {v3, v0}, Lcom/google/android/music/utils/LruCache;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/music/store/NautilusContentCache;->mArtistCache:Lcom/google/android/music/utils/LruCache;

    .line 67
    new-instance v3, Lcom/google/android/music/utils/LruCache;

    invoke-direct {v3, v0}, Lcom/google/android/music/utils/LruCache;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/music/store/NautilusContentCache;->mAlbumCache:Lcom/google/android/music/utils/LruCache;

    .line 68
    new-instance v3, Lcom/google/android/music/utils/LruCache;

    invoke-direct {v3, v0}, Lcom/google/android/music/utils/LruCache;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/music/store/NautilusContentCache;->mTrackCache:Lcom/google/android/music/utils/LruCache;

    .line 69
    new-instance v3, Lcom/google/android/music/utils/LruCache;

    invoke-direct {v3, v1}, Lcom/google/android/music/utils/LruCache;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    .line 70
    return-void
.end method

.method private getArtistArtUrlCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 436
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "artistArtUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getExploreCacheKey(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "tabType"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 428
    const-string v0, "%s:%s:%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "explore"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getGenreCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 441
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "genre"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    .end local p1    # "parentGenreId":Ljava/lang/String;
    :goto_0
    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .restart local p1    # "parentGenreId":Ljava/lang/String;
    :cond_0
    const-string p1, "ROOT"

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/music/store/NautilusContentCache;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    const-class v1, Lcom/google/android/music/store/NautilusContentCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/store/NautilusContentCache;->sInstance:Lcom/google/android/music/store/NautilusContentCache;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/google/android/music/store/NautilusContentCache;

    invoke-direct {v0, p0}, Lcom/google/android/music/store/NautilusContentCache;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/music/store/NautilusContentCache;->sInstance:Lcom/google/android/music/store/NautilusContentCache;

    .line 79
    :cond_0
    sget-object v0, Lcom/google/android/music/store/NautilusContentCache;->sInstance:Lcom/google/android/music/store/NautilusContentCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;
    .locals 9
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "ttlMs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/music/utils/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<TT;",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/lang/String;",
            "J)TT;"
        }
    .end annotation

    .prologue
    .local p1, "cache":Lcom/google/android/music/utils/LruCache;, "Lcom/google/android/music/utils/LruCache<Ljava/lang/String;Landroid/util/Pair<TT;Ljava/lang/Long;>;>;"
    const/4 v2, 0x0

    .line 408
    monitor-enter p1

    .line 409
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/google/android/music/utils/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 410
    .local v0, "objectAndTtlMs":Landroid/util/Pair;, "Landroid/util/Pair<TT;Ljava/lang/Long;>;"
    if-nez v0, :cond_0

    .line 411
    monitor-exit p1

    move-object v1, v2

    .line 420
    :goto_0
    return-object v1

    .line 413
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v1, p3, v4

    if-eqz v1, :cond_1

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, p3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 414
    :cond_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 415
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    monitor-exit p1

    goto :goto_0

    .line 419
    .end local v0    # "objectAndTtlMs":Landroid/util/Pair;, "Landroid/util/Pair<TT;Ljava/lang/Long;>;"
    :catchall_0
    move-exception v1

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 418
    .restart local v0    # "objectAndTtlMs":Landroid/util/Pair;, "Landroid/util/Pair<TT;Ljava/lang/Long;>;"
    :cond_2
    :try_start_1
    invoke-virtual {p1, p2}, Lcom/google/android/music/utils/LruCache;->remove(Ljava/lang/Object;)V

    .line 419
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v2

    .line 420
    goto :goto_0
.end method

.method private getSearchCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 424
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "search"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSharedPlaylistEntriesCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "shareToken"    # Ljava/lang/String;

    .prologue
    .line 432
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "shared_playlist"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isCacheable(Lcom/google/android/music/cloudclient/AlbumJson;)Z
    .locals 1
    .param p1, "album"    # Lcom/google/android/music/cloudclient/AlbumJson;

    .prologue
    .line 457
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/music/cloudclient/AlbumJson;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCacheable(Lcom/google/android/music/cloudclient/ArtistJson;)Z
    .locals 1
    .param p1, "artist"    # Lcom/google/android/music/cloudclient/ArtistJson;

    .prologue
    .line 466
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mArtistId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCacheable(Lcom/google/android/music/sync/google/model/Track;)Z
    .locals 1
    .param p1, "track"    # Lcom/google/android/music/sync/google/model/Track;

    .prologue
    .line 449
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/music/sync/google/model/Track;->mNautilusId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private putObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 392
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 394
    :cond_0
    const-string v0, "NautilusContentCache"

    const-string v1, "Invalid arguments"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    :goto_0
    return-void

    .line 397
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    monitor-enter v1

    .line 398
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v2, p2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v2}, Lcom/google/android/music/utils/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private removeObject(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 378
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    const-string v0, "NautilusContentCache"

    const-string v1, "Invalid argument"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    monitor-enter v1

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    invoke-virtual {v0, p1}, Lcom/google/android/music/utils/LruCache;->remove(Ljava/lang/Object;)V

    .line 385
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public getAlbum(Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    .locals 4
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid nautilus id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_album_memory_cache_ttl_sec"

    const v3, 0x15180

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 112
    .local v0, "albumTtlSec":I
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mAlbumCache:Lcom/google/android/music/utils/LruCache;

    mul-int/lit16 v2, v0, 0x3e8

    int-to-long v2, v2

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/AlbumJson;

    return-object v1
.end method

.method public getArtist(Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    .locals 4
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid nautilus id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_artist_memory_cache_ttl_sec"

    const v3, 0x15180

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 96
    .local v0, "artistTtlSec":I
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mArtistCache:Lcom/google/android/music/utils/LruCache;

    mul-int/lit16 v2, v0, 0x3e8

    int-to-long v2, v2

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/ArtistJson;

    return-object v1
.end method

.method public getArtistArtUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getArtistArtUrlCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getExploreResponse(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;
    .locals 6
    .param p1, "tabType"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 306
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_explore_memory_cache_ttl_sec"

    const/16 v3, 0x384

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 310
    .local v0, "exploreTtlSec":I
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    invoke-direct {p0, p1, p2}, Lcom/google/android/music/store/NautilusContentCache;->getExploreCacheKey(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    mul-int/lit16 v3, v0, 0x3e8

    int-to-long v4, v3

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/TabJson;

    return-object v1
.end method

.method public getMusicGenresResponse(Ljava/lang/String;)Lcom/google/android/music/cloudclient/MusicGenresResponseJson;
    .locals 6
    .param p1, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 358
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_explore_memory_cache_ttl_sec"

    const/16 v3, 0x384

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 362
    .local v0, "exploreTtlSec":I
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getGenreCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    mul-int/lit16 v3, v0, 0x3e8

    int-to-long v4, v3

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;

    return-object v1
.end method

.method public getSearchResponse(Ljava/lang/String;)Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 266
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_search_memory_cache_ttl_sec"

    const/16 v3, 0x384

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 270
    .local v0, "searchTtlSec":I
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getSearchCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    mul-int/lit16 v3, v0, 0x3e8

    int-to-long v4, v3

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    return-object v1
.end method

.method public getSharedPlaylistEntriesResponse(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "shareToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_shared_playlsits_memory_cache_ttl_sec"

    const/16 v3, 0x384

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 339
    .local v0, "sharedPlaylistTtlSec":I
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mGenericCache:Lcom/google/android/music/utils/LruCache;

    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getSharedPlaylistEntriesCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    mul-int/lit16 v3, v0, 0x3e8

    int-to-long v4, v3

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    .locals 4
    .param p1, "nid"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid nautilus id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_track_memory_cache_ttl_sec"

    const v3, 0x15180

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 127
    .local v0, "trackTtlSec":I
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mTrackCache:Lcom/google/android/music/utils/LruCache;

    mul-int/lit16 v2, v0, 0x3e8

    int-to-long v2, v2

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/google/android/music/store/NautilusContentCache;->getObject(Lcom/google/android/music/utils/LruCache;Ljava/lang/String;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/Track;

    return-object v1
.end method

.method public putAlbum(Lcom/google/android/music/cloudclient/AlbumJson;)V
    .locals 8
    .param p1, "album"    # Lcom/google/android/music/cloudclient/AlbumJson;

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->isCacheable(Lcom/google/android/music/cloudclient/AlbumJson;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 168
    sget-boolean v2, Lcom/google/android/music/store/NautilusContentCache;->LOGV:Z

    if-eqz v2, :cond_0

    .line 169
    const-string v2, "NautilusContentCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not cacheable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    return-void

    .line 173
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/store/NautilusContentCache;->mAlbumCache:Lcom/google/android/music/utils/LruCache;

    monitor-enter v3

    .line 174
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/store/NautilusContentCache;->mAlbumCache:Lcom/google/android/music/utils/LruCache;

    iget-object v4, p1, Lcom/google/android/music/cloudclient/AlbumJson;->mAlbumId:Ljava/lang/String;

    new-instance v5, Landroid/util/Pair;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v5, p1, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v4, v5}, Lcom/google/android/music/utils/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    iget-object v2, p1, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 180
    iget-object v2, p1, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/Track;

    .line 181
    .local v1, "track":Lcom/google/android/music/sync/google/model/Track;
    invoke-virtual {p0, v1}, Lcom/google/android/music/store/NautilusContentCache;->putTrack(Lcom/google/android/music/sync/google/model/Track;)V

    goto :goto_0

    .line 176
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "track":Lcom/google/android/music/sync/google/model/Track;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public putArtist(Lcom/google/android/music/cloudclient/ArtistJson;)V
    .locals 10
    .param p1, "artist"    # Lcom/google/android/music/cloudclient/ArtistJson;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->isCacheable(Lcom/google/android/music/cloudclient/ArtistJson;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 137
    sget-boolean v3, Lcom/google/android/music/store/NautilusContentCache;->LOGV:Z

    if-eqz v3, :cond_0

    .line 138
    const-string v3, "NautilusContentCache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not cacheable: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_0
    return-void

    .line 142
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/store/NautilusContentCache;->mArtistCache:Lcom/google/android/music/utils/LruCache;

    monitor-enter v4

    .line 143
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/store/NautilusContentCache;->mArtistCache:Lcom/google/android/music/utils/LruCache;

    iget-object v5, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mArtistId:Ljava/lang/String;

    new-instance v6, Landroid/util/Pair;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v6, p1, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v3, v5, v6}, Lcom/google/android/music/utils/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    iget-object v3, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 149
    iget-object v3, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/AlbumJson;

    .line 150
    .local v0, "album":Lcom/google/android/music/cloudclient/AlbumJson;
    invoke-virtual {p0, v0}, Lcom/google/android/music/store/NautilusContentCache;->putAlbum(Lcom/google/android/music/cloudclient/AlbumJson;)V

    goto :goto_0

    .line 145
    .end local v0    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 154
    :cond_2
    iget-object v3, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mTopTracks:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 155
    iget-object v3, p1, Lcom/google/android/music/cloudclient/ArtistJson;->mTopTracks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/Track;

    .line 156
    .local v2, "track":Lcom/google/android/music/sync/google/model/Track;
    invoke-virtual {p0, v2}, Lcom/google/android/music/store/NautilusContentCache;->putTrack(Lcom/google/android/music/sync/google/model/Track;)V

    goto :goto_1
.end method

.method public putArtistArtUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 228
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    :cond_0
    sget-boolean v0, Lcom/google/android/music/store/NautilusContentCache;->LOGV:Z

    if-eqz v0, :cond_1

    .line 230
    const-string v0, "NautilusContentCache"

    const-string v1, "Not cacheable. id=%s, url=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_1
    :goto_0
    return-void

    .line 234
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getArtistArtUrlCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/music/store/NautilusContentCache;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public putExploreResponse(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;Lcom/google/android/music/cloudclient/TabJson;)V
    .locals 7
    .param p1, "tabType"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p2, "genreId"    # Ljava/lang/String;
    .param p3, "response"    # Lcom/google/android/music/cloudclient/TabJson;

    .prologue
    .line 278
    if-eqz p3, :cond_0

    iget-object v5, p3, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    if-nez v5, :cond_2

    .line 279
    :cond_0
    const-string v5, "NautilusContentCache"

    const-string v6, "Trying to insert a null explore response"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_1
    return-void

    .line 283
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/store/NautilusContentCache;->getExploreCacheKey(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, p3}, Lcom/google/android/music/store/NautilusContentCache;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    iget-object v5, p3, Lcom/google/android/music/cloudclient/TabJson;->mGroups:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;

    .line 289
    .local v2, "group":Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;
    iget-object v0, v2, Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;->mEntities:Ljava/util/List;

    .line 290
    .local v0, "entities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ExploreEntityJson;>;"
    if-eqz v0, :cond_3

    .line 291
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;

    .line 293
    .local v1, "entity":Lcom/google/android/music/cloudclient/ExploreEntityJson;
    iget-object v5, v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    if-eqz v5, :cond_5

    .line 294
    iget-object v5, v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {p0, v5}, Lcom/google/android/music/store/NautilusContentCache;->putTrack(Lcom/google/android/music/sync/google/model/Track;)V

    goto :goto_0

    .line 295
    :cond_5
    iget-object v5, v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mPlaylist:Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-nez v5, :cond_4

    .line 297
    iget-object v5, v1, Lcom/google/android/music/cloudclient/ExploreEntityJson;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    if-eqz v5, :cond_4

    goto :goto_0
.end method

.method public putMusicGenresResponse(Ljava/lang/String;Lcom/google/android/music/cloudclient/MusicGenresResponseJson;)V
    .locals 2
    .param p1, "parentGenreId"    # Ljava/lang/String;
    .param p2, "response"    # Lcom/google/android/music/cloudclient/MusicGenresResponseJson;

    .prologue
    .line 347
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/android/music/cloudclient/MusicGenresResponseJson;->mGenres:Ljava/util/List;

    if-nez v0, :cond_1

    .line 348
    :cond_0
    const-string v0, "NautilusContentCache"

    const-string v1, "Trying to insert a null genres response"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :goto_0
    return-void

    .line 351
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getGenreCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/music/store/NautilusContentCache;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public putSearchResponse(Ljava/lang/String;Lcom/google/android/music/cloudclient/SearchClientResponseJson;)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "response"    # Lcom/google/android/music/cloudclient/SearchClientResponseJson;

    .prologue
    .line 243
    if-eqz p2, :cond_0

    iget-object v3, p2, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mEntries:Ljava/util/List;

    if-nez v3, :cond_2

    .line 244
    :cond_0
    const-string v3, "NautilusContentCache"

    const-string v4, "Trying to insert a null search response"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_1
    return-void

    .line 248
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getSearchCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lcom/google/android/music/store/NautilusContentCache;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 250
    iget-object v0, p2, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mEntries:Ljava/util/List;

    .line 251
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;

    .line 253
    .local v1, "entry":Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    iget-object v3, v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mArtist:Lcom/google/android/music/cloudclient/ArtistJson;

    if-nez v3, :cond_3

    .line 257
    iget-object v3, v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    if-eqz v3, :cond_4

    .line 258
    iget-object v3, v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mAlbum:Lcom/google/android/music/cloudclient/AlbumJson;

    invoke-virtual {p0, v3}, Lcom/google/android/music/store/NautilusContentCache;->putAlbum(Lcom/google/android/music/cloudclient/AlbumJson;)V

    goto :goto_0

    .line 259
    :cond_4
    iget-object v3, v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    if-eqz v3, :cond_3

    .line 260
    iget-object v3, v1, Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;->mTrack:Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {p0, v3}, Lcom/google/android/music/store/NautilusContentCache;->putTrack(Lcom/google/android/music/sync/google/model/Track;)V

    goto :goto_0
.end method

.method public putSharedPlaylistEntriesResponse(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "shareToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 323
    .local p2, "response":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 324
    :cond_0
    const-string v0, "NautilusContentCache"

    const-string v1, "Trying to insert a null shared playlist entries response"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :goto_0
    return-void

    .line 328
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getSharedPlaylistEntriesCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/music/store/NautilusContentCache;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public putTrack(Lcom/google/android/music/sync/google/model/Track;)V
    .locals 6
    .param p1, "track"    # Lcom/google/android/music/sync/google/model/Track;

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->isCacheable(Lcom/google/android/music/sync/google/model/Track;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    sget-boolean v0, Lcom/google/android/music/store/NautilusContentCache;->LOGV:Z

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "NautilusContentCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not cacheable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/store/NautilusContentCache;->mTrackCache:Lcom/google/android/music/utils/LruCache;

    monitor-enter v1

    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/store/NautilusContentCache;->mTrackCache:Lcom/google/android/music/utils/LruCache;

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/util/Pair;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/utils/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeExploreResponse(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)V
    .locals 1
    .param p1, "tabType"    # Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;
    .param p2, "genreId"    # Ljava/lang/String;

    .prologue
    .line 315
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/store/NautilusContentCache;->getExploreCacheKey(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/store/NautilusContentCache;->removeObject(Ljava/lang/String;)V

    .line 316
    return-void
.end method

.method public removeSharedPlaylistEntries(Ljava/lang/String;)V
    .locals 1
    .param p1, "shareToken"    # Ljava/lang/String;

    .prologue
    .line 371
    invoke-direct {p0, p1}, Lcom/google/android/music/store/NautilusContentCache;->getSharedPlaylistEntriesCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/store/NautilusContentCache;->removeObject(Ljava/lang/String;)V

    .line 372
    return-void
.end method
