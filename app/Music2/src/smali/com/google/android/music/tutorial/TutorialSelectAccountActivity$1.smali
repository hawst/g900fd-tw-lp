.class Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;
.super Ljava/lang/Object;
.source "TutorialSelectAccountActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->addAccountClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 261
    :goto_0
    return-void

    .line 241
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 242
    .local v2, "result":Landroid/os/Bundle;
    const-string v4, "authAccount"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "accountName":Ljava/lang/String;
    const-string v4, "accountType"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, "accountType":Ljava/lang/String;
    const-string v4, "setupSkipped"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 246
    .local v3, "setupSkipped":Z
    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 248
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    # setter for: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountName:Ljava/lang/String;
    invoke-static {v4, v0}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$002(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 249
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    # setter for: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountType:Ljava/lang/String;
    invoke-static {v4, v1}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$102(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 255
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "accountType":Ljava/lang/String;
    .end local v2    # "result":Landroid/os/Bundle;
    .end local v3    # "setupSkipped":Z
    :catch_0
    move-exception v4

    goto :goto_0

    .line 251
    .restart local v0    # "accountName":Ljava/lang/String;
    .restart local v1    # "accountType":Ljava/lang/String;
    .restart local v2    # "result":Landroid/os/Bundle;
    .restart local v3    # "setupSkipped":Z
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    const/4 v5, 0x0

    # setter for: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountName:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$002(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 252
    iget-object v4, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$1;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    const/4 v5, 0x0

    # setter for: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->mCreatedAccountType:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$102(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 257
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "accountType":Ljava/lang/String;
    .end local v2    # "result":Landroid/os/Bundle;
    .end local v3    # "setupSkipped":Z
    :catch_1
    move-exception v4

    goto :goto_0

    .line 259
    :catchall_0
    move-exception v4

    throw v4

    .line 258
    :catch_2
    move-exception v4

    goto :goto_0
.end method
