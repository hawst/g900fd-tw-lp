.class public abstract Lcom/google/android/music/playback/IMusicPlaybackService$Stub;
.super Landroid/os/Binder;
.source "IMusicPlaybackService.java"

# interfaces
.implements Lcom/google/android/music/playback/IMusicPlaybackService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/IMusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/IMusicPlaybackService$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/music/playback/IMusicPlaybackService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/google/android/music/playback/IMusicPlaybackService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/google/android/music/playback/IMusicPlaybackService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 669
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 42
    :sswitch_0
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1

    .line 50
    sget-object v8, Lcom/google/android/music/medialist/SongList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongList;

    .line 56
    .local v0, "_arg0":Lcom/google/android/music/medialist/SongList;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 58
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_0

    move v4, v5

    .line 59
    .local v4, "_arg2":Z
    :cond_0
    invoke-virtual {p0, v0, v2, v4}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 53
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    .end local v2    # "_arg1":I
    .end local v4    # "_arg2":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    goto :goto_1

    .line 65
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    :sswitch_2
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2

    .line 68
    sget-object v8, Lcom/google/android/music/medialist/SongList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongList;

    .line 74
    .restart local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 75
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v0, v2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->openAndQueue(Lcom/google/android/music/medialist/SongList;I)V

    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 71
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    .end local v2    # "_arg1":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    goto :goto_2

    .line 81
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    :sswitch_3
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->clearQueue()V

    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 88
    :sswitch_4
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getQueuePosition()I

    move-result v6

    .line 90
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 91
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 96
    .end local v6    # "_result":I
    :sswitch_5
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 99
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->setQueuePosition(I)V

    .line 100
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 105
    .end local v0    # "_arg0":I
    :sswitch_6
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getQueueSize()I

    move-result v6

    .line 107
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 108
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 113
    .end local v6    # "_result":I
    :sswitch_7
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3

    .line 116
    sget-object v8, Lcom/google/android/music/medialist/SongList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongList;

    .line 122
    .local v0, "_arg0":Lcom/google/android/music/medialist/SongList;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 123
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v0, v2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->playNext(Lcom/google/android/music/medialist/SongList;I)V

    .line 124
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 119
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    .end local v2    # "_arg1":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    goto :goto_3

    .line 129
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    :sswitch_8
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isPlaying()Z

    move-result v6

    .line 131
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 132
    if-eqz v6, :cond_4

    move v4, v5

    :cond_4
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 137
    .end local v6    # "_result":Z
    :sswitch_9
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->pause()V

    .line 139
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 144
    :sswitch_a
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->play()V

    .line 146
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 151
    :sswitch_b
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->stop()V

    .line 153
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 158
    :sswitch_c
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->prev()V

    .line 160
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 165
    :sswitch_d
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->next()V

    .line 167
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 172
    :sswitch_e
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isCurrentSongLoaded()Z

    move-result v6

    .line 174
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 175
    if-eqz v6, :cond_5

    move v4, v5

    :cond_5
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 180
    .end local v6    # "_result":Z
    :sswitch_f
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getTrackName()Ljava/lang/String;

    move-result-object v6

    .line 182
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 183
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 188
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_10
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getAlbumName()Ljava/lang/String;

    move-result-object v6

    .line 190
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 191
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_11
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 197
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getAlbumId()J

    move-result-wide v6

    .line 198
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 199
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 204
    .end local v6    # "_result":J
    :sswitch_12
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getArtistName()Ljava/lang/String;

    move-result-object v6

    .line 206
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 207
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 212
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_13
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getArtistId()J

    move-result-wide v6

    .line 214
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 215
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 220
    .end local v6    # "_result":J
    :sswitch_14
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getSortableAlbumArtistName()Ljava/lang/String;

    move-result-object v6

    .line 222
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 223
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 228
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_15
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getSongStoreId()Ljava/lang/String;

    move-result-object v6

    .line 230
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 236
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_16
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v6

    .line 238
    .local v6, "_result":Lcom/google/android/music/medialist/SongList;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 239
    if-eqz v6, :cond_6

    .line 240
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 241
    invoke-virtual {v6, p3, v5}, Lcom/google/android/music/medialist/SongList;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 244
    :cond_6
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 250
    .end local v6    # "_result":Lcom/google/android/music/medialist/SongList;
    :sswitch_17
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v6

    .line 252
    .local v6, "_result":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 253
    if-eqz v6, :cond_7

    .line 254
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 255
    invoke-virtual {v6, p3, v5}, Lcom/google/android/music/download/ContentIdentifier;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 258
    :cond_7
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 264
    .end local v6    # "_result":Lcom/google/android/music/download/ContentIdentifier;
    :sswitch_18
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->supportsRating()Z

    move-result v6

    .line 266
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 267
    if-eqz v6, :cond_8

    move v4, v5

    :cond_8
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 272
    .end local v6    # "_result":Z
    :sswitch_19
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getRating()I

    move-result v6

    .line 274
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 275
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 280
    .end local v6    # "_result":I
    :sswitch_1a
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 282
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 283
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->setRating(I)V

    .line 284
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 289
    .end local v0    # "_arg0":I
    :sswitch_1b
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->position()J

    move-result-wide v6

    .line 291
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 292
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 297
    .end local v6    # "_result":J
    :sswitch_1c
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->duration()J

    move-result-wide v6

    .line 299
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 300
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 305
    .end local v6    # "_result":J
    :sswitch_1d
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 308
    .local v0, "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->seek(J)J

    move-result-wide v6

    .line 309
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 310
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 315
    .end local v0    # "_arg0":J
    .end local v6    # "_result":J
    :sswitch_1e
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 317
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 318
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->setShuffleMode(I)V

    .line 319
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 324
    .end local v0    # "_arg0":I
    :sswitch_1f
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getShuffleMode()I

    move-result v6

    .line 326
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 327
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 332
    .end local v6    # "_result":I
    :sswitch_20
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 334
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 335
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->setRepeatMode(I)V

    .line 336
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 341
    .end local v0    # "_arg0":I
    :sswitch_21
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 342
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getRepeatMode()I

    move-result v6

    .line 343
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 344
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 349
    .end local v6    # "_result":I
    :sswitch_22
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 350
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isPreparing()Z

    move-result v6

    .line 351
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 352
    if-eqz v6, :cond_9

    move v4, v5

    :cond_9
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 357
    .end local v6    # "_result":Z
    :sswitch_23
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isStreaming()Z

    move-result v6

    .line 359
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 360
    if-eqz v6, :cond_a

    move v4, v5

    :cond_a
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 365
    .end local v6    # "_result":Z
    :sswitch_24
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 366
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isInErrorState()Z

    move-result v6

    .line 367
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 368
    if-eqz v6, :cond_b

    move v4, v5

    :cond_b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 373
    .end local v6    # "_result":Z
    :sswitch_25
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getErrorType()I

    move-result v6

    .line 375
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 376
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 381
    .end local v6    # "_result":I
    :sswitch_26
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 382
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isStreamingFullyBuffered()Z

    move-result v6

    .line 383
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 384
    if-eqz v6, :cond_c

    move v4, v5

    :cond_c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 389
    .end local v6    # "_result":Z
    :sswitch_27
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isInFatalErrorState()Z

    move-result v6

    .line 391
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 392
    if-eqz v6, :cond_d

    move v4, v5

    :cond_d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 397
    .end local v6    # "_result":Z
    :sswitch_28
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 398
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->hasValidPlaylist()Z

    move-result v6

    .line 399
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 400
    if-eqz v6, :cond_e

    move v4, v5

    :cond_e
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 405
    .end local v6    # "_result":Z
    :sswitch_29
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isPlaylistLoading()Z

    move-result v6

    .line 407
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 408
    if-eqz v6, :cond_f

    move v4, v5

    :cond_f
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 413
    .end local v6    # "_result":Z
    :sswitch_2a
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->hasLocal()Z

    move-result v6

    .line 415
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 416
    if-eqz v6, :cond_10

    move v4, v5

    :cond_10
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 421
    .end local v6    # "_result":Z
    :sswitch_2b
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 422
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->hasRemote()Z

    move-result v6

    .line 423
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 424
    if-eqz v6, :cond_11

    move v4, v5

    :cond_11
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 429
    .end local v6    # "_result":Z
    :sswitch_2c
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 431
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_13

    .line 432
    sget-object v8, Lcom/google/android/music/download/ContentIdentifier;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/ContentIdentifier;

    .line 438
    .local v0, "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/download/IDownloadProgressListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/IDownloadProgressListener;

    move-result-object v2

    .line 439
    .local v2, "_arg1":Lcom/google/android/music/download/IDownloadProgressListener;
    invoke-virtual {p0, v0, v2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->addDownloadProgressListener(Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/download/IDownloadProgressListener;)Z

    move-result v6

    .line 440
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 441
    if-eqz v6, :cond_12

    move v4, v5

    :cond_12
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 435
    .end local v0    # "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    .end local v2    # "_arg1":Lcom/google/android/music/download/IDownloadProgressListener;
    .end local v6    # "_result":Z
    :cond_13
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    goto :goto_4

    .line 446
    .end local v0    # "_arg0":Lcom/google/android/music/download/ContentIdentifier;
    :sswitch_2d
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 448
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/download/IDownloadProgressListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/IDownloadProgressListener;

    move-result-object v0

    .line 449
    .local v0, "_arg0":Lcom/google/android/music/download/IDownloadProgressListener;
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->removeDownloadProgressListener(Lcom/google/android/music/download/IDownloadProgressListener;)V

    .line 450
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 455
    .end local v0    # "_arg0":Lcom/google/android/music/download/IDownloadProgressListener;
    :sswitch_2e
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 457
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_14

    move v0, v5

    .line 458
    .local v0, "_arg0":Z
    :goto_5
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->setUIVisible(Z)V

    .line 459
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_14
    move v0, v4

    .line 457
    goto :goto_5

    .line 464
    :sswitch_2f
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 465
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getPreviewPlayType()I

    move-result v6

    .line 466
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 467
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 472
    .end local v6    # "_result":I
    :sswitch_30
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 473
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getAudioSessionId()I

    move-result v6

    .line 474
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 475
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 480
    .end local v6    # "_result":I
    :sswitch_31
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 481
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isAlbumArtInService()Z

    move-result v6

    .line 482
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 483
    if-eqz v6, :cond_15

    move v4, v5

    :cond_15
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 488
    .end local v6    # "_result":Z
    :sswitch_32
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 490
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 491
    .local v0, "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getAlbumArtUrl(J)Ljava/lang/String;

    move-result-object v6

    .line 492
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 493
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 498
    .end local v0    # "_arg0":J
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_33
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 499
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getSelectedMediaRouteId()Ljava/lang/String;

    move-result-object v6

    .line 500
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 501
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 506
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_34
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 507
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isInCloudQueueMode()Z

    move-result v6

    .line 508
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 509
    if-eqz v6, :cond_16

    move v4, v5

    :cond_16
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 514
    .end local v6    # "_result":Z
    :sswitch_35
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 516
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_17

    move v0, v5

    .line 518
    .local v0, "_arg0":Z
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 519
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->setMediaRoute(ZLjava/lang/String;)V

    .line 520
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v2    # "_arg1":Ljava/lang/String;
    :cond_17
    move v0, v4

    .line 516
    goto :goto_6

    .line 525
    :sswitch_36
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 527
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 529
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    .line 530
    .local v2, "_arg1":D
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->setMediaRouteVolume(Ljava/lang/String;D)V

    .line 531
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 536
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":D
    :sswitch_37
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 538
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 540
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    .line 541
    .restart local v2    # "_arg1":D
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->updateMediaRouteVolume(Ljava/lang/String;D)V

    .line 542
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 547
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":D
    :sswitch_38
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 549
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/playback/IMusicCastMediaRouterCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    move-result-object v0

    .line 550
    .local v0, "_arg0":Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->registerMusicCastMediaRouterCallback(Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;)V

    .line 551
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 556
    .end local v0    # "_arg0":Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;
    :sswitch_39
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 557
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getLastUserInteractionTime()J

    move-result-wide v6

    .line 558
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 559
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 564
    .end local v6    # "_result":J
    :sswitch_3a
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 565
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;

    move-result-object v6

    .line 566
    .local v6, "_result":Lcom/google/android/music/playback/TrackInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 567
    if-eqz v6, :cond_18

    .line 568
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 569
    invoke-virtual {v6, p3, v5}, Lcom/google/android/music/playback/TrackInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 572
    :cond_18
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 578
    .end local v6    # "_result":Lcom/google/android/music/playback/TrackInfo;
    :sswitch_3b
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 579
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v6

    .line 580
    .local v6, "_result":Lcom/google/android/music/playback/PlaybackState;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 581
    if-eqz v6, :cond_19

    .line 582
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 583
    invoke-virtual {v6, p3, v5}, Lcom/google/android/music/playback/PlaybackState;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 586
    :cond_19
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 592
    .end local v6    # "_result":Lcom/google/android/music/playback/PlaybackState;
    :sswitch_3c
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 594
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1a

    .line 595
    sget-object v8, Lcom/google/android/music/mix/MixDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    .line 600
    .local v0, "_arg0":Lcom/google/android/music/mix/MixDescriptor;
    :goto_7
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 601
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 598
    .end local v0    # "_arg0":Lcom/google/android/music/mix/MixDescriptor;
    :cond_1a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/google/android/music/mix/MixDescriptor;
    goto :goto_7

    .line 606
    .end local v0    # "_arg0":Lcom/google/android/music/mix/MixDescriptor;
    :sswitch_3d
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 607
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->cancelMix()V

    .line 608
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 613
    :sswitch_3e
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 614
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->getMixState()Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v6

    .line 615
    .local v6, "_result":Lcom/google/android/music/mix/MixGenerationState;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 616
    if-eqz v6, :cond_1b

    .line 617
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 618
    invoke-virtual {v6, p3, v5}, Lcom/google/android/music/mix/MixGenerationState;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 621
    :cond_1b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 627
    .end local v6    # "_result":Lcom/google/android/music/mix/MixGenerationState;
    :sswitch_3f
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 628
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->isInIniniteMixMode()Z

    move-result v6

    .line 629
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 630
    if-eqz v6, :cond_1c

    move v4, v5

    :cond_1c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 635
    .end local v6    # "_result":Z
    :sswitch_40
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 636
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->shuffleAll()V

    .line 637
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 642
    :sswitch_41
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 643
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->shuffleOnDevice()V

    .line 644
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 649
    :sswitch_42
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 651
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1d

    .line 652
    sget-object v8, Lcom/google/android/music/medialist/SongList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongList;

    .line 657
    .local v0, "_arg0":Lcom/google/android/music/medialist/SongList;
    :goto_8
    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 658
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 655
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    :cond_1d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    goto :goto_8

    .line 663
    .end local v0    # "_arg0":Lcom/google/android/music/medialist/SongList;
    :sswitch_43
    const-string v8, "com.google.android.music.playback.IMusicPlaybackService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 664
    invoke-virtual {p0}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->refreshRadio()V

    .line 665
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
