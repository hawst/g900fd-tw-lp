.class Lcom/google/android/music/art/ArtRequest$4;
.super Ljava/lang/Object;
.source "ArtRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtRequest;->onLoadError(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtRequest;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtRequest;)V
    .locals 0

    .prologue
    .line 188
    .local p0, "this":Lcom/google/android/music/art/ArtRequest$4;, "Lcom/google/android/music/art/ArtRequest.4;"
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest$4;->this$0:Lcom/google/android/music/art/ArtRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 192
    .local p0, "this":Lcom/google/android/music/art/ArtRequest$4;, "Lcom/google/android/music/art/ArtRequest.4;"
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest$4;->this$0:Lcom/google/android/music/art/ArtRequest;

    # getter for: Lcom/google/android/music/art/ArtRequest;->mListeners:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/google/android/music/art/ArtRequest;->access$100(Lcom/google/android/music/art/ArtRequest;)Ljava/util/HashSet;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 193
    .local v0, "copy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/art/ArtRequest$Listener<TT;>;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/art/ArtRequest$Listener;

    .line 194
    .local v2, "l":Lcom/google/android/music/art/ArtRequest$Listener;, "Lcom/google/android/music/art/ArtRequest$Listener<TT;>;"
    iget-object v3, p0, Lcom/google/android/music/art/ArtRequest$4;->this$0:Lcom/google/android/music/art/ArtRequest;

    invoke-interface {v2, v3}, Lcom/google/android/music/art/ArtRequest$Listener;->onLoadError(Lcom/google/android/music/art/ArtRequest;)V

    goto :goto_0

    .line 196
    .end local v2    # "l":Lcom/google/android/music/art/ArtRequest$Listener;, "Lcom/google/android/music/art/ArtRequest$Listener<TT;>;"
    :cond_0
    return-void
.end method
