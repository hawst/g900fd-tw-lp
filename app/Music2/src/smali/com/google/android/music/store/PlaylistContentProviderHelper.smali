.class Lcom/google/android/music/store/PlaylistContentProviderHelper;
.super Ljava/lang/Object;
.source "PlaylistContentProviderHelper.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/PlaylistContentProviderHelper;->LOGV:Z

    return-void
.end method

.method private static canExecuteRemoteOperation(Landroid/accounts/Account;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)Z
    .locals 5
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "networkConnection"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .prologue
    .line 305
    const/4 v1, 0x0

    .line 307
    .local v1, "hasConnectivity":Z
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v2

    .line 308
    .local v2, "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    if-nez v2, :cond_0

    .line 309
    const-string v3, "PlaylistCPHelper"

    const-string v4, "Attempting search before network monitor is bound"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    .end local v2    # "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    :goto_0
    if-eqz p0, :cond_1

    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :goto_1
    return v3

    .line 311
    .restart local v2    # "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    :cond_0
    invoke-interface {v2}, Lcom/google/android/music/net/INetworkMonitor;->isConnected()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 314
    .end local v2    # "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "PlaylistCPHelper"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 317
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static createPlaylist(Landroid/content/Context;Lcom/google/android/music/store/Store;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/music/net/NetworkMonitorServiceConnection;)J
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "playlistName"    # Ljava/lang/String;
    .param p4, "playlistDescription"    # Ljava/lang/String;
    .param p5, "shareState"    # I
    .param p6, "networkConnection"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .prologue
    .line 220
    sget-boolean v15, Lcom/google/android/music/store/PlaylistContentProviderHelper;->LOGV:Z

    if-eqz v15, :cond_0

    .line 221
    const-string v15, "PlaylistCPHelper"

    const-string v16, "createPlaylist: name=%s description=%s shareState=%s"

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object p3, v17, v18

    const/16 v18, 0x1

    aput-object p4, v17, v18

    const/16 v18, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_0
    const-wide/16 v8, 0x0

    .line 226
    .local v8, "newId":J
    const/4 v11, 0x0

    .line 227
    .local v11, "success":Z
    move-object/from16 v0, p2

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->canExecuteRemoteOperation(Landroid/accounts/Account;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)Z

    move-result v4

    .line 229
    .local v4, "canExecuteRemote":Z
    const/4 v15, 0x1

    move/from16 v0, p5

    if-eq v0, v15, :cond_1

    if-nez v4, :cond_2

    .line 230
    :cond_1
    const/4 v15, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2, v15}, Lcom/google/android/music/store/Store;->createPlaylist(Ljava/lang/String;Ljava/lang/String;I)J

    move-result-wide v8

    move-wide/from16 v16, v8

    .line 265
    :goto_0
    return-wide v16

    .line 234
    :cond_2
    invoke-static/range {p5 .. p5}, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->localShareStateToRemote(I)Ljava/lang/String;

    move-result-object v10

    .line 235
    .local v10, "remoteShareState":Ljava/lang/String;
    if-nez v10, :cond_3

    .line 236
    const-wide/16 v16, 0x0

    goto :goto_0

    .line 238
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2, v10}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->createRemotePlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-result-object v14

    .line 241
    .local v14, "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    if-nez v14, :cond_4

    .line 242
    const-string v15, "PlaylistCPHelper"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Failed to create playlist remotely. Fallback on local: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v15, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2, v15}, Lcom/google/android/music/store/Store;->createPlaylist(Ljava/lang/String;Ljava/lang/String;I)J

    move-result-wide v16

    goto :goto_0

    .line 246
    :cond_4
    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->formatAsPlayList(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    move-result-object v7

    .line 247
    .local v7, "playlist":Lcom/google/android/music/store/PlayList;
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v15

    invoke-virtual {v7, v15}, Lcom/google/android/music/store/PlayList;->setSourceAccount(I)V

    .line 248
    const/4 v15, 0x0

    invoke-virtual {v7, v15}, Lcom/google/android/music/store/PlayList;->setNeedsSync(Z)V

    .line 250
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 251
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v5}, Lcom/google/android/music/store/PlayList;->compilePlayListInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    .line 253
    .local v6, "insert":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-virtual {v7, v6}, Lcom/google/android/music/store/PlayList;->insertList(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v12

    .line 255
    .local v12, "ret":J
    const-wide/16 v16, -0x1

    cmp-long v15, v12, v16

    if-eqz v15, :cond_6

    const/4 v11, 0x1

    .line 256
    :goto_1
    if-eqz v11, :cond_5

    .line 257
    move-wide v8, v12

    .line 260
    :cond_5
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 261
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    move-wide/from16 v16, v8

    .line 265
    goto :goto_0

    .line 255
    :cond_6
    const/4 v11, 0x0

    goto :goto_1

    .line 260
    .end local v12    # "ret":J
    :catchall_0
    move-exception v15

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 261
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v15
.end method

.method private static createRemotePlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "shareState"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 286
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 287
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    new-instance v2, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;

    invoke-direct {v2}, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;-><init>()V

    .line 288
    .local v2, "playlist":Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    iput-object p1, v2, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->mName:Ljava/lang/String;

    .line 289
    iput-object p2, v2, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->mDescription:Ljava/lang/String;

    .line 290
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->mLastModifiedTimestamp:J

    .line 291
    iput-object p3, v2, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->mShareState:Ljava/lang/String;

    .line 292
    invoke-interface {v0, v2}, Lcom/google/android/music/cloudclient/MusicCloud;->createPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 298
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    .end local v2    # "playlist":Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    :goto_0
    return-object v3

    .line 293
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v4, "PlaylistCPHelper"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 296
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 297
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "PlaylistCPHelper"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static getPlaylistShareState(Landroid/content/Context;Ljava/lang/String;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistRemoteId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 139
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 140
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-interface {v0, p1}, Lcom/google/android/music/cloudclient/MusicCloud;->isPlaylistShared(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    const/4 v2, 0x2

    .line 149
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :goto_0
    return v2

    .line 143
    .restart local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :cond_0
    const/4 v2, 0x3

    goto :goto_0

    .line 144
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v3, "PlaylistCPHelper"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 147
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 148
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "PlaylistCPHelper"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static loadPlaylist(Lcom/google/android/music/store/Store;J)Lcom/google/android/music/store/PlayList;
    .locals 5
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "playlistId"    # J

    .prologue
    const/4 v2, 0x0

    .line 122
    const/4 v1, 0x0

    .line 123
    .local v1, "playlist":Lcom/google/android/music/store/PlayList;
    invoke-virtual {p0}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 125
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v0, p1, p2, v3}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 127
    invoke-virtual {p0, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 130
    if-nez v1, :cond_0

    move-object v1, v2

    .line 134
    .end local v1    # "playlist":Lcom/google/android/music/store/PlayList;
    :cond_0
    return-object v1

    .line 127
    .restart local v1    # "playlist":Lcom/google/android/music/store/PlayList;
    :catchall_0
    move-exception v2

    invoke-virtual {p0, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v2
.end method

.method public static query(Landroid/content/Context;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "matchUri"    # I
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    .line 37
    new-instance v2, Landroid/database/MatrixCursor;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 39
    .local v2, "cursor":Landroid/database/MatrixCursor;
    const/16 v10, 0xbb9

    move/from16 v0, p2

    if-ne v0, v10, :cond_7

    .line 40
    invoke-static/range {p3 .. p3}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 41
    new-instance v10, Ljava/lang/UnsupportedOperationException;

    const-string v11, "count(*) not supported"

    invoke-direct {v10, v11}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 44
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 45
    .local v5, "playlistIdString":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 47
    .local v6, "playlistId":J
    const/4 v9, 0x1

    .line 48
    .local v9, "state":I
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Lcom/google/android/music/store/Store;->getPlaylistRemoteIdAndVersion(J)Landroid/util/Pair;

    move-result-object v4

    .line 50
    .local v4, "idAndVersion":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v4, :cond_2

    .line 51
    const-string v10, "PlaylistCPHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to get remote playlist and version id for id:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :goto_0
    move-object/from16 v0, p3

    array-length v10, v0

    new-array v8, v10, [Ljava/lang/Object;

    .line 59
    .local v8, "row":[Ljava/lang/Object;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move-object/from16 v0, p3

    array-length v10, v0

    if-ge v3, v10, :cond_6

    .line 60
    aget-object v10, p3, v3

    const-string v11, "_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 61
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v3

    .line 59
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 52
    .end local v3    # "i":I
    .end local v8    # "row":[Ljava/lang/Object;
    :cond_2
    iget-object v10, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v10, :cond_3

    .line 53
    const-string v10, "PlaylistCPHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to get remote playlist id for id:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    :cond_3
    iget-object v10, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-static {p0, v10}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->getPlaylistShareState(Landroid/content/Context;Ljava/lang/String;)I

    move-result v9

    goto :goto_0

    .line 62
    .restart local v3    # "i":I
    .restart local v8    # "row":[Ljava/lang/Object;
    :cond_4
    aget-object v10, p3, v3

    sget-object v11, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->STATE:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 63
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v3

    .line 64
    sget-boolean v10, Lcom/google/android/music/store/PlaylistContentProviderHelper;->LOGV:Z

    if-eqz v10, :cond_1

    .line 65
    const-string v10, "PlaylistCPHelper"

    const-string v11, "query: id=%s shareState=%s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 69
    :cond_5
    const-string v10, "PlaylistCPHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unsupported projection:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, p3, v3

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 73
    :cond_6
    invoke-virtual {v2, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 75
    .end local v3    # "i":I
    .end local v4    # "idAndVersion":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "playlistIdString":Ljava/lang/String;
    .end local v6    # "playlistId":J
    .end local v8    # "row":[Ljava/lang/Object;
    .end local v9    # "state":I
    :cond_7
    return-object v2
.end method

.method public static update(Landroid/content/Context;ILandroid/net/Uri;Landroid/content/ContentValues;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 79
    const/16 v7, 0xbb9

    if-ne p1, v7, :cond_5

    .line 80
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 81
    .local v1, "playlistIdString":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/store/MusicContentProvider;->isValidInteger(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 82
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid playlist id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 84
    :cond_0
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 86
    .local v2, "playlistId":J
    const/4 v5, 0x1

    .line 87
    .local v5, "state":I
    sget-object v7, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->STATE:Ljava/lang/String;

    invoke-virtual {p3, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 88
    const/4 v7, 0x1

    if-ne v5, v7, :cond_1

    .line 89
    const-string v7, "PlaylistCPHelper"

    const-string v8, "Unknown share state"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/4 v7, 0x0

    .line 118
    .end local v1    # "playlistIdString":Ljava/lang/String;
    .end local v2    # "playlistId":J
    .end local v5    # "state":I
    :goto_0
    return v7

    .line 93
    .restart local v1    # "playlistIdString":Ljava/lang/String;
    .restart local v2    # "playlistId":J
    .restart local v5    # "state":I
    :cond_1
    sget-boolean v7, Lcom/google/android/music/store/PlaylistContentProviderHelper;->LOGV:Z

    if-eqz v7, :cond_2

    .line 94
    const-string v7, "PlaylistCPHelper"

    const-string v8, "update: id=%s shareState=%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_2
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v7

    invoke-static {v7, v2, v3}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->loadPlaylist(Lcom/google/android/music/store/Store;J)Lcom/google/android/music/store/PlayList;

    move-result-object v0

    .line 98
    .local v0, "playlist":Lcom/google/android/music/store/PlayList;
    if-nez v0, :cond_3

    .line 99
    const-string v7, "PlaylistCPHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to load playlist with id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v7, 0x0

    goto :goto_0

    .line 103
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/music/store/PlayList;->getSourceId()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    .line 104
    const-string v7, "PlaylistCPHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Playlist doesn\'t have remote id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v7, 0x0

    goto :goto_0

    .line 107
    :cond_4
    invoke-static {v0, v5}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->parseAndSetShareState(Lcom/google/android/music/store/PlayList;I)Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;

    move-result-object v6

    .line 111
    .local v6, "syncablePlaylist":Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    iput-wide v8, v6, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->mLastModifiedTimestamp:J

    .line 113
    invoke-static {p0, v6}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->updateRemotePlaylist(Landroid/content/Context;Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-result-object v4

    .line 114
    .local v4, "res":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    if-eqz v4, :cond_5

    .line 115
    const/4 v7, 0x1

    goto :goto_0

    .line 118
    .end local v0    # "playlist":Lcom/google/android/music/store/PlayList;
    .end local v1    # "playlistIdString":Ljava/lang/String;
    .end local v2    # "playlistId":J
    .end local v4    # "res":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .end local v5    # "state":I
    .end local v6    # "syncablePlaylist":Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    :cond_5
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public static updatePlaylist(Landroid/content/Context;Lcom/google/android/music/store/Store;Landroid/accounts/Account;JLjava/lang/String;Ljava/lang/String;ILcom/google/android/music/net/NetworkMonitorServiceConnection;)I
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "playlistId"    # J
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "description"    # Ljava/lang/String;
    .param p7, "shareState"    # I
    .param p8, "networkConnection"    # Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .prologue
    .line 156
    move-object/from16 v0, p2

    move-object/from16 v1, p8

    invoke-static {v0, v1}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->canExecuteRemoteOperation(Landroid/accounts/Account;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)Z

    move-result v10

    .line 157
    .local v10, "canExecuteRemote":Z
    sget-boolean v4, Lcom/google/android/music/store/PlaylistContentProviderHelper;->LOGV:Z

    if-eqz v4, :cond_0

    .line 158
    const-string v4, "PlaylistCPHelper"

    const-string v5, "updatePlaylist: id=%s name=%s description=%s shareState=%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p5, v6, v7

    const/4 v7, 0x2

    aput-object p6, v6, v7

    const/4 v7, 0x3

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_0
    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->loadPlaylist(Lcom/google/android/music/store/Store;J)Lcom/google/android/music/store/PlayList;

    move-result-object v12

    .line 162
    .local v12, "playlist":Lcom/google/android/music/store/PlayList;
    if-nez v12, :cond_1

    .line 163
    const-string v4, "PlaylistCPHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updatePlaylist: Failed to load playlist id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const/4 v4, 0x0

    .line 213
    :goto_0
    return v4

    .line 167
    :cond_1
    invoke-virtual {v12}, Lcom/google/android/music/store/PlayList;->getSourceId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    move/from16 v0, p7

    if-ne v0, v4, :cond_2

    if-eqz v10, :cond_3

    :cond_2
    invoke-virtual {v12}, Lcom/google/android/music/store/PlayList;->getSourceAccount()I

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    move-object/from16 v4, p1

    move-object/from16 v5, p0

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    .line 170
    invoke-virtual/range {v4 .. v9}, Lcom/google/android/music/store/Store;->modifyPlaylist(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    .line 173
    :cond_4
    if-nez v10, :cond_5

    .line 174
    const-string v4, "PlaylistCPHelper"

    const-string v5, "updatePlaylist: No network connectivity"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const/4 v4, 0x0

    goto :goto_0

    .line 179
    :cond_5
    const/4 v4, 0x1

    move/from16 v0, p7

    if-ne v0, v4, :cond_6

    .line 180
    invoke-static {v12}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->parse(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-result-object v15

    .line 189
    .local v15, "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :goto_1
    move-object/from16 v0, p5

    iput-object v0, v15, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    .line 190
    move-object/from16 v0, p6

    iput-object v0, v15, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mDescription:Ljava/lang/String;

    .line 191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iput-wide v4, v15, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    .line 193
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->updateRemotePlaylist(Landroid/content/Context;Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-result-object v15

    .line 194
    if-nez v15, :cond_7

    .line 195
    const-string v4, "PlaylistCPHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to update remote playlist id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const/4 v4, 0x0

    goto :goto_0

    .line 183
    .end local v15    # "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_6
    move/from16 v0, p7

    invoke-static {v12, v0}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->parseAndSetShareState(Lcom/google/android/music/store/PlayList;I)Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;

    move-result-object v13

    .line 186
    .local v13, "playlistWithShareState":Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    move-object v15, v13

    .restart local v15    # "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    goto :goto_1

    .line 198
    .end local v13    # "playlistWithShareState":Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    :cond_7
    invoke-virtual {v15, v12}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->formatAsPlayList(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    .line 199
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, Lcom/google/android/music/store/PlayList;->setNeedsSync(Z)V

    .line 201
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 202
    .local v11, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v11}, Lcom/google/android/music/store/PlayList;->compilePlayListUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v16

    .line 205
    .local v16, "updatePlaylistStatement":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/google/android/music/store/PlayList;->update(Landroid/database/sqlite/SQLiteStatement;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 208
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 210
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    .line 211
    .local v14, "resolver":Landroid/content/ContentResolver;
    sget-object v4, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v14, v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 213
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 207
    .end local v14    # "resolver":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v4

    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 208
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v5}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v4
.end method

.method private static updateRemotePlaylist(Landroid/content/Context;Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .prologue
    const/4 v2, 0x0

    .line 272
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 273
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-interface {v0, p1}, Lcom/google/android/music/cloudclient/MusicCloud;->updatePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 279
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :goto_0
    return-object v2

    .line 274
    :catch_0
    move-exception v1

    .line 275
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v3, "PlaylistCPHelper"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 277
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 278
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "PlaylistCPHelper"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
