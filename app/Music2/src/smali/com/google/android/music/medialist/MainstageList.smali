.class public Lcom/google/android/music/medialist/MainstageList;
.super Lcom/google/android/music/medialist/MediaList;
.source "MainstageList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/MainstageList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/music/medialist/MainstageList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/MainstageList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/MainstageList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 19
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/music/medialist/MediaList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V

    .line 20
    return-void
.end method


# virtual methods
.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
