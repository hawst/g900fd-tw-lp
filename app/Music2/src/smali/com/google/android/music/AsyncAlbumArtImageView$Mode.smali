.class abstract Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.super Ljava/lang/Object;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Mode"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView$1;

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>()V

    return-void
.end method

.method protected static isEqual(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 1
    .param p0, "a"    # Landroid/net/Uri;
    .param p1, "b"    # Landroid/net/Uri;

    .prologue
    .line 130
    if-nez p0, :cond_1

    .line 131
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 133
    :goto_0
    return v0

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method protected static isEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "a"    # Ljava/lang/String;
    .param p1, "b"    # Ljava/lang/String;

    .prologue
    .line 123
    if-nez p0, :cond_1

    .line 124
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    .line 124
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public abstract copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.end method

.method public abstract createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
.end method

.method public cropToSquare()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public getExternalUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    const/4 v0, 0x0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 119
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "hashCode is not implemented for Mode objects"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
