.class Lcom/google/android/music/widgets/ExpandingScrollView$NoTwoThirdsExpandingStateTransition;
.super Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/widgets/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NoTwoThirdsExpandingStateTransition"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 978
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;-><init>(Lcom/google/common/collect/ImmutableList;)V

    .line 979
    return-void
.end method


# virtual methods
.method public convertToAllowedState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 0
    .param p1, "expandingState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 983
    return-object p1
.end method

.method public getPrevious(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p1, "x"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 988
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->getPrevious(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    .line 989
    .local v0, "previous":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    return-object v0
.end method
