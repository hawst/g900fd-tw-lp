.class public Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;
.super Landroid/app/IntentService;
.source "KeepOnUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/KeepOnUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateKeepOnTables"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mSendNotification:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 843
    const-string v0, "KeepOnUpdater"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 832
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->mHandler:Landroid/os/Handler;

    .line 834
    new-instance v0, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables$1;-><init>(Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;)V

    iput-object v0, p0, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->mSendNotification:Ljava/lang/Runnable;

    .line 844
    return-void
.end method

.method private static getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 848
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method private updateOnlineContainers()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 938
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnRadioUtils;->refreshAllKeepOnStations(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 853
    # getter for: Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/store/KeepOnUpdater;->access$100()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 854
    const-string v18, "KeepOnUpdater"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "onHandleIntent: intent="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    :cond_0
    const-string v18, "deselectedAlbums"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v6

    .line 858
    .local v6, "deselectedAlbums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "deselectedPlaylists"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v8

    .line 860
    .local v8, "deselectedPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "deselectedAutoPlaylists"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v7

    .line 862
    .local v7, "deselectedAutoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "deselectedRadioStations"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v9

    .line 865
    .local v9, "deselectedRadioStations":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "selectedAlbums"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v12

    .line 866
    .local v12, "selectedAlbums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "selectedPlaylists"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v14

    .line 867
    .local v14, "selectedPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "selectedAutoPlaylists"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v13

    .line 869
    .local v13, "selectedAutoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "selectedRadioStations"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->getLongCollection(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v15

    .line 872
    .local v15, "selectedRadioStations":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/music/store/KeepOnRadioUtils;->refreshKeepOnStations(Landroid/content/Context;Ljava/util/Collection;)I

    .line 874
    const/4 v10, 0x0

    .line 875
    .local v10, "fileCleanupNeeded":Z
    const/16 v17, 0x0

    .line 876
    .local v17, "totalChanges":I
    const/4 v4, 0x0

    .line 877
    .local v4, "changesMade":Z
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v11

    .line 879
    .local v11, "newKeeponIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v18, "onlineContainers"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 880
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->updateOnlineContainers()Ljava/util/Collection;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 883
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v16

    .line 884
    .local v16, "store":Lcom/google/android/music/store/Store;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 889
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v6, :cond_2

    .line 890
    :try_start_0
    # invokes: Lcom/google/android/music/store/KeepOnUpdater;->deleteAlbums(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    invoke-static {v5, v6}, Lcom/google/android/music/store/KeepOnUpdater;->access$200(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v18

    add-int v17, v17, v18

    .line 892
    :cond_2
    if-eqz v8, :cond_3

    .line 893
    # invokes: Lcom/google/android/music/store/KeepOnUpdater;->deletePlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    invoke-static {v5, v8}, Lcom/google/android/music/store/KeepOnUpdater;->access$300(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v18

    add-int v17, v17, v18

    .line 895
    :cond_3
    if-eqz v7, :cond_4

    .line 896
    # invokes: Lcom/google/android/music/store/KeepOnUpdater;->deleteAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    invoke-static {v5, v7}, Lcom/google/android/music/store/KeepOnUpdater;->access$400(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v18

    add-int v17, v17, v18

    .line 898
    :cond_4
    if-eqz v9, :cond_5

    .line 899
    # invokes: Lcom/google/android/music/store/KeepOnUpdater;->deleteRadioStations(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    invoke-static {v5, v9}, Lcom/google/android/music/store/KeepOnUpdater;->access$500(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v18

    add-int v17, v17, v18

    .line 903
    :cond_5
    if-eqz v12, :cond_6

    .line 904
    invoke-static {v5, v12}, Lcom/google/android/music/store/KeepOnUpdater;->insertAlbums(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 906
    :cond_6
    if-eqz v14, :cond_7

    .line 907
    invoke-static {v5, v14}, Lcom/google/android/music/store/KeepOnUpdater;->insertPlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 909
    :cond_7
    if-eqz v13, :cond_8

    .line 910
    invoke-static {v5, v13}, Lcom/google/android/music/store/KeepOnUpdater;->insertAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 913
    :cond_8
    if-eqz v15, :cond_9

    .line 914
    invoke-static {v5, v15}, Lcom/google/android/music/store/KeepOnUpdater;->insertRadioStations(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 918
    :cond_9
    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v18

    add-int v17, v17, v18

    .line 919
    if-lez v17, :cond_c

    const/4 v4, 0x1

    .line 921
    :goto_0
    if-eqz v4, :cond_a

    .line 922
    invoke-static {v5, v11}, Lcom/google/android/music/store/KeepOnUpdater;->updateNeedToKeepOn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Z

    move-result v10

    .line 923
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/google/android/music/store/Store;->updateKeeponCountsAndSizes(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 926
    :cond_a
    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 928
    if-eqz v4, :cond_b

    .line 930
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->mSendNotification:Ljava/lang/Runnable;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 931
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;->mSendNotification:Ljava/lang/Runnable;

    move-object/from16 v19, v0

    const-wide/16 v20, 0x7d0

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 932
    move-object/from16 v0, p0

    invoke-static {v0, v10}, Lcom/google/android/music/store/KeepOnUpdater;->sendShouldKeeponUpdatedBroadcast(Landroid/content/Context;Z)V

    .line 934
    :cond_b
    return-void

    .line 919
    :cond_c
    const/4 v4, 0x0

    goto :goto_0

    .line 926
    :catchall_0
    move-exception v18

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v18
.end method
