.class public Lcom/google/android/music/ui/ManageDownloadsFragment;
.super Lcom/google/android/music/ui/BaseListFragment;
.source "ManageDownloadsFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ManageDownloadsFragment$5;,
        Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;,
        Lcom/google/android/music/ui/ManageDownloadsFragment$ViewHolder;,
        Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;,
        Lcom/google/android/music/ui/ManageDownloadsFragment$onDownloadButtonClickListener;,
        Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/BaseListFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final QUERY_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

.field private mDownloadButton:Landroid/view/ViewGroup;

.field private mDownloadButtonIcon:Landroid/widget/ImageView;

.field private mDownloadButtonText:Landroid/widget/TextView;

.field private mFreeSizeTextView:Landroid/widget/TextView;

.field private mFreeSizeTextViewEmptyScreen:Landroid/widget/TextView;

.field private mHeaderView:Landroid/view/ViewGroup;

.field private mIsDownloadButtonUpdated:Z

.field private mIsViewCreated:Z

.field private mListBackingData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mOnContainerRowClickListener:Landroid/view/View$OnClickListener;

.field private final mProcessedData:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressBarEmptyScreen:Landroid/widget/ProgressBar;

.field private mUnavailableCardOpacity:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/ManageDownloadsFragment;->LOGV:Z

    .line 96
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "container_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "subtitle"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "subtype"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "artUrls"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "remoteId"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "containerSizeBytes"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/ManageDownloadsFragment;->QUERY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;-><init>()V

    .line 127
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;

    .line 147
    new-instance v0, Lcom/google/android/music/ui/ManageDownloadsFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/ManageDownloadsFragment$1;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mOnContainerRowClickListener:Landroid/view/View$OnClickListener;

    .line 180
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/ManageDownloadsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->handlePauseResumeToggle()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/ManageDownloadsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->updateHeaderButton()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextViewEmptyScreen:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/ui/ManageDownloadsFragment;)Ljava/util/LinkedHashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/ManageDownloadsFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/music/ui/ManageDownloadsFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/ManageDownloadsFragment;)Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/ManageDownloadsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->updateEmptyScreenVisibility()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/ui/ManageDownloadsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->updateProgress()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/music/ui/ManageDownloadsFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mUnavailableCardOpacity:F

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mOnContainerRowClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 54
    sget-boolean v0, Lcom/google/android/music/ui/ManageDownloadsFragment;->LOGV:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/ManageDownloadsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->showMenuInActionBar()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/ManageDownloadsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsDownloadButtonUpdated:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/music/ui/ManageDownloadsFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsDownloadButtonUpdated:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBarEmptyScreen:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/ManageDownloadsFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDownloadsFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private addHeaderAndFooter()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 245
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 246
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 247
    .local v1, "lv":Landroid/widget/ListView;
    const v3, 0x7f04006c

    invoke-virtual {v0, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    .line 249
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 250
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0164

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 253
    .local v2, "padding":I
    invoke-virtual {v1, v2, v5, v2, v5}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 255
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    const v4, 0x7f0e0194

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextView:Landroid/widget/TextView;

    .line 256
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    const v4, 0x7f0e018b

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 258
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    const v4, 0x7f0e0120

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButtonText:Landroid/widget/TextView;

    .line 260
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    const v4, 0x7f0e011f

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButtonIcon:Landroid/widget/ImageView;

    .line 261
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    const v4, 0x7f0e011e

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;

    .line 263
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;

    new-instance v4, Lcom/google/android/music/ui/ManageDownloadsFragment$onDownloadButtonClickListener;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/ManageDownloadsFragment$onDownloadButtonClickListener;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->updateHeaderButton()V

    .line 265
    return-void
.end method

.method private applyDifferences(Landroid/util/Pair;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "differences":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;>;"
    iget-object v5, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    .line 523
    .local v5, "toBeRemoved":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    iget-object v4, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    .line 524
    .local v4, "toBeAdded":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    .line 525
    .local v1, "entry":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->setRemoving(Z)V

    goto :goto_0

    .line 528
    .end local v1    # "entry":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    :cond_0
    new-instance v6, Lcom/google/android/music/ui/ManageDownloadsFragment$3;

    invoke-direct {v6, p0, v5}, Lcom/google/android/music/ui/ManageDownloadsFragment$3;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const/16 v8, 0xbb8

    invoke-static {v6, v7, v8}, Lcom/google/android/music/utils/MusicUtils;->runDelayedOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;I)Landroid/os/Handler;

    .line 539
    iget-object v6, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v6, v4}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 548
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 549
    iget-object v6, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-virtual {v6}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->notifyDataSetInvalidated()V

    .line 554
    :cond_1
    :goto_1
    new-instance v6, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;

    .line 560
    new-instance v3, Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 563
    .local v3, "listBackingDataCopy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 565
    .local v0, "appContext":Landroid/content/Context;
    new-instance v6, Lcom/google/android/music/ui/ManageDownloadsFragment$4;

    invoke-direct {v6, p0, v3, v0}, Lcom/google/android/music/ui/ManageDownloadsFragment$4;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment;Ljava/util/List;Landroid/content/Context;)V

    invoke-static {v6}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 583
    return-void

    .line 550
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v3    # "listBackingDataCopy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 551
    iget-object v6, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-virtual {v6}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method private calculateDifferences(Ljava/util/LinkedHashSet;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 506
    .local p1, "newEntries":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 507
    .local v1, "onlyInOld":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 508
    .local v0, "onlyInNew":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 510
    .local v2, "working":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 511
    invoke-virtual {v2, p1}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 512
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 514
    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->clear()V

    .line 515
    invoke-virtual {v2, p1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 516
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 517
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 518
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3
.end method

.method private handlePauseResumeToggle()V
    .locals 3

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    .line 143
    .local v0, "isDownloadPaused":Z
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setKeepOnDownloadPaused(Z)V

    .line 144
    return-void

    .line 143
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadFromCursor(Landroid/database/Cursor;)Ljava/util/LinkedHashSet;
    .locals 23
    .param p1, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 452
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 453
    .local v4, "result":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    if-nez p1, :cond_1

    .line 501
    :cond_0
    return-object v4

    .line 458
    :cond_1
    const/16 v19, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 459
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 460
    const/4 v5, 0x0

    .line 461
    .local v5, "item":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;->fromDbValue(I)Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    move-result-object v12

    .line 462
    .local v12, "type":Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 463
    .local v6, "id":J
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-eqz v19, :cond_3

    const/4 v9, 0x0

    .line 464
    .local v9, "title":Ljava/lang/String;
    :goto_1
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-eqz v19, :cond_4

    const/high16 v8, -0x80000000

    .line 465
    .local v8, "subType":I
    :goto_2
    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-eqz v19, :cond_5

    const/4 v13, 0x0

    .line 466
    .local v13, "urls":Ljava/lang/String;
    :goto_3
    const/4 v10, 0x0

    .line 467
    .local v10, "subtitle":Ljava/lang/String;
    const/4 v11, 0x0

    .line 468
    .local v11, "size":Ljava/lang/String;
    const/16 v19, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_2

    .line 469
    const/16 v19, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 470
    .local v16, "containerSize":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/google/android/music/utils/IOUtils;->getSizeUnitStrings(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v18

    .line 471
    .local v18, "units":[Ljava/lang/String;
    invoke-static/range {v16 .. v18}, Lcom/google/android/music/utils/IOUtils;->readableFileSize(J[Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v15

    .line 472
    .local v15, "sizeFormatArgs":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const v19, 0x7f0b036a

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    iget-object v0, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    const/16 v21, 0x1

    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 476
    .end local v15    # "sizeFormatArgs":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "containerSize":J
    .end local v18    # "units":[Ljava/lang/String;
    :cond_2
    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-eqz v19, :cond_6

    const/4 v14, 0x0

    .line 478
    .local v14, "remoteId":Ljava/lang/String;
    :goto_4
    sget-object v19, Lcom/google/android/music/ui/ManageDownloadsFragment$5;->$SwitchMap$com$google$android$music$ui$ManageDownloadsFragment$ContainerType:[I

    invoke-virtual {v12}, Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;->ordinal()I

    move-result v20

    aget v19, v19, v20

    packed-switch v19, :pswitch_data_0

    .line 497
    :goto_5
    new-instance v5, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    .end local v5    # "item":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    invoke-direct/range {v5 .. v14}, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;-><init>(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    .restart local v5    # "item":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    invoke-virtual {v4, v5}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 463
    .end local v8    # "subType":I
    .end local v9    # "title":Ljava/lang/String;
    .end local v10    # "subtitle":Ljava/lang/String;
    .end local v11    # "size":Ljava/lang/String;
    .end local v13    # "urls":Ljava/lang/String;
    .end local v14    # "remoteId":Ljava/lang/String;
    :cond_3
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 464
    .restart local v9    # "title":Ljava/lang/String;
    :cond_4
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    goto/16 :goto_2

    .line 465
    .restart local v8    # "subType":I
    :cond_5
    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_3

    .line 476
    .restart local v10    # "subtitle":Ljava/lang/String;
    .restart local v11    # "size":Ljava/lang/String;
    .restart local v13    # "urls":Ljava/lang/String;
    :cond_6
    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto :goto_4

    .line 480
    .restart local v14    # "remoteId":Ljava/lang/String;
    :pswitch_0
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-eqz v19, :cond_7

    const/4 v10, 0x0

    .line 481
    :goto_6
    goto :goto_5

    .line 480
    :cond_7
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_6

    .line 483
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b02cb

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 485
    goto :goto_5

    .line 487
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b02cb

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 489
    goto :goto_5

    .line 491
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v19

    if-eqz v19, :cond_8

    const v19, 0x7f0b02cc

    :goto_7
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_5

    :cond_8
    const v19, 0x7f0b02cd

    goto :goto_7

    .line 478
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private showMenuInActionBar()Z
    .locals 1

    .prologue
    .line 388
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    return v0
.end method

.method private updateEmptyScreenVisibility()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 366
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-virtual {v4}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    move v0, v3

    .line 367
    .local v0, "isEmpty":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setEmptyScreenVisible(Z)V

    .line 368
    if-eqz v0, :cond_4

    .line 369
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 370
    iget-boolean v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsViewCreated:Z

    if-eqz v3, :cond_1

    .line 371
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 372
    .local v1, "lv":Landroid/widget/ListView;
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 382
    .end local v1    # "lv":Landroid/widget/ListView;
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->showMenuInActionBar()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 383
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 385
    :cond_2
    return-void

    .end local v0    # "isEmpty":Z
    :cond_3
    move v0, v2

    .line 366
    goto :goto_0

    .line 375
    .restart local v0    # "isEmpty":Z
    :cond_4
    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 376
    iget-boolean v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsViewCreated:Z

    if-eqz v2, :cond_1

    .line 377
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 378
    .restart local v1    # "lv":Landroid/widget/ListView;
    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    goto :goto_1
.end method

.method private updateHeaderButton()V
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButtonText:Landroid/widget/TextView;

    const v1, 0x7f0b02c9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButtonIcon:Landroid/widget/ImageView;

    const v1, 0x7f02014b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 275
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButtonText:Landroid/widget/TextView;

    const v1, 0x7f0b02c7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 273
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButtonIcon:Landroid/widget/ImageView;

    const v1, 0x7f02012b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateList(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 444
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ManageDownloadsFragment;->loadFromCursor(Landroid/database/Cursor;)Ljava/util/LinkedHashSet;

    move-result-object v1

    .line 445
    .local v1, "newEntries":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;"
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/ManageDownloadsFragment;->calculateDifferences(Ljava/util/LinkedHashSet;)Landroid/util/Pair;

    move-result-object v0

    .line 446
    .local v0, "delta":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;Ljava/util/List<Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;>;>;"
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->applyDifferences(Landroid/util/Pair;)V

    .line 447
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->notifyDataSetChanged()V

    .line 448
    return-void
.end method

.method private updateProgress()V
    .locals 1

    .prologue
    .line 278
    new-instance v0, Lcom/google/android/music/ui/ManageDownloadsFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/ManageDownloadsFragment$2;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 363
    return-void
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Lcom/google/android/music/ui/BaseListFragment;->initEmptyScreen()V

    .line 216
    const v0, 0x7f040030

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setEmptyScreen(I)V

    .line 217
    const v0, 0x7f0b02a4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setEmptyScreenText(I)V

    .line 218
    const v0, 0x7f02017f

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setEmptyImageView(I)V

    .line 219
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 223
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 225
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getEmptyScreen()Landroid/view/View;

    move-result-object v1

    .line 226
    .local v1, "emptyView":Landroid/view/View;
    const v3, 0x7f0e0194

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mFreeSizeTextViewEmptyScreen:Landroid/widget/TextView;

    .line 227
    const v3, 0x7f0e018b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProgressBarEmptyScreen:Landroid/widget/ProgressBar;

    .line 228
    const v3, 0x7f0e011e

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 229
    .local v0, "downloadButton":Landroid/view/View;
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0165

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 234
    .local v2, "min":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;

    invoke-static {v3, v4, v2}, Lcom/google/android/music/utils/ViewUtils;->setWidthToThirdOfScreen(Landroid/content/Context;Landroid/view/ViewGroup;I)I

    .line 240
    .end local v2    # "min":I
    :cond_0
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setListShown(Z)V

    .line 241
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->updateProgress()V

    .line 242
    return-void

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mDownloadButton:Landroid/view/ViewGroup;

    invoke-static {v3, v4}, Lcom/google/android/music/utils/ViewUtils;->setWidthToShortestEdge(Landroid/content/Context;Landroid/view/ViewGroup;)I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 184
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 185
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setHasOptionsMenu(Z)V

    .line 186
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mUnavailableCardOpacity:F

    .line 188
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424
    new-instance v0, Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/ManageDownloadsFragment;->QUERY_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/KeepOnDownloadCursorLoader;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const v2, 0x7f0e02a0

    .line 393
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    if-nez v0, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->showMenuInActionBar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 398
    const/high16 v0, 0x7f140000

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 399
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 400
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0b02ca

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 401
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02014a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 406
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 407
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 403
    :cond_2
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0b02c8

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 404
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02012a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsViewCreated:Z

    .line 210
    invoke-super {p0}, Lcom/google/android/music/ui/BaseListFragment;->onDestroyView()V

    .line 211
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 429
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-boolean v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsViewCreated:Z

    if-eqz v0, :cond_0

    .line 430
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/ManageDownloadsFragment;->updateList(Landroid/database/Cursor;)V

    .line 431
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->updateEmptyScreenVisibility()V

    .line 433
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 54
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/ManageDownloadsFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 437
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mListBackingData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 438
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mProcessedData:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 439
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->notifyDataSetChanged()V

    .line 440
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 413
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 419
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 415
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->handlePauseResumeToggle()V

    .line 416
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->supportInvalidateOptionsMenu()V

    .line 417
    const/4 v0, 0x1

    goto :goto_0

    .line 413
    :pswitch_data_0
    .packed-switch 0x7f0e02a0
        :pswitch_0
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 192
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 194
    .local v0, "listView":Landroid/widget/ListView;
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 195
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 196
    const/high16 v2, 0x2000000

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 197
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->addHeaderAndFooter()V

    .line 198
    new-instance v2, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-direct {v2, p0, v5}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;-><init>(Lcom/google/android/music/ui/ManageDownloadsFragment;Lcom/google/android/music/ui/ManageDownloadsFragment$1;)V

    iput-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    .line 199
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/ManageDownloadsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 201
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 202
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    invoke-virtual {v1, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 204
    iput-boolean v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment;->mIsViewCreated:Z

    .line 205
    return-void
.end method
