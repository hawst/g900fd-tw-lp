.class public Lcom/google/android/music/leanback/LeanbackPlayActivity;
.super Lcom/google/android/music/leanback/LeanbackItemActivity;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/SongRowPresenter$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;,
        Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaySongRow;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCurrentQueuePosition:I

.field private mCurrentSongList:Lcom/google/android/music/medialist/SongList;

.field private mCurrentTrackId:J

.field private mCurrentTrackName:Ljava/lang/String;

.field private mDuration:J

.field private final mHandler:Landroid/os/Handler;

.field private mIsInInfiniteMixMode:Z

.field private mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

.field private mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

.field private mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

.field private mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

.field private mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

.field private mPlaybackServiceConnection:Landroid/content/ServiceConnection;

.field private mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

.field private mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

.field private mRadioAction:Lcom/google/android/music/leanback/IntentAction;

.field private mRating:I

.field private mRefreshDelayMs:J

.field private mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

.field private mRepeatMode:I

.field private mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

.field private mService:Lcom/google/android/music/playback/IMusicPlaybackService;

.field private mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

.field private mShuffleMode:I

.field private mStatusListener:Landroid/content/BroadcastReceiver;

.field private mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

.field private mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->LOGV:Z

    .line 109
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;-><init>()V

    .line 160
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentQueuePosition:I

    .line 162
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    .line 187
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    .line 205
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$3;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mStatusListener:Landroid/content/BroadcastReceiver;

    .line 993
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->playMusic(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->LOGV:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->toggleShuffle()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->cycleRepeat()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/IntentAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/leanback/IntentAction;)Lcom/google/android/music/leanback/IntentAction;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # Lcom/google/android/music/leanback/IntentAction;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/music/leanback/LeanbackPlayActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # I

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->changeRating(I)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/leanback/LeanbackPlayActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentQueuePosition:I

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p2, "x2"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Landroid/content/Context;JLjava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;

    .prologue
    .line 72
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->setBackground(Landroid/content/Context;JLjava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/playback/IMusicPlaybackService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # Lcom/google/android/music/playback/IMusicPlaybackService;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/NowPlayingItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfo()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/LeanbackPlayActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->refreshNow()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/LeanbackPlayActivity;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # J

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->queueNextRefresh(J)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfoImpl(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfo(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private changeRating(I)V
    .locals 3
    .param p1, "rating"    # I

    .prologue
    .line 511
    iget v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    if-ne v1, p1, :cond_0

    .line 513
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    .line 518
    :goto_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    .line 520
    .local v0, "ratingToSet":I
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/leanback/LeanbackPlayActivity$7;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$7;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;I)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 532
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateThumbs()V

    .line 533
    return-void

    .line 516
    .end local v0    # "ratingToSet":I
    :cond_0
    iput p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    goto :goto_0
.end method

.method private createSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 5
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    const/4 v4, 0x0

    .line 759
    if-nez p1, :cond_1

    .line 760
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v1, :cond_2

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 763
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p1, v1}, Lcom/google/android/music/medialist/SongList;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 767
    :cond_2
    if-nez p1, :cond_3

    .line 768
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v2, "New Song list: null"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    :goto_1
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentSongList:Lcom/google/android/music/medialist/SongList;

    .line 775
    if-nez p1, :cond_4

    .line 776
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v2, "The play queue not available."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->setSongListObjectAdapter(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;)V

    goto :goto_0

    .line 770
    :cond_3
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New Song list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/music/medialist/SongList;->freeze()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 781
    :cond_4
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity$8;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/medialist/SongList;)V

    .line 802
    .local v0, "adapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->setSongListObjectAdapter(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;)V

    .line 803
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 804
    sget-object v1, Lcom/google/android/music/xdi/TrackAdapter;->TRACK_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v4, v0, p1, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->loadMediaList(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method private cycleRepeat()V
    .locals 3

    .prologue
    .line 452
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v1, :cond_0

    .line 473
    :goto_0
    return-void

    .line 456
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getRepeatMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 465
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatMode:I

    .line 468
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatMode:I

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->setRepeatMode(I)V

    .line 469
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateRepeat()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 470
    :catch_0
    move-exception v0

    .line 471
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 458
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_0
    const/4 v1, 0x1

    :try_start_1
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatMode:I

    goto :goto_1

    .line 461
    :pswitch_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatMode:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 456
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I
    .locals 3
    .param p1, "objectAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p2, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 393
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v1

    .local v1, "size":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 394
    invoke-virtual {p1, v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p2, v2, :cond_0

    .line 398
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 393
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private handleSongId(JLjava/lang/String;)Z
    .locals 7
    .param p1, "id"    # J
    .param p3, "trackName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 885
    iget-wide v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentTrackId:J

    cmp-long v3, p1, v4

    if-nez v3, :cond_2

    .line 886
    if-nez p3, :cond_1

    .line 887
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentTrackName:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 929
    :cond_0
    :goto_0
    return v2

    .line 890
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentTrackName:Ljava/lang/String;

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 894
    :cond_2
    iput-wide p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentTrackId:J

    .line 895
    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentTrackName:Ljava/lang/String;

    .line 896
    sget-object v2, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "New Track: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    invoke-static {p1, p2, p3}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 900
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/SingleSongList;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .line 901
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v2, 0x2

    invoke-static {p0, v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->loadArtistArtwork(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/SongList;I)V

    .line 902
    new-instance v2, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;

    invoke-direct {v2, p0, v1, p3}, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 929
    const/4 v2, 0x1

    goto :goto_0
.end method

.method static loadArtistArtwork(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/SongList;I)V
    .locals 2
    .param p0, "leanbackItemActivity"    # Lcom/google/android/music/leanback/LeanbackItemActivity;
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "loaderId"    # I

    .prologue
    .line 934
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$10;-><init>(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/SongList;I)V

    .line 968
    .local v0, "itemGetter":Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetter;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getBackgroundImageMessageHandler()Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessageImmediate(Ljava/lang/Object;)V

    .line 970
    return-void
.end method

.method private playMusic(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 638
    sget-boolean v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->LOGV:Z

    if-eqz v0, :cond_0

    .line 639
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v1, "playMusic"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_1

    .line 642
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->processIntentImpl(Landroid/content/Intent;)Z

    .line 646
    :goto_0
    return-void

    .line 644
    :cond_1
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v1, "Playback service not initialized."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z
    .locals 6
    .param p1, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    const/4 v2, 0x0

    .line 699
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->toString()Ljava/lang/String;

    move-result-object v0

    .line 700
    .local v0, "mixString":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Starting playback of radio.  mix: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v3, :cond_0

    .line 702
    sget-object v3, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v4, "Playback service not initialized."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    :goto_0
    return v2

    .line 706
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v3, p1}, Lcom/google/android/music/playback/IMusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 712
    const/4 v2, 0x1

    goto :goto_0

    .line 707
    :catch_0
    move-exception v1

    .line 708
    .local v1, "re":Landroid/os/RemoteException;
    sget-object v3, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while playing radio.  mix: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processIntentImpl(Landroid/content/Intent;)Z
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 649
    sget-boolean v6, Lcom/google/android/music/leanback/LeanbackPlayActivity;->LOGV:Z

    if-eqz v6, :cond_0

    .line 650
    sget-object v6, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Processing intent: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    :cond_0
    const-string v6, "document"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 654
    const-string v5, "document"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 655
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    const/4 v5, 0x0

    invoke-static {p0, v0, v5}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 695
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_1
    :goto_0
    return v4

    .line 659
    :cond_2
    const-string v6, "is_now_playing"

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_1

    .line 663
    const-string v6, "song_list"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 664
    const-string v6, "song_list"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/medialist/SongList;

    .line 665
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    const-string v6, "play_mode"

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 666
    .local v2, "playMode":I
    packed-switch v2, :pswitch_data_0

    .line 677
    sget-object v5, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported play mode specified for song list "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 668
    :pswitch_0
    const-string v6, "offset"

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v3, v5}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    goto :goto_0

    .line 671
    :pswitch_1
    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 674
    :pswitch_2
    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->queue(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 683
    .end local v2    # "playMode":I
    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_3
    const-string v6, "mix_descriptor"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 684
    const-string v5, "mix_descriptor"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/mix/MixDescriptor;

    .line 685
    .local v1, "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    invoke-direct {p0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z

    goto :goto_0

    .line 689
    .end local v1    # "mixDescriptor":Lcom/google/android/music/mix/MixDescriptor;
    :cond_4
    const-string v6, "shuffle_all"

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 690
    sget-object v5, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v6, "Starting shuffle all my songs"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->shuffleAll()V

    goto :goto_0

    :cond_5
    move v4, v5

    .line 695
    goto :goto_0

    .line 666
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private queueNextRefresh(J)V
    .locals 3
    .param p1, "delay"    # J

    .prologue
    const/4 v2, 0x1

    .line 632
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 633
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 634
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 635
    return-void
.end method

.method private refreshNow()J
    .locals 12

    .prologue
    const-wide/16 v6, 0x1f4

    .line 716
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v8, :cond_0

    move-wide v4, v6

    .line 755
    :goto_0
    return-wide v4

    .line 721
    :cond_0
    :try_start_0
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v8}, Lcom/google/android/music/playback/IMusicPlaybackService;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v1

    .line 724
    .local v1, "state":Lcom/google/android/music/playback/PlaybackState;
    invoke-virtual {v1}, Lcom/google/android/music/playback/PlaybackState;->isPreparing()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v1}, Lcom/google/android/music/playback/PlaybackState;->isStreaming()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 725
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    sget v9, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 726
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->isFadingEnabled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 727
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setFadingEnabled(Z)V

    .line 741
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    .line 743
    invoke-virtual {v1}, Lcom/google/android/music/playback/PlaybackState;->getPosition()J

    move-result-wide v2

    .line 744
    .local v2, "pos":J
    iget-wide v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRefreshDelayMs:J

    iget-wide v10, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRefreshDelayMs:J

    rem-long v10, v2, v10

    sub-long v4, v8, v10

    .line 746
    .local v4, "remaining":J
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    iget-wide v10, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mDuration:J

    long-to-int v9, v10

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->setTotalTime(I)V

    .line 747
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    long-to-int v9, v2

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->setCurrentTime(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 752
    .end local v1    # "state":Lcom/google/android/music/playback/PlaybackState;
    .end local v2    # "pos":J
    .end local v4    # "remaining":J
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v8, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v4, v6

    .line 755
    goto :goto_0

    .line 729
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/music/playback/PlaybackState;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 730
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    sget v9, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 731
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->isFadingEnabled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 732
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setFadingEnabled(Z)V

    goto :goto_1

    .line 735
    :cond_3
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    sget v9, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 736
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->isFadingEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->isResumed()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 738
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setFadingEnabled(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private static setBackground(Landroid/content/Context;JLjava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistId"    # J
    .param p3, "artistMetajamId"    # Ljava/lang/String;
    .param p4, "itemGetterListener"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;

    .prologue
    .line 974
    new-instance v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/LeanbackPlayActivity$11;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/leanback/BackgroundImageMessageHandler$ItemGetterListener;J)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 991
    return-void
.end method

.method private toggleShuffle()V
    .locals 3

    .prologue
    .line 402
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v1, :cond_0

    .line 420
    :goto_0
    return-void

    .line 406
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->getShuffleMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 412
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleMode:I

    .line 415
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleMode:I

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->setShuffleMode(I)V

    .line 416
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateShuffle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 417
    :catch_0
    move-exception v0

    .line 418
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 408
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_0
    const/4 v1, 0x0

    :try_start_1
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleMode:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 406
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private updateRepeat()V
    .locals 4

    .prologue
    .line 476
    iget-boolean v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mIsInInfiniteMixMode:Z

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->remove(Ljava/lang/Object;)Z

    .line 503
    :goto_0
    return-void

    .line 481
    :cond_0
    iget v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatMode:I

    packed-switch v1, :pswitch_data_0

    .line 492
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v2, "Repeat is now REPEAT NONE!"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    sget v2, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->NONE:I

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->setIndex(I)V

    .line 496
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v0

    .line 497
    .local v0, "index":I
    if-ltz v0, :cond_1

    .line 498
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    goto :goto_0

    .line 483
    .end local v0    # "index":I
    :pswitch_0
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v2, "Repeat is now REPEAT ALL!"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    sget v2, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ALL:I

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->setIndex(I)V

    goto :goto_1

    .line 487
    :pswitch_1
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v2, "Repeat is now REPEAT CURRENT!"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    sget v2, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->ONE:I

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->setIndex(I)V

    goto :goto_1

    .line 501
    .restart local v0    # "index":I
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    invoke-virtual {v1, v2, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 481
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateShuffle()V
    .locals 5

    .prologue
    .line 423
    iget-boolean v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mIsInInfiniteMixMode:Z

    if-eqz v2, :cond_0

    .line 424
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->remove(Ljava/lang/Object;)Z

    .line 449
    :goto_0
    return-void

    .line 427
    :cond_0
    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleMode:I

    packed-switch v2, :pswitch_data_0

    .line 434
    sget-object v2, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v3, "Shuffle is now SHUFFLE NONE!"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    sget v3, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->OFF:I

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->setIndex(I)V

    .line 438
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v0

    .line 439
    .local v0, "index":I
    if-ltz v0, :cond_1

    .line 440
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    invoke-direct {p0, v3, v4}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    goto :goto_0

    .line 429
    .end local v0    # "index":I
    :pswitch_0
    sget-object v2, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v3, "Shuffle is now SHUFFLE NORMAL!"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    sget v3, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->ON:I

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->setIndex(I)V

    goto :goto_1

    .line 443
    .restart local v0    # "index":I
    :cond_1
    const/4 v1, 0x1

    .line 444
    .local v1, "shuffleIndex":I
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v2

    if-gez v2, :cond_2

    .line 445
    add-int/lit8 v1, v1, -0x1

    .line 447
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    invoke-virtual {v2, v1, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private updateThumbs()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 536
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    packed-switch v0, :pswitch_data_0

    .line 553
    :pswitch_0
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rating is now:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    .line 558
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    .line 560
    return-void

    .line 538
    :pswitch_1
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v1, "Rating is now THUMBS DOWN!"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->SOLID:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->setIndex(I)V

    .line 540
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;->setIndex(I)V

    goto :goto_0

    .line 543
    :pswitch_2
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v1, "Rating is now THUMBS UP!"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->setIndex(I)V

    .line 545
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->SOLID:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;->setIndex(I)V

    goto :goto_0

    .line 548
    :pswitch_3
    sget-object v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v1, "Rating is now NOT RATED!"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->setIndex(I)V

    .line 550
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    sget v1, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;->setIndex(I)V

    goto :goto_0

    .line 536
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private updateTrackInfo()V
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v0, :cond_0

    .line 622
    :goto_0
    return-void

    .line 620
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.metachanged"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-static {v0, v1, p0}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtras(Landroid/content/Intent;Lcom/google/android/music/playback/IMusicPlaybackService;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfo(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private updateTrackInfo(Landroid/content/Intent;)V
    .locals 4
    .param p1, "sourceIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x2

    .line 625
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 626
    .local v0, "numsg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 627
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 628
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 629
    return-void
.end method

.method private updateTrackInfoImpl(Landroid/content/Intent;)V
    .locals 22
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 811
    const-string v17, "currentSongLoaded"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 812
    new-instance v14, Lcom/google/android/music/download/ContentIdentifier;

    const-string v17, "id"

    const-wide/16 v18, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v18

    invoke-static {}, Lcom/google/android/music/download/ContentIdentifier$Domain;->values()[Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v17

    const-string v20, "domain"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    aget-object v17, v17, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, v17

    invoke-direct {v14, v0, v1, v2}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 817
    .local v14, "songId":Lcom/google/android/music/download/ContentIdentifier;
    const-string v17, "artist"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 818
    .local v5, "artistName":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 819
    const v17, 0x7f0b00c4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 822
    :cond_0
    const-string v17, "album"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 823
    .local v4, "albumName":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 824
    const v17, 0x7f0b00c5

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 827
    :cond_1
    const-string v17, "track"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 829
    .local v16, "trackName":Ljava/lang/String;
    const-string v17, "duration"

    const-wide/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mDuration:J

    .line 832
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/playback/IMusicPlaybackService;->getQueuePosition()I

    move-result v10

    .line 833
    .local v10, "queuePosition":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentQueuePosition:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v10, :cond_2

    .line 834
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentQueuePosition:I

    .line 835
    .local v9, "oldQueuePosition":I
    move-object/from16 v0, p0

    iput v10, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentQueuePosition:I

    .line 836
    sget-object v17, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Queue position is now "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mCurrentQueuePosition:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->onQueuePositionChanged(I)V

    .line 838
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->onQueuePositionChanged(I)V

    .line 841
    .end local v9    # "oldQueuePosition":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/playback/IMusicPlaybackService;->isInIniniteMixMode()Z

    move-result v8

    .line 842
    .local v8, "isInInfiniteMixMode":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mIsInInfiniteMixMode:Z

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v8, :cond_b

    const/4 v7, 0x1

    .line 843
    .local v7, "infiniteModeChanged":Z
    :goto_0
    if-eqz v7, :cond_3

    .line 844
    move-object/from16 v0, p0

    iput-boolean v8, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mIsInInfiniteMixMode:Z

    .line 847
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/playback/IMusicPlaybackService;->getShuffleMode()I

    move-result v13

    .line 848
    .local v13, "shuffleMode":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleMode:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v13, v0, :cond_4

    if-eqz v7, :cond_5

    .line 849
    :cond_4
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleMode:I

    .line 850
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateShuffle()V

    .line 853
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/playback/IMusicPlaybackService;->getRepeatMode()I

    move-result v12

    .line 854
    .local v12, "repeatMode":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatMode:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v12, v0, :cond_6

    if-eqz v7, :cond_7

    .line 855
    :cond_6
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatMode:I

    .line 856
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateRepeat()V

    .line 859
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/playback/IMusicPlaybackService;->getRating()I

    move-result v11

    .line 860
    .local v11, "rating":I
    const/16 v17, 0x1

    move/from16 v0, v17

    if-eq v11, v0, :cond_8

    const/16 v17, 0x5

    move/from16 v0, v17

    if-eq v11, v0, :cond_8

    .line 862
    invoke-static {v11}, Lcom/google/android/music/RatingSelector;->convertRatingToThumbs(I)I

    move-result v11

    .line 864
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v11, :cond_9

    .line 865
    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRating:I

    .line 866
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateThumbs()V

    .line 868
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/playback/IMusicPlaybackService;->getPlaybackState()Lcom/google/android/music/playback/PlaybackState;

    move-result-object v15

    .line 869
    .local v15, "state":Lcom/google/android/music/playback/PlaybackState;
    if-eqz v15, :cond_c

    invoke-virtual {v15}, Lcom/google/android/music/playback/PlaybackState;->hasValidPlaylist()Z

    move-result v17

    if-eqz v17, :cond_c

    invoke-virtual {v15}, Lcom/google/android/music/playback/PlaybackState;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v17

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->createSongList(Lcom/google/android/music/medialist/SongList;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    .end local v7    # "infiniteModeChanged":Z
    .end local v8    # "isInInfiniteMixMode":Z
    .end local v10    # "queuePosition":I
    .end local v11    # "rating":I
    .end local v12    # "repeatMode":I
    .end local v13    # "shuffleMode":I
    .end local v15    # "state":Lcom/google/android/music/playback/PlaybackState;
    :goto_2
    invoke-virtual {v14}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-object/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->handleSongId(JLjava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 878
    sget-object v17, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Artist: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    sget-object v17, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Album:  "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    .end local v4    # "albumName":Ljava/lang/String;
    .end local v5    # "artistName":Ljava/lang/String;
    .end local v14    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    .end local v16    # "trackName":Ljava/lang/String;
    :cond_a
    return-void

    .line 842
    .restart local v4    # "albumName":Ljava/lang/String;
    .restart local v5    # "artistName":Ljava/lang/String;
    .restart local v8    # "isInInfiniteMixMode":Z
    .restart local v10    # "queuePosition":I
    .restart local v14    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v16    # "trackName":Ljava/lang/String;
    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 869
    .restart local v7    # "infiniteModeChanged":Z
    .restart local v11    # "rating":I
    .restart local v12    # "repeatMode":I
    .restart local v13    # "shuffleMode":I
    .restart local v15    # "state":Lcom/google/android/music/playback/PlaybackState;
    :cond_c
    const/16 v17, 0x0

    goto :goto_1

    .line 871
    .end local v7    # "infiniteModeChanged":Z
    .end local v8    # "isInInfiniteMixMode":Z
    .end local v10    # "queuePosition":I
    .end local v11    # "rating":I
    .end local v12    # "repeatMode":I
    .end local v13    # "shuffleMode":I
    .end local v15    # "state":Lcom/google/android/music/playback/PlaybackState;
    :catch_0
    move-exception v6

    .line 873
    .local v6, "e1":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    .line 874
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->createSongList(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_2
.end method


# virtual methods
.method protected createFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 244
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-direct {v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .line 245
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->setupFragment(Landroid/app/Fragment;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 579
    sget-boolean v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->LOGV:Z

    if-eqz v1, :cond_0

    .line 580
    sget-object v1, Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->onCreate(Landroid/os/Bundle;)V

    .line 583
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v1}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 585
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 586
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 587
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 588
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 589
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 590
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 591
    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 592
    const-string v1, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 593
    const-string v1, "com.google.android.music.refreshcomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 594
    const-string v1, "com.google.android.music.refreshfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 595
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 598
    const v1, 0x7f0e00d0

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->getRefreshDelay(Landroid/view/View;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRefreshDelayMs:J

    .line 599
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getBackgroundImageMessageHandler()Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/leanback/NowPlayingItem;->init(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)V

    .line 600
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 604
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/NowPlayingItem;->cleanup()V

    .line 605
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 606
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 607
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 608
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V

    .line 611
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 612
    invoke-super {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->onDestroy()V

    .line 613
    return-void
.end method

.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 0
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 575
    return-void
.end method

.method public onSongRowClicked(Lcom/google/android/music/leanback/SongRow;)V
    .locals 0
    .param p1, "songRow"    # Lcom/google/android/music/leanback/SongRow;

    .prologue
    .line 564
    invoke-virtual {p1}, Lcom/google/android/music/leanback/SongRow;->onClicked()V

    .line 565
    return-void
.end method

.method public onSongRowRadioClicked(Lcom/google/android/music/leanback/SongRow;)V
    .locals 0
    .param p1, "songRow"    # Lcom/google/android/music/leanback/SongRow;

    .prologue
    .line 569
    invoke-virtual {p1}, Lcom/google/android/music/leanback/SongRow;->onRadioClicked()V

    .line 570
    return-void
.end method

.method protected setupFragment(Landroid/app/Fragment;)V
    .locals 11
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    const/4 v10, 0x0

    .line 251
    check-cast p1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .end local p1    # "fragment":Landroid/app/Fragment;
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .line 253
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v7, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 254
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v7, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 256
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c00bb

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 257
    .local v6, "primaryColor":I
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c00c5

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 259
    .local v1, "highlightColor":I
    new-instance v4, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;

    invoke-direct {v4}, Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;-><init>()V

    .line 260
    .local v4, "presenterSelector":Landroid/support/v17/leanback/widget/ControlButtonPresenterSelector;
    new-instance v7, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-direct {v7, p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    .line 261
    new-instance v7, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    invoke-direct {v7, p0, v1, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;-><init>(Landroid/content/Context;II)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    .line 262
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    sget v8, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->NONE:I

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;->setIndex(I)V

    .line 263
    new-instance v5, Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;-><init>(Landroid/content/Context;)V

    .line 265
    .local v5, "prevAction":Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;
    new-instance v2, Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;

    invoke-direct {v2, p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;-><init>(Landroid/content/Context;)V

    .line 267
    .local v2, "nextAction":Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;
    new-instance v7, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    invoke-direct {v7, p0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;-><init>(Landroid/content/Context;I)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    .line 268
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    sget v8, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->OFF:I

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;->setIndex(I)V

    .line 269
    new-instance v7, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    invoke-direct {v7, p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    .line 270
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    sget v8, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;->setIndex(I)V

    .line 271
    new-instance v7, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    invoke-direct {v7, p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    .line 272
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    sget v8, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsAction;->OUTLINE:I

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;->setIndex(I)V

    .line 273
    new-instance v7, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-direct {v7, v4}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    .line 274
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v7, v5}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 275
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlayPauseAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 276
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v7, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 278
    new-instance v7, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-direct {v7, v4}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    .line 279
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 280
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 281
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsDownAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsDownAction;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 282
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mThumbsUpAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ThumbsUpAction;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 285
    new-instance v3, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    new-instance v7, Lcom/google/android/music/leanback/LeanbackPlayActivity$4;

    invoke-direct {v7, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$4;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    invoke-direct {v3, v7}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 311
    .local v3, "playbackControlsRowPresenter":Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;
    new-instance v7, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;

    invoke-direct {v7, p0, v5, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$5;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipPreviousAction;Landroid/support/v17/leanback/widget/PlaybackControlsRow$SkipNextAction;)V

    invoke-virtual {v3, v7}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->setOnActionClickedListener(Landroid/support/v17/leanback/widget/OnActionClickedListener;)V

    .line 361
    invoke-virtual {v3, v10}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->setSecondaryActionsHidden(Z)V

    .line 362
    invoke-virtual {v3, v6}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->setBackgroundColor(I)V

    .line 363
    invoke-virtual {v3, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->setProgressColor(I)V

    .line 365
    new-instance v0, Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;-><init>()V

    .line 366
    .local v0, "classPresenterSelector":Landroid/support/v17/leanback/widget/ClassPresenterSelector;
    const-class v7, Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    invoke-virtual {v0, v7, v3}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 368
    const-class v7, Landroid/support/v17/leanback/widget/ListRow;

    new-instance v8, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v8}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-virtual {v0, v7, v8}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 369
    const-class v7, Lcom/google/android/music/leanback/SongRow;

    new-instance v8, Lcom/google/android/music/leanback/SongRowPresenter;

    invoke-direct {v8, p0}, Lcom/google/android/music/leanback/SongRowPresenter;-><init>(Lcom/google/android/music/leanback/SongRowPresenter$Listener;)V

    invoke-virtual {v0, v7, v8}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 370
    const-class v7, Lcom/google/android/music/leanback/SongTitleRow;

    new-instance v8, Lcom/google/android/music/leanback/SongTitleRowPresenter;

    invoke-direct {v8}, Lcom/google/android/music/leanback/SongTitleRowPresenter;-><init>()V

    invoke-virtual {v0, v7, v8}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 371
    const-class v7, Lcom/google/android/music/leanback/SongFooterRow;

    new-instance v8, Lcom/google/android/music/leanback/SongFooterRowPresenter;

    invoke-direct {v8}, Lcom/google/android/music/leanback/SongFooterRowPresenter;-><init>()V

    invoke-virtual {v0, v7, v8}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 373
    new-instance v7, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    new-instance v8, Lcom/google/android/music/leanback/SongTitleRow;

    const v9, 0x7f0b02f1

    invoke-virtual {p0, v9}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/music/leanback/SongTitleRow;-><init>(Ljava/lang/String;)V

    new-instance v9, Lcom/google/android/music/leanback/SongFooterRow;

    invoke-direct {v9}, Lcom/google/android/music/leanback/SongFooterRow;-><init>()V

    invoke-direct {v7, p0, v0, v8, v9}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    .line 375
    new-instance v7, Lcom/google/android/music/leanback/NowPlayingItem;

    const/4 v8, 0x3

    iget-object v9, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    invoke-direct {v7, p0, v8, v9}, Lcom/google/android/music/leanback/NowPlayingItem;-><init>(Lcom/google/android/music/leanback/LeanbackItemActivity;ILcom/google/android/music/leanback/NowPlayingItem$Listener;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    .line 377
    new-instance v7, Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-direct {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;-><init>(Ljava/lang/Object;)V

    iput-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    .line 378
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8, v10}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 379
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->setPrimaryActionsAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 380
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->setSecondaryActionsAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 381
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    new-instance v8, Lcom/google/android/music/leanback/LeanbackPlayActivity$6;

    invoke-direct {v8, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$6;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setFadeCompleteListener(Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;)V

    .line 389
    iget-object v7, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackOverlayFragment:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackDetailsObjectAdapter:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    invoke-virtual {v7, v8}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 390
    return-void
.end method
