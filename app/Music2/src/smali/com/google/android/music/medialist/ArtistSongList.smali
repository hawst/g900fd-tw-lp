.class public Lcom/google/android/music/medialist/ArtistSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "ArtistSongList.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArtistSongList"


# instance fields
.field private mArtUrl:Ljava/lang/String;

.field private mArtistMetajamId:Ljava/lang/String;

.field private mId:J

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;IZ)V
    .locals 3
    .param p1, "artistid"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "sortOrder"    # I
    .param p5, "shouldFilter"    # Z

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p4, p5, v0}, Lcom/google/android/music/medialist/SongList;-><init>(IZZ)V

    .line 39
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid artist id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    iput-wide p1, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    .line 43
    iput-object p3, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v0, p2, p3, v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->appendArtistToPlayList(Landroid/content/ContentResolver;JJ)I

    move-result v0

    return v0
.end method

.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 177
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "hasLocal"

    aput-object v0, v2, v8

    .line 180
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 183
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    const-string v0, "ArtistSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown artist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v8

    .line 187
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    .line 190
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v8, v0

    goto :goto_0

    :cond_1
    move v0, v8

    .line 187
    goto :goto_1

    .line 190
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 158
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "hasRemote"

    aput-object v0, v2, v8

    .line 161
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 164
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    const-string v0, "ArtistSongList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown artist id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v8

    .line 168
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ne v0, v7, :cond_1

    move v0, v7

    .line 171
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v8, v0

    goto :goto_0

    :cond_1
    move v0, v8

    .line 168
    goto :goto_1

    .line 171
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getAlbumCount(Landroid/content/Context;)I
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 205
    iget-wide v4, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 207
    .local v1, "albumsOfArtistUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_count"

    aput-object v0, v2, v7

    .line 208
    .local v2, "columns":[Ljava/lang/String;
    iget-boolean v6, p0, Lcom/google/android/music/medialist/ArtistSongList;->mShouldFilter:Z

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 211
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 214
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return v7

    .line 212
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    .line 214
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 66
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/music/medialist/ArtistSongList;->getSortOrder()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/music/medialist/ArtistSongList;->mShouldFilter:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 254
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "artworkUrl"

    aput-object v0, v2, v1

    .line 257
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 259
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 261
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 262
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtUrl:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 270
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtUrl:Ljava/lang/String;

    return-object v0

    .line 264
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtUrl:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 267
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getArtistId(Landroid/content/Context;)J
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 200
    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    return-wide v0
.end method

.method public getArtistMetajamId(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 229
    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtistMetajamId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 230
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "ArtistMetajamId"

    aput-object v0, v2, v1

    .line 233
    .local v2, "cols":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 235
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 237
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtistMetajamId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    :goto_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 246
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtistMetajamId:Ljava/lang/String;

    return-object v0

    .line 240
    .restart local v2    # "cols":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mArtistMetajamId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 243
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    iget-object v2, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newArtistDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/music/medialist/ArtistSongList;->getSortParam()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "sortParam":Ljava/lang/String;
    iget-wide v2, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v2, v3, v0}, Lcom/google/android/music/store/MusicContent$Artists;->getAudioByArtistUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 95
    const/4 v2, 0x5

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v4, v0

    iget-object v8, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistsUri(J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->createAlbumIdIteratorFactoryForContentUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;

    move-result-object v10

    move-object v1, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v11, p4

    move/from16 v12, p5

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getScreenTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/music/medialist/ArtistSongList;->getArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v0, "orders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    return-object v0
.end method

.method public hasArtistArt()Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x1

    return v0
.end method

.method public hasDifferentTrackArtists(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method public hasMetaData()Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    return v0
.end method

.method public populateExternalSearchExtras(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    const-string v0, "android.intent.extra.artist"

    iget-object v1, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    :cond_0
    return-void
.end method

.method public refreshMetaData(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 128
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "artist"

    aput-object v0, v2, v8

    .line 131
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 133
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 134
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 135
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 139
    .local v7, "name":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    iput-object v7, p0, Lcom/google/android/music/medialist/ArtistSongList;->mName:Ljava/lang/String;

    .line 143
    .end local v7    # "name":Ljava/lang/String;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 145
    :cond_1
    return-void
.end method

.method public registerMetaDataObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 152
    iget-wide v2, p0, Lcom/google/android/music/medialist/ArtistSongList;->mId:J

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 153
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 154
    return-void
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 58
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    iget v1, p0, Lcom/google/android/music/medialist/ArtistSongList;->mSortOrder:I

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setArtistSongsSortOrder(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 62
    return-void

    .line 60
    :catchall_0
    move-exception v1

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v1
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    return v0
.end method
