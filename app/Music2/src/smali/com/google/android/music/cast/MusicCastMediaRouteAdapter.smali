.class public Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;
.super Ljava/lang/Object;
.source "MusicCastMediaRouteAdapter.java"

# interfaces
.implements Lcom/google/cast/MediaRouteAdapter;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mMediaRouteStateChangeListener:Lcom/google/cast/MediaRouteStateChangeListener;

.field private mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

.field private final mRegisterCallbackRunnable:Ljava/lang/Runnable;

.field private mRouteId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$1;-><init>(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)V

    iput-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    .line 54
    new-instance v0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter$2;-><init>(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)V

    iput-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRegisterCallbackRunnable:Ljava/lang/Runnable;

    .line 62
    iput-object p1, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mContext:Landroid/content/Context;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRouteId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)Lcom/google/cast/MediaRouteStateChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMediaRouteStateChangeListener:Lcom/google/cast/MediaRouteStateChangeListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->registerCallback()V

    return-void
.end method

.method private isLogVerbose()Z
    .locals 2

    .prologue
    .line 123
    iget-object v1, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 125
    .local v0, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isLogFilesEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 127
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v1

    :catchall_0
    move-exception v1

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v1
.end method

.method private registerCallback()V
    .locals 3

    .prologue
    .line 110
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 112
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMusicCastMediaRouterCallback:Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->registerMusicCastMediaRouterCallback(Lcom/google/android/music/playback/IMusicCastMediaRouterCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MCMediaRouteAdapter"

    const-string v2, "Remote call registerMusicCastMediaRouterCallback failed"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 118
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, "MCMediaRouteAdapter"

    const-string v2, "Music service is null"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onDeviceAvailable(Lcom/google/cast/CastDevice;Ljava/lang/String;Lcom/google/cast/MediaRouteStateChangeListener;)V
    .locals 2
    .param p1, "device"    # Lcom/google/cast/CastDevice;
    .param p2, "routeId"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/google/cast/MediaRouteStateChangeListener;

    .prologue
    .line 69
    iput-object p2, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRouteId:Ljava/lang/String;

    .line 70
    iput-object p3, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMediaRouteStateChangeListener:Lcom/google/cast/MediaRouteStateChangeListener;

    .line 71
    invoke-direct {p0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->isLogVerbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "MCMediaRouteAdapter"

    const-string v1, "onDeviceAvailable"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRegisterCallbackRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->addRunOnPlaybackServiceConnected(Ljava/lang/Runnable;)V

    .line 77
    return-void
.end method

.method public onSetVolume(D)V
    .locals 3
    .param p1, "volume"    # D

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->isLogVerbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "MCMediaRouteAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSetVolume: volume="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMediaRouteStateChangeListener:Lcom/google/cast/MediaRouteStateChangeListener;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mMediaRouteStateChangeListener:Lcom/google/cast/MediaRouteStateChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/google/cast/MediaRouteStateChangeListener;->onVolumeChanged(D)V

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRouteId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRouteId:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/google/android/music/utils/MusicUtils;->setMediaRouteVolume(Ljava/lang/String;D)V

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_2
    const-string v0, "MCMediaRouteAdapter"

    const-string v1, "Empty route id"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUpdateVolume(D)V
    .locals 3
    .param p1, "delta"    # D

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->isLogVerbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "MCMediaRouteAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateVolume: delta="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRouteId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/google/android/music/cast/MusicCastMediaRouteAdapter;->mRouteId:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/google/android/music/utils/MusicUtils;->updateMediaRouteVolume(Ljava/lang/String;D)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_1
    const-string v0, "MCMediaRouteAdapter"

    const-string v1, "Empty route id"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
