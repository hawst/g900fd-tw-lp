.class public Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "AddBlacklistItemsRequestJson.java"


# instance fields
.field public mSyncableBlacklistItems:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;->mSyncableBlacklistItems:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addBlacklistItem(Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;)V
    .locals 1
    .param p1, "blacklistItem"    # Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/music/cloudclient/AddBlacklistItemsRequestJson;->mSyncableBlacklistItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method
