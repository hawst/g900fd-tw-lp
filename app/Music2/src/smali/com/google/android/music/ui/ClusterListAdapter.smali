.class public Lcom/google/android/music/ui/ClusterListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ClusterListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;
    }
.end annotation


# instance fields
.field private final mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

.field private final mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private final mClusterBuilder:Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;

.field private final mClusterInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/music/ui/Cluster;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mItemTypeCount:I

.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "builder"    # Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p4, "itemTypeCount"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterBuilder:Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;

    .line 37
    iput-object p1, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mContext:Landroid/content/Context;

    .line 38
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 39
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterInfoMap:Ljava/util/HashMap;

    .line 41
    iput-object p3, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 42
    iput p4, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mItemTypeCount:I

    .line 43
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mItemTypeCount:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterInfoMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 68
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 47
    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 24
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterInfoMap:Ljava/util/HashMap;

    move-object/from16 v19, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/ui/Cluster;

    .line 75
    .local v7, "clusterInfo":Lcom/google/android/music/ui/Cluster;
    if-nez v7, :cond_0

    .line 76
    new-instance v11, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/ClusterListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 126
    :goto_0
    return-object v11

    .line 83
    :cond_0
    if-eqz p2, :cond_1

    move-object/from16 v0, p2

    instance-of v0, v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move/from16 v19, v0

    if-eqz v19, :cond_1

    const/4 v4, 0x1

    .line 85
    .local v4, "canReuse":Z
    :goto_1
    if-eqz v4, :cond_2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v19

    if-ne v0, v7, :cond_2

    move-object/from16 v11, p2

    .line 87
    goto :goto_0

    .line 83
    .end local v4    # "canReuse":Z
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 90
    .restart local v4    # "canReuse":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/ClusterListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    move-object/from16 v19, v0

    const v20, 0x7f040092

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, p3

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 93
    .local v6, "cluster":Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getNbColumns()I

    move-result v15

    .line 94
    .local v15, "nbColumns":I
    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getVisibleContent()Ljava/util/List;

    move-result-object v10

    .line 95
    .local v10, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getCardType()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v5

    .line 97
    .local v5, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v19

    add-int v19, v19, v15

    add-int/lit8 v19, v19, -0x1

    div-int v16, v19, v15

    .line 98
    .local v16, "nbRows":I
    new-instance v8, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v19

    mul-int v19, v19, v15

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v20

    mul-int v20, v20, v16

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v8, v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 101
    .local v8, "clusterMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v12, v0, :cond_3

    .line 102
    div-int v19, v12, v15

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v20

    mul-int v17, v19, v20

    .line 103
    .local v17, "row":I
    rem-int v19, v12, v15

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v20

    mul-int v9, v19, v20

    .line 104
    .local v9, "column":I
    move/from16 v0, v17

    invoke-virtual {v8, v5, v9, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    .line 101
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 107
    .end local v9    # "column":I
    .end local v17    # "row":I
    :cond_3
    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getVisibleContent()Ljava/util/List;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/ClusterListAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    move-object/from16 v20, v0

    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v6, v8, v0, v1, v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;)V

    .line 110
    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v19

    if-nez v19, :cond_4

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/ClusterListAdapter;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V

    .line 114
    :cond_4
    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 116
    const/16 v18, 0x0

    .line 118
    .local v18, "titleSecondary":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getFullContent()Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v20

    sub-int v14, v19, v20

    .line 119
    .local v14, "moreItems":I
    if-lez v14, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/ClusterListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0260

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 121
    .local v13, "more":Ljava/lang/String;
    :goto_3
    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getTitle()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v6, v0, v1, v13}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->showHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {v7}, Lcom/google/android/music/ui/Cluster;->getMoreOnClickListener()Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMoreClickHandler(Landroid/view/View$OnClickListener;)V

    .line 124
    invoke-virtual {v6, v7}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setTag(Ljava/lang/Object;)V

    move-object v11, v6

    .line 126
    goto/16 :goto_0

    .line 119
    .end local v13    # "more":Ljava/lang/String;
    :cond_5
    const/4 v13, 0x0

    goto :goto_3
.end method

.method public getViewTypeCount()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 53
    iget v1, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mItemTypeCount:I

    if-ge v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mItemTypeCount:I

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public nullCursor(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterInfoMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    invoke-virtual {p0}, Lcom/google/android/music/ui/ClusterListAdapter;->notifyDataSetChanged()V

    .line 152
    return-void
.end method

.method public swapCursor(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    .line 141
    .local v0, "id":I
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterInfoMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterBuilder:Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;

    invoke-interface {v3, v0, p2}, Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;->buildCluster(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/ClusterListAdapter;->notifyDataSetChanged()V

    .line 147
    return-void

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/ClusterListAdapter;->mClusterInfoMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
