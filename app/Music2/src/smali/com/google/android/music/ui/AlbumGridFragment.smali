.class public Lcom/google/android/music/ui/AlbumGridFragment;
.super Lcom/google/android/music/ui/MediaListGridFragment;
.source "AlbumGridFragment.java"


# instance fields
.field protected mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;-><init>()V

    .line 16
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/AlbumGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method


# virtual methods
.method protected init(Lcom/google/android/music/medialist/AlbumList;)V
    .locals 2
    .param p1, "albumlist"    # Lcom/google/android/music/medialist/AlbumList;

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->PROJECTION:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/ui/AlbumGridFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 27
    return-void
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/music/ui/AlbumsAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/AlbumGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/music/ui/AlbumsAdapter;-><init>(Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/music/ui/AlbumsAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/AlbumGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/AlbumsAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    return-object v0
.end method
