.class Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;
.super Landroid/os/Handler;
.source "BatchedImpressionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlog/BatchedImpressionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/eventlog/BatchedImpressionHelper;


# direct methods
.method constructor <init>(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;->this$0:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 85
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 99
    const-string v1, "MusicImpressions"

    const-string v2, "Unexpected message type"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :goto_0
    return-void

    .line 87
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;->this$0:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    # invokes: Lcom/google/android/music/eventlog/BatchedImpressionHelper;->createEventImpl()V
    invoke-static {v1}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->access$000(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)V

    goto :goto_0

    .line 90
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 91
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;->this$0:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    # invokes: Lcom/google/android/music/eventlog/BatchedImpressionHelper;->insertDocumentImpl(Lcom/google/android/music/ui/cardlib/model/Document;)V
    invoke-static {v1, v0}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->access$100(Lcom/google/android/music/eventlog/BatchedImpressionHelper;Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto :goto_0

    .line 94
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;->this$0:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    # getter for: Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mAllDocuments:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->access$200(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 95
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;->this$0:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    # getter for: Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mNewDocuments:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->access$300(Lcom/google/android/music/eventlog/BatchedImpressionHelper;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 96
    iget-object v1, p0, Lcom/google/android/music/eventlog/BatchedImpressionHelper$1;->this$0:Lcom/google/android/music/eventlog/BatchedImpressionHelper;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/eventlog/BatchedImpressionHelper;->mContinuous:Z
    invoke-static {v1, v2}, Lcom/google/android/music/eventlog/BatchedImpressionHelper;->access$402(Lcom/google/android/music/eventlog/BatchedImpressionHelper;Z)Z

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
