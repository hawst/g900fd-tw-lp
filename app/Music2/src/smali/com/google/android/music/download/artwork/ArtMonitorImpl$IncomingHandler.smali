.class Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "ArtMonitorImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/ArtMonitorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IncomingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V
    .locals 1

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    .line 309
    const-string v0, "IncomingHandler"

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 310
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 314
    # getter for: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$200()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 315
    const-string v2, "ArtMonitor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got msg "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 335
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got unknown what="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 320
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 321
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "remoteUrl"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 323
    .local v1, "remoteArtUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # invokes: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->onArtChanged(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$600(Lcom/google/android/music/download/artwork/ArtMonitorImpl;Ljava/lang/String;)V

    .line 337
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "remoteArtUrl":Ljava/lang/String;
    :goto_0
    return-void

    .line 326
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 327
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v2, "remoteUrl"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 329
    .restart local v1    # "remoteArtUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # invokes: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->onError(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$700(Lcom/google/android/music/download/artwork/ArtMonitorImpl;Ljava/lang/String;)V

    goto :goto_0

    .line 332
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "remoteArtUrl":Ljava/lang/String;
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;->this$0:Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    # invokes: Lcom/google/android/music/download/artwork/ArtMonitorImpl;->handleServiceIdleLocked()V
    invoke-static {v2}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->access$800(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V

    goto :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
