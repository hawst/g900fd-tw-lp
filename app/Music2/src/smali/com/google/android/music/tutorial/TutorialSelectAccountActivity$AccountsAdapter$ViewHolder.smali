.class Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "TutorialSelectAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewHolder"
.end annotation


# instance fields
.field private account:Landroid/accounts/Account;

.field final synthetic this$1:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 437
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;->this$1:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 438
    const v0, 0x7f0e00b8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 439
    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;->account:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 432
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;->account:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$AccountsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    return-object v0
.end method
