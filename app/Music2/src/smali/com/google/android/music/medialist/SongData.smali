.class public Lcom/google/android/music/medialist/SongData;
.super Lcom/google/api/client/json/GenericJson;
.source "SongData.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field public mAlbum:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "album"
    .end annotation
.end field

.field public mAlbumArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtist"
    .end annotation
.end field

.field public mAlbumArtistId:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtistId"
    .end annotation
.end field

.field public mAlbumId:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumId"
    .end annotation
.end field

.field public mArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist"
    .end annotation
.end field

.field public mArtistSort:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistSort"
    .end annotation
.end field

.field public mDomainParam:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "domainParam"
    .end annotation
.end field

.field public mDuration:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "duration"
    .end annotation
.end field

.field public mHasLocal:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "hasLocal"
    .end annotation
.end field

.field public mHasRemote:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "hasRemote"
    .end annotation
.end field

.field public mRating:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "rating"
    .end annotation
.end field

.field public mSourceId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "sourceId"
    .end annotation
.end field

.field public mTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/medialist/SongData;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static parseFromJson(Ljava/lang/String;)Lcom/google/android/music/medialist/SongData;
    .locals 8
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 94
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 95
    .local v1, "in":Ljava/io/InputStream;
    sget-object v4, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    invoke-virtual {v4, v1}, Lorg/codehaus/jackson/JsonFactory;->createJsonParser(Ljava/io/InputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v3

    .line 96
    .local v3, "parser":Lorg/codehaus/jackson/JsonParser;
    invoke-virtual {v3}, Lorg/codehaus/jackson/JsonParser;->nextToken()Lorg/codehaus/jackson/JsonToken;

    .line 97
    const-class v4, Lcom/google/android/music/medialist/SongData;

    const/4 v6, 0x0

    invoke-static {v3, v4, v6}, Lcom/google/api/client/json/Json;->parseAndClose(Lorg/codehaus/jackson/JsonParser;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/medialist/SongData;
    :try_end_0
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 109
    .end local v1    # "in":Ljava/io/InputStream;
    .end local v3    # "parser":Lorg/codehaus/jackson/JsonParser;
    :goto_0
    return-object v4

    .line 98
    :catch_0
    move-exception v2

    .line 99
    .local v2, "je":Lorg/codehaus/jackson/JsonParseException;
    sget-boolean v4, Lcom/google/android/music/medialist/SongData;->LOGV:Z

    if-eqz v4, :cond_0

    .line 100
    const-string v4, "SongData"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to parse song s="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v4, v5

    .line 103
    goto :goto_0

    .line 105
    .end local v2    # "je":Lorg/codehaus/jackson/JsonParseException;
    :catch_1
    move-exception v0

    .line 106
    .local v0, "e":Ljava/io/IOException;
    sget-boolean v4, Lcom/google/android/music/medialist/SongData;->LOGV:Z

    if-eqz v4, :cond_1

    .line 107
    const-string v4, "SongData"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to parse song s="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move-object v4, v5

    .line 109
    goto :goto_0
.end method

.method public static toJson(Lcom/google/android/music/medialist/SongData;)Ljava/lang/String;
    .locals 7
    .param p0, "song"    # Lcom/google/android/music/medialist/SongData;

    .prologue
    .line 114
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 116
    .local v0, "byteStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v4, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    sget-object v5, Lorg/codehaus/jackson/JsonEncoding;->UTF8:Lorg/codehaus/jackson/JsonEncoding;

    invoke-virtual {v4, v0, v5}, Lorg/codehaus/jackson/JsonFactory;->createJsonGenerator(Ljava/io/OutputStream;Lorg/codehaus/jackson/JsonEncoding;)Lorg/codehaus/jackson/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 119
    .local v2, "generator":Lorg/codehaus/jackson/JsonGenerator;
    :try_start_1
    invoke-static {v2, p0}, Lcom/google/api/client/json/Json;->serialize(Lorg/codehaus/jackson/JsonGenerator;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    :try_start_2
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 128
    new-instance v3, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    .line 129
    .local v3, "s":Ljava/lang/String;
    sget-boolean v4, Lcom/google/android/music/medialist/SongData;->LOGV:Z

    if-eqz v4, :cond_0

    .line 130
    const-string v4, "SongData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "toJson: s="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    .end local v2    # "generator":Lorg/codehaus/jackson/JsonGenerator;
    .end local v3    # "s":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 121
    .restart local v2    # "generator":Lorg/codehaus/jackson/JsonGenerator;
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V

    throw v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 123
    .end local v2    # "generator":Lorg/codehaus/jackson/JsonGenerator;
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "SongData"

    const-string v5, "Unable to serialize Song as JSON"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 125
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public formatAsMusicFile(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;
    .locals 2
    .param p1, "file"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    new-instance p1, Lcom/google/android/music/store/MusicFile;

    .end local p1    # "file":Lcom/google/android/music/store/MusicFile;
    invoke-direct {p1}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 76
    .restart local p1    # "file":Lcom/google/android/music/store/MusicFile;
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/store/MusicFile;->reset()V

    .line 77
    iget-object v0, p0, Lcom/google/android/music/medialist/SongData;->mArtist:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/music/store/MusicFile;->setTrackArtist(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/music/medialist/SongData;->mAlbum:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/music/store/MusicFile;->setAlbumName(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/music/medialist/SongData;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/music/store/MusicFile;->setTitle(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/music/medialist/SongData;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;)V

    .line 81
    iget-wide v0, p0, Lcom/google/android/music/medialist/SongData;->mDuration:J

    invoke-virtual {p1, v0, v1}, Lcom/google/android/music/store/MusicFile;->setDurationInMilliSec(J)V

    .line 83
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {p1, v0}, Lcom/google/android/music/store/MusicFile;->setDomain(Lcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 85
    return-object p1
.end method
