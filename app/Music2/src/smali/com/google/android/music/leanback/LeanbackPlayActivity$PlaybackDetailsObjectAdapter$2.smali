.class Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/BitmapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->onNowPlayingChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V
    .locals 0

    .prologue
    .line 1078
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapLoaded(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1081
    if-eqz p1, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$3300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    iget-object v2, v2, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1089
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->access$3400(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V

    .line 1090
    return-void

    .line 1085
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    iget-object v0, v0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$3300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;->this$1:Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    iget-object v1, v1, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
