.class public Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
.super Ljava/lang/Object;
.source "LegacyPopupMenu.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;,
        Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;
    }
.end annotation


# instance fields
.field private final mAllowCustomView:Z

.field private final mAnchor:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private final mPopupActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;",
            ">;"
        }
    .end annotation
.end field

.field private mPopupListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

.field private mPopupSelector:Lcom/google/android/music/ui/cardlib/layout/PopupSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchor"    # Landroid/view/View;
    .param p3, "allowCustomView"    # Z

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mContext:Landroid/content/Context;

    .line 160
    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mAnchor:Landroid/view/View;

    .line 161
    iput-boolean p3, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mAllowCustomView:Z

    .line 162
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    .line 163
    return-void
.end method


# virtual methods
.method public addMenuItem(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "isEnabled"    # Z
    .param p3, "customView"    # Landroid/view/View;
    .param p4, "onActionSelectedListener"    # Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;-><init>(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method public addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "isEnabled"    # Z
    .param p3, "onActionSelectedListener"    # Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2, p3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;-><init>(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    return-void
.end method

.method public addSpinnerItem()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 207
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 208
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040076

    invoke-virtual {v0, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 209
    .local v1, "spinner":Landroid/view/View;
    const-string v2, ""

    invoke-virtual {p0, v2, v3, v1, v4}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 210
    return-void
.end method

.method public clearSpinnerItem()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 214
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->dismiss()V

    .line 204
    :cond_0
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    .line 191
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 194
    :cond_0
    return-void
.end method

.method public setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/content/DialogInterface$OnDismissListener;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 198
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->notifyDataSetChanged()V

    .line 186
    :goto_0
    return-void

    .line 181
    :cond_0
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mAllowCustomView:Z

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;-><init>(Landroid/content/Context;ZLjava/util/List;)V

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    .line 182
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mAnchor:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupListAdapter:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;)V

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    .line 183
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/music/ui/cardlib/layout/PopupSelector;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PopupSelector;->show()V

    goto :goto_0
.end method
