.class Lcom/google/android/music/art/ArtLoaderImpl;
.super Ljava/lang/Object;
.source "ArtLoaderImpl.java"

# interfaces
.implements Lcom/google/android/music/art/ArtLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor;,
        Lcom/google/android/music/art/ArtLoaderImpl$ResultRunnable;,
        Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;,
        Lcom/google/android/music/art/ArtLoaderImpl$AsyncRequestRunnable;,
        Lcom/google/android/music/art/ArtLoaderImpl$RequestRunnable;
    }
.end annotation


# instance fields
.field private mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mGetCachedOnlyExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mWaitForDownloadExecutor:Ljava/util/concurrent/ExecutorService;

.field private final sRequestCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x3

    const/16 v9, 0xa

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->sRequestCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 67
    new-instance v0, Lcom/google/android/music/art/ArtLoaderImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/art/ArtLoaderImpl$1;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;)V

    iput-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mComparator:Ljava/util/Comparator;

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mContext:Landroid/content/Context;

    .line 96
    invoke-static {p1}, Lcom/google/android/music/download/artwork/ArtMonitorFactory;->getArtMonitor(Landroid/content/Context;)Lcom/google/android/music/download/artwork/ArtMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    .line 98
    new-instance v1, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor;

    const/16 v3, 0x8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mComparator:Ljava/util/Comparator;

    invoke-direct {v7, v9, v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    new-instance v8, Lcom/google/android/music/art/ArtLoaderImpl$2;

    invoke-direct {v8, p0}, Lcom/google/android/music/art/ArtLoaderImpl$2;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;)V

    invoke-direct/range {v1 .. v8}, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mGetCachedOnlyExecutor:Ljava/util/concurrent/ExecutorService;

    .line 114
    new-instance v1, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor;

    const/4 v3, 0x5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mComparator:Ljava/util/Comparator;

    invoke-direct {v7, v9, v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    new-instance v8, Lcom/google/android/music/art/ArtLoaderImpl$3;

    invoke-direct {v8, p0}, Lcom/google/android/music/art/ArtLoaderImpl$3;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;)V

    invoke-direct/range {v1 .. v8}, Lcom/google/android/music/art/ArtLoaderImpl$ArtLoaderImplExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mWaitForDownloadExecutor:Ljava/util/concurrent/ExecutorService;

    .line 129
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/art/ArtLoaderImpl;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtLoaderImpl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->sRequestCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/art/ArtLoaderImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtLoaderImpl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/art/ArtLoaderImpl;)Lcom/google/android/music/download/artwork/ArtMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtLoaderImpl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;

    return-object v0
.end method


# virtual methods
.method public getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;)V
    .locals 1
    .param p1, "remoteArtUrl"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .param p3, "listener"    # Lcom/google/android/music/art/ArtLoader$Listener;

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/music/art/ArtLoaderImpl;->getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V

    .line 165
    return-void
.end method

.method public getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V
    .locals 7
    .param p1, "remoteArtUrl"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .param p3, "listener"    # Lcom/google/android/music/art/ArtLoader$Listener;
    .param p4, "listenerHandler"    # Landroid/os/Handler;

    .prologue
    .line 170
    if-eqz p4, :cond_0

    if-nez p3, :cond_0

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listenerHandler specified without listener?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    sget-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    if-ne p2, v0, :cond_1

    .line 175
    iget-object v6, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mWaitForDownloadExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 184
    :goto_0
    return-void

    .line 179
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/art/ArtLoaderImpl;->mGetCachedOnlyExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/music/art/ArtLoaderImpl$AsyncRequestRunnable;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/art/ArtLoaderImpl$AsyncRequestRunnable;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;Landroid/os/Handler;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public getArtFileDescriptorSync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;
    .locals 3
    .param p1, "remoteArtUrl"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertNotMainThread()V

    .line 141
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 142
    .local v0, "latch":Ljava/util/concurrent/CountDownLatch;
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 144
    .local v1, "pfdHolder":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Landroid/os/ParcelFileDescriptor;>;"
    new-instance v2, Lcom/google/android/music/art/ArtLoaderImpl$4;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/music/art/ArtLoaderImpl$4;-><init>(Lcom/google/android/music/art/ArtLoaderImpl;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/music/art/ArtLoaderImpl;->getArtFileDescriptorAsync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;Lcom/google/android/music/art/ArtLoader$Listener;)V

    .line 157
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 159
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/ParcelFileDescriptor;

    return-object v2
.end method
