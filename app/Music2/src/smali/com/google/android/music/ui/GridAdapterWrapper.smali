.class public Lcom/google/android/music/ui/GridAdapterWrapper;
.super Landroid/widget/BaseAdapter;
.source "GridAdapterWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/GridAdapterWrapper$DummyView;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mNumColumns:I

.field private final mWrappedAdapter:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListAdapter;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adapter"    # Landroid/widget/ListAdapter;
    .param p3, "numColumns"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mWrappedAdapter:Landroid/widget/ListAdapter;

    .line 29
    iget-object v0, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mWrappedAdapter:Landroid/widget/ListAdapter;

    new-instance v1, Lcom/google/android/music/ui/GridAdapterWrapper$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/GridAdapterWrapper$1;-><init>(Lcom/google/android/music/ui/GridAdapterWrapper;)V

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 35
    if-gtz p3, :cond_0

    const/4 p3, 0x1

    .line 36
    :cond_0
    iput p3, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mNumColumns:I

    .line 37
    return-void
.end method

.method private getCardLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 4

    .prologue
    .line 110
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, -0x2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    return-object v0
.end method

.method private getDummyView()Landroid/view/View;
    .locals 5

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/music/ui/GridAdapterWrapper$DummyView;

    iget-object v2, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/music/ui/GridAdapterWrapper$DummyView;-><init>(Landroid/content/Context;)V

    .line 125
    .local v0, "child":Landroid/view/View;
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 126
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 128
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    return-object v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mNumColumns:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 46
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 51
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 56
    iget-object v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    .line 58
    .local v5, "wrappedCount":I
    if-nez v5, :cond_1

    move-object v2, v6

    .line 102
    :cond_0
    return-object v2

    .line 60
    :cond_1
    if-nez p2, :cond_3

    .line 62
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 63
    .local v2, "group":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 64
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mNumColumns:I

    if-ge v3, v7, :cond_0

    .line 65
    iget v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mNumColumns:I

    mul-int/2addr v7, p1

    add-int v4, v7, v3

    .line 67
    .local v4, "mappedPosition":I
    if-ge v4, v5, :cond_2

    .line 68
    iget-object v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7, v4, v6, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 69
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0}, Lcom/google/android/music/ui/GridAdapterWrapper;->getCardLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 64
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/ui/GridAdapterWrapper;->getDummyView()Landroid/view/View;

    move-result-object v0

    .restart local v0    # "child":Landroid/view/View;
    goto :goto_1

    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "group":Landroid/widget/LinearLayout;
    .end local v3    # "i":I
    .end local v4    # "mappedPosition":I
    :cond_3
    move-object v2, p2

    .line 79
    check-cast v2, Landroid/view/ViewGroup;

    .line 80
    .local v2, "group":Landroid/view/ViewGroup;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 81
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 82
    .local v1, "childConvertView":Landroid/view/View;
    iget v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mNumColumns:I

    mul-int/2addr v7, p1

    add-int v4, v7, v3

    .line 83
    .restart local v4    # "mappedPosition":I
    if-ge v4, v5, :cond_6

    .line 86
    instance-of v7, v1, Lcom/google/android/music/ui/GridAdapterWrapper$DummyView;

    if-eqz v7, :cond_5

    .line 87
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 88
    iget-object v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7, v4, v6, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 89
    .restart local v0    # "child":Landroid/view/View;
    invoke-direct {p0}, Lcom/google/android/music/ui/GridAdapterWrapper;->getCardLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    invoke-virtual {v2, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 80
    .end local v0    # "child":Landroid/view/View;
    :cond_4
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 92
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/ui/GridAdapterWrapper;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7, v4, v1, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 93
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 96
    :cond_6
    instance-of v7, v1, Lcom/google/android/music/ui/GridAdapterWrapper$DummyView;

    if-nez v7, :cond_4

    .line 97
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 98
    invoke-direct {p0}, Lcom/google/android/music/ui/GridAdapterWrapper;->getDummyView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v2, v7, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_3
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method
