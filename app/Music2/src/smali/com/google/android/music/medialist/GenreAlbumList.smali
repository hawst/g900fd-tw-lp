.class public Lcom/google/android/music/medialist/GenreAlbumList;
.super Lcom/google/android/music/medialist/AlbumList;
.source "GenreAlbumList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/GenreAlbumList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDefaultAlbumIcon:Landroid/graphics/drawable/BitmapDrawable;

.field private mGenreId:J

.field private mGenreName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/music/medialist/GenreAlbumList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/GenreAlbumList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/GenreAlbumList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 3
    .param p1, "genreId"    # J
    .param p3, "shouldFilter"    # Z

    .prologue
    .line 27
    invoke-direct {p0, p3}, Lcom/google/android/music/medialist/AlbumList;-><init>(Z)V

    .line 28
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid genreId id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    iput-wide p1, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreId:J

    .line 32
    return-void
.end method

.method private getNames(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 46
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v2, v7

    .line 49
    .local v2, "cols":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Genres;->getGenreUri(Ljava/lang/Long;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 51
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 52
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 53
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreName:Ljava/lang/String;

    .line 56
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 59
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    return-void
.end method


# virtual methods
.method public getArgs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mShouldFilter:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Genres;->getAlbumsOfGenreUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getGenreId()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreId:J

    return-wide v0
.end method

.method public getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "allowAlias"    # Z

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/music/medialist/GenreAlbumList;->getNames(Landroid/content/Context;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreName:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mGenreId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 88
    iget-boolean v0, p0, Lcom/google/android/music/medialist/GenreAlbumList;->mShouldFilter:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
