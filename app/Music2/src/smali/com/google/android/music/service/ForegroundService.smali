.class public abstract Lcom/google/android/music/service/ForegroundService;
.super Landroid/app/Service;
.source "ForegroundService.java"


# instance fields
.field private mIsForeground:Z

.field private final mLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 15
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/service/ForegroundService;->mLock:Ljava/lang/Object;

    return-void
.end method

.method private setIsForeground(Z)V
    .locals 0
    .param p1, "isForeground"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/android/music/service/ForegroundService;->mIsForeground:Z

    .line 23
    return-void
.end method


# virtual methods
.method protected isForeground()Z
    .locals 2

    .prologue
    .line 51
    iget-object v1, p0, Lcom/google/android/music/service/ForegroundService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/service/ForegroundService;->mIsForeground:Z

    monitor-exit v1

    return v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startForegroundService(ILandroid/app/Notification;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "notification"    # Landroid/app/Notification;

    .prologue
    .line 32
    iget-object v1, p0, Lcom/google/android/music/service/ForegroundService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 33
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/service/ForegroundService;->startForeground(ILandroid/app/Notification;)V

    .line 34
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/service/ForegroundService;->setIsForeground(Z)V

    .line 35
    monitor-exit v1

    .line 36
    return-void

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopForegroundService(Z)V
    .locals 2
    .param p1, "removeNotification"    # Z

    .prologue
    .line 44
    iget-object v1, p0, Lcom/google/android/music/service/ForegroundService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 45
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/service/ForegroundService;->stopForeground(Z)V

    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/service/ForegroundService;->setIsForeground(Z)V

    .line 47
    monitor-exit v1

    .line 48
    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
