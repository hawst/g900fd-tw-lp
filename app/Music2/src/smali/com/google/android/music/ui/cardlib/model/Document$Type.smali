.class public final enum Lcom/google/android/music/ui/cardlib/model/Document$Type;
.super Ljava/lang/Enum;
.source "Document.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/Document;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum IM_FEELING_LUCKY:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum SITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum SUBSITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum UNKNOWN:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field public static final enum VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 60
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->UNKNOWN:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 61
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "ARTIST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 62
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "GENRE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 63
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 64
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "PLAYLIST"

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 65
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "RADIO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 66
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "TRACK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 67
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "NAUTILUS_GENRE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 68
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "IM_FEELING_LUCKY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->IM_FEELING_LUCKY:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 69
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "ALL_SONGS_ARTIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 70
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "ALL_SONGS_GENRE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 71
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "VIDEO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 72
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "SITUATION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->SITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 73
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    const-string v1, "SUBSITUATION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->SUBSITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 59
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->UNKNOWN:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->IM_FEELING_LUCKY:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->SITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->SUBSITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->$VALUES:[Lcom/google/android/music/ui/cardlib/model/Document$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOrdinal(I)Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .locals 1
    .param p0, "typeIndex"    # I

    .prologue
    .line 76
    invoke-static {}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->values()[Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 59
    const-class v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->$VALUES:[Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0}, [Lcom/google/android/music/ui/cardlib/model/Document$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/ui/cardlib/model/Document$Type;

    return-object v0
.end method
