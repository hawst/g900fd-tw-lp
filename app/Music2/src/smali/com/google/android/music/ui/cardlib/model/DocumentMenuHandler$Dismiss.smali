.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Dismiss"
.end annotation


# instance fields
.field final CROSSFADE_DURATION:J

.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1375
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1376
    const v0, 0x7f0e0034

    const v1, 0x7f0b0249

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1373
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;->CROSSFADE_DURATION:J

    .line 1377
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 2

    .prologue
    .line 1383
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPlayCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$1000(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1384
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss$1;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;)V

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 1403
    :goto_0
    return-void

    .line 1401
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Dismiss;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->dismissFromMainstage(Landroid/content/ContentResolver;)V

    goto :goto_0
.end method
