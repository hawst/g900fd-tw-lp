.class public Lcom/google/android/music/store/KeepOnUpdater;
.super Ljava/lang/Object;
.source "KeepOnUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;,
        Lcom/google/android/music/store/KeepOnUpdater$SyncListener;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final NO_NULL_COLUMN_HACK:Ljava/lang/String;

.field private static final NO_WHERE_ARGS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    .line 41
    sput-object v1, Lcom/google/android/music/store/KeepOnUpdater;->NO_WHERE_ARGS:[Ljava/lang/String;

    .line 42
    sput-object v1, Lcom/google/android/music/store/KeepOnUpdater;->NO_NULL_COLUMN_HACK:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/google/android/music/store/KeepOnUpdater;->updateKeeponTables(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    return v0
.end method

.method static synthetic access$200(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 1
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "x1"    # Ljava/util/Collection;

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/google/android/music/store/KeepOnUpdater;->deleteAlbums(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 1
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "x1"    # Ljava/util/Collection;

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/google/android/music/store/KeepOnUpdater;->deletePlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 1
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "x1"    # Ljava/util/Collection;

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/google/android/music/store/KeepOnUpdater;->deleteAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 1
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "x1"    # Ljava/util/Collection;

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/google/android/music/store/KeepOnUpdater;->deleteRadioStations(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method private static deleteAlbums(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "AlbumId"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteByIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static deleteAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "AutoListId"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteByIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static deleteByIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)I
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 457
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 458
    const/4 v2, 0x0

    .line 466
    :goto_0
    return v2

    .line 460
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    .line 461
    .local v0, "size":I
    new-instance v1, Ljava/lang/StringBuilder;

    mul-int/lit8 v2, v0, 0xb

    add-int/lit8 v2, v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 464
    .local v1, "whereClause":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    invoke-static {v1, p1}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    .line 466
    const-string v2, "KEEPON"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/store/KeepOnUpdater;->NO_WHERE_ARGS:[Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method private static deleteInvalidKeeponEntries(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 506
    const-string v1, "KEEPON"

    const-string v2, "(ListId NOT NULL AND NOT EXISTS (select 1 from LISTS where LISTS.Id=KEEPON.ListId)) OR (AlbumId NOT NULL AND NOT EXISTS (select 1 from MUSIC where MUSIC.AlbumId=KEEPON.AlbumId LIMIT 1)) OR (RadioStationId NOT NULL AND NOT EXISTS (select 1 from RADIO_STATIONS where RADIO_STATIONS.Id=KEEPON.RadioStationId LIMIT 1))"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 525
    .local v0, "deleted":I
    if-lez v0, :cond_0

    .line 526
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnRadioUtils;->deleteRadioSongsFromUnpinnedStations(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 529
    :cond_0
    return v0
.end method

.method private static deleteInvalidShouldKeeponAutoPlayListItems(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 15
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 588
    const/4 v11, 0x0

    .line 589
    .local v11, "deleteCount":I
    const-string v1, "KEEPON"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "KeepOnId"

    aput-object v0, v2, v3

    const-string v0, "AutoListId"

    aput-object v0, v2, v5

    const-string v3, "AutoListId NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 593
    .local v12, "keeponCursor":Landroid/database/Cursor;
    if-eqz v12, :cond_5

    .line 594
    const/4 v0, 0x1

    :try_start_0
    new-array v14, v0, [Ljava/lang/String;

    .line 595
    .local v14, "selectionArgs":[Ljava/lang/String;
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 596
    const/4 v0, 0x1

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 597
    .local v8, "autoListId":J
    const/4 v10, 0x0

    .line 598
    .local v10, "autoListSelect":Ljava/lang/String;
    const-wide/16 v0, -0x4

    cmp-long v0, v8, v0

    if-nez v0, :cond_1

    .line 600
    const-string v10, "(SELECT DISTINCT MUSIC.Id\nFROM MUSIC\n WHERE MUSIC.SourceAccount <> 0 AND (Rating > 3))"

    .line 633
    :cond_0
    :goto_1
    if-nez v10, :cond_4

    .line 634
    const-string v0, "KeepOnUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to generate validate query for autoListId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 646
    .end local v8    # "autoListId":J
    .end local v10    # "autoListSelect":Ljava/lang/String;
    .end local v14    # "selectionArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 606
    .restart local v8    # "autoListId":J
    .restart local v10    # "autoListSelect":Ljava/lang/String;
    .restart local v14    # "selectionArgs":[Ljava/lang/String;
    :cond_1
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_2

    .line 608
    :try_start_1
    const-string v10, "(SELECT DISTINCT MUSIC.Id\nFROM MUSIC\n WHERE MUSIC.SourceAccount <> 0 AND +Domain=0 ORDER BY MUSIC.FileDate DESC  LIMIT 500)"

    goto :goto_1

    .line 616
    :cond_2
    const-wide/16 v0, -0x3

    cmp-long v0, v8, v0

    if-nez v0, :cond_3

    .line 618
    const-string v10, "(SELECT DISTINCT MUSIC.Id\nFROM MUSIC\n WHERE MUSIC.SourceAccount <> 0 AND (TrackType IN (2,3,1) AND +Domain=0))"

    goto :goto_1

    .line 624
    :cond_3
    const-wide/16 v0, -0x2

    cmp-long v0, v8, v0

    if-nez v0, :cond_0

    .line 626
    const-string v10, "(SELECT DISTINCT MUSIC.Id\nFROM MUSIC\n WHERE MUSIC.SourceAccount <> 0 AND +Domain=0)"

    goto :goto_1

    .line 638
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeepOnId=? AND MusicId NOT IN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 641
    .local v13, "selection":Ljava/lang/String;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v14, v0

    .line 642
    const-string v0, "SHOULDKEEPON"

    invoke-virtual {p0, v0, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    add-int/2addr v11, v0

    .line 643
    goto :goto_0

    .line 646
    .end local v8    # "autoListId":J
    .end local v10    # "autoListSelect":Ljava/lang/String;
    .end local v13    # "selection":Ljava/lang/String;
    .end local v14    # "selectionArgs":[Ljava/lang/String;
    :cond_5
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 648
    return v11
.end method

.method private static deleteInvalidShouldKeeponItems(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 653
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteInvalidShouldKeeponPlayListItems(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 655
    .local v0, "deleteCount":I
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteInvalidShouldKeeponAutoPlayListItems(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v1

    add-int/2addr v0, v1

    .line 657
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteInvalidShouldKeeponRadioSongs(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v1

    add-int/2addr v0, v1

    .line 660
    const-string v1, "SHOULDKEEPON"

    const-string v2, " ROWID IN (select SHOULDKEEPON.rowid from KEEPON, SHOULDKEEPON where KEEPON.AlbumId NOT NULL AND KEEPON.KeepOnId=SHOULDKEEPON.KeepOnId AND NOT EXISTS(select 1 from MUSIC where MUSIC.Id=SHOULDKEEPON.MusicId AND MUSIC.AlbumId=KEEPON.AlbumId))"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 674
    return v0
.end method

.method private static deleteInvalidShouldKeeponPlayListItems(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 12
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v0, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 561
    const/4 v8, 0x0

    .line 565
    .local v8, "deleteCount":I
    const-string v1, "KEEPON"

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "KeepOnId"

    aput-object v0, v2, v3

    const-string v0, "ListId"

    aput-object v0, v2, v5

    const-string v3, "ListId NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 569
    .local v9, "keeponCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    .line 570
    :try_start_0
    const-string v10, "KeepOnId=? AND MusicId NOT IN (select MUSIC.Id from LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  where LISTITEMS.ListId=?)"

    .line 574
    .local v10, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v11, v0, [Ljava/lang/String;

    .line 575
    .local v11, "selectionArgs":[Ljava/lang/String;
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v11, v0

    .line 577
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v11, v0

    .line 578
    const-string v0, "SHOULDKEEPON"

    const-string v1, "KeepOnId=? AND MusicId NOT IN (select MUSIC.Id from LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  where LISTITEMS.ListId=?)"

    invoke-virtual {p0, v0, v1, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    add-int/2addr v8, v0

    goto :goto_0

    .line 582
    .end local v10    # "selection":Ljava/lang/String;
    .end local v11    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 584
    return v8

    .line 582
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static deleteInvalidShouldKeeponRadioSongs(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 12
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v0, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 535
    const/4 v8, 0x0

    .line 537
    .local v8, "deleteCount":I
    const-string v1, "KEEPON"

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "KeepOnId"

    aput-object v0, v2, v3

    const-string v0, "RadioStationId"

    aput-object v0, v2, v5

    const-string v3, "RadioStationId NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 541
    .local v9, "keeponCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    .line 542
    :try_start_0
    const-string v10, "KeepOnId=? AND MusicId NOT IN (select MUSIC.Id from RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id)  where RADIO_SONGS.RadioStationId=?)"

    .line 546
    .local v10, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v11, v0, [Ljava/lang/String;

    .line 547
    .local v11, "selectionArgs":[Ljava/lang/String;
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v11, v0

    .line 549
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v11, v0

    .line 550
    const-string v0, "SHOULDKEEPON"

    const-string v1, "KeepOnId=? AND MusicId NOT IN (select MUSIC.Id from RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id)  where RADIO_SONGS.RadioStationId=?)"

    invoke-virtual {p0, v0, v1, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    add-int/2addr v8, v0

    goto :goto_0

    .line 554
    .end local v10    # "selection":Ljava/lang/String;
    .end local v11    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 556
    return v8

    .line 554
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static deleteOrphanedShouldKeeponItems(Landroid/database/sqlite/SQLiteDatabase;Z)Z
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "deleteInvalidItems"    # Z

    .prologue
    const/4 v4, 0x0

    .line 419
    const/4 v1, 0x0

    .line 421
    .local v1, "fileCleanupNeeded":Z
    const-string v2, "SHOULDKEEPON"

    const-string v3, "KeepOnId NOT IN (SELECT KeepOnId FROM KEEPON)"

    invoke-virtual {p0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 426
    .local v0, "deleted":I
    if-eqz p1, :cond_0

    .line 427
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteInvalidShouldKeeponItems(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v2

    add-int/2addr v0, v2

    .line 430
    :cond_0
    if-lez v0, :cond_1

    .line 433
    invoke-static {p0}, Lcom/google/android/music/store/Store;->resetLocalCopyForOrphanedShouldKeepOnMusic(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v2

    if-lez v2, :cond_1

    .line 434
    const/4 v1, 0x1

    .line 438
    :cond_1
    if-eqz p1, :cond_2

    .line 441
    const-string v2, "SHOULDKEEPON"

    const-string v3, "SHOULDKEEPON.MusicId IN (SELECT MusicId FROM SHOULDKEEPON LEFT JOIN MUSIC ON (SHOULDKEEPON.MusicId=MUSIC.Id) WHERE MUSIC.Id IS NULL)"

    invoke-virtual {p0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 450
    :cond_2
    return v1
.end method

.method private static deletePlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "ListId"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteByIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static deleteRadioStations(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v1, "RadioStationId"

    invoke-static {p0, p1, v1}, Lcom/google/android/music/store/KeepOnUpdater;->deleteByIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)I

    move-result v0

    .line 126
    .local v0, "deleted":I
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnRadioUtils;->deleteRadioSongsFromUnpinnedStations(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 127
    return v0
.end method

.method public static deselectAlbumUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    const/4 v2, 0x0

    .line 730
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 731
    .local v1, "albums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    .line 732
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 733
    return-void
.end method

.method public static deselectAutoPlaylistUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "autoPlaylistId"    # J

    .prologue
    const/4 v1, 0x0

    .line 755
    new-instance v3, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 756
    .local v3, "autoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    .line 757
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 758
    return-void
.end method

.method public static deselectPlaylistUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J

    .prologue
    const/4 v1, 0x0

    .line 742
    new-instance v2, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 743
    .local v2, "playlists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    .line 744
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 745
    return-void
.end method

.method public static deselectRadioUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "radioId"    # J

    .prologue
    const/4 v1, 0x0

    .line 768
    new-instance v4, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 769
    .local v4, "radioIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    .line 770
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 771
    return-void
.end method

.method private static getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 812
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/store/KeepOnUpdater$UpdateKeepOnTables;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 813
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.UPDATE_KEEP_ON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 814
    return-object v0
.end method

.method static insertAlbums(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "AlbumId"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static insertAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "AutoListId"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private static insertFromAlbums(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 262
    const-string v0, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,KEEPON.KeepOnId\nFROM KEEPON\n JOIN MUSIC ON (KEEPON.AlbumId = MUSIC.AlbumId)  WHERE MUSIC.SourceAccount <> 0 AND Domain = 0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 263
    return-void
.end method

.method private static insertFromAlbums(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J

    .prologue
    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,KEEPON.KeepOnId\nFROM KEEPON\n JOIN MUSIC ON (KEEPON.AlbumId = MUSIC.AlbumId)  WHERE MUSIC.SourceAccount <> 0 AND Domain = 0 AND KEEPON.KeepOnId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 268
    return-void
.end method

.method private static insertFromAllSongsPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J

    .prologue
    .line 299
    sget-boolean v1, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v1, :cond_0

    .line 300
    const-string v1, "KeepOnUpdater"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertFromAllSongsPlaylist: keepOnId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC.SourceAccount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+Domain=0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 309
    .local v0, "insert":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 310
    return-void
.end method

.method private static insertFromAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 330
    sget-boolean v0, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v0, :cond_0

    .line 331
    const-string v0, "KeepOnUpdater"

    const-string v1, "insertFromAutoPlaylists"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :cond_0
    const-string v1, "KEEPON"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "KeepOnId"

    aput-object v0, v2, v3

    const-string v0, "AutoListId"

    aput-object v0, v2, v5

    const-string v3, "AutoListId NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 337
    .local v10, "keeponCursor":Landroid/database/Cursor;
    if-eqz v10, :cond_5

    .line 338
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 339
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 340
    .local v12, "keeponId":J
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 342
    .local v8, "autoListId":J
    const-wide/16 v0, -0x4

    cmp-long v0, v8, v0

    if-nez v0, :cond_2

    .line 343
    invoke-static {p0, v12, v13}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromThumbsUpPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 354
    .end local v8    # "autoListId":J
    .end local v12    # "keeponId":J
    :catchall_0
    move-exception v0

    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 344
    .restart local v8    # "autoListId":J
    .restart local v12    # "keeponId":J
    :cond_2
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_3

    .line 345
    :try_start_1
    invoke-static {p0, v12, v13}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromRecentlyAddedPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_0

    .line 346
    :cond_3
    const-wide/16 v0, -0x3

    cmp-long v0, v8, v0

    if-nez v0, :cond_4

    .line 347
    invoke-static {p0, v12, v13}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromStoreSongsPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_0

    .line 348
    :cond_4
    const-wide/16 v0, -0x2

    cmp-long v0, v8, v0

    if-nez v0, :cond_1

    .line 349
    invoke-static {p0, v12, v13}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromAllSongsPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 354
    .end local v8    # "autoListId":J
    .end local v12    # "keeponId":J
    :cond_5
    invoke-static {v10}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 356
    return-void
.end method

.method private static insertFromAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;JJ)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J
    .param p3, "autoListId"    # J

    .prologue
    .line 359
    sget-boolean v0, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v0, :cond_0

    .line 360
    const-string v0, "KeepOnUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertFromAutoPlaylists: keepOnId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " autoListId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_0
    const-wide/16 v0, -0x4

    cmp-long v0, p3, v0

    if-nez v0, :cond_1

    .line 365
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromThumbsUpPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 375
    :goto_0
    return-void

    .line 366
    :cond_1
    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-nez v0, :cond_2

    .line 367
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromRecentlyAddedPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_0

    .line 368
    :cond_2
    const-wide/16 v0, -0x3

    cmp-long v0, p3, v0

    if-nez v0, :cond_3

    .line 369
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromStoreSongsPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_0

    .line 370
    :cond_3
    const-wide/16 v0, -0x2

    cmp-long v0, p3, v0

    if-nez v0, :cond_4

    .line 371
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromAllSongsPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_0

    .line 373
    :cond_4
    const-string v0, "KeepOnUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to insert auto playlist with id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static insertFromPlaylists(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 378
    const-string v0, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,KEEPON.KeepOnId\nFROM KEEPON\n JOIN LISTITEMS ON (KEEPON.ListId = LISTITEMS.ListId) \n JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE MUSIC.SourceAccount<> 0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 379
    return-void
.end method

.method private static insertFromPlaylists(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J

    .prologue
    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,KEEPON.KeepOnId\nFROM KEEPON\n JOIN LISTITEMS ON (KEEPON.ListId = LISTITEMS.ListId) \n JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE MUSIC.SourceAccount<> 0 AND KEEPON.KeepOnId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 384
    return-void
.end method

.method private static insertFromRadioStations(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 387
    const-string v0, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,KEEPON.KeepOnId\nFROM KEEPON\n JOIN RADIO_SONGS ON (KEEPON.RadioStationId = RADIO_SONGS.RadioStationId) \n JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id) "

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method private static insertFromRadioStations(Landroid/database/sqlite/SQLiteDatabase;JJ)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J
    .param p3, "radioStationId"    # J

    .prologue
    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,KEEPON.KeepOnId\nFROM KEEPON\n JOIN RADIO_SONGS ON (KEEPON.RadioStationId = RADIO_SONGS.RadioStationId) \n JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id)  WHERE KEEPON.KeepOnId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 395
    return-void
.end method

.method private static insertFromRecentlyAddedPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J

    .prologue
    .line 313
    sget-boolean v1, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v1, :cond_0

    .line 314
    const-string v1, "KeepOnUpdater"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertFromRecentlyAddedPlaylists: keepOnId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC.SourceAccount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+Domain=0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ORDER BY "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC.FileDate DESC "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIMIT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x1f4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "insert":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method private static insertFromStoreSongsPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J

    .prologue
    .line 285
    sget-boolean v1, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v1, :cond_0

    .line 286
    const-string v1, "KeepOnUpdater"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertFromStoreSongsPlaylists: keepOnId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC.SourceAccount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(TrackType IN (2,3,1) AND +Domain=0)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 295
    .local v0, "insert":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 296
    return-void
.end method

.method private static insertFromThumbsUpPlaylist(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "keepOnId"    # J

    .prologue
    .line 271
    sget-boolean v1, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "KeepOnUpdater"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertFromThumbsUpPlaylists: keepOnId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO SHOULDKEEPON(MusicId,KeepOnId)\nSELECT DISTINCT MUSIC.Id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MUSIC.SourceAccount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(Rating > 3)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "insert":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 282
    return-void
.end method

.method private static insertIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;
    .locals 12
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 477
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    sget-boolean v7, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v7, :cond_0

    .line 478
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 479
    .local v2, "id":J
    const-string v7, "KeepOnUpdater"

    const-string v8, "insertIdsInColumn: id=%d column=%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object p2, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 482
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "id":J
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v1

    .line 484
    .local v1, "newIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 500
    :cond_1
    return-object v1

    .line 488
    :cond_2
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 490
    .local v6, "values":Landroid/content/ContentValues;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 491
    .restart local v2    # "id":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, p2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 492
    const-string v7, "KEEPON"

    sget-object v8, Lcom/google/android/music/store/KeepOnUpdater;->NO_NULL_COLUMN_HACK:Ljava/lang/String;

    invoke-virtual {p0, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 495
    .local v4, "rowid":J
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-eqz v7, :cond_3

    .line 496
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 498
    :cond_3
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    goto :goto_1
.end method

.method static insertPlaylists(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "ListId"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static insertRadioStations(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "RadioStationId"

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertIdsInColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static selectAlbumUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    const/4 v1, 0x0

    .line 736
    new-instance v5, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 737
    .local v5, "albums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    .line 738
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 739
    return-void
.end method

.method public static selectAutoPlaylistUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "autoPlaylistId"    # J

    .prologue
    const/4 v1, 0x0

    .line 761
    new-instance v7, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 762
    .local v7, "autoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v8, v1

    .line 763
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 764
    return-void
.end method

.method public static selectPlaylistUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J

    .prologue
    const/4 v1, 0x0

    .line 748
    new-instance v6, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 749
    .local v6, "playlists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    move-object v8, v1

    .line 750
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 751
    return-void
.end method

.method public static selectRadioUpdateKeepOn(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "radioId"    # J

    .prologue
    const/4 v1, 0x0

    .line 774
    new-instance v8, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 775
    .local v8, "radioIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    .line 776
    invoke-static/range {v0 .. v8}, Lcom/google/android/music/store/KeepOnUpdater;->startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 777
    return-void
.end method

.method public static sendShouldKeeponUpdatedBroadcast(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestFileCleanup"    # Z

    .prologue
    .line 688
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.music.NEW_SHOULDKEEPON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 689
    .local v0, "update":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 690
    const-string v1, "deleteCachedFiles"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 693
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 694
    return-void
.end method

.method public static startUpdateKeepon(Landroid/content/Context;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 790
    .local p1, "deselectedAlbums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    .local p2, "deselectedPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    .local p3, "deselectedAutoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    .local p4, "deselectedRadioStations":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    .local p5, "selectedAlbums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    .local p6, "selectedPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    .local p7, "selectedAutoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    .local p8, "selectedRadioStations":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    sget-boolean v1, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v1, :cond_0

    .line 791
    const-string v1, "KeepOnUpdater"

    const-string v2, "startUpdateKeepon"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnUpdater;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 796
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "deselectedAlbums"

    check-cast p1, Ljava/io/Serializable;

    .end local p1    # "deselectedAlbums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 797
    const-string v1, "deselectedPlaylists"

    check-cast p2, Ljava/io/Serializable;

    .end local p2    # "deselectedPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 798
    const-string v1, "deselectedAutoPlaylists"

    check-cast p3, Ljava/io/Serializable;

    .end local p3    # "deselectedAutoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 799
    const-string v1, "deselectedRadioStations"

    check-cast p4, Ljava/io/Serializable;

    .end local p4    # "deselectedRadioStations":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 801
    const-string v1, "selectedAlbums"

    check-cast p5, Ljava/io/Serializable;

    .end local p5    # "selectedAlbums":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 802
    const-string v1, "selectedPlaylists"

    check-cast p6, Ljava/io/Serializable;

    .end local p6    # "selectedPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 803
    const-string v1, "selectedAutoPlaylists"

    check-cast p7, Ljava/io/Serializable;

    .end local p7    # "selectedAutoPlaylists":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 804
    const-string v1, "selectedRadioStations"

    check-cast p8, Ljava/io/Serializable;

    .end local p8    # "selectedRadioStations":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 806
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_1

    .line 807
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not start the keep on updater service"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 809
    :cond_1
    return-void
.end method

.method public static startUpdateOnlineContainers(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 819
    sget-boolean v1, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v1, :cond_0

    .line 820
    const-string v1, "KeepOnUpdater"

    const-string v2, "startUpdateOnlineContainers"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnUpdater;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 824
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "onlineContainers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 825
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_1

    .line 826
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not start the keep on updater service"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 828
    :cond_1
    return-void
.end method

.method private static updateKeeponTables(Landroid/content/Context;Z)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deleteOldData"    # Z

    .prologue
    const/4 v5, 0x1

    .line 223
    sget-boolean v6, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v6, :cond_0

    const-string v6, "KeepOnUpdater"

    const-string v7, "Updating SHOULDKEEPON"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/KeepOnRadioUtils;->updateKeepOnRadio(Landroid/content/Context;)V

    .line 227
    const/4 v2, 0x0

    .line 228
    .local v2, "filePathCleanupNeeded":Z
    const/4 v4, 0x0

    .line 229
    .local v4, "success":Z
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    .line 230
    .local v3, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 232
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/KeepOnUpdater;->deleteInvalidKeeponEntries(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 233
    const/4 v1, 0x0

    .line 234
    .local v1, "deletedShouldKeepOnCount":I
    if-eqz p1, :cond_1

    .line 235
    const-string v6, "SHOULDKEEPON"

    const-string v7, "1"

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 239
    :cond_1
    invoke-static {v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromAlbums(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 240
    invoke-static {v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromPlaylists(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 241
    invoke-static {v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 242
    invoke-static {v0}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromRadioStations(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 243
    invoke-virtual {v3, v0}, Lcom/google/android/music/store/Store;->updateKeeponCountsAndSizes(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 244
    if-eqz p1, :cond_5

    .line 245
    if-lez v1, :cond_2

    .line 246
    invoke-static {v0}, Lcom/google/android/music/store/Store;->resetLocalCopyForOrphanedShouldKeepOnMusic(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-lez v6, :cond_4

    move v2, v5

    .line 253
    :cond_2
    :goto_0
    const/4 v4, 0x1

    .line 255
    invoke-virtual {v3, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 257
    sget-boolean v5, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v5, :cond_3

    const-string v5, "KeepOnUpdater"

    const-string v6, "Update of SHOULDKEEPON complete"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_3
    invoke-static {p0, v2}, Lcom/google/android/music/store/KeepOnUpdater;->sendShouldKeeponUpdatedBroadcast(Landroid/content/Context;Z)V

    .line 259
    return-void

    .line 246
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 251
    :cond_5
    const/4 v5, 0x1

    :try_start_1
    invoke-static {v0, v5}, Lcom/google/android/music/store/KeepOnUpdater;->deleteOrphanedShouldKeeponItems(Landroid/database/sqlite/SQLiteDatabase;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    goto :goto_0

    .line 255
    .end local v1    # "deletedShouldKeepOnCount":I
    :catchall_0
    move-exception v5

    invoke-virtual {v3, v0, v4}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v5
.end method

.method static updateNeedToKeepOn(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)Z
    .locals 24
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "newKeepOnId":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    sget-boolean v6, Lcom/google/android/music/store/KeepOnUpdater;->LOGV:Z

    if-eqz v6, :cond_0

    .line 181
    const-string v6, "KeepOnUpdater"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateNeedToKeepOn: newKeepOnId.size="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_7

    .line 184
    const/16 v16, 0x0

    .line 186
    .local v16, "c":Landroid/database/Cursor;
    :try_start_0
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0xa

    move-object/from16 v0, v21

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 187
    .local v21, "strNewIds":Ljava/lang/StringBuilder;
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    .line 188
    .local v18, "newId":Ljava/lang/Long;
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x2c

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 215
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "newId":Ljava/lang/Long;
    .end local v21    # "strNewIds":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v6

    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v6

    .line 190
    .restart local v17    # "i$":Ljava/util/Iterator;
    .restart local v21    # "strNewIds":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_1
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 191
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 193
    .local v20, "newIds":Ljava/lang/String;
    const-string v7, "KEEPON"

    const/4 v6, 0x5

    new-array v8, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v9, "KeepOnId"

    aput-object v9, v8, v6

    const/4 v6, 0x1

    const-string v9, "AlbumId"

    aput-object v9, v8, v6

    const/4 v6, 0x2

    const-string v9, "ListId"

    aput-object v9, v8, v6

    const/4 v6, 0x3

    const-string v9, "AutoListId"

    aput-object v9, v8, v6

    const/4 v6, 0x4

    const-string v9, "RadioStationId"

    aput-object v9, v8, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KeepOnId IN ("

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ")"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v6, p0

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 200
    :cond_2
    :goto_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 201
    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 202
    .local v18, "newId":J
    const/4 v6, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_3

    .line 203
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromAlbums(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_1

    .line 204
    :cond_3
    const/4 v6, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_4

    .line 205
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromPlaylists(Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_1

    .line 206
    :cond_4
    const/4 v6, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_5

    .line 207
    const/4 v6, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 208
    .local v14, "autoListId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2, v14, v15}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromAutoPlaylists(Landroid/database/sqlite/SQLiteDatabase;JJ)V

    goto :goto_1

    .line 209
    .end local v14    # "autoListId":J
    :cond_5
    const/4 v6, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_2

    .line 210
    const/4 v6, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 211
    .local v22, "radioStationId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v22

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/KeepOnUpdater;->insertFromRadioStations(Landroid/database/sqlite/SQLiteDatabase;JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 215
    .end local v18    # "newId":J
    .end local v22    # "radioStationId":J
    :cond_6
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 218
    .end local v16    # "c":Landroid/database/Cursor;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v20    # "newIds":Ljava/lang/String;
    .end local v21    # "strNewIds":Ljava/lang/StringBuilder;
    :cond_7
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/google/android/music/store/KeepOnUpdater;->deleteOrphanedShouldKeeponItems(Landroid/database/sqlite/SQLiteDatabase;Z)Z

    move-result v6

    return v6
.end method
