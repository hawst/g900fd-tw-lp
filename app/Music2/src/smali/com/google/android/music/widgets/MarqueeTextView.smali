.class public Lcom/google/android/music/widgets/MarqueeTextView;
.super Landroid/widget/TextView;
.source "MarqueeTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/MarqueeTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 22
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/MarqueeTextView;->setHorizontallyScrolling(Z)V

    .line 23
    return-void
.end method


# virtual methods
.method public doMarquee(Z)V
    .locals 0
    .param p1, "marquee"    # Z

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/MarqueeTextView;->setSelected(Z)V

    .line 56
    return-void
.end method

.method public onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/music/widgets/MarqueeTextView;->getMeasuredWidth()I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 43
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/MarqueeTextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/music/widgets/MarqueeTextView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/widgets/MarqueeTextView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "focused"    # Z

    .prologue
    .line 27
    if-eqz p1, :cond_0

    .line 33
    invoke-super {p0, p1}, Landroid/widget/TextView;->onWindowFocusChanged(Z)V

    .line 35
    :cond_0
    return-void
.end method
