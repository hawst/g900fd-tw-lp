.class Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;
.super Ljava/lang/Object;
.source "MaterialTrackContainerFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/MaterialTrackContainerFragment;->asyncPopulateContainerMetadata()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

.field private final mSavedSongList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialTrackContainerFragment;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    .line 200
    new-instance v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    invoke-direct {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 6

    .prologue
    .line 204
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/music/medialist/SongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumId:J

    .line 205
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/music/medialist/SongList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistId:J

    .line 206
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->primaryTitle:Ljava/lang/String;

    .line 207
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/music/medialist/SongList;->getSecondaryName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->secondaryTitle:Ljava/lang/String;

    .line 208
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/music/medialist/SongList;->getDescription(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->description:Ljava/lang/String;

    .line 210
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/google/android/music/medialist/SongList;->getDescriptionAttribution(Landroid/content/Context;)Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;

    move-result-object v0

    .line 212
    .local v0, "descriptionAttribution":Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;
    if-eqz v0, :cond_0

    .line 213
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, v0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->sourceTitle:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceTitle:Ljava/lang/String;

    .line 214
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, v0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->sourceUrl:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceUrl:Ljava/lang/String;

    .line 215
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, v0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->licenseTitle:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseTitle:Ljava/lang/String;

    .line 216
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, v0, Lcom/google/android/music/medialist/MediaList$DescriptionAttribution;->licenseUrl:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseUrl:Ljava/lang/String;

    .line 218
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-wide v4, v4, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistId:J

    invoke-static {v3, v4, v5}, Lcom/google/android/music/utils/MusicUtils;->getArtistArtUrl(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    .line 221
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v2, :cond_2

    .line 222
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    .line 224
    .local v1, "nautilusSongList":Lcom/google/android/music/medialist/NautilusAlbumSongList;
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamAlbumId:Ljava/lang/String;

    .line 225
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v3}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getArtistMetajamId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamArtistId:Ljava/lang/String;

    .line 226
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v4, v4, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamArtistId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/music/utils/MusicUtils;->getNautilusArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    .line 228
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v3}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getAlbumYear(Landroid/content/Context;)I

    move-result v3

    iput v3, v2, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    .line 233
    .end local v1    # "nautilusSongList":Lcom/google/android/music/medialist/NautilusAlbumSongList;
    :cond_1
    :goto_0
    return-void

    .line 229
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v2, :cond_1

    .line 230
    iget-object v3, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v2, Lcom/google/android/music/medialist/AlbumSongList;

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/google/android/music/medialist/AlbumSongList;->getAlbumYear(Landroid/content/Context;)I

    move-result v2

    iput v2, v3, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    # setter for: Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->access$102(Lcom/google/android/music/ui/MaterialTrackContainerFragment;Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .line 244
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mMaterialHeader:Lcom/google/android/music/ui/MaterialContainerHeaderView;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->access$200(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)Lcom/google/android/music/ui/MaterialContainerHeaderView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->access$100(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setContainerMetadata(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mHeaderBg:Lcom/google/android/music/ui/MaterialContainerHeaderBg;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->access$300(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    # getter for: Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->access$100(Lcom/google/android/music/ui/MaterialTrackContainerFragment;)Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->setHeaderArt(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)V

    .line 251
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-wide v2, v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumId:J

    iput-wide v2, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mAlbumId:J

    .line 252
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->mSavedMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    iget-object v1, v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->metajamAlbumId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mAlbumMetajamId:Ljava/lang/String;

    .line 255
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SongList;->supportsVideoCluster()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$2;->this$0:Lcom/google/android/music/ui/MaterialTrackContainerFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->initVideoCluster()V

    goto :goto_0
.end method
