.class Lcom/google/android/music/LicenseActivity$LicenseFileLoader;
.super Ljava/lang/Object;
.source "LicenseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/LicenseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LicenseFileLoader"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/google/android/music/LicenseActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/LicenseActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/music/LicenseActivity$LicenseFileLoader;->this$0:Lcom/google/android/music/LicenseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p2, p0, Lcom/google/android/music/LicenseActivity$LicenseFileLoader;->mHandler:Landroid/os/Handler;

    .line 42
    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/16 v8, 0x800

    .line 46
    const/4 v6, 0x0

    .line 48
    .local v6, "status":I
    const/4 v2, 0x0

    .line 49
    .local v2, "inputReader":Ljava/io/InputStreamReader;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 51
    .local v0, "data":Ljava/lang/StringBuilder;
    const/16 v8, 0x800

    :try_start_0
    new-array v7, v8, [C

    .line 54
    .local v7, "tmp":[C
    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v8, Ljava/util/zip/GZIPInputStream;

    iget-object v9, p0, Lcom/google/android/music/LicenseActivity$LicenseFileLoader;->this$0:Lcom/google/android/music/LicenseActivity;

    invoke-virtual {v9}, Lcom/google/android/music/LicenseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090007

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    .end local v2    # "inputReader":Ljava/io/InputStreamReader;
    .local v3, "inputReader":Ljava/io/InputStreamReader;
    :goto_0
    :try_start_1
    invoke-virtual {v3, v7}, Ljava/io/InputStreamReader;->read([C)I

    move-result v5

    .local v5, "numRead":I
    if-ltz v5, :cond_3

    .line 58
    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 60
    .end local v5    # "numRead":I
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 61
    .end local v3    # "inputReader":Ljava/io/InputStreamReader;
    .end local v7    # "tmp":[C
    .local v1, "e":Ljava/io/IOException;
    .restart local v2    # "inputReader":Ljava/io/InputStreamReader;
    :goto_1
    :try_start_2
    const-string v8, "LicenseActivity"

    const-string v9, "Error reading license HTML file"

    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 62
    const/4 v6, 0x1

    .line 65
    if-eqz v2, :cond_0

    .line 66
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 72
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    if-nez v6, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 73
    const-string v8, "LicenseActivity"

    const-string v9, "License HTML is empty"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v6, 0x2

    .line 78
    :cond_1
    iget-object v8, p0, Lcom/google/android/music/LicenseActivity$LicenseFileLoader;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v6, v9}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 79
    .local v4, "msg":Landroid/os/Message;
    if-nez v6, :cond_2

    .line 80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 82
    :cond_2
    iget-object v8, p0, Lcom/google/android/music/LicenseActivity$LicenseFileLoader;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 83
    return-void

    .line 65
    .end local v2    # "inputReader":Ljava/io/InputStreamReader;
    .end local v4    # "msg":Landroid/os/Message;
    .restart local v3    # "inputReader":Ljava/io/InputStreamReader;
    .restart local v5    # "numRead":I
    .restart local v7    # "tmp":[C
    :cond_3
    if-eqz v3, :cond_4

    .line 66
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    move-object v2, v3

    .line 69
    .end local v3    # "inputReader":Ljava/io/InputStreamReader;
    .restart local v2    # "inputReader":Ljava/io/InputStreamReader;
    goto :goto_2

    .line 68
    .end local v2    # "inputReader":Ljava/io/InputStreamReader;
    .restart local v3    # "inputReader":Ljava/io/InputStreamReader;
    :catch_1
    move-exception v8

    move-object v2, v3

    .line 70
    .end local v3    # "inputReader":Ljava/io/InputStreamReader;
    .restart local v2    # "inputReader":Ljava/io/InputStreamReader;
    goto :goto_2

    .line 64
    .end local v5    # "numRead":I
    .end local v7    # "tmp":[C
    :catchall_0
    move-exception v8

    .line 65
    :goto_3
    if-eqz v2, :cond_5

    .line 66
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 69
    :cond_5
    :goto_4
    throw v8

    .line 68
    .restart local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    goto :goto_2

    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v9

    goto :goto_4

    .line 64
    .end local v2    # "inputReader":Ljava/io/InputStreamReader;
    .restart local v3    # "inputReader":Ljava/io/InputStreamReader;
    .restart local v7    # "tmp":[C
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "inputReader":Ljava/io/InputStreamReader;
    .restart local v2    # "inputReader":Ljava/io/InputStreamReader;
    goto :goto_3

    .line 60
    .end local v7    # "tmp":[C
    :catch_4
    move-exception v1

    goto :goto_1
.end method
