.class public Lcom/google/android/music/art/ContainerMixDescriptorPair;
.super Ljava/lang/Object;
.source "ContainerMixDescriptorPair.java"


# instance fields
.field public final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field public final mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;


# direct methods
.method public constructor <init>(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 0
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 15
    iput-object p2, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20
    if-ne p0, p1, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v1

    .line 21
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 23
    check-cast v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;

    .line 25
    .local v0, "that":Lcom/google/android/music/art/ContainerMixDescriptorPair;
    iget-object v3, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-object v4, v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/ContainerDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    move v1, v2

    .line 27
    goto :goto_0

    .line 25
    :cond_5
    iget-object v3, v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-nez v3, :cond_4

    .line 29
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    iget-object v4, v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v3, v4}, Lcom/google/android/music/mix/MixDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 31
    goto :goto_0

    .line 29
    :cond_7
    iget-object v3, v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 39
    iget-object v2, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->hashCode()I

    move-result v0

    .line 40
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v1}, Lcom/google/android/music/mix/MixDescriptor;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 41
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 39
    goto :goto_0
.end method
