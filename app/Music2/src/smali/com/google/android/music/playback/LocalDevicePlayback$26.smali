.class Lcom/google/android/music/playback/LocalDevicePlayback$26;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$isGaplessEnabledForCached:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V
    .locals 0

    .prologue
    .line 3256
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-boolean p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->val$isGaplessEnabledForCached:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 3259
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v8

    const/4 v9, 0x1

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->getNextPlayPosition(ZZII)I
    invoke-static {v5, v6, v7, v8, v9}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9000(Lcom/google/android/music/playback/LocalDevicePlayback;ZZII)I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->findPlayableSong(IZZ)Landroid/util/Pair;
    invoke-static {v4, v5, v6, v7}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9100(Lcom/google/android/music/playback/LocalDevicePlayback;IZZ)Landroid/util/Pair;

    move-result-object v17

    .line 3265
    .local v17, "playPosAndMusicFile":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Lcom/google/android/music/store/MusicFile;>;"
    if-nez v17, :cond_1

    .line 3266
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->invalidateNextPlayer()V
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9200(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 3379
    :cond_0
    :goto_0
    return-void

    .line 3270
    :cond_1
    move-object/from16 v0, v17

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 3271
    .local v16, "nextpos":I
    move-object/from16 v0, v17

    iget-object v10, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/music/store/MusicFile;

    .line 3273
    .local v10, "preferredMusicFile":Lcom/google/android/music/store/MusicFile;
    if-ltz v16, :cond_0

    .line 3274
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getAudioIdAndListItemId(I)Landroid/util/Pair;

    move-result-object v13

    .line 3276
    .local v13, "audioIdAndListItemId":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/Long;>;"
    if-eqz v13, :cond_0

    .line 3277
    iget-object v15, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v15, Lcom/google/android/music/download/ContentIdentifier;

    .line 3279
    .local v15, "nextAudioId":Lcom/google/android/music/download/ContentIdentifier;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    invoke-virtual {v4, v15}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3280
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3281
    const-string v4, "LocalDevicePlayback"

    const-string v5, "Next track is the same."

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3285
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->invalidateNextPlayer()V
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9200(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 3287
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v14

    .line 3288
    .local v14, "currentPlayer":Lcom/google/android/music/playback/AsyncMediaPlayer;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v18

    .line 3290
    .local v18, "store":Lcom/google/android/music/store/Store;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayingLocally()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3292
    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    invoke-virtual {v15}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v6

    aput-wide v6, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->requiresDownloadManager([J)[J

    move-result-object v4

    if-eqz v4, :cond_3

    .line 3295
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->val$isGaplessEnabledForCached:Z

    if-eqz v4, :cond_0

    .line 3318
    :cond_3
    const-string v5, "LocalDevicePlayback"

    const-string v6, "setNextTrack: id=%s, playPos=%s, track=%s"

    const/4 v4, 0x3

    new-array v7, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v15, v7, v4

    const/4 v4, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    const/4 v8, 0x2

    if-eqz v10, :cond_4

    invoke-virtual {v10}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v4

    :goto_1
    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3323
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayer()Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9300(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v2

    .line 3324
    .local v2, "nextPlayer":Lcom/google/android/music/playback/AsyncMediaPlayer;
    if-nez v2, :cond_5

    .line 3325
    const-string v4, "LocalDevicePlayback"

    const-string v5, "createPlayer failed to crate new player"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3318
    .end local v2    # "nextPlayer":Lcom/google/android/music/playback/AsyncMediaPlayer;
    :cond_4
    const-string v4, "external"

    goto :goto_1

    .line 3328
    .restart local v2    # "nextPlayer":Lcom/google/android/music/playback/AsyncMediaPlayer;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v4, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$602(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/playback/AsyncMediaPlayer;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 3329
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v4, v15}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2202(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;

    .line 3330
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    move/from16 v0, v16

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayPos:I
    invoke-static {v4, v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 3332
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/music/playback/AsyncMediaPlayer;->getAudioSessionId()I

    move-result v4

    invoke-interface {v2, v4}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setAudioSessionId(I)V

    .line 3334
    move-object v3, v15

    .line 3335
    .local v3, "preferredNextAudioId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v10, :cond_6

    .line 3336
    new-instance v3, Lcom/google/android/music/download/ContentIdentifier;

    .end local v3    # "preferredNextAudioId":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {v10}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v4

    invoke-virtual {v10}, Lcom/google/android/music/store/MusicFile;->getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 3340
    .restart local v3    # "preferredNextAudioId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_6
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->duration(Lcom/google/android/music/download/ContentIdentifier;)J
    invoke-static {v6, v15}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$9400(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)J

    move-result-wide v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/playback/LocalDevicePlayback$26;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    new-instance v12, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v15, v14}, Lcom/google/android/music/playback/LocalDevicePlayback$26$1;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$26;Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/playback/AsyncMediaPlayer;)V

    invoke-interface/range {v2 .. v12}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setDataSource(Lcom/google/android/music/download/ContentIdentifier;JJZZLcom/google/android/music/store/MusicFile;Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;)V

    goto/16 :goto_0
.end method
