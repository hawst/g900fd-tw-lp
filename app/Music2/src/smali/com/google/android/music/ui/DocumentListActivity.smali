.class public Lcom/google/android/music/ui/DocumentListActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "DocumentListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/DocumentListActivity$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    .line 95
    return-void
.end method

.method public static final buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;ZZLcom/google/android/music/store/ContainerDescriptor;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p4, "isEmulatedRadio"    # Z
    .param p5, "playAll"    # Z
    .param p6, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "ZZ",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 105
    .local p2, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/DocumentListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 106
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const-string v1, "documentList"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 108
    const-string v1, "docType"

    invoke-virtual {p3}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 109
    const-string v1, "isEmulatedRadio"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 110
    const-string v1, "playAll"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 111
    const-string v1, "containerDescriptor"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 112
    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    .line 125
    const v0, 0x10a0002

    const v1, 0x10a0003

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/DocumentListActivity;->overridePendingTransition(II)V

    .line 126
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, -0x1

    const/4 v12, 0x0

    .line 37
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 40
    .local v4, "intent":Landroid/content/Intent;
    const-string v10, "title"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/music/ui/DocumentListActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentListActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v10

    if-nez v10, :cond_0

    .line 43
    const-string v10, "docType"

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 44
    .local v9, "typeOrdinal":I
    if-ne v9, v11, :cond_1

    .line 45
    const-string v10, "DocumentListActivity"

    const-string v11, "Document type not specified"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentListActivity;->finish()V

    .line 100
    .end local v9    # "typeOrdinal":I
    :cond_0
    :goto_0
    return-void

    .line 50
    .restart local v9    # "typeOrdinal":I
    :cond_1
    invoke-static {}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->values()[Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v10

    aget-object v8, v10, v9

    .line 51
    .local v8, "type":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    const-string v10, "documentList"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 52
    .local v1, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    if-nez v1, :cond_2

    .line 53
    const-string v10, "DocumentListActivity"

    const-string v11, "Document list not specified"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentListActivity;->finish()V

    goto :goto_0

    .line 58
    :cond_2
    const-string v10, "isEmulatedRadio"

    invoke-virtual {v4, v10, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 60
    .local v5, "isEmulatedRadio":Z
    const-string v10, "playAll"

    invoke-virtual {v4, v10, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 61
    .local v7, "playAll":Z
    const-string v10, "containerDescriptor"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/store/ContainerDescriptor;

    .line 63
    .local v0, "containerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    const/4 v2, 0x0

    .line 67
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v5, :cond_3

    .line 68
    new-instance v3, Lcom/google/android/music/ui/DocumentGridFragment;

    invoke-direct {v3}, Lcom/google/android/music/ui/DocumentGridFragment;-><init>()V

    .line 69
    .local v3, "gridFragment":Lcom/google/android/music/ui/DocumentGridFragment;
    const/4 v10, 0x1

    invoke-virtual {v3, v1, v8, v10}, Lcom/google/android/music/ui/DocumentGridFragment;->initialize(Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Z)V

    .line 70
    move-object v2, v3

    .line 98
    .end local v3    # "gridFragment":Lcom/google/android/music/ui/DocumentGridFragment;
    :goto_1
    invoke-virtual {p0, v2, v12}, Lcom/google/android/music/ui/DocumentListActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    goto :goto_0

    .line 74
    :cond_3
    sget-object v10, Lcom/google/android/music/ui/DocumentListActivity$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 93
    new-instance v3, Lcom/google/android/music/ui/DocumentGridFragment;

    invoke-direct {v3}, Lcom/google/android/music/ui/DocumentGridFragment;-><init>()V

    .line 94
    .restart local v3    # "gridFragment":Lcom/google/android/music/ui/DocumentGridFragment;
    invoke-virtual {v3, v1, v8, v12}, Lcom/google/android/music/ui/DocumentGridFragment;->initialize(Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Z)V

    .line 95
    move-object v2, v3

    goto :goto_1

    .line 79
    .end local v3    # "gridFragment":Lcom/google/android/music/ui/DocumentGridFragment;
    :pswitch_0
    new-instance v6, Lcom/google/android/music/ui/DocumentListFragment;

    invoke-direct {v6}, Lcom/google/android/music/ui/DocumentListFragment;-><init>()V

    .line 80
    .local v6, "listFragment":Lcom/google/android/music/ui/DocumentListFragment;
    invoke-virtual {v6, v1, v8, v7, v0}, Lcom/google/android/music/ui/DocumentListFragment;->initialize(Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;ZLcom/google/android/music/store/ContainerDescriptor;)V

    .line 81
    move-object v2, v6

    .line 82
    goto :goto_1

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method
