.class public abstract Lcom/google/android/music/leanback/LeanbackDetailsActivity;
.super Lcom/google/android/music/leanback/LeanbackItemActivity;
.source "LeanbackDetailsActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnActionClickedListener;
.implements Lcom/google/android/music/leanback/SongRowPresenter$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackItemActivity;-><init>()V

    return-void
.end method

.method private addClassPresenters(Landroid/support/v17/leanback/widget/ClassPresenterSelector;)V
    .locals 3
    .param p1, "classPresenterSelector"    # Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    .prologue
    .line 73
    new-instance v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackDetailsActivity;)V

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 98
    .local v0, "detailsOverviewRowPresenter":Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;
    invoke-virtual {v0, p0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setOnActionClickedListener(Landroid/support/v17/leanback/widget/OnActionClickedListener;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setBackgroundColor(I)V

    .line 101
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setStyleLarge(Z)V

    .line 102
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;->setTransitionInfo(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;)V

    .line 103
    const-string v1, "hero_image"

    invoke-virtual {v0, p0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setSharedElementEnterTransition(Landroid/app/Activity;Ljava/lang/String;)V

    .line 104
    const-class v1, Landroid/support/v17/leanback/widget/ListRow;

    new-instance v2, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v2}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-virtual {p1, v1, v2}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 105
    const-class v1, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    invoke-virtual {p1, v1, v0}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 107
    const-class v1, Lcom/google/android/music/leanback/SongRow;

    new-instance v2, Lcom/google/android/music/leanback/SongRowPresenter;

    invoke-direct {v2, p0}, Lcom/google/android/music/leanback/SongRowPresenter;-><init>(Lcom/google/android/music/leanback/SongRowPresenter$Listener;)V

    invoke-virtual {p1, v1, v2}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 108
    const-class v1, Lcom/google/android/music/leanback/SongTitleRow;

    new-instance v2, Lcom/google/android/music/leanback/SongTitleRowPresenter;

    invoke-direct {v2}, Lcom/google/android/music/leanback/SongTitleRowPresenter;-><init>()V

    invoke-virtual {p1, v1, v2}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 109
    const-class v1, Lcom/google/android/music/leanback/SongFooterRow;

    new-instance v2, Lcom/google/android/music/leanback/SongFooterRowPresenter;

    invoke-direct {v2}, Lcom/google/android/music/leanback/SongFooterRowPresenter;-><init>()V

    invoke-virtual {p1, v1, v2}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 110
    return-void
.end method


# virtual methods
.method protected createFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Landroid/support/v17/leanback/app/DetailsFragment;

    invoke-direct {v0}, Landroid/support/v17/leanback/app/DetailsFragment;-><init>()V

    .line 33
    .local v0, "detailsFragment":Landroid/support/v17/leanback/app/DetailsFragment;
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;->setupFragment(Landroid/app/Fragment;)V

    .line 34
    return-object v0
.end method

.method protected abstract getAdapter(Landroid/support/v17/leanback/widget/PresenterSelector;)Landroid/support/v17/leanback/widget/ObjectAdapter;
.end method

.method public onActionClicked(Landroid/support/v17/leanback/widget/Action;)V
    .locals 2
    .param p1, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 50
    instance-of v1, p1, Lcom/google/android/music/leanback/IntentAction;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 51
    check-cast v0, Lcom/google/android/music/leanback/IntentAction;

    .line 52
    .local v0, "intentAction":Lcom/google/android/music/leanback/IntentAction;
    invoke-virtual {v0}, Lcom/google/android/music/leanback/IntentAction;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/google/android/music/leanback/IntentAction;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 56
    .end local v0    # "intentAction":Lcom/google/android/music/leanback/IntentAction;
    :cond_0
    return-void
.end method

.method public onSongRowClicked(Lcom/google/android/music/leanback/SongRow;)V
    .locals 0
    .param p1, "songRow"    # Lcom/google/android/music/leanback/SongRow;

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/google/android/music/leanback/SongRow;->onClicked()V

    .line 61
    return-void
.end method

.method public onSongRowRadioClicked(Lcom/google/android/music/leanback/SongRow;)V
    .locals 0
    .param p1, "songRow"    # Lcom/google/android/music/leanback/SongRow;

    .prologue
    .line 65
    invoke-virtual {p1}, Lcom/google/android/music/leanback/SongRow;->onRadioClicked()V

    .line 66
    return-void
.end method

.method protected setTransitionInfo(Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;)V
    .locals 1
    .param p1, "detailsOverviewRowPresenter"    # Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    .prologue
    .line 69
    const-string v0, "hero_image"

    invoke-virtual {p1, p0, v0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setSharedElementEnterTransition(Landroid/app/Activity;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method protected setupFragment(Landroid/app/Fragment;)V
    .locals 3
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 39
    move-object v1, p1

    check-cast v1, Landroid/support/v17/leanback/app/DetailsFragment;

    .line 40
    .local v1, "detailsFragment":Landroid/support/v17/leanback/app/DetailsFragment;
    new-instance v0, Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;-><init>()V

    .line 41
    .local v0, "classPresenterSelector":Landroid/support/v17/leanback/widget/ClassPresenterSelector;
    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;->addClassPresenters(Landroid/support/v17/leanback/widget/ClassPresenterSelector;)V

    .line 43
    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackDetailsActivity;->getAdapter(Landroid/support/v17/leanback/widget/PresenterSelector;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/app/DetailsFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 44
    invoke-virtual {v1, p0}, Landroid/support/v17/leanback/app/DetailsFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 45
    invoke-virtual {v1, p0}, Landroid/support/v17/leanback/app/DetailsFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 46
    return-void
.end method
