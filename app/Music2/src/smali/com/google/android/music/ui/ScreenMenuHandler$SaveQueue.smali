.class Lcom/google/android/music/ui/ScreenMenuHandler$SaveQueue;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "ScreenMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ScreenMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveQueue"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ScreenMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ScreenMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/music/ui/ScreenMenuHandler$SaveQueue;->this$0:Lcom/google/android/music/ui/ScreenMenuHandler;

    .line 170
    const v0, 0x7f0e0030

    const v1, 0x7f0b00ce

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 171
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 8

    .prologue
    .line 175
    iget-object v5, p0, Lcom/google/android/music/ui/ScreenMenuHandler$SaveQueue;->this$0:Lcom/google/android/music/ui/ScreenMenuHandler;

    # getter for: Lcom/google/android/music/ui/ScreenMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;
    invoke-static {v5}, Lcom/google/android/music/ui/ScreenMenuHandler;->access$000(Lcom/google/android/music/ui/ScreenMenuHandler;)Lcom/google/android/music/ui/MusicFragment;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 176
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    .line 178
    :try_start_0
    sget-object v5, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v5}, Lcom/google/android/music/playback/IMusicPlaybackService;->getMediaList()Lcom/google/android/music/medialist/SongList;

    move-result-object v4

    .line 179
    .local v4, "songList":Lcom/google/android/music/medialist/SongList;
    new-instance v3, Lcom/google/android/music/ui/AddToPlaylistFragment;

    invoke-direct {v3}, Lcom/google/android/music/ui/AddToPlaylistFragment;-><init>()V

    .line 180
    .local v3, "fragment":Landroid/support/v4/app/Fragment;
    const-wide/16 v6, -0x1

    invoke-static {v4, v6, v7}, Lcom/google/android/music/ui/AddToPlaylistFragment;->createArgs(Lcom/google/android/music/medialist/SongList;J)Landroid/os/Bundle;

    move-result-object v1

    .line 181
    .local v1, "args":Landroid/os/Bundle;
    invoke-static {v0, v3, v1}, Lcom/google/android/music/ui/FragmentUtils;->addFragment(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v4    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_0
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v2

    .line 183
    .local v2, "e":Landroid/os/RemoteException;
    const-string v5, "ScreenMenuHandler"

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
