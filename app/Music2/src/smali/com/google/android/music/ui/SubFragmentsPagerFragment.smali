.class public Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "SubFragmentsPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;
    }
.end annotation


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAdapter:Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;

.field private mDefaultTab:Ljava/lang/String;

.field private mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mTabs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/FragmentTabInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 39
    new-instance v0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment$1;-><init>(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 148
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mAdapter:Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getDefaultTabIndex()I
    .locals 5

    .prologue
    .line 75
    iget-object v2, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 76
    .local v1, "tabsCount":I
    const/4 v0, 0x0

    .local v0, "tabIndex":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 77
    iget-object v2, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/ui/FragmentTabInfo;

    iget-object v2, v2, Lcom/google/android/music/ui/FragmentTabInfo;->mTag:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mDefaultTab:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    .end local v0    # "tabIndex":I
    :goto_1
    return v0

    .line 76
    .restart local v0    # "tabIndex":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_1
    const-string v2, "SubFgtPagerFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Default tab not found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mDefaultTab:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 71
    const v0, 0x7f040090

    return v0
.end method

.method protected init(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .param p2, "defaultSelection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/FragmentTabInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    iput-object p1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;

    .line 64
    iput-object p2, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mDefaultTab:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string v5, "accessibility"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 91
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getLayoutId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 92
    .local v1, "myView":Landroid/view/View;
    const v4, 0x7f0e01e8

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/PlayTabContainer;

    .line 93
    .local v3, "tabBar":Lcom/google/android/music/ui/PlayTabContainer;
    const v4, 0x7f0e01ea

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    .line 95
    .local v2, "pager":Landroid/support/v4/view/ViewPager;
    new-instance v4, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;-><init>(Lcom/google/android/music/ui/SubFragmentsPagerFragment;Landroid/support/v4/app/FragmentManager;)V

    iput-object v4, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mAdapter:Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;

    .line 97
    iget-object v4, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mAdapter:Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 101
    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 103
    invoke-direct {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getDefaultTabIndex()I

    move-result v0

    .line 104
    .local v0, "defaultIndex":I
    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 105
    new-instance v4, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;

    invoke-direct {v4, p0, v3, v2}, Lcom/google/android/music/ui/SubFragmentsPagerFragment$2;-><init>(Lcom/google/android/music/ui/SubFragmentsPagerFragment;Lcom/google/android/music/ui/PlayTabContainer;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c007d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/ui/PlayTabContainer;->setSelectedIndicatorColor(I)V

    .line 135
    invoke-virtual {v3, v2}, Lcom/google/android/music/ui/PlayTabContainer;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 136
    invoke-virtual {v3, v0}, Lcom/google/android/music/ui/PlayTabContainer;->onPageSelected(I)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v4, v5}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 139
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 145
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 146
    return-void
.end method
