.class Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;
.super Ljava/lang/Object;
.source "MaterialContainerHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialContainerHeaderView$2;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iput-object p2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iget-object v2, v2, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->fromSongList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v1

    # setter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$102(Lcom/google/android/music/ui/MaterialContainerHeaderView;Lcom/google/android/music/ui/cardlib/model/Document;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 80
    return-void
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 84
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$200(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/BaseListFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 91
    new-instance v0, Lcom/google/android/music/ui/ScreenMenuHandler;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iget-object v1, v1, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$200(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/BaseListFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->this$1:Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    iget-object v2, v2, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;->TRACK_CONTAINER:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/ScreenMenuHandler;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;)V

    .line 94
    .local v0, "menuHandler":Lcom/google/android/music/ui/ScreenMenuHandler;
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ScreenMenuHandler;->showPopupMenu(Landroid/view/View;)V

    goto :goto_0
.end method
