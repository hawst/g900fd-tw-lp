.class Lcom/google/android/music/cast/CastSessionManager$1;
.super Landroid/os/Handler;
.source "CastSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/CastSessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/CastSessionManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/CastSessionManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/music/cast/CastSessionManager$1;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 52
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 60
    const-string v0, "MusicCastSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled message:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_0
    return-void

    .line 54
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager$1;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->handleStartSession(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    invoke-static {v1, v0}, Lcom/google/android/music/cast/CastSessionManager;->access$000(Lcom/google/android/music/cast/CastSessionManager;Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    goto :goto_0

    .line 57
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$1;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->handleEndSession()V
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$100(Lcom/google/android/music/cast/CastSessionManager;)V

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
