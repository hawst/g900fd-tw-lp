.class public Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSharedPlaylistEntriesResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field public mEntries:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "entries"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "GetSharedPlaylists"

    sput-object v0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;->mEntries:Ljava/util/List;

    .line 19
    return-void
.end method
