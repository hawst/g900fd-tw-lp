.class Lcom/google/android/music/ui/TrackListAdapter$2;
.super Ljava/lang/Object;
.source "TrackListAdapter.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TrackListAdapter;->updatePlaybackState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private volatile mTmpAudioId:Lcom/google/android/music/download/ContentIdentifier;

.field private volatile mTmpIsInfiniteMix:Z

.field private volatile mTmpIsPlaying:Z

.field private volatile mTmpPosition:I

.field private volatile mTmpTrackMetajamId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/TrackListAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$currentSongList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/TrackListAdapter;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 661
    iput-object p1, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    iput-object p2, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->val$currentSongList:Lcom/google/android/music/medialist/SongList;

    iput-object p3, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 663
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpPosition:I

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 6

    .prologue
    .line 670
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isInInfiniteMixMode()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpIsInfiniteMix:Z

    .line 671
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getCurrentAudioId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpAudioId:Lcom/google/android/music/download/ContentIdentifier;

    .line 672
    iget-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->val$currentSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v4, v4, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    if-eqz v4, :cond_0

    .line 673
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getNowPlayingList()Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    .line 674
    .local v2, "playingSongList":Lcom/google/android/music/medialist/SongList;
    iget-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->val$currentSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v4, v2}, Lcom/google/android/music/medialist/SongList;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpIsPlaying:Z

    .line 687
    .end local v2    # "playingSongList":Lcom/google/android/music/medialist/SongList;
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpIsPlaying:Z

    if-eqz v4, :cond_3

    .line 688
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getQueuePosition()I

    move-result v4

    iput v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpPosition:I

    .line 692
    :goto_1
    return-void

    .line 676
    :cond_0
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->getCurrentTrackInfo()Lcom/google/android/music/playback/TrackInfo;

    move-result-object v3

    .line 677
    .local v3, "trackInfo":Lcom/google/android/music/playback/TrackInfo;
    iget-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->val$currentSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v5, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->val$context:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/google/android/music/medialist/SongList;->getContainerDescriptor(Landroid/content/Context;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 679
    .local v0, "adapterContainerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    const/4 v1, 0x0

    .line 680
    .local v1, "currentContainerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    if-eqz v3, :cond_1

    .line 681
    invoke-virtual {v3}, Lcom/google/android/music/playback/TrackInfo;->getContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 682
    invoke-virtual {v3}, Lcom/google/android/music/playback/TrackInfo;->getTrackMetajamId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpTrackMetajamId:Ljava/lang/String;

    .line 684
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/ContainerDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_2
    iput-boolean v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpIsPlaying:Z

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 690
    .end local v0    # "adapterContainerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v1    # "currentContainerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v3    # "trackInfo":Lcom/google/android/music/playback/TrackInfo;
    :cond_3
    const/4 v4, -0x1

    iput v4, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpPosition:I

    goto :goto_1
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->val$currentSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    # getter for: Lcom/google/android/music/ui/TrackListAdapter;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/TrackListAdapter;->access$000(Lcom/google/android/music/ui/TrackListAdapter;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 714
    :cond_0
    :goto_0
    return-void

    .line 699
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/TrackListAdapter;->isActivityValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    iget-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpIsInfiniteMix:Z

    # setter for: Lcom/google/android/music/ui/TrackListAdapter;->mIsInInfiniteMix:Z
    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->access$102(Lcom/google/android/music/ui/TrackListAdapter;Z)Z

    .line 703
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    iget-boolean v1, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpIsPlaying:Z

    # setter for: Lcom/google/android/music/ui/TrackListAdapter;->mIsContainerCurrentlyPlaying:Z
    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->access$202(Lcom/google/android/music/ui/TrackListAdapter;Z)Z

    .line 704
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    iget v1, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpPosition:I

    # setter for: Lcom/google/android/music/ui/TrackListAdapter;->mCurrentPlayPosition:I
    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->access$302(Lcom/google/android/music/ui/TrackListAdapter;I)I

    .line 705
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpAudioId:Lcom/google/android/music/download/ContentIdentifier;

    # setter for: Lcom/google/android/music/ui/TrackListAdapter;->mCurrentAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->access$402(Lcom/google/android/music/ui/TrackListAdapter;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;

    .line 706
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->mTmpTrackMetajamId:Ljava/lang/String;

    # setter for: Lcom/google/android/music/ui/TrackListAdapter;->mCurrentTrackMetajamId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->access$502(Lcom/google/android/music/ui/TrackListAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 708
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    # getter for: Lcom/google/android/music/ui/TrackListAdapter;->mTempRowMapping:Landroid/util/SparseIntArray;
    invoke-static {v0}, Lcom/google/android/music/ui/TrackListAdapter;->access$600(Lcom/google/android/music/ui/TrackListAdapter;)Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/google/android/music/ui/TrackListAdapter$2;->this$0:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/TrackListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
