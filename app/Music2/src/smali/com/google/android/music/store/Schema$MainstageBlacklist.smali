.class public interface abstract Lcom/google/android/music/store/Schema$MainstageBlacklist;
.super Ljava/lang/Object;
.source "Schema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/Schema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MainstageBlacklist"
.end annotation


# static fields
.field public static final REASON_TYPE_VALUE_ARTIST_PLAYING_NEAR_YOU:I

.field public static final REASON_TYPE_VALUE_FREE_FROM_GOOGLE:I

.field public static final REASON_TYPE_VALUE_IDENTIFIED_ON_SOUND_SEARCH:I

.field public static final REASON_TYPE_VALUE_RECENTLY_ADDED:I

.field public static final REASON_TYPE_VALUE_RECENTLY_CREATED:I

.field public static final REASON_TYPE_VALUE_RECENTLY_MODIFIED:I

.field public static final REASON_TYPE_VALUE_RECENTLY_PLAYED:I

.field public static final REASON_TYPE_VALUE_RECENTLY_PURCHASED:I

.field public static final REASON_TYPE_VALUE_RECENTLY_SUBSCRIBED:I

.field public static final REASON_TYPE_VALUE_RECOMMENDED_FOR_YOU_FROM_LOCKER:I

.field public static final REASON_TYPE_VALUE_RECOMMENDED_FOR_YOU_RADIO:I

.field public static final REASON_TYPE_VALUE_SUGGESTED_NEW_RELEASE:I

.field public static final REASON_TYPE_VALUE_UNKNOWN:I

.field public static final REASON_TYPE_VALUE_YOUTUBE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2085
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->UNKNOWN_REASON:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_UNKNOWN:I

    .line 2087
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_PURCHASED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_PURCHASED:I

    .line 2089
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_ADDED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_ADDED:I

    .line 2091
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_PLAYED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_PLAYED:I

    .line 2093
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_SUBSCRIBED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_SUBSCRIBED:I

    .line 2095
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_CREATED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_CREATED:I

    .line 2097
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECENTLY_MODIFIED:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECENTLY_MODIFIED:I

    .line 2099
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->SUGGESTED_NEW_RELEASE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_SUGGESTED_NEW_RELEASE:I

    .line 2101
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECOMMENDED_FOR_YOU_RADIO:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECOMMENDED_FOR_YOU_RADIO:I

    .line 2103
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->RECOMMENDED_FOR_YOU_FROM_LOCKER:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECOMMENDED_FOR_YOU_FROM_LOCKER:I

    .line 2105
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->IDENTIFIED_ON_SOUND_SEARCH:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_IDENTIFIED_ON_SOUND_SEARCH:I

    .line 2107
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->ARTIST_PLAYING_NEAR_YOU:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_ARTIST_PLAYING_NEAR_YOU:I

    .line 2109
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->FREE_FROM_GOOGLE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_FREE_FROM_GOOGLE:I

    .line 2111
    sget-object v0, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->YOUTUBE:Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/ListenNowSuggestionReason$SuggestionReason;->getValue()I

    move-result v0

    sput v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_YOUTUBE:I

    return-void
.end method
