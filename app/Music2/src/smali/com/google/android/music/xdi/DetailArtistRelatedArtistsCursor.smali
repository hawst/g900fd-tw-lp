.class Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistRelatedArtistsCursor.java"


# static fields
.field private static final LOGV:Z

.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->LOGV:Z

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mContext:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 49
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->addRows(Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method private addRows(Ljava/lang/String;)V
    .locals 20
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v17

    .line 54
    .local v17, "isNautilusArtistId":Z
    move-object/from16 v18, p1

    .line 55
    .local v18, "nautilusArtistId":Ljava/lang/String;
    if-nez v17, :cond_1

    .line 58
    :try_start_0
    invoke-static/range {p1 .. p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 63
    .local v11, "artistIdLong":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/android/music/xdi/XdiUtils;->getArtistMetajamId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    .line 64
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    sget-boolean v2, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->LOGV:Z

    if-eqz v2, :cond_0

    .line 66
    const-string v2, "MusicXdi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not get nautilus artist ID from long ID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".  Related artists will not be added."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .end local v11    # "artistIdLong":Ljava/lang/Long;
    :cond_0
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v15

    .line 60
    .local v15, "e":Ljava/lang/NumberFormatException;
    const-string v2, "MusicXdi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error converting artist ID to long: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    .end local v15    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mContext:Landroid/content/Context;

    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/MusicContent$Artists;->getRelatedArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v14

    .line 79
    .local v14, "cursor":Landroid/database/Cursor;
    if-eqz v14, :cond_0

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v2}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v19, v0

    .line 86
    .local v19, "values":[Ljava/lang/Object;
    :goto_1
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 87
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 88
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 89
    .local v13, "artistName":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 90
    .local v12, "artistMetajamId":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 91
    .local v10, "artUri":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 95
    :cond_2
    invoke-static {v12}, Lcom/google/android/music/xdi/XdiUtils;->getArtistDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v16

    .line 97
    .local v16, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "_id"

    invoke-interface {v14}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_name"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v13}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_subname"

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_description"

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "display_number"

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "image_uri"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "item_display_type"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v3, "action_uri"

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 111
    .end local v10    # "artUri":Ljava/lang/String;
    .end local v12    # "artistMetajamId":Ljava/lang/String;
    .end local v13    # "artistName":Ljava/lang/String;
    .end local v16    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v2

    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    :cond_3
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method
