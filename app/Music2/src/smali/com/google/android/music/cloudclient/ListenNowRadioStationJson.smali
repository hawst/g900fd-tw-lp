.class public Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowRadioStationJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;
    }
.end annotation


# instance fields
.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mHighlightColor:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "highlight_color"
    .end annotation
.end field

.field public mId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field

.field public mProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "profile_image"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 26
    return-void
.end method
