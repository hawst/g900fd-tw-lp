.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;
.super Ljava/lang/Object;
.source "DocumentMenuHandler.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->showPopupMenu(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final mSongList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V
    .locals 2

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 179
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, v8, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v8}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    iput-object v8, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    .line 180
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v5, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/ui/UIStateManager;->getStoreService()Lcom/google/android/music/store/IStoreService;

    move-result-object v4

    .line 184
    .local v4, "storeService":Lcom/google/android/music/store/IStoreService;
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    if-eqz v4, :cond_5

    move v5, v6

    :goto_1
    # setter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsValidStoreService:Z
    invoke-static {v8, v5}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$002(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Z)Z

    .line 185
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsValidStoreService:Z
    invoke-static {v5}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$000(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 186
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v8, v9, v4}, Lcom/google/android/music/medialist/SongList;->isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z

    move-result v8

    iput-boolean v8, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mIsSelectedForOfflineCaching:Z

    .line 191
    :goto_2
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v8, v9}, Lcom/google/android/music/medialist/SongList;->containsRemoteItems(Landroid/content/Context;)Z

    move-result v8

    iput-boolean v8, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mHasRemote:Z

    .line 192
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 194
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v5, v5, Lcom/google/android/music/medialist/NautilusSongList;

    if-eqz v5, :cond_2

    .line 195
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v5, Lcom/google/android/music/medialist/NautilusSongList;

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, v8, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v5, v8}, Lcom/google/android/music/medialist/NautilusSongList;->isAllInLibrary(Landroid/content/Context;)Z

    move-result v2

    .line 197
    .local v2, "isAllInLibrary":Z
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    if-nez v2, :cond_7

    :goto_3
    invoke-virtual {v5, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setCanAddToLibrary(Z)V

    .line 211
    .end local v2    # "isAllInLibrary":Z
    :cond_2
    :goto_4
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_3

    .line 212
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->getIsAllNautilus()Z
    invoke-static {v6}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$100(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setCanRemoveFromLibrary(Z)V

    .line 215
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v5, v6, :cond_4

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/model/Document;->getSourceAccount()I

    move-result v5

    if-eqz v5, :cond_4

    .line 218
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->getPlaylistShareState(Landroid/content/Context;J)I

    move-result v3

    .line 220
    .local v3, "shareState":I
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareState(I)V

    .line 224
    .end local v3    # "shareState":I
    :cond_4
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v5, v5, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v5, :cond_0

    .line 225
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v5, Lcom/google/android/music/medialist/SharedWithMeSongList;

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v5, v6}, Lcom/google/android/music/medialist/SharedWithMeSongList;->canFollow(Landroid/content/Context;)Z

    move-result v0

    .line 227
    .local v0, "canFollow":Z
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setCanFollow(Z)V

    goto/16 :goto_0

    .end local v0    # "canFollow":Z
    :cond_5
    move v5, v7

    .line 184
    goto/16 :goto_1

    .line 189
    :cond_6
    const-string v5, "DocumentMenuHandler"

    const-string v8, "Invalid StoreService instance"

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .restart local v2    # "isAllInLibrary":Z
    :cond_7
    move v6, v7

    .line 197
    goto :goto_3

    .line 204
    .end local v2    # "isAllInLibrary":Z
    :cond_8
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v5, v5, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v5, :cond_2

    .line 205
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v5, Lcom/google/android/music/medialist/AlbumSongList;

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v6, v6, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v5, v6}, Lcom/google/android/music/medialist/AlbumSongList;->hasPersistentNautilus(Landroid/content/Context;)Z

    move-result v1

    .line 207
    .local v1, "containsPersistentNautilus":Z
    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v5, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setCanRemoveFromLibrary(Z)V

    goto/16 :goto_4
.end method

.method public taskCompleted()V
    .locals 7

    .prologue
    .line 233
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v4, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
    invoke-static {v4}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$200(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 240
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v5}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    .line 241
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v4, v4, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    if-eqz v4, :cond_0

    .line 244
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
    invoke-static {v4}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$200(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->clearSpinnerItem()V

    .line 246
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 247
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    invoke-virtual {v4, v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->addMenuEntries(Ljava/util/List;)V

    .line 249
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 251
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;

    .line 252
    .local v1, "entry":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    new-instance v3, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;

    invoke-direct {v3, p0, v1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$1;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;)V

    .line 262
    .local v3, "listener":Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
    invoke-static {v4}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$200(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->menuTitle:Ljava/lang/String;

    iget-boolean v6, v1, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;->isEnabled:Z

    invoke-virtual {v4, v5, v6, v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    goto :goto_1

    .line 265
    .end local v1    # "entry":Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$MenuEntry;
    .end local v3    # "listener":Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # getter for: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mPopupMenu:Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
    invoke-static {v4}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$200(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;->show()V

    .line 269
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 270
    new-instance v4, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$2;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1$2;-><init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;)V

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$1;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v5, v5, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v4, v5}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    goto :goto_0
.end method
