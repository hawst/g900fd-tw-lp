.class final Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;
.super Ljava/lang/Object;
.source "AppNavigationMetajamHelper.java"

# interfaces
.implements Lcom/google/android/music/utils/MusicUtils$QueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AppNavigationMetajamHelper;->showNautilusTrack(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/google/android/music/ui/BaseActivity;

.field final synthetic val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

.field final synthetic val$metajamId:Ljava/lang/String;

.field final synthetic val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iput-object p2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$metajamId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    iput-object p4, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryComplete(Landroid/database/Cursor;)V
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 414
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v4}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v4}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 453
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 455
    :goto_0
    return-void

    .line 417
    :cond_1
    if-eqz p1, :cond_8

    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 418
    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v0, :cond_4

    .line 419
    .local v0, "availableInNautilus":Z
    :goto_1
    if-eqz v0, :cond_5

    .line 420
    # getter for: Lcom/google/android/music/ui/AppNavigationMetajamHelper;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->access$000()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 421
    const-string v3, "MusicNavigationHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Found track: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$metajamId:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/music/ui/TrackContainerActivity;->showNautilusAlbum(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;Landroid/view/View;)V

    .line 427
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v3}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->isAutoPlay()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 428
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v3}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 432
    .local v1, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v2, Lcom/google/android/music/medialist/NautilusSingleSongList;

    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v3}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    .local v2, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-static {v3, v2}, Lcom/google/android/music/utils/MusicUtils;->playRadio(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 436
    .end local v1    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v2    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v3}, Lcom/google/android/music/ui/BaseActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    .end local v0    # "availableInNautilus":Z
    :goto_2
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_4
    move v0, v3

    .line 418
    goto :goto_1

    .line 438
    .restart local v0    # "availableInNautilus":Z
    :cond_5
    :try_start_2
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v3}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->canSendToStore()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 439
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v4, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    iget-object v5, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    # invokes: Lcom/google/android/music/ui/AppNavigationMetajamHelper;->openTrackInStore(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    invoke-static {v3, v4, v5}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->access$200(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 453
    .end local v0    # "availableInNautilus":Z
    :catchall_0
    move-exception v3

    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v3

    .line 440
    .restart local v0    # "availableInNautilus":Z
    :cond_6
    :try_start_3
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    if-eqz v3, :cond_7

    .line 441
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    invoke-interface {v3}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;->onTrackError()V

    goto :goto_2

    .line 443
    :cond_7
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v3}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    goto :goto_2

    .line 447
    .end local v0    # "availableInNautilus":Z
    :cond_8
    const-string v3, "MusicNavigationHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Track not found for metajam id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$metajamId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v4, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$4;->val$metajamId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/music/purchase/Finsky;->startBuyTrackActivity(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method
