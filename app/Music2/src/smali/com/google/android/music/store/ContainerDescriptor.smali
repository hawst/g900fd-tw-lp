.class public Lcom/google/android/music/store/ContainerDescriptor;
.super Ljava/lang/Object;
.source "ContainerDescriptor.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/ContainerDescriptor$2;,
        Lcom/google/android/music/store/ContainerDescriptor$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private static final TYPE_VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;


# instance fields
.field private final mExtData:Ljava/lang/String;

.field private final mExtId:Ljava/lang/String;

.field private final mId:J

.field private final mName:Ljava/lang/String;

.field private final mType:Lcom/google/android/music/store/ContainerDescriptor$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor$Type;->values()[Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor;->TYPE_VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 358
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor$1;

    invoke-direct {v0}, Lcom/google/android/music/store/ContainerDescriptor$1;-><init>()V

    sput-object v0, Lcom/google/android/music/store/ContainerDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 120
    sget-object v0, Lcom/google/android/music/store/ContainerDescriptor;->TYPE_VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v1, v0, v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/store/ContainerDescriptor$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/store/ContainerDescriptor$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/store/ContainerDescriptor$Type;J)V
    .locals 8
    .param p1, "type"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p2, "id"    # J

    .prologue
    const/4 v4, 0x0

    .line 96
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V
    .locals 8
    .param p1, "type"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p2, "id"    # J
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 100
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "type"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p2, "id"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "extId"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p2, "id"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "extId"    # Ljava/lang/String;
    .param p6, "extData"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    if-nez p1, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mType:Lcom/google/android/music/store/ContainerDescriptor$Type;

    .line 112
    iput-wide p2, p0, Lcom/google/android/music/store/ContainerDescriptor;->mId:J

    .line 113
    iput-object p4, p0, Lcom/google/android/music/store/ContainerDescriptor;->mName:Ljava/lang/String;

    .line 114
    iput-object p5, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtId:Ljava/lang/String;

    .line 115
    iput-object p6, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtData:Ljava/lang/String;

    .line 116
    invoke-direct {p0}, Lcom/google/android/music/store/ContainerDescriptor;->validate()V

    .line 117
    return-void
.end method

.method static synthetic access$000()[Lcom/google/android/music/store/ContainerDescriptor$Type;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/music/store/ContainerDescriptor;->TYPE_VALUES:[Lcom/google/android/music/store/ContainerDescriptor$Type;

    return-object v0
.end method

.method private static decode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "encodedValue"    # Ljava/lang/String;

    .prologue
    .line 379
    const-string v0, "<null>"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    invoke-static {p0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 382
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 371
    if-nez p0, :cond_0

    .line 372
    const-string v0, "<null>"

    .line 374
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static newAlbumDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 2
    .param p0, "albumId"    # J
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 164
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newAllSongsInGenreDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 2
    .param p0, "genreId"    # J
    .param p2, "genreName"    # Ljava/lang/String;

    .prologue
    .line 197
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->ALL_SONGS_IN_A_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newArtistDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 2
    .param p0, "artistId"    # J
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newArtistShuffleDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p0, "artistMetajamId"    # Ljava/lang/String;
    .param p1, "artistName"    # Ljava/lang/String;

    .prologue
    .line 202
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->ARTIST_SHUFFLE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newAutoPlaylistDescriptor(Lcom/google/android/music/store/ContainerDescriptor$Type;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4
    .param p0, "type"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 129
    sget-object v0, Lcom/google/android/music/store/ContainerDescriptor$2;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {p0}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid auto playlist descriptor type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 138
    :pswitch_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    const-wide/16 v2, 0x2a

    invoke-direct {v0, p0, v2, v3, p1}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static newLuckyRadioDescriptor()Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;J)V

    return-object v0
.end method

.method public static newNautilusSingleSongDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p0, "nautilustTrackId"    # Ljava/lang/String;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Nautilus track id is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newNautuilusAlbumDescriptor(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p0, "albumId"    # J
    .param p2, "nautilusAlbumId"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "nautilusAlbumId is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-wide v2, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newNautuilusArtistDescriptor(JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p0, "artistId"    # J
    .param p2, "nautilusArtistId"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "nautilusArtistId is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-wide v2, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newPlaylistDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 2
    .param p0, "id"    # J
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newRadioDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 2
    .param p0, "localRadioId"    # J
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 192
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->RADIO:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newSearchResultsDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 2
    .param p0, "musicId"    # J
    .param p2, "searchString"    # Ljava/lang/String;

    .prologue
    .line 229
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->SEARCH_RESULTS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newSearchResultsDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p0, "trackMetajamId"    # Ljava/lang/String;
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 235
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackMetajamId is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->SEARCH_RESULTS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newSharedWithMePlaylistDescriptor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 7
    .param p0, "sharedToken"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "artUrl"    # Ljava/lang/String;

    .prologue
    .line 156
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sharedToken is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->SHARED_WITH_ME_PLAYLIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    move-object v4, p1

    move-object v5, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 2
    .param p0, "trackId"    # J
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 151
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;)V

    return-object v0
.end method

.method public static newTopSongsArtistDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p0, "artistMetajamId"    # Ljava/lang/String;
    .param p1, "artistName"    # Ljava/lang/String;

    .prologue
    .line 208
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "artistMetajamId is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_ARTIST:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newTopSongsGenreDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 6
    .param p0, "genreId"    # Ljava/lang/String;
    .param p1, "genreName"    # Ljava/lang/String;

    .prologue
    .line 216
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "genreId is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_0
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_GENRE:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newTopSongsInAllAccessDescriptor()Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4

    .prologue
    .line 224
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_IN_ALL_ACCESS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;J)V

    return-object v0
.end method

.method public static newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;
    .locals 4

    .prologue
    .line 243
    const-string v0, "ContainerDescriptor"

    const-string v1, "Using unknown descriptor type"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->UNKNOWN:Lcom/google/android/music/store/ContainerDescriptor$Type;

    const-wide/16 v2, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;J)V

    return-object v0
.end method

.method public static newUnvalidatedDescriptor(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 7
    .param p0, "type"    # Lcom/google/android/music/store/ContainerDescriptor$Type;
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "extId"    # Ljava/lang/String;
    .param p5, "extData"    # Ljava/lang/String;

    .prologue
    .line 249
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static parseFromExportString(Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 11
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 277
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v10, :cond_1

    .line 278
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v8, "String too short"

    invoke-direct {v0, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v8, ","

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 282
    .local v7, "items":[Ljava/lang/String;
    if-eqz v7, :cond_2

    array-length v0, v7

    const/4 v8, 0x5

    if-eq v0, v8, :cond_3

    .line 283
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v8, "Invalid number of items"

    invoke-direct {v0, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_3
    const/4 v0, 0x0

    aget-object v0, v7, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/music/store/ContainerDescriptor$Type;->fromDBValue(I)Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v1

    .line 287
    .local v1, "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    aget-object v0, v7, v9

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 288
    .local v2, "id":J
    aget-object v0, v7, v10

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/ContainerDescriptor;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 289
    .local v4, "name":Ljava/lang/String;
    const/4 v0, 0x3

    aget-object v0, v7, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/ContainerDescriptor;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 290
    .local v5, "extId":Ljava/lang/String;
    const/4 v0, 0x4

    aget-object v0, v7, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/ContainerDescriptor;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 291
    .local v6, "extData":Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/store/ContainerDescriptor;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/ContainerDescriptor;-><init>(Lcom/google/android/music/store/ContainerDescriptor$Type;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private validate()V
    .locals 0

    .prologue
    .line 390
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 313
    instance-of v3, p1, Lcom/google/android/music/store/ContainerDescriptor;

    if-nez v3, :cond_1

    move v1, v2

    .line 320
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 316
    check-cast v0, Lcom/google/android/music/store/ContainerDescriptor;

    .line 317
    .local v0, "other":Lcom/google/android/music/store/ContainerDescriptor;
    if-eq p0, p1, :cond_0

    .line 320
    iget-object v3, p0, Lcom/google/android/music/store/ContainerDescriptor;->mType:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/ContainerDescriptor$Type;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/music/store/ContainerDescriptor;->mId:J

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/store/ContainerDescriptor;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtData:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getExtData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public getExtData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtData:Ljava/lang/String;

    return-object v0
.end method

.method public getExtId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 257
    iget-wide v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mId:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/google/android/music/store/ContainerDescriptor$Type;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mType:Lcom/google/android/music/store/ContainerDescriptor$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 329
    const/4 v0, 0x0

    .line 330
    .local v0, "h":I
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mType:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    add-int/2addr v0, v1

    .line 331
    int-to-long v2, v0

    mul-int/lit8 v1, v0, 0x1f

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/google/android/music/store/ContainerDescriptor;->mId:J

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 332
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 333
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/store/ContainerDescriptor;->mName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 335
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 336
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 338
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtData:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 339
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 341
    :cond_2
    return v0
.end method

.method public isValid()Z
    .locals 4

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mType:Lcom/google/android/music/store/ContainerDescriptor$Type;

    sget-object v1, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_ALBUM:Lcom/google/android/music/store/ContainerDescriptor$Type;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mType:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-virtual {v1}, Lcom/google/android/music/store/ContainerDescriptor$Type;->getDBValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 299
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    iget-wide v2, p0, Lcom/google/android/music/store/ContainerDescriptor;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 301
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mName:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/ContainerDescriptor;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/ContainerDescriptor;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    iget-object v1, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtData:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/store/ContainerDescriptor;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mType:Lcom/google/android/music/store/ContainerDescriptor$Type;

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 352
    iget-wide v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 353
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/google/android/music/store/ContainerDescriptor;->mExtData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 356
    return-void
.end method
