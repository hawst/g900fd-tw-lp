.class Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;
.super Ljava/lang/Object;
.source "NowPlayingWidgetUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/NowPlayingWidgetUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WidgetParams"
.end annotation


# instance fields
.field public final hideThumbs:Z

.field public final layoutId:I

.field public final size:I


# direct methods
.method private constructor <init>(IIZ)V
    .locals 0
    .param p1, "layoutId"    # I
    .param p2, "size"    # I
    .param p3, "hideThumbs"    # Z

    .prologue
    .line 525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526
    iput p1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;->layoutId:I

    .line 527
    iput p2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;->size:I

    .line 528
    iput-boolean p3, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;->hideThumbs:Z

    .line 529
    return-void
.end method

.method synthetic constructor <init>(IIZLcom/google/android/music/utils/NowPlayingWidgetUtils$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/google/android/music/utils/NowPlayingWidgetUtils$1;

    .prologue
    .line 520
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/utils/NowPlayingWidgetUtils$WidgetParams;-><init>(IIZ)V

    return-void
.end method
