.class public Lcom/google/android/music/ui/dialogs/UndoDialog;
.super Landroid/app/Dialog;
.source "UndoDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIdsToRemove:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;

.field private mMsgText:Landroid/widget/TextView;

.field private mRemoveHandler:Landroid/os/Handler;

.field private mSongList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;Landroid/content/Context;)V
    .locals 5
    .param p1, "undoClickListener"    # Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const v3, 0x1030010

    invoke-direct {p0, p2, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 32
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mRemoveHandler:Landroid/os/Handler;

    .line 35
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;

    .line 50
    iput-object p2, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mContext:Landroid/content/Context;

    .line 52
    iput-object p1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mListener:Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;

    .line 54
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 55
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f040105

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 56
    .local v1, "layout":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/dialogs/UndoDialog;->setContentView(Landroid/view/View;)V

    .line 59
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/dialogs/UndoDialog;->setupWindow(Landroid/content/Context;)V

    .line 62
    const v3, 0x7f0e0230

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 63
    .local v2, "undoButton":Landroid/view/View;
    new-instance v3, Lcom/google/android/music/ui/dialogs/UndoDialog$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/dialogs/UndoDialog$1;-><init>(Lcom/google/android/music/ui/dialogs/UndoDialog;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    new-instance v3, Lcom/google/android/music/ui/dialogs/UndoDialog$2;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/dialogs/UndoDialog$2;-><init>(Lcom/google/android/music/ui/dialogs/UndoDialog;)V

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/dialogs/UndoDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 88
    const v3, 0x7f0e0251

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mMsgText:Landroid/widget/TextView;

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/dialogs/UndoDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/dialogs/UndoDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mRemoveHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/dialogs/UndoDialog;)Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/dialogs/UndoDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mListener:Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/dialogs/UndoDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/dialogs/UndoDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/dialogs/UndoDialog;Landroid/app/Dialog;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/dialogs/UndoDialog;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/dialogs/UndoDialog;->getRemoveRunnable(Landroid/app/Dialog;)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/dialogs/UndoDialog;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/dialogs/UndoDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/dialogs/UndoDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/dialogs/UndoDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getRemoveRunnable(Landroid/app/Dialog;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "undoDialog"    # Landroid/app/Dialog;

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/music/ui/dialogs/UndoDialog$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/dialogs/UndoDialog$3;-><init>(Lcom/google/android/music/ui/dialogs/UndoDialog;)V

    return-object v0
.end method

.method private setupWindow(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, -0x2

    .line 139
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    const v4, 0x7f0a009c

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 145
    const-string v3, "window"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 146
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 150
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 151
    .local v1, "wlp":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x50

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 152
    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 153
    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 154
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x6

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 155
    invoke-virtual {p0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 156
    return-void
.end method


# virtual methods
.method public setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 0
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 93
    return-void
.end method

.method public show(J)V
    .locals 7
    .param p1, "itemToRemoveId"    # J

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mMsgText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120008

    iget-object v3, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mIdsToRemove:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mRemoveHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/music/ui/dialogs/UndoDialog;->mRemoveHandler:Landroid/os/Handler;

    invoke-direct {p0, p0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->getRemoveRunnable(Landroid/app/Dialog;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 110
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 111
    return-void
.end method
