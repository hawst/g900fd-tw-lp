.class Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;
.super Ljava/lang/Object;
.source "AutoCacheSchedulingService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/AutoCacheSchedulingService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/AutoCacheSchedulingService;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    iput-object p2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 136
    const/4 v1, 0x0

    .line 137
    .local v1, "okToAttemptAutoCache":Z
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->val$intent:Landroid/content/Intent;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->val$intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "action":Ljava/lang/String;
    :goto_0
    # getter for: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "MusicLeanback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Service action:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->loadState()V
    invoke-static {v2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$100(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V

    .line 142
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    const-string v3, "com.google.android.music.leanback.APP_IN_USE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "com.google.android.music.leanback.CONNECTIVITY_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->updateSuggestedMixes(ZZ)Z
    invoke-static {v2, v3, v4}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$200(Lcom/google/android/music/leanback/AutoCacheSchedulingService;ZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 148
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->releaseWakeLock(Landroid/content/Intent;)V
    invoke-static {v2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$300(Landroid/content/Intent;)V

    .line 151
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    const-string v3, "com.google.android.music.leanback.POWER_CONNECTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->checkAutoCacheConditions(Z)Z
    invoke-static {v2, v3}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$400(Lcom/google/android/music/leanback/AutoCacheSchedulingService;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->startAutoCaching()V
    invoke-static {v2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$500(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V

    .line 158
    :goto_1
    return-void

    .line 137
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 148
    .restart local v0    # "action":Ljava/lang/String;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->releaseWakeLock(Landroid/content/Intent;)V
    invoke-static {v3}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$300(Landroid/content/Intent;)V

    throw v2

    .line 155
    :cond_2
    const-string v2, "MusicLeanback"

    const-string v3, "Conditions not met for autocaching."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v2, p0, Lcom/google/android/music/leanback/AutoCacheSchedulingService$1;->this$0:Lcom/google/android/music/leanback/AutoCacheSchedulingService;

    # invokes: Lcom/google/android/music/leanback/AutoCacheSchedulingService;->stopAutoCaching()V
    invoke-static {v2}, Lcom/google/android/music/leanback/AutoCacheSchedulingService;->access$600(Lcom/google/android/music/leanback/AutoCacheSchedulingService;)V

    goto :goto_1
.end method
