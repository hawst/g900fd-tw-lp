.class Lcom/google/android/music/xdi/XdiPlaybackService$2;
.super Ljava/lang/Object;
.source "XdiPlaybackService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/xdi/XdiPlaybackService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/xdi/XdiPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/xdi/XdiPlaybackService;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlaybackService$2;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "iBinder"    # Landroid/os/IBinder;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService$2;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/music/xdi/XdiPlaybackService;->mConnected:Z
    invoke-static {v0, v1}, Lcom/google/android/music/xdi/XdiPlaybackService;->access$102(Lcom/google/android/music/xdi/XdiPlaybackService;Z)Z

    .line 53
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService$2;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    # getter for: Lcom/google/android/music/xdi/XdiPlaybackService;->mPendingPlaybackIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/google/android/music/xdi/XdiPlaybackService;->access$200(Lcom/google/android/music/xdi/XdiPlaybackService;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService$2;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    iget-object v1, p0, Lcom/google/android/music/xdi/XdiPlaybackService$2;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    # getter for: Lcom/google/android/music/xdi/XdiPlaybackService;->mPendingPlaybackIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/music/xdi/XdiPlaybackService;->access$200(Lcom/google/android/music/xdi/XdiPlaybackService;)Landroid/content/Intent;

    move-result-object v1

    # invokes: Lcom/google/android/music/xdi/XdiPlaybackService;->playMusic(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/google/android/music/xdi/XdiPlaybackService;->access$300(Lcom/google/android/music/xdi/XdiPlaybackService;Landroid/content/Intent;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService$2;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/xdi/XdiPlaybackService;->mPendingPlaybackIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/google/android/music/xdi/XdiPlaybackService;->access$202(Lcom/google/android/music/xdi/XdiPlaybackService;Landroid/content/Intent;)Landroid/content/Intent;

    .line 57
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService$2;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/xdi/XdiPlaybackService;->mConnected:Z
    invoke-static {v0, v1}, Lcom/google/android/music/xdi/XdiPlaybackService;->access$102(Lcom/google/android/music/xdi/XdiPlaybackService;Z)Z

    .line 62
    return-void
.end method
