.class Lcom/google/android/music/ui/ManageDownloadsFragment$4;
.super Ljava/lang/Object;
.source "ManageDownloadsFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ManageDownloadsFragment;->applyDifferences(Landroid/util/Pair;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$listBackingDataCopy:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ManageDownloadsFragment;Ljava/util/List;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 565
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->val$listBackingDataCopy:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 568
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->val$listBackingDataCopy:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    .line 569
    .local v0, "entry":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    iget-object v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->songList:Lcom/google/android/music/medialist/SongList;

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->val$appContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/google/android/music/medialist/SongList;->getDownloadedSongCount(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->downloadedSongCount:I

    .line 570
    iget-object v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->songList:Lcom/google/android/music/medialist/SongList;

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->val$appContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/google/android/music/medialist/SongList;->getKeepOnSongCount(Landroid/content/Context;)I

    move-result v2

    iput v2, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->keeponSongCount:I

    goto :goto_0

    .line 572
    .end local v0    # "entry":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 576
    iget-object v1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/ManageDownloadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 577
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$4;->this$0:Lcom/google/android/music/ui/ManageDownloadsFragment;

    # getter for: Lcom/google/android/music/ui/ManageDownloadsFragment;->mAdapter:Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;
    invoke-static {v1}, Lcom/google/android/music/ui/ManageDownloadsFragment;->access$1300(Lcom/google/android/music/ui/ManageDownloadsFragment;)Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/ManageDownloadsFragment$DownloadListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
