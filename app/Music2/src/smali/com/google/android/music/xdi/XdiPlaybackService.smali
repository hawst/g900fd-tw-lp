.class public Lcom/google/android/music/xdi/XdiPlaybackService;
.super Landroid/app/Service;
.source "XdiPlaybackService.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private mConnected:Z

.field private mPendingPlaybackIntent:Landroid/content/Intent;

.field private mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

.field private mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/xdi/XdiPlaybackService;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 26
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlaybackService;->LOGV:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/music/xdi/XdiPlaybackService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlaybackService;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mConnected:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/music/xdi/XdiPlaybackService;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlaybackService;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mPendingPlaybackIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/xdi/XdiPlaybackService;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlaybackService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mPendingPlaybackIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/xdi/XdiPlaybackService;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlaybackService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiPlaybackService;->playMusic(Landroid/content/Intent;)V

    return-void
.end method

.method private playMusic(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 92
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlaybackService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 93
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "playMusic. intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/music/xdi/XdiPlayHelper;->processIntent(Landroid/content/Intent;)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_1
    const-string v0, "MusicXdi"

    const-string v1, "Playback service not initialized."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 104
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 36
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlaybackService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 37
    const-string v0, "MusicXdi"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :cond_0
    new-instance v0, Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-direct {v0, p0}, Lcom/google/android/music/xdi/XdiPlayHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    .line 40
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    new-instance v1, Lcom/google/android/music/xdi/XdiPlaybackService$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/xdi/XdiPlaybackService$1;-><init>(Lcom/google/android/music/xdi/XdiPlaybackService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper;->setCompletionListener(Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;)V

    .line 49
    new-instance v0, Lcom/google/android/music/xdi/XdiPlaybackService$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/xdi/XdiPlaybackService$2;-><init>(Lcom/google/android/music/xdi/XdiPlaybackService;)V

    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 64
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 69
    sget-boolean v0, Lcom/google/android/music/xdi/XdiPlaybackService;->LOGV:Z

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "MusicXdi"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mPlayHelper:Lcom/google/android/music/xdi/XdiPlayHelper;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/XdiPlayHelper;->cleanup()V

    .line 74
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V

    .line 77
    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mConnected:Z

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiPlaybackService;->playMusic(Landroid/content/Intent;)V

    .line 88
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    .line 86
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlaybackService;->mPendingPlaybackIntent:Landroid/content/Intent;

    goto :goto_0
.end method
