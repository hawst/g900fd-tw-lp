.class public Lcom/google/android/music/ui/DocumentGridFragment;
.super Lcom/google/android/music/ui/GridFragment;
.source "DocumentGridFragment.java"


# instance fields
.field protected final mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private mClearedFromOnStop:Z

.field private mDocList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mIsEmulatedRadio:Z

.field private mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mClearedFromOnStop:Z

    .line 36
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/DocumentGridFragment;->setRetainInstance(Z)V

    .line 41
    return-void
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Z)V
    .locals 0
    .param p2, "type"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p3, "isEmulatedRadio"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "doclist":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    iput-object p1, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mDocList:Ljava/util/List;

    .line 51
    iput-object p2, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 52
    iput-boolean p3, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mIsEmulatedRadio:Z

    .line 54
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/google/android/music/ui/GridFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 71
    iget-object v1, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mDocList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-nez v1, :cond_1

    .line 72
    :cond_0
    const-string v1, "DocumentGrid"

    const-string v2, "Arguments not initialized!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 85
    :goto_0
    return-void

    .line 77
    :cond_1
    new-instance v0, Lcom/google/android/music/ui/DocumentListAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mDocList:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    iget-object v4, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    iget-boolean v5, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mIsEmulatedRadio:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/DocumentListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/music/ui/cardlib/model/Document$Type;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Z)V

    .line 80
    .local v0, "docAdapter":Landroid/widget/ListAdapter;
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getScreenColumns()I

    move-result v7

    .line 82
    .local v7, "numColumns":I
    new-instance v6, Lcom/google/android/music/ui/GridAdapterWrapper;

    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v6, v1, v0, v7}, Lcom/google/android/music/ui/GridAdapterWrapper;-><init>(Landroid/content/Context;Landroid/widget/ListAdapter;I)V

    .line 84
    .local v6, "gridAdapter":Landroid/widget/ListAdapter;
    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/DocumentGridFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/android/music/ui/GridFragment;->onStart()V

    .line 90
    iget-boolean v0, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mClearedFromOnStop:Z

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mClearedFromOnStop:Z

    .line 92
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 94
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/android/music/ui/GridFragment;->onStop()V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/DocumentGridFragment;->mClearedFromOnStop:Z

    .line 100
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/utils/Utils;->clearPlayCardThumbnails(Landroid/view/ViewGroup;)V

    .line 101
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/GridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 62
    .local v0, "lv":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/google/android/music/ui/DocumentGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 64
    .local v1, "padding":I
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 65
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 66
    return-void
.end method
