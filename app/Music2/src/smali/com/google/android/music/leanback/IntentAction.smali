.class Lcom/google/android/music/leanback/IntentAction;
.super Landroid/support/v17/leanback/widget/Action;
.source "IntentAction.java"


# instance fields
.field private final mIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "label1"    # Ljava/lang/CharSequence;
    .param p4, "label2"    # Ljava/lang/CharSequence;
    .param p5, "intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/content/Intent;)V

    .line 22
    return-void
.end method

.method constructor <init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/content/Intent;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "label1"    # Ljava/lang/CharSequence;
    .param p4, "label2"    # Ljava/lang/CharSequence;
    .param p5, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p6, "intent"    # Landroid/content/Intent;

    .prologue
    .line 16
    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Landroid/support/v17/leanback/widget/Action;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 17
    iput-object p6, p0, Lcom/google/android/music/leanback/IntentAction;->mIntent:Landroid/content/Intent;

    .line 18
    return-void
.end method


# virtual methods
.method getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/music/leanback/IntentAction;->mIntent:Landroid/content/Intent;

    return-object v0
.end method
