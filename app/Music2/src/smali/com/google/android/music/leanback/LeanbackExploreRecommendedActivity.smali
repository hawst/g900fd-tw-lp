.class public Lcom/google/android/music/leanback/LeanbackExploreRecommendedActivity;
.super Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;
.source "LeanbackExploreRecommendedActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getGroupItemsUri(J)Landroid/net/Uri;
    .locals 1
    .param p1, "groupId"    # J

    .prologue
    .line 21
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected getGroupsUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedGroupsUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
