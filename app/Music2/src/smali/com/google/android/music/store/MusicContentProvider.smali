.class public Lcom/google/android/music/store/MusicContentProvider;
.super Landroid/content/ContentProvider;
.source "MusicContentProvider.java"


# static fields
.field private static final DEFAULT_SEARCH_SUGGESTIONS_PROJECTION:[Ljava/lang/String;

.field private static final LOGV:Z

.field private static sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAlbumArtistsProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAlbumsProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAutoPlaylistsProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sCloudQueueProjection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sGenresProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sGroupedMusicProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sKeepOnProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMusicProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPlaylistMembersProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPlaylistsProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sQueueProjection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sRadioProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSuggestedSeedsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sTrackArtistsProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mArtContentProviderHelper:Lcom/google/android/music/store/ArtContentProviderHelper;

.field private mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mNetworkMonitorConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

.field private mSituationsCache:Lcom/google/android/music/store/SituationsCache;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    .line 349
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_icon_large"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_data_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_last_access_hint"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->DEFAULT_SEARCH_SUGGESTIONS_PROJECTION:[Ljava/lang/String;

    .line 3125
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 3126
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "audio"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3127
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "audio/selected/*"

    const/16 v3, 0x132

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3130
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "audio/*"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3131
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "DownloadQueue"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3132
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "play"

    const/16 v3, 0x131

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3134
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "search/search_suggest_query"

    const/16 v3, 0x44f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3136
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "search/search_suggest_query/*"

    const/16 v3, 0x44e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3138
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "search"

    const/16 v3, 0x44d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3139
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "search/*"

    const/16 v3, 0x44c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3140
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "searchsuggestedquery"

    const/16 v3, 0x451

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3142
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "searchsuggestedquery/*"

    const/16 v3, 0x450

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3145
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "album"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3146
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "album/artists"

    const/16 v3, 0x192

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3148
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "album/store"

    const/16 v3, 0x193

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3150
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "album/*"

    const/16 v3, 0x191

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3152
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "album/*/audio"

    const/16 v3, 0x12e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3156
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artists"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3157
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artists/*"

    const/16 v3, 0x1f5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3159
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artists/*/album"

    const/16 v3, 0x1f6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3161
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artists/*/audio"

    const/16 v3, 0x12f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3164
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artists/*/topsongs"

    const/16 v3, 0x1f7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3167
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artists/*/artists"

    const/16 v3, 0x1f8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3170
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artists/*/artUrl"

    const/16 v3, 0x1f9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3174
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlists"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3175
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlists/suggested"

    const/16 v3, 0x25c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3177
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlists/recent"

    const/16 v3, 0x25d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3180
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlists/radio_stations"

    const/16 v3, 0x25e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3183
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlists/#"

    const/16 v3, 0x259

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3185
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlists/#/members"

    const/16 v3, 0x25a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3188
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlists/#/members/#"

    const/16 v3, 0x25b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3191
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "auto_playlists"

    const/16 v3, 0x26c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3193
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "auto_playlists/#"

    const/16 v3, 0x26d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3195
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "auto_playlists/#/members"

    const/16 v3, 0x26e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3199
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "genres"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3200
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "genres/#/members"

    const/16 v3, 0x2bd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3203
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "genres/#/album"

    const/16 v3, 0x2be

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3206
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "genres/album"

    const/16 v3, 0x2bf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3208
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "genres/#"

    const/16 v3, 0x2c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3210
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "albumart/#"

    const/16 v3, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3211
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlistfauxart/#"

    const/16 v3, 0x322

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3213
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "artistfauxart/#"

    const/16 v3, 0x321

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3215
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "albumorfauxart/#"

    const/16 v3, 0x323

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3217
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "largealbumart/#"

    const/16 v3, 0x325

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3219
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "albumfauxart/#"

    const/16 v3, 0x324

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3221
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "urlart"

    const/16 v3, 0x326

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3222
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "Recent"

    const/16 v3, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3223
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "Mainstage"

    const/16 v3, 0x708

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3225
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "KeepOn"

    const/16 v3, 0x3b6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3226
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "KeepOn/#"

    const/16 v3, 0x3b7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3228
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "MediaStore/audio/#"

    const/16 v3, 0x4b0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3231
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "account"

    const/16 v3, 0x514

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3233
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "isNautilusEnabled"

    const/16 v3, 0x515

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3235
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "isAcceptedUser"

    const/16 v3, 0x516

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3238
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "seeds"

    const/16 v3, 0x578

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3240
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "seeds/#"

    const/16 v3, 0x579

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3242
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "remote"

    const/16 v3, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3245
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/recommended"

    const/16 v3, 0x640

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3248
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/recommended/#"

    const/16 v3, 0x641

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3251
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/topcharts"

    const/16 v3, 0x64a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3254
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/topcharts/#"

    const/16 v3, 0x64b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3257
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/newreleases"

    const/16 v3, 0x654

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3260
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/newreleases/#"

    const/16 v3, 0x655

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3263
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/genres"

    const/16 v3, 0x65e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3266
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "explore/genres/*"

    const/16 v3, 0x65f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3270
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "radio_stations"

    const/16 v3, 0x6a4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3272
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "radio_stations/#"

    const/16 v3, 0x6a5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3275
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "shared_with_me_playlist/*"

    const/16 v3, 0x76c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3278
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "queue"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3281
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "queue/#"

    const/16 v3, 0x7d1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3284
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "playlist_share_state/#"

    const/16 v3, 0xbb9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3287
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "music_user_content"

    const/16 v3, 0x834

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3290
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "situations/topsituationheader"

    const/16 v3, 0xc1d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3293
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "situations/topsituation"

    const/16 v3, 0xc1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3296
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "situations/subsituations/*/"

    const/16 v3, 0xc1f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3299
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.MusicContent"

    const-string v2, "situations/radio_stations/*/"

    const/16 v3, 0xc20

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3302
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    .line 3303
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3304
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    const-string v1, "audio_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3305
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->addAudioMapping(Ljava/util/HashMap;Z)V

    .line 3306
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3307
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v5, v4}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3309
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    .line 3310
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3311
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    const-string v1, "audio_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3312
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v5}, Lcom/google/android/music/store/MusicContentProvider;->addAudioMapping(Ljava/util/HashMap;Z)V

    .line 3313
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(distinct(SongId))"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3315
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v5, v5}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3317
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    .line 3318
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "GenreId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3319
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "Genre"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3320
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "CanonicalGenre"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3321
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "album"

    const-string v2, "Album"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3322
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "AlbumArtist"

    const-string v2, "AlbumArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3323
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_id"

    const-string v2, "AlbumId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3324
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "SongCount"

    const-string v2, "count(distinct MUSIC.SongId)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3326
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "genreServerId"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3327
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "subgenreCount"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3328
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(distinct GenreId)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3329
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3331
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    .line 3332
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "AlbumArtistId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3333
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "artist"

    const-string v2, "AlbumArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3334
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "artistSort"

    const-string v2, "CanonicalAlbumArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3336
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "isTrackOnly"

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3337
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(distinct AlbumArtistId)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3339
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3341
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "ArtistMetajamId"

    const-string v2, "(SELECT m1.ArtistMetajamId FROM MUSIC as m1  WHERE m1.ArtistId=MUSIC.AlbumArtistId AND m1.ArtistMetajamId NOT NULL  GROUP BY m1.ArtistMetajamId ORDER BY count(1) DESC LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3343
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "artworkUrl"

    const-string v2, "(SELECT m1.ArtistArtLocation FROM MUSIC as m1  WHERE m1.ArtistId=MUSIC.AlbumArtistId AND m1.ArtistMetajamId NOT NULL AND m1.ArtistArtLocation NOT NULL GROUP BY m1.ArtistMetajamId ORDER BY count(1) DESC LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3346
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    .line 3347
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "ArtistId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3348
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "artist"

    const-string v2, "Artist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3349
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "artistSort"

    const-string v2, "CanonicalArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3351
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "isTrackOnly"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3352
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "artworkUrl"

    const-string v2, "max(ArtistArtLocation)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3355
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(distinct ArtistId)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3357
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3358
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "KeepOnId"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3359
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "ArtistMetajamId"

    const-string v2, "max(ArtistMetajamId)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3362
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    .line 3363
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "MUSIC.AlbumId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3364
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_id"

    const-string v2, "MUSIC.AlbumId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3365
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_name"

    const-string v2, "Album"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3366
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_sort"

    const-string v2, "CanonicalAlbum"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3367
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_art"

    const-string v2, "(SELECT AlbumArtLocation FROM MUSIC as t1 WHERE t1.AlbumId=MUSIC.AlbumId AND t1.AlbumArtLocation NOT NULL ORDER BY LocalCopyType DESC LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3368
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_artist"

    const-string v2, "AlbumArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3369
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_artist_sort"

    const-string v2, "CanonicalAlbumArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3371
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_artist_id"

    const-string v2, "AlbumArtistId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3372
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "SongCount"

    const-string v2, "count(distinct MUSIC.SongId)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3373
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "HasDifferentTrackArtists"

    const-string v2, "((min(MUSIC.ArtistId) != MUSIC.AlbumArtistId) OR (max(MUSIC.ArtistId) != MUSIC.AlbumArtistId))"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3375
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "isAllLocal"

    const-string v2, "NOT EXISTS(select 1 from MUSIC AS m  WHERE MUSIC.AlbumId=m.AlbumId GROUP BY m.SongId HAVING MAX(m.LocalCopyType = 0) LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3376
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "StoreAlbumId"

    const-string v2, "max(StoreAlbumId)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3377
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "ArtistMetajamId"

    const-string v2, "(SELECT m1.ArtistMetajamId FROM MUSIC as m1  WHERE m1.ArtistId=MUSIC.AlbumArtistId AND m1.ArtistMetajamId NOT NULL  GROUP BY m1.ArtistMetajamId ORDER BY count(1) DESC LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3379
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "hasPersistNautilus"

    const-string v2, "EXISTS(select 1 from MUSIC AS m  WHERE MUSIC.AlbumId=m.AlbumId AND +m.TrackType = 5 LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3381
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullNautilusMappings(Ljava/util/HashMap;)V

    .line 3382
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(distinct MUSIC.AlbumId)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3384
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v5, v5}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3385
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_year"

    const-string v2, "max(Year)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3386
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_description"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3387
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_description_attr_source_title"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3388
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_description_attr_source_url"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3389
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_description_attr_license_title"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3390
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_description_attr_license_url"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3392
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    .line 3393
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "c_album_id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3394
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_id"

    const-string v2, "c_album_id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3395
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_name"

    const-string v2, "c_album"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3396
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_sort"

    const-string v2, "c_canonicalAlbum"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3397
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_art"

    const-string v2, "(SELECT AlbumArtLocation FROM MUSIC as t1 WHERE t1.AlbumId=MUSIC.AlbumId AND t1.AlbumArtLocation NOT NULL ORDER BY LocalCopyType DESC LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3399
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_artist"

    const-string v2, "c_artist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3400
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_artist_sort"

    const-string v2, "c_canonicalArtist"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3402
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "album_artist_id"

    const-string v2, "c_artistId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3403
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "SongCount"

    const-string v2, "c_songCount"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3404
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "KeepOnId"

    const-string v2, "KEEPON.KeepOnId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3407
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "hasLocal"

    const-string v2, "EXISTS(select 1 from MUSIC WHERE AlbumId=c_album_id AND LocalCopyType IN (100,200,300) LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3411
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    const-string v1, "hasRemote"

    const-string v2, "EXISTS(select 1 from MUSIC WHERE AlbumId=c_album_id AND LocalCopyType != 300 LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3417
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 3418
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "LISTS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3419
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_name"

    const-string v2, "LISTS.Name"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3421
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_name_sort"

    const-string v2, "LISTS.NameSort"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3423
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_description"

    const-string v2, "Description"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3425
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_owner_name"

    const-string v2, "OwnerName"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3427
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_share_token"

    const-string v2, "ShareToken"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3429
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_art_url"

    const-string v2, "ListArtworkLocation"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3431
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_owner_profile_photo_url"

    const-string v2, "OwnerProfilePhotoUrl"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3434
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_id"

    const-string v2, "LISTS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3435
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3437
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "isAllLocal"

    const-string v2, "NOT EXISTS(select 1 from MUSIC AS m, LISTITEMS as i WHERE i.ListId=LISTS.Id AND m.Id=i.MusicId GROUP BY m.SongId HAVING MAX(m.LocalCopyType = 0) LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3438
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "hasLocal"

    const-string v2, "EXISTS (SELECT 1 FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE (ListId=LISTS.Id) AND LocalCopyType IN (100,200,300) LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3440
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "hasRemote"

    const-string v2, "EXISTS (SELECT 1 FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE (ListId=LISTS.Id) AND LocalCopyType != 300 LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3442
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_type"

    const-string v2, "ListType"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3443
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "SourceAccount"

    const-string v2, "SourceAccount"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3445
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "SourceId"

    const-string v2, "SourceId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3447
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContentProvider;->addKeepOnMapping(Ljava/util/HashMap;)V

    .line 3449
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    .line 3450
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "LISTITEMS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3452
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    const-string v1, "audio_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3454
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_id"

    const-string v2, "LISTS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3456
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->addAudioMapping(Ljava/util/HashMap;Z)V

    .line 3457
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3458
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4, v4}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3461
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 3462
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3464
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistsProjectionMap:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContentProvider;->addKeepOnMapping(Ljava/util/HashMap;)V

    .line 3465
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistsProjectionMap:Ljava/util/HashMap;

    const-string v1, "playlist_type"

    const-string v2, "ListType"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3468
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;

    .line 3469
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3471
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;

    const-string v1, "audio_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3474
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->addAudioMapping(Ljava/util/HashMap;Z)V

    .line 3475
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(distinct MUSIC.SongId)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3476
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3479
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    .line 3480
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "KeepOnId"

    const-string v2, "KEEPON.KeepOnId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3481
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "ListId"

    const-string v2, "KEEPON.ListId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3482
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "AlbumId"

    const-string v2, "KEEPON.AlbumId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3483
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "AutoListId"

    const-string v2, "KEEPON.AutoListId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3484
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "RadioStationId"

    const-string v2, "KEEPON.RadioStationId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3486
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "DateAdded"

    const-string v2, "KEEPON.DateAdded"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3489
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "songCount"

    const-string v2, "SongCount"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3490
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "downloadedSongCount"

    const-string v2, "DownloadedSongCount"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3493
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3495
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    .line 3496
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "SUGGESTED_SEEDS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3498
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    const-string v1, "SeedAudioId"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3499
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->addAudioMapping(Ljava/util/HashMap;Z)V

    .line 3500
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    const-string v1, "SeedListId"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3501
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3502
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    const-string v1, "hasLocal"

    const-string v2, "LocalCopyType IN (100,200,300)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3503
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    const-string v1, "hasRemote"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3505
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    .line 3506
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3507
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3508
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContentProvider;->addDefaultRadioMappings(Ljava/util/HashMap;)V

    .line 3509
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContentProvider;->addKeepOnMapping(Ljava/util/HashMap;)V

    .line 3510
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    const-string v1, "hasLocal"

    const-string v2, "EXISTS (SELECT 1 FROM RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id)  WHERE (RadioStationId=RADIO_STATIONS.Id) AND LocalCopyType IN (100,200,300) LIMIT 1)"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3511
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    const-string v1, "hasRemote"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3515
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    .line 3516
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "QUEUE_ITEMS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3518
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "audio_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3519
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->addAudioMapping(Ljava/util/HashMap;Z)V

    .line 3520
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "trackId"

    const-string v2, "MusicId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3521
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "itemState"

    const-string v2, "State"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3522
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "containerId"

    const-string v2, "QUEUE_CONTAINERS.ContainerId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3525
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "containerType"

    const-string v2, "QUEUE_CONTAINERS.Type"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3528
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "containerName"

    const-string v2, "QUEUE_CONTAINERS.Name"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3531
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    invoke-static {v0, v4, v4}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3532
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3534
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    .line 3535
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "CLOUD_QUEUE_ITEMS.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3537
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "audio_id"

    const-string v2, "MUSIC.Id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3538
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->addAudioMapping(Ljava/util/HashMap;Z)V

    .line 3539
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "trackId"

    const-string v2, "MusicId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3541
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "itemState"

    const-string v2, "State"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3543
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "containerId"

    const-string v2, "CLOUD_QUEUE_CONTAINERS.ContainerId"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3546
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "containerType"

    const-string v2, "CLOUD_QUEUE_CONTAINERS.Type"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3549
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "containerName"

    const-string v2, "CLOUD_QUEUE_CONTAINERS.Name"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3552
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    invoke-static {v0, v4, v4}, Lcom/google/android/music/store/MusicContentProvider;->addCategoryMappings(Ljava/util/HashMap;ZZ)V

    .line 3553
    sget-object v0, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3554
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static addAudioMapping(Ljava/util/HashMap;Z)V
    .locals 3
    .param p1, "isGrouped"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 3028
    const-string v0, "title"

    const-string v1, "Title"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3029
    const-string v0, "CanonicalName"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3030
    const-string v0, "album"

    const-string v1, "Album"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3031
    const-string v0, "CanonicalAlbum"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3032
    const-string v0, "album_id"

    const-string v1, "MUSIC.AlbumId"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3036
    const-string v0, "artist"

    const-string v1, "Artist"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3037
    const-string v0, "artist_id"

    const-string v1, "ArtistId"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3039
    const-string v0, "artistSort"

    const-string v1, "CanonicalAlbumArtist"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3041
    const-string v0, "StoreAlbumId"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3042
    const-string v0, "AlbumArtistId"

    const-string v1, "MUSIC.AlbumArtistId"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3045
    const-string v0, "AlbumArtist"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3046
    const-string v0, "AlbumArtistId"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3048
    const-string v0, "composer"

    const-string v1, "Composer"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3050
    const-string v0, "Genre"

    const-string v1, "Genre"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3051
    const-string v0, "GenreId"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3053
    const-string v0, "year"

    const-string v1, "Year"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3054
    const-string v0, "duration"

    const-string v1, "Duration"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3055
    const-string v0, "TrackCount"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3056
    const-string v0, "track"

    const-string v1, "TrackNumber"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3057
    const-string v0, "DiscCount"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3058
    const-string v0, "DiscNumber"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3059
    const-string v0, "Compilation"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3060
    const-string v0, "BitRate"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3061
    const-string v0, "FileDate"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3062
    const-string v0, "Size"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3063
    const-string v0, "Rating"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3065
    const-string v0, "StoreId"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3066
    const-string v0, "SongId"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3068
    const-string v0, "Domain"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3070
    const-string v0, "domainParam"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3073
    const-string v0, "bookmark"

    const-string v1, "0"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3074
    const-string v0, "is_podcast"

    const-string v1, "0"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3075
    const-string v0, "is_music"

    const-string v1, "1"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3076
    const-string v0, "mime_type"

    const-string v1, "\'audio/*\'"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3079
    const-string v0, "SourceId"

    const-string v1, "MUSIC.SourceId"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3080
    const-string v0, "SourceAccount"

    const-string v1, "MUSIC.SourceAccount"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3082
    const-string v0, "isAllPersistentNautilus"

    const-string v1, "NOT EXISTS(select 1 from MUSIC AS m WHERE MUSIC.SongId=m.SongId AND +m.TrackType != 5 LIMIT 1)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3084
    const-string v0, "trackAvailableForPurchase"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3085
    const-string v0, "trackAvailableForSubscription"

    const-string v1, "null"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3087
    invoke-static {p0}, Lcom/google/android/music/store/MusicContentProvider;->addNullNautilusMappings(Ljava/util/HashMap;)V

    .line 3089
    if-eqz p1, :cond_0

    .line 3090
    const-string v0, "Nid"

    const-string v1, "max(Nid)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3092
    const-string v0, "StoreAlbumId"

    const-string v1, "max(StoreAlbumId)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3094
    const-string v0, "ArtistMetajamId"

    const-string v1, "max(ArtistMetajamId)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3096
    const-string v0, "ArtistArtLocation"

    const-string v1, "max(ArtistArtLocation)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3098
    const-string v0, "AlbumArtLocation"

    const-string v1, "max(AlbumArtLocation)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3100
    const-string v0, "Vid"

    const-string v1, "max(Vid)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3102
    const-string v0, "VThumbnailUrl"

    const-string v1, "max(VThumbnailUrl)"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3117
    :goto_0
    const-string v0, "itemState"

    invoke-static {p0, v0, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3118
    const-string v0, "containerId"

    invoke-static {p0, v0, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3119
    const-string v0, "containerType"

    invoke-static {p0, v0, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3120
    const-string v0, "containerName"

    invoke-static {p0, v0, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3122
    return-void

    .line 3106
    :cond_0
    const-string v0, "Nid"

    const-string v1, "Nid"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3107
    const-string v0, "StoreAlbumId"

    const-string v1, "StoreAlbumId"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3108
    const-string v0, "ArtistMetajamId"

    const-string v1, "ArtistMetajamId"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3109
    const-string v0, "ArtistArtLocation"

    const-string v1, "ArtistArtLocation"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3110
    const-string v0, "AlbumArtLocation"

    const-string v1, "AlbumArtLocation"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3111
    const-string v0, "Vid"

    const-string v1, "Vid"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3112
    const-string v0, "VThumbnailUrl"

    const-string v1, "VThumbnailUrl"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static addCategoryMappings(Ljava/util/HashMap;ZZ)V
    .locals 2
    .param p1, "keepOn"    # Z
    .param p2, "group"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2939
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 2940
    invoke-static {p0}, Lcom/google/android/music/store/MusicContentProvider;->addKeepOnMapping(Ljava/util/HashMap;)V

    .line 2943
    :cond_0
    const-string v1, "hasLocal"

    if-eqz p2, :cond_1

    const-string v0, "(MAX(LocalCopyType)  IN (100,200,300))"

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 2945
    const-string v1, "hasRemote"

    if-eqz p2, :cond_2

    const-string v0, "(MIN(LocalCopyType)  != 300)"

    :goto_1
    invoke-static {p0, v1, v0}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 2947
    return-void

    .line 2943
    :cond_1
    const-string v0, "LocalCopyType IN (100,200,300)"

    goto :goto_0

    .line 2945
    :cond_2
    const-string v0, "LocalCopyType != 300"

    goto :goto_1
.end method

.method static addDefaultPlaylistMappings(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3649
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "playlist_id"

    const-string v1, "LISTS.Id"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3650
    const-string v0, "playlist_name"

    const-string v1, "LISTS.Name"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3651
    const-string v0, "playlist_type"

    const-string v1, "ListType"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3652
    const-string v0, "playlist_owner_name"

    const-string v1, "OwnerName"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3653
    const-string v0, "playlist_description"

    const-string v1, "Description"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3655
    const-string v0, "playlist_share_token"

    const-string v1, "ShareToken"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3657
    const-string v0, "playlist_art_url"

    const-string v1, "ListArtworkLocation"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3659
    const-string v0, "playlist_owner_profile_photo_url"

    const-string v1, "OwnerProfilePhotoUrl"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3661
    const-string v0, "playlist_editor_artwork"

    const-string v1, "EditorArtworkUrl"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3663
    return-void
.end method

.method static addDefaultRadioMappings(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3565
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "RADIO_STATIONS."

    .line 3567
    .local v0, "prefix":Ljava/lang/String;
    const-string v1, "radio_id"

    const-string v2, "RADIO_STATIONS.Id"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3570
    const-string v1, "radio_name"

    const-string v2, "RADIO_STATIONS.Name"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3573
    const-string v1, "radio_description"

    const-string v2, "RADIO_STATIONS.Description"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3576
    const-string v1, "radio_art"

    const-string v2, "RADIO_STATIONS.ArtworkLocation"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3579
    const-string v1, "radio_seed_source_id"

    const-string v2, "RADIO_STATIONS.SeedSourceId"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3582
    const-string v1, "radio_seed_source_type"

    const-string v2, "RADIO_STATIONS.SeedSourceType"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3585
    const-string v1, "radio_source_id"

    const-string v2, "RADIO_STATIONS.SourceId"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3588
    const-string v1, "radio_highlight_color"

    const-string v2, "RADIO_STATIONS.HighlightColor"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3591
    const-string v1, "radio_profile_image"

    const-string v2, "RADIO_STATIONS.ProfileImage"

    invoke-static {p0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3593
    return-void
.end method

.method static addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 3
    .param p1, "countExpression"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2928
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "_count"

    invoke-static {p0, v0, p1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 2930
    const-string v0, "count(*)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_count"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2933
    const-string v0, "hasAny"

    const-string v1, "1"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 2934
    return-void
.end method

.method static addKeepOnMapping(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2950
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "KeepOnId"

    const-string v1, "KEEPON.KeepOnId"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 2951
    const-string v0, "keeponSongCount"

    const-string v1, "SongCount"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 2952
    const-string v0, "keeponDownloadedSongCount"

    const-string v1, "DownloadedSongCount"

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 2954
    return-void
.end method

.method protected static addMapping(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 0
    .param p1, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2962
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2963
    return-void
.end method

.method static addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sql"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2972
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2973
    return-void
.end method

.method protected static varargs addNotNullCaseMapping(Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "cases"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2999
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CASE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3000
    .local v1, "sql":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 3001
    const-string v2, " WHEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3002
    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3003
    const-string v2, " IS NOT NULL THEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3004
    add-int/lit8 v2, v0, 0x1

    aget-object v2, p2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3000
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 3006
    :cond_0
    const-string v2, " END"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3007
    return-void
.end method

.method protected static varargs addNotNullCaseMappingWithDefault(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;
    .param p3, "cases"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3016
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CASE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3017
    .local v1, "sql":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p3

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 3018
    const-string v2, " WHEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3019
    aget-object v2, p3, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3020
    const-string v2, " IS NOT NULL THEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3021
    add-int/lit8 v2, v0, 0x1

    aget-object v2, p3, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3017
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 3023
    :cond_0
    const-string v2, " ELSE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3024
    const-string v2, " END"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 3025
    return-void
.end method

.method static addNullAlbumMappings(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3621
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "album_id"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3622
    const-string v0, "album_name"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3623
    const-string v0, "album_sort"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3624
    const-string v0, "album_art"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3625
    const-string v0, "album_artist"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3626
    const-string v0, "album_artist_id"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3627
    const-string v0, "album_artist_sort"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3628
    const-string v0, "StoreAlbumId"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3629
    const-string v0, "album_year"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3630
    const-string v0, "album_description"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3631
    const-string v0, "album_description_attr_source_title"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3632
    const-string v0, "album_description_attr_source_url"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3633
    const-string v0, "album_description_attr_license_title"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3634
    const-string v0, "album_description_attr_license_url"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3636
    invoke-static {p0}, Lcom/google/android/music/store/MusicContentProvider;->addNullNautilusMappings(Ljava/util/HashMap;)V

    .line 3639
    return-void
.end method

.method protected static addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 2
    .param p1, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2957
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "null AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2958
    return-void
.end method

.method static addNullNautilusMappings(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3685
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "artworkUrl"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3686
    return-void
.end method

.method static addNullRadioMappings(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3604
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "radio_id"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3605
    const-string v0, "radio_name"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3606
    const-string v0, "radio_description"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3607
    const-string v0, "radio_art"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3608
    const-string v0, "radio_seed_source_id"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3609
    const-string v0, "radio_seed_source_type"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3610
    const-string v0, "radio_source_id"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 3611
    return-void
.end method

.method static appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 2
    .param p0, "buffer"    # Ljava/lang/StringBuilder;
    .param p1, "condition"    # Ljava/lang/String;

    .prologue
    .line 2216
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2217
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2218
    const-string v0, " AND "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2220
    :cond_0
    const/16 v0, 0x28

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2222
    :cond_1
    return-object p0
.end method

.method static appendIgnoreRecommendedPlaylistCondition(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p0, "out"    # Ljava/lang/StringBuilder;

    .prologue
    .line 2291
    const-string v0, "ListType<>51"

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2293
    return-void
.end method

.method static appendIgnoreRecommendedRadioCondition(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p0, "out"    # Ljava/lang/StringBuilder;

    .prologue
    .line 2323
    const-string v0, "SourceId NOT NULL "

    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2324
    return-void
.end method

.method private appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V
    .locals 1
    .param p1, "out"    # Ljava/lang/StringBuilder;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "filter"    # I
    .param p4, "suppressDomainIndex"    # Z

    .prologue
    .line 2246
    if-eqz p3, :cond_0

    .line 2247
    const/4 v0, 0x3

    if-ne p3, v0, :cond_1

    .line 2250
    invoke-static {p4}, Lcom/google/android/music/store/Filters;->appendUserAllFilter(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2255
    :cond_0
    :goto_0
    return-void

    .line 2252
    :cond_1
    invoke-static {p3}, Lcom/google/android/music/store/Filters;->getFilter(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V
    .locals 1
    .param p1, "out"    # Ljava/lang/StringBuilder;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "suppressDomainIndex"    # Z

    .prologue
    .line 2240
    invoke-static {p2}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v0

    .line 2241
    .local v0, "filter":I
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 2242
    return-void
.end method

.method static appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;I)V
    .locals 3
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "filter"    # I

    .prologue
    .line 2269
    if-eqz p1, :cond_0

    .line 2270
    invoke-static {p1}, Lcom/google/android/music/store/Filters;->getFilter(I)Ljava/lang/String;

    move-result-object v1

    .line 2271
    .local v1, "musicFilter":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2275
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v2, "EXISTS (SELECT 1 FROM "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2276
    const-string v2, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2277
    const-string v2, " WHERE ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2278
    const-string v2, "ListId=LISTS.Id"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2279
    const-string v2, ") AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2280
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2281
    const-string v2, " LIMIT 1)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2283
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2286
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "musicFilter":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method static appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;)V
    .locals 1
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2258
    invoke-static {p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v0

    .line 2259
    .local v0, "filter":I
    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;I)V

    .line 2260
    return-void
.end method

.method static appendRadioFilteringCondition(Ljava/lang/StringBuilder;I)V
    .locals 3
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "filter"    # I

    .prologue
    .line 2297
    if-eqz p1, :cond_0

    .line 2298
    invoke-static {p1}, Lcom/google/android/music/store/Filters;->getFilter(I)Ljava/lang/String;

    move-result-object v1

    .line 2299
    .local v1, "musicFilter":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2302
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2306
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v2, "KEEPON.RadioStationId NOT NULL AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2307
    const-string v2, "EXISTS (SELECT 1 FROM "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2308
    const-string v2, "RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id) "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2309
    const-string v2, " WHERE ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2310
    const-string v2, "RadioStationId=RADIO_STATIONS.Id"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2311
    const-string v2, ") AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2312
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2313
    const-string v2, " LIMIT 1)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2315
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2318
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "musicFilter":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method static appendRadioFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;)V
    .locals 1
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2263
    invoke-static {p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v0

    .line 2264
    .local v0, "filter":I
    invoke-static {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->appendRadioFilteringCondition(Ljava/lang/StringBuilder;I)V

    .line 2265
    return-void
.end method

.method private checkHasGetAccountsPermission()V
    .locals 3

    .prologue
    .line 2859
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2861
    const-string v0, "MusicContentProvider"

    const-string v1, "Not enough permissions"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2862
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Caller (uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") does not have "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " permission."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2867
    :cond_0
    return-void
.end method

.method private checkSignatureOrSystem()V
    .locals 8

    .prologue
    .line 2875
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2876
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    move-result v4

    .line 2878
    .local v4, "signatureCheck":I
    if-nez v4, :cond_0

    const/4 v2, 0x1

    .line 2879
    .local v2, "passSignatureCheck":Z
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/store/MusicContentProvider;->getCallerPackages()[Ljava/lang/String;

    move-result-object v0

    .line 2880
    .local v0, "callingPackages":[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/google/android/music/store/MusicContentProvider;->isPackageFromSystem([Ljava/lang/String;)Z

    move-result v3

    .line 2881
    .local v3, "passSystemCheck":Z
    if-nez v2, :cond_1

    if-nez v3, :cond_1

    .line 2882
    new-instance v5, Ljava/lang/SecurityException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calling process ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") does not have same signature as this process ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2878
    .end local v0    # "callingPackages":[Ljava/lang/String;
    .end local v2    # "passSignatureCheck":Z
    .end local v3    # "passSystemCheck":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2887
    .restart local v0    # "callingPackages":[Ljava/lang/String;
    .restart local v2    # "passSignatureCheck":Z
    .restart local v3    # "passSystemCheck":Z
    :cond_1
    return-void
.end method

.method private static checkWritePermission()V
    .locals 3

    .prologue
    .line 2847
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2848
    const-string v0, "MusicContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Another application (uid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is attempting a write operation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2851
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Music content provider access is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2853
    :cond_0
    return-void
.end method

.method private static countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "groupByColumn"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2185
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "groupByColumn"    # Ljava/lang/String;
    .param p3, "condition"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 2190
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "groupByColumn"    # Ljava/lang/String;
    .param p3, "condition"    # Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2198
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 2199
    .local v0, "hasCondition":Z
    :goto_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    move v1, v2

    .line 2200
    .local v1, "hasSelection":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 2201
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT COUNT(1) AS _count FROM (SELECT 1 FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " AND ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GROUP BY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p5}, Lcom/google/android/music/store/Store;->executeRawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2208
    :goto_3
    return-object v2

    .end local v0    # "hasCondition":Z
    .end local v1    # "hasSelection":Z
    :cond_0
    move v0, v3

    .line 2198
    goto :goto_0

    .restart local v0    # "hasCondition":Z
    :cond_1
    move v1, v3

    .line 2199
    goto :goto_1

    .line 2201
    .restart local v1    # "hasSelection":Z
    :cond_2
    const-string v2, ""

    goto :goto_2

    .line 2208
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT COUNT(1) AS _count FROM (SELECT 1 FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " WHERE ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_4
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GROUP BY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/music/store/Store;->executeRawQuery(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_3

    :cond_4
    const-string v2, ""

    goto :goto_4
.end method

.method private getCallerPackages()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 2894
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2895
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 2896
    .local v0, "callingPackages":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 2897
    const-string v2, "MusicContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Calling binder id ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") did not have any packages"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2900
    :cond_0
    return-object v0
.end method

.method static getMusicFilterIndex(Landroid/net/Uri;)I
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2226
    const-string v2, "filter"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2227
    .local v1, "filterParam":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 2229
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2235
    :goto_0
    return v2

    .line 2230
    :catch_0
    move-exception v0

    .line 2231
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "MusicContentProvider"

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2235
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getShareStateParam(Landroid/net/Uri;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2808
    const-string v3, "sharedstate"

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2811
    .local v1, "shareStateString":Ljava/lang/String;
    const/4 v0, 0x1

    .line 2812
    .local v0, "shareState":I
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2813
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 2814
    .local v2, "value":I
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2815
    move v0, v2

    .line 2824
    .end local v2    # "value":I
    :cond_0
    :goto_0
    return v0

    .line 2816
    .restart local v2    # "value":I
    :cond_1
    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 2817
    move v0, v2

    goto :goto_0

    .line 2818
    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 2819
    move v0, v2

    goto :goto_0

    .line 2821
    :cond_3
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid share state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;
    .locals 5
    .param p1, "sortParam"    # Ljava/lang/String;
    .param p2, "inGroup"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 454
    const/4 v0, 0x1

    .line 455
    .local v0, "canGroupByOrder":Z
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_7

    .line 456
    const-string v2, "name"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 457
    const-string v1, "MUSIC.CanonicalName"

    .line 475
    .local v1, "sortOrder":Ljava/lang/String;
    :goto_0
    new-instance v2, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 458
    .end local v1    # "sortOrder":Ljava/lang/String;
    :cond_0
    const-string v2, "album"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 459
    const-string v1, "CanonicalAlbum, DiscNumber, TrackNumber, CanonicalName"

    .restart local v1    # "sortOrder":Ljava/lang/String;
    goto :goto_0

    .line 460
    .end local v1    # "sortOrder":Ljava/lang/String;
    :cond_1
    const-string v2, "artist"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 461
    const-string v1, "CanonicalArtist, CanonicalName"

    .restart local v1    # "sortOrder":Ljava/lang/String;
    goto :goto_0

    .line 462
    .end local v1    # "sortOrder":Ljava/lang/String;
    :cond_2
    const-string v2, "date"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 463
    if-eqz p2, :cond_3

    const-string v1, "MAX(MUSIC.FileDate) DESC "

    .line 465
    .restart local v1    # "sortOrder":Ljava/lang/String;
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 463
    .end local v1    # "sortOrder":Ljava/lang/String;
    :cond_3
    const-string v1, "MUSIC.FileDate DESC "

    goto :goto_1

    .line 466
    :cond_4
    const-string v2, "ratingTimestamp"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 467
    if-eqz p2, :cond_5

    const-string v1, "MAX(MUSIC.RatingTimestampMicrosec) DESC, MUSIC.CanonicalName"

    .restart local v1    # "sortOrder":Ljava/lang/String;
    :goto_2
    goto :goto_0

    .end local v1    # "sortOrder":Ljava/lang/String;
    :cond_5
    const-string v1, "MUSIC.RatingTimestampMicrosec DESC, MUSIC.CanonicalName"

    goto :goto_2

    .line 470
    :cond_6
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid sort param "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 473
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "sortOrder":Ljava/lang/String;
    goto :goto_0
.end method

.method private getStreamingAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 2120
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->getStreamingAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static hasCount([Ljava/lang/String;)Z
    .locals 6
    .param p0, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 435
    const/4 v0, 0x0

    .line 436
    .local v0, "foundCount":Z
    if-eqz p0, :cond_1

    .line 437
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 438
    aget-object v2, p0, v1

    const-string v3, "count("

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "_count"

    aget-object v3, p0, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 439
    :cond_0
    const/4 v0, 0x1

    .line 444
    .end local v1    # "i":I
    :cond_1
    if-eqz v0, :cond_3

    array-length v2, p0

    if-le v2, v5, :cond_3

    .line 445
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Count can be the only column in the projection. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, p0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, p0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 437
    .restart local v1    # "i":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 449
    .end local v1    # "i":I
    :cond_3
    return v0
.end method

.method private isAcceptedUser()Z
    .locals 6

    .prologue
    .line 2165
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    if-eq v4, v5, :cond_0

    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isGlass()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2166
    new-instance v4, Ljava/lang/SecurityException;

    const-string v5, "isAcceptedUser can only be accessed on from within music app or on a glass device"

    invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2169
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/store/MusicContentProvider;->checkSignatureOrSystem()V

    .line 2172
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 2173
    .local v2, "token":J
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 2175
    .local v1, "prefsOwner":Ljava/lang/Object;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 2177
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isAcceptedUser()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 2179
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 2180
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return v4

    .line 2179
    .end local v0    # "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :catchall_0
    move-exception v4

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 2180
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method

.method private isDownloadedOnlyMode()Z
    .locals 3

    .prologue
    .line 2151
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 2152
    .local v1, "prefsOwner":Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 2155
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 2157
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method private isNautilusUser()Z
    .locals 6

    .prologue
    .line 2127
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    if-eq v4, v5, :cond_0

    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isGlass()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2128
    new-instance v4, Ljava/lang/SecurityException;

    const-string v5, "isNautilusUser can only be accessed on from within music app or on a glass device"

    invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2131
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/store/MusicContentProvider;->checkSignatureOrSystem()V

    .line 2134
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 2135
    .local v2, "token":J
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 2137
    .local v1, "prefsOwner":Ljava/lang/Object;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 2139
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 2141
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 2142
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return v4

    .line 2141
    .end local v0    # "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :catchall_0
    move-exception v4

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 2142
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method

.method private isPackageFromSystem([Ljava/lang/String;)Z
    .locals 11
    .param p1, "packages"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 2907
    if-nez p1, :cond_1

    .line 2923
    :cond_0
    :goto_0
    return v7

    .line 2910
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 2911
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v6, v1, v3

    .line 2913
    .local v6, "pkge":Ljava/lang/String;
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v5, v6, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 2914
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v8, v0, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_2

    .line 2915
    const/4 v7, 0x1

    goto :goto_0

    .line 2917
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    .line 2918
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "MusicContentProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Could not find ApplicationInfo for package: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2911
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method static isValidInteger(Ljava/lang/String;)Z
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 408
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 410
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    const/4 v1, 0x1

    .line 417
    :cond_0
    :goto_0
    return v1

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "MusicContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid integer: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;
    .locals 129
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "cancellationSignal"    # Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    .prologue
    .line 495
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_0

    .line 496
    const-string v4, "MusicContentProvider"

    const-string v5, "query: uri: %s projectionIn=%s selection=%s selectionArgs=%s sortOrder=%s"

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v7, v9

    const/4 v9, 0x1

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/utils/DebugUtils;->arrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v9

    const/4 v9, 0x2

    aput-object p3, v7, v9

    const/4 v9, 0x3

    invoke-static/range {p4 .. p4}, Lcom/google/android/music/utils/DebugUtils;->arrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v9

    const/4 v9, 0x4

    aput-object p5, v7, v9

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v19

    .line 503
    .local v19, "store":Lcom/google/android/music/store/Store;
    new-instance v43, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct/range {v43 .. v43}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 504
    .local v43, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/16 v70, 0x0

    .line 505
    .local v70, "distinct":Z
    const/16 v83, 0x1

    .line 507
    .local v83, "ignoreSelection":Z
    const-string v4, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 508
    .local v51, "limit":Ljava/lang/String;
    const/16 v48, 0x0

    .line 509
    .local v48, "groupBy":Ljava/lang/String;
    new-instance v64, Ljava/lang/StringBuilder;

    invoke-direct/range {v64 .. v64}, Ljava/lang/StringBuilder;-><init>()V

    .line 510
    .local v64, "conditions":Ljava/lang/StringBuilder;
    sget-object v4, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 511
    .local v6, "matchUri":I
    const/16 v102, 0x0

    .line 512
    .local v102, "projection":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v41, 0x0

    .line 513
    .local v41, "isGlobalSearch":Z
    const/16 v94, 0x0

    .line 514
    .local v94, "mergeOnColumn":Ljava/lang/String;
    const/16 v98, 0x0

    .line 515
    .local v98, "nautilusCursor":Landroid/database/Cursor;
    const/16 v72, 0x0

    .line 518
    .local v72, "excludeClause":Ljava/lang/String;
    if-eqz p2, :cond_2

    move-object/from16 v0, p2

    array-length v4, v0

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    const-string v4, "hasAny"

    const/4 v5, 0x0

    aget-object v5, p2, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v74, 0x1

    .line 520
    .local v74, "existsCheck":Z
    :goto_0
    if-eqz v74, :cond_1

    .line 521
    const-string v51, "1"

    .line 522
    const/16 p5, 0x0

    .line 525
    :cond_1
    sparse-switch v6, :sswitch_data_0

    .line 2037
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown URI "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 518
    .end local v74    # "existsCheck":Z
    :cond_2
    const/16 v74, 0x0

    goto :goto_0

    .line 530
    .restart local v74    # "existsCheck":Z
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v121

    .line 531
    .local v121, "strAlbumId":Ljava/lang/String;
    invoke-static/range {v121 .. v121}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v54

    .line 532
    .local v54, "albumId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v54, v4

    if-lez v4, :cond_4

    if-eqz p2, :cond_4

    move-object/from16 v0, p2

    array-length v4, v0

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    const-string v4, "AlbumArtLocation"

    const/4 v5, 0x0

    aget-object v5, p2, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 534
    move-object/from16 v0, v19

    move-wide/from16 v1, v54

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->getRemoteArtCursorForAlbum(J)Landroid/database/Cursor;

    move-result-object v61

    .line 2095
    .end local v19    # "store":Lcom/google/android/music/store/Store;
    .end local v54    # "albumId":J
    .end local v121    # "strAlbumId":Ljava/lang/String;
    :cond_3
    :goto_1
    return-object v61

    .line 536
    .restart local v19    # "store":Lcom/google/android/music/store/Store;
    .restart local v54    # "albumId":J
    .restart local v121    # "strAlbumId":Ljava/lang/String;
    :cond_4
    const/16 v61, 0x0

    goto :goto_1

    .line 540
    .end local v54    # "albumId":J
    .end local v121    # "strAlbumId":Ljava/lang/String;
    :sswitch_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v39

    .line 541
    .local v39, "filter":I
    if-eqz v74, :cond_9

    .line 542
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 608
    :goto_2
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 609
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    .line 2040
    .end local v39    # "filter":I
    :cond_5
    :goto_3
    if-eqz v102, :cond_6

    .line 2041
    move-object/from16 v0, v43

    move-object/from16 v1, v102

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 2042
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_6

    .line 2043
    const-string v4, "MusicContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "projectionMap="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v102

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2047
    :cond_6
    move-object/from16 v0, v64

    move-object/from16 v1, v72

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2049
    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_8

    .line 2050
    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v128

    .line 2051
    .local v128, "whereClause":Ljava/lang/String;
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_7

    const-string v4, "MusicContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Append Where: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v128

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2052
    :cond_7
    move-object/from16 v0, v43

    move-object/from16 v1, v128

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2055
    .end local v128    # "whereClause":Ljava/lang/String;
    :cond_8
    if-eqz v83, :cond_ba

    .line 2056
    if-eqz p3, :cond_b9

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_b9

    .line 2057
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Selection is not supported. \'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\' is ignored"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 543
    .restart local v39    # "filter":I
    :cond_9
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 544
    if-nez v39, :cond_a

    .line 545
    const-string v4, "MUSIC"

    const-string v5, "AlbumId"

    move-object/from16 v0, v19

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 547
    :cond_a
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 548
    const-string v4, "MUSIC"

    const-string v5, "AlbumIdSourceText"

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-static {v0, v4, v5, v7}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 552
    :cond_b
    const-string v123, ""

    .line 553
    .local v123, "table":Ljava/lang/String;
    const-string v4, "metajamId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v57

    .line 555
    .local v57, "albumMetajamId":Ljava/lang/String;
    invoke-static/range {v57 .. v57}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    const/16 v75, 0x1

    .line 556
    .local v75, "hasAlbumMetajamId":Z
    :goto_4
    if-eqz v75, :cond_c

    .line 557
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StoreAlbumId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v57 .. v57}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    :cond_c
    const/4 v4, 0x3

    move/from16 v0, v39

    if-ne v0, v4, :cond_d

    if-eqz v75, :cond_10

    .line 562
    :cond_d
    const-string v123, "MUSIC LEFT  JOIN KEEPON ON (MUSIC.AlbumId = KEEPON.AlbumId) "

    .line 571
    :goto_5
    move-object/from16 v0, v43

    move-object/from16 v1, v123

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 573
    const-string v48, "MUSIC.AlbumIdSourceText"

    .line 574
    const-string p5, "MUSIC.AlbumIdSourceText"

    .line 577
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v116

    .line 578
    .local v116, "sortParam":Ljava/lang/String;
    if-eqz v116, :cond_e

    invoke-virtual/range {v116 .. v116}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_e

    .line 579
    const-string v4, "artist"

    move-object/from16 v0, v116

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 580
    const-string p5, "MUSIC.CanonicalAlbumArtist, MUSIC.CanonicalAlbum"

    .line 591
    :cond_e
    new-instance v84, Ljava/lang/StringBuilder;

    invoke-direct/range {v84 .. v84}, Ljava/lang/StringBuilder;-><init>()V

    .line 592
    .local v84, "innerConditions":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v84

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 593
    invoke-virtual/range {v84 .. v84}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_12

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v84 .. v84}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v85

    .line 597
    .local v85, "innerWhere":Ljava/lang/String;
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.AlbumIdSourceText IN (SELECT AlbumIdSourceText FROM MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v85

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v48, :cond_13

    const-string v4, ""

    :goto_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p5, :cond_14

    const-string v4, ""

    :goto_8
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v51, :cond_15

    const-string v4, ""

    :goto_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    const/16 v51, 0x0

    goto/16 :goto_2

    .line 555
    .end local v75    # "hasAlbumMetajamId":Z
    .end local v84    # "innerConditions":Ljava/lang/StringBuilder;
    .end local v85    # "innerWhere":Ljava/lang/String;
    .end local v116    # "sortParam":Ljava/lang/String;
    :cond_f
    const/16 v75, 0x0

    goto/16 :goto_4

    .line 567
    .restart local v75    # "hasAlbumMetajamId":Z
    :cond_10
    const-string v123, "MUSIC INDEXED BY MUSIC_DOMAIN_ALBUMID_SOURCE_TEXT_INDEX  LEFT  JOIN KEEPON ON (MUSIC.AlbumId = KEEPON.AlbumId) "

    goto/16 :goto_5

    .line 585
    .restart local v116    # "sortParam":Ljava/lang/String;
    :cond_11
    const-string v4, "album"

    move-object/from16 v0, v116

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 586
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid album sort parameter: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v116

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 593
    .restart local v84    # "innerConditions":Ljava/lang/StringBuilder;
    :cond_12
    const-string v85, ""

    goto :goto_6

    .line 597
    .restart local v85    # "innerWhere":Ljava/lang/String;
    :cond_13
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " GROUP BY "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_7

    :cond_14
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ORDER BY "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    :cond_15
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " LIMIT "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v51

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    .line 615
    .end local v39    # "filter":I
    .end local v57    # "albumMetajamId":Ljava/lang/String;
    .end local v75    # "hasAlbumMetajamId":Z
    .end local v84    # "innerConditions":Ljava/lang/StringBuilder;
    .end local v85    # "innerWhere":Ljava/lang/String;
    .end local v116    # "sortParam":Ljava/lang/String;
    .end local v123    # "table":Ljava/lang/String;
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 616
    .local v8, "id":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v7, p1

    move-object/from16 v9, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-result-object v110

    .line 619
    .local v110, "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-nez v110, :cond_16

    const/16 v61, 0x0

    goto/16 :goto_1

    .line 621
    :cond_16
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v98

    .line 622
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getLocalId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    .line 626
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    :cond_17
    if-nez v74, :cond_18

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 627
    :cond_18
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 633
    :goto_a
    const/16 p5, 0x0

    .line 634
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    .line 636
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.AlbumId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 629
    :cond_19
    const-string v4, "MUSIC LEFT  JOIN KEEPON ON (MUSIC.AlbumId = KEEPON.AlbumId) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 631
    const-string v48, "MUSIC.AlbumId"

    goto :goto_a

    .line 645
    .end local v8    # "id":Ljava/lang/String;
    :sswitch_3
    if-nez v74, :cond_1a

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 646
    :cond_1a
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v4}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v4

    .line 649
    :cond_1b
    new-instance v103, Ljava/lang/StringBuffer;

    invoke-direct/range {v103 .. v103}, Ljava/lang/StringBuffer;-><init>()V

    .line 650
    .local v103, "queryBuffer":Ljava/lang/StringBuffer;
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/music/store/Filters;->getFilter(I)Ljava/lang/String;

    move-result-object v39

    .line 651
    .local v39, "filter":Ljava/lang/String;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_20

    const/16 v77, 0x1

    .line 655
    .local v77, "hasFilter":Z
    :goto_b
    const/4 v4, 0x0

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 656
    const-string v4, "SELECT "

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 657
    sget-object v4, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistClustersProjectionMap:Ljava/util/HashMap;

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/music/utils/DbUtils;->formatProjection([Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 660
    const-string v4, " FROM (SELECT AlbumId AS c_album_id, Album AS c_album, AlbumArtist AS c_artist, AlbumArtistId AS c_artistId, CanonicalAlbumArtist AS c_canonicalArtist, CanonicalAlbum AS c_canonicalAlbum, count(distinct(SongId)) AS c_songCount FROM MUSIC"

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 670
    if-eqz v77, :cond_1c

    .line 671
    const-string v4, " WHERE "

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 673
    :cond_1c
    const-string v4, " GROUP BY AlbumId, AlbumArtistId UNION SELECT AlbumId AS c_album_id, Album AS c_album, Artist AS c_artist, ArtistId AS c_artistId, CanonicalArtist AS c_canonicalArtist, CanonicalAlbum AS c_canonicalAlbum, count(distinct(SongId)) AS c_songCount FROM MUSIC WHERE (ArtistId<>AlbumArtistId) AND EXISTS(SELECT 1 FROM MUSIC AS m WHERE m.AlbumArtistId=MUSIC.ArtistId"

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 693
    if-eqz v77, :cond_1d

    .line 695
    const-string v4, " AND "

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 699
    :cond_1d
    const-string v4, " LIMIT 1) "

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 700
    if-eqz v77, :cond_1e

    .line 702
    const-string v4, " AND "

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 705
    :cond_1e
    const-string v4, " GROUP BY AlbumId, ArtistId) LEFT JOIN KEEPON ON (c_album_id=AlbumId) ORDER BY c_canonicalArtist, c_canonicalAlbum"

    move-object/from16 v0, v103

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 714
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 716
    .local v10, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual/range {v103 .. v103}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v10, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    .line 717
    .local v61, "c":Landroid/database/Cursor;
    if-eqz v61, :cond_1f

    .line 718
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 719
    invoke-interface/range {v61 .. v61}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 723
    :cond_1f
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_1

    .line 651
    .end local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v61    # "c":Landroid/database/Cursor;
    .end local v77    # "hasFilter":Z
    :cond_20
    const/16 v77, 0x0

    goto/16 :goto_b

    .line 723
    .restart local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v77    # "hasFilter":Z
    :catchall_0
    move-exception v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4

    .line 731
    .end local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v39    # "filter":Ljava/lang/String;
    .end local v77    # "hasFilter":Z
    .end local v103    # "queryBuffer":Ljava/lang/StringBuffer;
    :sswitch_4
    const-string v4, "storeAlbumId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v119

    .line 732
    .local v119, "storeAlbumId":Ljava/lang/String;
    invoke-static/range {v119 .. v119}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_23

    const/16 v78, 0x1

    .line 733
    .local v78, "hasStoreAlbumId":Z
    :goto_c
    if-eqz v78, :cond_24

    .line 734
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StoreAlbumId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v119 .. v119}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742
    :goto_d
    const-string v4, "TrackType IN (2,3,1)"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    if-nez v74, :cond_21

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 749
    :cond_21
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 750
    const/16 p5, 0x0

    .line 777
    :cond_22
    :goto_e
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    .line 779
    goto/16 :goto_3

    .line 732
    .end local v78    # "hasStoreAlbumId":Z
    :cond_23
    const/16 v78, 0x0

    goto :goto_c

    .line 737
    .restart local v78    # "hasStoreAlbumId":Z
    :cond_24
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto :goto_d

    .line 752
    :cond_25
    const-string v4, "MUSIC LEFT  JOIN KEEPON ON (MUSIC.AlbumId = KEEPON.AlbumId) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 754
    const-string v48, "MUSIC.AlbumId"

    .line 755
    if-eqz v78, :cond_26

    .line 756
    const/16 p5, 0x0

    goto :goto_e

    .line 758
    :cond_26
    const-string p5, "MUSIC.CanonicalAlbum"

    .line 761
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v116

    .line 762
    .restart local v116    # "sortParam":Ljava/lang/String;
    if-eqz v116, :cond_22

    invoke-virtual/range {v116 .. v116}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_22

    .line 763
    const-string v4, "artist"

    move-object/from16 v0, v116

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 764
    const-string p5, "MUSIC.CanonicalAlbumArtist, MUSIC.CanonicalAlbum"

    goto :goto_e

    .line 769
    :cond_27
    const-string v4, "album"

    move-object/from16 v0, v116

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_22

    .line 770
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid album sort parameter: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v116

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 782
    .end local v78    # "hasStoreAlbumId":Z
    .end local v116    # "sortParam":Ljava/lang/String;
    .end local v119    # "storeAlbumId":Ljava/lang/String;
    :sswitch_5
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v39

    .line 783
    .local v39, "filter":I
    if-eqz v74, :cond_28

    .line 784
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 836
    :goto_f
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    .line 837
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    goto/16 :goto_3

    .line 785
    :cond_28
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 786
    if-nez v39, :cond_29

    .line 787
    const-string v4, "MUSIC"

    const-string v5, "AlbumArtistId"

    move-object/from16 v0, v19

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 789
    :cond_29
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 790
    const-string v4, "MUSIC"

    const-string v5, "CanonicalAlbumArtist"

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-static {v0, v4, v5, v7}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 794
    :cond_2a
    const-string v123, ""

    .line 795
    .restart local v123    # "table":Ljava/lang/String;
    const-string v4, "metajamId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v58

    .line 797
    .local v58, "artistMetajamId":Ljava/lang/String;
    invoke-static/range {v58 .. v58}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2d

    const/16 v76, 0x1

    .line 798
    .local v76, "hasArtistMetajamId":Z
    :goto_10
    if-eqz v76, :cond_2b

    .line 799
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ArtistMetajamId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v58 .. v58}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804
    :cond_2b
    const/4 v4, 0x4

    move/from16 v0, v39

    if-eq v0, v4, :cond_2c

    const/4 v4, 0x5

    move/from16 v0, v39

    if-eq v0, v4, :cond_2c

    if-eqz v76, :cond_2e

    .line 807
    :cond_2c
    const-string v123, "MUSIC"

    .line 814
    :goto_11
    move-object/from16 v0, v43

    move-object/from16 v1, v123

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 816
    const-string v48, "MUSIC.CanonicalAlbumArtist"

    .line 817
    const-string p5, "MUSIC.CanonicalAlbumArtist"

    .line 819
    new-instance v84, Ljava/lang/StringBuilder;

    invoke-direct/range {v84 .. v84}, Ljava/lang/StringBuilder;-><init>()V

    .line 820
    .restart local v84    # "innerConditions":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v84

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 821
    invoke-virtual/range {v84 .. v84}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_2f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v84 .. v84}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v85

    .line 825
    .restart local v85    # "innerWhere":Ljava/lang/String;
    :goto_12
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.CanonicalAlbumArtist IN (SELECT CanonicalAlbumArtist FROM MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v85

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v48, :cond_30

    const-string v4, ""

    :goto_13
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p5, :cond_31

    const-string v4, ""

    :goto_14
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v51, :cond_32

    const-string v4, ""

    :goto_15
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    const/16 v51, 0x0

    goto/16 :goto_f

    .line 797
    .end local v76    # "hasArtistMetajamId":Z
    .end local v84    # "innerConditions":Ljava/lang/StringBuilder;
    .end local v85    # "innerWhere":Ljava/lang/String;
    :cond_2d
    const/16 v76, 0x0

    goto/16 :goto_10

    .line 811
    .restart local v76    # "hasArtistMetajamId":Z
    :cond_2e
    const-string v123, "MUSIC INDEXED BY MUSIC_DOMAIN_CANONICAL_ALBUM_ARTIST_INDEX"

    goto :goto_11

    .line 821
    .restart local v84    # "innerConditions":Ljava/lang/StringBuilder;
    :cond_2f
    const-string v85, ""

    goto :goto_12

    .line 825
    .restart local v85    # "innerWhere":Ljava/lang/String;
    :cond_30
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " GROUP BY "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_13

    :cond_31
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ORDER BY "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_14

    :cond_32
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " LIMIT "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v51

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_15

    .line 844
    .end local v39    # "filter":I
    .end local v58    # "artistMetajamId":Ljava/lang/String;
    .end local v76    # "hasArtistMetajamId":Z
    .end local v84    # "innerConditions":Ljava/lang/StringBuilder;
    .end local v85    # "innerWhere":Ljava/lang/String;
    .end local v123    # "table":Ljava/lang/String;
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 845
    .restart local v8    # "id":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_34

    .line 846
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v7, p1

    move-object/from16 v9, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-result-object v110

    .line 849
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-nez v110, :cond_33

    const/4 v4, 0x0

    :goto_16
    move-object/from16 v61, v4

    goto/16 :goto_1

    :cond_33
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    goto :goto_16

    .line 852
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    :cond_34
    const/4 v4, 0x1

    new-array v13, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v8, v13, v4

    .line 861
    .local v13, "artistLookupId":[Ljava/lang/String;
    const/16 v61, 0x0

    .line 863
    .restart local v61    # "c":Landroid/database/Cursor;
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 865
    .restart local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v74, :cond_35

    :try_start_1
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_36

    .line 866
    :cond_35
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 870
    :goto_17
    sget-object v4, Lcom/google/android/music/store/MusicContentProvider;->sAlbumArtistsProjectionMap:Ljava/util/HashMap;

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 871
    const-string v12, "MUSIC.AlbumArtistId=?"

    const-string v14, "MUSIC.AlbumArtistId"

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v9, v43

    move-object/from16 v11, p2

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    .line 875
    if-eqz v61, :cond_37

    invoke-interface/range {v61 .. v61}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_37

    .line 876
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 895
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_1

    .line 868
    :cond_36
    :try_start_2
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_17

    .line 895
    :catchall_1
    move-exception v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4

    .line 879
    :cond_37
    :try_start_3
    invoke-static/range {v61 .. v61}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 880
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 881
    sget-object v4, Lcom/google/android/music/store/MusicContentProvider;->sTrackArtistsProjectionMap:Ljava/util/HashMap;

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 882
    const-string v12, "MUSIC.ArtistId=?"

    const-string v14, "MUSIC.ArtistId"

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v9, v43

    move-object/from16 v11, p2

    invoke-virtual/range {v9 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    .line 888
    if-eqz v61, :cond_38

    .line 889
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 890
    invoke-interface/range {v61 .. v61}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 895
    :cond_38
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_1

    .line 901
    .end local v8    # "id":Ljava/lang/String;
    .end local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v13    # "artistLookupId":[Ljava/lang/String;
    .end local v61    # "c":Landroid/database/Cursor;
    :sswitch_7
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 902
    .local v18, "artistId":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 903
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->isDownloadedOnlyMode()Z

    move-result v4

    if-eqz v4, :cond_39

    .line 904
    new-instance v61, Landroid/database/MatrixCursor;

    const/4 v4, 0x0

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 906
    :cond_39
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v15

    move/from16 v16, v6

    move-object/from16 v17, p1

    move-object/from16 v19, p2

    invoke-static/range {v14 .. v19}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    .end local v19    # "store":Lcom/google/android/music/store/Store;
    move-result-object v110

    .line 908
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-nez v110, :cond_3a

    const/16 v61, 0x0

    goto/16 :goto_1

    .line 911
    :cond_3a
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 914
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    .restart local v19    # "store":Lcom/google/android/music/store/Store;
    :cond_3b
    if-nez v74, :cond_3c

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 915
    :cond_3c
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 916
    const/16 p5, 0x0

    .line 924
    :goto_18
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    .line 926
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.AlbumArtistId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 931
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto/16 :goto_3

    .line 918
    :cond_3d
    const-string v4, "MUSIC LEFT  JOIN KEEPON ON (MUSIC.AlbumId = KEEPON.AlbumId) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 921
    const-string v48, "MUSIC.AlbumId"

    .line 922
    const-string p5, "MUSIC.CanonicalAlbum"

    goto :goto_18

    .line 936
    .end local v18    # "artistId":Ljava/lang/String;
    :sswitch_8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->isDownloadedOnlyMode()Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 937
    new-instance v61, Landroid/database/MatrixCursor;

    const/4 v4, 0x0

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 939
    :cond_3e
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 940
    .restart local v18    # "artistId":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_40

    .line 941
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v15

    move/from16 v16, v6

    move-object/from16 v17, p1

    move-object/from16 v19, p2

    invoke-static/range {v14 .. v19}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    .end local v19    # "store":Lcom/google/android/music/store/Store;
    move-result-object v110

    .line 943
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-eqz v110, :cond_3f

    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    :goto_19
    move-object/from16 v61, v4

    goto/16 :goto_1

    :cond_3f
    const/4 v4, 0x0

    goto :goto_19

    .line 945
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    .restart local v19    # "store":Lcom/google/android/music/store/Store;
    :cond_40
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Top songs query is only supported fornautilus artists."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 950
    .end local v18    # "artistId":Ljava/lang/String;
    :sswitch_9
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->isDownloadedOnlyMode()Z

    move-result v4

    if-eqz v4, :cond_41

    .line 951
    new-instance v61, Landroid/database/MatrixCursor;

    const/4 v4, 0x0

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 953
    :cond_41
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 954
    .restart local v18    # "artistId":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_43

    .line 955
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v15

    move/from16 v16, v6

    move-object/from16 v17, p1

    move-object/from16 v19, p2

    invoke-static/range {v14 .. v19}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    .end local v19    # "store":Lcom/google/android/music/store/Store;
    move-result-object v110

    .line 957
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-eqz v110, :cond_42

    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    :goto_1a
    move-object/from16 v61, v4

    goto/16 :goto_1

    :cond_42
    const/4 v4, 0x0

    goto :goto_1a

    .line 959
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    .restart local v19    # "store":Lcom/google/android/music/store/Store;
    :cond_43
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Related artists query is only supported fornautilus artists."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 964
    .end local v18    # "artistId":Ljava/lang/String;
    :sswitch_a
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 965
    .restart local v18    # "artistId":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_45

    .line 966
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v15

    move/from16 v16, v6

    move-object/from16 v17, p1

    move-object/from16 v19, p2

    invoke-static/range {v14 .. v19}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    .end local v19    # "store":Lcom/google/android/music/store/Store;
    move-result-object v110

    .line 968
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-eqz v110, :cond_44

    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    :goto_1b
    move-object/from16 v61, v4

    goto/16 :goto_1

    :cond_44
    const/4 v4, 0x0

    goto :goto_1b

    .line 970
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    .restart local v19    # "store":Lcom/google/android/music/store/Store;
    :cond_45
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Artist art url query is only supported fornautilus artists."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 975
    .end local v18    # "artistId":Ljava/lang/String;
    :sswitch_b
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 976
    if-nez v74, :cond_46

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_47

    .line 977
    :cond_46
    const/16 p5, 0x0

    .line 982
    :goto_1c
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    .line 983
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 985
    const-string v4, "MUSIC.GenreId > 0"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 979
    :cond_47
    const-string v48, "MUSIC.GenreId"

    .line 980
    const-string p5, "MUSIC.CanonicalGenre"

    goto :goto_1c

    .line 992
    :sswitch_c
    if-nez v74, :cond_48

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_48

    .line 993
    const-string v48, "MUSIC.GenreId"

    .line 995
    :cond_48
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 996
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.GenreId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 998
    const/16 p5, 0x0

    .line 999
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    .line 1002
    goto/16 :goto_3

    .line 1005
    :sswitch_d
    if-nez v74, :cond_49

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4a

    .line 1006
    :cond_49
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v4}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v4

    .line 1008
    :cond_4a
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1009
    const-string p5, "MUSIC.CanonicalGenre, MUSIC.CanonicalAlbum"

    .line 1011
    const-string v48, "MUSIC.AlbumId, MUSIC.GenreId"

    .line 1015
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGenresProjectionMap:Ljava/util/HashMap;

    .line 1016
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1018
    const-string v4, "MUSIC.GenreId > 0"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1023
    :sswitch_e
    if-nez v74, :cond_4b

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4d

    .line 1024
    :cond_4b
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1025
    const/16 p5, 0x0

    .line 1040
    :cond_4c
    :goto_1d
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    .line 1042
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GenreId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v7, 0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1046
    const-string v4, "albumIdFilter"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v56

    .line 1047
    .local v56, "albumIdFilter":Ljava/lang/String;
    if-eqz v56, :cond_5

    .line 1048
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.AlbumId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v56

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1050
    const-string p5, "DiscNumber, TrackNumber, CanonicalName"

    goto/16 :goto_3

    .line 1027
    .end local v56    # "albumIdFilter":Ljava/lang/String;
    :cond_4d
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1028
    const-string v48, "MUSIC.SongId"

    .line 1030
    if-eqz p5, :cond_4e

    .line 1031
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "sortOrder not supported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1033
    :cond_4e
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 p5, v0

    .end local p5    # "sortOrder":Ljava/lang/String;
    check-cast p5, Ljava/lang/String;

    .line 1035
    .restart local p5    # "sortOrder":Ljava/lang/String;
    if-nez p5, :cond_4c

    .line 1037
    const-string p5, "MUSIC.CanonicalName"

    goto/16 :goto_1d

    .line 1056
    :sswitch_f
    if-nez v74, :cond_4f

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_50

    .line 1057
    :cond_4f
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1058
    const/16 p5, 0x0

    .line 1066
    :goto_1e
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAlbumsProjectionMap:Ljava/util/HashMap;

    .line 1068
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.GenreId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v7, 0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1071
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto/16 :goto_3

    .line 1060
    :cond_50
    const-string v4, "MUSIC LEFT  JOIN KEEPON ON (MUSIC.AlbumId = KEEPON.AlbumId) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1063
    const-string v48, "MUSIC.AlbumId"

    .line 1064
    const-string p5, "MUSIC.CanonicalAlbum"

    goto :goto_1e

    .line 1075
    :sswitch_10
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1076
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v39

    .line 1077
    .restart local v39    # "filter":I
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_53

    .line 1078
    if-nez v39, :cond_52

    if-eqz p3, :cond_51

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_52

    .line 1081
    :cond_51
    const-string v4, "MUSIC"

    const-string v5, "SongId"

    move-object/from16 v0, v19

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 1083
    :cond_52
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 1084
    const-string v20, "MUSIC"

    const-string v21, "CanonicalName, SongId"

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v23, p3

    move-object/from16 v24, p4

    invoke-static/range {v19 .. v24}, Lcom/google/android/music/store/MusicContentProvider;->countGroups(Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 1089
    :cond_53
    if-nez v74, :cond_58

    .line 1090
    const-string v4, "metajamId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v126

    .line 1092
    .local v126, "trackMetajamId":Ljava/lang/String;
    invoke-static/range {v126 .. v126}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_55

    const/16 v79, 0x1

    .line 1093
    .local v79, "hasTrackMetajamId":Z
    :goto_1f
    if-eqz v79, :cond_54

    .line 1094
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Nid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v126 .. v126}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1099
    :cond_54
    if-eqz p5, :cond_56

    .line 1100
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "sortOrder not supported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1092
    .end local v79    # "hasTrackMetajamId":Z
    :cond_55
    const/16 v79, 0x0

    goto :goto_1f

    .line 1102
    .restart local v79    # "hasTrackMetajamId":Z
    :cond_56
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v115

    .line 1104
    .local v115, "sortInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-object/from16 v0, v115

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 p5, v0

    .end local p5    # "sortOrder":Ljava/lang/String;
    check-cast p5, Ljava/lang/String;

    .line 1105
    .restart local p5    # "sortOrder":Ljava/lang/String;
    if-nez p5, :cond_57

    .line 1107
    const-string p5, "CanonicalName"

    .line 1145
    :cond_57
    move-object/from16 v0, v115

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_5a

    .line 1147
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SongId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 1148
    move-object/from16 v48, p5

    .line 1151
    const/16 p5, 0x0

    .line 1156
    .end local v79    # "hasTrackMetajamId":Z
    .end local v115    # "sortInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v126    # "trackMetajamId":Ljava/lang/String;
    :cond_58
    :goto_20
    const/16 v83, 0x0

    .line 1157
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    .line 1161
    new-instance v84, Ljava/lang/StringBuilder;

    invoke-direct/range {v84 .. v84}, Ljava/lang/StringBuilder;-><init>()V

    .line 1163
    .restart local v84    # "innerConditions":Ljava/lang/StringBuilder;
    const-string v4, "true"

    const-string v5, "hasServerAudio"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_59

    .line 1164
    const-string v4, "MUSIC.SourceAccount!=0"

    move-object/from16 v0, v84

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1165
    const-string v4, "MUSIC.SourceAccount!=0"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1168
    :cond_59
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v84

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    .line 1169
    invoke-virtual/range {v84 .. v84}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_5b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v84 .. v84}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v85

    .line 1173
    .restart local v85    # "innerWhere":Ljava/lang/String;
    :goto_21
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.SongId IN (SELECT SongId FROM MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v85

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v48, :cond_5c

    const-string v4, ""

    :goto_22
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p5, :cond_5d

    const-string v4, ""

    :goto_23
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v51, :cond_5e

    const-string v4, ""

    :goto_24
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1180
    const/16 v51, 0x0

    .line 1182
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;IZ)V

    goto/16 :goto_3

    .line 1153
    .end local v84    # "innerConditions":Ljava/lang/StringBuilder;
    .end local v85    # "innerWhere":Ljava/lang/String;
    .restart local v79    # "hasTrackMetajamId":Z
    .restart local v115    # "sortInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .restart local v126    # "trackMetajamId":Ljava/lang/String;
    :cond_5a
    const-string v48, "SongId"

    goto/16 :goto_20

    .line 1169
    .end local v79    # "hasTrackMetajamId":Z
    .end local v115    # "sortInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v126    # "trackMetajamId":Ljava/lang/String;
    .restart local v84    # "innerConditions":Ljava/lang/StringBuilder;
    :cond_5b
    const-string v85, ""

    goto :goto_21

    .line 1173
    .restart local v85    # "innerWhere":Ljava/lang/String;
    :cond_5c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " GROUP BY "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_22

    :cond_5d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ORDER BY "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_23

    :cond_5e
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " LIMIT "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v51

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_24

    .line 1188
    .end local v39    # "filter":I
    .end local v84    # "innerConditions":Ljava/lang/StringBuilder;
    .end local v85    # "innerWhere":Ljava/lang/String;
    :sswitch_11
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1189
    .restart local v8    # "id":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_60

    .line 1192
    const-string v94, "Nid"

    .line 1196
    move-object/from16 v0, p2

    move-object/from16 v1, v94

    invoke-static {v0, v1}, Lcom/google/android/music/utils/DbUtils;->injectColumnIntoProjection([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    .line 1198
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v7, p1

    move-object/from16 v9, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-result-object v110

    .line 1200
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-nez v110, :cond_5f

    const/16 v61, 0x0

    goto/16 :goto_1

    .line 1202
    :cond_5f
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v98

    .line 1205
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1206
    const-string v48, "MUSIC.SongId"

    .line 1207
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    .line 1209
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StoreAlbumId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v8}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1211
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto/16 :goto_3

    .line 1215
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    :cond_60
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1216
    if-nez v74, :cond_61

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_63

    .line 1217
    :cond_61
    const/16 p5, 0x0

    .line 1227
    :cond_62
    :goto_25
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    .line 1229
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.AlbumId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1231
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto/16 :goto_3

    .line 1220
    :cond_63
    const-string v48, "MUSIC.SongId"

    .line 1222
    if-nez p5, :cond_62

    .line 1223
    const-string p5, "DiscNumber, TrackNumber, CanonicalName"

    goto :goto_25

    .line 1237
    .end local v8    # "id":Ljava/lang/String;
    :sswitch_12
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 1238
    .local v24, "audioArtistId":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_65

    .line 1239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    move-object/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v21

    move/from16 v22, v6

    move-object/from16 v23, p1

    move-object/from16 v25, p2

    invoke-static/range {v20 .. v25}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-result-object v110

    .line 1241
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-nez v110, :cond_64

    const/16 v61, 0x0

    goto/16 :goto_1

    .line 1243
    :cond_64
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v98

    .line 1244
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getLocalId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    .line 1245
    const-string v4, "MUSIC.SongId"

    move-object/from16 v0, v110

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getExcludeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v72

    .line 1250
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    :cond_65
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1251
    if-nez v74, :cond_66

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_68

    .line 1252
    :cond_66
    const/16 p5, 0x0

    .line 1267
    :cond_67
    :goto_26
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    .line 1268
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AlbumArtistId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ArtistId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1272
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto/16 :goto_3

    .line 1255
    :cond_68
    const-string v48, "MUSIC.SongId"

    .line 1256
    if-eqz p5, :cond_69

    .line 1257
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "sortOrder not supported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1259
    :cond_69
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 p5, v0

    .end local p5    # "sortOrder":Ljava/lang/String;
    check-cast p5, Ljava/lang/String;

    .line 1261
    .restart local p5    # "sortOrder":Ljava/lang/String;
    if-nez p5, :cond_67

    .line 1263
    const-string p5, "MUSIC.CanonicalName"

    goto :goto_26

    .line 1277
    .end local v24    # "audioArtistId":Ljava/lang/String;
    :sswitch_13
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v29

    .line 1278
    .local v29, "strMusicId":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6b

    .line 1281
    const-string v94, "Nid"

    .line 1285
    move-object/from16 v0, p2

    move-object/from16 v1, v94

    invoke-static {v0, v1}, Lcom/google/android/music/utils/DbUtils;->injectColumnIntoProjection([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    .line 1287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    move-object/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v26

    move/from16 v27, v6

    move-object/from16 v28, p1

    move-object/from16 v30, p2

    invoke-static/range {v25 .. v30}, Lcom/google/android/music/store/NautilusContentProviderHelper;->query(Ljava/util/concurrent/ThreadPoolExecutor;Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-result-object v110

    .line 1289
    .restart local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    if-nez v110, :cond_6a

    const/16 v61, 0x0

    goto/16 :goto_1

    .line 1291
    :cond_6a
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getCursor()Landroid/database/Cursor;

    move-result-object v98

    .line 1294
    invoke-virtual/range {v110 .. v110}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;->getLocalId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v29

    .line 1296
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1297
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sGroupedMusicProjectionMap:Ljava/util/HashMap;

    .line 1299
    const-string v48, "MUSIC.SongId"

    .line 1300
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SongId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1303
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SourceAccount<>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1308
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto/16 :goto_3

    .line 1313
    .end local v110    # "result":Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    :cond_6b
    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v96

    .line 1314
    .local v96, "musicId":J
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1315
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    .line 1317
    const-string v4, "vers"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v127

    .line 1318
    .local v127, "version":Ljava/lang/String;
    if-nez v127, :cond_6c

    .line 1319
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v96

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1322
    :cond_6c
    const-string v51, "1"

    .line 1323
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SongId=(SELECT SongId FROM MUSIC as m WHERE m.Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v96

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIMIT 1)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1329
    const-string v4, "media"

    move-object/from16 v0, v127

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6d

    .line 1330
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SourceAccount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1334
    :cond_6d
    const-string v4, "remote"

    move-object/from16 v0, v127

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6e

    .line 1335
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SourceAccount<>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1339
    :cond_6e
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid value for vers parameter: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v127

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1352
    .end local v29    # "strMusicId":Ljava/lang/String;
    .end local v96    # "musicId":J
    .end local v127    # "version":Ljava/lang/String;
    :sswitch_14
    const-string v4, ","

    invoke-static {v4}, Lcom/google/common/base/Splitter;->on(Ljava/lang/String;)Lcom/google/common/base/Splitter;

    move-result-object v118

    .line 1353
    .local v118, "splitter":Lcom/google/common/base/Splitter;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v118

    invoke-virtual {v0, v4}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v82

    .line 1354
    .local v82, "ids":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface/range {v82 .. v82}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_6f

    .line 1355
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Must have at least one id in selection"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1358
    :cond_6f
    const/16 v80, 0x0

    .line 1359
    .local v80, "i":I
    new-instance v69, Ljava/util/LinkedList;

    invoke-direct/range {v69 .. v69}, Ljava/util/LinkedList;-><init>()V

    .line 1360
    .local v69, "cursorList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Landroid/database/Cursor;>;"
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 1362
    .restart local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_4
    sget-object v4, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1363
    const-string p5, "selected.sel_pos"

    .line 1364
    :cond_70
    :goto_27
    invoke-interface/range {v82 .. v82}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_74

    .line 1366
    new-instance v60, Ljava/lang/StringBuffer;

    invoke-direct/range {v60 .. v60}, Ljava/lang/StringBuffer;-><init>()V

    .line 1367
    .local v60, "buffer":Ljava/lang/StringBuffer;
    const-string v4, "("

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1368
    :cond_71
    :goto_28
    invoke-interface/range {v82 .. v82}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_72

    .line 1369
    invoke-interface/range {v82 .. v82}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1370
    .restart local v8    # "id":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 1371
    const-string v4, "select "

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " as sel_id, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1372
    add-int/lit8 v80, v80, 0x1

    move-object/from16 v0, v60

    move/from16 v1, v80

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " as sel_pos "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1374
    move/from16 v0, v80

    rem-int/lit16 v4, v0, 0xc8

    if-nez v4, :cond_73

    .line 1380
    .end local v8    # "id":Ljava/lang/String;
    :cond_72
    const-string v4, ") as selected JOIN "

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " ON ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1382
    const-string v4, "MUSIC.Id"

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "selected.sel_id) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1383
    invoke-virtual/range {v60 .. v60}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1385
    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v38, 0x0

    move-object/from16 v30, v43

    move-object/from16 v31, v10

    move-object/from16 v32, p2

    move-object/from16 v37, p5

    move-object/from16 v39, p6

    invoke-static/range {v30 .. v39}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v61

    .line 1388
    .restart local v61    # "c":Landroid/database/Cursor;
    if-eqz v61, :cond_70

    .line 1392
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1393
    move-object/from16 v0, v69

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto/16 :goto_27

    .line 1401
    .end local v60    # "buffer":Ljava/lang/StringBuffer;
    .end local v61    # "c":Landroid/database/Cursor;
    :catchall_2
    move-exception v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4

    .line 1376
    .restart local v8    # "id":Ljava/lang/String;
    .restart local v60    # "buffer":Ljava/lang/StringBuffer;
    :cond_73
    :try_start_5
    invoke-interface/range {v82 .. v82}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_71

    .line 1377
    const-string v4, " UNION ALL "

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_28

    .line 1397
    .end local v8    # "id":Ljava/lang/String;
    .end local v60    # "buffer":Ljava/lang/StringBuffer;
    :cond_74
    invoke-virtual/range {v69 .. v69}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_75

    .line 1398
    new-instance v61, Lcom/google/android/music/store/CustomMergeCursor;

    const/4 v4, 0x0

    new-array v4, v4, [Landroid/database/Cursor;

    move-object/from16 v0, v69

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/database/Cursor;

    move-object/from16 v0, v61

    invoke-direct {v0, v4}, Lcom/google/android/music/store/CustomMergeCursor;-><init>([Landroid/database/Cursor;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1401
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_1

    :cond_75
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_3

    .line 1407
    .end local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v69    # "cursorList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Landroid/database/Cursor;>;"
    .end local v80    # "i":I
    .end local v82    # "ids":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v118    # "splitter":Lcom/google/common/base/Splitter;
    :sswitch_15
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 1409
    const/4 v4, 0x3

    new-array v0, v4, [J

    move-object/from16 v81, v0

    fill-array-data v81, :array_0

    .line 1413
    .local v81, "ids":[J
    new-instance v61, Lcom/google/android/music/store/AutoPlaylistCursor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    move-object/from16 v2, v81

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/music/store/AutoPlaylistCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;[J)V

    .line 1414
    .restart local v61    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 1419
    .end local v61    # "c":Landroid/database/Cursor;
    .end local v81    # "ids":[J
    :sswitch_16
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 1420
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->uriIdToAutoPlaylistId(J)J

    move-result-wide v100

    .line 1422
    .local v100, "playListId":J
    const/4 v4, 0x1

    new-array v0, v4, [J

    move-object/from16 v81, v0

    const/4 v4, 0x0

    aput-wide v100, v81, v4

    .line 1423
    .restart local v81    # "ids":[J
    new-instance v61, Lcom/google/android/music/store/AutoPlaylistCursor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    move-object/from16 v2, v81

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/music/store/AutoPlaylistCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;[J)V

    .line 1424
    .restart local v61    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 1429
    .end local v61    # "c":Landroid/database/Cursor;
    .end local v81    # "ids":[J
    .end local v100    # "playListId":J
    :sswitch_17
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sAutoPlaylistMembersProjectionMap:Ljava/util/HashMap;

    .line 1430
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->uriIdToAutoPlaylistId(J)J

    move-result-wide v100

    .line 1432
    .restart local v100    # "playListId":J
    const-wide/16 v4, -0x4

    cmp-long v4, v100, v4

    if-nez v4, :cond_7a

    .line 1434
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1435
    const-string v4, "(Rating > 3)"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1437
    if-nez v74, :cond_76

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_77

    .line 1438
    :cond_76
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1439
    const/16 p5, 0x0

    goto/16 :goto_3

    .line 1441
    :cond_77
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1442
    const-string v48, "MUSIC.SongId"

    .line 1443
    if-eqz p5, :cond_78

    .line 1444
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "sortOrder not supported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1446
    :cond_78
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 p5, v0

    .end local p5    # "sortOrder":Ljava/lang/String;
    check-cast p5, Ljava/lang/String;

    .line 1448
    .restart local p5    # "sortOrder":Ljava/lang/String;
    if-nez p5, :cond_79

    .line 1449
    const-string p5, "MAX(MUSIC.RatingTimestampMicrosec) DESC, MUSIC.CanonicalName"

    .line 1453
    :cond_79
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SongId IN (SELECT SongId FROM MUSIC WHERE ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v125

    .line 1456
    .local v125, "thumbsUpSongSelection":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v64

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1457
    move-object/from16 v0, v64

    move-object/from16 v1, v125

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1459
    .end local v125    # "thumbsUpSongSelection":Ljava/lang/String;
    :cond_7a
    const-wide/16 v4, -0x1

    cmp-long v4, v100, v4

    if-nez v4, :cond_80

    .line 1461
    if-nez v74, :cond_7b

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7d

    .line 1462
    :cond_7b
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1463
    const/16 p5, 0x0

    .line 1478
    :cond_7c
    :goto_29
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1480
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SongId IN (SELECT SongId FROM MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_7f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " WHERE ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_2a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ORDER BY "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC.FileDate DESC "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v109

    .line 1486
    .local v109, "recentSongSelection":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v64

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1487
    move-object/from16 v0, v64

    move-object/from16 v1, v109

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1465
    .end local v109    # "recentSongSelection":Ljava/lang/String;
    :cond_7d
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1466
    const-string v48, "MUSIC.SongId"

    .line 1468
    if-eqz p5, :cond_7e

    .line 1469
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "sortOrder not supported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1471
    :cond_7e
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 p5, v0

    .end local p5    # "sortOrder":Ljava/lang/String;
    check-cast p5, Ljava/lang/String;

    .line 1473
    .restart local p5    # "sortOrder":Ljava/lang/String;
    if-nez p5, :cond_7c

    .line 1474
    const-string p5, "MAX(MUSIC.FileDate) DESC "

    goto/16 :goto_29

    .line 1480
    :cond_7f
    const-string v4, ""

    goto :goto_2a

    .line 1489
    :cond_80
    const-wide/16 v4, -0x3

    cmp-long v4, v100, v4

    if-nez v4, :cond_85

    .line 1490
    const-string v4, "storeSongId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v120

    .line 1491
    .local v120, "storeSongId":Ljava/lang/String;
    invoke-static/range {v120 .. v120}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_82

    .line 1492
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1500
    :goto_2b
    const-string v4, "(TrackType IN (2,3,1) AND +Domain=0)"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1502
    if-nez v74, :cond_81

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_83

    .line 1503
    :cond_81
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1504
    const/16 p5, 0x0

    goto/16 :goto_3

    .line 1494
    :cond_82
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StoreId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v120 .. v120}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2b

    .line 1506
    :cond_83
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1507
    const-string v48, "MUSIC.SongId"

    .line 1509
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 p5, v0

    .end local p5    # "sortOrder":Ljava/lang/String;
    check-cast p5, Ljava/lang/String;

    .line 1511
    .restart local p5    # "sortOrder":Ljava/lang/String;
    if-nez p5, :cond_84

    .line 1512
    const-string p5, "MAX(MUSIC.FileDate) DESC "

    .line 1517
    :cond_84
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SongId IN (SELECT SongId FROM MUSIC WHERE ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v114

    .line 1520
    .local v114, "songSelection":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v64

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1521
    move-object/from16 v0, v64

    move-object/from16 v1, v114

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1523
    .end local v114    # "songSelection":Ljava/lang/String;
    .end local v120    # "storeSongId":Ljava/lang/String;
    :cond_85
    const-wide/16 v4, -0x2

    cmp-long v4, v100, v4

    if-nez v4, :cond_89

    .line 1524
    const-string v4, "storeSongId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v120

    .line 1525
    .restart local v120    # "storeSongId":Ljava/lang/String;
    invoke-static/range {v120 .. v120}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_87

    .line 1526
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1532
    :goto_2c
    if-nez v74, :cond_86

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_88

    .line 1533
    :cond_86
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1534
    const/16 p5, 0x0

    goto/16 :goto_3

    .line 1528
    :cond_87
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StoreId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v120 .. v120}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2c

    .line 1536
    :cond_88
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1537
    const-string v48, "MUSIC.SongId"

    .line 1539
    const-string v4, "order"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/music/store/MusicContentProvider;->getSortOrderFromQueryParam(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v4

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 p5, v0

    .end local p5    # "sortOrder":Ljava/lang/String;
    check-cast p5, Ljava/lang/String;

    .line 1541
    .restart local p5    # "sortOrder":Ljava/lang/String;
    if-nez p5, :cond_5

    .line 1542
    const-string p5, "MAX(MUSIC.FileDate) DESC "

    goto/16 :goto_3

    .line 1546
    .end local v120    # "storeSongId":Ljava/lang/String;
    :cond_89
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to handle auto-playlist for uri: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1553
    .end local v100    # "playListId":J
    :sswitch_18
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 1555
    if-nez v74, :cond_8a

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8b

    .line 1556
    :cond_8a
    const-string v4, "LISTS"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1557
    const/16 p5, 0x0

    .line 1564
    :goto_2d
    const-string v4, "name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v95

    .line 1565
    .local v95, "name":Ljava/lang/String;
    const-string v4, "sourceId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v117

    .line 1567
    .local v117, "sourceId":Ljava/lang/String;
    const-string v4, "shareToken"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v112

    .line 1570
    .local v112, "shareToken":Ljava/lang/String;
    if-eqz v95, :cond_8c

    invoke-virtual/range {v95 .. v95}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_8c

    .line 1571
    const-string p3, "LISTS.Name=?"

    .line 1572
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object v95, p4, v4

    .line 1573
    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    const/16 v83, 0x0

    .line 1588
    :goto_2e
    const-string v4, "excludeFollowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v73

    .line 1590
    .local v73, "excludeFollowed":Ljava/lang/String;
    invoke-static/range {v73 .. v73}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8f

    invoke-static/range {v73 .. v73}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_8f

    .line 1591
    const-string v4, "ListType IN (0, 1)"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1559
    .end local v73    # "excludeFollowed":Ljava/lang/String;
    .end local v95    # "name":Ljava/lang/String;
    .end local v112    # "shareToken":Ljava/lang/String;
    .end local v117    # "sourceId":Ljava/lang/String;
    :cond_8b
    const-string v4, "LISTS LEFT  JOIN KEEPON ON (KEEPON.ListId = LISTS.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1561
    const-string p5, "LISTS.NameSort COLLATE UNICODE ASC"

    goto :goto_2d

    .line 1574
    .restart local v95    # "name":Ljava/lang/String;
    .restart local v112    # "shareToken":Ljava/lang/String;
    .restart local v117    # "sourceId":Ljava/lang/String;
    :cond_8c
    invoke-static/range {v117 .. v117}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8d

    .line 1575
    const-string p3, "SourceId=?"

    .line 1576
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object v117, p4, v4

    .line 1577
    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    const/16 v83, 0x0

    goto :goto_2e

    .line 1578
    :cond_8d
    invoke-static/range {v112 .. v112}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8e

    .line 1579
    const-string p3, "ShareToken=?"

    .line 1580
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    aput-object v112, p4, v4

    .line 1581
    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    const/16 v83, 0x0

    goto :goto_2e

    .line 1583
    :cond_8e
    move-object/from16 v0, v64

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;)V

    goto :goto_2e

    .line 1593
    .restart local v73    # "excludeFollowed":Ljava/lang/String;
    :cond_8f
    const-string v4, "ListType IN (0, 1, 71)"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1599
    .end local v73    # "excludeFollowed":Ljava/lang/String;
    .end local v95    # "name":Ljava/lang/String;
    .end local v112    # "shareToken":Ljava/lang/String;
    .end local v117    # "sourceId":Ljava/lang/String;
    :sswitch_19
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 1601
    const-string v59, "RECENT JOIN LISTS ON (RecentListId=LISTS.Id) "

    .line 1602
    .local v59, "baseTable":Ljava/lang/String;
    if-nez v74, :cond_90

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_91

    .line 1603
    :cond_90
    const-string v4, "RECENT JOIN LISTS ON (RecentListId=LISTS.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1604
    const/16 p5, 0x0

    .line 1612
    :goto_2f
    const-string v4, "excludeFollowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v73

    .line 1614
    .restart local v73    # "excludeFollowed":Ljava/lang/String;
    invoke-static/range {v73 .. v73}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_92

    invoke-static/range {v73 .. v73}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_92

    .line 1615
    const-string v4, "ListType IN (0, 1)"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1620
    :goto_30
    move-object/from16 v0, v64

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;)V

    goto/16 :goto_3

    .line 1606
    .end local v73    # "excludeFollowed":Ljava/lang/String;
    :cond_91
    const-string v4, "RECENT JOIN LISTS ON (RecentListId=LISTS.Id)  LEFT  JOIN KEEPON ON (KEEPON.ListId = LISTS.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1607
    const-string p5, "Priority DESC, ItemDate DESC"

    goto :goto_2f

    .line 1617
    .restart local v73    # "excludeFollowed":Ljava/lang/String;
    :cond_92
    const-string v4, "ListType IN (0, 1, 71)"

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_30

    .line 1627
    .end local v59    # "baseTable":Ljava/lang/String;
    .end local v73    # "excludeFollowed":Ljava/lang/String;
    :sswitch_1a
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 1628
    if-nez v74, :cond_93

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_94

    .line 1629
    :cond_93
    const-string v4, "LISTS JOIN SUGGESTED_SEEDS ON (LISTS.Id=SUGGESTED_SEEDS.SeedListId) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1630
    const/16 p5, 0x0

    .line 1638
    :goto_31
    move-object/from16 v0, v64

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;)V

    goto/16 :goto_3

    .line 1632
    :cond_94
    const-string v4, "LISTS JOIN SUGGESTED_SEEDS ON (LISTS.Id=SUGGESTED_SEEDS.SeedListId)  LEFT  JOIN KEEPON ON (KEEPON.ListId = LISTS.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1634
    const-string p5, "SeedOrder"

    goto :goto_31

    .line 1643
    :sswitch_1b
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    .line 1645
    const-string v4, "RADIO_STATIONS LEFT  JOIN KEEPON ON (KEEPON.RadioStationId = RADIO_STATIONS.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1647
    if-nez v74, :cond_95

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_96

    .line 1648
    :cond_95
    const/16 p5, 0x0

    .line 1652
    :goto_32
    move-object/from16 v0, v64

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendRadioFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;)V

    .line 1653
    invoke-static/range {v64 .. v64}, Lcom/google/android/music/store/MusicContentProvider;->appendIgnoreRecommendedRadioCondition(Ljava/lang/StringBuilder;)V

    goto/16 :goto_3

    .line 1650
    :cond_96
    const-string p5, "RADIO_STATIONS.RecentTimestamp DESC "

    goto :goto_32

    .line 1658
    :sswitch_1c
    const-string v4, "RADIO_STATIONS LEFT  JOIN KEEPON ON (KEEPON.RadioStationId = RADIO_STATIONS.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1660
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sRadioProjectionMap:Ljava/util/HashMap;

    .line 1662
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1663
    .restart local v8    # "id":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1668
    .end local v8    # "id":Ljava/lang/String;
    :sswitch_1d
    const-string v4, "isInCloudQueueMode"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v62

    .line 1670
    .local v62, "cloudQueueQueryParam":Ljava/lang/String;
    const-string v4, "true"

    move-object/from16 v0, v62

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v87

    .line 1671
    .local v87, "isInCloudQueueMode":Z
    if-eqz v87, :cond_9c

    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    .line 1673
    :goto_33
    if-nez v74, :cond_97

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9d

    .line 1674
    :cond_97
    const/16 p5, 0x0

    .line 1680
    :goto_34
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueTableName(Z)Ljava/lang/String;

    move-result-object v124

    .line 1681
    .local v124, "tableName":Ljava/lang/String;
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueItemsContainerId(Z)Ljava/lang/String;

    move-result-object v108

    .line 1682
    .local v108, "queueItemsContainerId":Ljava/lang/String;
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueContainerTableName(Z)Ljava/lang/String;

    move-result-object v105

    .line 1683
    .local v105, "queueContainers":Ljava/lang/String;
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueContainersId(Z)Ljava/lang/String;

    move-result-object v106

    .line 1685
    .local v106, "queueContainersId":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v124

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ON "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MusicId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC.Id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ON "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v108

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v106

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1693
    const-string v4, "containerType"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v67

    .line 1696
    .local v67, "containerType":Ljava/lang/String;
    invoke-static/range {v67 .. v67}, Lcom/google/android/music/store/MusicContentProvider;->isValidInteger(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_98

    .line 1697
    const/16 v67, 0x0

    .line 1700
    :cond_98
    const-string v4, "containerId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 1703
    .local v66, "containerId":Ljava/lang/String;
    invoke-static/range {v66 .. v66}, Lcom/google/android/music/store/MusicContentProvider;->isValidInteger(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_99

    .line 1704
    const/16 v66, 0x0

    .line 1707
    :cond_99
    const-string v4, "containerExtId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v65

    .line 1710
    .local v65, "containerExtId":Ljava/lang/String;
    if-eqz v67, :cond_9a

    .line 1711
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueContainersType(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v67

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1714
    :cond_9a
    if-eqz v66, :cond_9f

    .line 1715
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v106

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1721
    :cond_9b
    :goto_35
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    goto/16 :goto_3

    .line 1671
    .end local v65    # "containerExtId":Ljava/lang/String;
    .end local v66    # "containerId":Ljava/lang/String;
    .end local v67    # "containerType":Ljava/lang/String;
    .end local v105    # "queueContainers":Ljava/lang/String;
    .end local v106    # "queueContainersId":Ljava/lang/String;
    .end local v108    # "queueItemsContainerId":Ljava/lang/String;
    .end local v124    # "tableName":Ljava/lang/String;
    :cond_9c
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    goto/16 :goto_33

    .line 1676
    :cond_9d
    if-eqz v87, :cond_9e

    const-string p5, "CLOUD_QUEUE_ITEMS.ItemOrder"

    :goto_36
    goto/16 :goto_34

    :cond_9e
    const-string p5, "QUEUE_ITEMS.ItemOrder"

    goto :goto_36

    .line 1716
    .restart local v65    # "containerExtId":Ljava/lang/String;
    .restart local v66    # "containerId":Ljava/lang/String;
    .restart local v67    # "containerType":Ljava/lang/String;
    .restart local v105    # "queueContainers":Ljava/lang/String;
    .restart local v106    # "queueContainersId":Ljava/lang/String;
    .restart local v108    # "queueItemsContainerId":Ljava/lang/String;
    .restart local v124    # "tableName":Ljava/lang/String;
    :cond_9f
    if-eqz v65, :cond_9b

    .line 1717
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueContainersExtId(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v65 .. v65}, Lcom/google/android/music/utils/DbUtils;->quoteStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_35

    .line 1724
    .end local v62    # "cloudQueueQueryParam":Ljava/lang/String;
    .end local v65    # "containerExtId":Ljava/lang/String;
    .end local v66    # "containerId":Ljava/lang/String;
    .end local v67    # "containerType":Ljava/lang/String;
    .end local v87    # "isInCloudQueueMode":Z
    .end local v105    # "queueContainers":Ljava/lang/String;
    .end local v106    # "queueContainersId":Ljava/lang/String;
    .end local v108    # "queueItemsContainerId":Ljava/lang/String;
    .end local v124    # "tableName":Ljava/lang/String;
    :sswitch_1e
    const-string v4, "isInCloudQueueMode"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    .line 1726
    .local v104, "queryParam":Ljava/lang/String;
    const-string v4, "true"

    move-object/from16 v0, v104

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v87

    .line 1727
    .restart local v87    # "isInCloudQueueMode":Z
    if-eqz v87, :cond_a0

    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sCloudQueueProjection:Ljava/util/HashMap;

    .line 1729
    :goto_37
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueTableName(Z)Ljava/lang/String;

    move-result-object v124

    .line 1730
    .restart local v124    # "tableName":Ljava/lang/String;
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueContainerTableName(Z)Ljava/lang/String;

    move-result-object v107

    .line 1731
    .local v107, "queueContainersTableName":Ljava/lang/String;
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueItemsContainerId(Z)Ljava/lang/String;

    move-result-object v108

    .line 1732
    .restart local v108    # "queueItemsContainerId":Ljava/lang/String;
    invoke-static/range {v87 .. v87}, Lcom/google/android/music/store/Store;->getQueueContainersId(Z)Ljava/lang/String;

    move-result-object v106

    .line 1734
    .restart local v106    # "queueContainersId":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v124

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ON "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MusicId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC.Id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v107

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ON "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v108

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v106

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1742
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1743
    .restart local v8    # "id":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/store/MusicContentProvider;->isValidInteger(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a1

    .line 1744
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Invalid id"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1727
    .end local v8    # "id":Ljava/lang/String;
    .end local v106    # "queueContainersId":Ljava/lang/String;
    .end local v107    # "queueContainersTableName":Ljava/lang/String;
    .end local v108    # "queueItemsContainerId":Ljava/lang/String;
    .end local v124    # "tableName":Ljava/lang/String;
    :cond_a0
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sQueueProjection:Ljava/util/HashMap;

    goto/16 :goto_37

    .line 1746
    .restart local v8    # "id":Ljava/lang/String;
    .restart local v106    # "queueContainersId":Ljava/lang/String;
    .restart local v107    # "queueContainersTableName":Ljava/lang/String;
    .restart local v108    # "queueItemsContainerId":Ljava/lang/String;
    .restart local v124    # "tableName":Ljava/lang/String;
    :cond_a1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1750
    .end local v8    # "id":Ljava/lang/String;
    .end local v87    # "isInCloudQueueMode":Z
    .end local v104    # "queryParam":Ljava/lang/String;
    .end local v106    # "queueContainersId":Ljava/lang/String;
    .end local v107    # "queueContainersTableName":Ljava/lang/String;
    .end local v108    # "queueItemsContainerId":Ljava/lang/String;
    .end local v124    # "tableName":Ljava/lang/String;
    :sswitch_1f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v4, v0, v6, v1}, Lcom/google/android/music/store/SharedContentProviderHelper;->query(Landroid/content/Context;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    .line 1751
    .restart local v61    # "c":Landroid/database/Cursor;
    if-eqz v61, :cond_3

    .line 1752
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1753
    invoke-interface/range {v61 .. v61}, Landroid/database/Cursor;->getCount()I

    goto/16 :goto_1

    .line 1758
    .end local v61    # "c":Landroid/database/Cursor;
    :sswitch_20
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v4, v0, v6, v1}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->query(Landroid/content/Context;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    .line 1759
    .restart local v61    # "c":Landroid/database/Cursor;
    if-eqz v61, :cond_3

    .line 1760
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1761
    invoke-interface/range {v61 .. v61}, Landroid/database/Cursor;->getCount()I

    goto/16 :goto_1

    .line 1766
    .end local v61    # "c":Landroid/database/Cursor;
    :sswitch_21
    const-string v4, "LISTS LEFT  JOIN KEEPON ON (KEEPON.ListId = LISTS.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1767
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LISTS.Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1769
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistsProjectionMap:Ljava/util/HashMap;

    .line 1771
    const/16 p5, 0x0

    .line 1772
    goto/16 :goto_3

    .line 1775
    :sswitch_22
    const-string v4, "LISTITEMS JOIN LISTS ON (LISTS.Id = LISTITEMS.ListId)  JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1777
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    .line 1778
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v100

    .line 1779
    .restart local v100    # "playListId":J
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1780
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LISTS.Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v100

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1782
    if-nez v74, :cond_a2

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a3

    .line 1783
    :cond_a2
    const/16 p5, 0x0

    goto/16 :goto_3

    .line 1785
    :cond_a3
    const-string p5, "ServerOrder, ClientPosition"

    .line 1786
    move-object/from16 v0, v19

    move-wide/from16 v1, v100

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->getShareTokenIfSharedPlaylist(J)Ljava/lang/String;

    move-result-object v112

    .line 1787
    .restart local v112    # "shareToken":Ljava/lang/String;
    if-eqz v112, :cond_5

    .line 1788
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v53

    .line 1789
    .local v53, "account":Landroid/accounts/Account;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, v53

    move-wide/from16 v1, v100

    move-object/from16 v3, v112

    invoke-static {v4, v0, v1, v2, v3}, Lcom/google/android/music/store/SharedContentProviderHelper;->updateFollowedSharedPlaylist(Landroid/content/Context;Landroid/accounts/Account;JLjava/lang/String;)V

    goto/16 :goto_3

    .line 1796
    .end local v53    # "account":Landroid/accounts/Account;
    .end local v100    # "playListId":J
    .end local v112    # "shareToken":Ljava/lang/String;
    :sswitch_23
    const-string v4, "LISTITEMS JOIN LISTS ON (LISTS.Id = LISTITEMS.ListId)  JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1798
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sPlaylistMembersProjectionMap:Ljava/util/HashMap;

    .line 1800
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v88

    check-cast v88, Ljava/lang/String;

    .line 1801
    .local v88, "listId":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v89

    check-cast v89, Ljava/lang/String;

    .line 1802
    .local v89, "memberId":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LISTS.Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v88

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "LISTITEMS.Id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v89

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1809
    .end local v88    # "listId":Ljava/lang/String;
    .end local v89    # "memberId":Ljava/lang/String;
    :sswitch_24
    if-nez p2, :cond_a4

    .line 1810
    sget-object p2, Lcom/google/android/music/store/MusicContentProvider;->DEFAULT_SEARCH_SUGGESTIONS_PROJECTION:[Ljava/lang/String;

    .line 1812
    :cond_a4
    const/16 v41, 0x1

    .line 1815
    :sswitch_25
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-static {v4, v0, v1, v2}, Lcom/google/android/music/store/MainstageContentProviderHelper;->getRecentContent(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 1818
    :sswitch_26
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-static {v4, v0, v1, v2}, Lcom/google/android/music/store/MainstageContentProviderHelper;->getRecommendedContent(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 1821
    :sswitch_27
    const-string v4, "KEEPON"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1822
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    .line 1823
    goto/16 :goto_3

    .line 1826
    :sswitch_28
    const-string v4, "KEEPON"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1827
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sKeepOnProjectionMap:Ljava/util/HashMap;

    .line 1828
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KEEPON.KeepOnId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1832
    :sswitch_29
    if-eqz p3, :cond_a5

    .line 1833
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " Does not support selection"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1836
    :cond_a5
    if-nez p2, :cond_a6

    .line 1837
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " Does not support null projection in"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1841
    :cond_a6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move-object/from16 v2, p6

    invoke-static {v4, v0, v1, v2}, Lcom/google/android/music/store/DownloadQueueContentProviderHelper;->getDownloadQueueContent(Landroid/content/Context;Lcom/google/android/music/store/Store;[Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 1844
    :sswitch_2a
    if-nez p2, :cond_a7

    .line 1845
    sget-object p2, Lcom/google/android/music/store/MusicContentProvider;->DEFAULT_SEARCH_SUGGESTIONS_PROJECTION:[Ljava/lang/String;

    .line 1847
    :cond_a7
    const/16 v41, 0x1

    .line 1850
    :sswitch_2b
    if-nez p2, :cond_a8

    .line 1851
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "null projection not allowed"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1855
    :cond_a8
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v36

    .line 1857
    .local v36, "searchValue":Ljava/lang/String;
    const/16 v39, 0x3

    .line 1858
    .restart local v39    # "filter":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->isDownloadedOnlyMode()Z

    move-result v86

    .line 1859
    .local v86, "isDownloadedOnly":Z
    if-eqz v86, :cond_a9

    .line 1860
    const/16 v39, 0x5

    .line 1862
    :cond_a9
    move-object/from16 v0, p0

    move/from16 v1, v41

    move/from16 v2, v86

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->shouldDoNautilusSearch(ZZ)Z

    move-result v42

    .line 1865
    .local v42, "doNautilusSearch":Z
    new-instance v30, Lcom/google/android/music/store/SearchHandler;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    move-object/from16 v33, v0

    move-object/from16 v32, v19

    move-object/from16 v34, v43

    move-object/from16 v35, p1

    move-object/from16 v37, v51

    move-object/from16 v38, p2

    move-object/from16 v40, p6

    invoke-direct/range {v30 .. v42}, Lcom/google/android/music/store/SearchHandler;-><init>(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/concurrent/ThreadPoolExecutor;Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;ZZ)V

    .line 1869
    .local v30, "sh":Lcom/google/android/music/store/SearchHandler;
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/store/SearchHandler;->performQuery()Landroid/database/Cursor;

    move-result-object v68

    .local v68, "cursor":Landroid/database/Cursor;
    move-object/from16 v61, v68

    .line 1871
    goto/16 :goto_1

    .line 1874
    .end local v30    # "sh":Lcom/google/android/music/store/SearchHandler;
    .end local v36    # "searchValue":Ljava/lang/String;
    .end local v39    # "filter":I
    .end local v42    # "doNautilusSearch":Z
    .end local v68    # "cursor":Landroid/database/Cursor;
    .end local v86    # "isDownloadedOnly":Z
    :sswitch_2c
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->isDownloadedOnlyMode()Z

    move-result v86

    .line 1875
    .restart local v86    # "isDownloadedOnly":Z
    move-object/from16 v0, p0

    move/from16 v1, v41

    move/from16 v2, v86

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->shouldDoNautilusSearch(ZZ)Z

    move-result v42

    .line 1877
    .restart local v42    # "doNautilusSearch":Z
    if-nez v42, :cond_aa

    .line 1879
    const/16 v61, 0x0

    goto/16 :goto_1

    .line 1882
    :cond_aa
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v36

    .line 1883
    .restart local v36    # "searchValue":Ljava/lang/String;
    const/16 v39, 0x3

    .line 1884
    .restart local v39    # "filter":I
    if-eqz v86, :cond_ab

    .line 1885
    const/16 v39, 0x5

    .line 1887
    :cond_ab
    new-instance v30, Lcom/google/android/music/store/SearchHandler;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    move-object/from16 v33, v0

    move-object/from16 v32, v19

    move-object/from16 v34, v43

    move-object/from16 v35, p1

    move-object/from16 v37, v51

    move-object/from16 v38, p2

    move-object/from16 v40, p6

    invoke-direct/range {v30 .. v42}, Lcom/google/android/music/store/SearchHandler;-><init>(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/util/concurrent/ThreadPoolExecutor;Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;ZZ)V

    .line 1891
    .restart local v30    # "sh":Lcom/google/android/music/store/SearchHandler;
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/store/SearchHandler;->performSuggestedQueryLookup()Landroid/database/Cursor;

    move-result-object v68

    .restart local v68    # "cursor":Landroid/database/Cursor;
    move-object/from16 v61, v68

    .line 1892
    goto/16 :goto_1

    .line 1895
    .end local v30    # "sh":Lcom/google/android/music/store/SearchHandler;
    .end local v36    # "searchValue":Ljava/lang/String;
    .end local v39    # "filter":I
    .end local v42    # "doNautilusSearch":Z
    .end local v68    # "cursor":Landroid/database/Cursor;
    .end local v86    # "isDownloadedOnly":Z
    :sswitch_2d
    const/16 v61, 0x0

    goto/16 :goto_1

    .line 1897
    :sswitch_2e
    const/16 v61, 0x0

    goto/16 :goto_1

    .line 1899
    :sswitch_2f
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v92

    .line 1901
    .local v92, "mediaStoreId":J
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v4

    const/4 v5, 0x1

    move-wide/from16 v0, v92

    invoke-virtual {v4, v0, v1, v5}, Lcom/google/android/music/store/Store;->getMusicIdForSystemMediaStoreId(JZ)J

    move-result-wide v90

    .line 1903
    .local v90, "mappedMusicId":J
    const-string v4, "MUSIC"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1904
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sMusicProjectionMap:Ljava/util/HashMap;

    .line 1905
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MUSIC.Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v90

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_3

    .line 1906
    .end local v90    # "mappedMusicId":J
    :catch_0
    move-exception v71

    .line 1908
    .local v71, "e":Ljava/io/FileNotFoundException;
    new-instance v61, Landroid/database/MatrixCursor;

    const/4 v4, 0x0

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1914
    .end local v71    # "e":Ljava/io/FileNotFoundException;
    .end local v92    # "mediaStoreId":J
    :sswitch_30
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->checkHasGetAccountsPermission()V

    .line 1915
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v63, v0

    const/4 v4, 0x0

    const-string v5, "name"

    aput-object v5, v63, v4

    const/4 v4, 0x1

    const-string v5, "type"

    aput-object v5, v63, v4

    .line 1919
    .local v63, "columns":[Ljava/lang/String;
    if-eqz p2, :cond_ac

    move-object/from16 v0, v63

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_ac

    .line 1920
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported projection for account query"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1922
    :cond_ac
    new-instance v68, Landroid/database/MatrixCursor;

    const/4 v4, 0x1

    move-object/from16 v0, v68

    move-object/from16 v1, v63

    invoke-direct {v0, v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 1923
    .local v68, "cursor":Landroid/database/MatrixCursor;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v122

    .line 1924
    .local v122, "streamingAccount":Landroid/accounts/Account;
    if-eqz v122, :cond_ad

    .line 1925
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v122

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, v122

    iget-object v7, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v7, v4, v5

    move-object/from16 v0, v68

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_ad
    move-object/from16 v61, v68

    .line 1927
    goto/16 :goto_1

    .line 1931
    .end local v63    # "columns":[Ljava/lang/String;
    .end local v68    # "cursor":Landroid/database/MatrixCursor;
    .end local v122    # "streamingAccount":Landroid/accounts/Account;
    :sswitch_31
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v63, v0

    const/4 v4, 0x0

    const-string v5, "isNautilusEnabled"

    aput-object v5, v63, v4

    .line 1934
    .restart local v63    # "columns":[Ljava/lang/String;
    new-instance v68, Landroid/database/MatrixCursor;

    const/4 v4, 0x1

    move-object/from16 v0, v68

    move-object/from16 v1, v63

    invoke-direct {v0, v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 1935
    .restart local v68    # "cursor":Landroid/database/MatrixCursor;
    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->isNautilusUser()Z

    move-result v4

    if-eqz v4, :cond_ae

    const/4 v4, 0x1

    :goto_38
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v7

    move-object/from16 v0, v68

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object/from16 v61, v68

    .line 1936
    goto/16 :goto_1

    .line 1935
    :cond_ae
    const/4 v4, 0x0

    goto :goto_38

    .line 1940
    .end local v63    # "columns":[Ljava/lang/String;
    .end local v68    # "cursor":Landroid/database/MatrixCursor;
    :sswitch_32
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v63, v0

    const/4 v4, 0x0

    const-string v5, "isAcceptedUser"

    aput-object v5, v63, v4

    .line 1943
    .restart local v63    # "columns":[Ljava/lang/String;
    new-instance v68, Landroid/database/MatrixCursor;

    const/4 v4, 0x1

    move-object/from16 v0, v68

    move-object/from16 v1, v63

    invoke-direct {v0, v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 1944
    .restart local v68    # "cursor":Landroid/database/MatrixCursor;
    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->isAcceptedUser()Z

    move-result v4

    if-eqz v4, :cond_af

    const/4 v4, 0x1

    :goto_39
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v7

    move-object/from16 v0, v68

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object/from16 v61, v68

    .line 1945
    goto/16 :goto_1

    .line 1944
    :cond_af
    const/4 v4, 0x0

    goto :goto_39

    .line 1949
    .end local v63    # "columns":[Ljava/lang/String;
    .end local v68    # "cursor":Landroid/database/MatrixCursor;
    :sswitch_33
    const-string v4, "SUGGESTED_SEEDS JOIN MUSIC ON (SeedSourceAccount=MUSIC.SourceAccount AND SeedTrackSourceId=MUSIC.SourceId) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1951
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    .line 1952
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendMusicFilteringCondition(Ljava/lang/StringBuilder;Landroid/net/Uri;Z)V

    .line 1954
    if-nez v74, :cond_b0

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b1

    .line 1955
    :cond_b0
    const/16 p5, 0x0

    goto/16 :goto_3

    .line 1957
    :cond_b1
    const-string p5, "SeedOrder"

    .line 1959
    goto/16 :goto_3

    .line 1962
    :sswitch_34
    const-string v4, "SUGGESTED_SEEDS JOIN MUSIC ON (SeedSourceAccount=MUSIC.SourceAccount AND SeedTrackSourceId=MUSIC.SourceId) "

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1964
    sget-object v102, Lcom/google/android/music/store/MusicContentProvider;->sSuggestedSeedsMap:Ljava/util/HashMap;

    .line 1966
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1967
    .restart local v8    # "id":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SUGGESTED_SEEDS.Id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1980
    .end local v8    # "id":Ljava/lang/String;
    :sswitch_35
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v4, v0, v6, v1}, Lcom/google/android/music/store/ExploreContentProviderHelper;->query(Landroid/content/Context;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    .line 1990
    .restart local v61    # "c":Landroid/database/Cursor;
    goto/16 :goto_1

    .line 1994
    .end local v61    # "c":Landroid/database/Cursor;
    :sswitch_36
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_b2

    .line 1995
    const-string v4, "MusicContentProvider"

    const-string v5, "TOP_SITUATION_HEADER"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1997
    :cond_b2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/store/MusicContentProvider;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    invoke-static {v4, v5}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getInstance(Landroid/content/Context;Lcom/google/android/music/store/SituationsCache;)Lcom/google/android/music/store/SituationsContentProviderHelper;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getTopSituationHeader([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 2002
    :sswitch_37
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_b3

    .line 2003
    const-string v4, "MusicContentProvider"

    const-string v5, "TOP_SITUATIONS"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2005
    :cond_b3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/store/MusicContentProvider;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    invoke-static {v4, v5}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getInstance(Landroid/content/Context;Lcom/google/android/music/store/SituationsCache;)Lcom/google/android/music/store/SituationsContentProviderHelper;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getTopLevelSituations([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 2010
    :sswitch_38
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v113

    .line 2011
    .local v113, "situationId":Ljava/lang/String;
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_b4

    .line 2012
    const-string v4, "MusicContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SUB_SITUATIONS: situationId="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v113

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2014
    :cond_b4
    if-nez v113, :cond_b5

    .line 2015
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Missing situation id"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2017
    :cond_b5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/store/MusicContentProvider;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    invoke-static {v4, v5}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getInstance(Landroid/content/Context;Lcom/google/android/music/store/SituationsCache;)Lcom/google/android/music/store/SituationsContentProviderHelper;

    move-result-object v4

    move-object/from16 v0, v113

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getSubSituations(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 2022
    .end local v113    # "situationId":Ljava/lang/String;
    :sswitch_39
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v113

    .line 2023
    .restart local v113    # "situationId":Ljava/lang/String;
    if-nez v113, :cond_b6

    .line 2024
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Missing situation id"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2026
    :cond_b6
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_b7

    .line 2027
    const-string v4, "MusicContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SITUATIONS_ID_RADIO_STATIONS: situationId="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v113

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2029
    :cond_b7
    if-nez v113, :cond_b8

    .line 2030
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Missing situation id"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2032
    :cond_b8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/store/MusicContentProvider;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    invoke-static {v4, v5}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getInstance(Landroid/content/Context;Lcom/google/android/music/store/SituationsCache;)Lcom/google/android/music/store/SituationsContentProviderHelper;

    move-result-object v4

    move-object/from16 v0, v113

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Lcom/google/android/music/store/SituationsContentProviderHelper;->getRadioStations(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    goto/16 :goto_1

    .line 2060
    .end local v113    # "situationId":Ljava/lang/String;
    :cond_b9
    const/16 p3, 0x0

    .line 2061
    const/16 p4, 0x0

    .line 2063
    :cond_ba
    sget-boolean v4, Lcom/google/android/music/store/MusicContentProvider;->LOGV:Z

    if-eqz v4, :cond_bb

    .line 2065
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/utils/DebugUtils;->arrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v99

    .line 2066
    .local v99, "projText":Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Lcom/google/android/music/utils/DebugUtils;->arrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v111

    .line 2068
    .local v111, "selargText":Ljava/lang/String;
    const-string v4, "MusicContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Running Query: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", table: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v43 .. v43}, Landroid/database/sqlite/SQLiteQueryBuilder;->getTables()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", projection: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v99

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", selection: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", select args: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v111

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", sort order: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2072
    .end local v99    # "projText":Ljava/lang/String;
    .end local v111    # "selargText":Ljava/lang/String;
    :cond_bb
    move-object/from16 v0, v43

    move/from16 v1, v70

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 2074
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 2076
    .restart local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v49, 0x0

    move-object/from16 v44, v10

    move-object/from16 v45, p2

    move-object/from16 v46, p3

    move-object/from16 v47, p4

    move-object/from16 v50, p5

    move-object/from16 v52, p6

    :try_start_7
    invoke-static/range {v43 .. v52}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v61

    .line 2080
    .restart local v61    # "c":Landroid/database/Cursor;
    if-eqz v98, :cond_bc

    .line 2081
    if-eqz v94, :cond_be

    .line 2082
    move-object/from16 v0, v98

    move-object/from16 v1, v61

    move-object/from16 v2, v94

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->merge(Landroid/database/Cursor;Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v61

    .line 2089
    :cond_bc
    :goto_3a
    if-eqz v61, :cond_bd

    .line 2090
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 2091
    invoke-interface/range {v61 .. v61}, Landroid/database/Cursor;->getCount()I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 2095
    :cond_bd
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_1

    .line 2084
    :cond_be
    const/4 v4, 0x2

    :try_start_8
    new-array v4, v4, [Landroid/database/Cursor;

    const/4 v5, 0x0

    aput-object v98, v4, v5

    const/4 v5, 0x1

    aput-object v61, v4, v5

    invoke-static {v4}, Lcom/google/android/music/store/NautilusContentProviderHelper;->merge([Landroid/database/Cursor;)Landroid/database/Cursor;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-result-object v61

    goto :goto_3a

    .line 2095
    .end local v61    # "c":Landroid/database/Cursor;
    :catchall_3
    move-exception v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4

    .line 525
    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_10
        0x12d -> :sswitch_13
        0x12e -> :sswitch_11
        0x12f -> :sswitch_12
        0x132 -> :sswitch_14
        0x190 -> :sswitch_1
        0x191 -> :sswitch_2
        0x192 -> :sswitch_3
        0x193 -> :sswitch_4
        0x1f4 -> :sswitch_5
        0x1f5 -> :sswitch_6
        0x1f6 -> :sswitch_7
        0x1f7 -> :sswitch_8
        0x1f8 -> :sswitch_9
        0x1f9 -> :sswitch_a
        0x258 -> :sswitch_18
        0x259 -> :sswitch_21
        0x25a -> :sswitch_22
        0x25b -> :sswitch_23
        0x25c -> :sswitch_1a
        0x25d -> :sswitch_19
        0x26c -> :sswitch_15
        0x26d -> :sswitch_16
        0x26e -> :sswitch_17
        0x2bc -> :sswitch_b
        0x2bd -> :sswitch_e
        0x2be -> :sswitch_f
        0x2bf -> :sswitch_d
        0x2c0 -> :sswitch_c
        0x320 -> :sswitch_0
        0x384 -> :sswitch_25
        0x3b6 -> :sswitch_27
        0x3b7 -> :sswitch_28
        0x3e8 -> :sswitch_29
        0x44c -> :sswitch_2b
        0x44d -> :sswitch_2d
        0x44e -> :sswitch_2a
        0x44f -> :sswitch_24
        0x450 -> :sswitch_2c
        0x451 -> :sswitch_2e
        0x4b0 -> :sswitch_2f
        0x514 -> :sswitch_30
        0x515 -> :sswitch_31
        0x516 -> :sswitch_32
        0x578 -> :sswitch_33
        0x579 -> :sswitch_34
        0x640 -> :sswitch_35
        0x641 -> :sswitch_35
        0x64a -> :sswitch_35
        0x64b -> :sswitch_35
        0x654 -> :sswitch_35
        0x655 -> :sswitch_35
        0x65e -> :sswitch_35
        0x65f -> :sswitch_35
        0x6a4 -> :sswitch_1b
        0x6a5 -> :sswitch_1c
        0x708 -> :sswitch_26
        0x76c -> :sswitch_1f
        0x7d0 -> :sswitch_1d
        0x7d1 -> :sswitch_1e
        0xbb9 -> :sswitch_20
        0xc1d -> :sswitch_36
        0xc1e -> :sswitch_37
        0xc1f -> :sswitch_38
        0xc20 -> :sswitch_39
    .end sparse-switch

    .line 1409
    :array_0
    .array-data 8
        -0x4
        -0x1
        -0x3
    .end array-data
.end method

.method private shouldDoNautilusSearch(ZZ)Z
    .locals 2
    .param p1, "isGlobalSearch"    # Z
    .param p2, "isDownloadedOnly"    # Z

    .prologue
    .line 2106
    iget-object v1, p0, Lcom/google/android/music/store/MusicContentProvider;->mNetworkMonitorConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v1}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->hasConnectivity()Z

    move-result v0

    .line 2111
    .local v0, "hasConnectivity":Z
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/music/store/MusicContentProvider;->isNautilusUser()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateAudio(Landroid/content/ContentValues;J)I
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "audioId"    # J

    .prologue
    .line 2829
    invoke-virtual {p1}, Landroid/content/ContentValues;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 2830
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Only rating can be update"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2832
    :cond_0
    const-string v2, "Rating"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 2833
    .local v0, "rating":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 2834
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Missing value for rating"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2836
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, p2, p3, v3}, Lcom/google/android/music/store/Store;->updateRating(JI)I

    move-result v1

    .line 2837
    .local v1, "updated":I
    if-lez v1, :cond_2

    .line 2839
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-wide/16 v4, -0x4

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$AutoPlaylists;->getAutoPlaylistUri(J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2843
    :cond_2
    return v1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 33
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "userWhere"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 2570
    invoke-static {}, Lcom/google/android/music/store/MusicContentProvider;->checkWritePermission()V

    .line 2571
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 2572
    .local v7, "context":Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v25

    .line 2573
    .local v25, "store":Lcom/google/android/music/store/Store;
    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->areUpstreamTrackDeletesEnabled(Landroid/content/Context;)Z

    move-result v9

    .line 2575
    .local v9, "enableRemoteTrackDeletes":Z
    const/16 v23, 0x0

    .line 2577
    .local v23, "result":I
    const-string v29, "false"

    const-string v30, "notify_on_delete"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_0

    const/16 v24, 0x0

    .line 2581
    .local v24, "shouldNotifyChange":Z
    :goto_0
    sget-object v29, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v11

    .line 2582
    .local v11, "matchUri":I
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    .line 2583
    .local v22, "resolver":Landroid/content/ContentResolver;
    const/16 v28, 0x1

    .line 2584
    .local v28, "sync":Z
    sparse-switch v11, :sswitch_data_0

    .line 2644
    new-instance v29, Ljava/lang/UnsupportedOperationException;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Delete not supported on URI: "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v29

    .line 2577
    .end local v11    # "matchUri":I
    .end local v22    # "resolver":Landroid/content/ContentResolver;
    .end local v24    # "shouldNotifyChange":Z
    .end local v28    # "sync":Z
    :cond_0
    const/16 v24, 0x1

    goto :goto_0

    .line 2586
    .restart local v11    # "matchUri":I
    .restart local v22    # "resolver":Landroid/content/ContentResolver;
    .restart local v24    # "shouldNotifyChange":Z
    .restart local v28    # "sync":Z
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 2587
    .local v16, "musicId":J
    if-eqz v9, :cond_5

    .line 2588
    move-object/from16 v0, v25

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->deleteAllDefaultDomainMatchingMusicFiles(J)I

    move-result v23

    .line 2589
    if-lez v23, :cond_4

    const/16 v28, 0x1

    .line 2648
    .end local v16    # "musicId":J
    :cond_1
    :goto_1
    if-eqz v24, :cond_3

    .line 2649
    const/16 v29, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move-object/from16 v2, v29

    move/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2650
    const/16 v29, 0x12d

    move/from16 v0, v29

    if-eq v11, v0, :cond_2

    const/16 v29, 0x191

    move/from16 v0, v29

    if-ne v11, v0, :cond_d

    .line 2652
    :cond_2
    sget-object v29, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2660
    :cond_3
    :goto_2
    return v23

    .line 2589
    .restart local v16    # "musicId":J
    :cond_4
    const/16 v28, 0x0

    goto :goto_1

    .line 2591
    :cond_5
    move-object/from16 v0, v25

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->deleteAllMatchingLocalMusicFiles(J)I

    move-result v23

    .line 2593
    const/16 v28, 0x0

    .line 2595
    goto :goto_1

    .line 2597
    .end local v16    # "musicId":J
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 2598
    .local v18, "playlistId":J
    move-object/from16 v0, v25

    move-wide/from16 v1, v18

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/android/music/store/Store;->deletePlaylist(Landroid/content/Context;J)Z

    move-result v29

    if-eqz v29, :cond_6

    const/16 v23, 0x1

    .line 2599
    :goto_3
    if-eqz v24, :cond_1

    .line 2600
    sget-object v29, Lcom/google/android/music/store/MusicContent$Playlists;->RECENTS_URI:Landroid/net/Uri;

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_1

    .line 2598
    :cond_6
    const/16 v23, 0x0

    goto :goto_3

    .line 2604
    .end local v18    # "playlistId":J
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    .line 2605
    .local v26, "stationId":J
    move-object/from16 v0, v25

    move-wide/from16 v1, v26

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/android/music/store/Store;->deleteRadioStation(Landroid/content/Context;J)Z

    move-result v29

    if-eqz v29, :cond_7

    const/16 v23, 0x1

    .line 2606
    :goto_4
    goto :goto_1

    .line 2605
    :cond_7
    const/16 v23, 0x0

    goto :goto_4

    .line 2608
    .end local v26    # "stationId":J
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v29

    const/16 v30, 0x1

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 2609
    .local v14, "listId":J
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v29

    const/16 v30, 0x3

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 2610
    .local v12, "itemId":J
    move-object/from16 v0, v25

    invoke-virtual {v0, v14, v15, v12, v13}, Lcom/google/android/music/store/Store;->deletePlaylistItem(JJ)Z

    move-result v29

    if-eqz v29, :cond_8

    const/16 v23, 0x1

    .line 2611
    :goto_5
    goto/16 :goto_1

    .line 2610
    :cond_8
    const/16 v23, 0x0

    goto :goto_5

    .line 2613
    .end local v12    # "itemId":J
    .end local v14    # "listId":J
    :sswitch_4
    const/16 v29, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v29

    invoke-virtual {v0, v7, v1}, Lcom/google/android/music/store/Store;->deleteRemoteMusicAndPlaylists(Landroid/content/Context;Z)Z

    move-result v29

    if-eqz v29, :cond_9

    const/16 v23, 0x1

    .line 2614
    :goto_6
    const/16 v28, 0x0

    .line 2615
    goto/16 :goto_1

    .line 2613
    :cond_9
    const/16 v23, 0x0

    goto :goto_6

    .line 2617
    :sswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2618
    .local v4, "albumId":J
    const-string v29, "deleteMode"

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2620
    .local v8, "deleteMode":Ljava/lang/String;
    const-string v29, "NAUTILUS"

    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    .line 2621
    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Lcom/google/android/music/store/Store;->deletePersistentNautilusTracksFromAlbum(J)I

    move-result v23

    .line 2622
    if-lez v23, :cond_a

    const/16 v28, 0x1

    :goto_7
    goto/16 :goto_1

    :cond_a
    const/16 v28, 0x0

    goto :goto_7

    .line 2624
    :cond_b
    new-instance v29, Ljava/lang/UnsupportedOperationException;

    const-string v30, "Full deletion not supported on Albums."

    invoke-direct/range {v29 .. v30}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v29

    .line 2629
    .end local v4    # "albumId":J
    .end local v8    # "deleteMode":Ljava/lang/String;
    :sswitch_6
    const-string v29, "isInCloudQueueMode"

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2632
    .local v6, "cloudQueueQueryParam":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 2633
    .local v20, "queueItemId":J
    const-string v29, "true"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 2634
    .local v10, "isInCloudQueueMode":Z
    invoke-static {v7}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v29

    move-object/from16 v0, v25

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->caqGetQueueItemMusicId(J)J

    move-result-wide v30

    move-object/from16 v0, v25

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2, v10}, Lcom/google/android/music/store/Store;->caqGetContainerSummary(JZ)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v32

    invoke-virtual/range {v29 .. v32}, Lcom/google/android/music/eventlog/MusicEventLogger;->logRemoveFromQueue(JLcom/google/android/music/store/ContainerDescriptor;)V

    .line 2638
    move-object/from16 v0, v25

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2, v10}, Lcom/google/android/music/store/Store;->caqDeleteItem(JZ)Z

    move-result v29

    if-eqz v29, :cond_c

    const/16 v23, 0x1

    .line 2639
    :goto_8
    const/16 v29, 0x1

    move/from16 v0, v23

    move/from16 v1, v29

    if-ne v0, v1, :cond_1

    .line 2640
    invoke-static {v7}, Lcom/google/android/music/cloudclient/CloudQueueManager;->updateCloudQueue(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2638
    :cond_c
    const/16 v23, 0x0

    goto :goto_8

    .line 2655
    .end local v6    # "cloudQueueQueryParam":Ljava/lang/String;
    .end local v10    # "isInCloudQueueMode":Z
    .end local v20    # "queueItemId":J
    :cond_d
    sget-object v29, Lcom/google/android/music/store/MusicContent$Recent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2656
    sget-object v29, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto/16 :goto_2

    .line 2584
    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_0
        0x191 -> :sswitch_5
        0x259 -> :sswitch_1
        0x25b -> :sswitch_3
        0x5dc -> :sswitch_4
        0x6a5 -> :sswitch_2
        0x7d1 -> :sswitch_6
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2328
    sget-object v1, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2445
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 2446
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "MusicContentProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getType called on Unknonw Uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2447
    throw v0

    .line 2340
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :sswitch_0
    const-string v1, "vnd.android.cursor.item/vnd.google.xaudio"

    .line 2442
    :goto_0
    return-object v1

    .line 2347
    :sswitch_1
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.album"

    goto :goto_0

    .line 2350
    :sswitch_2
    const-string v1, "vnd.android.cursor.item/vnd.google.music.album"

    goto :goto_0

    .line 2354
    :sswitch_3
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.artist"

    goto :goto_0

    .line 2357
    :sswitch_4
    const-string v1, "vnd.android.cursor.item/vnd.google.music.artist"

    goto :goto_0

    .line 2361
    :sswitch_5
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.genre"

    goto :goto_0

    .line 2364
    :sswitch_6
    const-string v1, "vnd.android.cursor.item/vnd.google.music.genre"

    goto :goto_0

    .line 2370
    :sswitch_7
    const-string v1, "vnd.android.cursor.item/vnd.google.music.albumart"

    goto :goto_0

    .line 2373
    :sswitch_8
    const-string v1, "vnd.android.cursor.item/vnd.google.music.cachedart"

    goto :goto_0

    .line 2376
    :sswitch_9
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.recent"

    goto :goto_0

    .line 2379
    :sswitch_a
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.keepon"

    goto :goto_0

    .line 2382
    :sswitch_b
    const-string v1, "vnd.android.cursor.item/vnd.google.music.keepon"

    goto :goto_0

    .line 2385
    :sswitch_c
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.downloadqueue"

    goto :goto_0

    .line 2391
    :sswitch_d
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.playlist"

    goto :goto_0

    .line 2394
    :sswitch_e
    const-string v1, "vnd.android.cursor.item/vnd.google.music.playlist"

    goto :goto_0

    .line 2397
    :sswitch_f
    const-string v1, "vnd.android.cursor.dir/vnd.google.listitems"

    goto :goto_0

    .line 2400
    :sswitch_10
    const-string v1, "vnd.android.cursor.item/vnd.google.listitems"

    goto :goto_0

    .line 2403
    :sswitch_11
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.autoplaylist"

    goto :goto_0

    .line 2406
    :sswitch_12
    const-string v1, "vnd.android.cursor.item/vnd.google.music.autoplaylist"

    goto :goto_0

    .line 2409
    :sswitch_13
    const-string v1, "vnd.android.cursor.dir/vnd.google.autolistitems"

    goto :goto_0

    .line 2412
    :sswitch_14
    const-string v1, "vnd.android.cursor.item/vnd.google.music.account"

    goto :goto_0

    .line 2415
    :sswitch_15
    const-string v1, "vnd.android.cursor.item/vnd.google.music.config"

    goto :goto_0

    .line 2418
    :sswitch_16
    const-string v1, "vnd.android.cursor.item/vnd.google.music.config"

    goto :goto_0

    .line 2421
    :sswitch_17
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.seeds"

    goto :goto_0

    .line 2424
    :sswitch_18
    const-string v1, "vnd.android.cursor.item/vnd.google.music.seeds"

    goto :goto_0

    .line 2427
    :sswitch_19
    const-string v1, "vnd.android.cursor.dir/vnd.google.xaudio"

    goto :goto_0

    .line 2430
    :sswitch_1a
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.radio_stations"

    goto :goto_0

    .line 2433
    :sswitch_1b
    const-string v1, "vnd.android.cursor.item/vnd.google.music.radio_stations"

    goto :goto_0

    .line 2436
    :sswitch_1c
    const-string v1, "vnd.android.cursor.item/vnd.google.music.mainstage"

    goto :goto_0

    .line 2439
    :sswitch_1d
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.shared_with_me_playlist"

    goto :goto_0

    .line 2442
    :sswitch_1e
    const-string v1, "vnd.android.cursor.dir/vnd.google.music.playlist_share_state"

    goto :goto_0

    .line 2328
    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_0
        0x12d -> :sswitch_0
        0x12e -> :sswitch_0
        0x12f -> :sswitch_0
        0x130 -> :sswitch_0
        0x131 -> :sswitch_0
        0x132 -> :sswitch_0
        0x190 -> :sswitch_1
        0x191 -> :sswitch_2
        0x192 -> :sswitch_1
        0x193 -> :sswitch_1
        0x1f4 -> :sswitch_3
        0x1f5 -> :sswitch_4
        0x1f6 -> :sswitch_1
        0x1f7 -> :sswitch_0
        0x1f8 -> :sswitch_3
        0x258 -> :sswitch_d
        0x259 -> :sswitch_e
        0x25a -> :sswitch_f
        0x25b -> :sswitch_10
        0x25c -> :sswitch_d
        0x25d -> :sswitch_d
        0x25e -> :sswitch_d
        0x26c -> :sswitch_11
        0x26d -> :sswitch_12
        0x26e -> :sswitch_13
        0x2bc -> :sswitch_5
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_1
        0x2bf -> :sswitch_5
        0x2c0 -> :sswitch_6
        0x320 -> :sswitch_7
        0x323 -> :sswitch_7
        0x324 -> :sswitch_7
        0x325 -> :sswitch_7
        0x326 -> :sswitch_8
        0x384 -> :sswitch_9
        0x3b6 -> :sswitch_a
        0x3b7 -> :sswitch_b
        0x3e8 -> :sswitch_c
        0x4b0 -> :sswitch_0
        0x514 -> :sswitch_14
        0x515 -> :sswitch_15
        0x516 -> :sswitch_16
        0x578 -> :sswitch_17
        0x579 -> :sswitch_18
        0x5dc -> :sswitch_19
        0x6a4 -> :sswitch_1a
        0x6a5 -> :sswitch_1b
        0x708 -> :sswitch_1c
        0x76c -> :sswitch_1d
        0xbb9 -> :sswitch_1e
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 34
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 2454
    invoke-static {}, Lcom/google/android/music/store/MusicContentProvider;->checkWritePermission()V

    .line 2456
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2457
    .local v4, "context":Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    .line 2458
    .local v6, "store":Lcom/google/android/music/store/Store;
    sget-object v14, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v24

    .line 2459
    .local v24, "matchUri":I
    const-wide/16 v26, 0x0

    .line 2460
    .local v26, "newId":J
    const/16 v28, 0x0

    .line 2461
    .local v28, "newUri":Landroid/net/Uri;
    sparse-switch v24, :sswitch_data_0

    .line 2557
    new-instance v14, Ljava/lang/UnsupportedOperationException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Insert not supported on URI: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " (match="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, v24

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 2463
    :sswitch_0
    const/16 v31, 0x0

    .line 2464
    .local v31, "playlistType":I
    const-string v14, "playlist_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2465
    .local v7, "listName":Ljava/lang/String;
    const-string v14, "playlist_type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 2466
    const-string v14, "playlist_type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v31

    .line 2468
    :cond_0
    const-string v14, "playlist_description"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2470
    .local v8, "listDescription":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v5

    .line 2471
    .local v5, "account":Landroid/accounts/Account;
    const/16 v14, 0x47

    move/from16 v0, v31

    if-ne v0, v14, :cond_3

    .line 2472
    const-string v14, "playlist_share_token"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2474
    .local v10, "shareToken":Ljava/lang/String;
    const-string v14, "playlist_owner_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2476
    .local v9, "listOwnerName":Ljava/lang/String;
    const-string v14, "playlist_art_url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2478
    .local v11, "artUrl":Ljava/lang/String;
    const-string v14, "playlist_owner_profile_photo_url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 2480
    .local v12, "ownerProfilePhotoUrl":Ljava/lang/String;
    const-string v14, "playlist_editor_artwork"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 2482
    .local v13, "editorArtwork":Ljava/lang/String;
    invoke-static/range {v4 .. v13}, Lcom/google/android/music/store/SharedContentProviderHelper;->createFollowedSharedPlaylist(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/music/store/Store;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v26

    .line 2492
    .end local v9    # "listOwnerName":Ljava/lang/String;
    .end local v10    # "shareToken":Ljava/lang/String;
    .end local v11    # "artUrl":Ljava/lang/String;
    .end local v12    # "ownerProfilePhotoUrl":Ljava/lang/String;
    .end local v13    # "editorArtwork":Ljava/lang/String;
    :goto_0
    const-wide/16 v14, 0x0

    cmp-long v14, v26, v14

    if-eqz v14, :cond_1

    .line 2493
    invoke-static/range {v26 .. v27}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v28

    .line 2561
    .end local v5    # "account":Landroid/accounts/Account;
    .end local v7    # "listName":Ljava/lang/String;
    .end local v8    # "listDescription":Ljava/lang/String;
    .end local v31    # "playlistType":I
    :cond_1
    :goto_1
    if-eqz v28, :cond_2

    .line 2562
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v16

    invoke-virtual {v14, v0, v15, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2564
    :cond_2
    return-object v28

    .line 2487
    .restart local v5    # "account":Landroid/accounts/Account;
    .restart local v7    # "listName":Ljava/lang/String;
    .restart local v8    # "listDescription":Ljava/lang/String;
    .restart local v31    # "playlistType":I
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/store/MusicContentProvider;->getShareStateParam(Landroid/net/Uri;)I

    move-result v19

    .line 2488
    .local v19, "shareState":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MusicContentProvider;->mNetworkMonitorConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    move-object/from16 v20, v0

    move-object v15, v6

    move-object/from16 v16, v5

    move-object/from16 v17, v7

    move-object/from16 v18, v8

    invoke-static/range {v14 .. v20}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->createPlaylist(Landroid/content/Context;Lcom/google/android/music/store/Store;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/music/net/NetworkMonitorServiceConnection;)J

    move-result-wide v26

    goto :goto_0

    .line 2497
    .end local v5    # "account":Landroid/accounts/Account;
    .end local v7    # "listName":Ljava/lang/String;
    .end local v8    # "listDescription":Ljava/lang/String;
    .end local v19    # "shareState":I
    .end local v31    # "playlistType":I
    :sswitch_1
    const-string v14, "audio_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    .line 2498
    .local v25, "musicId":Ljava/lang/Long;
    if-eqz v25, :cond_4

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_4

    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->size()I

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_5

    .line 2499
    :cond_4
    new-instance v14, Ljava/lang/IllegalArgumentException;

    const-string v15, "When inserting playlist items, the music id must be provided and nothing else"

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 2503
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v14

    const/4 v15, 0x1

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v32

    .line 2505
    .local v32, "playlistId":J
    :try_start_0
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1, v14, v15}, Lcom/google/android/music/store/Store;->appendPlaylistItem(JJ)J

    move-result-wide v26

    .line 2506
    const-wide/16 v14, 0x0

    cmp-long v14, v26, v14

    if-eqz v14, :cond_1

    .line 2507
    move-wide/from16 v0, v32

    move-wide/from16 v2, v26

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemUri(JJ)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v28

    goto :goto_1

    .line 2509
    :catch_0
    move-exception v22

    .line 2510
    .local v22, "e":Ljava/io/FileNotFoundException;
    const-string v14, "MusicContentProvider"

    const-string v15, "Failed to insert playlist item"

    move-object/from16 v0, v22

    invoke-static {v14, v15, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 2519
    .end local v22    # "e":Ljava/io/FileNotFoundException;
    .end local v25    # "musicId":Ljava/lang/Long;
    .end local v32    # "playlistId":J
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v29

    .line 2520
    .local v29, "nid":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 2521
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cannot insert a local item: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 2523
    :cond_6
    move/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v29

    invoke-static {v4, v6, v0, v1, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insert(Landroid/content/Context;Lcom/google/android/music/store/Store;ILandroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v28

    .line 2525
    goto/16 :goto_1

    .line 2528
    .end local v29    # "nid":Ljava/lang/String;
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v30

    .line 2530
    .local v30, "nids":Ljava/lang/String;
    const-string v14, ","

    move-object/from16 v0, v30

    invoke-virtual {v0, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    .line 2531
    .local v21, "delimiterIndex":I
    const/4 v14, -0x1

    move/from16 v0, v21

    if-le v0, v14, :cond_7

    const/4 v14, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v23

    .line 2534
    .local v23, "isNautilus":Z
    :goto_2
    if-nez v23, :cond_8

    .line 2535
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cannot insert local items: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v30

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 2531
    .end local v23    # "isNautilus":Z
    :cond_7
    invoke-static/range {v30 .. v30}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v23

    goto :goto_2

    .line 2537
    .restart local v23    # "isNautilus":Z
    :cond_8
    move/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-static {v4, v6, v0, v1, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insert(Landroid/content/Context;Lcom/google/android/music/store/Store;ILandroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v28

    .line 2539
    goto/16 :goto_1

    .line 2543
    .end local v21    # "delimiterIndex":I
    .end local v23    # "isNautilus":Z
    .end local v30    # "nids":Ljava/lang/String;
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v14

    const/4 v15, 0x1

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 2544
    .restart local v29    # "nid":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 2545
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cannot insert a local item: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 2547
    :cond_9
    move/from16 v0, v24

    move-object/from16 v1, p1

    move-object/from16 v2, v29

    invoke-static {v4, v6, v0, v1, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->insert(Landroid/content/Context;Lcom/google/android/music/store/Store;ILandroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v28

    .line 2550
    goto/16 :goto_1

    .line 2553
    .end local v29    # "nid":Ljava/lang/String;
    :sswitch_5
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v6, v0, v1}, Lcom/google/android/music/store/SharedContentProviderHelper;->insert(Landroid/content/Context;Lcom/google/android/music/store/Store;ILandroid/net/Uri;)Landroid/net/Uri;

    move-result-object v28

    .line 2554
    goto/16 :goto_1

    .line 2461
    nop

    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_2
        0x12e -> :sswitch_4
        0x12f -> :sswitch_4
        0x132 -> :sswitch_3
        0x258 -> :sswitch_0
        0x25a -> :sswitch_1
        0x76c -> :sswitch_5
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 388
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    .line 390
    new-instance v0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-direct {v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/store/MusicContentProvider;->mNetworkMonitorConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 391
    iget-object v0, p0, Lcom/google/android/music/store/MusicContentProvider;->mNetworkMonitorConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->bindToService(Landroid/content/Context;)V

    .line 393
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v3, 0x2

    const-wide/16 v4, 0x3

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/google/android/music/store/MusicContentProvider;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 398
    new-instance v0, Lcom/google/android/music/store/ArtContentProviderHelper;

    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/store/ArtContentProviderHelper;-><init>(Landroid/content/Context;Landroid/content/UriMatcher;)V

    iput-object v0, p0, Lcom/google/android/music/store/MusicContentProvider;->mArtContentProviderHelper:Lcom/google/android/music/store/ArtContentProviderHelper;

    .line 400
    new-instance v0, Lcom/google/android/music/store/SituationsCache;

    invoke-virtual {p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/store/MusicContentProvider;->mNetworkMonitorConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/store/SituationsCache;-><init>(Landroid/content/Context;Lcom/google/android/music/net/NetworkMonitorServiceConnection;)V

    iput-object v0, p0, Lcom/google/android/music/store/MusicContentProvider;->mSituationsCache:Lcom/google/android/music/store/SituationsCache;

    .line 404
    return v2
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/music/store/MusicContentProvider;->mArtContentProviderHelper:Lcom/google/android/music/store/ArtContentProviderHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/ArtContentProviderHelper;->openArtFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 481
    new-instance v6, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    invoke-direct {v6}, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/MusicContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 488
    new-instance v6, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;

    invoke-direct {v6, p6}, Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;-><init>(Landroid/os/CancellationSignal;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/store/MusicContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 41
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "userWhere"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 2666
    const/16 v39, 0x1

    .line 2667
    .local v39, "sync":Z
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 2668
    .local v5, "context":Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    .line 2669
    .local v6, "store":Lcom/google/android/music/store/Store;
    sget-object v13, Lcom/google/android/music/store/MusicContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v34

    .line 2670
    .local v34, "matchUri":I
    const/16 v13, 0x514

    move/from16 v0, v34

    if-eq v0, v13, :cond_0

    .line 2671
    invoke-static {}, Lcom/google/android/music/store/MusicContentProvider;->checkWritePermission()V

    .line 2673
    :cond_0
    const/16 v35, 0x0

    .line 2674
    .local v35, "result":I
    sparse-switch v34, :sswitch_data_0

    .line 2796
    new-instance v13, Ljava/lang/UnsupportedOperationException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Update not supported on URI: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 2676
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v30

    .line 2677
    .local v30, "audioId":J
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-wide/from16 v2, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/store/MusicContentProvider;->updateAudio(Landroid/content/ContentValues;J)I

    move-result v35

    .line 2803
    .end local v30    # "audioId":J
    :cond_1
    :goto_0
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v13, v0, v14, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2804
    return v35

    .line 2680
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2681
    .local v8, "playlistId":J
    const-string v13, "action"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 2683
    .local v38, "specialAction":Ljava/lang/String;
    if-eqz v38, :cond_2

    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_3

    .line 2685
    :cond_2
    const-string v13, "playlist_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2686
    .local v10, "newName":Ljava/lang/String;
    const-string v13, "playlist_description"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2688
    .local v11, "newDescription":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/store/MusicContentProvider;->getShareStateParam(Landroid/net/Uri;)I

    move-result v12

    .line 2689
    .local v12, "shareState":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v7

    .line 2690
    .local v7, "account":Landroid/accounts/Account;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/store/MusicContentProvider;->mNetworkMonitorConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-static/range {v5 .. v13}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->updatePlaylist(Landroid/content/Context;Lcom/google/android/music/store/Store;Landroid/accounts/Account;JLjava/lang/String;Ljava/lang/String;ILcom/google/android/music/net/NetworkMonitorServiceConnection;)I

    move-result v35

    .line 2693
    goto :goto_0

    .end local v7    # "account":Landroid/accounts/Account;
    .end local v10    # "newName":Ljava/lang/String;
    .end local v11    # "newDescription":Ljava/lang/String;
    .end local v12    # "shareState":I
    :cond_3
    const-string v13, "album"

    move-object/from16 v0, v38

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 2696
    const-string v13, "album_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    .line 2697
    .local v26, "albumId":J
    move-wide/from16 v0, v26

    invoke-virtual {v6, v8, v9, v0, v1}, Lcom/google/android/music/store/Store;->appendAlbumToPlaylist(JJ)I

    move-result v35

    .line 2699
    goto :goto_0

    .end local v26    # "albumId":J
    :cond_4
    const-string v13, "artist"

    move-object/from16 v0, v38

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 2702
    const-string v13, "AlbumArtistId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    .line 2704
    .local v28, "artistId":J
    move-wide/from16 v0, v28

    invoke-virtual {v6, v8, v9, v0, v1}, Lcom/google/android/music/store/Store;->appendArtistToPlaylist(JJ)I

    move-result v35

    .line 2706
    goto :goto_0

    .end local v28    # "artistId":J
    :cond_5
    const-string v13, "genre"

    move-object/from16 v0, v38

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 2709
    const-string v13, "GenreId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 2710
    .local v16, "genreId":J
    const-string v13, "album_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 2711
    const-string v13, "album_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object v13, v6

    move-wide v14, v8

    invoke-virtual/range {v13 .. v19}, Lcom/google/android/music/store/Store;->appendGenreToPlaylist(JJJ)I

    move-result v35

    goto/16 :goto_0

    .line 2714
    :cond_6
    move-wide/from16 v0, v16

    invoke-virtual {v6, v8, v9, v0, v1}, Lcom/google/android/music/store/Store;->appendGenreToPlaylist(JJ)I

    move-result v35

    goto/16 :goto_0

    .line 2717
    .end local v16    # "genreId":J
    :cond_7
    const-string v13, "playlist"

    move-object/from16 v0, v38

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 2720
    const-string v13, "playlist_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v36

    .line 2721
    .local v36, "sourcePlaylistId":J
    move-wide/from16 v0, v36

    invoke-virtual {v6, v8, v9, v0, v1}, Lcom/google/android/music/store/Store;->appendPlaylistToPlaylist(JJ)I

    move-result v35

    .line 2722
    goto/16 :goto_0

    .end local v36    # "sourcePlaylistId":J
    :cond_8
    const-string v13, "playqueue"

    move-object/from16 v0, v38

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2724
    const-string v13, "isInCloudQueueMode"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2726
    .local v4, "cloudQueueQueryParam":Ljava/lang/String;
    const-string v13, "true"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    .line 2727
    .local v33, "isInCloudQueueMode":Z
    move/from16 v0, v33

    invoke-virtual {v6, v8, v9, v0}, Lcom/google/android/music/store/Store;->caqAppendPlayQueueToPlaylist(JZ)I

    move-result v35

    .line 2728
    goto/16 :goto_0

    .line 2732
    .end local v4    # "cloudQueueQueryParam":Ljava/lang/String;
    .end local v8    # "playlistId":J
    .end local v33    # "isInCloudQueueMode":Z
    .end local v38    # "specialAction":Ljava/lang/String;
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x1

    invoke-interface {v13, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 2733
    .local v20, "listId":J
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x3

    invoke-interface {v13, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v22

    .line 2734
    .local v22, "itemToMove":J
    const-string v13, "moveBefore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    .local v24, "moveBefore":J
    move-object/from16 v19, v6

    .line 2736
    invoke-virtual/range {v19 .. v25}, Lcom/google/android/music/store/Store;->movePlaylistItem(JJJ)V

    goto/16 :goto_0

    .line 2740
    .end local v20    # "listId":J
    .end local v22    # "itemToMove":J
    .end local v24    # "moveBefore":J
    :sswitch_3
    new-instance v13, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v13}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v13

    .line 2767
    :sswitch_4
    const/16 v39, 0x0

    .line 2768
    const-string v13, "moveFrom"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v32

    .line 2770
    .local v32, "fromPos":I
    const-string v13, "moveTo"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v40

    .line 2773
    .local v40, "toPos":I
    const-string v13, "isInCloudQueueMode"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2776
    .restart local v4    # "cloudQueueQueryParam":Ljava/lang/String;
    const-string v13, "true"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    .line 2777
    .restart local v33    # "isInCloudQueueMode":Z
    sget-object v13, Lcom/google/android/music/store/QueueUtils$MoveMode;->AUTO:Lcom/google/android/music/store/QueueUtils$MoveMode;

    move/from16 v0, v32

    move/from16 v1, v40

    move/from16 v2, v33

    invoke-virtual {v6, v0, v1, v13, v2}, Lcom/google/android/music/store/Store;->caqMoveItem(IILcom/google/android/music/store/QueueUtils$MoveMode;Z)Z

    .line 2778
    if-eqz v33, :cond_1

    .line 2779
    move/from16 v0, v32

    int-to-long v14, v0

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v5, v14, v15, v0, v1}, Lcom/google/android/music/cloudclient/CloudQueueManager;->updateCloudQueueVersionForUpdatedItems(Landroid/content/Context;JJ)V

    goto/16 :goto_0

    .line 2784
    .end local v4    # "cloudQueueQueryParam":Ljava/lang/String;
    .end local v32    # "fromPos":I
    .end local v33    # "isInCloudQueueMode":Z
    .end local v40    # "toPos":I
    :sswitch_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MusicContentProvider;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v5, v6, v0, v13}, Lcom/google/android/music/store/MainstageContentProviderHelper;->dismissItem(Landroid/content/Context;Lcom/google/android/music/store/Store;Landroid/net/Uri;Landroid/accounts/Account;)V

    .line 2788
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Lcom/google/android/music/store/MusicContent$Recent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 2791
    :sswitch_6
    move/from16 v0, v34

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v5, v0, v1, v2}, Lcom/google/android/music/store/PlaylistContentProviderHelper;->update(Landroid/content/Context;ILandroid/net/Uri;Landroid/content/ContentValues;)I

    move-result v35

    .line 2792
    goto/16 :goto_0

    .line 2674
    nop

    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_0
        0x259 -> :sswitch_1
        0x25b -> :sswitch_2
        0x514 -> :sswitch_3
        0x708 -> :sswitch_5
        0x7d0 -> :sswitch_4
        0xbb9 -> :sswitch_6
    .end sparse-switch
.end method
