.class public Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
.super Lcom/google/api/client/json/GenericJson;
.source "SyncableBlacklistItem.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;


# instance fields
.field public mDismissalTimestampMillsec:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "dismissalTimestamp"
    .end annotation
.end field

.field public mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "item_id"
    .end annotation
.end field

.field public mLocalId:J

.field public mRemoteId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "uuid"
    .end annotation
.end field

.field public mSuggestionReason:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "suggestionReason"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 43
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mLocalId:J

    return-void
.end method

.method public static parse(Lcom/google/android/music/store/BlacklistItem;)Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    .locals 6
    .param p0, "blacklistItem"    # Lcom/google/android/music/store/BlacklistItem;

    .prologue
    .line 161
    new-instance v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-direct {v1}, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;-><init>()V

    .line 162
    .local v1, "syncable":Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getSourceId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mRemoteId:Ljava/lang/String;

    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getDismissDate()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    .line 164
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getReasonType()I

    move-result v2

    iput v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mSuggestionReason:I

    .line 165
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mLocalId:J

    .line 166
    new-instance v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    invoke-direct {v2}, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;-><init>()V

    iput-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    .line 167
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mLocalId:J

    .line 169
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getAlbumLocalId()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 170
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    sget-object v3, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->ALBUM:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;

    invoke-virtual {v3}, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->getRemoteValue()I

    move-result v3

    iput v3, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mType:I

    .line 171
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    new-instance v3, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;-><init>()V

    iput-object v3, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    .line 172
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getAlbumArtist()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mArtist:Ljava/lang/String;

    .line 173
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mMetajamId:Ljava/lang/String;

    .line 174
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getAlbumTitle()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mTitle:Ljava/lang/String;

    .line 191
    :cond_0
    :goto_0
    return-object v1

    .line 175
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getListShareToken()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 176
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    sget-object v3, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->PLAYLIST:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;

    invoke-virtual {v3}, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->getRemoteValue()I

    move-result v3

    iput v3, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mType:I

    .line 177
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    new-instance v3, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;-><init>()V

    iput-object v3, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mPlaylistId:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;

    .line 178
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mPlaylistId:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;

    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getListShareToken()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;->mShareToken:Ljava/lang/String;

    goto :goto_0

    .line 179
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getRadioSeedId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 180
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    sget-object v3, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->RADIO_STATION:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;

    invoke-virtual {v3}, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson$IdType;->getRemoteValue()I

    move-result v3

    iput v3, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mType:I

    .line 182
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    new-instance v3, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;-><init>()V

    iput-object v3, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mRadioId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    .line 183
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mRadioId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mSeeds:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getRadioSeedId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getRadioSeedType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/music/sync/google/model/RadioSeed;->createRadioSeed(Ljava/lang/String;I)Lcom/google/android/music/sync/google/model/RadioSeed;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    invoke-virtual {p0}, Lcom/google/android/music/store/BlacklistItem;->getRadioRemoteId()Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "radioId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 188
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mRadioId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    iput-object v0, v2, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public formatAsBlacklistItem(Lcom/google/android/music/store/BlacklistItem;)Lcom/google/android/music/store/BlacklistItem;
    .locals 5
    .param p1, "blacklistItem"    # Lcom/google/android/music/store/BlacklistItem;

    .prologue
    const/4 v4, 0x0

    .line 195
    if-nez p1, :cond_0

    .line 196
    new-instance p1, Lcom/google/android/music/store/BlacklistItem;

    .end local p1    # "blacklistItem":Lcom/google/android/music/store/BlacklistItem;
    invoke-direct {p1}, Lcom/google/android/music/store/BlacklistItem;-><init>()V

    .line 198
    .restart local p1    # "blacklistItem":Lcom/google/android/music/store/BlacklistItem;
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/store/BlacklistItem;->reset()V

    .line 199
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    if-eqz v2, :cond_2

    .line 200
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mArtist:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setAlbumArtist(Ljava/lang/String;)V

    .line 201
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setAlbumTitle(Ljava/lang/String;)V

    .line 202
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mAlbumId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;->mMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 204
    invoke-virtual {p1}, Lcom/google/android/music/store/BlacklistItem;->getAlbumTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/music/store/BlacklistItem;->getAlbumArtist()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MusicFile;->calculateAlbumId(Ljava/lang/String;Ljava/lang/String;Z)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/store/BlacklistItem;->setAlbumLocalId(J)V

    .line 220
    :cond_1
    :goto_0
    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    invoke-virtual {p1, v2, v3}, Lcom/google/android/music/store/BlacklistItem;->setDismissDate(J)V

    .line 221
    iget v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mSuggestionReason:I

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setReasonType(I)V

    .line 222
    return-object p1

    .line 206
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mPlaylistId:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;

    if-eqz v2, :cond_3

    .line 207
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mPlaylistId:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;->mShareToken:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setListShareToken(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mRadioId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    if-eqz v2, :cond_1

    .line 209
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mRadioId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setRadioRemoteId(Ljava/lang/String;)V

    .line 210
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;->mRadioId:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;

    iget-object v1, v2, Lcom/google/android/music/cloudclient/ListenNowRadioStationJson$RadioStationIdJson;->mSeeds:Ljava/util/List;

    .line 211
    .local v1, "seeds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/RadioSeed;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 212
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-virtual {v2}, Lcom/google/android/music/sync/google/model/RadioSeed;->getSourceIdAndType()Landroid/util/Pair;

    move-result-object v0

    .line 213
    .local v0, "seed":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setRadioSeedId(Ljava/lang/String;)V

    .line 216
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/BlacklistItem;->setRadioSeedType(I)V

    goto :goto_0
.end method

.method public getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forAddBlacklistItems()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getCreationTimestamp()J
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unneeded operation for config API"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forGetBlacklistItems()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLastModifiedTimestamp()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    return-wide v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mLocalId:J

    return-wide v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 125
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public isUpdate()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public serializeAsJson()[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 114
    :try_start_0
    invoke-static {p0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MusicSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to serialize a Dissmissed Item entry as JSON: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 118
    new-instance v1, Lcom/google/android/music/store/InvalidDataException;

    const-string v2, "Unable to serialize blacklist item for upstream sync."

    invoke-direct {v1, v2, v0}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setCreationTimestamp(J)V
    .locals 2
    .param p1, "time"    # J

    .prologue
    .line 86
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unneeded operation for config API"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setIsDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 98
    return-void
.end method

.method public setLastModifiedTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    .line 77
    return-void
.end method

.method public setRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mRemoteId:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public validateForUpstreamDelete()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public validateForUpstreamInsert()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid blacklist item for upstream insert."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    return-void
.end method

.method public validateForUpstreamUpdate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public wipeAllFields()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    iput-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mIdentifier:Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mSuggestionReason:I

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    .line 50
    iput-object v2, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mRemoteId:Ljava/lang/String;

    .line 51
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mLocalId:J

    .line 52
    return-void
.end method
