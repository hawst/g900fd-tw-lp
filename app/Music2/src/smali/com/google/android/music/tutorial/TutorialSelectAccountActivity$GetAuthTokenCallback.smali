.class public Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;
.super Ljava/lang/Object;
.source "TutorialSelectAccountActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GetAuthTokenCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field final synthetic this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Landroid/accounts/Account;)V
    .locals 0
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    iput-object p2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->mAccount:Landroid/accounts/Account;

    .line 283
    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const v6, 0x7f0b0044

    const v5, 0x7f0b0043

    .line 286
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    invoke-virtual {v2}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->isActivityDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 288
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 289
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 290
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    iget-object v3, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->mAccount:Landroid/accounts/Account;

    # invokes: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->authenticationSuccess(Landroid/accounts/Account;)V
    invoke-static {v2, v3}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$200(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;Landroid/accounts/Account;)V

    .line 312
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 292
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    const v3, 0x7f0b0043

    const v4, 0x7f0b0044

    # invokes: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->authenticationFailed(II)V
    invoke-static {v2, v3, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$300(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;II)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 298
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 299
    .local v1, "e":Landroid/accounts/OperationCanceledException;
    const-string v2, "MusicAccountSelect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResult failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    .end local v1    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v1

    .line 302
    .local v1, "e":Landroid/accounts/AuthenticatorException;
    const-string v2, "MusicAccountSelect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResult failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    # invokes: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->authenticationFailed(II)V
    invoke-static {v2, v5, v6}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$300(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;II)V

    goto :goto_0

    .line 305
    .end local v1    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v1

    .line 306
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "MusicAccountSelect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResult failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-object v2, p0, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity$GetAuthTokenCallback;->this$0:Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;

    const v3, 0x7f0b0041

    const v4, 0x7f0b0042

    # invokes: Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->authenticationFailed(II)V
    invoke-static {v2, v3, v4}, Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;->access$300(Lcom/google/android/music/tutorial/TutorialSelectAccountActivity;II)V

    goto :goto_0
.end method
