.class public abstract Lcom/google/android/music/medialist/ExternalSongList;
.super Lcom/google/android/music/medialist/SongList;
.source "ExternalSongList.java"


# static fields
.field private static final LOGV:Z

.field private static final TAG:Ljava/lang/String; = "ExternalSongList"


# instance fields
.field private mFlags:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/medialist/ExternalSongList;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/download/ContentIdentifier$Domain;)V
    .locals 1
    .param p1, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;

    .prologue
    .line 27
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/medialist/ExternalSongList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;Z)V

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/download/ContentIdentifier$Domain;Z)V
    .locals 1
    .param p1, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;
    .param p2, "includeExternal"    # Z

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, v0, p1, v0, p2}, Lcom/google/android/music/medialist/SongList;-><init>(ILcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V

    .line 24
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/music/medialist/ExternalSongList;->mFlags:I

    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne p1, v0, :cond_1

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 40
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/ExternalSongList;->unsetFlag(I)V

    .line 41
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/ExternalSongList;->unsetFlag(I)V

    .line 42
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/ExternalSongList;->unsetFlag(I)V

    .line 43
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/ExternalSongList;->unsetFlag(I)V

    .line 44
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/ExternalSongList;->unsetFlag(I)V

    .line 45
    return-void
.end method


# virtual methods
.method public addToStore(Landroid/content/Context;Z)[J
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "addToLibrary"    # Z

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "localIds":[J
    invoke-virtual {p0}, Lcom/google/android/music/medialist/ExternalSongList;->isAddToStoreSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/ExternalSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1, p2}, Lcom/google/android/music/store/MusicContent;->addExternalSongsToStore(Landroid/content/Context;Landroid/net/Uri;Z)[J

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    .line 128
    :cond_0
    const-string v1, "ExternalSongList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addToDatabase called on a song list that doesn\'t support the operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/medialist/ExternalSongList;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/music/medialist/ExternalSongList;->mFlags:I

    return v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 95
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getMusicFile(ILcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;
    .locals 1
    .param p1, "position"    # I
    .param p2, "file"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 99
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getSongCursor(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p3, "cols"    # [Ljava/lang/String;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p3, v0}, Lcom/google/android/music/medialist/ExternalSongList;->getCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v0

    return-object v0
.end method

.method public getValidSortOrders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAddToStoreSupported()Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method protected final setFlag(I)V
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 86
    iget v0, p0, Lcom/google/android/music/medialist/ExternalSongList;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/music/medialist/ExternalSongList;->mFlags:I

    .line 87
    return-void
.end method

.method public storeDefaultSortOrder(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    return-void
.end method

.method protected final unsetFlag(I)V
    .locals 2
    .param p1, "flag"    # I

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/music/medialist/ExternalSongList;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/medialist/ExternalSongList;->mFlags:I

    .line 79
    return-void
.end method
