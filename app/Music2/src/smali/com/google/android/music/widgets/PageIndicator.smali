.class public Lcom/google/android/music/widgets/PageIndicator;
.super Landroid/widget/LinearLayout;
.source "PageIndicator.java"


# instance fields
.field private mIndicator1:Landroid/widget/ImageView;

.field private mIndicator2:Landroid/widget/ImageView;

.field private mIndicator3:Landroid/widget/ImageView;

.field private mIndicator4:Landroid/widget/ImageView;

.field private mIndicators:[Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method private setSelectedState(Landroid/widget/ImageView;Z)V
    .locals 1
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "selected"    # Z

    .prologue
    .line 45
    if-eqz p2, :cond_0

    .line 46
    const v0, 0x7f020127

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    const v0, 0x7f020128

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    const v0, 0x7f0e01e4

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/PageIndicator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator1:Landroid/widget/ImageView;

    .line 33
    const v0, 0x7f0e01e5

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/PageIndicator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator2:Landroid/widget/ImageView;

    .line 34
    const v0, 0x7f0e01e6

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/PageIndicator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator3:Landroid/widget/ImageView;

    .line 35
    const v0, 0x7f0e01e7

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/PageIndicator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator4:Landroid/widget/ImageView;

    .line 36
    return-void
.end method

.method public setVideoServiceAvailability(Z)V
    .locals 6
    .param p1, "isVideoServiceAvailable"    # Z

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    if-eqz p1, :cond_0

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator1:Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator2:Landroid/widget/ImageView;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator3:Landroid/widget/ImageView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator4:Landroid/widget/ImageView;

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicators:[Landroid/widget/ImageView;

    .line 55
    iget-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator4:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    :goto_0
    return-void

    .line 57
    :cond_0
    new-array v0, v5, [Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator1:Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator2:Landroid/widget/ImageView;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator3:Landroid/widget/ImageView;

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicators:[Landroid/widget/ImageView;

    .line 58
    iget-object v0, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicator4:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateSelectedDrawable(I)V
    .locals 3
    .param p1, "selected"    # I

    .prologue
    .line 39
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicators:[Landroid/widget/ImageView;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 40
    iget-object v1, p0, Lcom/google/android/music/widgets/PageIndicator;->mIndicators:[Landroid/widget/ImageView;

    aget-object v2, v1, v0

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v2, v1}, Lcom/google/android/music/widgets/PageIndicator;->setSelectedState(Landroid/widget/ImageView;Z)V

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 42
    :cond_1
    return-void
.end method
