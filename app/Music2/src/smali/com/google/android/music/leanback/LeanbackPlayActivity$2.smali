.class Lcom/google/android/music/leanback/LeanbackPlayActivity$2;
.super Landroid/os/Handler;
.source "LeanbackPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 190
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 202
    :goto_0
    return-void

    .line 192
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->refreshNow()J
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$500(Lcom/google/android/music/leanback/LeanbackPlayActivity;)J

    move-result-wide v0

    .line 193
    .local v0, "next":J
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->queueNextRefresh(J)V
    invoke-static {v2, v0, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$600(Lcom/google/android/music/leanback/LeanbackPlayActivity;J)V

    goto :goto_0

    .line 196
    .end local v0    # "next":J
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/content/Intent;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->updateTrackInfoImpl(Landroid/content/Intent;)V
    invoke-static {v3, v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$700(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/content/Intent;)V

    goto :goto_0

    .line 190
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
