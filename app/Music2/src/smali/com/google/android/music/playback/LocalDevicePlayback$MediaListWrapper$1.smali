.class Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;
.super Landroid/database/ContentObserver;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 1470
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 1473
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->processContentChange()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->access$5000(Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;)V

    .line 1474
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->isQueueLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1478
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->refreshCloudQueue()V

    .line 1480
    :cond_0
    return-void
.end method
