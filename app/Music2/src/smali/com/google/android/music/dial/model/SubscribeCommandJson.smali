.class public Lcom/google/android/music/dial/model/SubscribeCommandJson;
.super Lcom/google/android/music/dial/model/GenericDialCommand;
.source "SubscribeCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;,
        Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;
    }
.end annotation


# instance fields
.field public mBody:Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "body"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/music/dial/model/GenericDialCommand;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/music/dial/model/CommandHeaderJson;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/CommandHeaderJson;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    .line 53
    iget-object v0, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mNamespace:Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mHeader:Lcom/google/android/music/dial/model/CommandHeaderJson;

    const-string v1, "subscribe"

    iput-object v1, v0, Lcom/google/android/music/dial/model/CommandHeaderJson;->mCommand:Ljava/lang/String;

    .line 56
    new-instance v0, Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;

    invoke-direct {v0}, Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mBody:Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;

    .line 57
    return-void
.end method


# virtual methods
.method protected getBodyAsJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson;->mBody:Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;

    invoke-virtual {v0}, Lcom/google/android/music/dial/model/SubscribeCommandJson$SubscribeCommandBody;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
